@extends('layouts.base')
@section('content')

 @include('errors.error_validation')
  <form style="border-style:dotted; border-width: 1px;" action="/payments{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFieldsForPayment()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/payments'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Payment

      </h3></div>
    </div>

    <div class="row">
      <div class="col-lg-6">

 <div class="form-group required">
              <label for="project_id@yield('project_name_d')">{{ __('labels.payments.project_name') }}</label>
              <select  class="form-control mandatory" name="project_id"
               id="project_id"@yield('project_name_d')>
                  @if(isset($project))
                  <option value="0">--select--</option>'
                  @foreach($project as $pro_val)
                  @if(isset($sel_project_id))
                  @if($pro_val->id === $sel_project_id)
                  <option value="{{$pro_val->id}}" selected>{{$pro_val->name}}</option>
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
       </div>

       </div>

         <div class="col-lg-6">
    <div class="form-group required">
           <label for="service_name">{{__('labels.payments.service_name')}}</label>
            @if(!empty(old('service_name')))
            <input type="text" class="form-control mandatory" id="service_name" name="service_name" @yield('service_name_d') value="{{ old('service_name') }}">
            @else
            <input type="text" class="form-control mandatory" id="service_name" name="service_name"  value=" @yield('service_name')">
            @endif
         </div>  
   </div>
  
    
  </div>


   <div class="row">
      <div class="col-lg-4">
         <div class="form-group required">
              <label for="organization_id@yield('org_name_d')">{{ __('labels.payments.org_name') }}</label>
              <select  class="form-control mandatory" name="organization_id"
               id="organization_id"@yield('org_name_d')>
                  @if(isset($organization))
                  <option value="0">--select--</option>'
                  @foreach($organization as $org_val)
                  @if(isset($sel_organization_id))
                  @if($org_val->id === $sel_organization_id)
                  <option value="{{$org_val->id}}" selected>{{$org_val->name}}</option>
                  @else
                  <option value="{{$org_val->id}}">{{$org_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$org_val->id}}">{{$org_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>     
  </div>
     <div class="col-lg-4">
 <div class="form-group required">
              <label for="expense_type_id@yield('expense_type_id_d')">{{ __('labels.payments.expense_type_id') }}</label>
              <select  class="form-control mandatory" name="expense_type_id"
               id="expense_type_id"@yield('expense_type_id_d')>
                  @if(isset($expense_type))
                  <option value="0">--select--</option>'
                  @foreach($expense_type as $exp_val)
                  @if(isset($sel_expense_type_id))
                  @if($exp_val->id === $sel_expense_type_id)
                  <option value="{{$exp_val->id}}" selected>{{$exp_val->name}}</option>
                  @else
                  <option value="{{$exp_val->id}}">{{$exp_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$exp_val->id}}">{{$exp_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>

     </div>
   <div class="col-lg-4">

      <div class="form-group required">
           <label for="expense_date">{{__('labels.payments.expense_date')}}</label>
            @if(!empty(old('expense_date')))
            <input type="date" class="form-control mandatory" name="expense_date" id="expense_date" value="{{ old('expense_date') }}"  @yield('expense_date_d') onkeydown="return false"/>
            @else
            <input type="date" class="form-control mandatory" name="expense_date" id="expense_date" value="@yield('expense_date')" @yield('expense_date_d') onkeydown="return false" />
            @endif

         </div>
     
   </div>
</div>

 <div class="row">
 
</div>
   <p></p>

 <div class="row">
   <div class="col-lg-6">
    <div class="form-group required">
           <label for="bill_no">{{__('labels.payments.bill_no')}}</label>
            @if(!empty(old('bill_no')))
            <input type="text"  class="form-control mandatory" name="bill_no" @yield('bill_no_d') value="{{ old('bill_no') }}">
            @else
            <input type="text" class="form-control mandatory" name="bill_no" value="@yield('bill_no')"   @yield('bill_no_d')>
            @endif
         </div>  
   </div>
   <div class="col-lg-6">
    <div class="form-group required">
           <label for="item_desc">{{__('labels.payments.item_desc')}}</label>
            @if(!empty(old('item_desc')))
            <input type="text" class="form-control mandatory" name="item_desc" @yield('item_desc_d') value="{{ old('item_desc') }}">
            @else
            <input type="text" class="form-control mandatory" name="item_desc" value="@yield('item_desc')"  @yield('item_desc_d')>
            @endif
         </div>  
   </div>
     
</div>

 <div class="row">
   <div class="col-lg-4">
    <div class="form-group required">
           <label for="unit">{{__('labels.payments.unit')}}</label>
            @if(!empty(old('unit')))
            <input type="text" class="form-control mandatory" id="unit" name="unit" @yield('unit_d') value="{{ old('unit') }}">
            @else
            <input type="text" class="form-control mandatory" id="unit" name="unit" value="@yield('unit')"  @yield('unit_d')>
            @endif
         </div>  
   </div>
   <div class="col-lg-4">
    <div class="form-group required">
            <label for="qty">{{__('labels.payments.qty')}}</label>
            @if(!empty(old('qty')))
            <input type="number" min="0" step="0.01"  id='qty' class="form-control mandatory " name="qty" value="{{ old('qty') }}" @yield('qty_d')>
            @else
            <input type="number"  min="0" step="0.01" id='qty' class="form-control mandatory" name="qty"  value="@yield('qty')" @yield('qty_d')>
            @endif
         </div>  
   </div>
      <div class="col-lg-4">
    <div class="form-group required">
           <label for="rate">{{__('labels.payments.rate')}}</label>
            @if(!empty(old('rate')))
            <input type="number" step="0.01" min="0" id="rate"  class="form-control mandatory " name="rate" value="{{ old('rate') }}" @yield('rate_d')>
            @else
            <input type="number" step="0.01" min="0" id='rate' class="form-control mandatory" name="rate" value="@yield('rate')"  @yield('rate_d')>
            @endif
         </div>  
   </div>

</div>
 <div class="row">
   <div class="col-lg-4">
    <div class="form-group required">
           <label for="base_amount">{{__('labels.payments.base_amount')}}</label>
            @if(!empty(old('base_amount')))
            <input type="number" step="0.01" min="0" id="base_amount"  class="form-control mandatory cal_amount "  name="base_amount" value="{{ old('base_amount') }}" @yield('base_amount_d')>
            @else
            <input type="number"  step="0.01" min="0" id="base_amount"  class="form-control mandatory" name="base_amount" value="@yield('base_amount')"  @yield('base_amount_d')>
            @endif
         </div>  
   </div>
   <div class="col-lg-4">
    <div class="form-group required">
           <label for="gst_amount">{{__('labels.payments.gst_amount')}}</label>
            @if(!empty(old('gst_amount')))
            <input type="number" step="0.01" min="0"  class="form-control mandatory" id="gst_amount" name="gst_amount" value="{{ old('gst_amount') }}" @yield('gst_amount_d')>
            @else
            <input type="number" step="0.01" min="0" class="form-control mandatory" id="gst_amount" name="gst_amount" value="@yield('gst_amount')"   @yield('gst_amount_d')>
            @endif
         </div>  
   </div>
    <div class="col-lg-4">
    <div class="form-group required">
           <label for="amount">{{__('labels.payments.amount')}}</label>
            @if(!empty(old('amount')))
            <input type="number" step="0.01" min="0" id="amount"  class="form-control mandatory" readonly="readonly" name="amount" value="{{ old('amount') }}" @yield('amount_d')>
            @else
            <input type="number"  step="0.01" min="0" id="amount"  class="form-control mandatory" readonly="readonly" name="amount" value="@yield('amount')"  @yield('amount_d')>
            @endif
         </div>  
   </div>
</div>
 <div class="row">
     <div class="col-lg-6">
    <div class="form-group ">
           <label for="amount_paid">{{__('labels.payments.amount_paid')}}</label>
            @if(!empty(old('amount_paid')))
            <input type="number" step="0.01" min="0"  class="form-control " name="amount_paid" value="{{ old('amount_paid') }}" @yield('amount_paid_d')>
            @else
            <input type="number" step="0.01" min="0"  class="form-control " name="amount_paid" value="@yield('amount_paid')"  @yield('amount_paid_d')>
            @endif
         </div>  
   </div> 

 </div>
<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.payments.submit')}}</button>
</div>
</div>
<p></p>

<!-- for import user -->
@section('importBoq')
@show
@endsection

@push('scripts')

<p></p>

<p></p>

</div>
</form> 

<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script>
$(document).ready(function(){

$('#organization_id').select2({
  }); 
$('#project_id').select2({
  });
$('#expense_type_id').select2({
  }); 


 var now = new Date(),
    // max date the user can choose, in this case now and in the future
minDate = now.toISOString().substring(0,10);
$('#expense_date').prop('max', minDate);


//------- amount calculation


//----- amount calculation
    var rr=$("#rate");
    rr.keyup(function(){
       var base_amount=isNaN(parseFloat( rr.val()* $("#qty").val())) ? 0 :( rr.val()* $("#qty").val())
       
        $("#base_amount").val(Number(base_amount).toFixed(2));

        var total=isNaN(parseFloat(gg.val()) + parseFloat($("#gst_amount").val())) ? 0 :
                      (parseFloat(gg.val()) + parseFloat($("#gst_amount").val()))
        $("#amount").val(Number(total).toFixed(2));
    });

    var qq=$("#qty");
    qq.keyup(function(){
       var base_amount=isNaN(parseFloat( qq.val()* $("#rate").val())) ? 0 :( qq.val()* $("#rate").val())
        $("#base_amount").val(Number(base_amount).toFixed(2));

        var total=isNaN(parseFloat(gg.val()) + parseFloat($("#gst_amount").val())) ? 0 :
                      (parseFloat(gg.val()) + parseFloat($("#gst_amount").val()))
        $("#amount").val(Number(total).toFixed(2));
    });


    var kk=$("#gst_amount");
    kk.keyup(function(){
       var total=isNaN(parseFloat(kk.val()) + parseFloat($("#base_amount").val())) ? 0 :
                      (parseFloat(kk.val()) + parseFloat($("#base_amount").val()))
        $("#amount").val(Number(total).toFixed(2));
    }); 

    var gg=$("#base_amount");
    //gg.change(function(){
    gg.on('input', function() {
       var total=isNaN(parseFloat(gg.val()) + parseFloat($("#gst_amount").val())) ? 0 :
                      (parseFloat(gg.val()) + parseFloat($("#gst_amount").val()))
        $("#amount").val(Number(total).toFixed(2));
    });

    ///--------- restrict inputs


document.querySelector("#unit").addEventListener("keypress", function (evt) {
    if (evt.which > 32 && evt.which < 65)
    {
        evt.preventDefault();
    }
});
document.querySelector("#service_name").addEventListener("keypress", function (evt) {
    if (evt.which > 32 && evt.which < 65)
    {
        evt.preventDefault();
    }
});


});




/*
 $(document).ready(function(){

  $(".cal_amount").keyup(function() {
    alert("m in");
    var rate = $("#rate").val();
    var qty = $("#qty").val();
    var calulated_amount = 0;
    if(rate != '' && qty != ''){
     calulated_amount = rate*qty;
    }
    alert(calulated_amount);
    $("#amount").val(calulated_amount);
  });
});

$("#organization_id").on("change", function() {
      // alert('ok');
      org_id = $("#organization_id").val();
      console.log(org_id)
      if(org_id !=0) {
      // get_project_url = APP.'getCompanyName/'+formData;
        get_project_url= "{{url('/')}}"+"/getCompanyProject/"+org_id
        console.log(get_project_url)
        $.ajax({
          url: get_project_url ,
          method: 'get',
          type: 'JSON',
          success: function(response) {
            console.log('This is response')
            console.log(response)
            $('#project_id option').remove();
            $('#project_id').append("<option value=''>--Select--</option>");
            console.log($.type(response))
            for (var i = 0; i < response.length; i++) {
              console.log(response[0]);
              $('#project_id').append("<option id='client_role_idOptions' value="+response[0]['id']+">"+response[i]['name']+"</option>");
              console.log(response[0]['name']);
            }
          },
          error: function() {
            alert('Error : There no projects')
          }
        })
      }else {
          $('#project_id option').remove();

      }
    })
*/


function validateMandatoryFieldsForPayment(){

  var valid = true;
  valid=validateMandatoryFields();

      var project_id = $('#project_id').val();
    // alert(project_id);
  if(project_id == 0) {
document.getElementById("select2-project_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-project_id-container").style.border = "0px solid red";

}

      var organization_id = $('#organization_id').val();
    // alert(organization_id);
  if(organization_id == 0) {
document.getElementById("select2-organization_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-organization_id-container").style.border = "0px solid red";

}

      var expense_type_id = $('#expense_type_id').val();
    // alert(expense_type_id);
  if(expense_type_id == 0) {
document.getElementById("select2-expense_type_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-expense_type_id-container").style.border = "0px solid red";

}

     return valid;
}

</script>
@endpush


