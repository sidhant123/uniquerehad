<?php

Route::get('/flush', 'vnnogile\client\controllers\CacheController@flushCacheView')->name('flush.show');
Route::post('/flush', 'vnnogile\client\controllers\CacheController@flushCache')->name('flush.cache');
