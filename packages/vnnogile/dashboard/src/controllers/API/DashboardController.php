<?php

namespace vnnogile\Dashboard\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//custom

use Auth;
use vnnogile\Dashboard\Services\DashboardService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;


class DashboardController extends Controller
{

    public function getQuizPerformanceForUserInProgram(Request $request){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $validator = Validator::make($request->all(), [
            'userId' => 'required|numeric',
            'programId' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = Lang::get('custom.invalid_input');
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return Response::json($response);
        };
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return DashboardService::getQuizPerformanceForUserInProgram($request['programId'], $request['userId']);
    }

    public function getOverallQuizPerformanceForUser(Request $request){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $validator = Validator::make($request->all(), [
            'userId' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = Lang::get('custom.invalid_input');
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return Response::json($response);
        };
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return DashboardService::getQuizPerformanceForUser($request['userId']);
        
    }

}
