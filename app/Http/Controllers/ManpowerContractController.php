<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;
use App\Services\ValidationRulesService;

use App\Models\ManpowerContract;
use App\Services\ManpowerContractService;
use App\Services\ProjectService;
use App\Services\SiteService;
use App\Services\OrganizationService;
use App\Services\BoqService;
use App\Http\Controllers\Controller\PaymentController;

class ManpowerContractController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Log::debug(\Auth::user()->hasAccess(['ManpowerContract']));
        //$this->authorize('List ManpowerContract', 'List ManpowerContract');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request); App\Utilities\ManpowerContractFilterUtility
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return PaginateController::paginate('ManpowerContract', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
     //     $this->authorize('Create manpowerContracts', 'Create manpowerContracts');
         $organization = OrganizationService::retrieveOrganization();
         // $site = SiteService::retrieveSites();
         $project = ProjectService::retrieveProject();
         return view('manpower_contract.create_manpower_contract',compact('organization','project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Create manpowerContracts', 'Create manpowerContracts');
        try {
             $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            $rules = $valdationRulesService->validationRulesWithTenant('ManpowerContract');
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return redirect('manpowerContracts/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }         

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['ManpowerContract'] = $request;
            $data['request'] = $request;
           ManpowerContractService::saveManpowerContractData($data);
           

            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ManpowerContractController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
        $row_data = ManpowerContract::where('id', $id)->first();
        $organization = OrganizationService::retrieveOrganization();
        // $site = SiteService::retrieveSites();
        $project = ProjectService::retrieveProject();
        $sel_organization_id = $row_data['organization_id'];
        $sel_project_id = $row_data['site_id'];
         $row_data['status'] = trim($row_data['status']);
        $sel_service = $row_data['service_name'];
         $service = BoqService::getBoqServiceList($sel_project_id);  
            return view('manpower_contract.view_manpower_contract', compact('row_data','organization','project','sel_organization_id','sel_project_id','sel_service','service'));


        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
         $row_data = ManpowerContract::where('id', $id)->first();
         $organization = OrganizationService::retrieveOrganization();
         // $site = SiteService::retrieveSites();
        $project = ProjectService::retrieveProject();
        $sel_organization_id = $row_data['organization_id'];
        $sel_project_id = $row_data['project_id'];
        $sel_service = $row_data['service_name'];
         $service = BoqService::getBoqServiceList($sel_project_id);  
         $row_data['status'] = trim($row_data['status']);
        return view('manpower_contract.edit_manpower_contract',compact('row_data','organization','project','sel_organization_id','sel_project_id','sel_service','service'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Edit manpowerContracts', 'Edit manpowerContracts');
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('ManpowerContract', '$manpower_contract', $id);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                   return redirect('manpowerContracts/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
            $data['request'] = $request;

               ManpowerContractService::updateData($data);
          
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ManpowerContractController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Delete ManpowerContracts', 'Delete ManpowerContracts');

            $deleted = ManpowerContractService::deleteManpowerContract($id);
            if ($deleted) {
                Session::flash('status', 'ManpowerContract deleted successfully');
            } else {
                Session::flash('status', 'ManpowerContract cant be deleted');
            }
            // $path_array = explode('/', action('ManpowerContractController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'manpowerContracts';

        } catch (Exception $e) {
            Log::error('Exception while deleting ManpowerContract ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }


    public static function retriveServiceList($id=null) {
    
       $data = BoqService::getBoqServiceList($id);  
       
        return $data;
    }
}
