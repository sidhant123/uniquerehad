console.log(max_cell_no);

   $(document).on('click','#svg_div a',function(e){
       //
      // e.preventDefault();
       var href= $(this).attr('xlink:href')
       console.log($(this).attr('xlink:href'));
      if (href) {
       if (href.includes('svg')) {
          //
           e.preventDefault();

           if (href.includes('/svg')) {
              // getSvgData(href);
           }
           else{
              href="/"+href;
           }
           getSvgData(href);
       }
       else{
          // no action
       } 
     }
  })

  function getSvgData(url){
      //
      axios.get(url)
        .then(function (response) {

             data=response['data'];
             // console.log(data);
             
             // setting into the svg div
             $('#svg_div').html(data);
                          

        }).catch(function (error) {
             console.log("error "+error);
        });
  }



 ///////////////////////// new events for program \\\\\\\\\\\\\\\\\\\\
 // var facilitator=new Object();
 var facilitator_object=new Object();
 var moderator_object=new Object();
 // appendModerator();

 $(document).on('click','#load_learning_tree', function(){
    //
    // url
        var learning_tree_id=$('#selected_learning_tree_id').val();
        if (learning_tree_id != '0') {
            
            // if changing 
            // if ($('#learning_tree_div').css('display') == 'none') {
            if ($('#prime').has('td').length) {
                var r=confirm('Are you sure to import above Learning Tree and remove existing Learning Tree for program');
                // deleting older lt topics from program
            }
            else{
                var r=confirm('Are you sure to import above learning tree for program');
            }
            if (r == true) { 
               saveLearningTreeForProgram(learning_tree_id);
            } 
            
        }
 })

 function saveLearningTreeForProgram(learning_tree_id){
    //
    var program_id=$('#program_hidden_program_id').val();

    var url='/saveLTForProgram/'+learning_tree_id+"/"+program_id;
    axios.get(url)
        .then(function (response) {
        
        var data=response['data'];
        console.log(data);

        $('#learning_tree_div').show();
        
        // setting facilitator object
        facilitator_object=data['facilitator'];

        // setiing moderators object
        moderator_object=data['moderator'];

        // checking for publish btn
        if (data['publish'] == 'yes') {
           $('#publish_save_btn').show();
        }
        else if (data['publish'] == 'no'){
           $('#publish_save_btn').hide();
        }

        // creating table
        createStructure(data['object'],1);

    })
        .catch(function (error) {
        console.log("error"+error);
    });
 }


 ////////////////////////////////// JS from learning tree blade  \\\\\\\\\\\\\\\\\\\\\

 //$(document).ready(function() {

   //
   $("table").colResizable();

   // for modal select
   $('#modal_form_id select').css('width', '100%');
  
   $('#learning_tree_id').select2({
   });
   $('#myModal #learning_tree_id').empty();
   $('#myModal #learning_tree_id').append('<option value="0">--select--</option>');

   $('#selected_learning_tree_id').empty();
   $('#selected_learning_tree_id').append('<option value="0">--select--</option>');
   
   $('#facilitator_id').select2({
    });
   $('#facilitator_id').empty();
   $('#facilitator_id').append('<option value="0">--select--</option>');

   $('#moderator_id').select2({
    });
   $('#moderator_id').empty();
   $('#moderator_id').append('<option value="0">--select--</option>');

   // date pickers for durations
   //$("#program_lt_from_id").datepicker({
     //           dateFormat: "dd-mm-yy",
       //  minDate: new Date(),
         //   });
//   $("#program_lt_to_id").datepicker({
  //              dateFormat: "dd-mm-yy",
    //     minDate: new Date(),
      //      });

 var newDate=new Date();
   newDate.setHours(0,0,0,0);
   console.log(newDate);

   $('#program_lt_from_id').datetimepicker({

      format: 'DD-MM-YYYY HH:mm',
      // minDate: new Date(),
      // minDate: newDate,
       defaultDate:newDate
   });
   $('#program_lt_from_id').data("DateTimePicker").minDate(newDate);

   $('#program_lt_to_id').datetimepicker({
      format: 'DD-MM-YYYY HH:mm',
      // minDate: newDate,
   });
   $('#program_lt_to_id').data("DateTimePicker").minDate(newDate);


   // datepicker logic
   $('#program_lt_from_id').on('dp.change', function(e){
     // console.log(e.data().DateTimePicker.date()._d);
     var start_date=$('#program_lt_from_id').data().DateTimePicker.date()._d;
     console.log(start_date);

     $('#program_lt_to_id').data("DateTimePicker").minDate(start_date);

     if ($('#program_lt_to_id').val()!='') {
         var end_date=$('#program_lt_to_id').data().DateTimePicker.date()._d;
         console.log(end_date);

         if (start_date > end_date) {
             $("#program_lt_to_id").val('');
         }
     }
     var durations = $('#expected_duration').val();
     autoPopulateDate(start_date,durations);
     
  })

 // });


  $(document).on('keyup','#expected_duration',function(){
    console.log("durations");
    var durations = $(this).val();
    // var program_start_date = $('#program_lt_from_id').val();
    if ($('#program_lt_from_id').val()!='') {
        var program_start_date =$('#program_lt_from_id').data().DateTimePicker.date()._d;
        autoPopulateDate(program_start_date,durations);
     }
   });

  $('#program_lt_to_id').on('dp.change', function(e){
    calculateDateDifference();
  });

function autoPopulateDate(program_start_date,durations){

    var newdate = program_start_date;
    console.log('hello');
    newdate.setDate(newdate.getDate() + Number(durations));
    console.log(newdate);
    if(durations){
      // $('#program_lt_to_id').data().DateTimePicker.date(newdate);
      $('#program_lt_to_id').data("DateTimePicker").date(newdate);
    }
 }

  function calculateDateDifference(){
        var program_start_date = $('#program_lt_from_id').val();
        var program_end_date = $('#program_lt_to_id').val();

        var start_date_array  = program_start_date.split(' ');
        var start_date_array1  = start_date_array[0].split('-');
        var start_date_array2  = start_date_array[1].split(':');

        var end_date_array  = program_end_date.split(' ');
        var end_date_array1  = end_date_array[0].split('-');
        var end_date_array2  = end_date_array[1].split(':');

        var converted_start_date = new Date(start_date_array1[2], start_date_array1[1], start_date_array1[0], start_date_array2[0], start_date_array2[1]);
        var converted_end_date = new Date(end_date_array1[2], end_date_array1[1], end_date_array1[0], end_date_array2[0], end_date_array2[1]);
        var addDay = 0;
        // if(start_date_array1[1] == end_date_array1[1]){
        //   addDay = 1;
        // }
        var timeDiff = Math.abs(converted_end_date.getTime() - converted_start_date.getTime());
        var diffDays = Math.ceil((timeDiff / (1000 * 3600 * 24)) + addDay);//Math.abs(date2.getDate() - date1.getDate());
        $('#expected_duration').val(diffDays);
 }
   
   
/////////////////////////////////////// ends  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


////////////////////////////////// L.T. Configuration \\\\\\\\\\\\\\\\\\\\\\\\\


 function populateLearningTreeConfiguration(){
    
         var containerLearningTree=$('#hidden_learning_tree_id_id').val();
         getStructureObjectsFromDataBase(containerLearningTree,'onUpdateLearningTree',0);
}


  var right_clicked_td;
  var artefact_type_id;

  var entityNameObject=new Object();
  // entityNameObject['vnn-bookmark draggable']='MajorNode';
  entityNameObject['vnn-node']='MajorNode';

  entityNameObject['vnn-video']='Video';
  entityNameObject['vnn-pdf']='Pdf';
  entityNameObject['vnn-word']='Word';
   entityNameObject['vnn-ppt']='Ppt';
  // entityNameObject['vnn-document']='Document';

  entityNameObject['vnn-quiz']='Quiz';
  entityNameObject['vnn-assessment']='Assessment';

  entityNameObject['vnn-vc']='Virtual Classroom';
  entityNameObject['vnn-youtube']='Youtube';
  entityNameObject['vnn-tree']='LearningTree';
  entityNameObject['vnn-audio']='Audio';
  entityNameObject['vnn-assignment']='Assignment';

  entityNameObject['vnn-web']='Web';
  entityNameObject['vnn-text']='Text';
  entityNameObject['vnn-xl']='Exel';





///////// tabs starts

////////tabs ends


/////////drag and drop starts
function allowDrop(ev) {
    ev.preventDefault();
}
function noAllowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    console.log(data);
    var cl=$('#'+data).attr('class');
    console.log(data+" "+cl);

    $('#'+data).parent().append("<i class='"+cl+"' id='"+data+"' draggable='true' ondragstart='drag(event)' ></i>");
    ev.target.appendChild(document.getElementById(data));
    // ev.target.setAttribute('ondragover','noAllowDrop(event)')

    // removing more drag and drop for list 
    ev.target.removeAttribute('ondragover');
    ev.target.removeAttribute('ondrop');

    $('#'+ev.target.id).find('i').removeAttr('ondragstart');
    $('#'+ev.target.id).find('i').removeAttr('draggable');
    // ev.target.find('i').removeAttribute('draggable');
    


    //// before presenting modal
    resetModal();
    $('#modal_heading').html('Create Topic');

    // setting hidden_parent_topic_id
    $('#hidden_parent_topic_id').val('a');

    ////// setting current list a id
    $('#'+ev.target.id).attr('id','current_list');
    console.log($('#'+ev.target.id).html());


    // for docment path -> entity name;
    var icon_class = $('#'+data).attr('class');
    console.log(icon_class);
    saveEntityName(icon_class);
    if (icon_class == 'vnn-vc') {
        $('#vc_participant_link').hide();
    }

    // for moderator
    if (icon_class =='vnn-vc') {
      //
      appendModerator();
      appendFacilitator();
    }

    $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
      });

}
///////////// d & d ends \\\\\\\\\\\



/////////// modal starts
 $('#closeBtn').click(function() {

    // tab logic
    if ($('#hidden_parent_topic_id').val() == "") {
         //removing tab
     }

    if ($('#hidden_flag').val() != "update"){
        $('#current_list').parent().remove();
        $('#current_list').removeAttr('id');
        // resetModal();
    }
  });

 $('#modal_close_sign').click(function() {

    // tab logic
    if ($('#hidden_parent_topic_id').val() == "") {
         //removing tab
     }

    else if($('#hidden_flag').val() != "update"){
        // removing list
        $('#current_list').parent().remove();
        // removing id
        $('#current_list').removeAttr('id');
        // resetModal();
    }

  });

  function resetModal() {
	var date = new Date();
      $('#modal_form_id').find(':input').val("");
      $('#modal_form_id').find(':input[type="checkbox"]').attr('checked',false);

      $('#document_path_option_id').empty();
      //changing id off
      $('#current_list').removeAttr('id');

      $('#alert_before_nodes').removeClass('mandatory');
      //for learning tree id
      // $('#myModal #learning_tree_id').empty();
      // $('#myModal #learning_tree_id').append('<option value="0">--select--</option>');
	//$("#myModal #program_lt_from_id").datepicker("setDate", date);
    //$("#myModal #program_lt_to_id").datepicker("setDate", date);
    //calculateDateDifference();

    /// node_icon_file
    $('#node_icon_file').html('');    

  }
 //////////// modal ends


   function deleteCurrentList(node_info){
         //
         console.log(node_info);

         $('#current_list').parent().find('#node_topic').html('&nbsp;'+node_info['topic_name']);
         $('#current_list').parent().attr('title',node_info['topic_name']);
         // adding value in hidden field
         console.log(node_info['topic_id']);
         // $('#current_list').find(':input[type="hidden"]').val(node_info['node']);
         $('#current_list').find('.list_hidden_node_value').val(node_info['topic_id']);
         $('#current_list').find('.list_hidden_node_value').attr('id',node_info['topic_id']);

         // removing current id
         $('#current_list').removeAttr('id');
   } 

   function validateMandatoryFields(id){

         var valid = true;
         $("#"+id+" "+".mandatory").each(function () {
          if ($(this).val().trim() === '' || $(this).val() === '0')
          {
              $(".mandatory").removeClass('fillFields');
              $(this).addClass('fillFields');
              valid = false;
              // console.log($(this).val());
              return valid;
            }
         });

         console.log('in validateMandatoryFields'+valid);
          return valid;
    }


////////// end of onchang parent node select

  ////on clicking elsewhere
   $('html').click(function (e) {
     console.log(e.target.className);

    if (e.target.className == 'td_li' || $(e.target).parent().attr('class') == 'td_li') {
    console.log($(this).attr('id'));

    $('#selected_list').removeAttr('id');
    
    $('.td_li').css('background-color','skyblue');

    if (e.target.className == 'td_li') {
     $(e.target).attr('id','selected_list');
     $(e.target).css('background-color','gray');
    }
    else if ($(e.target).parent().attr('class') == 'td_li') {
      //
     $(e.target).parent().attr('id','selected_list');
     $(e.target).parent().css('background-color','gray');
    }
    else{ }

    } 
    else{

        // event.target.className
        $('#selected_list').css('background-color','skyblue');
        $('#selected_list').removeAttr('id');
        // $('.td_li').css('background-color','skyblue');
        console.log('outside the list');
    }

    console.log($('svg_div').has(e.target).length);
    if (!$('.svg_div_class').has(e.target).length) {
       //
       console.log('inside of svg_div')
       var svg_div_width=parseInt($("#svg_div").width() / $("#svg_div").parent().width() * 100);
      
       if(svg_div_width ==20){

        $("#svg_div").animate({width: '0%'});
        $("#svg_btn").animate({right: '0%'});

        $("#svg_btn").html('&laquo;');
        $('#svg_parent').css('width','2.5%');
        $('#svg_btn').css('width','100%');
 
       }

    }

});


///////////////////////////////// for document path \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  $(document).on('click','#document_button_id',function(){
    //validation

    console.log(artefact_type_id+'checkit');
    var tag=document.getElementById('document_input_id').value;
    console.log(tag);
    if(tag){
    getTopicDocumentPath('/topicDocument/'+tag+'/'+artefact_type_id);
    }

});

   var documents=new Object();

function getTopicDocumentPath(url){
    axios.get(url)
        .then(function (response) {

        console.log(response);
        console.log('after printing response getTopicDocumentPath');

        $("#document_path_option_id").empty();
        if(Object.keys(response).length > 0){


           $.each(response['data'], function (key, value) {
           console.log(value.id);
           $("#document_path_option_id").append("<tr><td width=10% ><input type=checkbox class='document_checkbox_class' name='entity_id' value="+value.id+"></td><td width=90%>"+value.name+"</td></tr>")

           documents[value.id]=[value.name,value.document_path,value.duration,value.size,value.description,value.pages,value.title,value.summary];
           });
        }
    })
        .catch(function (error) {
        console.log("error"+error);
   });
  }


   $(document).on('change','input.document_checkbox_class',function(){
        // if(this.checked == true){
        var id=$(this).val();
        // $('#hidden_document_name_id').val=documents[id][0];
        // document.getElementById('hidden_document_name_id2').value=documents[id][0];
        document.getElementById('hidden_document_name_id2').value=documents[id][6];
        console.log(documents[id][0]);
        document.getElementById('hidden_document_url_id2').value=documents[id][1];

        document.getElementById('hidden_document_duration_id').value=documents[id][2];
        document.getElementById('hidden_document_size_id').value=documents[id][3];
        document.getElementById('hidden_document_description_id').value=documents[id][4];

        document.getElementById('hidden_document_pages_id').value=documents[id][5];
        document.getElementById('hidden_document_title_id').value=documents[id][6];
        document.getElementById('hidden_document_summary_id').value=documents[id][7];

        
        console.log(id+' '+documents[id][0]+' '+documents[id][1]+' '+documents[id][2]+' '+documents[id][3]+' '+documents[id][4]);

         $('input.document_checkbox_class').not(this).prop('checked', false);

         // for autopopulate
         if (artefact_type_id == 'Assessment' || artefact_type_id == 'Quiz') {
             document.getElementById('hidden_document_name_id2').value=documents[id][0];
             $('#topic_name').val(documents[id][0]);
             // $('#topic_description').val(documents[id][4]);
         }
         else{
             $('#topic_name').val(documents[id][6]);
             $('#topic_description').val(documents[id][7]);
         }
      });

//////// document path ends




  function saveEntityName(icon_class){

       console.log(icon_class);
       $.each(entityNameObject, function (clas, artefact_type) {
            console.log(clas+" "+artefact_type);
             if(icon_class==clas){

                console.log(artefact_type);
                artefact_type_id=artefact_type;
                $('#hidden_entity_type_id').val(artefact_type);
             }
           });
       /// changing modal as per the artefact type
       if (artefact_type_id == 'MajorNode') {
           $('#document_section').hide();
           $('#lt_search_section').hide(); 
           $('#lt_option_section').hide();
           $('#moderator_section_id').hide();
           $('#icon_div').hide();
           $('#assignment_section').hide();
           $('#majornode_section').show();
           $('#is_optional_section').hide();
           $('#status_color_section').show();
           setStatusColorValues();
       }
       else if (artefact_type_id == 'LearningTree') { 
           $('#document_section').hide();
           $('#lt_search_section').show();
           $('#lt_option_section').hide();
           $('#moderator_section_id').hide();
           $('#icon_div').hide();
           $('#assignment_section').hide();
           $('#majornode_section').hide();
           $('#is_optional_section').hide();
           $('#status_color_section').hide();
       }
       else if (artefact_type_id == 'Virtual Classroom') {
          $('#document_section').hide();
           $('#lt_search_section').hide(); 
           $('#lt_option_section').hide();
           $('#moderator_section_id').show();
           $('#icon_div').show();
           $('#assignment_section').hide();
           $('#majornode_section').hide();
           $('#is_optional_section').show();
           $('#status_color_section').hide();
       } 
       else if (artefact_type_id == 'Assignment') {
           $('#document_section').hide();
           $('#lt_search_section').hide(); 
           $('#lt_option_section').hide();
           $('#icon_div').show();
           $('#moderator_section_id').hide();
           $('#assignment_section').show(); 
           // clearing earlier table
           $('#file_list tr').remove(); 
           $('#doc_list tr').remove(); 
           $('#majornode_section').hide();
           $('#is_optional_section').hide();
           $('#status_color_section').hide();
           //
           $('#links_id').val('links');
           $('#file_upload_id').val('file');
           $('#edit_pdf_id').val('editDoc');

           $('#submit_type_id').val('submit');
           $('#save_type_id').val('save');

           // $("[name=activityAction]").val(["links"]);
           // $('#file_wrapper').show();
       }
       else {
           $('#document_section').show();
           $('#lt_search_section').hide(); 
           $('#lt_option_section').hide();
           $('#moderator_section_id').hide();
           $('#icon_div').show();
           $('#assignment_section').hide();
           $('#majornode_section').hide();
           $('#is_optional_section').show();
           $('#status_color_section').hide();
       }
  }

//////////// show section for tab   \\\\\\\\\\\\\\\\\\\\
  function showSectionForTab(){
    //
    $('#document_section').hide();
    $('#lt_search_section').hide();
    $('#lt_option_section').show();
    $('#icon_div').show();
    $('#moderator_section_id').hide();
    $('#assignment_section').hide();
    $('#majornode_section').hide();
    $('#is_optional_section').hide();
    $('#status_color_section').show();
    setStatusColorValues();
  }

 function setStatusColorValues(){
    $('#sequence_method_id').val('sequence');
    $('#parallel_method_id').val('parallel'); 
    $('[name=topic_status_color_allocation_method]').attr('checked', false);
 }

  var i=1;
  $("input").keypress(function(ee){
      if(ee.keyCode === 13){
             
          i=0;
          console.log('hi');
        }
   });

  $(document).on('click','#lt_option_btn',function(e){
        //
        e.preventDefault();
        console.log(e.keyCode);

        console.log(i);
        if (i==1) {
        if ($('#lt_search_section').is(":visible")) {
            
            $('#lt_search_section').hide();
            $('#hidden_entity_type_id').val('MajorNode');
        }
        else{
            // clearing lt
            $("#learning_tree_id").select2("val","0");
            $('#lt_search_section').show();
            $('#hidden_entity_type_id').val('LearningTree');
        }   
       } 
       else{
        i=1;
       }
      
  })  





//////////////////////////////////////////// new logics \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ //////////  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
////////


// single object
var cell_object=new Object();

 function createTableForParentTopic(){

         /// start
         var i=0;
         var j=1;
         // append at first row
         var row_id = appendRowN(j);
         console.log(row_id);

       $.each(cell_object, function (cell_no, topic_info) {

          i++;
          if (cell_no%5 == 1 && cell_no > 5) {
           j++;
           // append at row level
           row_id = appendRowN(j);
           console.log(j);
          }
          console.log(i);

          // append at cell level
          console.log(row_id);
          var cell_id = appendCellN(cell_no,row_id);
          
          if (topic_info) {
            appendsListN(cell_no,topic_info);
          }
        });

       $('td').attr('contextmenu','mymenu');
       $(".loader").fadeOut('slow');
  }


  function appendRowN(row_value){

       var id='tr'+row_value;
       $('#table_body').append("<tr height='110px' id="+id+"></tr>");
       console.log(id);
       return id;
  }

  function appendCellN(cell_value,row_id){

       var id='td'+cell_value;
       $('#'+row_id).append("<td style='width: 20%;' class='table-cell' id="+id+"><ul style='list-style-type: none;' ></ul></td>")
       console.log(id);
       return id;
  }


  function appendsListN(cell_no,topic_info){

      // appending UL
       var cell_id='td'+cell_no;

       // getting icon class
       var icon_class;
       $.each(entityNameObject, function (clas, artefact_type) {

             if(artefact_type==topic_info['entity_type']){
                console.log(topic_info['entity_type']);
                icon_class=clas;
             }
           });

       //showing on screen table
       console.log(cell_id+' '+topic_info['topic_name']+' '+icon_class);
       // $('#'+cell_id+' ul').append("<li class='td_li' title="+topic_info['topic_name']+"><span style='height: 15px; width: 13px; background-color: gainsboro; display:inline-block;' ondragover='allowDrop(event)' ondrop='drop(event)'><input id="+topic_info['topic_id']+" class='list_hidden_node_value' type='hidden' value="+topic_info['topic_id']+"><i id='drag1' class="+icon_class+" ondragstart='drag(event)' draggable='true'></i></span><span id='node_topic'>  "+topic_info['topic_name']+"</span></li>");
       $('#'+cell_id+' ul').append("<li class='td_li' title='"+topic_info['topic_name']+"'><span style='height: 15px; width: 13px; background-color: gainsboro; display:inline-block;'><input id="+topic_info['topic_id']+" class='list_hidden_node_value' type='hidden' value="+topic_info['topic_id']+"><i id='drag1' class="+icon_class+" ></i></span><span id='node_topic'>  "+topic_info['topic_name']+"</span></li>");
    
       // $(".loader").fadeOut('slow');
  }

  function tabStructureN(id,name){

    var i=1;
    $(".tab_button").each(function () {
           i++;
     });
    console.log(i);

    var tab_id="tab"+i;
    $('#plus').remove();
    $('.tab').append("<button class='tab_button' id="+tab_id+" onclick=tabEvent('"+tab_id+"')>"+i+"<input type=hidden id="+name+" value="+id+"></button>");
    $('.tab').append("<button id='plus' style='width: 30px;' onclick='increaseButton()'>+</button>");

    
    // creating table
    $('#table_body').find('tr').remove();
    createTableForParentTopic();

    tabEventN(tab_id);


    // storingObjectInDatabase(nodeData,nodeNoData,cells_nodes_object,'save');

    return('successful');
  }



  function tabEventN(id){
   console.log(id);
   $('.tab_button').css('background-color','#f1f1f1;');
   $('#'+id).css('background-color','#ddd');

   //setting tab value
   var tab_no=id.split('b');
   document.getElementById('hidden_tab_value').value=tab_no[1];

}


 $('#saveBtn').click(function() {

       var tab=$('#hidden_tab_value').val();
       var node_for_edit=$('#hidden_flag').val();

//////// tab logic
if ($('#hidden_parent_topic_id').val() == '') {
        
         var formDataForTab =  new FormData(document.getElementById("modal_form_id"));
         if(validateMandatoryFields('modal_form_id')== true && validateModal()== true){
           console.log('verified');
         }
         else{
           //alert('please fill all the mandatory fields');
           bootbox.alert('please fill all the mandatory fields');
            return false;
         }

      // save in database
        var containerLearningTreeId=$('#hidden_learning_tree_id_id').val();
        var containerLearningTreeName=$('#hidden_learning_tree_name_id').val();

        formDataForTab.append('containerLearningTree', containerLearningTreeId);
        formDataForTab.append('containerLearningTreeName', containerLearningTreeName);
        formDataForTab.append('parent_topic_id',0);

        var url='/saveProgramTopic';
        var topic_id=saveTopicInDatabase(url,formDataForTab,'tab',0);

     }
///////// end tab logic
  else{

     //form logic
      var formData = new FormData(document.getElementById("modal_form_id"));
      console.log(formData); 

      if(validateMandatoryFields('modal_form_id')== true && validateModal()==true){

        // if(node_for_edit != ""){
        //    //setting id

        // }

         // save in database

         var cells_id=$('#current_list').parent().parent().parent().attr('id');
         var cells_id_array=cells_id.split('d');
         console.log(cells_id_array[1]);
         // formData.append('cell_no', cells_id_array[1]);
         var cell_no=cells_id_array[1]
         
         var containerLearningTreeId=$('#hidden_learning_tree_id_id').val();
         formData.append('containerLearningTree', containerLearningTreeId);

         // adding parent topic
         formData.append('parent_topic_id', $('#parent_topic_id_id').val());
         formData.append('parent_topic_title', $('#parent_topic_name').val());
         
         // creating sequence
         var sequence= saveSequence(cell_no);
         console.log(sequence);
         formData.append('sequence', sequence);

         var url='/saveProgramTopic';
         var topic_id=saveTopicInDatabase(url,formData,'node',cell_no);
        //apply data-dismiss
      }
      else{
       //alert('please fill all the mandatory fields');
	bootbox.alert('please fill all the mandatory fields');
        // remove data dismiss
         return false;
      }
    }

});

 

  function saveTopicInDatabase(url,data,flag,cell_no){
     
     var tab=$('#hidden_tab_value').val();

     console.log('in saveTopicInDatabase function');
     console.log(url+" "+data+" "+tab+" "+flag);

     axios({
        method: 'post',
        url: url,
        data: data,

       }).then(function(response) {

        var server_data=response['data'];
        console.log(server_data);

        if (flag =='node')
        {

        // saving in breadcrumb  
        // recreating will happen on adding lt in middle

        

        // saving in cell_object

        var node_for_edit=$('#hidden_flag').val();

        if(node_for_edit == ""){
           //
           console.log('in save');
           cell_object=server_data['object'];
           $('#table_body').find('tr').remove();
           createTableForParentTopic();
        }
        else if(node_for_edit == 'update'){
            //
            console.log('in update');
            cell_object[cell_no]={topic_id:server_data['topic_id'], entity_type:server_data['entity_type'], topic_name:server_data['topic_name'], sequence:server_data['sequence']};
           // showing on label
           deleteCurrentList(server_data);
        }

        // createTableForParentTopic();

        }
        else if (flag == 'tab')
        {
        //
        // saving parent box
        // saveTopicInParentBox();
        $('#parent_topic_name').val(server_data['topic_name']);
        $('#parent_topic_id_id').val(server_data['topic_id']);

        // saving in breadcrumb

        
        // crating tab structure
        // var node_for_edit=$('#hidden_old_node_no').val();
        var node_for_edit=$('#hidden_flag').val();

        if(node_for_edit == ""){
          // setting cell object
          cell_object=server_data['object'];
          var a=tabStructureN(server_data['topic_id'],server_data['topic_name']);
        }
        else if(node_for_edit == 'update'){

          // getting topic id of tab
          var tab=$('#hidden_tab_value').val();
          $('#tab'+tab).find('input').attr('id',server_data['topic_name']);
          // $('#tab'+tab).find('input').attr('class',server_data['facilitator_id']);
        }


        }

      }).catch(function (error) {
      console.log("error"+error);
     });
  }


  function increaseButton(){

    var i=1;
    $(".tab_button").each(function () {
           i++;
     });
    console.log(i);

  // providing madal  to be filled
    resetModal();
    $('#modal_heading').html('Create Topic');

    // no entity type
    $('#hidden_entity_type_id').val('MajorNode');

    // hiding document part
    // $('#document_section').hide();
    // $('#lt_search_section').hide();
    
    // $('#lt_option_section').show();
    // $('#icon_div').show();
    // $('#moderator_section_id').hide();
    showSectionForTab();

    /////////// new settings
    $('#hidden_parent_topic_id').val('');

    $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
    });

    // saving topic in database
  }


  function tabEvent(id){
   console.log(id);

   // if same tab dont proceed -- dont check like this
   var tab_topic_id=$('#'+id).find('input').val()
   var current_parent_id= $('#parent_topic_id_id').val();
   console.log(tab_topic_id+" "+current_parent_id);

   if (tab_topic_id == current_parent_id) {
      return false;
   }


   $(".loader").fadeIn();
   tabEventN(id);

   // getting topic id of tab
   var topic_id=$('#'+id).find('input').val();
   var topic_name=$('#'+id).find('input').attr('id');

   // getting structure from the database
   getParentStructure(topic_id,topic_name);

}

function getParentStructure(id,name){
     //
      
      var containerLearningTree=$('#hidden_learning_tree_id_id').val();
      var url="/getProgramLtParentStructure/"+containerLearningTree+"/"+id;

    axios.get(url)
        .then(function (response) {

             data=response['data'];
             console.log(data);
             // return data;
             
             // setting the object value
             cell_object=data;
             
             // setting parent box
             $('#parent_topic_name').val(name);
             $('#parent_topic_id_id').val(id);

             //creating table for tab
             $('#table_body').find('tr').remove();
             createTableForParentTopic();

        }).catch(function (error) {
             console.log("error "+error);
        });

}


  $(document).on('dblclick','.table-cell',function() {

         //checking previous list has
         console.log($("#prime").find(".td_li:last").attr('class'));
         var cell_id=$(this).attr('id');

         var j=0;
         if (!$(this).parent().parent().find(".td_li:last").attr("class")) {
             j=1;
             $('ul', this).append("<li class='td_li'><span id='temp' ondrop='drop(event)' ondragover='allowDrop(event)' style='height: 15px; width: 13px; background-color: gainsboro; display: inline-block'><input type='hidden' id='node_no' class='list_hidden_node_value'></span><span id='node_topic'></span></li>");
          }

         var i=0;
         var k=0;
         if(j == 0){
         $(this).parent().parent().find(".td_li").each(function () {

            if (!$(this).attr('title')) {
               i++;
               $(this).remove();
            }

            if ($(this).parent().parent().attr('id') == cell_id) {
                k++;
                setParentTopic(cell_id);
            }
         });

         if(i==0 && k==0){
           // $('ul', this).append("<li class='td_li'><span id='temp' style='height: 15px; width: 13px; background-color: gainsboro; display: inline-block'><input type='hidden' id='node_no' class='list_hidden_node_value'></span><span id='node_topic'></span></li>");
           $('ul', this).append("<li class='td_li'><span id='temp' ondrop='drop(event)' ondragover='allowDrop(event)' style='height: 15px; width: 13px; background-color: gainsboro; display: inline-block'><input type='hidden' id='node_no' class='list_hidden_node_value'></span><span id='node_topic'></span></li>");

         }
        }
  });

////////////////////////////////////// **** for Creating LT Structure **** \\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//calling function for structure
// calling lt configuration update

  function createStructureOnUpdate(){
       //
       $(".loader").fadeIn();

      $('#parent_topic_name').val('');
      var containerLearningTree=$('#hidden_learning_tree_id_id').val();
      var url="/getParentLtUpdateStructure/"+containerLearningTree;

    axios.get(url)
        .then(function (response) {

             server_data=response['data'];
             console.log(server_data);

             // setting facilitator
             facilitator_object=server_data['facilitator'];

             // setiing moderators object
             moderator_object=server_data['moderator'];


             //calling function for structure
             createStructure(server_data['object'],1);


        }).catch(function (error) {
             console.log("error "+error);
        });
  }


  function createStructure(server_data,tab){
        //
             // creating tabs
             createTabs(server_data['tabs_object']);
             
             // setting object value
             cell_object=server_data['first_tab_object'];
             
             // setting parent box
             if(Object.keys(cell_object).length !=0){
              console.log('tab not zero');
             $('#parent_topic_name').val(server_data['tabs_object'][tab]['topic_name']);
             $('#parent_topic_id_id').val(server_data['tabs_object'][tab]['topic_id']);

             tabEventN('tab'+tab);
             }
             // else set the empty value

             //creating table for tab
             $('#table_body').find('tr').remove();
             createTableForParentTopic();
  }


  function createTabs(tabs_object){
       
         $('.tab').empty();

         $.each(tabs_object, function (tab, tab_data) {
            console.log(tab);
            appendTabsOneByOne(tab, tab_data['topic_id'], tab_data['topic_name']); 
         });

         $('.tab').append("<button id='plus' style='width: 30px;' onclick='increaseButton()'>+</button>");
  }

  function appendTabsOneByOne(i, id, name){
         
         tab_id="tab"+i; 
         // appending tab
         $('.tab').append("<button class='tab_button' id="+tab_id+" onclick=tabEvent('"+tab_id+"')>"+i+"<input type=hidden id="+name+" value="+id+"></button>");
  }



  // tab delete
    $('#delete_btn').click(function(){

       var tab=$('#hidden_tab_value').val();
       var containerLearningTree=$('#hidden_learning_tree_id_id').val();

    if (!$('#selected_list').attr('id')) {
        
        console.log('its tab');
        
        var id= $('#tab'+tab).find('input').val();
        var name= $('#tab'+tab).find('input').attr('id');

        var r=confirm('Are you sure you want to delete Topic'+ name+'(tab'+tab+')');
        if (r == false) {
           return false;
        } 

        deleteNodesFromDataBase(id,containerLearningTree,tab,'tab');
    }
    else{

        var cell_id = $('#selected_list').parent().parent().attr('id');
        var cells_id_array=cell_id.split('d');
        cell_no=cells_id_array[1];

      if (cell_object[cell_no]['topic_id']) {  
        var id=cell_object[cell_no]['topic_id'];
        var name=cell_object[cell_no]['topic_name'];

        console.log('its node');
        var r=confirm('Are you sure you want to delete Topic '+ name);
        if (r == false) {
           return false;
        } 

        var id=$('#selected_list').find('.list_hidden_node_value').val();
        // second way is to get by cell no and cell object
        var parent_id=$('#parent_topic_id_id').val()

        deleteNodesFromDataBase(id,parent_id,0,'node');
      }
      else{
        $('#selected_list').remove();  
      }

    }

  })

  function deleteNodesFromDataBase(id,containerLearningTree,t,flag){
      
      $(".loader").fadeIn();

      console.log('in deleteNodesFromDataBase function');
      console.log(id+" "+flag);

      var tab=$('#hidden_tab_value').val();

      // proper tab
      var last_tab_id=$('.tab').find(".tab_button:last").attr('id');

      if(last_tab_id=='tab'+tab && tab!=1){
         tab=parseInt(tab)-1;
      }

      if (flag == 'node') {
         tab=t;
       }

      var url="/deleteProgramTopic/"+id+"/"+containerLearningTree+"/"+tab;

    axios.get(url)
        .then(function (response) {

             var server_data=response['data'];

             if (flag =='tab') {
                //
                console.log(server_data); 

                createStructure(server_data,tab);

             }
             else if (flag =='node') {
                //
                console.log(server_data); 
                
                // setting new obje
                cell_object=server_data;

                // creating table
                $('#table_body').find('tr').remove();
                createTableForParentTopic();

             }

             }).catch(function (error) {
             console.log("error"+error);
          });
   }    

  // tab edit

  // node edit

  // edit

   $('#edit_btn').click(function(){

          var tab=$('#hidden_tab_value').val();
          var containerLearningTree=$('#hidden_learning_tree_id_id').val();

        if (!$('#selected_list').attr('id')) {
           
           resetModal();
           $('#modal_heading').html('Edit Topic');

           console.log('its tab');
           var id= $('#tab'+tab).find('input').val();
           var name= $('#tab'+tab).find('input').attr('id');

           getTopicDataForEdit(id,'tab');

        }

        else{
          // var id=$('#selected_list').find('.list_hidden_node_value').val();
           // another method
          var cell_id = $('#selected_list').parent().parent().attr('id');
          var cells_id_array=cell_id.split('d');
          cell_no=cells_id_array[1];
          var id=cell_object[cell_no]['topic_id'];


          resetModal();
          $('#modal_heading').html('Edit Topic');

          var icon_class = $('#selected_list').find('i').attr('class');
          console.log(icon_class);
          saveEntityName(icon_class);
          if (icon_class == 'vnn-vc') {
            $('#vc_participant_link').show();
          }
          
          // setting selected list as current_list for saving data
          $('#selected_list').find('span').first().attr('id','current_list');
          getTopicDataForEdit(id,'node');

        }
    });


 function getTopicDataForEdit(id,flag){

            //
            console.log('in getTopicDataForEdit function');
            var url="/getProgramTopicForEdit/"+id;

    axios.get(url)
        .then(function (response) {

             data=response['data'];
             console.log(data);
             // return data;

             if (flag=='tab') {
               editTopicForTab(data,id);
             }
             else if (flag == 'node'){
              editTopic(data,id);
             }

             }).catch(function (error) {
             console.log("error "+error);
          });
   }


   function editTopic(data,id){

          var tab=$('#hidden_tab_value').val();

          var topic_info_array=data;
          console.log(topic_info_array);
          console.log($('#selected_list').attr('id'));
         
      /// setting values  
          $('#topic_name').val(topic_info_array['name']);
          $('#topic_description').val(topic_info_array['description']);
          $('#expected_duration').val(topic_info_array['expected_duration']);
          $('#topics_to_complete').val(topic_info_array['topics_to_complete']);

          if(topic_info_array['critical'] == 1){
             $('#critical_topic').prop('checked',true);
             $('#alert_before_nodes').val(topic_info_array['alert_before_nodes']);
          }

        /// setting durations
          $('#program_lt_from_id').val(topic_info_array['available_from']);
          $('#program_lt_to_id').val(topic_info_array['available_to']);  
        
        /// status_color
          if (topic_info_array['status_color_allocation_method'] !=null) {
             $("[name=topic_status_color_allocation_method]").val([topic_info_array['status_color_allocation_method']])
          }

        
        /// setting document part
          if (topic_info_array['details'] !=null) {

             // node_icon_file
             if (topic_info_array['photo']!= null) {
               //
               $('#node_icon_file').html('(Uploaded)');
             }

             // setting optional
             if(topic_info_array['optional'] == 1){
                $('#is_optional').prop('checked',true);
             }
             // getting JSON
             var details=JSON.parse(topic_info_array['details']);
             console.log(details);

             if (details['entity_type'] == 'Virtual Classroom') {
               //
               appendModerator();
               setModerator(details['moderators']);

               appendFacilitator();
               setFacilitator(details['facilitators']);

               //
               $('#moderator_msg_id').val(topic_info_array['moderator_default_message']);
               $('#welcome_msg_id').val(topic_info_array['welcome_message']);
             }
             else if (details['entity_type'] == 'Assignment'){
                //
               var action=details['activityAction'];
               console.log(action);
               if (action=='links') {
                   var files=details['files'];
                   console.log(files);
                   
                   if (files['name']) {
                     var name=files['name'].split('.').join('');
                     $('#file_list').append('<tr><input type=hidden name='+name+' value='+files['path']+'><td><label>&nbsp;&nbsp;&nbsp;'+files['name']+'</label></td></tr>');
                   }
                   $("[name=activityAction]").val(["links"]);
                   // hide and remove mandatory
                   $('#file_wrapper').show();
                   $('#submit_type_wrapper').hide();
                   $('#document_file_wrapper').hide();
               }
               else if (action=='file'){
                   $("[name=activityAction]").val(["file"]);

                   if (details['submitType']) {
                       var submitType=details['submitType']
                       if (submitType == 'submit') {
                           $("[name=submitType]").val(["submit"]);
                       }
                       else{
                           $("[name=submitType]").val(["save"]);
                       }
                   }
                   $('#file_wrapper').hide();
                   $('#submit_type_wrapper').show();
                   $('#document_file_wrapper').hide();
               }
               else if (action=='editDoc'){
                   var files=details['files'];
                   console.log(files);
                   
                   if (files['name']) {
                     var name=files['name'].split('.').join('');
                     $('#doc_list').append('<tr><input type=hidden name='+name+' value='+files['path']+'><td><label>&nbsp;&nbsp;&nbsp;'+files['name']+'</label></td></tr>');
                   }
                   $("[name=activityAction]").val(["editDoc"]);
                   // hide and remove mandatory
                   $('#file_wrapper').hide();
                   $('#submit_type_wrapper').hide();
                   $('#document_file_wrapper').show();
               } 
               // var files=details['files'];
               // console.log(files);
               // // $('#file_list tr').remove();

               // $.each(files,function(key,value){
               //      //
               //      var name=value['name'].split('.').join('');;
               //      // $('#file_list').append('<li><label>'+value['name']+'</label><a href="#" style="float: right;"><i class="vnn-times-circle"></i></a></li>');
               //      $('#file_list').append('<tr><input type=hidden name='+name+' value='+value['path']+'><td><label>&nbsp;&nbsp;&nbsp;'+value['name']+'</label></td></tr>');
               //      // $('#file_list').append('<tr><input type=hidden name='+name+' value='+value['path']+'><td><label>&nbsp;&nbsp;&nbsp;'+value['name']+'</label><a href="#" style="float: right;"><i class="vnn-times-circle"></i>&nbsp;&nbsp;&nbsp;</a></td></tr>');
               // }) 
             }             
             else{
               // setting values
               if (details['entity_id']) {
               $('#hidden_document_name_id2').val(details['entity_name']);
               $('#hidden_document_url_id2').val(details['entity_url']);

               $('#hidden_document_duration_id').val(details['entity_duration']);
               $('#hidden_document_size_id').val(details['entity_size']);
               $('#hidden_document_description_id').val(details['entity_description']);

               $('#hidden_document_pages_id').val(details['entity_pages']);
               $('#hidden_document_title_id').val(details['entity_title']);
               $('#hidden_document_summary_id').val(details['entity_summary']);

               console.log(details['entity_id']);
               $("#document_path_option_id").append("<tr><td width=10% ><input type=checkbox class='document_checkbox_class' name='entity_id' value="+details['entity_id']+" checked></td><td width=90%>"+details['entity_name']+"</td></tr>");
               }
             }
          }

      /// setting update flag and id for update on server
          $('#hidden_flag').val('update');
          $('#hidden_topic_id').val(id);

        // for node and not tab  
          $('#hidden_parent_topic_id').val('a');
	  if(topic_info_array['photo_path']){
          $('#photo_path_id').val(topic_info_array['photo_path']);  
          }
          $("#myModal").modal({
          backdrop: 'static',
          keyboard: false
          });


      // // set current_list id
      //    $('#selected_list').find('span').first().attr('id','current_list');
   }

   function editTopicForTab(data,id){

          var topic_info_array=data;
          console.log(topic_info_array);

          // no entity type
          $('#hidden_entity_type_id').val('MajorNode');

          // hiding document part
          // $('#document_section').hide();
          // $('#lt_search_section').hide();
          // $('#lt_option_section').hide();
          // $('#moderator_section_id').hide();
          // $('#icon_div').show();
          showSectionForTab();


          // $('#icon_div').show();

          // node_icon_file
          if (topic_info_array['photo']!= null) {
             //
             $('#node_icon_file').html('(Uploaded)');
          }
      
       /// setting values
          $('#topic_name').val(topic_info_array['name']);
          $('#topic_description').val(topic_info_array['description']);
          $('#expected_duration').val(topic_info_array['expected_duration']);  
          $('#topics_to_complete').val(topic_info_array['topics_to_complete']);

          if(topic_info_array['critical'] == 1){
             $('#critical_topic').prop('checked',true);
             $('#alert_before_nodes').val(topic_info_array['alert_before_nodes'])
          }

        // setting durations
          $('#program_lt_from_id').val(topic_info_array['available_from']);
          $('#program_lt_to_id').val(topic_info_array['available_to']);    

        /// status_color
          if (topic_info_array['status_color_allocation_method'] !=null) {
             $("[name=topic_status_color_allocation_method]").val([topic_info_array['status_color_allocation_method']])
          }

       /// setting update flag and tab flag
          $('#hidden_parent_topic_id').val('');

          $('#hidden_flag').val('update');
          $('#hidden_topic_id').val(id);
	  if(topic_info_array['photo_path']){
          $('#photo_path_id').val(topic_info_array['photo_path']);  
          }
          $("#myModal").modal({
          backdrop: 'static',
          keyboard: false
          });
   }   

 // $(document).on('click','#file_list a',function(){
 //      //
 //      $(this).parent().parent().remove();
 // }) 

  // adding lt on tab

  // adding lt on node

  // do remaining by new style















function saveSequence(cell){
       //
       console.log(cell);

       // checking if this is first node
       var sequence;

       // finding previous and next  sequences
       var previous_sequence=0;
       var next_sequence=0;
       $.each(cell_object, function (cell_no, info) {
            
            // previous
            if (parseInt(cell_no) < parseInt(cell) && info != '') {
                previous_sequence = info['sequence'];
            }
            // next
            if (parseInt(cell_no) > parseInt(cell) && info != '') {
                next_sequence = info['sequence'];
                return false;
            }
       });       

       console.log(previous_sequence+" "+next_sequence);

       if (next_sequence==0 && previous_sequence==0) {
           //
           sequence=600;
           return sequence;
       }
       else{
           //
           if (next_sequence ==0) {
               //
               var i=parseInt(previous_sequence)/600;
               if (parseInt(previous_sequence)%600 !=0) {
                   sequence=600*(i+2);
                 }
                else{
                  sequence=600*(i+1);
                 }
                return sequence;
           }
           else{
               //
               sequence=parseInt((parseInt(previous_sequence)+parseInt(next_sequence))/2);
               return sequence;
           }
       }
 }


 ////////////////////////////////////// adding cell \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 
 var selected_cell_no;

 $('#cell_append_btn').click(function(){

      //
      var cell_id = $('#selected_list').parent().parent().attr('id');
      if (cell_id) {

           var cells_id_array=cell_id.split('d');
           selected_cell_no=cells_id_array[1];
      }
      else{
        selected_cell_no="";
      }
      console.log(selected_cell_no);
 })

  $("#cell_append_btn").popover({
     
      placement: 'top',
      html: 'true',
      trigger: "focus",
      content: '<div><button type="button" class=".btn btn-primary" id="left_button">left</button> <button type="button" class=".btn btn-primary" id="right_button">right</button></div>'
  });

   $(document).on('click','#left_button',function(){
      //
      console.log('in left_button');
      // // finding 
      var cell_no=selected_cell_no;

      // validation-- no validations
      if (cell_object[parseInt(cell_no)-1] == "") {
          alert('please choose right cell');
          return false;
      }
      else if (parseInt(cell_no)==max_cell_no) {
          alert('Cannot add more cells than '+max_cell_no);
          return false;
      }

      // but if its cell one then target cell will be one
      var target_cell;
      target_cell=parseInt(cell_no);

      addCell(target_cell);
  });  

  $(document).on('click','#right_button',function(){
      //validation

      var cell_no=selected_cell_no;

      //validtion
      if (cell_object[parseInt(cell_no)+1] == "") {
          alert('please choose right cell');
          return false;
      }
      else if (parseInt(cell_no)==max_cell_no) {
          alert('Cannot add more cells than '+max_cell_no);
          return false;
      }

      // validation if it is last cell of the table

      var target_cell=parseInt(cell_no) + 1;

      addCell(target_cell); 
  });

  function addCell(target_cell){

      //
      if (isNaN(target_cell)) {
        return false;
      }
      var new_object=new Object();
      // var new_object2=getStructureObject()
      // console.log(new_object);
      new_object[target_cell]="";
      var last_cell;

      $.each(cell_object, function (cell_no, topic_info) {
             //
             
             if (cell_no >= target_cell && topic_info !="") {
                 //
                 console.log(cell_no);
                 new_object[parseInt(cell_no)+1]=topic_info;
                 
             }
             else if (cell_no <target_cell) {
                 //
                 new_object[cell_no]=topic_info;
             }
             else{
              return false;
             }
             last_cell=parseInt(cell_no)+1;
      });
      // check if more than 15 cells validations
      console.log(last_cell);
      if (last_cell > max_cell_no) {
          alert('Cannot add more cells than '+max_cell_no);
          return false;
      }
      // check if length is less than 20
      if (last_cell <= max_cell_no) {
       for(var i=last_cell+1; i<=max_cell_no; i++)
       {
          //
          new_object[i]=cell_object[i];
       }
      }

      // checking if greater than 10 and last cell is not multiple of 5
      
      console.log(new_object);
      // calling the restructure function
      cell_object=new_object;
      $('#table_body').find('tr').remove();
      createTableForParentTopic();
  }

  ////////////////////////////////////////// Svg rendering part  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  $(document).on('click','#svg_btn',function(){
      //
      // validation -- if selected parent is not yet has child -- give msg cause no result
      var svg_div_width=parseInt($("#svg_div").width() / $("#svg_div").parent().width() * 100);
      
      if(svg_div_width!=20){
          

          getSvg('topic');
       }
       else{
        $("#svg_div").animate({width: '0%'});
        $("#svg_btn").animate({right: '0%'});

        $("#svg_btn").html('&laquo;');
        $('#svg_parent').css('width','2.5%');
        $('#svg_btn').css('width','100%');
       }    
            
      
  }); 

  function getSvg(flag){

      //
      var containerLearningTree=$('#hidden_learning_tree_id_id').val();
      if (flag == 'topic') {
          var parent_topic_id=$('#parent_topic_id_id').val();
          var parent_topic_name=$('#parent_topic_name').val();
      }
      else if (flag == 'lt') {
          var parent_topic_id=0; 
          var parent_topic_name=$('#hidden_learning_tree_name_id').val(); 
      }
      // call js
      var url='/getSvgForProgram/'+containerLearningTree+'/'+parent_topic_id+'/'+parent_topic_name;
      axios.get(url)
        .then(function (response) {

             data=response['data'];
             // console.log(data);
             // return data;
             $('#svg_parent').css('width','100%');
             $('#svg_btn').css('width','2.5%');

             $('#svg_div').html(data);
             // $('#prime').html(data);

             $("#svg_div").animate({width: '20.5%'});
             $("#svg_btn").animate({right: '20.5%'});

             $("#svg_btn").html('&raquo;');


             }).catch(function (error) {
             console.log("error "+error);
          });
  } 



  //////////////////////////// parent event \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  function setParentTopic(cell_id){
      //
      // check for leaf node
      var cells_id_array=cell_id.split('d');

      if (cell_object[cells_id_array[1]]['entity_type'] != 'MajorNode') {
          //
          return false;
      }

      var parent_name=cell_object[cells_id_array[1]]['topic_name'];
      var parent_id=cell_object[cells_id_array[1]]['topic_id'];
      console.log(parent_name+" "+parent_id);

      // getting structure from the database and rebuild table
      getParentStructure(parent_id,parent_name);

  }

  ////////// learning tree as svg \\\\\\\\\\\
  $('#preview_btn').click(function(){
     
     //
      var svg_div_width=parseInt($("#svg_div").width() / $("#svg_div").parent().width() * 100);
      
      if(svg_div_width!=20){
          
          getSvg('lt');
       }
       else{
        // extra validation
        return false;
       }
  });


///////////////////////// moderator  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\  
function appendModerator(){

       $('#moderator_id').empty();
       $.each(moderator_object, function (key, value) {
          console.log(value.id);
          $("#moderator_id").append("<option value="+value.id+">"+value.first_name+" "+value.last_name+"</option>");
       });
}

function setModerator(moderators){
        //
        $("#moderator_id").val(moderators).select2();
}

function appendFacilitator(){

       $('#facilitator_id').empty();
       $.each(facilitator_object, function (key, value) {
          console.log(value.id);
          $("#facilitator_id").append("<option value="+value.id+">"+value.first_name+" "+value.last_name+"</option>");
       });
}

function setFacilitator (facilitators){
        //
        $("#facilitator_id").val(facilitators).select2();
}

 function validateModal(){
     //
     var valid=true;
     // for learning tree
     if ($('#lt_search_section').is(":visible")) {
         if ($('#learning_tree_id').val() == 0 || $('#learning_tree_id').val() ==null ) {
             valid=false;
             console.log(valid);
         }
     }
     // for moderator
     if ($('#moderator_section_id').is(":visible")) {
         // if (!Array.isArray($('#moderator_id').val()) ) {
         if ($('#moderator_id').val() == null ) {
             valid=false;
             console.log(valid);
         }

         if ($('#facilitator_id').val() == null ) {
             valid=false;
             console.log(valid);
         }
     }

     // for documents
     if ($('#document_section').is(":visible")) {
         // if (!Array.isArray($('#moderator_id').val()) ) {
         var a=0;
        if($('.document_checkbox_class').val()){
          console.log('hi');
          $('.document_checkbox_class').each(function(index, el) {
            console.log('pii');
            if ($(this).is(":checked")) {
                a=1;
                console.log('hii');
            }
           });
         // a=1;
        }
        if (a ==1) {
          valid= true;
        }
        else{
        valid=false;
        $('#document_input_id').addClass('fillFields');
        }
     }
     // for activity
    if ($('#assignment_section').is(":visible")) {
        // for radion button select
        if(!$('input[name=activityAction]:checked').val()){
             valid=false;
         }

        // for instruction upload
        if ($('#file_wrapper').is(":visible") && !$('#file_list').html()) {
        //  
            if ($('#assignment_file_id').val() === '' || $('#assignment_file_id').val() === '0') {
                valid=false;
            }
        }
        if ($('#document_file_wrapper').is(":visible") && !$('#doc_list').html()) {
        //  
            if ($('#assignment_doc_id').val() === '' || $('#assignment_doc_id').val() === '0') {
                valid=false;
            }
        }
        
        // for submit type
        if ($('#submit_type_wrapper').is(":visible") && !$('input[name=submitType]:checked').val()) {
        //  
            valid=false;
        }

     }

     return valid;
 }

 /////////////////////////////////// Save as Learing Tree Started \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 
 $(document).on('click','#lt_export_id',function(){
    //
    // resetting modal
    $('#export_program_form_id').find(':input').val("");
    $('#export_program_form_id').find(':input[type="checkbox"]').attr('checked',false);
     
    $("#lt_export_modal").modal({
       backdrop: 'static',
       keyboard: false
     });
 })

  // $(document).on('click','#export_lt_save_btn',function(){
  $('#export_lt_save_btn').click(function(){
    //
    
    var data = new FormData(document.getElementById("export_program_form_id"));  
    var prograId=$('#hidden_learning_tree_id_id').val();
    data.append('prograId', prograId);
   
    console.log(data);
    if (!validateMandatoryFields('export_program_form_id')) {
        //alert('Please fill all the mandatory fields');
        bootbox.alert('Please fill all the mandatory fields');
        return false;
    }

    var url='/saveProgramAsLt';

    axios({
        method: 'post',
        url: url,
        data: data,

       }).then(function(response) {
        
        var server_data=response['data'];
        console.log(server_data);
        //alert('Program saved as learning tree successfully');
        bootbox.alert('Program saved as learning tree successfully');

      }).catch(function (error) {
      console.log("error"+error);
     });
 })

 
 /////////////////////////////////// Save as Learing Tree Ended \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\



 //
 $(document).on('change','#critical_topic',function(){
if(this.checked == true){
$('#alert_before_nodes').addClass('mandatory');
}else{
$('#alert_before_nodes').removeClass('mandatory');
$('#alert_before_nodes').removeClass('fillFields');
}

});


//// for activity node
  $('input[type=radio][name=activityAction]').change(function() {
       //
       console.log('test');
       if ($('input[name=activityAction]:checked').val() == 'links') {
           //
           $('#file_wrapper').show();
           $('#submit_type_wrapper').hide();
           $('#document_file_wrapper').hide();
       }else if ($('input[name=activityAction]:checked').val() == 'file'){
           $('#file_wrapper').hide();
           $('#submit_type_wrapper').show();
           $('#document_file_wrapper').hide();
       }  
       else {
           // changes in file typ
           $('#file_wrapper').hide();
           $('#submit_type_wrapper').hide();
           $('#document_file_wrapper').show();
       }      
  }); 