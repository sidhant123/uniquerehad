<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;


class BoqItem extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		// 'boq_id' => 'int',
		// 'service_id' => 'int',
		// 'activity_id' => 'int',
		// 'created_by' => 'int',
		// 'updated_by' => 'int',
		// 'deleted_by' => 'int'
	];

	// protected $fillable = [
	// 	'name',
	// 	'desription',
	// 	'boq_id',
	// 	'rate',
	// 	'quantity',
	// 	'amount',
	// 	'service_id',
	// 	'activity_id',
	// 	'created_by',
	// 	'updated_by',
	// 	'deleted_by'
	// ];

// public function boq(){
//     return $this->belongsTo('App\Boq');
// }
}
