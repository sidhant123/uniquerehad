<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 20 Mar 2019 15:24:35 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PasswordLogic
 * 
 * @property int $id
 * @property string $password_logic
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class PasswordLogic extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'password_logic',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
