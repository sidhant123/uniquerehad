<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class BoqReportService {
	
	  public static function retrieveBoqReportQuery($project_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $queryBuilder = DB::table('boqs')
            ->select(
                'projects.name as project_name' , 
                'boqs.name as boq_no' ,  
                'boqs.description' ,  
                'boqs.unit_rate', 'boqs.rate', 'boqs.quantity', 'boqs.amount', 'boqs.status' 
               
        )
            ->leftJoin('projects', 'projects.id', '=', 'boqs.project_id')
            ->where('boqs.project_id','=', $project_id)
            ->orderBy('boqs.parent_id', 'asc')->orderBy('boqs.id','asc');

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

    public static function getProjectName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $project = Project::select('name')->where('id', '=', $Id)->first();
        return trim($project->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }


// raw query for retrieveBoqReportDetailQuery

// select projects.name, boqs.name, dprs.created_date, users.first_name,users.last_name,
// dprs.length, dprs.width, dprs.height, dprs.depth, dprs.quantity

//  from dprs 
// left join projects on dprs.project_id = projects.id
// left join boqs on boqs.id = dprs.boq_id
// left join users on users.id = dprs.created_by
// where projects.id = 70 and
//  dprs.created_date >= '2020-07-01'
//   AND dprs.created_date <  '2020-07-30'
//   and dprs.is_active = 'Y'
//   order by dprs.created_date


 public static function retrieveBoqReportDetailQuery($project_id,$start_date,$end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';
        $queryBuilder = DB::table('dprs')
            ->selectRaw(
             ' boqs.name, boqs.description, date(dprs.created_date) as "xcreated_date" ,
                SUM(dprs.length) as length,SUM(dprs.width) as width,SUM(dprs.depth) as depth,
              SUM(dprs.quantity) as quantity, SUM(dprs.total) as total' )
            ->leftJoin('projects', 'projects.id', '=', 'dprs.project_id')
            ->leftJoin('boqs', 'boqs.id', '=', 'dprs.boq_id')
            ->where('boqs.project_id','=', $project_id)
            ->where('dprs.is_active','=', 'Y')
            ->whereBetween('dprs.created_date', [$start_date, $end_date])
            ->groupBy('boqs.name','boqs.description', 'xcreated_date')
           ->orderBy('xcreated_date', 'asc')->orderBy('boqs.name','asc');
           // dd($queryBuilder);
        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

 public static function retrieveBoqSummaryReportQuery($project_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $queryBuilder = DB::table('dprs')
            ->selectRaw(
              "boqs.name, boqs.description, boqs.quantity as actual_qty,
               SUM(dprs.length) as length,SUM(dprs.width) as width, SUM(dprs.depth) as depth,
              SUM(dprs.quantity) as quantity, SUM(dprs.total) as total
              ")
            ->leftJoin('projects', 'projects.id', '=', 'dprs.project_id')
            ->leftJoin('boqs', 'boqs.id', '=', 'dprs.boq_id')
            ->where('boqs.project_id','=', $project_id)
            ->where('dprs.is_active','=', 'Y')
            ->groupBy('boqs.name','boqs.description','boqs.quantity')
            ->orderBy('boqs.name','asc');
        Log::debug('****QUERY*****');
    //    dd($queryBuilder);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }




}
