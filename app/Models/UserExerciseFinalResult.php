<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 09 Aug 2018 13:11:21 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserExerciseFinalResult
 * 
 * @property int $id
 * @property int $program_id
 * @property int $user_id
 * @property int $parent_topic_id
 * @property int $topic_id
 * @property int $customer_exercise_bank_id
 * @property int $exercise_bank_id
 * @property string $exercise_name
 * @property int $total_questions
 * @property int $total_pages
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property array $question_answers
 * @property int $total_question_not_considered
 * @property int $total_correct_answers
 * @property int $total_incorrect_answers
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class UserExerciseFinalResult extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'user_id' => 'int',
		'parent_topic_id' => 'int',
		'topic_id' => 'int',
		'customer_exercise_bank_id' => 'int',
		'exercise_bank_id' => 'int',
		'total_questions' => 'int',
		'total_pages' => 'int',
		'tenant_party_id' => 'int',
		'customer_party_id' => 'int',
		'question_answers' => 'json',
		'total_question_not_considered' => 'int',
		'total_correct_answers' => 'int',
		'total_incorrect_answers' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'user_id',
		'parent_topic_id',
		'topic_id',
		'customer_exercise_bank_id',
		'exercise_bank_id',
		'exercise_name',
		'total_questions',
		'total_pages',
		'tenant_party_id',
		'customer_party_id',
		'question_answers',
		'total_question_not_considered',
		'total_correct_answers',
		'total_incorrect_answers',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
