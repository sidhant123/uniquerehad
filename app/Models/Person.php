<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Sofa\Eloquence\Eloquence;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class Person
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $gender
 * @property string $employee_id
 * @property string $department
 * @property string $designation
 * @property string $linkedin_profile
 * @property string $facebook_id
 * @property string $twitter_handle
 * @property string $photo
 * @property int $party_id
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Person extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['first_name','last_name', 'email'];
	protected $casts = [
		'party_id' => 'int',
		'tenant_party_id' => 'int',
		'customer_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'personal_email',
		'employee_id',
		'department',
		'designation',
		'linkedin_profile',
		'facebook_id',
		'twitter_handle',
		'google_profile',
		'photo',
		'party_id',
		'tenant_party_id',
		'customer_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	//Determine the party of the person (i.e. self)
	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class);
	}

	//For a person - determine which tenant the person belongs to
	public function tenant_party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}

	//For a person - determine which customer the person belongs to
	public function customer_party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'customer_party_id');
	}	

	//For a person - determine which organization the person belongs to
	public function organization()
	{
		return $this->belongsTo(\App\Models\Organization::class, 'customer_party_id', 'party_id');
	}
	
		
}
