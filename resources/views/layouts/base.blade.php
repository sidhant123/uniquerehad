<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>UniqueRehab Back Office</title>

        <!-- Commented only for a single JS to load -->
        <script src="{{ asset('/js/jquery-2.2.4.js') }}"></script>
	
        <!-- Following files are for the favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/img/company_logo.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/company_logo.png') }}">
	<link rel="manifest" href="{{ asset('/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">



        <!-- Commented only for a single CSS to load -->
         <link href="{{ asset('/css/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/vnnogile.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/style4.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/jquery-ui-dateTimepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/vnno.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <!-- <link href="/css/site.min.css" rel="stylesheet"> -->

    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="loader"></div>
            </div>
        </div>
      </div>
        <div class="wrapper" >


          @include('menu.menu', ['some' => @menuItems, 'some' => @authenticatedUser])
          <div id="content" class="col-xs-12">

              <div id="main_layout_content" >
                    <!-- <div id="openbars" class="col-xs-1">
                      <span onclick="openNav()"><i class="fa fa-bars"></i></span>
                    </div> -->
                    <nav id="navbar-div" class="navbar navbar-default">
                      <div class="container-fluid">
                        <div class="navbar-header">
                          <a class="navbar-brand" href="#">
                            <img alt="Brand" src="{{ asset('/img/company_logo.png') }}" width="100" height="100"  style="padding: 0px;">
                          </a>
                        </div>
                        <div class="collapse navbar-collapse">
                          <div class="urb_header_right" style="margin-top: 30px">
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                            @else
                              <li>
                                <a href="/companies" aria-expanded="false" aria-haspopup="true">
                                  <i class="vnn vnn-home" aria-hidden="true"></i>
                                </a>
                              </li>
                               {{-- <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <i class="vnn vnn-bell" aria-hidden="true">
                                    </i>
                                  </a>
                                </li> --}}
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                        <i class="vnn vnn-user" data-toggle="tooltip" aria-hidden="true" title="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}"></i><span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li>

                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                <i class="vnn vnn-signout"></i> Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                        <li>
                                      <i class="vnn vnn-user">                                        
                                      </i> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                                      </li>
                                    </ul>
                                </li>
                            @endguest
                            </ul>
                            <form class="navbar-form navbar-right" action="/{{ Request::path() }}" role="search" method="get">
                              <div class="form-group">
                                @if(request()->has('search'))
                                <input type="text" value="{{ request('search') }}" name="search" class="form-control" placeholder="Search">
                                @else
                                <input type="text" name="search" class="form-control" placeholder="Search">
                                @endif
                                <!--<input type="hidden" value="1" name="fromSearch"> -->
                                <button type="submit" class="btn btn-primary"><span class="vnn vnn-search"></span></button>
                              </div>
                            </form>
                          </div>
<!--                             <form class="navbar-form navbaruser-right" role="search">
  <div class="form-group search">
    <span class="glyphicon glyphicon-search"></span>
    <input type="text" class="form-control" placeholder="Search">
  </div>
    <button type="submit" class="btn btn-default">Go</button>
</form> -->
                        </div>
                      </div>
                    </nav>
                  <div class='row'>
                      <div class="col-xs-12">
                            @yield('filterContent')
                            @yield('content')
                            @yield('tagContent')
                      </div>
                  </div>

              </div>

          </div>

        </div>

        @yield('css')
        <!-- Commented only for a single JS to load -->
        <script src="{{ asset('/js/axios.min.js') }}"></script>
        <script src="{{ asset('/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.js') }}"></script>
        <script src="{{ asset('/js/bootbox.min.js') }}"></script>
        <script src="{{ asset('/js/select2.min.js') }}"></script>
        <script src="{{ asset('/js/cgx.js') }}"></script>
        <script src="{{ asset('/js/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('/js/colResizable-1.6.min.js') }}"></script>
        <script src="{{ asset('/js/moment.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script>

        {{-- <script src="/js/bundle.min.js"></script> --}}
        </div>
    </body>
    <head>
    <!-- Head Contents -->

    @stack('scripts')

    <script>
        $(function() {
            $(document).ready(function () {
                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                });
            });
        });
    </script>
</head>
</html>
