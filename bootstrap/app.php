<?php

$cool = require __DIR__ . './../config/cool.php';
$crypt = new \Illuminate\Encryption\Encrypter($cool['stuff']);
$secEnv = [];

function secEnv($name, $fallback = '')
{
    global $crypt, $secEnv;

    if ( ! isset($secEnv[$name]) && env($name) && strpos(env($name), "ENC:") === 0)
    { 
        $secEnv[$name] = $crypt->decrypt(substr(env($name), 4));
    }

    return isset($secEnv[$name]) ? $secEnv[$name] : env($name, $fallback);
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->configureMonologUsing(function ($monolog) use ($app) {
  //  $monolog->pushHandler(new Monolog\Handler\StreamHandler($app->storagePath() . '/logs/bo_debug.log', Monolog\Logger::DEBUG, false));
 //   $monolog->pushHandler(new Monolog\Handler\StreamHandler($app->storagePath() . '/logs/bo_info.log', Monolog\Logger::INFO, false));
    $monolog->pushHandler(new Monolog\Handler\StreamHandler($app->storagePath() . '/logs/bo_error.log', Monolog\Logger::NOTICE, false));    

    return $monolog;
});

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
