<?php

namespace vnnogile\Mailer\Providers;

use Illuminate\Support\ServiceProvider;

class EmailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->publishes([
        __DIR__.'/../ui' => public_path('vendor/vnnogile'),
    ], 'public');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
        $this->app->make('vnnogile\mailer\controllers\MailController');
        $this->app->make('vnnogile\mailer\controllers\EmailAddressController');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../jobs/EmailJob.php';
        include __DIR__.'/../jobs/EmailSuccessJob.php';        
        include __DIR__.'/../utility/EmailUtility.php';
        include __DIR__.'/../controllers/MailController.php';
        include __DIR__.'/../controllers/EmailAddressController.php';
        
    }
}
