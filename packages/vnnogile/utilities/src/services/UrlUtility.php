<?php

namespace vnnogile\Utilities\Services;

//Custom Imports Below
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * This is a a common utility method for URL handling
 *
 */
class UrlUtility
{
    /**
     *
     * @param Request $request
     * @param string $folder
     *
     * @return boolean True or False
     */
    public static function checkIfUrlExists($url){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $isValid = "False";

        try{
            $status = (get_headers($url))[0];
            if(strpos($status,"200") !== false) {
                $isValid = "True";
            }
        }catch(\Exception $exception){
            Log::error($exception);
        }catch(\Throwable $exception){
            Log::error($exception);
        }
       
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        return $isValid;

    }



}
