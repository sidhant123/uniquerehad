<?php

namespace App\Http\Controllers;

use App\Models\ArtefactType;
use App\Models\ExerciseType;
use App\Models\LearningTree;
use App\Models\Configuration;
use App\Services\LearningTreeService;
use App\Services\LearningTreeSvgCoordinatorService;
use App\Services\ValidationRulesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Session;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;
use vnnogile\Tags\services\TagService;

class LearningTreeController extends BaseController {
	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		$this->middleware('CheckCompositeMiddleWare', ['except' => ['index', 'update']]);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function index(Request $request, $flag = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		//$this->authorize('List Companies');
		// $this->authorize('List Companies', 'List Companies');
	        //parent::populateOrClearSearch($request);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		//return PaginateController::paginate('LearningTree', 'learningTrees', parent::$utilsInterface, parent::$masterDataInterface);
		return PaginateController::paginate('LearningTree', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
	}

	public function create() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// $this->authorize('Create Companies', 'Create Companies');

		//  if($flag){
		//     $programData['program.create_program'] = array('organization'=>$organization,'isTaggale' => $request['isTaggable'],'badgeInfo'=>$badgeInfo);
		// return $programData;
		// }else{
		//     return view('learning_tree.create_learning_trees');
		// }

		$learningTree = LearningTree::all();

		$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
		$max_cell_no = $config['config_value'];

		return view('learning_tree.create_learning_tree', compact('learningTree', 'max_cell_no'));
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function store(Request $request) {
		//
	}

	public function show($id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {

			$data = LearningTreeService::getLearningTreeData($id);
			$learningTree = LearningTree::all();
			$check = LearningTree::where([['learning_tree_code', '=', $data['learning_tree_code']],
				['version', '=', $data['version'] + 1]])->first();
			if (isset($check)) {
				Session::flash('status', 'Old Version of Learning Tree Cannot Be Edited');
				return redirect()->action('LearningTreeController@index');
			}

			$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
			$max_cell_no = $config['config_value'];

			return view('learning_tree.edit_learning_tree', compact('data', 'learningTree', 'max_cell_no'));
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

	}

		public function edit($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// $this->authorize('Show Users', 'Show Users');
		try {

			//checking if next version of same learning tree exist
			$data = LearningTreeService::getLearningTreeData($id);
			$learningTree = LearningTree::all();
			$check = LearningTree::where([['learning_tree_code', '=', $data['learning_tree_code']],
				['version', '=', $data['version'] + 1]])->first();
			if (isset($check)) {
				Session::flash('status', 'Old Version of Learning Tree Cannot Be Edited');
				return redirect()->action('LearningTreeController@index');
			}

			// if($data['usage_count']!=0){

			//    $data['utilsInterface'] = parent::$utilsInterface;
			//    $data['masterDataInterface'] = parent::$masterDataInterface;
			//    $data['authenticatedUser'] = \Auth::user();
			//    $new_version_lt_id=LearningTreeService::createNewVersionLearningTree($data);

			//    $data = LearningTreeService::getLearningTreeData($new_version_lt_id);
			//    return view('learning_tree.edit_learning_trees', compact('data'));
			// }

			//
			$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
			$max_cell_no = $config['config_value'];

			return view('learning_tree.edit_learning_tree', compact('data', 'learningTree', 'max_cell_no'));
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

	}

	public function update(Request $request, $id) {

	}

	public function destroy($id) {
		//
	}

public function createLearningTree(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {

			$data = $request->all();

			// Log::debug($data);
			// exit();

			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			if ($data['learning_tree_hidden_id'] != "tesseract") {
				$rules = $valdationRulesService->validationRulesWithTenant('LearningTree', '$learning_tree', $data['learning_tree_hidden_id']);
			} else {
				$rules = $valdationRulesService->validationRulesWithTenant('LearningTree');
			}
			$request->validate($rules);

			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();

			$lt = LearningTreeService::saveLearningTree($data);

			Log::debug($lt);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $lt;

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}
	}

	
	public function getTopicDocument($tag, $artefact_name) {

		Log::debug($artefact_name);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$artefactType = $this->getArtefactTypeAndModel($artefact_name);

		// getting artefact type_id
		if ($artefactType['model'] == 'Document') {
			//
			$artefactType1 = ArtefactType::where('name', $artefactType['artefact_type'])->first();
			$artefact_type_id = $artefactType1['id'];
		} else if ($artefactType['model'] == 'ExerciseBank') {
			//
			$artefactType1 = ExerciseType::where('name', $artefact_name)->first();
			$artefact_type_id = $artefactType1['id'];
		}

		Log::debug($tag . ' ' . $artefact_type_id . ' ' . $artefactType['model'] . ' ' . $artefactType['sorting_column']);
		// $documents = TagService::searchTagsForDocuments($tag, $artefactType['model'], $artefactType['sorting_column'], $artefact_type_id);
		// }
		$documents = TagService::searchTagsForDocuments($tag, $artefactType['model'], $artefactType['sorting_column'], $artefact_type_id, $artefact_name);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		Log::debug($documents);

		return $documents;
	}


	public function getArtefactTypeAndModel($artefact_type_id) {
		$model;
		$sorting_column;
		$artefact_type;
		if ($artefact_type_id == 'Assessment') {
			$model = 'ExerciseBank';
			$artefact_type = 'Assessment';
			$sorting_column = 'exercise_types_id';
		} elseif ($artefact_type_id == 'Quiz') {
			$model = 'ExerciseBank';
			$artefact_type = 'Quiz';
			$sorting_column = 'exercise_types_id';
		} elseif ($artefact_type_id == 'Video') {
			$model = 'Document';
			$artefact_type = 'Video';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Audio') {
			$model = 'Document';
			$artefact_type = 'Audio';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Pdf') {
			$model = 'Document';
			$artefact_type = 'Document';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Word') {
			$model = 'Document';
			$artefact_type = 'Document';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Ppt') {
			$model = 'Document';
			$artefact_type = 'Document';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Youtube') {
			$model = 'Document';
			$artefact_type = '';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Text') {
			$model = 'Document';
			$artefact_type = 'Document';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Exel') {
			$model = 'Document';
			$artefact_type = 'Document';
			$sorting_column = 'artefact_type_id';
		} elseif ($artefact_type_id == 'Web') {
			$model = 'Document';
			$artefact_type = 'Document';
			$sorting_column = 'artefact_type_id';
		} else {
			$model = '';
			$artefact_type = '';
			$sorting_column = '';
		}

		$artefactType['model'] = $model;
		$artefactType['sorting_column'] = $sorting_column;
		$artefactType['artefact_type'] = $artefact_type;
		return $artefactType;
	}

public function saveTopic(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {

			$data = $request->all();
			log::debug($data);
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['request'] = $request;

			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('Topic');
			$request->validate($rules);

			if (!empty($_FILES['node_icon']['tmp_name'])) {
				$file = imgToBase64($_FILES['node_icon']['tmp_name']);
				$data['node_icon'] = $file;
			} else {
				$data['node_icon'] = null;
			}

			//
			$topic_id = LearningTreeService::saveTopic($data);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $topic_id;
	}
	
	public function deleteTopic($id, $containerLearningTree, $tab) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {

			$masterDataInterface = parent::$masterDataInterface;

			$topic_id = LearningTreeService::deleteTopic($id, $containerLearningTree, $tab, $masterDataInterface);
			return $topic_id;

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function getTopicForEdit($id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$topic = LearningTreeService::getTopicForEdit($id);
			Log::debug($topic);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $topic;
	}

	public function getParentStructure($containerLearningTree, $id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$parent_structure = LearningTreeService::getParentStructure($containerLearningTree, $id);
			Log::debug($parent_structure);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $parent_structure;
	}

	public function getUpdateStructure($containerLearningTree) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$update_structure = LearningTreeService::getUpdateStructure($containerLearningTree);
			Log::debug($update_structure);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $update_structure;
	}

	public function getSvg($containerLearningTree, $parent_id, $parent_name) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($parent_id);
		// var_dump($parent_id);
		// exit();

		try {
			//
			// print_r($containerLearningTree." ".$parent_id);
			// exit();
			// $svg = LearningTreeSvgRenderService::renderLearningTree('MLT', $containerLearningTree, $parent_id, parent::$masterDataInterface, 1);
			$svg = LearningTreeSvgCoordinatorService::createLearningTreeSvg('MLT', $containerLearningTree, $parent_name, $parent_id, parent::$masterDataInterface, 1);
			// Log::debug($svg);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $svg;
	}

	public function generateSvg(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		return LearningTreeSvgCoordinatorService::createLearningTreeSvg($request['tree_type'], $request['tree_id'], $request['parent_node_title'], $request['parent_node_id'], parent::$masterDataInterface, 1);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

/////////////////////////////// program learning tree part \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

}

