<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class PaymentFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        //dd("hi");
        // filter method for getting filtered data
        try {
            $data = DB::table('payments')
                ->leftJoin('organizations as org', 'org.id', '=', 'payments.organization_id')
                ->leftJoin('projects as pro', 'pro.id', '=', 'payments.project_id')
                ->leftJoin('expense_types as et', 'et.id', '=', 'payments.expense_type_id')
                ->select(
                    'org.name as company_name',
                    'pro.name as project_name',
                    'et.name as expense_type',
                    'payments.expense_date as expense_date',
                    'payments.service_name as service_name', 
                    'payments.amount as amount', 
                    'payments.amount_paid as amount_paid', 
                    'payments.id as id')->where('payments.deleted_at', null);

            if ($request->has('search')) {
                $data->whereRaw("( 
                 lower(pro.name) like '%" . strtolower($request['search']). "' or
                 lower(org.name) like '%" . strtolower($request['search']). "'  or
                 lower(et.name) like '%" . strtolower($request['search']). "'  or
                 lower(payments.service_name) like '%" . strtolower($request['search']). "' 

            )");
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'company_name':
                        $data->orderBy('org.name', $request['sort']);
                        break;
                   /* case 'project_name':
                        $data->orderBy('pro.name', $request['sort']);
                        break;
                    case 'expense_type':
                        $data->orderBy('expense_date.unit', $request['sort']);
                        break;
                    case 'service_name':
                        $data->orderBy('payments.service_name', $request['sort']);
                        break;
                    case 'amount':
                        $data->orderBy('payments.amount', $request['sort']);
                        break;*/
              
                   
                }
            } else {
                $data->orderBy('payments.id', 'desc');
            }

            //dd($filterColumns);
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            //dd($data->get());
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}

