
ALTER table materials ADD column supplier_id integer;
alter table materials drop column unit_id;

INSERT INTO public.menus(
     id, display_name, name, sequence,  auth, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (86, 'Materials', 'Materials', 60,  1, '', now(), now(),  1, 1);


    INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES  
    ( 'List', 'List Materials', 65, 86,  1, now(), now(),  1, 1), 
    (  'New Material', 'Create Materials', 65, 86,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020


INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (31,'Materials', now(), now(), 1, 1),
     (32, 'List Materials', now(), now(), 1, 1),
     (33, 'Create Materials', now(), now(), 1, 1),
     (34, 'Show Materials', now(), now(), 1, 1),
     (35, 'Delete Materials', now(), now(), 1, 1),
     (36, 'Edit Materials', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (31, 1, 31, now(), now(), 1,1),
     (32, 1, 32, now(), now(), 1,1),
     (33, 1, 33, now(), now(), 1,1),
     (34, 1, 34, now(), now(), 1,1),
     (35, 1, 35, now(), now(), 1,1),
     (36, 1, 36, now(), now(), 1,1);


INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Material', 'Update', 'name', 'required|max:255|unique:materials,name,$material,id,deleted_at,NULL,supplier_id,$supplier_id', 1, now(), now(), 1, 1),
     ('Material', 'Create', 'name', 'required|max:255|unique:materials,name,NULL,id,deleted_at,NULL,supplier_id,$supplier_id', 1, now(), now(), 1, 1);
  

ALTER SEQUENCE materials_id_seq RESTART WITH 10;

INSERT INTO public.list_view_masters(
             function_name, entity_name,  
             query_columns, 
            heading_columns,
             filter_columns, 
             display_css_widths, 
             relations, replace_columns,
              created_at, updated_at, created_by, updated_by,
              filter_class_names,
              entity_namespace)
    VALUES ('List Materials', 'Material',
            'name,code,unit,supplier_name,id', 
            'Name,Code,Unit,Supplier,Action',
             '', 
             'col-xs-2,col-xs-1,col-xs-2,col-xs-2,col-xs-2', 
             '', '', now(),now(),1,1, 
             'App\Utilities\MaterialFilterUtility', 
             'App\Models\');
