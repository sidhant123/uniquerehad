<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jan 2018 14:15:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CommunicationEventTemplatePlaceHolder
 * 
 * @property int $id
 * @property int $template_place_holders_id
 * @property int $communication_events_id
 * 
 * @property \App\Models\CommunicationEvent $communication_event
 * @property \App\Models\TemplatePlaceHolder $template_place_holder
 *
 * @package App\Models
 */
class CommunicationEventTemplatePlaceHolder extends Eloquent
{
	protected $table = 'communication_event_template_place_holder';
	public $timestamps = false;

	protected $casts = [
		'template_place_holders_id' => 'int',
		'communication_events_id' => 'int'
	];

	protected $fillable = [
		'template_place_holders_id',
		'communication_events_id'
	];

	public function communication_event()
	{
		return $this->belongsTo(\App\Models\CommunicationEvent::class, 'communication_events_id');
	}

	public function template_place_holder()
	{
		return $this->belongsTo(\App\Models\TemplatePlaceHolder::class, 'template_place_holders_id');
	}
}
