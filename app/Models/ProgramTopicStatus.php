<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Jul 2018 17:16:48 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramTopicStatus
 * 
 * @property int $id
 * @property int $program_id
 * @property int $topic_id
 * @property int $parent_topic_id
 * @property int $children_node_count
 * @property \Carbon\Carbon $start_datetime
 * @property \Carbon\Carbon $end_datetime
 * @property string $status
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ProgramTopicStatus extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'topic_id' => 'int',
		'parent_topic_id' => 'int',
		'children_node_count' => 'int',
		'user_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'start_datetime',
		'end_datetime'
	];

	protected $fillable = [
		'program_id',
		'topic_id',
		'parent_topic_id',
		'children_node_count',
		'start_datetime',
		'end_datetime',
		'status',
		'user_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
