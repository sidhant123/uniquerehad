
  ALTER TABLE expenses ALTER COLUMN qty TYPE NUMERIC(10,2);
  ALTER TABLE expenses ALTER COLUMN bill_no TYPE varchar(100);
  ALTER TABLE expenses ADD COLUMN base_amount  NUMERIC(15,2);



  ALTER TABLE payments ADD COLUMN base_amount  NUMERIC(15,2);
  ALTER TABLE payments ADD COLUMN total_deduction  NUMERIC(15,2);

  ALTER TABLE payments ALTER COLUMN invoice_no TYPE varchar(100);


update list_view_masters set query_columns= 'payment_no,project_name,boq_no,invoice_no,invoice_date,invoice_amount,total_deduction,amount_received,id' ,
heading_columns='Payment No.,Project,BOQ No.,Invoice No.,Invoice Date,Invoice Amount,Total Deduction Amount,Received Amount,Action',
display_css_widths = 'col-xs-1,col-xs-2,col-xs-1,col-xs-2,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1'
where entity_name = 'Payment';

-- added mohit's script
alter table sites add column location varchar(25);
alter table sites add column code varchar(25);
update list_view_masters set query_columns = 'name,address,location,id' where function_name = 'List Sites';
update list_view_masters set heading_columns = 'Site name,Address,Location,Action' where function_name = 'List Sites';
update list_view_masters set display_css_widths = 'col-xs-3,col-xs-5,col-xs-3,col-xs-1' where function_name = 'List Sites';

update list_view_masters set heading_columns = 'Company Name,Company Code,City,Phone,Action' where id = 1;
update list_view_masters set heading_columns = 'Company Name,Company Code,City,Phone,Action' where id = 1;
update list_view_masters set heading_columns = 'Material Name,Material Code, Material Unit,Material Supplier,Action' where id = 10;

