<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 04 Jan 2018 22:51:07 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class CompositionMaster
 * 
 * @property int $id
 * @property string $route_name
 * @property string $controller_name
 * @property string $view_name
 * @property int $sequence
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class CompositionMaster extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'sequence' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'route_name',
		'controller_name',
		'view_name',
		'sequence',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
