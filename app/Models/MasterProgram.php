<?php


namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

class MasterProgram extends BaseModel
{
    //
     	use \Illuminate\Database\Eloquent\SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     // protected $searchableColumns = ['name','description', 'start_date','end_date'];

    // protected $fillable = [
    //                 'name','description', 'start_date','end_date','customer_party_id','created_by','updated_by','deleted_by','deleted_at'
    // ];
}
