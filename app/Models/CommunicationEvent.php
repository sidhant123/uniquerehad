<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jan 2018 11:05:31 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class CommunicationEvent
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property array $only_for_customers
 * @property array $customers_excluded_from_event
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $templates
 * @property \Illuminate\Database\Eloquent\Collection $communication_trails
 *
 * @package App\Models
 */
class CommunicationEvent extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'only_for_customers' => 'json',
		'customers_excluded_from_event' => 'json',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'only_for_customers',
		'customers_excluded_from_event',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\User::class, 'deleted_by');
	}

	public function templates()
	{
		return $this->belongsToMany(\App\Models\Template::class)
					->withPivot('id', 'created_by', 'updated_by', 'deleted_by', 'deleted_at')
					->withTimestamps();
	}
	public function templates_place_holders()
	{
		return $this->belongsToMany(\App\Models\TemplatePlaceHolder::class)
					->withPivot('id');
					
	}

	public function communication_trails()
	{
		return $this->hasMany(\App\Models\CommunicationTrail::class);
	}
}
