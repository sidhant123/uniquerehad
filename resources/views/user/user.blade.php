@extends('layouts.base')
@section('content')

  @include('errors.error_validation')
  <form style="border-style:dotted; border-width: 1px;" action="/users{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFieldsForUser()" enctype="multipart/form-data">
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/users'])
    <div class="row">
      <div class="col-lg-11"><h3 class="headings">{{ucfirst(substr(Route::currentRouteName(),6))}} User</h3></div>
    </div>

    <div class="row">
      @isset($data->photo)
      <div class="col-lg-12 @yield('logo')" align="right"><img src="/storage{{str_replace('public/','',$data->photo)}}" alt="" width="100" height="100"  style="border:groove 1px;"></div>
      @endisset
    </div>

  
 <div class="row">
  <div class="col-lg-6">
      <div id="company_div_id" class="form-group required">
        <label for="company_id@yield('organization_name_d')">{{ __('labels.users.company_name') }}</label>
        <select  class="form-control mandatory" name="organization_name" id="company_id"@yield('organization_name_d')>
  @if(isset($organization))
  <option value="0">--select--</option>'
  @foreach($organization as $company)
  @isset($org_id)
  @if($company->id === $org_id)
  <option value="{{$company->id}}" selected>{{$company->name}}</option>
  @else
  <option value="{{$company->id}}">{{$company->name}}</option>
  @endif
  @endisset
   @if(!empty(old('organization_name')))
   @if($company->id === old('organization_name'))
   <option value="{{$company->id}}" selected>{{$company->name}}</option>
   @else
<option value="{{$company->id}}">{{$company->name}}</option>
   @endif
   @else
  <option value="{{$company->id}}">{{$company->name}}</option>
  @endif
  @endforeach
  @endif
        </select>
    </div>

  </div>
      <div class="col-lg-6">
      <div class="form-group required">
        <label for="assign_role_id@yield('role_name_d')">{{ __('labels.users.role_name') }}</label>
        <select  class="form-control mandatory" name="assign_role_id"
         id="assign_role_id"@yield('role_name_d')>
  <option value="0">--select--</option>'
  @if(isset($role))
  @foreach($role as $role_value)
  @if(isset($sel_role_id))
  @if($role_value->id === $sel_role_id)
  <option value="{{$role_value->id}}" selected>{{$role_value->name}}</option>
  @else
  <option value="{{$role_value->id}}">{{$role_value->name}}</option>
  @endif
  @else
  <option value="{{$role_value->id}}">{{$role_value->name}}</option>
  @endif
  @endforeach
  @endif
        </select>
    </div>
</div>
</div>


    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="firstName">{{__('labels.users.first_name')}}</label>
            @if(!empty(old('firstName')))
            <input type="text" class="form-control mandatory" id="first_name"  style="text-transform:capitalize"  name="firstName" value="{{ old('firstName') }}" @yield('firstName_d')>
            @else
            <input type="text" class="form-control mandatory" id="first_name"  style="text-transform:capitalize"   name="firstName" value="@yield('firstName')" @yield('firstName_d')>
            @endif

           <input type="hidden" name="old_photo" value="@yield('old_photo')">
           <input type="hidden" name="old_photo_name" value="@yield('old_photo_name')">
           <input type="hidden" name="old_userid" value="@yield('old_userid')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="lastName">{{__('labels.users.last_name')}}</label>
            @if(!empty(old('lastName')))
            <input type="text" class="form-control mandatory"  style="text-transform:capitalize"  id="last_name" name="lastName" value="{{ old('lastName') }}"  @yield('lastName_d')>
            @else
            <input type="text" class="form-control mandatory" style="text-transform:capitalize"  id="last_name" name="lastName" value="@yield('lastName')"  @yield('lastName_d')>
            @endif

         </div>
     </div>
  </div>

  <div class="row">
      <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="email">{{__('labels.users.email')}}</label>
           @if(!empty(old('email')))
           <input type="text" class="form-control mandatory" id="email" name="email" value="{{ old('email') }}">
           @else
           <input type="text" class="form-control mandatory" id="email" name="email" value="@yield('email')">
           @endif
         </div>
     </div>
       <div class="col-lg-6" >
       <div class="form-group required" id="dob_wrapper">
           <label for="twitterHandle">Date of Birth</label>
           @if(!empty(old('dob')))
           <input type="text" onkeypress="return isNumeric(event, this.value);" class="form-control mandatory" id="dob" name="dob" value="{{ old('dob') }}" placeholder="MM/DD/YYYY">
           @else
           <input type="text" onkeypress="return isNumeric(event, this.value);" class="form-control mandatory" id="dob" name="dob" value="@yield('dob')" placeholder="MM/DD/YYYY">
           @endif
        </div>
    </div>
  </div>
<div class="row">
      <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="primaryContact">{{__('labels.users.primary_contact_no')}}</label>
           @if(!empty(old('primaryContact')))
           <input type="number" onKeyPress="if(this.value.length>9) return false;" class="form-control mandatory" id="primaryContact" name="primaryContact" value="{{ old('primaryContact') }}" pattern="[1-9]{1}[0-9]{9}">
           @else
           <input type="number" onKeyPress="if(this.value.length>9) return false;" class="form-control mandatory" id="primaryContact" name="primaryContact" value="@yield('primaryContact')" pattern="[1-9]{1}[0-9]{9}">
           @endif
         </div>
     </div>

      <div class="col-lg-6 req">
       <div class="form-group">
           <label for="secondaryContact">{{__('labels.users.secondary_contact_no')}}</label>
           @if(!empty(old('secondaryContact')))
           <input type="number" onKeyPress="if(this.value.length>9) return false;" class="form-control" id="secondaryContact" name="secondaryContact" value="{{ old('secondaryContact') }}" pattern="[1-9]{1}[0-9]{9}">
           @else
           <input type="number" onKeyPress="if(this.value.length>9) return false;" class="form-control" id="secondaryContact" name="secondaryContact" value="@yield('secondaryContact')" pattern="[1-9]{1}[0-9]{9}">
           @endif
         </div>
     </div>
  </div>


  <div class="row">
    <div class="col-lg-6 req">
       <div class="form-group" id="photo_div_id">
           <label for="file">{{__('labels.users.photo')}}</label>
           <input  type="file" class="form-control " id="file" onchange="return fileValidation(this.id);" name="photo" accept=".png,.jpeg,.jpg">
         </div>
     </div>
     <div class="col-lg-6 form-group required">
        <label for="compnay_id">Gender</label>
        <div class="row" style="font-weight: bold;">
            <div class="col-lg-3">
             <input type="radio" name="gender" checked="checked" value="M">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Male
            </div>
            <div class="col-lg-6">
             <input type="radio" name="gender" value="F">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Female
            </div>
        </div>
     </div>

  </div>

  <!-- abhi - 26/03/19 -->
  <div class="row">
  
  
  </div>

 <!-- <div class="row" >
    <div class="col-lg-12" >
     <label><input type="checkbox" id="facilitator_check_id" name="facilitator_check" >  Is a Facilitator </label>
    </div>
  </div>
-->

<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.users.submit')}}</button>
</div>
</div>
<p></p>
</form>

<p></p>

<!-- for import user -->
@section('importUser')
@show
@endsection

@section('css')
<style>
  .disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}

</style>
@endsection

@push('scripts')

<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script src="{{ asset('/js/user.js')}}"></script>
<script>
function validateMandatoryFieldsForUser(){
  var valid = true;

var company_id = $('#company_id').val();
if(company_id == 0) {
document.getElementById("select2-company_id-container").style.border = "1px solid red";
}else {
  document.getElementById("select2-country_id-container").style.border = "0px solid red";
}

  valid=validateMandatoryFields();

  if(valid){
  var isnumFirst = /^\d+$/.test($("#first_name").val());
  var isnumLast = /^\d+$/.test($("#last_name").val());
  if(isnumFirst) {
   alert('First name can not be number');
   return false;
  } else if(isnumLast){
   alert('First name can not be number');
   return false;
  }
  else {
    valid = true;
  }




   var email = $('#email').val();
     var res = validateemail(email);
     if(!res){
      return false;
     }else {

       var phone = $('#primaryContact').val();
     // alert(phone);
    var contactRes = validatecontact(phone);
         if(!contactRes){
      return false;
     } else {

  var gender=$('input[name=gender]:checked').val();
  if (!gender && valid ) {

     var selector='input[name=gender]';
     scrollToElement(selector)
     valid=false;
  }
     return valid;
     }
  }

} return valid;
}

function scrollToElement(selector){
  console.log('in scrollToElement')
  var scrollPos =  $(selector).offset().top;
  $('html, body').animate({scrollTop:(Number(scrollPos) - 72)}, 'slow');
}

// abhi
$(document).on('change','#company_id',function(){
  var id=$('#company_id').val();
  getOrgPassLogic(id);
})

function getOrgPassLogic(id){

    var url="/getOrgPassLogic/"+id;
    axios.get(url)
        .then(function (response) {
        var responseData=response['data'];
        console.log(responseData);

        if (responseData['dob_mandatory']) {
          $('#dob').addClass("mandatory");
          //$('#dob_wrapper').addClass("required");
        }else{
          $('#dob').removeClass("mandatory");
         // $('#dob_wrapper').removeClass("required");
        }
    })
        .catch(function (error) {
        console.log("error"+error);
   });
}

function isNumeric(event) { // Numeric only

    var dob =$('#dob').val().trim();
    if (dob.length == 2 || dob.length == 5) {
      if (event.which != 47) {
          return false;
      }
      else{
        return true;
      }
    }else if (dob.length == 10) {
        return false;
    }

    var k;
    document.all ? k = event.keycode : k = event.which;
    return((k > 47 && k < 58) || k === 0 || k === 8 || k === 118 || k === 86 || k === 17);
}
$(document).ready(function() {

document.querySelector("#first_name").addEventListener("keypress", function (evt) {
    if (evt.which > 32 && evt.which < 65)
    {
        evt.preventDefault();
    }
});

document.querySelector("#last_name").addEventListener("keypress", function (evt) {
    if (evt.which > 32 && evt.which < 65)
    {
        evt.preventDefault();
    }
});


document.querySelector("#primaryContact").addEventListener("keypress", function (evt) {
    if (evt.which > 47 && evt.which < 58)
    {
      return true;
    } else {
        evt.preventDefault();
    }
});

document.querySelector("#secondaryContact").addEventListener("keypress", function (evt) {
    if (evt.which > 47 && evt.which < 58)
    {
      return true;
    } else {
        evt.preventDefault();
    }
});


});
// function ValidateEmail(email)
// {
// var mailformat = /^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/;
// if(email.value.match(mailformat))
// {
// document.form1.text1.focus();
// alert('email is valid');
// return true;
// }
// else
// {
// alert("You have entered an invalid email address!");
// document.form1.text1.focus();
// return false;
// }
// }


function validateemail(email)
{
var x=email;
var atposition=x.indexOf("@");
var dotposition=x.lastIndexOf(".");
if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){
  alert("Please enter a valid e-mail address");  
  return false;
  } else {
    return true;
  }
}

function validatecontact(phone) {


  if(phone.length > 9 && phone.length < 11)
   {  
    return true;
    }
  else {

  alert("Please enter a valid contact number");
  return false;
    }
  }

$("#company_id").on("change", function() {
      // alert('ok');
      customer_party_id = $("#company_id").val();
      console.log('Company customer part yid')
      console.log(customer_party_id)
      if(customer_party_id !=0) {
      // get_company_url = APP.'getCompanyName/'+formData;
        get_company_url= "{{url('/')}}"+"/getCompanyRole/"+customer_party_id
        console.log(get_company_url)
        $.ajax({
          url: get_company_url ,
          method: 'get',
          type: 'JSON',
          success: function(response) {
            console.log('This is response')
            console.log(response)
            $('#assign_role_id option').remove();
            $('#assign_role_id').append("<option value=''>--Select--</option>");
            console.log($.type(response))
            console.log("length = "+response);
            for (var i = 0; i < response.length; i++) {
             console.log("IN LOOP == "+response[i]['name']);

              $('#assign_role_id').append("<option  id='client-roleOptions+"+response[i]['id']+"' value="+response[i]['id']+">"+response[i]['name']+"</option>");
              console.log("IN LOOP == "+response[i]['name']);
            }
          },
          error: function() {
            alert('There no role')
          }
        })
      }else {
          $('#assign_role_id option').remove();

      }
    });

</script>
@endpush


