@extends('layouts.base')
@section('content')

  @include('errors.error_validation')
   
  <form style="border-style:dotted; border-width: 1px;" action="/projects{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFieldsForProject()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/projects'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Project

      </h3></div>
    </div>
  <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
        <label for="customer_id@yield('cust_name_d')">{{ __('labels.projects.cust_name') }}</label>
        <select  class="form-control mandatory" name="customer_id"
         id="customer_id"@yield('cust_name_d')>
            @if(isset($customer))
            <option value="0">--select--</option>'
            @foreach($customer as $cust_val)
            @if(isset($org_id))
            @if($cust_val->id == $org_id)
            <option value="{{$cust_val->id}}" selected>{{$cust_val->name}}</option>
            @else
            <option value="{{$cust_val->id}}">{{$cust_val->name}}</option>
            @endif
            @else
            <option value="{{$cust_val->id}}">{{$cust_val->name}}</option>
            @endif
            @endforeach
            @endif
        </select>
    </div>

     </div>
   </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="name">{{__('labels.projects.name')}}</label>
            @if(!empty(old('name')))
              <input type="text" class="form-control mandatory"  name="name" id="name" value="{{ old('name') }}"  style="text-transform:capitalize " @yield('name_d')>
            @else
            <input type="text"  style="text-transform:capitalize " class="form-control mandatory" id="name"  name="name" value="@yield('name')" @yield('name_d')>
            @endif        
         </div>
     </div>
     <div class="col-lg-6">
       <div class="form-group ">
           <label for="description">{{__('labels.projects.description')}}</label>
            @if(!empty(old('description')))
            <input type="text" class="form-control" max="500" name="description" id="description" value="{{ old('description') }}"  @yield('description_d')>
            @else
            <input type="text" class="form-control" max="500" name="description" id="description" value="@yield('description')"  @yield('description_d')>
            @endif

         </div>
     </div>
  </div>


<div class="row">
   <div class="col-lg-6 req">
   <div class="form-group required">
        <label for="site_id@yield('site_name_d')">{{ __('labels.projects.site_name') }}</label>
         <select  class="form-control mandatory" name="site_name" id="site_id" @yield('site_name_d')>
            @if(isset($site))
            <option value="0">--select--</option>'
            @foreach($site as $site_attr)
            @isset($site_id)
            @if($site_attr->id === $site_id)
            <option value="{{$site_attr->id}}" selected>{{$site_attr->name}}</option>
            @else
            <option value="{{$site_attr->id}}">{{$site_attr->name}}</option>
            @endif
            @endisset
              @if(!empty(old('site_name')))
             @if($site_attr->id === old('site_name'))
             <option value="{{$site_attr->id}}" selected>{{$site_attr->name}}</option>
             @else
          <option value="{{$site_attr->id}}">{{$site_attr->name}}</option>
             @endif
             @else
            <option value="{{$site_attr->id}}">{{$site_attr->name}}</option>
            @endif
            @endforeach
            @endif
          </select>
     </div>
</div>
   <div class="col-lg-6">
       <div class="form-group ">
           <label for="site_location">{{__('labels.projects.site_location')}}</label>
            @if(!empty(old('site_location')))
            <input type="text" class="form-control" name="site_location" id="site_location" value="{{ old('site_location') }}" readonly="readonly"  @yield('site_location_d')>
            @else
            <input type="text" class="form-control" name="site_location" id="site_location" value="@yield('site_location')" readonly="readonly" @yield('site_location_d')>
            @endif

         </div>
     </div>
</div>
<div class="row">
 <div class="col-lg-6 req">
    <div class="form-group required">
        <label for="site_eng_user_id@yield('site_eng_user_id_d')">{{ __('labels.projects.site_eng_user_id') }}</label>
         <select  class="form-control mandatory"  name="site_eng_user_id" id="site_eng_user_id"@yield('site_eng_user_id_d')>
            @if(isset($user_site_eng))
          <option value="0">--select--</option>'
            @foreach($user_site_eng as $user_site_eng_row)
          @if(isset($site_engineer_user_id))
            @if($user_site_eng_row['id'] === $site_engineer_user_id)
            <option value="{{$user_site_eng_row['id']}}" selected>{{$user_site_eng_row['first_name'].'-'.$user_site_eng_row['email']}}</option>
          @else
            <option value="{{$user_site_eng_row['id']}}">{{$user_site_eng_row['first_name'].'-'.$user_site_eng_row['email']}}</option>
          @endif
          @else
            <option value="{{$user_site_eng_row['id']}}">{{$user_site_eng_row['first_name'].'-'.$user_site_eng_row['email']}}</option>
         @endif
         @endforeach
         @endif
          </select>
     </div>
    </div>  
<div class="col-lg-6 req">
   <div class="form-group required">
        <label for="client_role_id@yield('client_role_id_d')">{{ __('labels.projects.client_role_id') }}</label>
          <select  class="form-control mandatory" name="client_role_id" id="client_role_id"@yield('client_role_id_d')>
           <option value="0">--select--</option>
               @if(isset($user_client))
             @foreach($user_client as $user_client_row)
             @if(isset($client_user_id))
             @if($user_client_row['id'] == $client_user_id)
             <option value="{{$user_client_row['id']}}" selected>{{$user_client_row['first_name'].'-'.$user_client_row['email']}}</option>
             @else 
                <option value="{{$user_client_row['id']}}">{{$user_client_row['first_name'].'-'.$user_client_row['email']}}</option> 
               @endif

             @endif
            @endforeach
            @endif
          </select>
     </div>
   </div>
  </div>

<div class="row">
      <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="start_date">{{__('labels.projects.start_date')}}</label>
            @if(!empty(old('start_date')))
           <input type="date" class="form-control mandatory" id="start_date" name="start_date" value="{{ old('start_date') }}"  onchange="return validateDate()">

           @else
           <input type="date" class="form-control mandatory" id="start_date" name="start_date" value="@yield('start_date')"  onchange="return validateDate()">
           @endif
         </div>
     </div> 
     <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="end_date">{{__('labels.projects.end_date')}}</label>
            @if(!empty(old('end_date')))
           <input type="date" class="form-control mandatory" id="end_date" name="end_date" value="{{ old('end_date') }} " onchange="return validateDate()">

           @else
           <input type="date" class="form-control mandatory" id="end_date" name="end_date" value="@yield('end_date')" onchange="return validateDate()">
           @endif
         </div>
     </div>
   </div>


   <div class="row">
  
       <div class="col-lg-6">
       <div class="form-group required">
           <label for="delay_hr">{{__('labels.projects.delay_hr')}}</label>
            @if(!empty(old('delay_hr')))
              <input type="number" min="1" class="form-control mandatory" id="delay_hr" name="delay_hr" value="{{ old('delay_hr') }}" @yield('delay_hr_d')>
            @else
            <input type="number" min="1" class="form-control mandatory" id="delay_hr"  name="delay_hr" value="@yield('delay_hr')" @yield('delay_hr_d')>
            @endif        
         </div>
     </div>
</div>


<div class="row">
     <div class="col-lg-6 req">
      <div class="form-group" id="tender_doc_div_id">
           <label for="file">{{__('labels.projects.tender_doc')}}</label>
           <input  type="file" class="form-control " id="tender_doc_id" onchange="return fileValidation(this.id);" name="tender_doc" accept=".doc,.docx,.xlsx,.pdf">
         </div>
     </div>  

      <div class="col-lg-6 req">
      <div class="form-group" id="schedule_pdf">
           <label for="schedule_pdf">{{__('labels.projects.schedule_pdf')}}</label>
           <input  type="file" class="form-control " id="schedule_pdf_id" onchange="return fileValidation(this.id);" name="schedule_pdf" accept=".pdf">
         </div>
     </div>
</div>

 <div class="row">
@if(isset($tender_doc_path ))
@if($tender_doc_path != '')
      <div class="col-lg-6 align=center">
        <label for="tender file">
        <a href="{{$tender_doc_path}}" target="_blank">
        <span class="glyphicon glyphicon-file"></span> Tender Document</a>
       </label></div> 
@endif
@endif  
@if(isset($schedule_pdf_path ))
@if($schedule_pdf_path != '')     
       <div class="col-lg-6 align=center">
        <label for="tender file">
        <a href="{{$schedule_pdf_path}}" target="_blank">
        <span class="glyphicon glyphicon-file"></span>Schedule Pdf</a>
       </label></div>
@endif
@endif        
  </div>

<p></p> 
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.projects.submit')}}</button>
</div>
</div>
<p></p>

<!-- for import user -->
@section('importBoq')
@show
@endsection

@push('scripts')

<p></p>

<p></p>

</div>
</form> 

<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script>

$(document).ready(function() {
$('#site_eng_user_id').select2({
  }); 



$('#customer_id').select2({
  });
$('#client_role_id').select2({
  });
 $('#site_id').select2({
  }); 
    $(".mandatory").removeClass('fillFields');
});

function validateMandatoryFieldsForProject(){

  var valid = true;
 valid=validateMandatoryFields();

      var customer_id = $('#customer_id').val();
    // alert(customer_id);
  if(customer_id == 0) {
document.getElementById("select2-customer_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-customer_id-container").style.border = "0px solid red";

}

      var site_id = $('#site_id').val();
    // alert(site_id);
  if(site_id == 0) {
document.getElementById("select2-site_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-site_id-container").style.border = "0px solid red";

}

      var client_role_id = $('#client_role_id').val();
    // alert(client_role_id);
  if(client_role_id == 0) {
document.getElementById("select2-client_role_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-client_role_id-container").style.border = "0px solid red";

}

      var site_eng_user_id = $('#site_eng_user_id').val();
    // alert(site_eng_user_id);
  if(site_eng_user_id == 0) {
document.getElementById("select2-site_eng_user_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-site_eng_user_id-container").style.border = "0px solid red";

}
if(valid == false){
return false;
}
  if(valid){
  var name = $('#name').val();
  var desc = $('#description').val();
  var delay_hr =  parseInt($('#delay_hr').val());
  var isnum = /^\d+$/.test(name);
  var isnum_desc = /^\d+$/.test(desc);

  var check1 = delay_hr >= 1 && delay_hr <= 168;
  if(isnum) {
  alert('Project name can not be only number');
  return false;
  }
  else if(isnum_desc){
  alert('Description can not be only number');
  return false;
  }
  else if (!check1) {
    alert("ETA/Complaint should be between 1 to 168 hr(s)");
      return false;
  }
  else {
    valid = true;
  }
}
  return valid;
}

function scrollToElement(selector){
  console.log('in scrollToElement')
  var scrollPos =  $(selector).offset().top;
  $('html, body').animate({scrollTop:(Number(scrollPos) - 72)}, 'slow');
}

// function validateMandatoryFieldsForProject(){
//   var valid = true;
//   valid = validateMandatoryFields();
  
// }

function validateDate(){

var valid = true;
var fromdate = Date.parse($("#start_date").val());
var todate = Date.parse($("#end_date").val());
//alert(fromdate>todate);
if(fromdate>todate){
  $("#end_date").val("");
     var selector='input[name=end_date]';
     scrollToElement(selector)
     valid=false;
}
  return valid;
}
$("#customer_id").on("change", function() {
      // alert('ok');
      customer_party_id = $("#customer_id").val();
      console.log('Company customer part yid')
      console.log(customer_party_id)
      if(customer_party_id !=0) {
      // get_company_url = APP.'getCompanyName/'+formData;
        get_company_url= "{{url('/')}}"+"/getCompanyUser/"+customer_party_id
        console.log(get_company_url)
        $.ajax({
          url: get_company_url ,
          method: 'get',
          type: 'JSON',
          success: function(response) {
            console.log('This is response')
            console.log(response)
            $('#client_role_id option').remove();
            $('#client_role_id').append("<option value=''>--Select--</option>");
            console.log($.type(response))
            console.log("length = "+response);
            for (var i = 0; i < response.length; i++) {
             console.log("IN LOOP == "+response[i]['first_name']);

              $('#client_role_id').append("<option  id='client-userOptions+"+response[i]['id']+"' value="+response[i]['id']+">"+response[i]['first_name']+"-"+response[i]['email']+"</option>");
              console.log("IN LOOP == "+response[i]['first_name']);
            }
          },
          error: function() {
            alert('There no client users')
          }
        })
      }else {
          $('#client_role_id option').remove();

      }
    });

$("#site_id").on("change", function() {
      // alert('ok');
      site_id = $("#site_id").val();
      console.log('Company customer part yid')
      console.log(site_id)
      if(site_id !=0) {
      // get_company_url = APP.'getCompanyName/'+formData;
        get_site_url= "{{url('/')}}"+"/getSiteLocation/"+site_id
        console.log(get_site_url)
        $.ajax({
          url: get_site_url ,
          method: 'get',
          type: 'JSON',
          success: function(response) {
            console.log('This is response')
            console.log(response)
            $('#site_location').val('');
           $('#site_location').val(response['location']);

            console.log("IN LOOP == "+response['location']);
          },
          error: function() {
            alert('There no site location')
          }
        })
      }else {
         // $('#client_role_id option').remove();

      }
    });


// edit 


</script>
@endpush


