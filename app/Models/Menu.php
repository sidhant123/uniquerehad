<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 03 Feb 2018 07:59:31 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;


/**
 * Class Menu
 * 
 * @property int $id
 * @property string $display_name
 * @property string $name
 * @property int $sequence
 * @property int $parent_menu_id
 * @property string $icon
 * @property int $auth
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Menu $menu
 * @property \Illuminate\Database\Eloquent\Collection $menus
 *
 * @package App\Models
 */
class Menu extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'sequence' => 'int',
		'parent_menu_id' => 'int',
		'auth' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'display_name',
		'name',
		'sequence',
		'parent_menu_id',
		'icon',
		'auth',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function menu()
	{
		return $this->belongsTo(\App\Models\Menu::class, 'parent_menu_id');
	}

	public function menus()
	{
		return $this->hasMany(\App\Models\Menu::class, 'parent_menu_id');
	}
}
