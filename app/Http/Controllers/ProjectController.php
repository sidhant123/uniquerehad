<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;
use DB;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;

use App\Models\Project;
use App\Models\Organization;
use App\Models\Person;
use App\Services\ValidationRulesService;
use App\Services\ProjectService;
use App\Services\SiteService;
use App\Services\UserService;
use App\Services\BoqService;
use App\Services\OrganizationService;
use App\Services\DocumentService;



class ProjectController extends BaseController
{


     public function __construct() {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        $this->middleware('CheckCompositeMiddleWare', ['except' => ['update', 'create', 'show']]);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
     //   Log::debug(\Auth::user()->hasAccess(['Projects']));
    //$this->authorize('List Projects', 'List Projects');

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

        return PaginateController::paginate('Project', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);  

    }



    /**
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
     //     $this->authorize('Create Projects', 'Create Projects');
        $customer = OrganizationService::retrieveOrganization();
        $site = SiteService::retrieveSites();
        $user_site_eng = self::getSiteUsers();
       // $user_site_eng = UserService::retriveSiteEng();
        //$user_client = UserService::retriveClient();
         return view('project.create_project', compact('site','user_client', 'user_site_eng', 'customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Create Projects', 'Create Projects');
        DB::beginTransaction();

        try {

           $request['name'] = ucwords(strtolower(trim($request['name'])));

             $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            $rules = $valdationRulesService->validationRulesWithTenant('Project');

            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                //dd($validator);
                if ($validator->fails()) {
                    return redirect('projects/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['request'] = $request;
           
            $inserted_project_id = ProjectService::saveData($data);
            $data['inserted_project_id'] = $inserted_project_id;
            $data['route_name'] = "store";
            if(isset($data['imported_boq_file'])) {

              $res = self::verifyImporBoqtData($data);
            }

            if ($res=='COLS_TITLE_MISMATCH') {
               DB::rollback();
            Log::debug(__CLASS__ . "::" . __METHOD__ . "COLS_TITLE_MISMATCH DB--ROLLBACK");
            Log::error(__CLASS__ . "::" . __METHOD__ . "COLS_TITLE_MISMATCH DB--ROLLBACK");
             Session::flash('status', __('Error:Please check BOQ import excel format.'));
              return redirect()->action('ProjectController@index');

 
            }
            elseif($res=='ERROR_IN_IMPORT'){
              DB::rollback();
            Log::debug(__CLASS__ . "::" . __METHOD__ . "ERROR_IN_IMPORT DB--ROLLBACK");
            Log::error(__CLASS__ . "::" . __METHOD__ . "ERROR_IN_IMPORT DB--ROLLBACK");
             Session::flash('status', __('Error:BOQ import data not saved successfully'));

            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ProjectController@index');

            }
            else {
              DB::commit();
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ProjectController@index');

            Log::debug(__CLASS__ . "::" . __METHOD__ . "DB-- COMMIT");
            }
 
        } catch (Exception $e) {
             DB::rollback();
            Log::error('Exception while storing project :: ', $e->getMessage());
            Session::flash('status', __('Error:Data not saved successfully'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ProjectController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
            $row_data = Project::where('id', $id)->first();

            $site_id = $row_data['site_id'];
            $org_id = $row_data['organization_id'];
            $site_engineer_user_id = $row_data['site_engineer_user_id'];
            $client_user_id = $row_data['client_user_id'];
            $tender_doc_id = $row_data['tender_doc'];
            $schedule_pdf_id = $row_data['schedule_pdf'];

            $customer = OrganizationService::retrieveOrganization();
            $site = SiteService::retrieveSites();
            //$user_site_eng = UserService::retriveSiteEng();
            //$user_client = UserService::retriveClient();
            $user_site_eng = self::getSiteUsers();
            $user_client = self::getClientUsers($org_id);
            $boq_list = BoqService::getBoqList($id);
           // $site_engineer_user_id = ProjectService::getProjectUsers($id,'Site Engineer');

            if($tender_doc_id>0){
            $tender_doc_data = DocumentService::getDocumentDetails($tender_doc_id,'tender_doc');
            $tender_doc_path = "\storage\\".$tender_doc_data['path'];
            }else {
            $tender_doc_path = '';
            }

            if($schedule_pdf_id>0){
            $schedule_pdf_data = DocumentService::getDocumentDetails($schedule_pdf_id,'project_doc');
            $schedule_pdf_path = "\storage\\".$schedule_pdf_data['path'];
            }else {
            $schedule_pdf_path = '';
            }

            // $site_engineer_user_data = User::find($row_data['site_engineer_user_id']);
            // $client_user_data = User::find($row_data['client_user_id']);

            // $site_engineer_user_name = $site_engineer_user_data['first_name'].' '.$site_engineer_user_data['last_name'];

            // $client_user_name = $client_user_data['first_name'].' '.$client_user_data['last_name'];
           // $site_engineer_user_id= $row_data['site_engineer_user_id'];
            $client_user_id= $row_data['client_user_id'];

            Log::error($row_data);
            Log::error($site_engineer_user_id);
            Log::error($client_user_id);
            return view('project.view_project', compact('row_data','site','user_site_eng','user_client', 'site_id', 'site_engineer_user_id', 'client_user_id', 'boq_list','customer','org_id','tender_doc_path','schedule_pdf_path'));


            // return view('project.edit_project', compact('row_data','site','user_site_eng','user_client', 'site_id', 'site_engineer_user_id', 'client_user_id'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = Project::where('id', $id)->first();

            $site_id = $row_data['site_id'];
            $org_id = $row_data['organization_id'];
            $site_engineer_user_id = $row_data['site_engineer_user_id'];
            $client_user_id = $row_data['client_user_id'];
            $tender_doc_id = $row_data['tender_doc'];
            $schedule_pdf_id = $row_data['schedule_pdf'];

            $customer = OrganizationService::retrieveOrganization();
            $site = SiteService::retrieveSites();
            //$user_site_eng = UserService::retriveSiteEng();                   
            //$user_client = UserService::retriveClient(); 
            $user_site_eng = self::getSiteUsers();
            $user_client = self::getClientUsers($org_id);
            $boq_list = BoqService::getBoqList($id);
            //multiple site engg commented
            //$site_engineer_user_id = ProjectService::getProjectUsers($id,'Site Engineer');
            
            if($tender_doc_id>0){
            $tender_doc_data = DocumentService::getDocumentDetails($tender_doc_id,'tender_doc');
            $tender_doc_path = "\storage\\".$tender_doc_data['path'];
            }else {
            $tender_doc_path = '';
            }

            if($schedule_pdf_id>0){
            $schedule_pdf_data = DocumentService::getDocumentDetails($schedule_pdf_id,'project_doc');
            $schedule_pdf_path = "\storage\\".$schedule_pdf_data['path'];
            }else {
            $schedule_pdf_path = '';
            }

            // dd($user_client);
            return view('project.edit_project', compact('row_data','site','user_site_eng','user_client', 'site_id', 'site_engineer_user_id', 'client_user_id', 'boq_list','customer','org_id','tender_doc_path','schedule_pdf_path'));

        } catch (Exception $e) {
           DB::rollback();
            Log::error('Exception while updating project :: ', $e->getMessage());
            Session::flash('status', __('Error:Data not saved successfully'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ProjectController@index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {

 Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $this->authorize('Edit Projects', 'Edit Projects');
       DB::beginTransaction();
        try {
            $request['name'] = ucwords(strtolower(trim($request['name'])));

            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('Project', '$project', $id);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                   return redirect('projects/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
         
           
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
                $data['request'] = $request;

      
               ProjectService::updateData($data);
            $data['inserted_project_id'] = $id;
            $data['route_name'] = 'update';
            $res = null;
            if(isset($data['imported_boq_file'])) {
              $res = self::verifyImporBoqtData($data);
             
            }

          
           if ($res=='COLS_TITLE_MISMATCH') {
               DB::rollback();
            Log::debug(__CLASS__ . "::" . __METHOD__ . "COLS_TITLE_MISMATCH DB--ROLLBACK");
            Log::error(__CLASS__ . "::" . __METHOD__ . "COLS_TITLE_MISMATCH DB--ROLLBACK");
             Session::flash('status', __('Error:Please check BOQ import excel format.'));
              return redirect()->action('ProjectController@index');

 
            }
            elseif($res=='ERROR_IN_IMPORT'){
              DB::rollback();
            Log::debug(__CLASS__ . "::" . __METHOD__ . "ERROR_IN_IMPORT DB--ROLLBACK");
            Log::error(__CLASS__ . "::" . __METHOD__ . "ERROR_IN_IMPORT DB--ROLLBACK");
             Session::flash('status', __('Error:BOQ import data not saved successfully'));

            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ProjectController@index');

            }
            else {
              DB::commit();
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ProjectController@index');

            Log::debug(__CLASS__ . "::" . __METHOD__ . "DB-- COMMIT");
            }

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }   

      }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Delete Projects', 'Delete Projects');

            $deleted = ProjectService::deleteProject($id);
            if ($deleted) {
                Session::flash('status', 'Project deleted successfully');
            } else {
                Session::flash('status', 'Project cant be deleted as its BOQ items exists');
            }
            // $path_array = explode('/', action('ProjectController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'projects';

        } catch (Exception $e) {
            Log::error('Exception while deleting project ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }

//// -------------------------- import code
    public function verifyImporBoqtData($data) {

            $check = ProjectService::verifyBoqImportData($data);
            $keys = array();
            //dd($check=='ERROR_IN_IMPORT');
            if ($check != 'SUCCESS') {

                if ($check=='ERROR_IN_IMPORT') {
                    //Session::flash('status', __('Error in importing file'));
                    return 'ERROR_IN_IMPORT';
                } 
                if ($check=='COLS_TITLE_MISMATCH') {
                    //Session::flash('status', __('Error in importing file'));
                    return 'COLS_TITLE_MISMATCH';
                }
                foreach ($check as $key => $value) {
                     $keys =  array_keys($value);  
                    break;
                }

                  ob_end_clean();
                  ob_start();
                $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
               parent::downloadSingleSheetExcelReport("Boq_Import_Error_File" . $timeNow, "Invalid Records", "Invalid Records", "Invalid Records", $keys, $check);

                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            }
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }


//------------------- export code
      public function exportBoq($id, Request $request)
    {

         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
         $boq_data = BoqService::getBoqDataForExport($id);
         if(count($boq_data)>1) { 

            $project_data = Project::where('id', $id)->first();
            $project_name = $project_data['name'];

         $result = self::WriteDataDownloadExcel($boq_data,$project_name);
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

    }


public static function WriteDataDownloadExcel($data,$project_name)
{
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug('*************** WRITE DATA IN EXCEL ****************');
            $keys = array();

        // extract key

            $row_data=$data->toArray();

           foreach ($row_data as $key => $value) {
            foreach ($value as $key1 => $value1) {
           
                array_push($keys, $key1);
            }
            break;
        }

        $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
        $filename = $project_name. "-BOQ Data-" . $timeNow;
        $basec = new BaseController();
         ob_end_clean();
         ob_start();       
        $basec->downloadSingleSheetExcelReport($filename, "Records 1", "Records2", "Records3", $keys, $row_data); 

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
}

public static function deleteBoq($id)
{
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(" delete function in project controller started");
        try {
            //$this->authorize('Delete Projects', 'Delete Projects');

            $deleted = BoqService::deleteBoq($id);
            if ($deleted) {
                Session::flash('status', 'Boq deleted successfully');
            } else {
                Session::flash('status', 'Boq cant be deleted as its users exist');
            }
            // $path_array = explode('/', action('ProjectController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'projects';

        } catch (Exception $e) {
            Log::error('Exception while deleting project ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }

}
// CREATED BY DHRUVI FOR GETTING  ROLE USERS COMPANY WISE

 public function getClientUsers($org_id) {
    $org = Organization::find($org_id);
        $org_party_id= $org['party_id'];   
        $people = Person::select('id','party_id')->where('customer_party_id', $org_party_id)->get();  
       $cuser = array();
       $count =0;
        foreach ($people as $key => $value) {
           $user_party_id = $value['party_id'];

            $users = User::select('id','first_name', 'email')->whereHas(
                'roles', function($q){
                    $q->where('name', 'Client');
                }
            )->where('party_id', $user_party_id)->get();
           
           if(count($users) >0){
            $cuser[$count]['id'] = $users[0]['id'];
            $cuser[$count]['first_name'] = $users[0]['first_name'];
            $cuser[$count]['email'] = $users[0]['email'];
            $count++;
          }
             
        }
        return $cuser;
    }


 public function getSiteUsers() {
// get only site engineer user from tenant company
  $tenant_id = 1;
    $org = Organization::find($tenant_id);

        $org_party_id = $org['party_id'];   
        $people = Person::select('id','party_id')->where('customer_party_id', $org_party_id)->get();  
       $cuser = array();
       $count=0;
        foreach ($people as $key => $value) {
           $user_party_id = $value['party_id'];
            $users= User::select('id','first_name', 'email')->whereHas(
                'roles', function($q){
                    $q->where('name', 'Site Engineer');
                }
            )->where('party_id', $user_party_id)->get();
            if(count($users) >0){

            $cuser[$count]['id'] = $users[0]['id'];
            $cuser[$count]['first_name'] = $users[0]['first_name'];
            $cuser[$count]['email'] = $users[0]['email'];
            $count++;
          }
             
        }
        return $cuser;
    }

public static function getCompanyProjects($id) {
       $data = Project::select('id','name')->where('organization_id', $id)->get();  

        return $data;
    }
public static function retriveSiteLocation($id) {
       $site_location = SiteService::retriveSiteLocation($id);
       return $site_location;
    }
 
public static function changeStatusToStarted($id)
{
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $changed = BoqService::changeStatus($id,'started');
            if ($changed) {
                Session::flash('status', 'Boq status successfully changed');
            } else {
                Session::flash('status', 'Error while changing status');
            }
            // $path_array = explode('/', action('ProjectController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'projects';

        } catch (Exception $e) {
            Log::error('Exception while changing status ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }

}

public static function changeStatusToCompleted($id)
{
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $changed = BoqService::changeStatus($id,'completed');
            if ($changed) {
                Session::flash('status', 'Boq status successfully changed');
            } else {
                Session::flash('status', 'Error while changing status');
            }
            // $path_array = explode('/', action('ProjectController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'projects';

        } catch (Exception $e) {
            Log::error('Exception while changing status ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }

}

public static function retrieveProjectForFilter() {
      $project = ProjectService::retrieveProject();
       return $project;
  }
public function retrieveClient() {
    return OrganizationService::retrieveOrganization();
  }
}
