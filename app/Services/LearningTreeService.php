<?php

// cssir

namespace App\Services;

//Custom imports
use App\Models\ArtefactType;
use App\Models\Configuration;
use App\Models\Document;
use App\Models\LearningTree;
use App\Models\LearningTreeColor;
use App\Models\Topic;
use App\Services\TenantService;
use Config;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use vnnogile\DMS\controllers\DocumentController;
use vnnogile\DMS\services\DmsService;

class LearningTreeService {
	//Gloabal variable

	// public $related_child_topics = array();

	// for adding learning tree as node
	var $old_lt_id;
	var $new_lt_id;
	var $loggedInUserId;
	var $parent_topic_color;
	var $storage_utility_class_name;
	private static $filePath = "lt/";
	// private static $vc_delete_array=[];
	//////////////////////////////////// Learning tree functions\\\\\\\\\\\\\\\\\
	public static function saveLearningTree($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($data['learning_tree_hidden_id']);
		if ($data['learning_tree_hidden_id'] != "tesseract") {
			Log::debug('555555555555');
			$learning_tree_id = self::updateLearningTree($data);
			return $learning_tree_id;
			return 2;
		}

		$learning_tree_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['tenantPartyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			// learning_tree table
			$lt = new LearningTree;

			$lt->name = $data['lt_name'];
			$lt->learning_tree_code = $data['lt_code'];
			$lt->description = $data['lt_description'];
			$lt->tenant_party_id = $data['tenantPartyId'];

			$lt->created_by = $data['loggedInUserId'];
			$lt->updated_by = $data['loggedInUserId'];

			$lt->save();
			$learning_tree_id = $lt->id;
			Log::debug($learning_tree_id);

			// creating returning data
			$return['version'] = 'old';
			$return['name'] = $data['lt_name'];
			$return['id'] = $learning_tree_id;
			return $return;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $learning_tree_id;
	}

	public static function updateLearningTree($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$learning_tree_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['tenantPartyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			////checking usage_count
			$lt_data = LearningTreeService::getLearningTreeData($data['learning_tree_hidden_id']);
			if ($lt_data['usage_count'] != 0) {

				$lt_data['tenantPartyId'] = $data['tenantPartyId'];
				$lt_data['loggedInUserId'] = $data['loggedInUserId'];

				$new_version_lt_id = LearningTreeService::createNewVersionLearningTree($lt_data);

				// creating returning data
				$return['version'] = 'new';
				$return['name'] = $data['lt_name'];
				$return['id'] = $new_version_lt_id;
				return $return;
			}

			///updating learning tree
			$program_id = LearningTree::where('id', $data['learning_tree_hidden_id'])->update(['learning_tree_code' => $data['lt_code'], 'name' => $data['lt_name'], 'description' => $data['lt_description'], 'updated_by' => $data['loggedInUserId']]);

			// creating returning data
			$return['version'] = 'old';
			$return['name'] = $data['lt_name'];
			$return['id'] = $data['learning_tree_hidden_id'];
			return $return;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		Log::debug($learning_tree_id);

		return $learning_tree_id;
	}

	public static function getLearningTreeData($id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$data = LearningTree::find($id);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $data;
	}

	public static function createNewVersionLearningTree($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$learning_tree_id = DB::transaction(function () use ($data) {

			// changing usage count in orgiginal learning tree table
			// LearningTree::where('id',$data['id'])->update(['usage_count'=>'0']);

			// learning_tree table
			$lt = new LearningTree;

			$lt->name = $data['name'];
			$lt->learning_tree_code = $data['learning_tree_code'];
			$lt->description = $data['description'];
			$lt->version = $data['version'] + 1;

			$lt->is_active = $data['is_active'];

			// $lt->tenant_party_id= $data['tenantPartyId'];
			$lt->created_by = $data['created_by'];
			$lt->updated_by = $data['loggedInUserId'];

			$lt->save();
			$learning_tree_id = $lt->id;
			Log::debug($learning_tree_id);

			/////copying related topics in database
			$topics = Topic::where('container_learning_tree_id', $data['id'])->get();
			Log::debug('777777777');
			// Log::debug(empty($topics));

			if (isset($topics[0])) {
				// first level
				foreach ($topics as $topic_key => $topic_data) {

					if ($topic_data['parent_topic_id'] == null) {
						//save in table
						$new_topic_id = self::createNewTopic($topic_data, $learning_tree_id, $data, 0);

						//creating array
						$parent_topic_ids_array[$topic_data['id']] = $new_topic_id;

						//removing this key from array
						unset($topics[$topic_key]);

					}
				}

				$check = 1;
				while ($check == 1) {

					$returned_array = self::createChildTopicsByLevel($parent_topic_ids_array, $topics, $learning_tree_id, $data);
					$parent_topic_ids_array = $returned_array['next_level'];
					$topics = $returned_array['topics'];
					$check = $returned_array['check'];
					Log::debug($parent_topic_ids_array);
					Log::debDug("22222222" . $topics);
				}
			} //end of if

			return $learning_tree_id;

		});
		return $learning_tree_id;

	}

	public static function createChildTopicsByLevel($parent_topic_ids_array, $topics, $learning_tree_id, $data) {
		////// topic table next level
		$next_level = array();
		$check = 0;
		foreach ($topics as $topic_key => $topic_data) {

			foreach ($parent_topic_ids_array as $old_topic_id => $new_topic_id) {
				if ($topic_data['parent_topic_id'] == $old_topic_id) {

					// save new in table with new parent topic id
					$new_topic_id = self::createNewTopic($topic_data, $learning_tree_id, $data, $new_topic_id);

					//creating array
					$next_level[$topic_data['id']] = $new_topic_id;
					Log::debug($next_level);

					$check = 1;
					//removing this key from ---topics array
					unset($topics[$topic_key]);
					//break
					break;
				}
			}
		}

		$array['topics'] = $topics;
		$array['next_level'] = $next_level;
		$array['check'] = $check;
		return $array;
	}

	public static function createNewTopic($topic_data, $learning_tree_id, $data, $parent_topic_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$topic = new Topic;

		$topic->name = $topic_data['name'];
		$topic->description = $topic_data['description'];

		$topic->available_from = $topic_data['available_from'];
		$topic->available_to = $topic_data['available_to'];
		$topic->sequence = $topic_data['sequence'];
		$topic->direction = $topic_data['direction'];
		$topic->expected_duration = $topic_data['expected_duration'];

		if ($parent_topic_id != 0) {
			$topic->parent_topic_id = $parent_topic_id;
		}

		$topic->container_learning_tree_id = $learning_tree_id;
		$topic->details = $topic_data['details'];
		// $topic->artefact_type_id=$topic_data['artefact_type_id'];
		$topic->critical = $topic_data['critical'];
		$topic->alert_before_nodes = $topic_data['alert_before_nodes'];

		$topic->created_by = $topic_data['created_by'];
		$topic->updated_by = $data['loggedInUserId'];

		$topic->save();

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished" . $topic->id);

		return $topic->id;

	}
////////////////////////////////////// learning tree function ends\\\\\\\\\\\\\\\\\\\\

////////////////////////    learning tree configration(topics) function starts  \\\\\\\\\\\\\\\\\
	public static function saveTopic($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		// checking if save or update
		if ($data['hidden_flag'] != "") {
			$topic_id = self::updateTopic($data);
			return $topic_id;
		}

		$topic_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			/// saving topic

			$topic = new Topic;

			$topic->name = $data['topic_name'];
			$topic->description = $data['topic_description'];
			$topic->expected_duration = $data['expected_duration'];

			// for parent topic
			if ($data['parent_topic_id'] != 0) {
				// finding id of parent node
				$topic->parent_topic_id = $data['parent_topic_id'];

				$parent_topic = Topic::where('id', $data['parent_topic_id'])->first();
				$topic->color = $parent_topic['color'];
			} else {
				// finding sequence for tab
				$last_tab_seq = Topic::where(['container_learning_tree_id' => $data['containerLearningTree'], 'parent_topic_id' => null])->max('sequence');

				if (!$last_tab_seq) {
					//
					$sequence = 600;
				} else {
					//
					$i = (int) $last_tab_seq / 600;
					if ((int) $last_tab_seq % 600 == 0) {
						$sequence = 600 * ($i + 1);
					} else {
						$sequence = 600 * ($i + 2);
					}
				}
				$data['sequence'] = $sequence;

				/// adding color new way
				$lastTabTopic = Topic::where(['container_learning_tree_id' => $data['containerLearningTree'], 'parent_topic_id' => null])->orderBy('sequence', 'desc')->first();
				$total_color = LearningTreeColor::all();
				$lastTabColor = $total_color->where('color', $lastTabTopic['color'])->first();

				if ($lastTabColor['id'] == count($total_color)) {
					//
					$currentTabColorId = 1;
				} else {
					$currentTabColorId = $lastTabColor['id'] + 1;
				}

				$color = $total_color->where('id', $currentTabColorId)->first();
				$topic->color = $color['color'];

				/// adding color old way
				// $total_nodes = Topic::where(['container_learning_tree_id' => $data['containerLearningTree'], 'parent_topic_id' => null])->get();

				// $total_color = LearningTreeColor::all();
				// $total_color_count = count($total_color) + 1;

				// $color_rem = (count($total_nodes) + 1) % $total_color_count;
				// $color_div = (count($total_nodes) + 1) / $total_color_count;
				// $color_id = $color_rem + (int) $color_div;

				// $color = LearningTreeColor::select('color')->where('id', $color_id)->first();
				// $color = $color->color;
				// $topic->color = $color;

				// setting for topics_to_complete
				$data['topics_to_complete'] = 0;
			}

			//
			$topic->sequence = $data['sequence'];

			// creating json for details
			// prevent for learning tree also
			if ($data['entity_type'] != 'MajorNode' && $data['entity_type'] != 'LearningTree') {

				//for virtual class room
				if ($data['entity_type'] == 'Virtual Classroom') {
					//
					//$details_array['moderators'] = [];
					//$details_array['facilitators'] = [];
					Log::debug('in VirtualClassRoom section');

					//
				} elseif ($data['entity_type'] == 'Assignment') {

					//checking type of action
					$action = $data['activityAction'];
					$details_array['activityAction'] = $action;
					$file = [];
					$submitType = 'submit';
					if ($action == 'links' || $action == 'editDoc') {
						if (isset($data['assignment_file'])) {
							$assignment_artefact = ArtefactType::where('name', 'Assignment')->first();
							$paths = DocumentController::getArtefactTypePaths($assignment_artefact['id']);
							$Info = self::uploadFile($data, $paths);

							foreach ($Info['uploadedFileNames']['assignment_file'] as $key => $value) {
								//
								$name = $Info['originalFileNames']['assignment_file'][$key];
								// $file[] = ['name' => $name, 'path' => $paths['temp_path'] . $value];
								$file['name'] = $name;
								$file['path'] = $paths['temp_path'] . $value;
							}
						}
					} elseif ($action = 'file') {
						if ($data['submitType'] == 'submit') {
							$submitType = 'submit';
						} else {
							$submitType = 'save';
						}
					}
					//
					$details_array['files'] = $file;
					$details_array['submitType'] = $submitType;

				} else {
					//
					if (isset($data['entity_id'])) {

						$details_array['entity_name'] = $data['entity_name'];
						$details_array['entity_id'] = $data['entity_id'];
						$details_array['entity_url'] = $data['entity_url'];

						$details_array['entity_duration'] = $data['entity_duration'];
						$details_array['entity_size'] = $data['entity_size'];
						$details_array['entity_description'] = $data['entity_description'];

						$details_array['entity_pages'] = $data['entity_pages'];
						$details_array['entity_title'] = $data['entity_title'];
						$details_array['entity_summary'] = $data['entity_summary'];
					}
				}

				$details_array['entity_type'] = $data['entity_type'];

				// getting type
				// check for document and learning tree -- no need for learning tree
				if ($data['entity_type'] == 'Pdf' || $data['entity_type'] == 'Ppt' || $data['entity_type'] == 'Word' || $data['entity_type'] == 'Exel' || $data['entity_type'] == 'Text' || $data['entity_type'] == 'Web') {
					$entity_type_name = 'Document';
				} else if ($data['entity_type'] == 'Youtube' || $data['entity_type'] == 'Vimeo') {
					$entity_type_name = 'Video';
				} else {
					$entity_type_name = $data['entity_type'];
				}

				$artefact_type = ArtefactType::where('name', $entity_type_name)->first();
				Log::debug($artefact_type['id']);
				if ($artefact_type['id'] != null) {
					$details_array['artefact_type_id'] = $artefact_type['id'];

				} else {
					$details_array['artefact_type_id'] = null;
				}

				$details_jason = json_encode($details_array);

				$topic->details = $details_jason;

				// saving icon for leaf node
				if (!empty($data['node_icon'])) {
					$topic->icon = $data['node_icon'];
				} else {
					$topic->icon = Config::get('app.learning_tree_leaf_icon');
				}

			} elseif ($data['entity_type'] == 'MajorNode') {
				//
				if ($data['topics_to_complete']) {
					$topic->topics_to_complete = $data['topics_to_complete'];
				}
				// $topic->topics_to_complete = $data['topics_to_complete'];
			}

			// $topic->artefact_type_id=$data['entity_type'];

			if (array_key_exists("critical_topic", $data)) {
				// if(isset($data['critical_topic'])){
				$topic->critical = '1';
				$topic->alert_before_nodes = $data['alert_before_nodes'];
			} else {
				$topic->critical = 0;
			}

			if (array_key_exists('is_optional', $data)) {
				//
				$topic->optional = 1;
			}

			// $topic->container_learning_tree_id=$data['hidden_learning_tree_id'];
			$topic->container_learning_tree_id = $data['containerLearningTree'];
			$topic->created_by = $data['loggedInUserId'];
			$topic->updated_by = $data['loggedInUserId'];

			if (!isset($data['node_icon'])) {
				if (isset($data['entity_id'])) {
					$doc = Document::find($data['entity_id']);
					if ($doc['document_photo']) {
						$topic->photo = $doc['document_photo'];
						$topic->photo_path = $doc['document_photo_path'];
					}
				}
			}

			$topic->save();
			$id = $topic->id;
			if (isset($data['node_icon'])) {
				self::uploadLTIcon($data, $data['containerLearningTree'], $id);
			}
			/// saving in object no need

			//// after adding lt as node
			if ($data['entity_type'] == 'LearningTree') {

				Log::debug($data['learning_tree'] . '>>>>>>>>>>>>>>>>>');

				$GLOBALS['new_lt_id'] = $data['containerLearningTree'];
				$GLOBALS['loggedInUserId'] = $data['loggedInUserId'];
				$GLOBALS['old_lt_id'] = $data['learning_tree'];
				$GLOBALS['parent_topic_color'] = $topic->color;

				// for copying photo_path and photo
				$configurations = $data['masterDataInterface']->all('Configuration');
				foreach ($configurations as $configuration) {
					$configurationsMap[$configuration->configKey] = $configuration->configValue;
				}
				$data['configurations'] = $configurationsMap;
				$class = $data['configurations']['storage_implementation_class'];
				$GLOBALS['storage_utility_class_name'] = $class;

				//saving in topic table
				self::saveChildTopicInDatabase(null, $topic->id);
			}

			/// creating object to return
			if ($data['parent_topic_id'] != 0) {
				//

				$returned_array['topic_id'] = $id;
				$returned_array['topic_name'] = $data['topic_name'];
				// $returned_array['sequence']=$data['sequence'];
				// $returned_array['entity_type']=$data['entity_type'];

				$topic_array = Topic::where(['container_learning_tree_id' => $data['containerLearningTree'], 'parent_topic_id' => $data['parent_topic_id']])->orderBy('sequence', 'asc')->get();

				$object = self::gettingTopicObject($topic_array);

				$returned_array['object'] = $object;

			} else {
				//
				if ($data['entity_type'] == 'LearningTree') {
					//
					$topic_array = Topic::where(['container_learning_tree_id' => $data['containerLearningTree'], 'parent_topic_id' => $id])->orderBy('sequence', 'asc')->get();
					$object = self::gettingTopicObject($topic_array);
				} else {
					//
					$object = self::creatingObjectForNewTab();
				}

				$returned_array['object'] = $object;
				$returned_array['sequence'] = $data['sequence'];
				$returned_array['topic_id'] = $id;
				$returned_array['topic_name'] = $data['topic_name'];

			}

			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished" . $topic->id);

			return $returned_array;
		});

		return $topic_id;

	}
	private static function uploadLTIcon($data, $learning_tree_id, $topic_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$data['masterDataInterface'] = $data['masterDataInterface'];
		//storage configurations
		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;
		$data['topic_id'] = $topic_id;
		$data['learning_tree_id'] = $learning_tree_id;
		$document_details = self::uploadLTPhoto($data);
		Log::debug($document_details);
		$topic = Topic::find($topic_id);
		if (isset($data['node_icon'])) {
			$topic->photo = $document_details['originalFileNames']['node_icon'][0];
			$topic->photo_path = "/storage/" . self::$filePath . $learning_tree_id . "/topic/" . $topic_id . "/" . $document_details['uploadedFileNames']['node_icon'][0];
		}
		$topic->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

	}

	private static function uploadLTPhoto($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->uploadFileToPublicStorage($data['request'], self::$filePath . $data['learning_tree_id'] . "/topic/" . $data['topic_id']);
	}
	private static function deleteLTPhoto($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->deleteFile(str_replace("storage", "public", $data['remove_file']));
	}
	private static function copyLTPhoto($fileName, $sourceFolder, $id, $container_learning_tree_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//
		$source_array = explode('/', $sourceFolder);

		$corrected_source_folder1 = str_replace("storage", "public", $sourceFolder);
		$corrected_source_folder = str_replace(end($source_array), '', $corrected_source_folder1);
		$destination_folder = '/public/lt/' . $container_learning_tree_id . '/topic/' . $id . '/';

		$class = $GLOBALS['storage_utility_class_name'];
		$storageUtility = new $class;
		$copy_status = $storageUtility->copyFile(end($source_array), $corrected_source_folder, $destination_folder);

		$corrected_destination_folder = str_replace("public", "storage", $destination_folder) . '' . end($source_array);

		$topic = Topic::find($id);
		$topic->photo = $fileName;
		$topic->photo_path = $corrected_destination_folder;
		$topic->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function deleteFiles($file) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($file);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return Storage::delete($file);
	}

	public static function uploadFile($data, $paths) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$upload_data['request'] = $data['request'];
		$upload_data['temp_path'] = $paths['temp_path'];
		$upload_data['storage_path'] = $paths['storage_path'];
		$upload_data['authenticatedUser'] = $data['authenticatedUser'];
		$upload_data['utilsInterface'] = $data['utilsInterface'];
		$upload_data['masterDataInterface'] = $data['masterDataInterface'];

		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$upload_data['configurations'] = $configurationsMap;

		$uploadInfo = DmsService::uploadDocument($upload_data);

		Log::debug($uploadInfo);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $uploadInfo;
	}

	public static function creatingObjectForNewTab() {

		//
		$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
		$max_cell_no = $config['config_value'];

		$object = [];
		for ($i = 1; $i <= $max_cell_no; $i++) {
			$object[$i] = '';
		}

		return $object;
	}

////// adding learning tree as topic functions starts \\\\\

	public static function saveChildTopicInDatabase($old_parent_id, $new_parent_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		// getting first level topics
		$topics = self::gettingTopicsByLevel($old_parent_id);
		Log::debug($topics);

		if (count($topics)) {

			$i = 1;
			foreach ($topics as $key => $topic_array) {

				//saving 1st topic
				$topic_id = self::saveTopicOfInsertedLtInDataBase($topic_array, $new_parent_id);

				$tt = self::saveChildUnderTab($topic_array['id'], $topic_id);

				$i++;
			}
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function saveChildUnderTab($old_parent_id, $new_parent_id) {

		// getting first level onwards topics
		$topics = self::gettingTopicsByLevel($old_parent_id);
		Log::debug($topics);

		if (count($topics)) {

			$i = 1;
			foreach ($topics as $key => $topic_array) {

				//saving 1st topic
				$topic_id = self::saveTopicOfInsertedLtInDataBase($topic_array, $new_parent_id);

				$last_cell = self::saveChildUnderTab($topic_array['id'], $topic_id);
				$i++;
			}
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function saveTopicOfInsertedLtInDataBase($topic_array, $new_parent_id) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		Log::debug($topic_array);
		Log::debug($new_parent_id);
		Log::debug('********/*/*/*/***/**/');

		$topic = new Topic;
		$topic->container_learning_tree_id = $GLOBALS['new_lt_id'];

		$topic->name = $topic_array['name'];
		$topic->description = $topic_array['description'];
		$topic->sequence = $topic_array['sequence'];
		// $topic->icon = $topic_array['icon'];
		// $topic->color = $topic_array['color'];
		$topic->color = $GLOBALS['parent_topic_color'];

		$topic->expected_duration = $topic_array['expected_duration'];
		$topic->topics_to_complete = $topic_array['topics_to_complete'];
		$topic->parent_topic_id = $new_parent_id;

		if ($topic_array['details'] != null) {
			//
			$topic->details = $topic_array['details'];
		}

		if ($topic_array['critical'] != null) {
			$topic->critical = $topic_array['critical'];
			$topic->alert_before_nodes = $topic_array['alert_before_nodes'];
		}

		// $topic->container_learning_tree_id = $GLOBALS['new_lt_id'];
		$topic->created_by = $GLOBALS['loggedInUserId'];
		$topic->updated_by = $GLOBALS['loggedInUserId'];

		$topic->save();
		$topic_id = $topic->id;
		//setting icon
		if ($topic_array['photo'] != null) {
			self::copyLTPhoto($topic_array['photo'], $topic_array['photo_path'], $topic_id, $GLOBALS['new_lt_id']);
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		return $topic->id;
	}

	public static function gettingTopicsByLevel($parent_topic) {
		//
		$topics = Topic::where(['container_learning_tree_id' => $GLOBALS['old_lt_id'], 'parent_topic_id' => $parent_topic])->orderBy('sequence', 'asc')->get();

		return $topics;
	}

/////////////////////////////////////////////////
	public static function updateTopic($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//getting data
		$topic_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->party_id;
			}
			$data['masterDataInterface'] = $data['masterDataInterface'];
			//storage configurations
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;
			if (isset($data['node_icon'])) {
				if (isset($data['photo_path'])) {
					$data['remove_file'] = $data['photo_path'];
					self::deleteLTPhoto($data);
				}
			}
			if (isset($data['node_icon'])) {
				self::uploadLTIcon($data, $data['containerLearningTree'], $data['id']);
			} else {
				//also check if uploaded photo_path does'nt belong to 'topic/' -- not

				if (isset($data['entity_id'])) {
					$top = Topic::find($data['id']);
					Log::debug('checking');
					if (!$top['photo']) {
						Log::debug('checking1');
						$doc = Document::find($data['entity_id']);
						if ($doc['document_photo']) {
							Log::debug('checking2');
							$top->photo = $doc['document_photo'];
							$top->photo_path = $doc['document_photo_path'];
							$top->save();
						}
					}
				}
			}

			///updating topic

			$containerLearningTree = $data['containerLearningTree'];

			if ($data['entity_type'] != 'MajorNode') {

				//for virtual class room
				if ($data['entity_type'] == 'Virtual Classroom') {
					//
					//$details['moderators'] = [];
					//$details['facilitators'] = [];
					Log::debug('in VirtualClassRoom section');

				} elseif ($data['entity_type'] == 'Assignment') {

					// checking action
					$action = $data['activityAction'];
					$details['activityAction'] = $action;
					$file = [];
					$submitType = 'submit';

					$topic = Topic::where('id', $data['id'])->first();
					$old_details_array = json_decode($topic['details'], true);

					if ($action == 'links' || $action == 'editDoc') {
						if (isset($data['assignment_file'])) {
							$assignment_artefact = ArtefactType::where('name', 'Assignment')->first();
							$paths = DocumentController::getArtefactTypePaths($assignment_artefact['id']);
							$Info = self::uploadFile($data, $paths);

							foreach ($Info['uploadedFileNames']['assignment_file'] as $key => $value) {
								$name = $Info['originalFileNames']['assignment_file'][$key];
								$file['name'] = $name;
								$file['path'] = $paths['temp_path'] . $value;
							}
							//delete older file if exist
						} //new changes for activity
						else {
							$file = $old_details_array['files'];
						}
					} elseif ($action = 'file') {
						//
						if ($data['submitType'] == 'submit') {
							$submitType = 'submit';
						} else {
							$submitType = 'save';
						}

					} else {
						// check if previous action was link .. then delete
						if ($old_details_array['activityAction'] = 'links') {
							// delete old file
						}
					}
					// checking earliear ones
					// $topic = Topic::where('id', $data['id'])->first();
					// $details_array = json_decode($topic['details'], true);

					// Log::debug($details_array['files']);
					// foreach ($details_array['files'] as $key => $value) {
					// 	//
					// 	Log::debug($value);
					// 	$check = implode('', explode('.', $value['name']));
					// 	if (isset($data[$check])) {
					// 		Log::debug($value);

					// 		$file[] = ['name' => $value['name'], 'path' => $value['path']];
					// 	} else {

					// 		// delete files
					// 		self::deleteFiles($value['path']);

					// 	}
					// }

					Log::debug($file);
					$details['submitType'] = $submitType;
					$details['files'] = $file;

				} else {

					if (isset($data['entity_id'])) {
						$details['entity_name'] = $data['entity_name'];
						$details['entity_id'] = $data['entity_id'];
						$details['entity_url'] = $data['entity_url'];

						$details['entity_duration'] = $data['entity_duration'];
						$details['entity_size'] = $data['entity_size'];
						$details['entity_description'] = $data['entity_description'];

						$details['entity_pages'] = $data['entity_pages'];
						$details['entity_title'] = $data['entity_title'];
						$details['entity_summary'] = $data['entity_summary'];
					}
				}

				$details['entity_type'] = $data['entity_type'];

				// getting type
				if ($data['entity_type'] == 'Pdf' || $data['entity_type'] == 'Ppt' || $data['entity_type'] == 'Word' || $data['entity_type'] == 'Exel' || $data['entity_type'] == 'Text' || $data['entity_type'] == 'Web') {
					$entity_type_name = 'Document';
				} else if ($data['entity_type'] == 'Youtube' || $data['entity_type'] == 'Vimeo') {
					$entity_type_name = 'Video';
				} else {
					$entity_type_name = $data['entity_type'];
				}
				$artefact_type = ArtefactType::where('name', $entity_type_name)->first();
				Log::debug($artefact_type['id']);
				if ($artefact_type['id'] != null) {
					$details['artefact_type_id'] = $artefact_type['id'];

				} else {
					$details['artefact_type_id'] = null;
				}

				$details_jason = json_encode($details);
			} else {
				$details_jason = NULL;
				if (!$data['topics_to_complete']) {
					$data['topics_to_complete'] = 0;
				}
			}

			Log::debug($details_jason . 'rrrrrr');

			//for critical nodes
			if (array_key_exists("critical_topic", $data)) {
				$critical = '1';
				$nodes_before = $data['alert_before_nodes'];
			} else {
				$critical = '0';
				// $nodes_before=$data['alert_before_nodes'];
				$nodes_before = 0;
			}
			if (array_key_exists('is_optional', $data)) {
				//
				$optional = '1';
			} else {
				$optional = '0';
			}

			Log::debug($critical);

			// updating icon
			// 'icon' => $data['node_icon']

			$topic_id = Topic::where('id', $data['id'])->update(['name' => $data['topic_name'], 'description' => $data['topic_description'], 'expected_duration' => $data['expected_duration'], 'details' => $details_jason, 'critical' => $critical, 'optional' => $optional, 'alert_before_nodes' => $nodes_before, 'topics_to_complete' => $data['topics_to_complete'], 'updated_by' => $data['loggedInUserId']]);

			// creating returned data -- check for both tab and child  child remianing
			$returned_array['topic_id'] = $data['id'];
			$returned_array['topic_name'] = $data['topic_name'];
			if (isset($data['sequence'])) {
				$returned_array['sequence'] = $data['sequence'];
			}
			$returned_array['entity_type'] = $data['entity_type'];

			return $returned_array;
		});

		return $topic_id;

	}

////////  delete topic under configuration starts \\\\\\\\\\\\\\\\\\\\
	public static function deleteTopic($id, $containerLearningTree, $tab, $masterDataInterface) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($id . ' ' . $containerLearningTree);

		$object_details = DB::transaction(function () use ($id, $containerLearningTree, $tab, $masterDataInterface) {

			// deleting from database
			self::deletChildUnderTab($id);
			// deleting tab from topic database
			self::deleteFromDatabase($id);

			if ($tab == 0) {
				// its node
				// container learning tree is parent topic
				$topic_array = Topic::where('parent_topic_id', $containerLearningTree)->orderBy('sequence', 'asc')->get();

				$returned_array = self::gettingTopicObject($topic_array);

			} else {
				// its tab
				$returned_array = self::gettingTabObject($containerLearningTree, $tab);

				// saving svg for tabs
			}

			return $returned_array;
		});
		return $object_details;
	}

	public static function deleteFromDatabase($id) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$u = Topic::where('id', $id)->update(['deleted_by' => auth()->user()->id]);
		$d = Topic::where('id', $id)->delete();

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished" . $d);
	}

	public static function deletChildUnderTab($parent_id) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$child_topics = Topic::where('parent_topic_id', $parent_id)->get();

		if ($child_topics) {

			foreach ($child_topics as $key => $topic_array) {
				//
				self::deletChildUnderTab($topic_array['id']);
			}
			// deleting all child topics
			//
			$u = Topic::where('parent_topic_id', $parent_id)->update(['deleted_by' => auth()->user()->id]);
			$d = Topic::where('parent_topic_id', $parent_id)->delete();
		} else {
			// getting ids for moderator check and photo check
			// array_push(self::$vc_delete_array, $parent_id);
		}
	}

	/////

	public static function getTopicForEdit($id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//
		$topic = Topic::where('id', $id)->first();
		Log::debug($topic);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $topic;
	}

	public static function getParentStructure($containerLearningTree, $id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$check = DB::transaction(function () use ($containerLearningTree, $id) {

			//
			$topic_array = Topic::where(['container_learning_tree_id' => $containerLearningTree, 'parent_topic_id' => $id])->orderBy('sequence', 'asc')->get();

			$object = self::gettingTopicObject($topic_array);

			Log::debug($object);
			//
			return $object;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $check;
	}

	public static function gettingTopicObject($topic_array) {

		//
		$object = [];
		$i = 0;
		foreach ($topic_array as $key => $topic_data) {
			//
			$i++;

			$object[$i]['topic_id'] = $topic_data['id'];
			$object[$i]['topic_name'] = $topic_data['name'];
			$object[$i]['sequence'] = $topic_data['sequence'];

			// $object[$i]['entity_type']=$topic_data['artefact_type_id'];
			if ($topic_data['details'] != null) {
				//
				$details = json_decode($topic_data['details'], true);
				// print_r($details);
				$object[$i]['entity_type'] = $details['entity_type'];
			} else {
				$object[$i]['entity_type'] = 'MajorNode';
			}
		}

		$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
		$max_cell_no = $config['config_value'];

		if ($i < $max_cell_no) {
			//
			for ($j = $i + 1; $j <= $max_cell_no; $j++) {
				$object[$j] = '';
			}
		}

		// // completing row by 5 div
		// if ($end_cell % 5 != 0 && $end_cell > 11) {
		//     //
		//     $remainder= $end_cell % 5;

		//     for($i=1; $i <= (5-$remainder); $i++){
		//         $GLOBALS['cellObject'][$end_cell + $i]=[];
		//      }
		// }
		return $object;
	}

	public static function getUpdateStructure($containerLearningTree) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$check = DB::transaction(function () use ($containerLearningTree) {

			//
			// $topic_tab_array=Topic::where(['container_learning_tree_id'=>$containerLearningTree,'parent_topic_id'=>null])->orderBy('sequence','asc')->get();

			$object = self::gettingTabObject($containerLearningTree, 1);

			Log::debug($object);
			//
			return $object;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $check;
	}

	public static function gettingTabObject($containerLearningTree, $tab) {
		//
		$topic_tab_array = Topic::where(['container_learning_tree_id' => $containerLearningTree, 'parent_topic_id' => null])->orderBy('sequence', 'asc')->get();

		$i = 0;
		$tab_object = [];
		foreach ($topic_tab_array as $key => $topic_data) {
			//
			$i++;
			//
			$tab_object[$i]['topic_id'] = $topic_data['id'];
			$tab_object[$i]['topic_name'] = $topic_data['name'];
			$tab_object[$i]['sequence'] = $topic_data['sequence'];
		}

		Log::debug($topic_tab_array);
		$object = [];

		if (count($topic_tab_array)) {
			$first_tab_id = $tab_object[$tab]['topic_id'];
			// print_r($topic_tab_array);
			// exit();

			$topic_array = Topic::where('parent_topic_id', $first_tab_id)->orderBy('sequence', 'asc')->get();
			$object = self::gettingTopicObject($topic_array);
		}

		$returned_array['tabs_object'] = $tab_object;
		$returned_array['first_tab_object'] = $object;

		return $returned_array;
	}

/////////////////////////////// learning tree for program  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

}
