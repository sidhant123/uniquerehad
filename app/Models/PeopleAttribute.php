<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 19:48:19 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PeopleAttribute
 * 
 * @property int $id
 * @property int $people_id
 * @property string $attr_name
 * @property string $attr_value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Person $person
 *
 * @package App\Models
 */
class PeopleAttribute extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'people_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'people_id',
		'attr_name',
		'attr_value',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function person()
	{
		return $this->belongsTo(\App\Models\Person::class, 'people_id');
	}
}
