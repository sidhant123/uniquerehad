Create sequence manpower_contracts_seq; 

-- Table: public.manpower_contracts

-- DROP TABLE public.manpower_contracts;

CREATE TABLE public.manpower_contracts
(
  id bigint NOT NULL DEFAULT nextval('manpower_contracts_seq'::regclass),
  organization_id integer NOT NULL,
  site_id integer NOT NULL,
  service_name varchar,
  rate numeric(8,2),
  status character(10),
  document_id integer,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT manpower_contracts_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.manpower_contracts
  OWNER TO postgres;

-- ---------------------------------------------------------------------------------------------


INSERT INTO public.menus(
     id, display_name, name, sequence,  auth, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (100, 'Manpower Contracts', 'Manpower Contracts', 135,  1, '', now(), now(),  1, 1);


INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES  
    ( 'List', 'List Manpower Contracts', 140, 100,  1, now(), now(),  1, 1), 
    ( 'New Manpower Contract', 'Create Manpower Contracts', 145, 100,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020


INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (55,'Manpower Contracts', now(), now(), 1, 1),
     (56, 'List Manpower Contracts', now(), now(), 1, 1),
     (57, 'Create Manpower Contracts', now(), now(), 1, 1),
     (58, 'Show Manpower Contracts', now(), now(), 1, 1),
     (59, 'Delete Manpower Contracts', now(), now(), 1, 1),
     (60, 'Edit Manpower Contracts', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (55, 1, 55, now(), now(), 1,1),
     (56, 1, 56, now(), now(), 1,1),
     (57, 1, 57, now(), now(), 1,1),
     (58, 1, 58, now(), now(), 1,1),
     (59, 1, 59, now(), now(), 1,1),
     (60, 1, 60, now(), now(), 1,1);


INSERT INTO public.list_view_masters(
             function_name, entity_name,  
             query_columns, 
             heading_columns,
             filter_columns, 
             display_css_widths, 
             relations, replace_columns,
              created_at, updated_at, created_by, updated_by,
              filter_class_names,
              entity_namespace)
    VALUES ('List Manpower Contracts', 'ManpowerContract',
            'company_name,site_name,service_name,rate,active,id',
            'Supplier,Site,Service,Rate,Active,Action', 
             '', 
             'col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-1', 
             '', '', now(),now(),1,1, 
             'App\Utilities\ManpowerFilterUtility', 
             'App\Models\');
