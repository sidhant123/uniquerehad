<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\UtilsInterface;

/**
 * Class TenantService
 * 
 */
class TenantService
{

    public static function getTenantPartyIdForAuthUser(UtilsInterface $utilsInterface, $user)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");        
        $tenant_party=$utilsInterface->getTenantForAuthUser($user);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $tenant_party->id;
    }

    public static function getTenantForAuthUser(UtilsInterface $utilsInterface, $user)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");        
        return $utilsInterface->getTenantForAuthUser($user);
    }
}
