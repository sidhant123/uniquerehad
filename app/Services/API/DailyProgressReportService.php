<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services\API;

//Custom imports
use Illuminate\Support\Facades\Log;
use App\Models\Boq;
use App\Models\BoqItem;

/**
 * Class TenantService
 * 
 */
class DailyProgressReportService
{

    public static function getBoqHeaderList($name) {

    	Log::error('in the service function');

    	$boq_data = Boq::where('name', $name)
    	->orWhere('name', 'like', '%' . $name . '%')->get();

    	Log::error($boq_data);
    	return $boq_data;

    }


    public static function getBoqDetailsList($id) {

    	$boq_item_data = BoqItem::where('boq_id', $id)->get();

    	Log::error($boq_item_data);

    	return $boq_item_data;
    	// exit();

    }
}
