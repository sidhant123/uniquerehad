<?php

return [

    /*
   |--------------------------------------------------------------------------
   | User Auth : Login / Forget Password / Change Password etc.
   |--------------------------------------------------------------------------
   */
    'auth_success' => 'User authenticated successfully.',
    'auth_failed' => 'User authentication failed.',
    'user_not_active' => 'User is not activate now.',
    'profile_update_success' => 'Profile updated successfully.',
    'profile_update_failed' => 'Failed to update profile. Please try again.',
    'otp_mailed_success' => 'We have mailed you an OTP. Please check.',
    'otp_mailed_failed' => 'Failed to send OTP. Please try again.',
    'otp_matched_failed' => 'OTP does not matched.',
    'password_update_success' => 'Password updated successfully.',
    'password_update_failed' => 'Failed to update password. Please try again.',
    'password_not_matched' => 'Old password does not matched.',

    /*
    |--------------------------------------------------------------------------
    | Custom error messages for API and Front
    |--------------------------------------------------------------------------
    */

    'invalid_input' => 'Invalid inputs. Please check.',
    'something_wrong' => 'Something went wrong. Please try again.',
    'empty_list' => 'No :list_name found.',
    'invalid_program' => 'Invalid program specified!',
    'invalid_user' => 'Invalid user specified!',

    /*
    |--------------------------------------------------------------------------
    | Database Success / Failure messages
    |--------------------------------------------------------------------------
    */
    'db_success' => ':table :action successfully',
    'db_error' => ':table failed to :action',

];