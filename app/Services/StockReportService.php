<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Site;

/**
 * Class AddressMasterService
 *
 */
class StockReportService {
	
	  public static function retrieveStockReportQuery($siteId){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");



        $queryBuilder = DB::table('inventories')
            ->select(
                'sites.name as site_name' , 
                'sites.location' ,  
                'projects.name as project_name' ,  
                'materials.name as material_name',
                'inventories.quantity'
                // 'DB::raw('sum(inventories.quantity) as qty')',
                // 'SUM(inventories.quantity) as qty',

                //  ->selectRaw(
                // 'sites.name as site_name, sites.location as location ,projects.name as project_name, materials.name as material_name,SUM(inventories.quantity) as qty'
        )
            ->leftJoin('sites', 'sites.id', '=', 'inventories.site_id')
            ->leftJoin('projects', 'projects.id', '=', 'inventories.project_id')
            ->leftJoin('materials', 'materials.id', '=', 'inventories.material_id')
            ->where('inventories.site_id','=', $siteId)
           // ->sum('inventories.quantity')
            // ->groupBy('sites.name, sites.location, projects.name, materials.name, inventories.quantity' );
            ->orderBy('projects.name', 'asc' )
            ->orderBy('materials.name', 'asc' )
            ->orderBy('inventories.quantity', 'asc' )
            ;

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }



    public static function getSiteName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $site = Site::select('name')->where('id', '=', $Id)->first();
        return trim($site->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
