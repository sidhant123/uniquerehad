@extends('project.project')
@section('editID', '/'.$row_data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('edit_btn','hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Service')
@section('customer_id',$row_data->organization_id)
@section('name',$row_data->name)
@section('description',$row_data->description)
@section('start_date',$row_data->start_date)
@section('end_date',$row_data->end_date)
@section('site_name',$row_data->site_id)
@section('site_location',$row_data->site_location)
@section('site_eng_user_id',$row_data->site_engineer_user_id)
@section('client_user_id',$row_data->client_role_id)
@section('delay_hr',$row_data->delay_hr)
@section('importBoq')
      @if(count($boq_list)>1)

  

 
<div style="border-style:dotted; border-width: 1px;">
    {{ csrf_field() }}
  <h3 class="headings">{{__('labels.projects.edit_import_boq')}}</h3>
  <div class="row">
     <div class="col-xs-12 req">
      <div class="form-group required">
          <label for="boqFile">{{__('labels.projects.edit_file')}}</label>
          <input type="file" class="form-control import"  id="boqFile"  name="imported_boq_file" accept=".xlsx" >
        </div>
   
    <div class="form-group">
      
    <button class="btn btn-primary">
    <a href="{{ url('/projects/' . $row_data->id . '/exportBoq') }}" >{{__('labels.projects.export_file')}}</a></button>

  </div>
</div>
</div>
</div>

<p></p> 

  <h4 class="headings">{{__('labels.projects.import_boq_list')}} &emsp; 
  </h4>
<div id="boq_list">
<table id="boq_table" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>BOQ No.</th>
                <th>SrNo.</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Service</th> 
                <th class="col-xs-1">Status</th> 
                <th>Delete</th> 
       
            </tr>
        </thead>
        <tbody>
        @foreach($boq_list as $boq_val)
        <tr>
         <td>{{$boq_val->name}}</td>
         <td>{{$boq_val->srno}}</td>
          <td>{{$boq_val->description}}</td>
          <td>{{$boq_val->unit_rate}}</td>
          <td>{{$boq_val->quantity}}</td>
          <td>{{$boq_val->rate}}</td>
          <td align="right">{{$boq_val->amount}}</td>
          <td>{{$boq_val->activity_name}}</td>
         <!--  <td>
            <input type="checkbox" checked data-toggle="toggle" data-on="Completed" data-off="Started" data-onstyle="success" data-offstyle="warning" onchange ="return changeStatusToStarted('{{$boq_val->id}}','projects/changeStatusToStarted')">
          </td> -->
        @if($boq_val->status == 'completed')
          <td> 
             <input type="checkbox" checked data-toggle="toggle" data-on="Completed" data-off="Started" data-onstyle="success" data-offstyle="warning" onchange ="return changeStatus('{{$boq_val->id}}','projects/changeStatusToStarted')">
            <!-- <a href="#" title="Click here to change status started" onclick="return changeStatusToStarted('{{$boq_val->id}}','projects/changeStatusToStarted')">Completed <span class="glyphicon glyphicon-repeat"></span> </a> -->
          </td>
        @elseif($boq_val->dpr_id > 1)
          <td>
         <!--  <a href="#" title="Click here to chnage status Completed" onclick="return changeStatusToCompleted('{{$boq_val->id}}','projects/changeStatusToCompleted')">Started <span class="glyphicon glyphicon-check"></span> </a> -->
             <input type="checkbox"  data-toggle="toggle" data-on="Completed" data-off="Started" data-onstyle="success" data-offstyle="warning" onchange ="return changeStatus('{{$boq_val->id}}','projects/changeStatusToCompleted')">
          </td>
        @else 
          <td>
             <input type="checkbox" class="btn btn-secondary" data-toggle="toggle" data-on="Not Started" data-off="Not Started" disabled="disabled">
          </td>
        @endif
          <td><a href="#" onclick="return deleteRecord('{{$boq_val->id}}','projects/deleteBoq')" class="">
            <i class="action-big-vnn vnn-del" aria-hidden="true"></i></a></td>
        <tr>
     @endforeach
        </tbody>
      </table>
</div> 
         @endif             
@endsection

@push('scripts')
<!--
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
--> 
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

//multi site engg commented 
// $("#site_eng_user_id").val([{{$site_engineer_user_id}}]).trigger("change");

function deleteRecord(id,route){
  bootbox.confirm({
      message: "Do you want to delete this record?",
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
          if (result){
            deleteActualRecord(id, route);

          }
          
      }
  });
}
function changeStatus(id,route){
            deleteActualRecord(id, route);
}

function deleteActualRecord(id,route){

  var url = '/'+ route +'/'+ id;
  console.log('****route*****');
  console.log(route);
  console.log('****id*****');
  console.log(id);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  }); 
  $.ajax({
    type: "delete",
    url: url,

    success: success,
  });
  function success(data){
    var data = data.split("/");
    if(data[0] == 'deleted successfully'){
     // window.location = '/'+data[1];
     location.reload();

    }else if (data[0] == 'deleted unsuccessfull') {
      //window.location = '/'+data[1];
      location.reload();

    }
  }     
}
function errorResponse(errorResponse){
  console.log(errorResponse);
}
</script>

@endpush