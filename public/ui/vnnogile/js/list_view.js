$(document).ready(function() {
	$.getJSON('/getRowCount', new Date(), populateRowCount).error(errorResponse);
  $("table").colResizable();
});

function setRowCount(value,routeName){
	console.log(routeName);
	var url = '/'+routeName+'/?rowCount='+value;
	window.location = url;
	//localStorage.setItem('rowCount', value);
	sessionStorage.setItem('rowCount', value);
}


function populateRowCount(data){
	console.log('populateRowCount');
	$("#row_count").empty();

	$.each(data['row_count_list'], function (key, value) {

		$("#row_count").append("<option value='" + value + "'>" + value+ "</option>");

	}); 
//	if(localStorage.getItem('rowCount')){
	if(sessionStorage.getItem('rowCount')){
		//console.log("localStorage");
		console.log("sessionStorage");
//		$("#row_count").val(localStorage.getItem('rowCount'));	
		$("#row_count").val(sessionStorage.getItem('rowCount'));	
	}else{
		$("#row_count").val(data['default_row_count']);
	}  
}

function deleteRecord(id,route){
  bootbox.confirm({
      message: "Do you want to delete this record?",
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
          if (result){
            deleteActualRecord(id, route);

          }
          
      }
  });
}

function deleteActualRecord(id,route){

  var url = '/'+ route +'/'+ id;
  console.log('****route*****');
  console.log(route);
  console.log('****id*****');
  console.log(id);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  }); 
  $.ajax({
    type: "delete",
    url: url,

    success: success,
  });
  function success(data){
    var data = data.split("/");
    if(data[0] == 'deleted successfully'){
      window.location = '/'+data[1];
    }else if (data[0] == 'deleted unsuccessfull') {
      window.location = '/'+data[1];
    }
  }     
}
function errorResponse(errorResponse){
	console.log(errorResponse);
}
