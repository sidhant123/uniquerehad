<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class ExpenseTypeFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('expense_types')
                ->select(
                    'expense_types.name as name',
                    'expense_types.track_payment as track_payment',
                     'expense_types.id as id')->where('expense_types.deleted_at', null);
                                
            if ($request->has('search')) {
                $data->whereRaw("( lower(expense_types.name) like '%" . strtolower($request['search']) . "%')");
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'name':
                        $data->orderBy('expense_types.name', $request['sort']);
                        break;
                    case 'address':
                        $data->orderBy('expense_types.address', $request['sort']);
                        break;
                                     
                }
            } else {
                $data->orderBy('expense_types.id');
            }
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            // dd($data->get());
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}
