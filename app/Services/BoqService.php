<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Boq;
use App\Models\BoqItem;

/**
 * Class AddressMasterService
 *
 */
class BoqService {
	use ValidationTrait;


	public static function saveBoq($data, $project_id) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		try {
			
		
			$userId = $data['authenticatedUser']->id;

			$obj_boq = new Boq;

			$obj_boq->description = trim($data['description']);
			$obj_boq->name = $project_id."000";
			$obj_boq->project_id = $project_id;
			//$obj_boq->user_id = $userId;
			$obj_boq->boq_type = $data['boq_type'];
			$obj_boq->parent_id = $data['boq_parent_id'];
			// detail data
			$obj_boq->unit_rate = ucwords(strtolower(trim($data['unit'])));
			$obj_boq->rate = $data['rate'];
			$obj_boq->amount = $data['amount'];
			$obj_boq->quantity = $data['qty'];
			$obj_boq->activity_name = ucwords(strtolower(trim($data['service'])));
			$obj_boq->status = "not started";
			$obj_boq->srno = trim($data['srno']);

			$obj_boq->created_by = $userId;
			$obj_boq->updated_by = $userId;

 			$save = $obj_boq->save();

			$boq_id = $obj_boq->id;

				$curr_seq_no = self::generateBoqNoSeq($project_id, $data['counter']);

				Boq::where('id', $boq_id)->update([
				'name' => $project_id."000".$curr_seq_no,
				'seq_no' => $curr_seq_no,
				]);

///check for parent id	
				if( $data['boq_type'] == 'Header'){
					Boq::where('id', $boq_id)->update([
					'parent_id' => $boq_id,
					]);
				}
						
 			if($data['boq_parent_id'] ==null){
				$parent_id = $obj_boq->id;
 			}else {
 				$parent_id = $data['boq_parent_id'];
 			}
			return $parent_id;
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

			} catch (Exception $e) {

			Log::error('Exception while updating expense :: ', $e->getMessage());
		      return  false;
		   }

	}
	
	public static function editBoq($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data ) {
			$userId = $data['authenticatedUser']->id;
			$obj_boq  = Boq::where('name', $data['boq_no'])->update([
			'description'=> trim($data['description']),
			'unit_rate' => trim($data['unit']),
			'rate' => $data['rate'],
			'amount' => $data['amount'],
			'quantity' => $data['qty'],
			'activity_name' => trim($data['service']),
			'srno' => trim($data['srno']),
			'created_by' => $userId,
			'updated_by' => $userId,
			]);
		
		$boq_data  = Boq::where('name', $data['boq_no'])->get();
		$parent_id = $boq_data[0]['parent_id'];
		Log::debug("******boq no saved****** = ". $data['boq_no']);
		Log::debug("*****dhruvi edit* EDIT BOQ EDIT FUNCTION CALLED ******".$data['boq_no']);

			return $parent_id;
		});
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	

	public static  function getBoqList_old($project_id)
	{
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Boq::where('project_id', $project_id)
		->orderBy('parent_id', 'asc')
		->orderBy('id', 'asc')
		->get();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
		
	}
	public static  function getBoqList($project_id)
	{
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		 $data = DB::table('boqs')
                ->leftJoin('dprs as dpr', 'dpr.boq_id', '=', 'boqs.id')
                ->select(
                	//'DISTINCT  dpr.boq_id as dpr_id',
                	'dpr.boq_id as dpr_id',
                	 'boqs.id',
                     'boqs.parent_id',             
                    'boqs.boq_type' ,          
                    'boqs.project_id',             
                	'boqs.name',
                    'boqs.description',             
                    'boqs.client_user_id',             
                    'boqs.service_id',             
                    'boqs.unit_rate',             
                    'boqs.rate',             
                    'boqs.amount',             
                    'boqs.quantity',             
                    'boqs.activity_name',             
                    'boqs.status',             
                    'boqs.srno'             
                            
                    )->where('boqs.deleted_at', null)->where('boqs.project_id',$project_id);

         $data->groupBy('dpr.boq_id',
         	'boqs.parent_id',
         	'boqs.id',
         	'boqs.boq_type',
 			'boqs.project_id',             
                	'boqs.name',
                    'boqs.description',             
                    'boqs.client_user_id',             
                    'boqs.service_id',             
                    'boqs.unit_rate',             
                    'boqs.rate',             
                    'boqs.amount',             
                    'boqs.quantity',             
                    'boqs.activity_name',             
                    'boqs.status',             
                    'boqs.srno'           
         );
         $data->orderBy('boqs.parent_id', 'asc')->orderBy('boqs.id','asc');

         $rows = $data->get();
        // dd($rows);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $rows;
	 
	}
   
	public static  function getBoqDataForExport($project_id)
	{
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Boq::select('name as Boq_no','srno as SrNo','description as Description','unit_rate as Unit', 
			'quantity as Qty', 'rate as Rate', 'amount as Amount','activity_name as Service' )
		->where('project_id', $project_id)
		->orderBy('id', 'asc')->get();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		
		return $data;
		
	}
public static function deleteBoq($id) {
		
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
				$userId = auth()->user()->id;


		$Boqs = Boq::where('id', $id)->get();
			if($Boqs[0]['boq_type']=='Header'){
				$details = Boq::where('parent_id', $id)->get();

					foreach ($details as $key => $value) {
					  $updateBOQ = Boq::where('id', $value['id']);
					  $updateBOQ->update(['deleted_by' => $userId]);  
					  $check1 = $value->delete();
					}
					
				}else {
				
				$BoqSingle = Boq::where('id', $id);
				$BoqSingle->update(['deleted_by' => $userId]);
				$check1 = $BoqSingle->delete();
			}
			
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check1;

		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;

	}
public static function changeStatus($id,$status) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id,$status) {
			
				$userId = auth()->user()->id;
				$boq = Boq::where('id', $id);
				$boq->update(['status' => $status]);
				return true;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}

	public static function generateBoqNoSeq($project_id, $counter) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

		$boq_seq = DB::table('boqs')->select('seq_no')->where('project_id', $project_id)->max('seq_no');
			Log::debug("MAX NUMBER = " .$counter);
		if(intval($boq_seq) > 0){
			$db_seq_no = intval($boq_seq) + 1;
			Log::debug("IF SEQENCE NULL == " .$db_seq_no);

		}else {
			$db_seq_no = intval(0) + 1 ;
			 Log::debug("ELSE SEQENCE HAS MAX value == " .$db_seq_no);

		}
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $db_seq_no;
	}



public static  function getBoqServiceList($project_id)
	{
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		 $data = DB::table('boqs')
                ->select(
                    'boqs.activity_name'             
                    )->where('boqs.deleted_at', null)->where('boqs.project_id',$project_id)
                     ->where('boqs.activity_name','!=', "");
         $data->groupBy(      
                    'boqs.activity_name'            
                   );
         $data->orderBy('boqs.activity_name', 'asc');

         $rows = $data->get();
        // dd($rows);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

		return $rows;
	 
	}

}
