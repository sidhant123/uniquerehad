<?php

namespace App\Http\Controllers;

//Custom Imports
use App\Models\Address;
use App\Models\Organization;
use App\Models\OrganizationAttribute;
use App\Models\Party;
use App\Models\PasswordLogic;
use App\Models\PeopleAttribute;
use App\Models\Person;
use App\Models\RoleUser;
use App\Models\Phone;
use App\Services\OrganizationService;
use App\Services\UserService;
use App\Services\ValidationRulesService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;
use App\Services\ManpowerContractService;

class OrganizationController extends BaseController {

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function index(Request $request, $flag = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		//$this->authorize('List Companies');
		// $this->authorize('List Companies', 'List Companies');
		//parent::populateOrClearSearch($request);
		// checking for company coordinator party id
		$curr_url = $request->path();
		if($curr_url == '/'){
			//return redirect()->action('OrganizationController@index');
				return redirect('companies/')
						->withErrors(true)
						->withInput();
		}
		
		
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		
		return PaginateController::paginate('Organization', 'companies', parent::$utilsInterface, parent::$masterDataInterface, $flag);
		// try {
		//     return PaginateController::paginate('Organization', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
		// } catch (\Exception $exception) {
		//     Log::error("Exception handled in OrganizationController");
		// } catch (\Exception $exception) {
		//     Log::error("Exception handled in OrganizationController");
		// }

	}

	public function create(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$this->authorize('Create Companies', 'Create Companies');
		$geoData['country_id'] = self::getDataFromRoutes($request, 'countriesForFilter');
		$geoData['state_id'] = array();
		$geoData['city_id'] = array();
		$geoData['pincode_id'] = array();
		$orgData['password_logics'] = PasswordLogic::all()->toArray();
		//dump($geoData);
		//exit();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return view('company.create_company', compact('geoData', 'orgData'));
	}

	public function store(Request $organizationRequest) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
		 $organizationRequest['organization_name'] = ucwords(strtolower(trim($organizationRequest['organization_name'])));
		// $organizationRequest['organization_code'] = strtoupper(trim($organizationRequest['name']));

			$this->authorize('Create Companies', 'Create Companies');
			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('Organization');
			if ($organizationRequest->ajax()) {
				$organizationRequest->validate($rules);
			} else {
				$validator = Validator::make($organizationRequest->all(), $rules);
				if ($validator->fails()) {
					return redirect('companies/create')
						->withErrors($validator)
						->withInput();
				}
			}
			$data = $organizationRequest->all();
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			//Then call saveUser - which will do the needful
			if (sizeof($organizationRequest->allFiles()) > 0) {
				//organizationRequest needed only to upload the file otherwise not needed
				$data['organizationRequest'] = $organizationRequest;
				//OrganizationService::saveOrganization($data, true);
				OrganizationService::saveOnlyCompany($data, true);
			} else {
				//OrganizationService::saveOrganization($data, false);
				OrganizationService::saveOnlyCompany($data, false);
			}
			Session::flash('status', 'data stored successfully');
		} catch (Exception $e) {
			Log::error('Error storing organization ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return redirect()->action('OrganizationController@index');
	}

	private function checkAuthorization($id) {
		$isAuthorized = false;
		if (session()->has('userPrincipal')) {
			$userPrincipal = session('userPrincipal');

			if (empty($userPrincipal->getCustomerPartyId())) {
				return true;
			}

			$organization = Organization::find($id);
			if ($organization['party_id'] != $userPrincipal->getCustomerPartyId()) {
				$isAuthorized = false;
				Log::debug(__LINE__ . " NOT AUTHORIZED " . $userPrincipal->getCustomerPartyId() . " -- " . $organization['party_id']);
			} else {
				$isAuthorized = true;
				Log::debug(__LINE__ . " AUTHORIZED " . $userPrincipal->getCustomerPartyId() . " -- " . $organization['party_id']);
			}

		}
		return $isAuthorized;
	}

	public function show($id, Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			//$this->authorize('Edit Companies', 'Edit Companies');

			// checking for valid user
			// if (!$this->checkAuthorization($id)) {
			// 	Session::flash('status', 'you are not authorized to access this company');
			// 	return redirect()->action('OrganizationController@index');
			// }

			$orgData = Organization::where('id', $id)->first();
			$people = UserService::getUsersByCompany($orgData['party_id']);
			// org add and contact
			$orgData['orgAddress'] = Address::where('party_id', $orgData['party_id'])->first();
			$contact = Phone::where('party_id', $orgData['party_id'])->first();
			if ($contact != null) {
				$orgData['contact'] = $contact['phone_number'];
			} else {
				$orgData['contact'] = "";
			}
	
			$orgAttribute = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'password_logic'])->first();
			$orgData['password_logic_id'] = $orgAttribute['attr_value'];
			Log::error($orgData['password_logic_id']);
			$orgData['password_logics'] = PasswordLogic::all()->toArray();

			
			//geo data
			$geoData['country_id'] = self::getDataFromRoutes($request, 'countriesForFilter');
			$geoData['state_id'] = self::getDataFromRoutes($request, 'statesForFilter', 'country_id', $orgData['orgAddress']['country_id']);
			$geoData['city_id'] = self::getDataFromRoutes($request, 'citiesForFilter', 'state_id', $orgData['orgAddress']['state_id']);
			$geoData['pincode_id'] = self::getDataFromRoutes($request, 'pincodesForFilter', 'city_id', $orgData['orgAddress']['city_id']);
			Log::debug('OrganizationController::edit finished');
			return view('company.view_company', compact('orgData', 'pmData', 'ccData', 'people', 'geoData'));

		} catch (Exception $e) {
			Log::error('Exception showing edit company form ', $e->getMessage());
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		}

	}

	public function edit($id, Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			//$this->authorize('Edit Companies', 'Edit Companies');

			// checking for valid user
			// if (!$this->checkAuthorization($id)) {
			// 	Session::flash('status', 'you are not authorized to access this company');
			// 	return redirect()->action('OrganizationController@index');
			// }

			$orgData = Organization::where('id', $id)->first();
			$people = UserService::getUsersByCompany($orgData['party_id']);
//dd($orgData);
			// org add and contact
			$orgData['orgAddress'] = Address::where('party_id', $orgData['party_id'])->first();
			$contact = Phone::where('party_id', $orgData['party_id'])->first();
			if ($contact != null) {
				$orgData['contact'] = $contact['phone_number'];
			} else {
				$orgData['contact'] = "";
			}
//dd($orgData['contact']);

			// abhi - 26/03/19
			// $orgAttribute = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'comm_option'])->first();
			// $orgData['comm_option'] = $orgAttribute['attr_value'];
			// Log::error($orgData['comm_option']);

			$orgAttribute = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'password_logic'])->first();
			$orgData['password_logic_id'] = $orgAttribute['attr_value'];
			Log::error($orgData['password_logic_id']);
			$orgData['password_logics'] = PasswordLogic::all()->toArray();

			
			//geo data
			$geoData['country_id'] = self::getDataFromRoutes($request, 'countriesForFilter');
			$geoData['state_id'] = self::getDataFromRoutes($request, 'statesForFilter', 'country_id', $orgData['orgAddress']['country_id']);
			$geoData['city_id'] = self::getDataFromRoutes($request, 'citiesForFilter', 'state_id', $orgData['orgAddress']['state_id']);
			$geoData['pincode_id'] = self::getDataFromRoutes($request, 'pincodesForFilter', 'city_id', $orgData['orgAddress']['city_id']);
			Log::debug('OrganizationController::edit finished');
			return view('company.edit_company', compact('orgData', 'pmData', 'ccData', 'people', 'geoData'));

		} catch (Exception $e) {
			Log::error('Exception showing edit company form ', $e->getMessage());
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		}
	}

	private function getDataFromRoutes($request, $route, $routeParameter = false, $value = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$user = \Auth::user();
		if ($value) {
			$selectData = \Request::create(route($route, [$routeParameter => $value]), 'GET');
			$selectData->merge(['user' => $user]);
			//add this
			$selectData->setUserResolver(function () use ($user) {
				return $user;
			});
			$response = \Route::dispatch($selectData);
			$data = json_decode($response->getContent(), true);
			return $data;
		} else {
			$selectData = \Request::create(route($route), 'GET');
			$selectData->merge(['user' => $user]);
			//add this
			$selectData->setUserResolver(function () use ($user) {
				return $user;
			});
			$response = \Route::dispatch($selectData);
			$data = json_decode($response->getContent(), true);
			return $data;
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function update(Request $organizationRequest, $id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {

			$this->authorize('Edit Companies', 'Edit Companies');
			 $organizationRequest['organization_name'] = ucwords(strtolower(trim($organizationRequest['organization_name'])));

			$orgData = Organization::where('id', $id)->first();
			$contact = Phone::where('party_id', $orgData['party_id'])->first();
			 $contact_id = $contact['id'];
			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRules('Organization', '$company', $id);

			$rules['organization_contact'] ='required|min:10|max:10|unique:phones,phone_number,'.$contact_id.',id,deleted_at,NULL';
			$validator = Validator::make($organizationRequest->all(), $rules);
			Log::debug($validator->fails());
			if ($validator->fails()) {
				return redirect('companies/' . $id . '/edit')
					->withErrors($validator)
					->withInput();
			}
			$data = $organizationRequest->all();
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			//Then call updateUser - which will do the needful
			if (sizeof($organizationRequest->allFiles()) > 0) {
				//organizationRequest needed only to upload the file otherwise not needed
				$data['organizationRequest'] = $organizationRequest;
				$id = OrganizationService::updateOrganization($id, $data, true);
			} else {
				$id = OrganizationService::updateOrganization($id, $data, false);
			}
			Session::flash('status', 'data updated successfully');
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return redirect()->action('OrganizationController@index');
		} catch (Exception $e) {
			Log::error('Exception while updating organization ', $e->getMessage());
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		}
	}

	public function destroy($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$this->authorize('Delete Companies', 'Delete Companies');

			Log::error('in the destroy function of controller');

			$user_id = auth()->user()->id;
			Log::error('see below for user id');
			Log::error($user_id);

			$role_user_data = RoleUser::where('user_id', $user_id)->first();

			Log::error($role_user_data);
			// exit();

			// $role_user_data['role_id'] = 3;
			// check if under this compny manpower contract is exist or not?

			$m_list = ManpowerContractService::getManpowerForCompany($id);
			if ($role_user_data['role_id'] != 1) {

				Log::error('user is not tenant admin');

				Session::flash('status', 'User is not authorized to delete the company');
				# code...
			} 
			elseif(strlen($m_list)>=1){

				Session::flash('status', __('You can not delete this company as assigned in manpower contract(s) '));

			}
			else {

				$deleted = OrganizationService::deleteOrganization($id);
			if ($deleted) {
				Session::flash('status', 'Company deleted successfully');
			} else {
				Session::flash('status', 'Company cant be deleted as its users exist');
				}
			}

			// exit();


			// checking for valid user
			// if (!$this->checkAuthorization($id)) {
			// 	Session::flash('status', 'you are not authorized to access this company');
			// 	return redirect()->action('OrganizationController@index');
			// }

			
			// $path_array = explode('/', action('OrganizationController@index'));
			// $route = array_pop($path_array);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return "deleted successfully" . "/" . 'companies';

		} catch (Exception $e) {
			Log::error('Exception while deleting organization ', $e->getMessage());
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		}

	}

	public function getUserDataByID($id) {

		return UserService::getUserDetailsById($id, parent::$masterDataInterface);
		// UserService::getPersonByRoleCC($id);
		//UserService::getUserDetailsById($id);
	}

}
