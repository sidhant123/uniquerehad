<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Project;
use App\Models\Organization;
use App\Models\ExpenseType;

/**
 * Class AddressMasterService
 *
 */
class ManpowerReportService {
	
	  public static function retrieveManpowerReportDetailQuery($project_id,$org_id,$start_date,$end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';

        $queryBuilder = DB::table('manpowers')
            ->select(
                'organizations.name as org_name' , 
                'projects.name as project_name' , 
                 DB::raw("manpowers.created_date::Date as d_date"),   
                'manpowers.no_of_workers' ,  
                'manpower_documents.location as geolocation'  
        )
            ->leftJoin('projects', 'projects.id', '=', 'manpowers.project_id')
            ->leftJoin('organizations', 'organizations.id', '=', 'manpowers.contractor_id')
            ->leftJoin('manpower_documents', 'manpower_documents.manpower_id', '=', 'manpowers.id')
            ->where('manpowers.is_active', 1)
            ->whereBetween('manpowers.created_date', [$start_date, $end_date])
            ->orderBy('org_name','asc') 
            ->orderBy('manpowers.project_id','asc') 
            ->orderBy('d_date','asc') ;

            if($project_id>0){
               $queryBuilder->where('manpowers.project_id','=', $project_id);
            }   
            if($org_id>0){
               $queryBuilder->where('manpowers.contractor_id','=', $org_id);
            }

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

    public static function getProjectName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $project = Project::select('name')->where('id', '=', $Id)->first();
        return trim($project->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    public static function getOrgName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $name = Organization::select('name')->where('id', '=', $Id)->first();
        return trim($name->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
     
}
