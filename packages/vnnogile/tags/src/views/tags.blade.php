@section('tagContent')

<div class="row multiple" style="display: none;">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group required">
						<label for="tag_name_id">Tags</label>
						@if(!empty(old('tag')))
						<input type="text" class="form-control" id="tag_name_id" name="tag" value="{{ old('tag') }}">
						@else
						<input type="text" class="form-control" id="tag_name_id" name="tag" value="">
						@endif
						</div>
					</div>
				</div>
			</div>
@endsection
