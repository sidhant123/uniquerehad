<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:57 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BaseModel
 * 
 *
 * @package App\Models
 */
class BaseModel extends Eloquent
{
	protected $connection = 'pgsql';

	protected $hidden = [
		'created_by',
		'updated_by',
		'deleted_by',
		'created_at',
		'updated_at',
		'deleted_at'
	];	
}
