<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 19 Jun 2018 19:23:30 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
/**
 * Class ContentRating
 * 
 * @property int $id
 * @property int $artefact_type_id
 * @property int $entity_id
 * @property int $user_id
 * @property int $rating
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ContentRating extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'artefact_type_id' => 'int',
		'entity_id' => 'int',
		'user_id' => 'int',
		'rating' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'artefact_type_id',
		'entity_id',
		'user_id',
		'rating',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
