@extends('manpower_contract.manpower_contract')
@section('title','View Manpower Contract')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $row_data->id)


@section('title','Edit Manpowe Contract')
@section('name',$row_data->name)
@section('description',$row_data->description)
@section('rate',$row_data->rate)

@section('project_id',$row_data->project_id)


@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('.form-control').attr('readonly',true);
  $('#organization_id').prop('disabled', 'true');
  $('#project_id').prop('disabled', 'true');
  $('#service_name').prop('disabled', 'true');
});

@if ($row_data['status'] == 'Active') {
     $("[name=status]").val(["Active"]);
}
@else{
     $("[name=status]").val(["Inactive"]);
}
@endif
</script>

@endpush
