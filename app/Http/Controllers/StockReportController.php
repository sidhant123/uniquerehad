<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\StockReportService;
use App\Services\SiteService;
use App\Models\Site;



class StockReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function create() {

        $site = SiteService::retrieveSites();
		return view('reports.stock_report' ,compact('site'));
	}

	public function exportData(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$siteCode = StockReportService::getSiteName($request['site_id']);
		$queryBuilder = StockReportService::retrieveStockReportQuery($request['site_id']);

	    self::downloadReportExcel($queryBuilder, $siteCode );
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadReportExcel($queryBuilder, $siteCode ) {
		$fileName = $siteCode . '_Stock_Report';
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
//dd($row_data);
        // creating title header row
        // $keys = array();
        // foreach ($row_data as $key => $value) {
        // foreach ($value as $key1 => $value1) {
       
        //     array_push($keys, $key1);
        // }
        // break;
        // }
            
        $keys = array("Site Name","Site Location", "Project Name","Material Name", "Quantity");

        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array($row->site_name, $row->location, $row->project_name, $row->material_name, $row->quantity);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}

}
