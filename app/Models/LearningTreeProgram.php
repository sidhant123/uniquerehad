<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class LearningTreeProgram
 * 
 * @property int $id
 * @property int $learning_trees_id
 * 
 * @property \App\Models\LearningTree $learning_tree
 *
 * @package App\Models
 */
class LearningTreeProgram extends BaseModel
{
	protected $table = 'learning_tree_program';
	public $timestamps = false;

	protected $casts = [
		'learning_trees_id' => 'int'
	];

	protected $fillable = [
		'learning_trees_id'
	];

	public function learning_tree()
	{
		return $this->belongsTo(\App\Models\LearningTree::class, 'learning_trees_id');
	}
}
