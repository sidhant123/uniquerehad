<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Project;
use App\Models\ExpenseType;

/**
 * Class AddressMasterService
 *
 */
class ReceiptReportService {
	
	  public static function retrieveReceiptReportDetailQuery($project_id,$start_date,$end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';
        $queryBuilder = DB::table('receipts')
            ->select(
                'projects.name as project_name' , 
                'receipts.payment_no' ,  
                'receipts.boq_no' ,  
                'receipts.invoice_no' ,  
                'receipts.invoice_date' ,  
                'receipts.payment_method' ,  
                'receipts.payment_ref_no' ,  
                'receipts.payment_receipt_date' ,  
                'receipts.amount_received' ,  
                'receipts.gst_amount' ,  
                'receipts.invoice_amount' ,  
                'receipts.total_amount' ,  
                'receipts.tds_deduction' ,  
                'receipts.other_deduction' ,  
                'receipts.base_amount' ,  
                'receipts.total_deduction' ,
                'receipts.retension' ,
                'receipts.mobilization' 
               
        )
            ->leftJoin('projects', 'projects.id', '=', 'receipts.project_id')
             ->where('receipts.deleted_at', null)
            ->where('receipts.project_id','=', $project_id)
            ->where('receipts.project_id','=', $project_id)
            ->whereBetween('receipts.invoice_date', [$start_date, $end_date])
            ->orderBy('receipts.invoice_date','asc')
            ->orderBy('receipts.boq_no', 'asc');

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

    public static function getProjectName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $project = Project::select('name')->where('id', '=', $Id)->first();
        return trim($project->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
     
}
