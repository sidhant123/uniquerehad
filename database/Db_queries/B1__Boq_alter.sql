
-- changes by dhruvi 23 may 2020
/*

ALTER TABLE boqs ADD COLUMN  unit_rate character varying(200) ;
ALTER TABLE boqs ADD COLUMN   rate numeric(10,4) ;
ALTER TABLE boqs ADD COLUMN   amount numeric(10,4) ;
ALTER TABLE boqs ADD COLUMN   quantity numeric(10,2) ;
ALTER TABLE boqs ADD COLUMN   activity_name character varying ;


ALTER TABLE boqs DROP COLUMN id;
ALTER TABLE boqs ADD COLUMN id bigint NOT NULL DEFAULT nextval('boqs_seq'::regclass);
alter table boqs add CONSTRAINT boq_pkey PRIMARY KEY (id); */
-- Table: public.boqs

DROP TABLE public.boqs;
create sequence boqs_seq;

CREATE TABLE public.boqs
(
  id bigint NOT NULL DEFAULT nextval('boqs_seq'::regclass),
  
  parent_id integer,
  boq_type character varying,
  project_id integer,
  name character varying(255) NOT NULL,
  description character varying(500) NOT NULL,
  client_user_id integer ,
  service_id integer,
  unit_rate character varying(200),
  rate numeric(10,4),
  amount numeric(10,4),
  quantity numeric(10,2),
  activity_name character varying,
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
    CONSTRAINT boqs_pkey PRIMARY KEY (id)

)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.boqs
  OWNER TO postgres;

  update list_view_masters set entity_namespace='App\Models\' where id = 8;
