<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;
use App\Services\ValidationRulesService;

use App\Models\Project;
use App\Models\Site;
use App\Services\SiteService;

class SiteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Log::debug(\Auth::user()->hasAccess(['Site']));
        //$this->authorize('List Site', 'List Site');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return PaginateController::paginate('Site', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
     //     $this->authorize('Create sites', 'Create sites');
    
                         
         return view('site.create_site');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Create sites', 'Create sites');
        try {
            $request['name'] = ucwords(strtolower(trim($request['name'])));

             $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            $rules = $valdationRulesService->validationRulesWithTenant('Site');
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return redirect('sites/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }         

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['Site'] = $request;
            $data['request'] = $request;
           SiteService::saveData($data);
           

            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('SiteController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = Site::where('id', $id)->first();

            return view('site.view_site', compact('row_data'));


        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = Site::where('id', $id)->first();

            return view('site.edit_site', compact('row_data'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Edit sites', 'Edit sites');
        try {
        $request['name'] = ucwords(strtolower(trim($request['name'])));

            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('Site', '$site', $id);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                   return redirect('sites/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
            $data['request'] = $request;

               SiteService::updateData($data);
          
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('SiteController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Delete Sites', 'Delete Sites');

            $projectData = Project::where('site_id', $id)->first();

            Log::error($projectData);

            $date =date('Y-m-d');
            if ($date < $projectData['end_date'] ) {

                Session::flash('status', 'Project related to site is active site can not be deleted');
            } else {

                $deleted = SiteService::deleteSite($id);
            if ($deleted) {
                Session::flash('status', 'Site deleted successfully');
            } else {
                Session::flash('status', 'Site cant be deleted');
            }
            // $path_array = explode('/', action('SiteController@index'));
            // $route = array_pop($path_array);

                Log::error('You can delete site');
            }
            // exit();
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'sites';

        } catch (Exception $e) {
            Log::error('Exception while deleting Site ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }
}
