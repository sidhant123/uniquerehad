<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 15 Feb 2018 20:47:22 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Validation
 * 
 * @property int $id
 * @property string $entity_name
 * @property string $operation
 * @property string $field_name
 * @property string $validation_rule
 * @property int $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class Validation extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'entity_name',
		'operation',
		'field_name',
		'validation_rule',
		'active',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
