<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Mar 2018 15:02:24 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
/**
 * Class AssessmentAnswer
 * 
 * @property int $id
 * @property int $exercise_banks_id
 * @property string $description
 * @property int $exercise_types_id
 * @property array $answers
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Customer $customer
 * @property \App\Models\ExerciseBank $exercise_bank
 *
 * @package App\Models
 */
class AssessmentAnswer extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'exercise_banks_id' => 'int',
		'exercise_types_id' => 'int',
		//'answers' => 'json',
		'tenant_party_id' => 'int',
		'customer_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'exercise_banks_id',
		'description',
		'exercise_types_id',
		'answers',
		'tenant_party_id',
		'customer_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class, 'customer_party_id');
	}

	public function exercise_bank()
	{
		return $this->belongsTo(\App\Models\ExerciseBank::class, 'exercise_banks_id');
	}
}
