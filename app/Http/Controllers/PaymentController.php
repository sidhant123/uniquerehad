<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;
use App\Services\ValidationRulesService;

use App\Models\Payment;
use App\Services\PaymentService;
use App\Services\OrganizationService;
use App\Services\ProjectService;
use App\Services\ExpenseTypeService;

class PaymentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        $this->middleware('CheckCompositeMiddleWare', ['except' => ['update', 'create', 'show']]);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        //Log::debug(\Auth::user()->hasAccess(['Payment']));
        $this->authorize('List Payments', 'List Payments');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return PaginateController::paginate('Payment', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
          $this->authorize('Create Payments', 'Create Payments');
         $organization = OrganizationService::retrieveOrganization();
         $project = ProjectService::retrieveProject();
         $expense_type = ExpenseTypeService::retrieveExpenseTypes();

                         
         return view('payment.create_payment', compact('organization','project','expense_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        // $this->authorize('Create payments', 'Create payments');
        try {

             $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('Payment');
           
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return redirect('payments/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }         

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['Payment'] = $request;
            $data['request'] = $request;
             
             $saveResult = PaymentService::savePaymentData($data);
           if($saveResult){
              Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('PaymentController@index');

           }else {
              Session::flash('status', __('labels.flash_messages.store_error'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('PaymentController@index');
           }

          

        } catch (Exception $e) {
            Log::error('Exception while storing payment :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Edit Payments', 'Edit Payments');
          
            $row_data = Payment::where('id', $id)->first();
         $organization = OrganizationService::retrieveOrganization();
         $project = ProjectService::retrieveProject();
         $expense_type = ExpenseTypeService::retrieveExpenseTypes();
         $sel_organization_id = $row_data['organization_id'];
         $sel_project_id = $row_data['project_id'];
         $sel_expense_type_id = $row_data['expense_type_id'];

            return view('payment.view_payment', compact('row_data','organization','project','expense_type', 'sel_organization_id', 'sel_project_id','sel_expense_type_id'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Edit Payments', 'Edit Payments');
          
            $row_data = Payment::where('id', $id)->first();
              $organization = OrganizationService::retrieveOrganization();
         $project = ProjectService::retrieveProject();
         $expense_type = ExpenseTypeService::retrieveExpenseTypes();
         $sel_organization_id = $row_data['organization_id'];
         $sel_project_id = $row_data['project_id'];
         $sel_expense_type_id = $row_data['expense_type_id'];

            return view('payment.edit_payment', compact('row_data','organization','project','expense_type', 'sel_organization_id', 'sel_project_id','sel_expense_type_id'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //$this->authorize('Edit Payments', 'Edit Payments');
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('Payment', '$payment', $id);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                   return redirect('payments/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
            $data['request'] = $request;

             $saveResult =  PaymentService::updatePaymentData($data);
          if($saveResult){

            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('PaymentController@index');
          }else{
             Session::flash('status', __('labels.flash_messages.store_error'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('PaymentController@index');
          }

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Delete Payments', 'Delete Payments');

            $deleted = PaymentService::deletePayment($id);
            if ($deleted) {
                Session::flash('status', 'Payment deleted successfully');
            } else {
                Session::flash('status', 'Payment cant be deleted');
            }
            // $path_array = explode('/', action('PaymentController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'payments';

        } catch (Exception $e) {
            Log::error('Exception while deleting Payment ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }
}
