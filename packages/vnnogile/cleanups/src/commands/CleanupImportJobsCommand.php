<?php

namespace vnnogile\Cleanups\Commands;

use Illuminate\Console\Command;

//Custom Imports
use Illuminate\Support\Facades\Log;
use vnnogile\Cleanups\Services\CleanupService;

class CleanupImportJobsCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:import_jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup import job records';

    /**
     * The assessment service.
     *
     * @var CleanupService $cleanupService
     */
    protected $cleanupService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CleanupService $cleanupService)
    {
        parent::__construct();
        $this->cleanupService = $cleanupService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        //Invoke the cleanupNotifications from the CleanupService
        $this->cleanupService->cleanupImportJobs();

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
