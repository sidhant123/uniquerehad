@extends('company.company')
@section('title','Create Company')
@section('editMethod')
{{method_field('post')}}
@endsection
@section('view-nav-bar', 'hide')
@section('update-nav-bar', 'hide')
@section('edit_btn','hide')
@push('scripts')
<style>
#pmCheck{
	display: none;
}
#ccCheck{
	display: none;
}
</style>

<script>
$("#sameUser").prop("checked", false);

$('#sameUser').change(function() {

    // this will contain a reference to the checkbox
    if (this.checked) {
     setUserdataOnChange();

    }
    else{
      $('#sameUser').prop("checked",true);
    }

});
function setUserdataOnChange(){
   var r=confirm("Program manager will be Company Coordinator");
        if (r == true) {

           $("#ccSelector :input").attr("readonly", true);

           var z = $('#pm_user_first_name_id').select2('data');

           if ($('#cc_first_name_id').find("option[value='" + z[0].id + "']").length) {
               $('#cc_first_name_id').val(z[0].id).trigger('change');
               } else {
               // Create a DOM Option and pre-select by default
               var newOption = new Option(z[0].text, z[0].id, true, true);
               $('#cc_first_name_id').append(newOption).trigger('change');
             }




           //changing mandatory
           $('#file3').removeClass("mandatory");
           $('#cc_photo_div').removeClass('required');
           //setting  value
           setValue('cc_lastName','pm_lastName');
           setValue('cc_employeeId','pm_employeeId');
           setValue('cc_department','pm_department');
           setValue('cc_email','pm_email');
           setValue('cc_designation','pm_designation');
           setValue('cc_primaryContact','pm_primaryContact');
           setValue('cc_secondaryContact','pm_secondaryContact');
           setValue('cc_linkedinProfile','pm_linkedinProfile');
           setValue('cc_fbProfile','pm_fbProfile');
           setValue('cc_twitterHandle','pm_twitterHandle');

           setValue('cc_userId','pm_userId');
           setValue('cc_personalEmail','pm_personalEmail');
           setValue('cc_googleId','pm_googleId');

           var gender=$('input[name=pm_gender]:checked').val();
           $("[name=cc_gender]").val([gender]);

           // abhi - 26/03/19
           setValue('cc_dob','pm_dob');
           var profle=$('input[name=pm_profile]:checked').val();
           $("[name=cc_profile]").val([profle]);

           console.log($('#pm_last_name_id').val());


         }
      else {
           $('#sameUser').attr("checked",false);
      }
}

function setValue(a,b){
              // var c=$("#"+b).val();
              var c=$("input[name="+b+"]").val();
              // $("#"+a).val(c);

              $("input[name="+a+"]").val(c);
}

// abhi 26/03/19
$(document).on('change','#pass_logic',function(){
  var passLogic=$(this).val();
  console.log(passLogic);
  if (passLogic == 2) {
      $('#pm_dob').addClass("mandatory");
      $('#pm_dob_wrapper').addClass("required");
      $('#cc_dob').addClass("mandatory");
      $('#cc_dob_wrapper').addClass("required");
      // remove the read only

  }else{
      $('#pm_dob').removeClass("mandatory");
      $('#pm_dob_wrapper').removeClass("required");
      $('#cc_dob').removeClass("mandatory");
      $('#cc_dob_wrapper').removeClass("required");
  }
})

</script>

@endpush

