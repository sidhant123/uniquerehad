<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Party
 * 
 * @property int $id
 * @property int $party_type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\PartyType $party_type
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $clusters
 * @property \Illuminate\Database\Eloquent\Collection $communication_events
 * @property \Illuminate\Database\Eloquent\Collection $competencies
 * @property \Illuminate\Database\Eloquent\Collection $customers
 * @property \Illuminate\Database\Eloquent\Collection $exercises
 * @property \Illuminate\Database\Eloquent\Collection $organizations
 * @property \Illuminate\Database\Eloquent\Collection $people
 * @property \Illuminate\Database\Eloquent\Collection $programs
 * @property \Illuminate\Database\Eloquent\Collection $questions
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \Illuminate\Database\Eloquent\Collection $tenants
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Party extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		//'party_type_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'party_type_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party_type()
	{
		return $this->belongsTo(\App\Models\PartyType::class);
	}

	public function user(){
		return $this->hasOne(\App\User::class);
	}

	//Every person will have a correspondng party record
	//Hence every Party will have one person
	public function person(){
		return $this->hasOne(\App\Models\Person::class);
	}

	//It can be either tenant or customer
	public function organization(){
		return $this->hasOne(\App\Models\Organization::class);
	}


	// A tenant party record would have many tenant persons
	public function tenant_person(){
		return $this->hasMany(\App\Models\Person::class, 'tenant_party_id');
	}

	// A tenant party record would have many tenant persons
	public function customer_person(){
		return $this->hasMany(\App\Models\Person::class, 'customer_party_id');
	}


	public function contacts(){
		return $this->hasMany(\App\Models\Phone::class);
	}

	public function addresses(){
		return $this->hasMany(\App\Models\Address::class);
	}	

	////////////////////////////////////

	public function people()
	{
		return $this->hasMany(\App\Models\Person::class, 'customer_party_id');
	}

public function clusters(){
		return $this->hasMany(\App\Models\Cluster::class, 'customer_party_id');
	}
}
