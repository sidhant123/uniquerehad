<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

//Custom Imports Below
use App\Models\Validation;

use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

class UserRequest extends FormRequest
{

    protected $masterDataInterface;
    protected $utilsInterface;

    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
    }   

    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $userPrincipal = session('userPrincipal');
        $tenantPartyId = $userPrincipal->getTenantPartyId();   
        $user='';
        $validationRule= array();
        print_r($this->route('user'));
        exit();
        if (null != $this->route('user')){
            //if condition means update
             $user = $this->route('user');
             $validations = $this->utilsInterface->getValidationRulesForEntity('User', 'Update');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $extractedFromDb = str_replace('$tenantPartyId', $tenantPartyId, $extractedFromDb);
                 $extractedFromDb = str_replace('$user', $user, $extractedFromDb);
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }else{
            //else condition means create
             $validations = $this->utilsInterface->getValidationRulesForEntity('User', 'Create');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $extractedFromDb = str_replace('$tenantPartyId', $tenantPartyId, $extractedFromDb);
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }
         Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
         return $validationRule;
    }
}
