delete from list_view_masters where id = 7;

INSERT INTO public.list_view_masters(
             function_name, entity_name, entity_namespace, query_columns, 
            heading_columns,
             filter_columns, display_css_widths, relations, replace_columns,
              created_at, updated_at, 
             created_by, updated_by )
    VALUES ('List Projects', 'Project', '', 
        'id,name,start_date,end_date,id', 
         'Id,Name,Start date,End Date,Action',
             '', 'col-xs-1,col-xs-2,col-xs-2,col-xs-2,col-xs-2', 
             '', '',
             now(),now(),1,1);

update validations set validation_rule = 'required|max:300'  where id =  1;

-- create project id sequence
create sequence projetcs_seq;
ALTER SEQUENCE projetcs_seq RESTART WITH 3;
ALTER TABLE projects ALTER COLUMN id SET DEFAULT nextval('projetcs_seq');
ALTER TABLE projects ALTER COLUMN id SET NOT NULL;
ALTER SEQUENCE projetcs_seq OWNED BY projects.id;


ALTER TABLE projects ALTER COLUMN start_date TYPE date;
ALTER TABLE projects ALTER COLUMN end_date TYPE date;


-- remove old data

update organizations set deleted_at = now(), deleted_by = 1 where id not in (110,1);
update users set deleted_at = now(), deleted_by = 1 where id < 300 and id !=1;