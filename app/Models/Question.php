<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;


/**
 * Class Question
 * 
 * @property int $id
 * @property string $main_text
 * @property string $individual_text
 * @property string $rater_textl
 * @property int $competencies_id
 * @property int $question_type_id
 * @property int $tenant_party_id
 * @property array $question_detail_options
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $customer_party_id
 * 
 * @property \App\Models\Competency $competency
 * @property \App\Models\Party $party
 * @property \App\Models\QuestionType $question_type
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Question extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'competencies_id' => 'int',
		// 'question_type_id' => 'int',
		'tenant_party_id' => 'int',
		//'question_detail_options' => 'json',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
		//'customer_party_id' => 'int'
	];

	protected $fillable = [
		'main_text',
		'individual_text',
		'rater_textl',
		'competencies_id',
		'question_type_id',
		'tenant_party_id',
		'question_detail_options',
		'created_by',
		'updated_by',
		'deleted_by',
		'customer_party_id'
	];

	public function competency()
	{
		return $this->belongsTo(\App\Models\Competency::class, 'competencies_id');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'customer_party_id');
	}

	public function question_type()
	{
		return $this->belongsTo(\App\Models\QuestionType::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}
}
