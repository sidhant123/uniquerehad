<?php

namespace vnnogile\TestPackage\Providers;

use Illuminate\Support\ServiceProvider;

class TestPackageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->publishes([
        __DIR__.'/../vendor' => public_path('vendor/vnnogile'),
    ], 'public');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //include __DIR__.'/../controllers/CommunicationChannelController.php';
        //include __DIR__.'/../services/TemplateUtility.php';
        // $this->app->make('vnnogile\templates\controllers\CommunicationChannelController');
    }
}
