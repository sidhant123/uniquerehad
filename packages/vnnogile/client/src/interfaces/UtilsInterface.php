<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\Client\Interfaces;

//Custom Imports
 use vnnogile\Client\Interfaces\MasterDataInterface;
/**
 * Service ServiceInterface
 * 
 */
interface UtilsInterface
{
	public function getRowCountList(MasterDataInterface $masterDatainterface);
	public function isTaggable(MasterDataInterface $masterDatainterface, $model);
	public function getTenantForAuthUser($user);
	public function getCustomerForAuthUser($user);
	public function getConfigValue(MasterDataInterface $masterDatainterface, $configKey);
	public function getUserPrincipal();
	public function getValidationRulesForEntity($entityName, $operation);

	public function getTenantForUser($userId);
	public function getCustomerForUser($userId);

}
