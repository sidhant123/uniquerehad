@extends('layouts.base')
@section('content')
<form action="" id="InwardReportForm" method="post" >
  {{ csrf_field() }}
 
  @section('editMethod')
  @show
  <h3 align="center">Export In-Out Report</h3>

    <strong>NOTE**</strong><p></p>
<textarea class="form-control" style="width:100%;" rows="4" cols="30" readonly="readonly">
You must have to select from date, to date and project for download Report.
</textarea>
    <p></p>

<div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <label for="project_id">Start Date</label>
        <input type="date" class="form-control mandatory" id="start_date" name="start_date" onchange="return validateDate()">
      </div>
</div>
<div class="col-lg-3">
      <div class="form-group">
        <label for="project_id">End Date</label>
        <input type="date" class="form-control mandatory" id="end_date" name="end_date" onchange="return validateDate()">
      </div>
</div>
</div>

  <div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <label for="project_id"></label>
        <select type="text" id="project_id" name="project_id" class="form-control mandatory">
          <option value="0">---Project---</option>
          @foreach($project as $val)
            <option value="{{$val->id}}">{{$val->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
   <div class="col-lg-3">
      <div class="form-group">
        <label for="report_type"></label>
        <select type="text" id="report_type" name="report_type" class="form-control mandatory">
          <option value="0">---Report Type---</option>
            <option value="Inward">Inward</option>
            <option value="Transfer">Transfer</option>
            <option value="Consumption">Consumption</option>
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <label for=""></label>
        <br>
        <button id="btnReport" class="btn btn-primary" type="button" style="height: 34px;" >
          Download Report &nbsp;&nbsp;<i class="vnn vnn-download"></i>
        </button>
      </div>
    </div>  

</div>      
</form>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
     $('#project_id').select2({});

$(document).on('click', '#btnReport', function() {
   var valid = true;
   valid = validateMandatoryFields();
   if(valid) {
     $('#InwardReportForm').attr('action', "/inwardReport").submit();
   }

  var project_id = $('#project_id').val();
  if(project_id == 0) {
  document.getElementById("select2-project_id-container").style.border = "1px solid red";
  } else {
    document.getElementById("select2-project_id-container").style.border = "0px solid red";
  }

 });

});

function validateDate(){
var valid = true;
var fromdate = Date.parse($("#start_date").val());
var todate = Date.parse($("#end_date").val());
//alert(fromdate>todate);
if(fromdate>todate){
     $("#end_date").val("");
     var selector='input[name=end_date]';
     scrollToElement(selector)
     valid=false;
}
      return valid;
}
</script>
@endpush
