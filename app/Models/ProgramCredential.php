<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 20 Jun 2019 19:15:01 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramCredential
 * 
 * @property int $id
 * @property int $program_id
 * @property string $attr_name
 * @property string $attr_value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Program $program
 *
 * @package App\Models
 */
class ProgramCredential extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'attr_name',
		'attr_value',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function program()
	{
		return $this->belongsTo(\App\Models\Program::class);
	}
}
