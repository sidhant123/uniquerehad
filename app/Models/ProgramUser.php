<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 01 Jun 2018 16:33:02 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramUser
 * 
 * @property int $id
 * @property int $program_id
 * @property int $user_id
 * @property int $role_user_id
 * @property int $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ProgramUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'program_user';

	protected $casts = [
		'program_id' => 'int',
		'user_id' => 'int',
		'role_user_id' => 'int',
		'is_active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'user_id',
		'role_user_id',
		'is_active',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
