

update menus set display_name='Payments', name='Payments' where name = 'Expenses' ;
update menus set display_name='List', name='List Payments' where name = 'List Expenses' ;
update menus set display_name='New Payment', name='Create Payments' where name = 'Create Expenses' ;


update permissions set name = 'Payments' where name = 'Expenses';
update permissions set name = 'List Payments' where name = 'List Expenses';
update permissions set name = 'Create Payments' where name = 'Create Expenses';
update permissions set name = 'Show Payments' where name = 'Show Expenses';
update permissions set name = 'Delete Payments' where name = 'Delete Expenses';
update permissions set name = 'Edit Payments' where name = 'Edit Expenses';

  
drop table expenses;
-- Table: public.payments

-- DROP TABLE public.payments;

CREATE TABLE public.payments
(
  id bigint NOT NULL DEFAULT nextval('payments_seq'::regclass),
  project_id integer NOT NULL,
  expense_date date NOT NULL,
  expense_type_id integer NOT NULL,
  organization_id integer NOT NULL,
  service_name character varying NOT NULL,
  bill_no character varying(100),
  item_desc character varying NOT NULL,
  unit character varying(30) NOT NULL,
  qty numeric(10,2) NOT NULL,
  rate numeric(10,2) NOT NULL,
  amount numeric(15,2) NOT NULL,
  gst_amount numeric(10,2) NOT NULL,
  amount_paid numeric(15,2),
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  base_amount numeric(15,2),
  CONSTRAINT payments_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.payments
  OWNER TO postgres;



  update list_view_masters set function_name = 'List Payments', entity_name ='Payment', 
    filter_class_names = 'App\Utilities\PaymentFilterUtility'
    where entity_name = 'Expense';

update validations set entity_name='Payment' where entity_name = 'Expense';

  update filter_masters set route_name = 'receipts.index' where route_name = 'expenses.index';
  update composition_masters set  route_name = 'receipts.index'  where route_name = 'expenses.index';
update composition_masters set  controller_name = 'App\Http\Controllers\ReceiptController'  where id =3;
