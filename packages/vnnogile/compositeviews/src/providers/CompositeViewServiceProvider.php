<?php

namespace vnnogile\CompositeViews\Providers;

use Illuminate\Support\ServiceProvider;

class CompositeViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../controllers/CompositeController.php';
         $this->app->make('vnnogile\CompositeViews\controllers\CompositeController');
    }
}
