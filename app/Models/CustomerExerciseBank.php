<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 17:44:18 +0530.
 */

namespace App\Models;

use App\Models\BaseModel;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CustomerExerciseBank
 *
 * @property int $id
 * @property int $exercise_banks_id
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property string $exercise_code
 * @property int $active
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @property \App\Models\ExerciseBank $exercise_bank
 * @property \App\Models\Customer $customer
 *
 * @package App\Models
 */
class CustomerExerciseBank extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
       // 'exercise_banks_id' => 'int',
        'active' => 'int',
        'tenant_party_id' => 'int',
        'customer_party_id' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int',
    ];

    protected $dates = [
        'start_date',
        'end_date',
    ];

    protected $fillable = [
        'exercise_banks_id',
        'program_id',
        'start_date',
        'end_date',
        'exercise_code',
        'active',
        'tenant_party_id',
        'customer_party_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public function getStartDateAttribute()
    {
        return date('d-m-Y', strtotime($this->attributes['start_date']));
    }
    public function getEndDateAttribute()
    {
        return date('d-m-Y', strtotime($this->attributes['end_date']));
    }
    public function exerciseBank()
    {
        return $this->belongsTo(\App\Models\ExerciseBank::class, 'exercise_banks_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_party_id');
    } 
    public function customer_exercise_participants()
    {
        return $this->hasMany(\App\Models\CustomerExerciseParticipant::class,'customer_exercise_banks_id');
    }

    public function customer_organization()
    {
        return $this->belongsTo(\App\Models\Organization::class, 'customer_party_id', 'party_id');
    } 

}
