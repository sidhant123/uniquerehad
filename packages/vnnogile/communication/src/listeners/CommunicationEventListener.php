<?php

namespace vnnogile\Communication\Listeners;

use App\User;

use vnnogile\Communication\Events\CommunicationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

//Custom Imports Below
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;
use vnnogile\mailer\Utility\EmailUtility;

class CommunicationEventListener implements ShouldQueue
{

    protected $masterDataInterface;
    protected $utilsInterface;
    public $tries = 1;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
    }

    /**
     * Handle the event.
     *
     * @param  UserUploadEvent  $event
     * @return void
     */
    public function handle(CommunicationEvent $event)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $communicationEvent = \App\Models\CommunicationEvent::where([
            ['name', '=', $event->eventName],
            ['tenant_party_id', '=', $event->tenantPartyId]
        ])->first();


        $communicationRoles = $communicationEvent->roles();
        Log::debug(print_r($communicationRoles, true));

        $subject = "Test email";
        $data = $event->data;
        $authenticatedUserId = $event->authenticatedUserId;
        $eventName = $event->eventName;
        $log = $event->log;
        $user = User::find($authenticatedUserId);
        $data['user'] = $user;

        Log::debug("event name ===> " . $eventName);
        Log::debug("data ===> ");
        Log::debug(print_r($data, true));

        if ('UserUploadEvent' == $eventName){
            $emailData = array('template_name' => "Hi <b>{USER.FIRST_NAME} {USER.LAST_NAME}</b>, Your file {FILENAME} has been uploaded and a new job {JOBID} has been created. Once the file is processed, you will be notified of the same!", 
                'data' => $data, 
                'to' => $user->email,
                'attachments' => array(),
                'subject' => $subject);
        }else{
            $emailData = array('template_name' => "Hi <b>{user.first_name} {USER.LAST_NAME}</b>, Your job {JOBID} for users upload for file {FILENAME} has been completed.", 
                'data' => $data, 
                'to' => $user->email,
                'attachments' => array(),
                'subject' => $subject);
        }

        EmailUtility::sendEmail($emailData, $this->masterDataInterface, $this->utilsInterface);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }


}
