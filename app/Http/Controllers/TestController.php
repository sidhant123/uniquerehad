<?php

namespace App\Http\Controllers;

use App\Models\ExerciseBank;

//Custom imports
use DB;
use PDF;
use App\User;
use App\Jobs\ExportContent;
use App\Jobs\ExportUsers;
use App\Models\Program;
use App\Models\Topic;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use vnnogile\Badge\Events\BadgeEvent;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Menu\Services\MenuService;
use vnnogile\Pagination\Utilities\PaginateController;
use vnnogile\ExportCommunicationTrails\Jobs\ExportCommunicationTrailsJob;
use vnnogile\ContentValidation\Jobs\ContentExtractionJob;

class TestController extends BaseController
{

    public function __construct()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public function dispatchJob(Request $request){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
//        ExportContent::dispatch($request['tenant_party_id']);
    	ContentExtractionJob::dispatch($request['tenantId']);
        //return \vnnogile\Utilities\Services\UrlUtility::checkIfUrlExists("http://www.vnnossgile.com");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");        
        return "Fnished";
        
    }

    public function generateSvg(Request $request){
      Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
      return LearningTreeSvgCoordinatorService::createLearningTreeSvg($request['tree_type'], $request['tree_id'], $request['parent_node_title'], $request['parent_node_id'], parent::$masterDataInterface, 1);;
      Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $templateObj = PaginateController::paginateByQuery('addressReport', 'AddressReport', parent::$utilsInterface, parent::$masterDataInterface);
        if ($flag) {
            return $templateObj;
        } else {
            $templateObj = PaginateController::paginateByQuery('addressReport', 'AddressReport');
            $paginationData = $templateObj['vnnogile::view_list']['paginationData'];
            $records = $templateObj['vnnogile::view_list']['records'];
            $entityColumns = $templateObj['vnnogile::view_list']['entityColumns'];
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return view('vnnogile::view_list', compact('entityColumns', 'records', 'paginationData'));
        }
    }
    public function test(Request $request)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

//Begin
        $data["Tenant Administrator"][31] =
            ['template_data' =>
            ['LOGINUSER.FIRSTNAME' => \Auth::user()->first_name,
                'LOGINUSER.LASTNAME' => \Auth::user()->last_name,
                'USERIMPORT.FILENAME' => 'Dummy File Name',
                'USERIMPORT.JOBID' => 'Dummy Job Id',
            ],
            'email_id' => 'dhananjay@vnnogile.com', 'phone_no' => '9930', 'program_code' => '21', 'customer_organization_code' => 'TCS'];

        return 2;
//End
        $eventName = "ActivityCompletionEvent";
        $data = ["programId" => $request['program'], "recipientUserId" => $request['user'], "topicId" => $request['topic']];

        $eventName = "QuizCompletionEvent";
        $data = ["programId" => $request['program'], "recipientUserId" => $request['user'], "topicId" => $request['topic']];

        $eventName = "OptionalProfileInfoFillingEvent";
        $data = ["programId" => $request['program'], "recipientUserId" => $request['user']];
        event(new BadgeEvent($eventName, $data));
        return Program::select('id')->find($request['program'])->user_badges()->where('program_user_badge.user_id', $request['user'])->first()->pivot;

        $eventName = "ContentRatingEvent";
        $data = ["programId" => $request['program'], "recipientUserId" => $request['user'], "topicId" => $request['topic']];

        $eventName = "FacilitatorBadgeDistributionEvent";
        $data = ["programId" => $request['program'], "recipientUserId" => $request['user'], "facilitatorId" => $request['ouser'], "badges" => $request['badgeCount']];

        $eventName = "ParticipantBadgeDistributionEvent";
        $data = ["programId" => $request['program'], "recipientUserId" => $request['user'], "donorUserId" => $request['ouser'], "badges" => $request['badgeCount']];

        // try{
        //   //"TCS/Nego/001"
        //   return BadgeService::retrieveBadgeForProgramIdUser($request['id'], $request['user']);
        //   //return ProgramService::retrieveProgramById($request['id']);
        // }catch(ModelNotFoundException $exception){
        //   Log::error('Exception:: ' . $exception->getMessage());
        // }

        return "Wow";

        $user = \Auth::user();
        $roles = $user->roles;
        return $roles;

        MenuService::buildMenus();
        // var_dump(app('router')->getRoutes());
        //   exit();
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

        $commEventRoles = \App\Models\CommunicationEvent::with(['roles' => function ($query) {$query->where('name', '=', 'Tenant Administrator');}])->where(['name' => 'UserUploadEvent'])->get();
        //return $commEventRoles;
        $commEvents = \App\Models\CommunicationEvent::where('name', '=', 'UserUploadEvent')->get();
        $arr = array();
        $arr1 = array();
        foreach ($commEvents as $commEvent) {
            foreach ($commEvent->roles as $role) {
                array_push($arr, $role->pivot->id);
            }
        }
        foreach ($arr as $key => $value) {
            array_push($arr1, \App\Models\CommunicationEventRole::with('templates')->where('id', '=', $value)->get());
        }
        return $arr1;
        return $arr1[1][0]['templates'][0]['template'];

        //foreach

        return parent::$utilsInterface->getTenantForUser(27);
        return parent::$utilsInterface->getRowCountList(parent::$masterDataInterface);
        return parent::$masterDataInterface->find("ArtefactType", 2);
        return parent::$masterDataInterface->findBy("ArtefactType", ['name' => 'Video']);
        return parent::$masterDataInterface->all("PartyType");
        return parent::$masterDataInterface->all("EntityConfiguration");
        return parent::$masterDataInterface->list("PartyType");
        return parent::$masterDataInterface->filteredList("PartyType", ['id' => 1], 1);
        return parent::$masterDataInterface->filteredList("PartyType", ['id' => 2], 2);

        return PaginateController::paginate('Party', 'parties', parent::$utilsInterface, parent::$masterDataInterface);
    }
    public function getListViewByQuery()
    {
        //first parameter will be records,route,entityReportName
        $templateObj = PaginateController::paginateByQuery('addressReport', 'AddressReport');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $paginationData = $templateObj['vnnogile::view_list']['paginationData'];
        $records = $templateObj['vnnogile::view_list']['records'];
        $entityColumns = $templateObj['vnnogile::view_list']['entityColumns'];
        dump($records);
        exit;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return view('vnnogile::view_list', compact('entityColumns', 'records', 'paginationData'));
    }

    public function commTest()
    {
        $data["Program Director"][31] =
            ['template_data' =>
            ['LOGINUSER.FIRSTNAME' => \Auth::user()->first_name,
                'LOGINUSER.LASTNAME' => \Auth::user()->last_name,
                'PROGRAM.ID' => '1',
                'PROGRAM.NAME' => 'Working with emotional intelligence',
            ],
            'email_id' => 'dhananjay@vnnogile.com', 'phone_no' => '9920763003', 'customer_organization_code' => "TCS", 'party_id' => 1];

        $data["Program Manager"][32] =
            ['template_data' =>
            ['LOGINUSER.FIRSTNAME' => \Auth::user()->first_name,
                'LOGINUSER.LASTNAME' => \Auth::user()->last_name,
                'PROGRAM.ID' => '1',
                'PROGRAM.NAME' => 'Working with emotional intelligence',
            ],
            'email_id' => 'ashwini@vnnogile.com', 'phone_no' => '9920763003', 'customer_organization_code' => "TCS", 'party_id' => 2];

        event(new CommEvent("ProgramCreatedEvent", $data, 'CGX', \Auth::user()->id));
    }

    public function jsonTest()
    {
        $exerciseBank = new ExerciseBank();
        $exerciseBank->name = "Test2";
        $exerciseBank->description = "Test2";
        $exerciseBank->exercise_types_id = 1;
        $child1 = (object) ["first_name" => "Advait", "last_name" => "Katre"];
        $child2 = (object) ["first_name" => "Sara", "last_name" => "Katre"];
        $parent1 = (object) ["parent" => "Dhananjay", "children" => [$child1, $child2]];

        $child3 = (object) ["first_name" => "Saachi", "last_name" => "Bhambure"];
        $child4 = (object) ["first_name" => "Saraa", "last_name" => "Bhambure"];
        $parent2 = (object) ["parent" => "Pankaj", "children" => [$child3, $child4]];

        $parents = array($parent1, $parent2);

        //$finalJson = (object) ['childrenlist' => $parents];
        $exerciseBank->questions = json_encode($parents);
        $exerciseBank->tenant_party_id = 1;
        $exerciseBank->created_by = 1;
        $exerciseBank->updated_by = 1;
        $exerciseBank->save();

    }
    public function eagerLoading()
    {
        // $books = \App\Models\Country::with('states.cities')->get();
        // return array("data");

//     $users = \App\Models\Country::with(['states.cities' => function ($query) {
        //     $query->where('cities.id', '=', 1);
        // }])->get();
        //     return $users;
        //       $countries = \App\Models\Country::with(['states' => function($query) {
        //     $query->where('id', 1);
        // }, 'states.cities' => function($query1) {
        //     $query1->where('id', 2);
        // },
        // 'cities.pincodes' => function($query2) {
        //     // $query1->where('id', 2);
        // }])->get();

        $country = \App\Models\Country::find(1);
//       $country->load('states.cities'); // eager load far relation

// $states = new Collection; // Illuminate\Database\Eloquent\Collection

// foreach ($country->states as $state)
        // {
        //    $states = $states->merge($state->cities);
        // }

        $country->load(['states.cities' => function ($q) use (&$cities) {
            $cities = $q->get();
        }]);

        return $cities;
    }
    public function testPDF()
    {
        // $pdf = \App::make('dompdf.wrapper');
        // $pdf->loadHTML('<h1>Test</h1>');
        // return $pdf->download();
        //load a data

        $pdf = PDF::loadView('pdf.learning_tree');
        return $pdf->download('learning_tree.pdf');
    }
    public function testLearningTree($learning_tree_id)
    {
//       SELECT *
// FROM(
// select tp.sequence,chtp.sequence child, chtp.description descp
// from topics tp
// inner join topics chtp on(tp.id = chtp.parent_topic_id)
// where tp.container_learning_tree_id = 4;
// UNION
// select sequence,sequence child, description descp from topics where container_learning_tree_id = 4 and parent_topic_id IS NULL
// -- and tp.sequence like '1%'
// ) ft
// ORDER BY 2;
      $lt = DB::table('learning_trees')->select('name','learning_tree_code','description')->where('id',$learning_tree_id)->first();

        $union = DB::table('topics')->select('name as name','sequence','sequence as child_nodes','description as descp','container_learning_tree_id as lt_id')->where(['container_learning_tree_id' => $learning_tree_id,'parent_topic_id'=> NULL]);
        $topics = DB::table('topics')
            ->Join('topics as tp', 'tp.parent_topic_id', '=', 'topics.id')
            ->select('tp.name','topics.sequence as parent_topic_id', 'tp.sequence as child_nodes','tp.description as descp','tp.container_learning_tree_id as lt_id')
            ->where('tp.container_learning_tree_id', $learning_tree_id)->union($union)
            ->orderBy('child_nodes')->get();
            // print_r($data);
            // exit();
        $company_logo =  imgToBase64('img/logo.png');
        $pdf = PDF::loadView('pdf.learning_tree', compact('topics', 'company_logo','lt'));
        return $pdf->download('learning_tree.pdf');
        // return view('pdf.learning_tree',compact('data','company_logo'));

    }
}
