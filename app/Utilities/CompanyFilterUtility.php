<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class CompanyFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('organizations')
                ->leftJoin('tenants', 'tenants.party_id', '=', 'organizations.party_id')
                ->leftJoin('addresses', 'addresses.party_id', '=', 'organizations.party_id')
                ->leftJoin('cities', 'cities.id', '=', 'addresses.city_id')
                ->leftJoin('phones', 'phones.party_id', '=', 'organizations.party_id')
                ->select('organizations.name as name', 'organizations.organization_code as organization_code', 'cities.name as city_name', 'phones.phone_number', 'organizations.id as id')->where('organizations.deleted_at', null);
            // if (session()->has('userPrincipal')) {
            //     $userPrincipal = session('userPrincipal');
            //     if (!empty($userPrincipal->getCustomerPartyId())) {
            //         $data->where('organizations.party_id', $userPrincipal->getCustomerPartyId());
            //     }

            // }
            // if ($request->has('search')) {
            //     $data->whereRaw("(organizations.name like '%" . $request['search'] . "%' or organizations.organization_code like '%" . $request['search'] . "%' or cities.name like '%" . $request['search'] . "%' or phones.phone_number like '%" . $request['search'] . "%' or pm.first_name like '%" . $request['search'] . "%' or cc.first_name like '%" . $request['search'] . "%')");
            // }


            if ($request->has('search')) {
                $data->whereRaw("(
                    lower(organizations.name) like '%" . strtolower($request['search']) . "%' or
                    lower(organizations.organization_code) like '%" . strtolower($request['search']) . "%' or
                    lower(cities.name) like '%" . strtolower($request['search']) . "%' or
                    lower(phones.phone_number) like '%" . strtolower($request['search']) . "%' 
             )");
            }

            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'name':
                        $data->orderBy('organizations.name', $request['sort']);
                        break;
                    case 'organization_code':
                        $data->orderBy('organizations.organization_code', $request['sort']);
                        break;
                    case 'city_name':
                        $data->orderBy('cities.name', $request['sort']);
                        break;
                    case 'phone_number':
                        $data->orderBy('phones.phone_number', $request['sort']);
                        break;
                   
                }
            } else {
                $data->orderBy('organizations.id');
            }
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            // dd($data);
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}
