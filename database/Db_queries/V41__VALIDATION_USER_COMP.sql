
-------- Org module
delete from validations where entity_name='Organization';
delete from validations where entity_name='Address';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Organization', 'Update', 'organization_name', 'required|min:4|max:250|unique:organizations,name,$company,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Organization', 'Create', 'organization_name', 'required|min:4|max:250|unique:organizations,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
   
     ('Organization', 'Update', 'organization_code', 'required|min:2|max:10|unique:organizations,organization_code,$company,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Organization', 'Create', 'organization_code', 'required|min:2|max:10|unique:organizations,organization_code,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
   
     ('Organization', 'Update', 'email', 'required|email|min:10|max:250|unique:organizations,email,$company,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Organization', 'Create', 'email', 'required|email|min:10|max:250|unique:organizations,email,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
   

     ('Address', 'Both', 'address1', 'required|min:4|max:100', 1, now(), now(), 1, 1),
     ('Address', 'Both', 'address2', 'required|min:4|max:100', 1, now(), now(), 1, 1),
     ('Address', 'Both', 'address3', 'required|min:4|max:100', 1, now(), now(), 1, 1)  ;




-------- User module
delete from validations where entity_name='User';

INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
   
     ('User', 'Update', 'email', 'required|email|min:10|max:250|unique:people,email,$user,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('User', 'Create', 'email', 'required|email|min:10|max:250|unique:people,email,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('User', 'Update', 'primaryContact', 'required|min:10|max:10|unique:phones,phone_number,$user,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('User', 'Create', 'primaryContact', 'required|min:10|max:10|unique:phones,phone_number,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),

     ('User', 'Both', 'firstName', 'required|min:4|max:100', 1, now(), now(), 1, 1),
     ('User', 'Both', 'lastName', 'required|min:4|max:100', 1, now(), now(), 1, 1),
     ('User', 'Both', 'dob', 'required', 1, now(), now(), 1, 1),
     ('User', 'Both', 'primaryContact', 'required|max:10', 1, now(), now(), 1, 1)  ;


