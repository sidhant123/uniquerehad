<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 13 Mar 2018 17:58:03 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CommunicationTrail
 * 
 * @property int $id
 * @property string $content
 * @property int $job_id
 * @property string $error_code
 * @property string $status
 * @property string $tenant_code
 * @property string $customer_organization_code
 * @property int $party_id
 * @property string $program_code
 * @property string $exception
 * @property int $communication_event_id
 * @property string $transaction_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\CommunicationEvent $communication_event
 *
 * @package App\Models
 */
class CommunicationTrail extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'job_id' => 'int',
		'communication_event_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'content',
		'job_id',
		'error_code',
		'status',
		'tenant_code',
		'customer_organization_code',
		'party_id',
		'program_code',
		'exception',
		'communication_event_id',
		'transaction_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function communication_event()
	{
		return $this->belongsTo(\App\Models\CommunicationEvent::class);
	}
}
