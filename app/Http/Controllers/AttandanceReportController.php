<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\AttandanceReportService;
use App\Services\ProjectService;



class AttandanceReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function create() {

        $site_users = ProjectService::getSiteEnggUsers();
		return view('reports.attandance_report' ,compact('site_users'));
	}

	public function attandanceReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");

		$start_date = $request['start_date'];
		$end_date = $request['end_date'];
	
		$queryBuilder = AttandanceReportService::retrieveAttandanceReportQuery($start_date,$end_date);

	    self::downloadReportExcel($queryBuilder,$start_date,$end_date);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadReportExcel($queryBuilder,$start_date,$end_date ) {
		$fileName = 'Attendance_Summary_Report_From_'.$start_date.'_To_'.$end_date;
		  ob_end_clean();
          ob_start();
          $row_data = DB::table(DB::raw($queryBuilder))->get();
          // $row_data = $queryBuilder->get();
			// dd($row_data);
          
        $keys = array("Site Engg Name","No of Attendance");

        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
         $name = $row->first_name.' '.$row->last_name;

        	$allData[] = array($name, $row->no_of_days);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}
/// detail report

	public function exportAttendanceDetailReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");

		$start_date = $request['start_date'];
		$end_date = $request['end_date'];
		$queryBuilder = AttandanceReportService::retrieveAttandanceDetailReportQuery($start_date,$end_date, $request['user_id']);

	    self::downloadDetailReportExcel($queryBuilder,$start_date,$end_date);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadDetailReportExcel($queryBuilder,$start_date,$end_date ) {
		$fileName = 'Attendance_Detail_Report_From_'.$start_date.'_To_'.$end_date;
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
          
        $keys = array("Site Engg Name","Date", "Time", "Project", "Location");

        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array($row->name, $row->d_date, $row->t_time, $row->project_name, $row->location);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}

}
