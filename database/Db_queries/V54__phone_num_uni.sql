delete from validations where field_name = 'primaryContact' and validation_rule = 'required|max:10';



-------- User module
delete from validations where entity_name='Organization';

INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES
('Organization', 'Update', 'organization_contact', 'required|min:10|max:10|unique:phones,phone_number,$company,id,deleted_at,NULL', 1, now(), now(), 1, 1),
('Organization', 'Create', 'organization_contact', 'required|min:10|max:10|unique:phones,phone_number,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1);
