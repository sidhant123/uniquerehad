<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;
use App\Services\ValidationRulesService;

use App\Models\ExpenseType;
use App\Services\ExpenseTypeService;

class ExpenseTypeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Log::debug(\Auth::user()->hasAccess(['ExpenseType']));
        //$this->authorize('List ExpenseType', 'List ExpenseType');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return PaginateController::paginate('ExpenseType', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
     //     $this->authorize('Create expenseTypes', 'Create expenseTypes');
    
                         
         return view('expense_type.create_expense_type');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Create expenseTypes', 'Create expenseTypes');
        try {
             $request['name'] = ucwords(strtolower(trim($request['name'])));
             $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            $rules = $valdationRulesService->validationRulesWithTenant('ExpenseType');
            //dd($rules);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return redirect('expenseTypes/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }         

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['ExpenseType'] = $request;
            $data['request'] = $request;
           ExpenseTypeService::saveExpenseTypeData($data);
           

            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ExpenseTypeController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = ExpenseType::where('id', $id)->first();

            return view('expense_type.view_expense_type', compact('row_data'));


        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = ExpenseType::where('id', $id)->first();

            return view('expense_type.edit_expense_type', compact('row_data'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Edit expenseTypes', 'Edit expenseTypes');
       $request['name'] = ucwords(strtolower(trim($request['name'])));

        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('ExpenseType', '$name', $id);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                   return redirect('expenseTypes/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
            $data['request'] = $request;

               ExpenseTypeService::updateData($data);
          
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ExpenseTypeController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Delete ExpenseTypes', 'Delete ExpenseTypes');

            $deleted = ExpenseTypeService::deleteExpenseType($id);
            if ($deleted) {
                Session::flash('status', 'ExpenseType deleted successfully');
            } else {
                Session::flash('status', 'ExpenseType cant be deleted');
            }
            // $path_array = explode('/', action('ExpenseTypeController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'expenseTypes';

        } catch (Exception $e) {
            Log::error('Exception while deleting ExpenseType ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }
}
