@extends('layouts.base')
@section('content')
<form action="" id="ReceiptReportForm" method="post" >
  {{ csrf_field() }}
  <input type="hidden" name="report_type" id="report_type">
  <input type="hidden" name="entity_type" id="entity_type">
  @section('editMethod')
  @show
  <h3 align="center">Export Receipt Report</h3>

    <strong>NOTE**</strong><p></p>
<textarea class="form-control" style="width:100%;" rows="4" cols="30" readonly="readonly">
You must have to select from date, to date and project for download Detailed Report.
Expense Type selection is optional if you have not selected expense type it will give all expense type data.
</textarea>
    <p></p>

<!--   <div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <div class="radio-inline">
          <label><input type="radio" name="optassessmentorprogram" checked value="Assessment">Assessment</label>
        </div>
        <div class="radio-inline">
          <label><input type="radio" name="optassessmentorprogram" value="Program">Program</label>
        </div>    
      </div>
    </div>
  </div> -->
<div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <label for="project_id">Start Date</label>
        <input type="date" class="form-control mandatory" id="start_date" name="start_date" onchange="return validateDate()">
      </div>
</div>
<div class="col-lg-3">
      <div class="form-group">
        <label for="project_id">End Date</label>
        <input type="date" class="form-control mandatory" id="end_date" name="end_date" onchange="return validateDate()">
      </div>
</div>
</div>

  <div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <label for="project_id"></label>
        <select type="text" id="project_id" name="project_id" class="form-control mandatory">
          <option value="0">---Project---</option>
          @foreach($project as $val)
            <option value="{{$val->id}}">{{$val->name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <label for=""></label>
        <br>
        <button id="btnDetailReport" class="btn btn-primary" type="button" style="height: 34px;" >
          Download Report &nbsp;&nbsp;<i class="vnn vnn-download"></i>
        </button>
      </div>
    </div>  

</div>      
</form>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
     $('#project_id').select2({});



$(document).on('click', '#btnDetailReport', function() {
   //$('#report_type').val('COMPLETION');
   //$('#ReceiptReportForm').submit();
  
   var valid = true;
   valid = validateMandatoryFields();
   if(valid) {
     $('#ReceiptReportForm').attr('action', "/receiptReport").submit();
   }

  var project_id = $('#project_id').val();
  if(project_id == 0) {
  document.getElementById("select2-project_id-container").style.border = "1px solid red";
  } else {
    document.getElementById("select2-project_id-container").style.border = "0px solid red";
  }

 });

});

  function validateDate(){

var valid = true;
var fromdate = Date.parse($("#start_date").val());
var todate = Date.parse($("#end_date").val());
//alert(fromdate>todate);
if(fromdate>todate){
  $("#end_date").val("");
     var selector='input[name=end_date]';
     scrollToElement(selector)
     valid=false;
}
  return valid;
}

</script>
@endpush
