<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 15:06:17 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class Exercise
 * 
 * @property int $id
 * @property int $exercise_banks_id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\ExerciseBank $exercise_bank
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Exercise extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'exercise_banks_id' => 'int',
		'tenant_party_id' => 'int',
		'customer_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];

	protected $fillable = [
		'exercise_banks_id',
		'name',
		'description',
		'start_date',
		'end_date',
		'tenant_party_id',
		'customer_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function exercise_bank()
	{
		return $this->belongsTo(\App\Models\ExerciseBank::class, 'exercise_banks_id');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'customer_party_id');
	}
}
