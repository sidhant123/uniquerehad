<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 March 2018 13:31:34 +0530.
 */

namespace vnnogile\Geography\services;

//Custom imports
use App\Models\Contory;
use App\Models\State;
use App\Models\City;
use DB;
use Illuminate\Support\Facades\Log;

/*use vnnogile\Utilities\Services\FileUploadUtility;
use vnnogile\Utilities\Services\StorageUtility;*/
/**
 * Class CompositionDataService
 *
 */
class CityService
{
	public static function saveCity($data)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        // First we will move file to temporary folder
        // Save the document information into the database
        // move the file from temporary file path to its respective location
        // finally update the file path of the document
        try {
            $check = DB::transaction(function () use ($data) {

                // print_r($data['authenticatedUser']->id);
                // exit();

                    $city_name = $data['city_name'];
                    $state_id = $data['state_id'];
                    // $country_id = $data['country_id'];

                $city = new City;
                $city->name = $city_name;
                $city->state_id = $state_id;
               // $city->country_id = $country_id;
                $city->created_by = $data['authenticatedUser']->id;
                $city->updated_by = $data['authenticatedUser']->id;
                $city->save();

                Log::debug("***********AFTER SAVEING CITY**************");
                $city_id = $city->id;
                return $city_id;
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            });
            return $check;
	    } catch (Exception $e) {
	            Log::debug("CityService::saveRecord ended" . print_r($e, true));
	            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	            return $e;
        }
    }


    public static function updateCity($data, $id)
    {
        try {
            $check = DB::transaction(function () use ($data, $id) {
                $city_id = $id;
                $city = self::getCityData($city_id);
                        $city->name = $data['city_name'];
                        $city->state_id =$data['state_id'];
                      //  $city->country_id =$data['country_id'];
                        $city->updated_by = 1; //$data['authenticatedUser']->id;
                        $city->save();
                $city_id = $city->id;
                return $city_id;
            });
            return $check;
        } catch (Exception $e) {
            Log::debug("CityService::updateRecord ended" . print_r($e, true));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $e;
        }
    }

        public static function getCityData($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        $city = City::find($id);
        return $city;
    }

        public static function deleteCity($id){
        City::where('id', $id)->update(['deleted_by' =>auth()->user()->id]);
        City::where('id', $id)->delete();
    }

    	public static function getCityList(){
  		return City::all();
  	}


}