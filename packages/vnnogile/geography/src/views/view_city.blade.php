@extends('vnnogile::city_base')
@section('title', 'View City')
@section('editMethod')

@endsection
@section('disabled', 'disabled')
@section('showCityName', 'readonly')
@section('editID', $data->id)
@section('city_name', $data->name)
@section('disable_submit', 'pointerEventNone')
