<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 22:07:03 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class BadgeType
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $triggering_event_name
 * @property int $default_system_value
 * @property int $tenant_party_id
 * @property int $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class BadgeType extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'default_system_value' => 'int',
		'tenant_party_id' => 'int',
		'active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'triggering_event_name',
		'default_system_value',
		'tenant_party_id',
		'active',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}
}
