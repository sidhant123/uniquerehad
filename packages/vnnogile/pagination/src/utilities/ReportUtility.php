<?php

namespace vnnogile\Pagination\Utilities;

//Custom Imports Below
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
/**
 * This is a a common utility method for handling PDF files
 *
 * All controllers that want to generate PDF files from views
 * or specific HTML should use this class.
 *
 * PDFS can be downloaded or viewed inline within the browser
 * if the browser supports viewing of documents inline
 * 
 * PDF file can be saved locally and sent in email.
 */

class ReportUtility
{
    /**
     *
     * @param Request $request
     * @param string $html
     *
     * @return PDF
     */
    public static function getReportDataFromName(Request $request,$entityName,$rowCount,$queryColumnNames){

        $reportArray = array();
        switch ($entityName) {
            case 'AddressReport':
              $reportArray = self::getAddressData($request,$rowCount,$queryColumnNames);      
            break;
        }
        return $reportArray;
    }
    
    static function getAddressData($request,$rowCount,$queryColumnNames){ 
       $data = DB::table('countries')
        ->leftJoin('states', 'states.country_id', '=', 'countries.id')
        ->leftJoin('cities', 'cities.state_id', '=', 'states.id')
        ->select('countries.id as id','countries.name as country_id','states.name as state_id','cities.name as city_id');
        foreach ($queryColumnNames as $key => $columnName) {
            if($request->has($columnName)){
                    $data->where($columnName,'=',$request[$columnName]); 
                }
        }
        if($request->has('sort')){
                $data->orderBy($request['sort_column_name'],$request['sort']);
            }
        return $data->paginate($rowCount);
    }
}
