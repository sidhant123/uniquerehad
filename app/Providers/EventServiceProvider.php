<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserUploadEvent' => [
            'App\Listeners\UserUploadEventListener',
        ],
        'vnnogile\Communication\Events\CommEvent' => [
            'vnnogile\Communication\Listeners\CommEventListener',
        ],
        'vnnogile\Badge\Events\BadgeEvent' => [
            'vnnogile\Badge\Listeners\BadgeEventListener',
        ],
        'App\Events\ExcelUploadEvent' => [
            'App\Listeners\ExcelUploadEventListener',
        ],
        'App\Events\MultipleSheetExcelUploadEvent' => [
            'App\Listeners\MultipleSheetExcelUploadEventListener',
        ],
         'App\Events\BoqUploadEvent' => [
            'App\Listeners\BoqUploadEventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'vnnogile\Client\Subscribers\UserEventSubscriber',
    ];
}
