create SEQUENCE   boqs_seq; 
create SEQUENCE   boq_items_seq; 
-- Table: public.boqs

 DROP TABLE public.boqs;

CREATE TABLE public.boqs
(
  id bigint NOT NULL DEFAULT nextval('boqs_seq'::regclass),
  boq_no varchar not null,
  header_name varchar NOT NULL,
  user_id  integer NOT null,
  project_id  integer not null,
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT boqs_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.boqs
  OWNER TO postgres;


-- Table: public.boq_items

DROP TABLE public.boq_items;

CREATE TABLE public.boq_items
(
  id bigint NOT NULL DEFAULT nextval('boq_items_seq'::regclass),
  boq_id integer NOT NULL,
  boq_no varchar not null,
  detail_name character varying(255) NOT NULL,
  unit_rate  character varying(200),
  rate numeric(10,4),
  amount numeric(10,4),
  quantity numeric(10,2),
  activity_name varchar,
  activity_id integer,
  service_id integer,
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT boq_items_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.boq_items
  OWNER TO postgres;
