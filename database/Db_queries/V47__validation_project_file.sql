
-------- project module
delete from validations where entity_name='Project';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Project', 'Update', 'name', 'required|min:4|max:255|unique:projects,name,$project,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Project', 'Create', 'name', 'required|min:4|max:255|unique:projects,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'tender_doc','nullable|file|mimes:xlsx,doc,docx,pdf|max:10240',  1, now(), now(), 1, 1),
     ('Project', 'Both', 'schedule_pdf', 'nullable|file|mimes:pdf|max:10240', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'imported_boq_file', 'nullable|file|mimes:xlsx|max:2000', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'start_date', 'required', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'end_date', 'required|date|after_or_equal:startDate', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'delay_hr', 'required|integer', 1, now(), now(), 1, 1)  ;


     --  'image' => 'required|mimes:jpeg,bmp,png|size:20000',
