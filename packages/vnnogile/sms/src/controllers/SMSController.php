<?php

namespace vnnogile\SMS\Controllers;

use App\Models\CommunicationEvent;
use App\Models\Phone;
use App\Models\Program;
//custom
//
//use Illuminate\Http\Request;
use App\Models\Role;
use App\Services\TemplateService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Sms\Utility\SmsUtility;

class SMSController extends BaseController {
	private static $filePath = "/temp/sms/";
	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function create() {

		$data['masterDataInterface'] = parent::$masterDataInterface;
		$templates = TemplateService::templates();
		$customers = parent::$masterDataInterface->all('Customer');
		// $communicationEvents = CommunicationEventService::communicationEvents($data);
		return view('vnnogile::create_sms', compact('customers', 'templates'));
	}
	public function getTemplatesFromCommEvent($comm_event_id) {
		$commEvent = CommunicationEvent::find($comm_event_id);
		return $commEvent->templates()->get();
	}
	public function sendSMS(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$data = array('team_name' => 'Mumbai Indians');
			Log::debug('Before sending sms');
			$attachments = array();
			$request_data = $request->all();
			$to = json_decode($request_data['to_emails'], true);
			// Log::error($request_data);
			// Log::error($to);
			// Log::error(count($to));
			// Log::error($request_data['participants']);

			// exit();

			$role = Role::select('id')->where('name', 'Participant')->first();
			$role_id = $role->id;
			// $templateObj = Template::where('id',$request_data['templates'])->first();
			// $comm_event_id = $templateObj->communication_events;
			// print_r($comm_event_id->id);
			// exit();
			if (!empty($request_data['program_id'])) {
				$participant_ids = explode(',', $request_data['participants']);
				Log::debug($participant_ids);
				foreach ($participant_ids as $key => $id) {
					$replaceData = array();
					$replaceData[$role_id][$id] = $this->replacePlaceHolders($request_data['program_id'], $id, $request_data['summernote'], $to[$key]);
					Log::debug($replaceData);
					// dump($replaceData);
					// exit();
					Log::debug('Before sending sms');
					Log::debug($replaceData);
					$return = SmsUtility::sendCommunicationSms($replaceData[$role_id], parent::$masterDataInterface, parent::$utilsInterface);
				}
				Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
				return "SMS Sent Successfully";
				// \Session::flash('status', __('labels.flash_messages.store'));
				// return redirect()->action('\vnnogile\Mailer\Controllers\MailController@create');
			}
		} catch (Exception $e) {
			Log::debug($e->getMessage());
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $e;
		}

	}
	private function replacePlaceHolders($program_id, $user_id, $template, $phone_number) {
		// $template = $template;
		$program = Program::find($program_id);
		$user = User::where('id', $user_id)->first();
		$data['template_data'] = ['FIRST_NAME' => $user['first_name'],
			'LAST_NAME' => $user['last_name'],
			'USER_ID' => $user['userid'],
			'PASSWORD' => 'pass@123',
			'ROLE_NAME' => 'Participant',
			'PROGRAM_NAME' => $program['name'],
			'START_DATE' => date('d-m-Y', strtotime($program['start_date'])),
			'END_DATE' => date('d-m-Y', strtotime($program['end_date'])),
		];
		foreach ($data['template_data'] as $key => $value) {
			# code...
			$template = str_replace('{' . strtoupper($key) . '}', $value, $template);
			// $subject = str_replace('{'.strtoupper($key).'}', $value, $subject);
		}
		$data['template'] = $template;
		$data['subject'] = '';
		$data['phone_no'] = $phone_number;
		$data['login_id'] = \Auth::user()->id;
		$data['communication_event_id'] = 1000;
		$data['tenant_code'] = 'CGX';
		return $data;
	}
	public function uploadFile(Request $request) {
		$request['masterDataInterface'] = parent::$masterDataInterface;
		//storage configurations
		$configurations = $request['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$request['configurations'] = $configurationsMap;
		//end checking configurations
		$file_path = $this->uploadAttachment($request);
		$renameFile = $this->renameUploadedFile($file_path);
		$file['original_name'] = $renameFile;
		$file['uploaded_file_path'] = storage_path() . "/app" . self::$filePath;
		return $file;
	}
	public function deleteFile(Request $request) {
		$request = $request->all();
		$request['masterDataInterface'] = parent::$masterDataInterface;
		//storage configurations
		$configurations = $request['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$request['configurations'] = $configurationsMap;
		$class = $request['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug('Before deleting file');
		Log::debug(self::$filePath . $request['fileName']);
		if ($storageUtility->deleteFile(str_replace(storage_path() . "/app", '', $request['fileName']))) {
			return array("Deleted Successfully.");
		} else {
			return array("Some error occured.");
		}
	}
	private function uploadAttachment($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->uploadFileToPrivateStorage($data, self::$filePath);
	}
	private function renameUploadedFile($uploadedFile) {
		$countFile = self::countFile($uploadedFile['originalFileNames']['file'][0]);
		if ($countFile == 0) {
			rename(storage_path() . "/app" . self::$filePath . $uploadedFile['uploadedFileNames']['file'][0], storage_path() . "/app" . self::$filePath . $uploadedFile['originalFileNames']['file'][0]);
			return $uploadedFile['originalFileNames']['file'][0];
		} else {
			$tempFileName = explode('.', $uploadedFile['originalFileNames']['file'][0]);
			rename(storage_path() . "/app" . self::$filePath . $uploadedFile['uploadedFileNames']['file'][0], storage_path() . "/app" . self::$filePath . $tempFileName[0] . "(" . $countFile . ")." . $tempFileName[1]);
			return $tempFileName[0] . "(" . $countFile . ")." . $tempFileName[1];
		}

	}
	private function countFile($fileOriginalName) {
		$countFile = 0;
		$fileName = explode('.', $fileOriginalName);
		while (true) {
			if ($countFile == 0) {
				if (file_exists(storage_path() . "/app" . self::$filePath . $fileOriginalName)) {
					Log::debug("FILECOUNT:" . storage_path() . "/app" . self::$filePath . $fileOriginalName);
					$countFile++;
				} else {
					break;
				}
			} else {
				if (file_exists(storage_path() . "/app" . self::$filePath . $fileName[0] . "(" . $countFile . ")." . $fileName[1])) {
					Log::debug("FILECOUNT:" . storage_path() . "/app" . self::$filePath . $fileName[0] . "(" . $countFile . ")." . $fileName[1]);
					$countFile++;
				} else {
					break;
				}
			}
		}
		Log::debug("FILECOUNT:" . $countFile);
		return $countFile;
	}
	public function oldsendMail(Request $request) {
		// Log::debug($request->all());
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$request = $request->all();
		$commEvent = CommunicationEvent::find($request['communication_event_id']);
		$program = Program::find($request['program_id']);
		$data = $request;
		$data['authenticatedUser'] = 1; //\Auth::user();
		$data['program_name'] = $program->name;
		$data['start_date'] = $program->start_date;
		$data['end_date'] = $program->end_date;
		$this->sendEmailViaCommEvent($commEvent->name, $data);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

	}
	public function sendEmailViaCommEvent($eventName, $data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$authenticatedUser = $data['authenticatedUser'];
		$login_id = $data['authenticatedUser'];
		$participant_ids = explode(',', $data['participants']);
		foreach ($participant_ids as $key => $id) {
			$data['Participant'][$id] = self::getData($id, $data, 'CGX', 'Participant');
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		Log::debug($data);
		event(new CommEvent($eventName, $data, 'CGX', \Auth::user()->id));
		// event(new CommEvent("", $data, $tenant_code, $login_id));
	}
	public static function getData($id, $entityData, $customer_org_code, $role_name) {
		//
		$attachments = array();
		if (!empty($entityData['attachments'])) {
			$attachments = explode(',', $entityData['attachments']);
		}
		$user_object = self::getUserObject($id);

		// finding phone number
		$phone = Phone::where('party_id', $user_object['party_id'])->first();

		// creating data
		$data['template_data'] = ['FIRST_NAME' => $user_object['first_name'],
			'LAST_NAME' => $user_object['last_name'],
			'ROLE_NAME' => $role_name,
			'PROGRAM_NAME' => $entityData['program_name'],
			'START_DATE' => $entityData['start_date'],
			'END_DATE' => $entityData['end_date'],
			'USER_ID' => $user_object['userid'],
			'PASSWORD' => 'pass@123',
		];

		$data['email_id'] = $user_object['email'];
		$data['phone_no'] = $phone['phone_number'];
		$data['customer_organization_code'] = $customer_org_code;
		$data['party_id'] = $user_object['party_id'];
		$data['attachments'] = $attachments;

		return $data;
	}
	private static function getUserObject($user_id) {
		$user = User::where('id', $user_id)->first();
		return $user;
	}
}
