<?php

namespace App\Services;

//Custom importsr

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;


use App\Models\Receipt;
use App\Models\Project;
use App\Models\Boq;
use Carbon\Carbon;

/**
 * Class AddressMasterService
 *
 */
class ReceiptService {
	use ValidationTrait;

	public static function retrieveReceipts() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Receipt::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

	public static function saveReceipt($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		try {
			$userId = $data['authenticatedUser']->id;
			$obj = new Receipt;
			$obj->project_id = $data['project_id'];
			$obj->payment_no = $data['project_id'].'000';
			$obj->boq_no = $data['boq_no'];
			$obj->invoice_no = strtoupper($data['invoice_no']);
			$obj->invoice_date = $data['invoice_date'];
			$obj->payment_method = $data['payment_method'];
			$obj->payment_ref_no = $data['payment_ref_no'];
			$obj->payment_receipt_date = $data['payment_receipt_date'];
			$obj->amount_received = $data['amount_received'];
			$obj->gst_amount = $data['gst_amount'];
			$obj->base_amount = $data['base_amount'];
			$obj->invoice_amount = $data['invoice_amount'];
			$obj->tds_deduction = $data['tds_deduction'];
			$obj->other_deduction = $data['other_deduction'];
			$obj->total_deduction = $data['other_deduction'] +  $data['tds_deduction'];

			$obj->remark = $data['remark'];
			$obj->mobilization = $data['mobilization'];
			$obj->retension = $data['retension'];
//				$obj->total_amount = $data['total_amount'];
			$obj->created_by = $userId;
			$obj->updated_by = $userId;
			$obj->save();	

			$receipt_id = $obj->id;		

			Receipt::where('id', $receipt_id)->update([
				'payment_no' => $data['project_id']."000".$receipt_id,
			]);

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

			$res = true;
		} catch (Exception $e) {
			Log::error('Exception while updating expense :: ', $e->getMessage());
			$res = false;
		}
		return $res;

	}
	
	public static function updateReceipt($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		try {
			$userId = $data['authenticatedUser']->id;
			$obj_receipt = Receipt::where('id', $data['id'])->update([
				'project_id'=> $data['project_id'],
				'boq_no'=> $data['boq_no'],
				'invoice_no' => strtoupper($data['invoice_no']),
				'invoice_date' => $data['invoice_date'],
				'payment_method' => $data['payment_method'],
				'payment_ref_no' => $data['payment_ref_no'],
				'payment_receipt_date' => $data['payment_receipt_date'],
				'amount_received' => $data['amount_received'],
				'gst_amount' => $data['gst_amount'],
				'base_amount' => $data['base_amount'],
				'invoice_amount' => $data['invoice_amount'],
				'tds_deduction' => $data['tds_deduction'],
				'other_deduction' => $data['other_deduction'],
				'total_deduction' => $data['other_deduction'] + $data['tds_deduction'],
				'remark' => $data['remark'],
				'retension' => $data['retension'],
				'mobilization' => $data['mobilization'],
			//'total_amount' => $data['total_amount'],

				'created_by' => $userId,
				'updated_by' => $userId,
			]);

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

			$res = true;
		} catch (Exception $e) {
			Log::error('Exception while updating expense :: ', $e->getMessage());
			$res = false;
		}
		return $res;

	}

	public static function deleteReceipt($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$userId = auth()->user()->id;
			$receipt = Receipt::where('id', $id);
			$receipt->update(['deleted_by' => $userId]);
			$check = $receipt->delete();
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	//-------------------- import code

	public static function verifyReceiptImportData($data,$project_id,$route='create') {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data,$project_id,$route) {

			$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;

			// $data['tempPath'] = "temp/projects/" . $project_id . "/receipt/receipt_imports/";
			$data['tempPath'] = "temp/receipt/receipt_imports/";
			$data['finalPath'] = "public/receipt/receipt_imports/";

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$class = $data['configurations']['storage_implementation_class'];
			$storageUtility = new $class;
			$updateInfo = $storageUtility->uploadFileToPrivateStorage($data['request'], $data['tempPath']);

			$uploadedFile = $updateInfo['uploadedFileNames']['imported_receipt_file'][0];
			$uploadedFile_ogrName = $updateInfo['originalFileNames']['imported_receipt_file'][0];
			$fileName = $uploadedFile;
			$folder = storage_path() . "/app/" . $data['tempPath'];
			
			if (isset($folder)) {
				Log::debug("**************FOLDER*************");
				Log::debug($folder);

				try {
					//carry out verification
					$excel = Excel::load($folder . '/' . $fileName);	
					$excelData = $excel->all()->toArray();	
					$sheetNames = $excel->getSheetNames();	
					if(count($sheetNames)>1){
						$importData = $excelData[0];
					}else {
						$importData = $excelData;
					}				   

// dd($importData);
					$keys = array();
					$validUsers = array();
					$invalidUsers = array();
					$manualErrors = array();
					$possibleDuplicateUserId = false;
					$possibleDuplicateEmailId = false;
					$possibleDuplicatePhotoPath = false;
					$isInvalid = false;
					//The following two are used to check if there are duplicate valies in the same file
					//Unique checks work only against records already present in the database
					//Hence we check for uniqueness in the file manually
					$userIdArray = array();
					$emailIdArray = array();
					$photoPathArray = array();
					
					if (!empty($importData)) {
						
						$duplicateCheckArray = array();
						foreach ($importData as $key => $value) {

						//dd($importData[$key]['invoice_no']);
							$importData[$key]['boq_no'] = strval(trim($importData[$key]['boq_no']));
							$importData[$key]['invoice_no'] = strtoupper(strval(trim($importData[$key]['ra_bill_no'])));
							$importData[$key]['gst_amount'] = strval(trim($importData[$key]['gst_amount']));
							$importData[$key]['base_amount'] = strval(trim($importData[$key]['base_amount']));
							$importData[$key]['payment_ref_no'] = strval(trim($importData[$key]['payment_ref_no']));
							if(!empty($importData[$key]['amount_received']))
								$importData[$key]['amount_received'] = strval(trim($importData[$key]['amount_received']));

							if(!empty($importData[$key]['tds_deduction']))
								$importData[$key]['tds_deduction'] = trim($importData[$key]['tds_deduction']);
							
							if(!empty($importData[$key]['emd']))
								$importData[$key]['other_deduction'] = trim($importData[$key]['emd']);
							else $importData[$key]['other_deduction'] = $importData[$key]['emd'];

							$importData[$key]['remark'] = strval(trim($importData[$key]['remark']));												

							$isInvalid = false;
							$messageBag = new MessageBag();
							$newUser = $value;
							$rules = [
				              //  'boq_no'=>'unique:receipts,boq_no,NULL,id,deleted_at,NULL,invoice_no,'.strval($importData[$key]['invoice_no']),
								'ra_bill_no' => 'required|max:100|unique:receipts,invoice_no,NULL,id,deleted_at,NULL,project_id,'.$project_id,
								'payment_method' => 'nullable|max:50|in:Cash,Cheque,Online,Other',
								'gst_amount' => 'required|numeric|max:9999999.99|min:0',
								'base_amount' => 'required|numeric|max:9999999.99|min:0',
								'payment_ref_no' => 'nullable|numeric|max:999999999|min:0',
								'amount_received' => 'nullable|numeric|max:9999999.99|min:0',
								'tds_deduction' => 'nullable|numeric|max:9999999.99|min:0',
								'emd' => 'nullable|numeric|max:9999999.99|min:0',
								'remark' => 'nullable|max:250',
								'invoice_date' => 'required|before_or_equal:today',
								'payment_receipt_date' => 'nullable|before_or_equal:today',
								'retension' => 'nullable|numeric|max:9999999.99|min:0',
								'mobilization' => 'nullable|numeric|max:9999999.99|min:0',

							]; 

							$custmessage = [
								'unique' => 'The R A Bill No. has already been taken.',
								'in' => 'Wrong payment method entered. Payment method should be:Cash,Cheque,Online,Other.',

							];

						// excel sheet validation start 
							// $duplicate_boq_result['success']=true;
							// $duplicate_boq_result['message']='Validation successful';
							// if($importData[$key]['boq_no'] != null || $importData[$key]['boq_no'] != '' || !empty($importData[$key]['boq_no'])){
							// 	if(!array_key_exists($importData[$key]['boq_no'], $duplicateCheckArray)){
							// 		$duplicateCheckArray[$importData[$key]['boq_no']] = array();

							// 	}
							// 	if(in_array($importData[$key]['invoice_no'], $duplicateCheckArray[$importData[$key]['boq_no']])){
							// 		$duplicate_boq_result['success']=false;
							// 		$duplicate_boq_result['errors'] ="You can not enter same R A Bill number against same boq number.";
							// 	} else {
							// 		$duplicateCheckArray[$importData[$key]['boq_no']][] = $importData[$key]['invoice_no'];
							// 		$duplicate_boq_result['success']=true;
							// 		$duplicate_boq_result['message']='Validation successful';
							// 	}
							// }

     					// excelsheet validation ends.
							if (trim($newUser['boq_no']) =='' || $newUser['boq_no'] == null ) {
								$validteBoq['success']=true;
								$validteBoq['message']='Validation successful';
							}else {
								$validteBoq = self::compareBoqNo($newUser['boq_no'],$project_id);
							}
							$validationResult = self::validateInputData($newUser, $rules,$custmessage);


							// validate duplicate boq and invoice number				


							if ($validteBoq['success'] == false) {
								if ($isInvalid == true) {
									foreach ($messageBag->getMessages() as $msgKey => $msgValue) {
										$validteBoq['errors']->add($msgKey, $msgValue[0]);
									}
								}
								$value["errors"] = $validteBoq['errors'];
								$invalidUsers[] = $value;
								foreach ($value as $key => $val) {
									if (array_key_exists($key, $keys)) {
									} else {
										continue;
										array_push($keys, $key);
									}
								}
							} 
							// elseif ($duplicate_boq_result['success'] == false) {
							// 	# code...
							// 	if ($isInvalid == true) {
							// 		foreach ($messageBag->getMessages() as $msgKey => $msgValue) {
							// 			$duplicate_boq_result['errors']->add($msgKey, $msgValue[0]);
							// 		}
							// 	}
							// 	$value["errors"] = $duplicate_boq_result['errors'];
							// 	$invalidUsers[] = $value;
							// 	foreach ($value as $key => $val) {
							// 		if (array_key_exists($key, $keys)) {
							// 		} else {
							// 			continue;
							// 			array_push($keys, $key);
							// 		}
							// 	}
							// }
							else {
								if ($validationResult['success'] == false) {
									if ($isInvalid == true) {
										foreach ($messageBag->getMessages() as $msgKey => $msgValue) {
											$validationResult['errors']->add($msgKey, $msgValue[0]);
										}
									}
									$value["errors"] = $validationResult['errors'];
									$invalidUsers[] = $value;
									foreach ($value as $key => $val) {
										if (array_key_exists($key, $keys)) {
										} else {
											continue;
											array_push($keys, $key);
										}
									}
								}
							}
							

						}
					}
					if (count($invalidUsers) > 0) {

						return $invalidUsers;
					}
					self::saveReceiptImportData($importData,$project_id);				

					Log::debug("BoqUploadEvent Called");
					return 'SUCCESS';
				} catch (ModelNotFoundException $exception) {
					Log::error('Exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
					return "MODEL_NOT_FOUND";
				} catch (Exception $exception) {
					Log::error('Exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
					return "some_error";

				} catch (\Throwable $exception) {
					Log::error('Throwable exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
					return "some_error";
				}

			}

		});
Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
return $check;

}
public static function saveReceiptImportData($data,$project_id)
{
	$counter = 0;

	foreach ($data as $key => $value) {
		$data['authenticatedUser'] = \Auth::user();
		$userId = $data['authenticatedUser']->id;
		//	$check = DB::transaction(function () use (&$value,$project_id,$counter, $userId) {

// check unique validation


		$obj = new Receipt;
		Log::debug("########## invoice_no ### " . $value['invoice_no']);

		$obj->project_id = $project_id;
		$obj->payment_no = $project_id.'000';
		$obj->boq_no = $value['boq_no'];
		$obj->invoice_no = $value['invoice_no'];
				//$obj->invoice_date = $value['invoice_date'];
		$invoice_date = date("Y-m-d", strtotime($value['invoice_date']));
		$obj->invoice_date = $value['invoice_date'];
		$obj->payment_method = $value['payment_method'];
		$obj->payment_ref_no = $value['payment_ref_no'];
		$obj->payment_receipt_date = $value['payment_receipt_date'];
		$obj->amount_received = $value['amount_received'];
		$obj->gst_amount = $value['gst_amount'];
		$obj->base_amount = $value['base_amount'];
		$obj->invoice_amount = strval(strval($value['base_amount']) + strval($value['gst_amount']));
		$obj->tds_deduction = $value['tds_deduction'];
		$obj->other_deduction = $value['other_deduction'];
		$obj->remark = $value['remark'];
		$obj->retension = $value['retension'];
		$obj->mobilization = $value['mobilization'];
				//$obj->total_amount = $value['total_amount'];
		$obj->created_by = $userId;
		$obj->updated_by = $userId;
				//dd($obj);
		$obj->save();	

		$receipt_id = $obj->id;		

		Receipt::where('id', $receipt_id)->update([
			'payment_no' => $project_id."000".$receipt_id,

		]);
		//});
	}
	return true;
}



public static  function getReceiptDataForExport($project_id)
{
	Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
	$data = Receipt::select('payment_no','boq_no', 'invoice_no','invoice_date', 
		'payment_method', 'payment_ref_no', 'payment_receipt_date','amount_received',
		'gst_amount', 'invoice_amount','tds_deduction','tds_deduction','remark' )
	->where('project_id', $project_id)
	->orderBy('boq_no', 'asc')->get();
	Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

		//dd($data);
	return $data;

}	

public static  function compareBoqNo($boq_no,$project_id)
{
	Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
	$data = Boq::select('name')
	->where('name', $boq_no)
	->where('project_id', $project_id)
	->get();
	Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
	if(count($data)>0){
		$validationResults['success']=true;
		$validationResults['message']='Validation successful';		
	}else {
		$validationResults['success']=false;
		$validationResults['errors']= 
		"Wrong Boq no ".$boq_no." entered. There is no such a boq no against selected project.";
	}
	return $validationResults;

}

}
