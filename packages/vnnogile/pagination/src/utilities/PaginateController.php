<?php

namespace vnnogile\Pagination\Utilities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

//Custom Templates
class PaginateController extends Controller
{

    public static function paginate($model, $route, UtilsInterface $utilsInterface, $masterDataInterface, $flag)
    {
        Log::debug('PaginateController::paginate started');
        try {
	     $request = request();
            if($request->has('rowCount')){
                session(['rowCount' => $request['rowCount']]);
            }else if (session()->has('rowCount')) {
                $request['rowCount'] = session('rowCount');
            }
            $entityColumns = EntityUtility::getEntityQueryColumnsAndDisplayColumns($model, $masterDataInterface);
            $paginationData = PaginationUtility::getRowCountAndRoutesData(request(), $route, $utilsInterface, $masterDataInterface);
            $data = ListingUtility::generateQueryForListView(request(), $entityColumns['records'], $entityColumns['queryColumnNames'], $entityColumns['filterColumnNames'], $masterDataInterface, $utilsInterface, $entityColumns['entityName'], $entityColumns['filterClass']);
            $records = EntityUtility::fireQuery($data['records'], $paginationData['rowCount'], $data['queries'], $entityColumns['relations'], $entityColumns['replace_columns']);
            Log::debug('PaginateController::paginate ended');
            $finalRoute = '';
            $splitRoute = preg_split('/(?=[A-Z])/', $paginationData['route']);
            foreach ($splitRoute as $value) {
                if ($finalRoute == '') {
                    $finalRoute = $value;
                } else {
                    $finalRoute = $finalRoute . ' ' . $value;
                }
            }
            $paginationData['finalRoute'] = $finalRoute;
            if ($flag) {
                $data['entityColumns'] = $entityColumns;
                $data['records'] = $records;
                $data['paginationData'] = $paginationData;
                $paginateObj['vnnogile::view_list'] = $data;
                return $paginateObj;
            } else {
                return view('vnnogile::view_list', compact('entityColumns', 'records', 'paginationData'));
            }
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e);
        }
    }
    public static function paginateByQuery($route, $entityName, UtilsInterface $utilsInterface, $masterDataInterface)
    {
        $entityColumns = EntityUtility::getQueryColumnsAndDisplayColumnsByQuery($entityName);
        $paginationData = PaginationUtility::getRowCountAndRoutesData(request(), $route, $utilsInterface, $masterDataInterface);
        $records = ReportUtility::getReportDataFromName(request(), $entityName, $paginationData['rowCount'], $entityColumns['queryColumnNames']);
        // print_r($records);
        // exit();
        $data['entityColumns'] = $entityColumns;
        $data['records'] = $records;
        $data['paginationData'] = $paginationData;
        $paginateObj['vnnogile::view_list'] = $data;
        return $paginateObj;
    }
    public function getRowCountList(UtilsInterface $utilsInterface, MasterDataInterface $masterDataInterface)
    {
        $rowCount = $utilsInterface->getRowCountList($masterDataInterface);
        return $rowCount;
    }
}
