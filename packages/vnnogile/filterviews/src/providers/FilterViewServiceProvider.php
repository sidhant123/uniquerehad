<?php

namespace vnnogile\FilterViews\Providers;

use Illuminate\Support\ServiceProvider;

class FilterViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->app->make('vnnogile\FilterViews\controllers\FilterController');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../controllers/FilterController.php';
        $this->app->bind('vnnogile\FilterViews\Interfaces\FilterInterface', function ($app) {
            return new \vnnogile\FilterViews\Interfaces\FilterInterface;
        });
         
    }
}
