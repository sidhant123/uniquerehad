<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class ProgramRole
 * 
 * @property int $id
 * @property int $app_role_id
 * @property int $prg_role_id
 * @property string $role_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Role $role
 *
 * @package App\Models
 */
class ProgramRole extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'program_role';

	protected $casts = [
		'app_role_id' => 'int',
		'prg_role_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'app_role_id',
		'prg_role_id',
		'role_name',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class, 'app_role_id');
	}
}
