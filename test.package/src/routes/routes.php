<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('listTemplates','vnnogile\Utilities\Utilities\TemplateController@index')->name('listTemplates')->middleware('CheckCompositeMiddleWare');


Route::group(['middleware' => ["web", "auth", "CheckRole:Tenant Administrator"]], function () {
	Route::get('/getEmailData', 'vnnogile\Templates\Controllers\EmailAddressController@getEmailData');

	Route::get('/getCommunicationChannel', 'vnnogile\Templates\Controllers\CommunicationChannelController@getCommunicationChannels')->name('getCommunicationChannel');

	Route::resource('templates','vnnogile\Templates\Controllers\TemplateController');	

	Route::get('/getTemplate/{id}', 'vnnogile\Templates\Controllers\TemplateController@getTemplateData');

	Route::get('/getAttachedCommEvents/{template_id}', 'vnnogile\Templates\Controllers\TemplateController@getAttachedCommEvents');

	Route::get('/placeholders/{conn_event_id}', 'vnnogile\Templates\Controllers\TemplateController@getPlaceHoldersFromCommEvent');

	Route::get('/communicationEvents', 'vnnogile\Templates\Controllers\TemplateController@getAllCommunicationEvents')->name("communicationEvents");

});
