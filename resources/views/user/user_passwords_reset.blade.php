@extends('layouts.loginBase')
@section('content')
 <section id="login">
    <div class="container">
        <h2>Admin Panel Reset Password</h2>
        <div class="box">
            <form method="post" action="{{ route('password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" id="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    <span>Email</span>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required>
                    <span>Password</span>
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>


                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    <span>Password Confirmation</span>
                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                </div>

                <br/>
                <div class="clearfix"></div>
                <button type="submit" class="share-btn">Password Reset</button>
                <div class="clearfix"></div>
            </form>
            <p>By login in, you agree to the <a href="#">Terms & Conditions</a> of Cognegix Digital Learning
                Redefined</p>
        </div>
    </div>
</section>
@endsection