var _fileValidatorObject = {};
$(document).on('change', '#artefact_id', populateMetaData);
$(document).ready(function () {

  $("#start_date_id").datepicker({
    dateFormat: "dd-mm-yy",
    minDate: new Date(),
  });

  $("#end_date_id").datepicker({
    dateFormat: "dd-mm-yy",
    minDate: new Date(),
  });
  setStartAndEndDate();
  var artefact_id = $("#artefact_id").val();
  var isTaggable = $("#isTaggable").val();
  console.log(artefact_id);
  console.log(isTaggable);
  if (artefact_id) {
    populateMetaData(true);
  }
  if (isTaggable) {
    var tagFromHidden = $('#tag_name').val();
    var tags = '';
    console.log('tags');
    console.log(tags);
    if (typeof tagFromHidden != 'undefined') {
      var tagsArray = JSON.parse(tagFromHidden);
      tags = tagsArray.join();
    }
    var tagHtml = ''; //$('.multiple').html();
    $('.multiple').each(function () {
      tagHtml = tagHtml + $(this).html();
    });
    var html = tagHtml.replace('value=""', 'value="' + tags + '"');
    console.log('html');
    console.log(html);
    $('#compositeDiv').append(html);
  }

  //code for on external checked
  $(document).on('change', '#external_doc_id', function () {
     var doc_type = $("#artefact_id option:selected").text();
    if (this.checked == true) {
      showExternalLinkDiv();
      if(doc_type == 'Document'){
        showExternalDocType(true);
      }
      
    } else {
      hideExternalLinkDiv();
        showExternalDocType(false);
    }
  });
  if ($('#external_doc_id').is(':checked')) {
    $('#external_doc_id').trigger('change');
  }
showIcon();
});

function showExternalLinkDiv() {
  $('.channel_partners_div').removeClass('hide');
  $('.external_document_type').removeClass('hide');
  $('#external_document_type_id').attr('disabled',true);
  $('#file_id').addClass('pointerEventNone');
  $('#file_id').removeClass('mandatory');
  // $('#channel_partners_id').addClass('mandatory');
  $('#external_link_id').addClass('mandatory');
  $('#document_name_id').addClass('mandatory');
  $('#document_name_id').attr('readonly', false);
}

function hideExternalLinkDiv() {
  $('.channel_partners_div').addClass('hide');
  $('.external_document_type').addClass('hide');
  $('#file_id').removeClass('pointerEventNone');
  $('#file_id').addClass('mandatory');
  // $('#channel_partners_id').removeClass('mandatory');
  $('#external_link_id').removeClass('mandatory');
  $('#document_name_id').removeClass('mandatory');
  $('#document_name_id').attr('readonly', true);
  $('#document_name_id').val('');
}

function populateMetaData(isPrefilled) {
  var id = $("#artefact_id").val();
  var doc_type = $("#artefact_id option:selected").text();
  if (id > 0 && isPrefilled) {
    getDataFromAxios('/getArtefactDetails', id);
  } else if (id > 0) {
    clearFields();
    setErrorMsg();
    getDataFromAxios('/getArtefactDetails', id);
  }
  populateAttrOnChange(doc_type);
  if ($('#external_doc_id').is(':checked')) {
    $('#external_doc_id').trigger('change');
  }
}
$(document).on('change', '#start_date_id', function () {
  var start_date = $(this).val();
  console.log(start_date);
  var array_start_date = start_date.split('-');
  var corrected_start_date = new Date(array_start_date[2], array_start_date[1], array_start_date[0]);
  var end_date_id = $("#end_date_id").val();
  var array_end_date_id = end_date_id.split('-');
  var corrected_end_date_id = new Date(array_end_date_id[2], array_end_date_id[1], array_end_date_id[0]);
  console.log(start_date);
  $('#end_date_id').datepicker('destroy');
  $("#end_date_id").datepicker({
    dateFormat: "dd-mm-yy",
    minDate: start_date,
  });
  if (corrected_start_date > corrected_end_date_id) {
    $("#end_date_id").val('');
  }
});
$(document).on('change', '#file_id', doValidations);

function doValidations() {
  if (!Object.keys(_fileValidatorObject).length > 0) {
    setErrorMsg('Please select the document type.');
    $(this).val('');
    return false;
  }
  var file = $(this)[0].files[0];
  var doc_type = $("#artefact_id option:selected").text();
  console.log(_fileValidatorObject);
  switch (doc_type) {
  case 'Audio':
    if (validateFileExtensions(file, _fileValidatorObject['allowed_extensions'], 'file_id')) {
      if (validateFileSize(file, _fileValidatorObject['max_size'], _fileValidatorObject['max_size_unit'], 'file_id')) {
        if (validateFileDuration(file, doc_type)) {}
      }
    }
    break;
  case 'Video':
    if (validateFileExtensions(file, _fileValidatorObject['allowed_extensions'], 'file_id')) {
      if (validateFileSize(file, _fileValidatorObject['max_size'], _fileValidatorObject['max_size_unit'], 'file_id')) {
        if (validateFileDuration(file, doc_type)) {}
      }
    }
    break;
  case 'Document':
    if (validateFileExtensions(file, _fileValidatorObject['allowed_extensions'], 'file_id')) {
      if (validateFileSize(file, _fileValidatorObject['max_size'], _fileValidatorObject['max_size_unit'], 'file_id')) {}
    }
    break;
  default:
    setErrorMsg(doc_type + ' document type is not configured.');
    $('#file_id').val('');
  }
}

function validateFileSize(file, size, sizeUnit, control_id) {
  var maxConvertedSize = formatFileSize(size, sizeUnit);
  if (file.size < maxConvertedSize) {
    setErrorMsg();
    var fileInMB = Number(file.size) / (1024 * 1024);
    $('#size_id').val(fileInMB.toFixed(2));
    $('#document_name_id').val(file.name);
    return true;
  } else {
    $('#' + control_id).val('');
    setErrorMsg('File size cannot be greater than max size ' + size + sizeUnit + '.');
    return false;
  }
}

function validateFileDuration(file, fileType) {
  console.log("validateFileDuration" + file.duration);
  var durations = [];
  window.URL = window.URL || window.webkitURL;
  durations.push(file);
  var content = document.createElement(jsUcfirst(fileType));
  content.preload = 'metadata';
  content.onloadedmetadata = function () {
    window.URL.revokeObjectURL(this.src)
    var duration = content.duration;
    durations[durations.length - 1].duration = duration;
    $('#duration_id').val(durations[0].duration.toFixed(2));
    console.log("duration" + durations[0].duration);
  }
  content.src = URL.createObjectURL(file);
}

function validateFileExtensions(file, extentions, control_id) {
  console.log(extentions);
  var exts = extentions.split(',');
  var filename = file.name;
  var fileExtention = filename.split('.').pop();
  console.log(exts.indexOf(fileExtention));
  if (exts.indexOf(fileExtention) !== -1) {
    setErrorMsg();
    return true;
  } else {
    $('#' + control_id).val('');
    clearFields();
    setErrorMsg('File extension does not match with the allowed extensions.');
    return false;
  }

}

function validateMandatoryFields() {
  var valid = true;
  console.log("In validateMandatoryFields function.");
  $(".mandatory").each(function () {
    if ($(this).val().trim() === '' || $(this).val() === '0') {
	var scrollPos =  $(this).offset().top;
      $(".mandatory").removeClass('fillFields');
      $(this).addClass('fillFields');
	$('html, body').animate({scrollTop:(Number(scrollPos) - 72)}, 'slow');
      valid = false;
      return valid;
    }
  });
  if ($("#tag_div_id").length) {
    $('#tag_hidden_field').val($('#tag_name_id').val());
  }
  if (valid) {
    var url = '/documents'
      console.log("in valid");
    if ($('#is_update').val() == 'N') {
      uploadDocument(url);
      console.log(url);
    } else {
      console.log("else");
      var document_id = $('#document_id').val();
      console.log(document_id);
      if (document_id > 0) {
        url = url + '/' + document_id;
        uploadDocument(url);
        console.log(url);
      }
    }
  }
  console.log(valid);
  return valid;
}

function getDataFromAxios(url, id) {
  axios.get(url + '/' + id)
  .then(function (response) {
    console.log(response);
    if (Object.keys(response).length > 0) {
      $.each(response['data'], function (key, value) {
        if (value['properties'].length > 0) {
          var validations = '';
          var properties = value['properties'][0];
          _fileValidatorObject = properties;
          for (var key in properties) {
            validations = validations.concat(key.replace('_', ' ') + " -> " + properties[key] + '&#13;&#10;');
            $('#artefact_properties_id').html(validations);
            if (key == 'max_size') {
              $('#size_id').attr('placeholder', properties[key]);
            }
            if (key == 'max_duration') {
              $('#duration_id').attr('placeholder', properties[key]);
            }
            if (key == 'max_pages') {
              $('#pages_id').attr('placeholder', properties[key]);
            }
          }
        } else {
          $('#artefact_properties_id').empty();
        }
      });
    }
  })
  .catch(function (error) {
    console.log("error" + error);
  });
}

function formatFileSize(size, unit) {
  var converted_unit = 0;
  console.log(unit);
  switch (unit) {
  case 'KB':
    converted_unit = Number(size * 1024);
    break;
  case 'MB':
    converted_unit = Number(size * (1024 * 1024));
    break;
  case 'GB':
    converted_unit = Number(size * (1024 * (1024 * 1024)));
    break;
  default:
    converted_unit = 0;
  }
  return converted_unit;
}

function populateAttrOnChange(doc_type) {
  switch (doc_type) {
  case 'Audio':
    showPages(false);
    showDuration(true);
    showExternalDocType(false);
    break;
  case 'Video':
    showPages(false);
    showDuration(true);
    showExternalDocType(false);
    break;
  case 'Document':
    showPages(true);
    showDuration(false);
    populateDocumentExtensions();
    break;
  default:
    converted_unit = 0;
  }
}
function populateDocumentExtensions(){
  $('#external_document_type_id').empty();
  axios.get('/document_types')
     .then(function(response) {
       console.log(response);
    $('#external_document_type_id').append('<option value="0">---Select---</option>');
    if(response['data'].length > 0){
      $.each(response['data'], function(index, val) {
         /* iterate through array or object */
         $('#external_document_type_id').append('<option value="'+val+'">'+val+'</option>');
      });
      if($('#external_document_type_id_hidden').val()){
        console.log($('#external_document_type_id_hidden').val());
        $('#external_document_type_id').val($('#external_document_type_id_hidden').val());
      }
    }      
     })
     .catch(function(error) {
       console.log("error" + error);
     });
}
function setErrorMsg(msg = false) {
  if (msg) {
    $('.errorMessage').html(msg);
  } else {
    $('.errorMessage').html('');
  }

}

function showDuration(flag) {
  if (flag) {
    $('#duration_div').removeClass('hide');
    $('#duration_div').addClass('show');
  } else {
    $('#duration_div').addClass('hide');
    $('#duration_div').removeClass('show');
  }
}

function showPages(flag) {
  if (flag) {
    $('#pages_div').removeClass('hide');
    $('#pages_div').addClass('show');
  } else {
    $('#pages_div').addClass('hide');
    $('#pages_div').removeClass('show');
  }
}
function showExternalDocType(flag){
if (flag) {
    // $('.external_document_type').removeClass('hide');
    $('.external_document_type').addClass('show');
    $('#external_document_type_id').addClass('mandatory');
    $('#external_document_type_id').attr('disabled',false);
  } else {
    // $('.external_document_type').addClass('hide');
    $('#external_document_type_id').attr('disabled',true);
    $('.external_document_type').removeClass('show');
    $('#external_document_type_id').removeClass('mandatory');
  }
}
function jsUcfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function clearFields() {
  $('#document_name_id').val('');
  $('#file_id').val('');
  // $('#start_date_id').val('');
  // $('#end_date_id').val('');
  $('#size_id').val('');
  $('#duration_id').val('');
  $('#pages_id').val('');
}

function uploadDocument(url) {
  console.log("Upload Document");
  $('.alert-danger').addClass('hide');
  $('.alert-danger').children().find('li').remove();
  if ($('#external_doc_id').is(':checked') == false) {
    $('.progress').removeClass('hide');
  }
  if ($('#file_id').val() == "") {
    $('.progress').addClass('hide');
    console.log("empty");
  }
  var myform = document.getElementById("documentForm");
  var fd = new FormData(myform);
  // var formData = $('#documentForm').serialize();
  console.log(fd);
  $.ajax({
    url: url,
    data: fd,
    processData: false,
    contentType: false,
    type: 'POST',
    // this part is progress bar
    xhr: function () {
      var xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener("progress", function (evt) {
        if (evt.lengthComputable) {

          var percentComplete = evt.loaded / evt.total;
          // console.log(percentComplete);
          percentComplete = parseInt(percentComplete * 100);
          $('.myprogress').text(percentComplete + '%');
          $('.myprogress').css('width', percentComplete + '%');
        }
      }, false);
      return xhr;
    },
    success: function (data) {
      console.log(data);
      if (data.indexOf("Document Uploaded Successfully.") != -1) {
        $('.successMessage').html(data);
        $('.progress').addClass('hide');
        // $(window).scrollTop(0);
         $('html, body').animate({scrollTop:0}, 'slow');
        window.location = "/documents";
      }

    },
    error: function (data) {
      console.log(data);
      $('.alert-danger').removeClass('hide');
      
      console.log(data.responseJSON.errors);
      $.each(data.responseJSON.errors, function (index, val) {
         // iterate through array or object 
        $('.alert-danger').children().append('<li>' + val + '</li>');
      });
      $(window).scrollTop(0);
      $('html, body').animate({scrollTop:0}, 'slow');
    }
  });
}
function setStartAndEndDate() {
  var document_end_year = $('#document_end_year').val();
  if (document_end_year) {
    var date = new Date();
    $("#start_date_id").datepicker("setDate", date);
    $("#end_date_id").datepicker("setDate", new Date(date.getFullYear() + Number(document_end_year), date.getMonth(), date.getDate()));
  }
}
function showIcon(){
  var icon_html = '<div class="col-lg-6">\
      <div class="form-group">\
        <label for="file_id">Document Photo</label>\
        <input type="file" class="form-control" id="document_photo_id" name="document_photo">\
      </div>\
    </div>';
    if($('#star_rating_div').hasClass('hide')){
      $('#rating_div').append(icon_html);
    }else{
      $('#doc_icon').removeClass('hide');
       $('#doc_icon').append(icon_html);
    }
}
