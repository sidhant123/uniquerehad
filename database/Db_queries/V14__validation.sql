


-- insert script 
delete from validations where id = 1;

INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Project', 'Update', 'name', 'required|max:255|unique:projects,name,$project,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Project', 'Create', 'name', 'required|max:255|unique:projects,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1);
   
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('User', 'Both', 'firstName', 'required|max:255', 1, now(), now(), 1, 1),
     ('User', 'Both', 'lastName', 'required|max:255', 1, now(), now(), 1, 1),
     ('User', 'Both', 'primaryContact', 'required|max:10', 1, now(), now(), 1, 1),
     ('User', 'Update', 'email', 'required|max:255|unique:people,email,$user,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('User', 'Create', 'email', 'required|max:255|unique:people,email,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1);

-- -------

update list_view_masters set filter_class_names = 'App\Utilities\ProjectFilterUtility',
query_columns='id,name,start_date,end_date,s_first_name,c_first_name,id', 
heading_columns ='Id,Name,Start date,End Date,Site Engineer,Client,Action',
display_css_widths='col-xs-1,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-1'
 where id = 8;


update menus set icon = 'glyphicon glyphicon-th-list' where id  = 80;


INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Organization', 'Update', 'organization_name', 'required|max:255|unique:organizations,name,$company,id,deleted_at,NULL,tenant_party_id,$tenantPartyId', 1, now(), now(), 1, 1),
     ('Organization', 'Create', 'organization_name', 'required|max:255|unique:organizations,name,NULL,id,deleted_at,NULL,tenant_party_id,$tenantPartyId', 1, now(), now(), 1, 1),
     ('Organization', 'Update', 'organization_code', 'required|max:20|unique:organizations,organization_code,$company,id,deleted_at,NULL,tenant_party_id,$tenantPartyId', 1, now(), now(), 1, 1),
     ('Organization', 'Create', 'organization_code', 'required|max:20|unique:organizations,organization_code,NULL,id,deleted_at,NULL,tenant_party_id,$tenantPartyId', 1, now(), now(), 1, 1),
     ('Organization', 'Both', 'organization_url', 'required', 1, now(), now(), 1, 1);