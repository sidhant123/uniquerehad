<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;


class Boq extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
	
	];

	// protected $fillable = [
	// 	'name',
	// 	'desription',
	// 	'client_user_id',
	// 	'project_id',
	// 	'created_date',
	// 	'created_by',
	// 	'updated_by',
	// 	'deleted_by'
	// ];
	public function BoqItem()
	{
		return $this->hasMany(\App\Models\BoqItem::class, 'boq_id');
	}

}
