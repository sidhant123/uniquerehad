<?php

namespace vnnogile\Sms\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//custom import
use App\Models\CommunicationTrail;
use Illuminate\Support\Facades\Log;

class SmsSuccessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $trail_data;
    public $tries = 5;
    
    public function __construct($trail_data)
    {
        $this->trail_data = $trail_data;
    }


    public function handle()
    {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " started---");

            $trail_data=$this->trail_data;

            $communicationTrail=new CommunicationTrail();
            $communicationTrail->error_code="no error";
            $communicationTrail->exception="no exception";
            $communicationTrail->status="success";

            if (array_key_exists('customer_organization_code', $trail_data)){
                $communicationTrail->communication_event_id=$trail_data['communication_event_id'];
            }
            

            $communicationTrail->created_by=$trail_data['login_id'];
            $communicationTrail->updated_by=$trail_data['login_id'];
            if (array_key_exists('customer_organization_code', $trail_data)){
                $communicationTrail->customer_organization_code=$trail_data['customer_organization_code'];
            }            
            
            if (array_key_exists('tenant_code', $trail_data)){
                $communicationTrail->tenant_code=$trail_data['tenant_code'];
            }
            
            if (array_key_exists('program_code', $trail_data)){
                $communicationTrail->program_code=$trail_data['program_code'];
            }
            
            if (array_key_exists('content', $trail_data)){
                $communicationTrail->content=$trail_data['content'];
            }
            
	if (array_key_exists('job_id', $trail_data)){
	    $communicationTrail->job_id=$trail_data['job_id'];
	}

            if (array_key_exists('transaction_id', $trail_data)){
                $communicationTrail->transaction_id=$trail_data['transaction_id'];
            }

            if (array_key_exists('party_id', $trail_data)){
                $communicationTrail->party_id=$trail_data['party_id'];
            }

            $communicationTrail->save();
            $ct_id=$communicationTrail->id;

            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished******");
        
    }
}
