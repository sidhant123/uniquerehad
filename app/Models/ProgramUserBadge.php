<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 11 May 2018 15:31:30 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramUserBadge
 * 
 * @property int $id
 * @property int $program_id
 * @property int $user_id
 * @property int $max_distribution_badges
 * @property int $current_distribution_badges
 * @property int $content_rated_count
 * @property int $fastest_activity_badge_count
 * @property int $content_rating_badge_count
 * @property int $quiz_completion_badge_count
 * @property int $facilitator_badge_count
 * @property int $participant_badge_count
 * @property int $extension_badge_count1
 * @property int $extension_badge_count2
 * @property int $extension_badge_count3
 * @property int $extension_badge_count4
 * @property int $approved
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Program $program
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class ProgramUserBadge extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'program_user_badge';

	protected $casts = [
		'program_id' => 'int',
		'user_id' => 'int',
		'max_distribution_badges' => 'int',
		'current_distribution_badges' => 'int',
		'content_rated_count' => 'int',
		'fastest_activity_badge_count' => 'int',
		'content_rating_badge_count' => 'int',
		'quiz_completion_badge_count' => 'int',
		'facilitator_badge_count' => 'int',
		'participant_badge_count' => 'int',
		'extension_badge_count1' => 'int',
		'extension_badge_count2' => 'int',
		'extension_badge_count3' => 'int',
		'extension_badge_count4' => 'int',
		'approved' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'user_id',
		'max_distribution_badges',
		'current_distribution_badges',
		'content_rated_count',
		'fastest_activity_badge_count',
		'content_rating_badge_count',
		'quiz_completion_badge_count',
		'facilitator_badge_count',
		'participant_badge_count',
		'extension_badge_count1',
		'extension_badge_count2',
		'extension_badge_count3',
		'extension_badge_count4',
		'approved',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function program()
	{
		return $this->belongsTo(\App\Models\Program::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
