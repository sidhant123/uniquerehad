<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace vnnogile\Cleanups\Services;

//Custom imports
use DB;
use App\User;

use App\Models\BatchJob;
use App\Models\CommunicationTrail;
use App\Models\CommunicationTrailArchive;
use App\Models\ImportJob;
use App\Models\ImportJobArchive;
use App\Models\Notification;
use App\Models\NotificationArchive;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * 
 */
class CleanupService
{

    /**
     * [sendAssessmentAutoReminder Used to send auto reminders for assessment]
     * @param  int $tenantPartyId 
     */
	public function cleanupNotifications($tenantPartyId){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $jobId = 0;

        try{

        	$batchJob = new BatchJob;
        	$batchJob->job_name='Cleanup Notifications';
        	$batchJob->status='IN_PROGRESS';
        	$batchJob->tenant_party_id=$tenantPartyId;
        	$batchJob->created_by=1;
        	$batchJob->updated_by=1;
        	$batchJob->save();
        	$jobId = $batchJob->id;

        	DB::beginTransaction();

	        $days = 30;
	        $readNotificationEndDate = Carbon::now()->subDays($days)->startOfDay();
	        $readNotificationStartDate = Carbon::now()->subDays($days + 1)->startOfDay();

			$days = 60;
	        $unreadNotificationEndDate = Carbon::now()->subDays($days)->startOfDay();
	        $unreadNotificationStartDate = Carbon::now()->subDays($days + 1)->startOfDay();
	        
	        Log::debug('$readNotificationStartDate -> ' . $readNotificationStartDate);
	        Log::debug('$readNotificationEndDate -> ' . $readNotificationEndDate);
	        $readNotifications = Notification::whereBetween('created_at', [$readNotificationStartDate, $readNotificationEndDate])
	        ->where('is_read', '=', 1)
	        ->where('tenant_party_id', '=', $tenantPartyId)
	        ->get()
	        ->toArray();
	        foreach ($readNotifications as $readNotification) 
	        {
	        	Log::debug($readNotification);
	            NotificationArchive::insert($readNotification);
	        }
	        
	        $readNotifications = Notification::whereBetween('created_at', [$readNotificationStartDate, $readNotificationEndDate])
	        ->where('is_read', '=', 1)
	        ->where('tenant_party_id', '=', $tenantPartyId)
	        ->forceDelete();

	        Log::debug('$unreadNotificationStartDate -> ' . $unreadNotificationStartDate);
	        Log::debug('$unreadNotificationEndDate -> ' . $unreadNotificationEndDate);
	        $unreadNotifications = Notification::whereBetween('created_at', [$unreadNotificationStartDate, $unreadNotificationEndDate])
	        ->where('is_read', '=', 0)
	        ->where('tenant_party_id', '=', $tenantPartyId)
	        ->get()
	        ->toArray();
	        foreach ($unreadNotifications as $unreadNotification) 
	        {
	        	Log::debug($unreadNotification);
	            NotificationArchive::insert($unreadNotification);
	        }
	        
	        $unreadNotifications = Notification::whereBetween('created_at', [$unreadNotificationStartDate, $unreadNotificationEndDate])
	        ->where('is_read', '=', 0)
	        ->where('tenant_party_id', '=', $tenantPartyId)
	        ->forceDelete();

	        $batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'SUCCESS';
	        $batchJob->save();

        	DB::commit();
        }catch(\Exception $exception){
        	DB::rollback();
        	$batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'FAILURE';
	        $batchJob->exception = $exception->getMessage();
	        $batchJob->save();
        }catch(\Throwable $exception){
        	DB::rollback();
        	$batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'FAILURE';
	        $batchJob->exception = $exception->getMessage();
	        $batchJob->save();
        }


        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
     * [sendAssessmentAutoReminder Used to send auto reminders for assessment]
     * @param  int $tenantPartyId 
     */
	public function cleanupCommunicationTrails($tenantCode){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $jobId = 0;
        try{

        	$batchJob = new BatchJob;
        	$batchJob->job_name='Cleanup Communication Trails';
        	$batchJob->status='IN_PROGRESS';
        	$batchJob->tenant_code=$tenantCode;        	
        	$batchJob->created_by=1;
        	$batchJob->updated_by=1;
        	$batchJob->save();
        	$jobId = $batchJob->id;

        	DB::beginTransaction();


        	$days = 1;
        	$commTrailEndDate = Carbon::now()->subDays($days)->startOfDay();
        	$commTrailStartDate = Carbon::now()->subDays($days + 1)->startOfDay();

        	$commTrails = CommunicationTrail::whereBetween('created_at', [$commTrailStartDate, $commTrailEndDate])
        	->where('tenant_code', '=', $tenantCode)
        	->get()
        	->toArray();
        	foreach ($commTrails as $commTrail) 
        	{
        	    CommunicationTrailArchive::insert($commTrail);
        	}
        	
        	$commTrails = CommunicationTrail::whereBetween('created_at', [$commTrailStartDate, $commTrailEndDate])
        	->where('tenant_code', '=', $tenantCode)
        	->forceDelete();

	        $batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'SUCCESS';
	        $batchJob->save();

        	DB::commit();
        }catch(\Exception $exception){
        	DB::rollback();
        	$batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'FAILURE';
	        $batchJob->exception = $exception->getMessage();
	        $batchJob->save();
        }catch(\Throwable $exception){
        	DB::rollback();
        	$batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'FAILURE';
	        $batchJob->exception = $exception->getMessage();
	        $batchJob->save();
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
     * [sendAssessmentAutoReminder Used to send auto reminders for assessment]
     * @param  int $tenantPartyId 
     */
	public function cleanupImportJobs(){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $jobId = 0;
        try{

        	$batchJob = new BatchJob;
        	$batchJob->job_name='Cleanup Import Jobs';
        	$batchJob->status='IN_PROGRESS';
        	$batchJob->created_by=1;
        	$batchJob->updated_by=1;
        	$batchJob->save();
        	$jobId = $batchJob->id;

        	DB::beginTransaction();


        	$days = 1;
        	$importJobsEndDate = Carbon::now()->subDays($days)->startOfDay();
        	$importJobsStartDate = Carbon::now()->subDays($days + 1)->startOfDay();

        	$importJobs = ImportJob::whereBetween('created_at', [$importJobsStartDate, $importJobsEndDate])
        	->get()
        	->toArray();
        	foreach ($importJobs as $importJob) 
        	{
        	    ImportJobArchive::insert($importJob);
        	}
        	
        	$importJobs = ImportJob::whereBetween('created_at', [$importJobsStartDate, $importJobsEndDate])
        	->forceDelete();

	        $batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'SUCCESS';
	        $batchJob->save();

        	DB::commit();
        }catch(\Exception $exception){
        	DB::rollback();
        	$batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'FAILURE';
	        $batchJob->exception = $exception->getMessage();
	        $batchJob->save();
        }catch(\Throwable $exception){
        	DB::rollback();
        	$batchJob = BatchJob::find($jobId);
	        $batchJob->status = 'FAILURE';
	        $batchJob->exception = $exception->getMessage();
	        $batchJob->save();
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

}
