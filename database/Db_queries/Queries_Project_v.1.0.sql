
--
--ALTER SEQUENCE role_user_seq RESTART WITH 4;

--- seq creation query for menus table
-- by DhruviB 10th may 2020


-- insert query for menus 
-- by DhruviB 10th may 2020
-- check last id for insert parent_id

INSERT INTO public.menus(
     display_name, name, sequence,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES 
	( 'Projects', 'Projects', 45,  1, now(), now(),  1, 1);


	INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES 	
	( 'List', 'List Projects', 50, _9,  1, now(), now(),  1, 1), 
	(  'New Project', 'Create Projects', 55, _9,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020

INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (19,'Projects', now(), now(), 1, 1),
     (20, 'List Projects', now(), now(), 1, 1),
     (21, 'Create Projects', now(), now(), 1, 1),
     (22, 'Show Projects', now(), now(), 1, 1),
     (23 , 'Delete Projects', now(), now(), 1, 1),
     (24 , 'Edit Projects', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (19, 1, 19, now(), now(), 1,1),
     (20, 1, 20, now(), now(), 1,1),
     (21, 1, 21, now(), now(), 1,1),
     (22, 1, 22, now(), now(), 1,1),
     (23, 1, 23, now(), now(), 1,1),
     (24, 1, 24, now(), now(), 1,1);



-- insert script 
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Project', 'Both', 'name', 'required|max:200|unique:deleted_at', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'start_date', 'required', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'end_date', 'required|date|after_or_equal:startDate', 1, now(), now(), 1, 1);



INSERT INTO public.list_view_masters(
             function_name, entity_name, entity_namespace, query_columns, 
            heading_columns,
             filter_columns, display_css_widths, relations, replace_columns,
              created_at, updated_at, 
             created_by, updated_by )
    VALUES ('List Projects', 'Project`', '', 
        'id,name,start_date,end_date,id', 
         'Id,Name,Start date,End Date,Action',
             '', 'col-xs-1,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2', 
             '', '',
             now(),now(),1,1);

			 
	ALTER TABLE projects
ADD COLUMN site_id integer;		 
