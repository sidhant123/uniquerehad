<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class ReceiptFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('receipts')
                ->leftJoin('projects as pro', 'pro.id', '=', 'receipts.project_id')
                ->select( 
                'receipts.payment_no as payment_no',
                'pro.name as project_name',
                'receipts.boq_no as boq_no',
                'receipts.invoice_no as invoice_no',
                'receipts.invoice_date as invoice_date',
                'receipts.amount_received as amount_received',
                'receipts.invoice_amount as invoice_amount',
                 'receipts.total_deduction as total_deduction',
                'receipts.id as id'
                ) ->where('receipts.deleted_at', null);
                        
                                                       
            if ($request->has('search')) {
                $data->whereRaw(
                    "( lower(receipts.boq_no) like '%" . strtolower($request['search']) . "%' or
                lower(pro.name) like '%" . strtolower($request['search']) . "%' or
                 lower(receipts.invoice_no) like '%" . strtolower($request['search']) . "%'
                ) ");
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'project_name':
                        $data->orderBy('pro.name', $request['sort']);
                        break;
                    case 'boq_no':
                        $data->orderBy('receipts.boq_no', $request['sort']);
                        break;
                    case 'invoice_no':
                        $data->orderBy('receipts.invoice_no', $request['sort']);
                        break;
                  case 'invoice_date':
                        $data->orderBy('receipts.invoice_date', $request['sort']);
                        break;
                    case 'amount_received':
                        $data->orderBy('receipts.amount_received', $request['sort']);
                        break;
                  case 'invoice_amount':
                        $data->orderBy('receipts.invoice_amount', $request['sort']);
                        break;
                   
                }
            } else {
                $data->orderBy('receipts.id', 'desc');
               // $data->orderBy('receipts.boq_no' , 'desc');
            }

            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }


            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::error($e->getMessage());
        }
    }
}
