<?php

namespace vnnogile\Pagination\Utilities;

//Custom Imports Below
//use App;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * This is a a common utility method for handling PDF files
 *
 * All controllers that want to generate PDF files from views
 * or specific HTML should use this class.
 *
 * PDFS can be downloaded or viewed inline within the browser
 * if the browser supports viewing of documents inline
 *
 * PDF file can be saved locally and sent in email.
 */

class EntityUtility
{
    /**
     *
     * @param Request $request
     * @param string $html
     *
     * @return PDF
     */
    public static function getEntityQueryColumnsAndDisplayColumns($entityName, $masterDataInterface)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $entityArray = array();
        $listViewMaster = $masterDataInterface->findBy('ListViewMaster', ['entity_name' => $entityName]);

        if (count($listViewMaster) == 0) {
            // return response()->json(['error' => 'Some error occured!Please try after some time.'], 401);
            throw new \Exception("0100-Entity configuration not defined for list view master");
        }
        $listViewMaster = $listViewMaster[0];
        $model = $listViewMaster->entityNamespace . $listViewMaster->entityName;
        $model = \App::make($model);
        if (!$model instanceof Model) {
            throw new \Exception("Class must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        $entityArray['records'] = $model;
        $entityArray['entityName'] = $listViewMaster->entityName;
        $entityArray['queryColumnNames'] = explode(',', $listViewMaster->queryColumns);
        $entityArray['headerColumnNames'] = explode(',', $listViewMaster->headingColumns);
        $entityArray['filterColumnNames'] = explode(',', $listViewMaster->filterColumns);
        $entityArray['displayColumnCSS'] = explode(',', $listViewMaster->displayCssWidths);
        $entityArray['filterClass'] = $listViewMaster->filterClassNames;
	$entityArray['htmlTag'] = explode(',', $listViewMaster->htmlTag);
        if ($listViewMaster->relations != '') {
            $entityArray['relations'] = explode(',', $listViewMaster->relations);
            $entityArray['replace_columns'] = array($listViewMaster->replaceColumns);
            Log::debug('*******RELATIONSHIPS FOUND********');
        } else {
            $entityArray['relations'] = 'relation';
            $entityArray['replace_columns'] = array();
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $entityArray;
    }
    public static function getQueryColumnsAndDisplayColumnsByQuery($entityName)
    {
        $entityArray = array();
        switch ($entityName) {
            case 'AddressReport':
                $entityArray['queryColumnNames'] = array('country_id', 'state_id', 'city_id');
                $entityArray['headerColumnNames'] = array('Country Name', 'State Name', 'City Name');
                $entityArray['displayColumnCSS'] = array('col-xs-1', 'col-xs-5', 'col-xs-3');
                $entityArray['filterColumnNames'] = array();
                $entityArray['relations'] = array();
                $entityArray['replace_columns'] = array();
                break;

        }
        return $entityArray;
    }
    public static function fireQuery($records, $rowCount, $query, $relations = 'relation', $replaceColumns = 'column')
    {
        if ($relations == 'relation') {
            $records = $records->paginate($rowCount)->appends($query);
            Log::debug('EntityUtility::dataWithOutRelation' . print_r($records, true));
            return $records;
        } else {
            $records = $records->with(array_unique($relations))->paginate($rowCount)->appends($query);
            foreach ($records as $record) {
                foreach ($relations as $key => $value) {
                    $columns = explode(',', $replaceColumns[$key]);
                    $record[$columns[0]] = $record[$value][$columns[1]];
                }
            }
            Log::debug('EntityUtility::dataWithRelation' . print_r($records, true));
            return $records;
        }
    }

    private static function makeModel($app, $model)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $model = $app->make($model);
        if (!$model instanceof Model) {
            throw new \Exception("Class must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $model;
    }

}
