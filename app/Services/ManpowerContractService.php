<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;


use App\Models\ManpowerContract;

/**
 * Class AddressMasterService
 *
 */
class ManpowerContractService {
	use ValidationTrait;

	public static function retrieveManpowerContracts() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = ManpowerContract::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

public static function saveManpowerContractData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data) {
			$userId = $data['authenticatedUser']->id;

			$obj = new ManpowerContract;
			$obj->organization_id = $data['organization_id'];
			$obj->site_id = $data['project_id'];
			$obj->project_id = $data['project_id'];
			$obj->service_name = $data['service_name'];
			$obj->rate = $data['rate'];
			$obj->status = $data['status'];
			//$obj->document_id = $data['document_id'];
    		$obj->created_by = $userId;
			$obj->updated_by = $userId;

			$obj->save();
			$manpower_contract_id = $obj->id;
			return $manpower_contract_id;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	
public static function updateData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
					//$userId = $data['authenticatedUser']->id;
			
			ManpowerContract::where('id', $data['id'])->update([
				'organization_id' => $data['organization_id'],
				'project_id' => $data['project_id'],
				'site_id' => $data['project_id'],
				'service_name' => $data['service_name'],
				'rate' => $data['rate'],
				'status' => $data['status'],
				'updated_by' => $data['authenticatedUser']->id
			]);

			return $data['id'];
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
public static function deleteManpowerContract($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$manpower_contract = ManpowerContract::where('id', $id);
			//$user = self::validateActiveManpowerContract($manpower_contract);

			//if ($user) {
				$userId = auth()->user()->id;

				$manpower_contract = ManpowerContract::where('id', $id);
				$manpower_contract->update(['deleted_by' => $userId]);
				$check = $manpower_contract->delete();
			//} else {
			//	$check = false;
			//}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	private static function validateActiveManpowerContract($manpower_contract) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$manpower_contract = $manpower_contract->first();
		Log::debug("********* VALIDATE ACTIVE manpower_contract 1".$manpower_contract);
		Log::debug("********* VALIDATE ACTIVE manpower_contract 2".$manpower_contract->id);


		$userObject = Project::where('manpower_contract_id', $manpower_contract->id);
		Log::debug("********* VALIDATE ACTIVE manpower_contract 3".$userObject);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		if ($userObject->first()->manpower_contract_id == $manpower_contract->id) {
			return false;
		} else {
			return $userObject;
		}
	}

	public static function getManpowerForCompany($organization_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

		$data = ManpowerContract::select('service_name')->where('organization_id',$organization_id)
		->get();
		$m_list="";
		$newdata= array();

		foreach ($data as $key => $value) {
			$newdata[] = $value['service_name'];
		}
		$m_list = implode(',',$newdata);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
 		return $m_list;
	}
	
	
}
