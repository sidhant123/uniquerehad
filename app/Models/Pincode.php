<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Pincode
 * 
 * @property int $id
 * @property string $code
 * @property int $city_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $countries_id
 * @property int $states_id
 * @property int $districts_id
 * 
 * @property \App\Models\User $user
 * @property \App\Models\City $city
 * @property \App\Models\Country $country
 * @property \App\Models\District $district
 * @property \App\Models\State $state
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 *
 * @package App\Models
 */
class Pincode extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'city_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
		'countries_id' => 'int',
		'states_id' => 'int',
		'districts_id' => 'int'
	];

	protected $fillable = [
		'code',
		'city_id',
		'created_by',
		'updated_by',
		'deleted_by',
		'countries_id',
		'states_id',
		'districts_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function city()
	{
		return $this->belongsTo(\App\Models\City::class);
	}

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class, 'countries_id');
	}

	public function district()
	{
		return $this->belongsTo(\App\Models\District::class, 'districts_id');
	}

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class, 'states_id');
	}

	public function addresses()
	{
		return $this->hasMany(\App\Models\Address::class);
	}
}
