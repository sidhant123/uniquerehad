<?php

// cssir

namespace App\Services;

//Custom imports
use DB;
use App\Models\Topic;
use Illuminate\Support\Facades\Log;

class LearningTreeSvgRenderService
{    

  public static function renderLearningTree($type, $learningTreeid, $nodeId, $masterDataInterface, $tenantPartyId)
  {
      Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

      //$type - MLT means master learning tree and PLT means Program Learning Tree
      //$nodeId - 0 means we have to fetch the first level nodes of the learning tree

      //Step 1 - Fetch the data from the database      

      //This will be done at the end after the entire structure is created
      $storageUtility = self::getStorageImplementationClass($masterDataInterface);
      //$data='This is sample data';
       $svgData = $storageUtility->readFile('svgs', $type . "_" . $learningTreeid . "_" . $nodeId . ".svg");
     /* if ($nodeId == 0){
        $svgData = $storageUtility->readFile('svgs', $type . "_" . $learningTreeid . ".svg");  
      }else{
        $svgData = $storageUtility->readFile('svgs', $type . "_" . $learningTreeid . "_" . $nodeId . ".svg");
      }*/
      Log::debug('svgs/' . $type . "_" . $learningTreeid . ".svg");
      Log::debug($svgData);
      Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
      return $svgData;
  }

  private static function getStorageImplementationClass(  $masterDataInterface){
    Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    $configurations = $masterDataInterface->all('Configuration');
    foreach ($configurations as $configuration) {
        $configurationsMap[$configuration->configKey] = $configuration->configValue;
    }
    $data['configurations'] = $configurationsMap;

    $class = $data['configurations']['storage_implementation_class'];
    $storageUtility = new $class;
    Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    return $storageUtility;
  }


}
