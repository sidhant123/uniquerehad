{{-- @if($errors->any())
    @foreach ($errors->all() as $error)
      <div class="errorMessage">{{ $error }}</div>
    @endforeach
@endif
 --}}

 <!-- {{$errors}} -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif