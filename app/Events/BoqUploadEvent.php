<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BoqUploadEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $folder;
    public $fileName;
    public $userId;
    public $jobId;
    public $tempPath;
    public $finalPath;
    public $uploadedFile;
public $project_id;
public  $route_name;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($folder, $fileName, $userId, $jobId, $tempPath, $finalPath, $uploadedFile, $project_id, $route_name)
    {
        $this->folder = $folder;
        $this->fileName = $fileName;
        $this->userId = $userId;
        $this->jobId = $jobId;
        $this->tempPath = $tempPath;
        $this->finalPath = $finalPath;
        $this->uploadedFile = $uploadedFile;
        $this->project_id = $project_id;
        $this->route_name = $route_name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
