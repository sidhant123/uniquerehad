<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Vnnogile Back Office</title>


        
        <!-- <link href="/css/bootstrap.css" rel="stylesheet"> -->
        <!-- <link href="/css/bootstrap.min.css" rel="stylesheet"> -->
        <script src="/js/jquery-2.2.4.js"></script>
        <link href="/css/reset.css" rel="stylesheet">
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/menu.css" rel="stylesheet">
        <link href="/css/vnnogile.css" rel="stylesheet">
        <link href="/css/select2.min.css" rel="stylesheet">
        <link href="/css/jquery-ui-dateTimepicker.css" rel="stylesheet">
        <link href="/css/fontawesome-all.min.css" rel="stylesheet">
        <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> -->
        <!-- <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet"> -->


    </head>
    <body>
        <div class="container-fluid">
          <div class="row">
              <div class="col-xs-12">
                  <div class="loader"></div>
              </div>
          </div>
            <div class="row">
                <div class="col-xs-2">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id='menuId'>
                              <span onclick="closeNav()"><i class="fa fa-bars pull-right" id="closebars"></i></span>
{{--                               @foreach ($menuItems as $menuItem)
                                @if (is_null($menuItem->parent_menu_id))
                                  <a class="menu_item" href="#" id="{{ $menuItem->id }}" onclick="return expandNode($(this));">{{ $menuItem->name }}&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                @else
                                  <a href="{{ $menuItem->url }}" class="expanded-menu {{ $menuItem->parent_menu_id }}" style="display: none">{{ $menuItem->name }}</a>
                                @endif
                              @endforeach --}}
                              <nav class="navbar-primary">
                                <a href="#" class="btn-expand-collapse"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></a>
                                <ul class="navbar-primary-menu">
                                  <li>
                                                                   @foreach ($menuItems as $menuItem)
                                                                    @if (is_null($menuItem->parent_menu_id))
                                                                      <a href="#" class="menu_item" id="{{ $menuItem->id }}" onclick="return expandNode($(this));"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span><span class="nav-label">{{ $menuItem->name }}&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
                                                                    @else
                                                                      <a href="{{ $menuItem->url }}" class="expanded-menu {{ $menuItem->parent_menu_id }}" style="display: none"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span><span class="nav-label">{{ $menuItem->name }}&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
                                                                    @endif
                                                                  @endforeach
<!--                                     <a href="#"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Dashboard</span></a>
<a href="#"><span class="glyphicon glyphicon-envelope"></span><span class="nav-label">Profile</span></a>
<a href="#"><span class="glyphicon glyphicon-cog"></span><span class="nav-label">Settings</span></a>
<a href="#"><span class="glyphicon glyphicon-film"></span><span class="nav-label">Notification</span></a>
<a href="#"><span class="glyphicon glyphicon-calendar"></span><span class="nav-label">Monitor</span></a> -->
                                  </li>
                                </ul>
                              </nav>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div id="main_layout_content" class="col-xs-10">
                    <div class="row">
                      <!-- <div id="openbars" class="col-xs-1">
                        <span onclick="openNav()"><i class="fa fa-bars"></i></span>
                      </div> -->
                      <nav id="navbar-div" class="navbar navbar-default">
                        <span id="openbars" onclick="openNav()"><i class="fa fa-bars"></i></span>
                        <div class="container-fluid">
                          <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                              <img alt="Brand" src="/img/logo.png" width="90">
                            </a>
                          </div>
                          <div class="collapse navbar-collapse">
{{--                           <!-- <p class="navbar-text navbar-right">Logged in as {{ $authenticatedUser->first_name }} {{ $authenticatedUser->last_name }}</p> -->
                          <!-- Right Side Of Navbar --> --}}
                          <ul class="nav navbar-nav navbar-right">
                              <!-- Authentication Links -->
                              @guest
                                  <li><a href="{{ route('login') }}">Login</a></li>
                              @else
                                <li>
                                  <a href="/companies" aria-expanded="false" aria-haspopup="true">
                                    <i class="fas fa-home" aria-hidden="true"></i>
                                  </a>
                                </li>
                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                      <i class="far fa-bell" aria-hidden="true"></i>
                                    </a>
                                  </li>
                                  <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                          <i class="fa fa-user-circle" data-toggle="tooltip" aria-hidden="true" title="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}"></i><span class="caret"></span>
                                      </a>

                                      <ul class="dropdown-menu">
                                          <li>
                                            
                                              <a href="{{ route('logout') }}"
                                                  onclick="event.preventDefault();
                                                           document.getElementById('logout-form').submit();">
                                                  <i class="fa fa-sign-out"></i>Logout
                                              </a>

                                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                  {{ csrf_field() }}
                                              </form>
                                          </li>
                                      </ul>
                                  </li>
                              @endguest
                              </ul>
                              <form class="navbar-form navbar-right" role="search">
                                <div class="form-group">
                                  <input type="text" id="search" class="form-control" placeholder="Search">
                                </div>
                                <button type="submit" class="btn btn-default">Go</button>
                              </form>                 
                          </div>
                        </div>
                      </nav>
                    </div>
                    <div class='row'>
                        <div class="col-xs-12">
                              @yield('filterContent')
                              @yield('content')
                              @yield('tagContent')
                        </div>                        
                    </div>

                </div>
            </div>
        </div>        
{{--         <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="loader"></div>
            </div>
        </div>
        <span onclick="openNav()"><i class="fa fa-bars" id="openbars"></i></span>
           <div class="navbar-header" id="navbar-header1">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img alt="Brand" src="img/pageLoader.gif" width="50" height="50"></a>

                  <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </form>
            </div>
 
        <div class="row">
            <div class="col-xs-12">
                <div id="menuId" class="menuClass">
                  <span onclick="closeNav()"><i class="fa fa-bars pull-right" id="closebars"></i></span>
                  @foreach ($menuItems as $menuItem)
                    @if (is_null($menuItem->parent_menu_id))
                      <a href="#" id="{{ $menuItem->id }}" onclick="return expandNode($(this));">{{ $menuItem->name }}&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    @else
                      <a href="{{ $menuItem->url }}" class="expanded-menu {{ $menuItem->parent_menu_id }}" style="display: none">{{ $menuItem->name }}</a>
                    @endif
                  @endforeach
                </div>
            </div>
        </div>
        <!-- Use any element to open the menuClass -->

        <div class="row">
            <div class="col-xs-12">
                <div id="main">
                    @yield('filterContent')
                    @yield('content')
                    @yield('tagContent')
                </div>
            </div>
        </div> --}}
       
        @yield('css')                
        <script src="/js/axios.min.js"></script>
        <script src="/js/jquery-ui.js"></script>    
        <script src="/js/bootstrap.js"></script>
        <script src="/js/bootbox.min.js"></script>
        <script src="/js/menu.js"></script>    
        <script src="/js/select2.min.js"></script>
        <script src="/js/cgx.js"></script>
        <script src="/js/jquery.slimscroll.min.js"></script>
        <script src="/js/colResizable-1.6.min.js"></script>
        </div>
    </body>
    <head>
    <!-- Head Contents -->

    @stack('scripts')

    <script>
        $(function() {
            //document.getElementById("menuId").style.width = "0";
            //document.getElementById("main").style.marginLeft = "0";
            //$('.fa.fa-bars').css({display: "block"});
        });
    </script>
</head>
</html>
