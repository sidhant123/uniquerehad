<?php

namespace App\Console;

use App\Jobs\ExportContent;
use App\Jobs\ExportUsers;

//Custom imports
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use vnnogile\ContentValidation\Jobs\ContentExtractionJob;
// use vnnogile\ExportCommunicationTrails\Jobs\ExportCommunicationTrailsJob;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule) {
		// $schedule->job(new ExportUsers(1))->daily();
		// $schedule->job(new ExportContent(1))->dailyAt('1:00');
		// // $schedule->job(new ExportCommunicationTrailsJob(1))->dailyAt('7:00');
		// $schedule->job(new ContentExtractionJob(1))->dailyAt('1:15');		
		// $schedule->command('cleanup:notifications 1')->dailyAt('23:50');
		// $schedule->command('cleanup:communication_trails CGX')->dailyAt('23:55');
		// $schedule->exec('/home/cognegix/b_log_manager.sh')->dailyAt('1:01');
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load(__DIR__ . '/Commands');
		require base_path('routes/console.php');
	}
}
