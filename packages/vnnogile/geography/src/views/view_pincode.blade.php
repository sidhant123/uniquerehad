@extends('vnnogile::pincode_base')
@section('title', 'View Pincode')
@section('editMethod')

@endsection
@section('disabled', 'disabled')
@section('showPincodeName', 'readonly')
@section('editID', $data->id)
@section('pincode_name', $data->name)
@section('disable_submit', 'pointerEventNone')
