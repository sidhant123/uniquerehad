@extends('layouts.base')
@section('content')

  @include('errors.error_validation')
  <form style="border-style:dotted; border-width: 1px;" action="/sites{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFields()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/sites'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Site

      </h3></div>
    </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="name">{{__('labels.sites.name')}}</label>
            @if(!empty(old('name')))
              <input style="text-transform: capitalize;" type="text" class="form-control mandatory" id="site_name_id" name="name" value="{{ old('name') }}" @yield('name_d')>
            @else
            <input style="text-transform: capitalize;" type="text" class="form-control mandatory" id="site_name_id"  name="name" value="@yield('name')" @yield('name_d')>
            @endif
         </div>
     </div>

     <div class="col-lg-6">
       <div class="form-group required">
           <label for="code">{{__('labels.sites.code')}}</label>
            @if(!empty(old('code')))
              <input maxlength="25" style="text-transform: uppercase;" type="text" class="form-control mandatory" id="site_code_id" name="code" value="{{ old('code') }}" @yield('code_d')>
            @else
            <input maxlength="25" style="text-transform: uppercase;" type="text" class="form-control mandatory" id="site_code_id"  name="code" value="@yield('code')" @yield('code_d')>
            @endif
         </div>
     </div>
  </div>



   <div class="row">
    <div class="col-lg-6">
       <div class="form-group required">
           <label for="location">{{__('labels.sites.location')}}</label>
            @if(!empty(old('location')))
              <input style="text-transform: capitalize;" maxlength="25" type="text" class="form-control mandatory" id="site_location_id" name="location" value="{{ old('location') }}" @yield('location_d')>
            @else
            <input style="text-transform: capitalize;" maxlength="25" type="text" class="form-control mandatory" id="site_location_id"  name="location" value="@yield('location')" @yield('location_d')>
            @endif
         </div>
     </div>

   <div class="col-lg-6">
    <div class="form-group required">
           <label for="address">{{__('labels.sites.address')}}</label>
            @if(!empty(old('address')))
            <textarea  type="text" class="form-control mandatory" id="address_id" name="address" @yield('address_d')>{{ old('address') }}</textarea>
            @else
            <textarea  type="text" class="form-control mandatory" name="address" id="address_id"   @yield('address_d')>@yield('address')</textarea>
            @endif

         </div>  
   </div>
</div>
<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.sites.submit')}}</button>
</div>
</div>
<p></p>

<!-- for import user -->
@section('importBoq')
@show
@endsection

@push('scripts')

<p></p>

<p></p>

</div>
</form> 

<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script>

$('#address_id').keyup(function(){
    if($(this).val().length>0){
      var character = $(this).val().charAt(0);
      if(character!=character.toUpperCase()){
          $(this).val($(this).val().charAt(0).toUpperCase()+$(this).val().substr(1));
       }
     }
});



</script>
@endpush


