
-------- project module
delete from validations where entity_name='Project';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Project', 'Update', 'name', 'required|min:4|max:255|unique:projects,name,$project,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Project', 'Create', 'name', 'required|min:4|max:255|unique:projects,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'start_date', 'required', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'end_date', 'required|date|after_or_equal:startDate', 1, now(), now(), 1, 1),
     ('Project', 'Both', 'delay_hr', 'required|integer', 1, now(), now(), 1, 1)  ;

-- payment in controller
delete from validations where entity_name='Payment';
-- ---------------- expense module 

delete from validations where entity_name='Expense';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Expense', 'Both', 'service_name', 'required|min:4|max:100', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'bill_no', 'required|min:1|max:100', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'item_desc', 'required|min:4|max:50', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'unit', 'required|min:1|max:25', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'qty', 'required|min:1|max:10', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'rate', 'required|min:1|max:10', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'gst_amount', 'required|min:1|max:10', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'amount', 'required|min:1|max:10', 1, now(), now(), 1, 1),
     ('Expense', 'Both', 'amount_paid', 'max:10', 1, now(), now(), 1, 1) ;



-- ---------------- site module 

delete from validations where entity_name='Site';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Site', 'Update', 'name', 'required|min:4|max:250|unique:sites,name,$site,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Site', 'Create', 'name', 'required|min:4|max:250|unique:sites,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Site', 'Both', 'address', 'required|min:4|max:1000', 1, now(), now(), 1, 1),
     ('Site', 'Both', 'location', 'required|min:4|max:25', 1, now(), now(), 1, 1),
     ('Site', 'Both', 'code', 'required|min:4|max:25', 1, now(), now(), 1, 1),
     ('Site', 'Both', 'description', 'min:4|max:100', 1, now(), now(), 1, 1) ;
  ALTER TABLE sites ALTER COLUMN name TYPE varchar(255);

-- ---------------- expense type module 

delete from validations where entity_name='ExpenseType';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('ExpenseType', 'Update', 'name', 'required|min:4|max:100|unique:expense_types,name,$name,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('ExpenseType', 'Create', 'name', 'required|min:4|max:100|unique:expense_types,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('ExpenseType', 'Both', 'description', 'max:250', 1, now(), now(), 1, 1) ;



-- ---------------- material in controller

delete from validations where entity_name='ManpowerContract';
INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('ManpowerContract', 'Both', 'service_name', 'required|min:4|max:100', 1, now(), now(), 1, 1) ,
     ('ManpowerContract', 'Both', 'rate', 'required|max:7', 1, now(), now(), 1, 1) ;

  ALTER TABLE manpower_contracts ALTER COLUMN rate TYPE numeric(10,2);





