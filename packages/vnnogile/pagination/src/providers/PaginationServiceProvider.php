<?php

namespace vnnogile\Pagination\Providers;

use Illuminate\Support\ServiceProvider;

class PaginationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->publishes([
        __DIR__.'/../ui' => public_path('ui/vnnogile'),
    ], 'public');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
