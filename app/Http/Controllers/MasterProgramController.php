<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Services\OrganizationService;
use Illuminate\Support\Facades\Log;
use vnnogile\Pagination\Utilities\PaginateController;
use vnnogile\Client\Controllers\BaseController;
use App\Models\MasterProgram;
use App\Models\Organization;
use App\Services\ValidationRulesService;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Session;
use DB;
use App\Services\MasterProgramService;
use App\Services\UserService;

class MasterProgramController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");        
        parent::__construct();
        $this->middleware('CheckCompositeMiddleWare', ['except' => ['update', 'create', 'show']]);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    // public function index(requestst $request, $flag = false) {
    //     dd($request->path());
    //     return PaginateController::paginate('MasterProgram', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    // }

    public function index(Request $request, $flag = false) {    
        // $now = new DateTime('Today');
        // dd($request->path());
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(\Auth::user()->hasAccess(['Users']));
        // $this->authorize('List Users', 'List Users');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        //return PaginateController::paginate('Person','users', parent::$utilsInterface, parent::$masterDataInterface);
        return PaginateController::paginate('MasterProgram', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {     
        // $users = UserService::getUsersByCompany();
        // $organization = OrganizationService::retrieveOrganizationForFilter();
        $organization = OrganizationService::retrieveOrganization();
        return view('master_program.create', compact('organization', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // dd('$validator');
        // dd($request->all());
        $masterProgramExists =  DB::table('master_programs')->where('name',$request->master_program_name)->first();
        // dd($masterProgramExists!=null);
        if($masterProgramExists!=null) {
            // dd('Already Esisst');
            return view('master_program.create')->with("warning","Master Program with this name has already been taken.");
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('MasterProgram');
            $data = $request->all();
            // dd($rules);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                // dd($validator);
                    return redirect('masterProgram/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }

            $masterProgramFlag = MasterProgramService::saveMasterProgram($data);
            // dd("Hello Dip");
            if($masterProgramFlag == True) {
                return redirect()->action('MasterProgramController@index');
            }
            else {
                Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
            }
        } catch (Exception $e) {
            Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            // $this->authorize('Edit Users', 'Edit Users');
            $data = MasterProgram::find($id);
            // dd($data);
            Log::debug("***********MASTER PROGRAM*****************");

            $organization_details = DB::table('organizations')->where('party_id', $data->customer_party_id)->first();

            // dd($data->customer_party_id);
            // dd($organization_details);
            $organization_name =  $organization_details->name;
            $organization_id =  $organization_details->id;
            $organization = OrganizationService::retrieveOrganization();
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return view('master_program.view', compact('data', 'organization', 'organization_name','organization_id'));

        } catch (Exception $e) {
            Log::debug('Some error occured on server!', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            // $this->authorize('Edit Users', 'Edit Users');
            $data = MasterProgram::find($id);
            // dd($data);
            Log::debug("***********MASTER PROGRAM*****************");

            $organization_details = DB::table('organizations')->where('id', $data->customer_party_id)->first();

            // dd($data->customer_party_id);
            // dd($data);
            $organization_name =  $organization_details->name;
            $organization_id =  $organization_details->id;
            $organization = OrganizationService::retrieveOrganization();
            // dd($organization);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return view('master_program.edit', compact('data', 'organization', 'organization_name', 'organization_id'));

        } catch (Exception $e) {
            Log::debug('Some error occured on server!', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('MasterProgram', '$user', $id);

            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return redirect('masterProgram/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            // dd('He');
            Log::debug(__LINE__ . "::" . __CLASS__ . "::" . __METHOD__ . $validator->fails());
            // exit();
            //Get some basic data ahead  of time and set it in data object
            // dd($id);
            $master_update_flag = MasterProgramService::updateMasterProgram($data, $id);

            if($master_update_flag == 'True') {                
                Session::flash('status', __('labels.flash_messages.update'));
                return redirect()->action('MasterProgramController@index');
            }
            else {
                Log::debug('Some Error occured on server ', $e->getMessage());                
            }
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        } catch (Exception $e) {
            Log::debug('Some Error occured on server ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
// ================Function to populate users for company selected begins by Dipak R

    public function getCompanyUsers($id) {
        // $data = DB::table('organizations')->select('party_id')->where('id', $company_id)->get();
        $data = Organization::find($id);
        $party_id = $data['party_id'];
        $users = UserService::getUsersByPatyId($party_id);
        return $users;
    }

// ================Function to populate users for company selected ends by Dipak R

}
