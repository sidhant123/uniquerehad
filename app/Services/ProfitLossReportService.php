<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class ProfitLossReportService {
	
	  public static function retrieveProfitLossReportQuery($project_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");



   $queryBuilder = DB::table('payments')
            ->selectRaw(
             'expense_types.name as expenses, 
              case when sum(payments.amount) > 0 then sum(payments.amount) else 0  end as amount
            ' 
         )
            ->leftJoin('expense_types', 'expense_types.id', '=', 'payments.expense_type_id')
            ->where('payments.project_id','=', $project_id)
             ->where('payments.deleted_at', null)
            ->groupBy('expense_types.name')
            ->orderBy('expense_types.name', 'asc');

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

  public static function retrievePaymentAmountQuery($project_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $queryBuilder = DB::table('payments')
            ->selectRaw(
            " 'Total Expense till date' as col1, 
            case when sum(amount) > 0 then sum(amount) else 0  end as total_payment_amount

              " ) 
        
        ->where('project_id','=', $project_id)
        ->where('payments.deleted_at', null);

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

    public static function retrieveReceiptAmountQuery($project_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $queryBuilder = DB::table('receipts')
            ->selectRaw(
            " 'Total Amount received till date' as col1, 
            case when sum(base_amount) > 0 then sum(base_amount) else 0  end as total_received_amount

           " ) 
         
             ->where('receipts.deleted_at', null)
        ->where('project_id','=', $project_id);

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

 public static function retrieveExtraAmountQuery($project_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        // $queryTDS = DB::table('receipts')
        //     ->selectRaw(
        //     " 'TDS Deduction' as col1, 
        //     case when sum(tds_deduction) > 0 then sum(tds_deduction) else 0  end as tds_deduction
        //    " ) 
        // ->where('project_id','=', $project_id);
        
        $queryEMD = DB::table('receipts')
            ->selectRaw(
            " 'EMD' as col1, 
            case when sum(other_deduction) > 0 then sum(other_deduction) else 0  end as total_extra_amount
           " ) 
        ->where('receipts.deleted_at', null)
        ->where('project_id','=', $project_id);

         $queryRet = DB::table('receipts')
            ->selectRaw(
            " 'Retension' as col1, 
            case when sum(retension) > 0 then sum(retension) else 0  end as total_extra_amount
           " ) 
        ->where('receipts.deleted_at', null)
        ->where('project_id','=', $project_id); 

         $queryMob = DB::table('receipts')
            ->selectRaw(
            " 'Mobilization' as col1, 
            case when sum(mobilization) > 0 then sum(mobilization) else 0  end as total_extra_amount
           " ) 
        ->where('receipts.deleted_at', null)
        ->where('project_id','=', $project_id);

      $receipts = DB::table('receipts')
       ->selectRaw(
            " 'TDS Deduction' as col1, 
            case when sum(tds_deduction) > 0 then sum(tds_deduction) else 0  end as total_extra_amount
           " ) 
        ->where('receipts.deleted_at', null)
        ->where('project_id','=', $project_id)
        ->union($queryEMD)
        ->union($queryRet)
        ->union($queryMob)
        ->get();

// dd($receipts);
        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $receipts;
    }



    public static function getName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $name = Project::select('name')->where('id', '=', $Id)->first();
        return trim($name->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
