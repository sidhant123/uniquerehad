@extends('site.site')
@section('editID', '/'.$row_data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('edit_btn','hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Site')
@section('name',$row_data->name)
@section('code',$row_data->code)
@section('location',$row_data->location)
@section('address',$row_data->address)
