Create sequence payments_seq; 

-- Table: public.payments

-- DROP TABLE public.payments;

CREATE TABLE public.payments
(
  id bigint NOT NULL DEFAULT nextval('payments_seq'::regclass),
  payment_no varchar,
  project_id integer NOT NULL,
  boq_no varchar NOT NULL,
  invoice_no varchar(20) NOT Null,
  invoice_date date NOT Null,
  payment_method varchar(50),
  payment_ref_no varchar NOT NULL,
  payment_receipt_date date,
  amount_received numeric(10,2) NOT Null,
  gst_amount numeric(8,2) NOT Null,
  invoice_amount numeric(8,2),
  total_amount numeric(10,2) NOT Null,
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT payments_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.payments
  OWNER TO postgres;

-- ---------------------------------------------------------------------------------------------



INSERT INTO public.menus(
     id, display_name, name, sequence,  auth, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (89, 'Payments', 'Payments', 75,  1, '', now(), now(),  1, 1);


    INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES  
    ( 'List', 'List Payments', 80, 89,  1, now(), now(),  1, 1), 
    (  'New Payment', 'Create Payments', 85, 89,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020


INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (37,'Payments', now(), now(), 1, 1),
     (38, 'List Payments', now(), now(), 1, 1),
     (39, 'Create Payments', now(), now(), 1, 1),
     (40, 'Show Payments', now(), now(), 1, 1),
     (41, 'Delete Payments', now(), now(), 1, 1),
     (42, 'Edit Payments', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (37, 1, 37, now(), now(), 1,1),
     (38, 1, 38, now(), now(), 1,1),
     (39, 1, 39, now(), now(), 1,1),
     (40, 1, 40, now(), now(), 1,1),
     (41, 1, 41, now(), now(), 1,1),
     (42, 1, 42, now(), now(), 1,1);


INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Payment', 'Both', 'project_id', 'required', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'boq_no', 'required|max:20|unique:payments,boq_no,NULL,id,deleted_at,NULL,invoice_no,$invoice_no', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'invoice_no', 'required|numeric', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'invoice_date', 'required|date', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'payment_receipt_date', 'required|date', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'amount_received', 'required|max:10|regex:/^\d+(\.\d{1,2})?$/', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'gst_amount', 'required|max:10|regex:/^\d+(\.\d{1,2})?$/', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'invoice_amount', 'required|max:10|regex:/^\d+(\.\d{1,2})?$/', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'total_amount', 'required|max:10|regex:/^\d+(\.\d{1,2})?$/', 1, now(), now(), 1, 1);

   --  ('Material', 'Update', 'name', 'required|max:255|unique:materials,name,$material,id,deleted_at,NULL,supplier_id,$supplier_id', 1, now(), now(), 1, 1),
    -- ('Material', 'Create', 'name', 'required|max:255|unique:materials,name,NULL,id,deleted_at,NULL,supplier_id,$supplier_id', 1, now(), now(), 1, 1);


INSERT INTO public.list_view_masters(
             function_name, entity_name,  
             query_columns, 
            heading_columns,
             filter_columns, 
             display_css_widths, 
             relations, replace_columns,
              created_at, updated_at, created_by, updated_by,
              filter_class_names,
              entity_namespace)
    VALUES ('List Payments', 'Payment',
            'payment_no,project_name,boq_no,invoice_no,invoice_date,amount_received,invoice_amount,total_amount,id', 
            'Payment No.,Project,BOQ No.,Invoice No.,Invoice Date, Received Amount, Invoice Amount, Total Amount,Action',
             '', 
             'col-xs-1,col-xs-2,col-xs-2,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1', 
             '', '', now(),now(),1,1, 
             'App\Utilities\PaymentFilterUtility', 
             'App\Models\');






