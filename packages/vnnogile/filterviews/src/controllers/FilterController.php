<?php

namespace vnnogile\FilterViews\Controllers;

use App\Http\Controllers\Controller;
use App\Services\FilterMasterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $route_name = $request['route_name'];
        $filter_master_data = FilterMasterService::getFilterDataFromRouteName($route_name);
        $disabled = false;
        try {
            foreach ($filter_master_data as $key => $value) {
                if ($value->action == 0) {
                    $selectData = \Request::create(route($value->data_url), 'GET');
                    $user = \Auth::user();
                    Log::debug('*******Authenticated User************');
                    Log::debug($user);
                    $selectData->merge(['user' => $user]);
                    //add this
                    $selectData->setUserResolver(function () use ($user) {
                        return $user;
                    });
                    $response = \Route::dispatch($selectData);
                    $compositeViewDataURL[$value->function_name] = json_decode($response->getContent(), true);
                }
                if ($value->action == 1) {
                    // print_r($request[$value->load_event]);

                    if ($request->has($value->load_event)) {
                        $route = str_replace('/', '', $value->data_url);
                        $selectData = \Request::create(route($route, [$value->load_event => $request[$value->load_event]]), 'GET');
                        $user = \Auth::user();
                        Log::debug('*******Authenticated User************');
                        Log::debug($user);
                        $selectData->merge(['user' => $user]);
                        //add this
                        $selectData->setUserResolver(function () use ($user) {
                            return $user;
                        });
                        $response = \Route::dispatch($selectData);
                        $compositeViewDataURL[$value->function_name] = json_decode($response->getContent(), true);

                    }
                }
            }
            // if (session()->has('userPrincipal')) {
            //     $userPrincipal = session('userPrincipal');
            //     $customer_party_id = $userPrincipal->getCustomerPartyId();
            //     if($customer_party_id > 0){
            //         $request['customer_party_id'] = $customer_party_id;
            //         $disabled = true;
            //     }
            // }
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            $filter_data['vnnogile::filter_view'] = array('route_name' => $route_name, 'filter_master_data' => $filter_master_data, 'compositeViewDataURL' => $compositeViewDataURL, 'request' => $request,'disabled' => $disabled);
            return $filter_data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
