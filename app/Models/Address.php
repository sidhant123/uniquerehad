<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Jan 2018 18:30:04 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Address
 * 
 * @property int $id
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $address4l
 * @property int $address_type_id
 * @property int $country_id
 * @property int $state_id
 * @property int $city_id
 * @property int $pincode_id
 * @property int $party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\AddressType $address_type
 * @property \App\Models\City $city
 * @property \App\Models\Country $country
 * @property \App\Models\Party $party
 * @property \App\Models\Pincode $pincode
 * @property \App\Models\State $state
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Address extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'address_type_id' => 'int',
		'country_id' => 'int',
		'state_id' => 'int',
		'city_id' => 'int',
		'pincode_id' => 'int',
		'party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'address1',
		'address2',
		'address3',
		'address4l',
		'address_type_id',
		'country_id',
		'state_id',
		'city_id',
		'pincode_id',
		'party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function address_type()
	{
		return $this->belongsTo(\App\Models\AddressType::class);
	}

	public function city()
	{
		return $this->belongsTo(\App\Models\City::class);
	}

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class);
	}

	public function pincode()
	{
		return $this->belongsTo(\App\Models\Pincode::class);
	}

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class);
	}

}
