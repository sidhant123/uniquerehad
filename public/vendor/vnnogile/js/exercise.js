  var competencies = {};
  $(document).ready(function() {
    $('#description_id,#summernoteForParticipant,#summernoteForRate').summernote({
     toolbar: [
       ['font', ['bold', 'underline', 'clear']],
       ['fontsize', ['fontsize']],
       ['fontname', ['fontname']],
       ['color', ['color']],
       ['para', ['ul', 'ol', 'paragraph', 'height', 'style']],
       ['table', ['table']],
       // ['insert', ['link', 'picture'
       //   //, 'video'
       // ]],
       ['view', ['fullscreen', 'codeview', 'help']]
     ],
   });
    $('.btnNext').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
  $('.alert-danger').addClass('hide');
});

  $('.btnPrevious').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
    var isTaggable = $("#isTaggable").val();
    $(function() {
      $("#selectable").selectable();
    });
    $(".show-modal").click(function() {
      $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
      });
    });
    $("ol.selectable").on("click", "li ", function() {
      $("ol.selectable > li").each(function() {
        console.log($(this).attr('class'));
        if ($(this).hasClass('ui-selected')) {
          $(this).find(':checkbox').prop('checked', true);
        } else {
          $(this).find(':checkbox').prop('checked', false);
        }
      });
    });
    $("ol.selectable").on("click", "li [type=checkbox]", function(e) {
      e.stopPropagation();
      if (this.checked == true) {
        $(this).parent().addClass('ui-selected');
      } else {
        $(this).parent().removeClass('ui-selected');
      }
    });
    $('#closeBtn').click(function(event) {
      /* Act on the event */
      resetModal();
    });
    if($('.sortable').length > 0){
      $('.sortable').each(function(){
           makeSortable($(this).attr('id'));
    })
    }
    getCompetencies();
    if(isTaggable){

      var tagFromHidden = $('#tag_name').val();
      var tags = '';
      console.log(tags);
      if(typeof tagFromHidden != 'undefined'){
        var tagsArray = JSON.parse(tagFromHidden);
        tags = tagsArray.join();
      }
  var tagHtml = '';//$('.multiple').html();
  $('.multiple').each(function(){
    tagHtml = tagHtml + $(this).html();
  });
  var html = tagHtml.replace('value=""', 'value="'+tags+'"');
  $('#compositeDiv').append(html);
}
  });

  function makeSortable(id) {
    console.log(id);
    $('#' + id).sortable({
      start: function(event, ui) {
        var start_pos = ui.item.index();
        ui.item.data('start_pos', start_pos);
      },
      change: function(event, ui) {
        var start_pos = ui.item.data('start_pos');
        var index = ui.placeholder.index();
        if (start_pos < index) {
          $('#' + id + ' li:nth-child(' + index + ')').addClass('highlights');
        } else {
          $('#' + id + ' li:eq(' + (index + 1) + ')').addClass('highlights');
        }
      },
      update: function(event, ui) {
        $('#' + id + ' li').removeClass('highlights');
        resetQuestionSequence(id);
      }
    }).disableSelection();
  }
  $(document).on('change', '#customer_id', function() {
    var id = $(this).val();
    if (id > 0) {
      getDataThroughAxiosModal('/getClustersFromCustomer/' + id, 'cluster_table_body_id');
    } else {
      getDataThroughAxiosModal('/getClusters', 'cluster_id');
    }
  });
  $(document).on('change', '#cluster_id', function() {
    var id = $(this).val();
    if (id > 0) {
      getDataThroughAxios('/getCompetencyfromCluster/' + id, 'competency_name_id');
    }
  });
  $(document).on('change', '#competency_name_id', function() {
    var id = $(this).val();
    console.log(id);
    var exercise_type_id = 0;
    $('.exercise_type_checkbox').each(function(){
      if(this.checked == true){
        exercise_type_id = $(this).val();
        console.log($(this).val());
      }
    });
    var customer_party_id = $('#customer_id').val();
    if (id > 0) {
      populateDataQuestions(id, exercise_type_id);
    }
  });

  function getDataThroughAxios(url, control_id) {
    axios.get(url)
      .then(function(response) {
        console.log(response);
        $("#" + control_id).empty();
        populateData(response['data'], control_id);
      })
      .catch(function(error) {
        console.log("error" + error);
      });
  }
  function getDataThroughAxiosModal(url, control_id) {
    axios.get(url)
      .then(function(response) {
        console.log(response);
        $("#" + control_id).empty();
        populateDataForModal(response['data'], control_id);
      })
      .catch(function(error) {
        console.log("error" + error);
      });
  }
  $(document).on('change','input.clusters',function(){
     if(this.checked == true){
      populateCompetencies(this.value);
    }
     else{
        removeCompetencies(this.value);
     }

  });
  $(document).on('change','#select_all_clusters',function(){
    if(this.checked == true){
    $('.clusters').each(function(index, el) {
      $(this).prop('checked', true);
       populateCompetencies(this.value);
    });
    }else{
       $('.clusters').each(function(index, el) {
      $(this).prop('checked', false);
      removeCompetencies(this.value);
    });

    }
  });
  $(document).on('change','#select_all_competency',function(){
    if(this.checked == true){
    $('.competencies').each(function(index, el) {
      $(this).prop('checked', true);
    });
    }else{
       $('.competencies').each(function(index, el) {
      $(this).prop('checked', false);
    });

    }
  });
  $(document).on('click','#show_questions',function(event){
    event.preventDefault();
     $(".loader").fadeIn();
     var exercise_type_id = 0;
    $('.exercise_type_checkbox').each(function(){
      if(this.checked == true){
        exercise_type_id = $(this).val();
        console.log($(this).val());
      }
    });
     var competencyIDs = $("#competency_table_id input:checkbox:checked.competencies").map(function(){
      return $(this).val();
    }).get();
    console.log(competencyIDs);
     var question_type_id = $('#question_type_id').val();
      populateDataQuestions(competencyIDs, exercise_type_id,question_type_id);
  });
  function populateCompetencies(cluster_id){
    console.log(competencies);
    for(var key in competencies) {
      var value = competencies[key];
      if(value.cluster_id == cluster_id){
       $('#competency_table_body_id').append("<tr><td width='10%'><input type='checkbox' class='competencies' value="+value.id+"><input type='hidden' value="+value.cluster_id+">\
        </td><td width='90%'>"+value.name+"</td></tr>");
    }
  }
}
function removeCompetencies(cluster_id){
       $('#competency_table_body_id tr').each(function(){
        if($(this).find('td:eq(0)').find('input[type=hidden]').val() == cluster_id){
          $(this).remove();
        }
       });
}
  function getCompetencies(){
   axios.get('/retrieveAllCompetencies')
      .then(function(response) {
        console.log(response);
        $.each(response['data'], function(key, value) {
        competencies[key] = {id: value.id,name: value.name, cluster_id: value.cluster_id};
      });
      })
      .catch(function(error) {
        console.log("error" + error);
      });


      console.log(competencies);
  }
  function populateData(data, control_id) {
    if (Object.keys(data).length > 0) {
      $('#' + control_id).append("<option value='0'>---Select---</option>");
      $.each(data, function(key, value) {
        $("#" + control_id).append("<option value='" + value.id + "'>" + value.name + "</option>");
      });
    }
  }

  function populateDataForModal(data, control_id) {
    if (Object.keys(data).length > 0) {
      $.each(data, function(key, value) {
        $("#" + control_id).append("<tr><td><input type='checkbox' class='clusters' value='"+value.id+"'></td><td>"+value.name+"</td></tr>");
      });
    }
  }

  function populateDataQuestions(id,exercise_type_id,question_type_id) {
     $('#myModal .errorMessage').html('');
     axios.get('/questionsByCompetency/' + JSON.stringify(id)+'/'+exercise_type_id+'/'+question_type_id)
      .then(function(response) {
        console.log(response);
        $("#selectable").empty();
        populateQuestions(response['data']);
      })
      .catch(function(error) {
        console.log("error" + error);
      });
  }

  function populateQuestions(data) {
    if(Object.keys(data).length == 0){
       $('#myModal .errorMessage').html("No questions found for checked competencies.");
        $(".loader").fadeOut("slow");
       return false;
    }
    $('#selectable').append('<li class="ui-widget-content"><input type="checkbox" class="select_all_questions"> Select all questions</li>');
    $.each(data, function(key, value) {
      $("#selectable").append('<li style="font-size:13px;" class="ui-widget-content question-text"><input type="checkbox" class="question_list" value="' + value.id + '"> ' + value.main_text + '</li>');
    });
     $(".loader").fadeOut("slow");
  }
  $(document).on("change", ".select_all_questions", function(e) {
      e.stopPropagation();
      if (this.checked == true) {
      $(".question_list").each(function(index, el) {
        $(this).prop('checked', true);
        $(this).parent().addClass('ui-selected');
      });
    }else{
      $(".question_list").each(function(index, el) {
        $(this).prop('checked', false);
        $(this).parent().removeClass('ui-selected');
      });
    }
    });
  function resetModal() {
    $('#myModal .errorMessage').html('');
    $('.clusters').prop('checked', false);
    $('.competencies').prop('checked', false);
    $('#competency_table_body_id').empty();
    $('#cluster_id').val(0);
    $('#competency_name_id').val(0);
    $("#selectable").empty();
  }

  function resetQuestionSequence(id) {
    var counter = 1;
    $("#" + id + " > li").each(function() {
      var rowCount = $(this).text().trim().replace(/(^\d+)(.+$)/i, '$1');
      $(this).html($(this).html().replace(rowCount, counter));
      counter++;
    });
  }

  function setFocus(id) {
    console.log(id);
    $('#' + id).focus();
  }

  function checkQuestionsAddInList(question_id, selectable_id) {
    var question = '';
    $("#" + selectable_id + " > li").each(function() {
      console.log($(this).find('input[type=hidden]').val());
      console.log(question_id);
      console.log($(this).find('input[type=hidden]').val() == question_id);
      if ($(this).find('input[type=hidden]').val() == question_id) {
        var rowCount = $(this).text().trim().replace(/(^\d+)(.+$)/i, '$1');
        console.log(rowCount);
        question = $(this).text().replace(rowCount, '');
        console.log(question);
        return false;
      }
    });
    console.log(question);
    return question;
  }
  $(document).on('click', '#add_question', function() {
    $('#myModal .errorMessage').html('');
        var page_id = $('#current_page_id').val();//$(this).closest('div').siblings().attr('id');
        var page_count = page_id.replace( /^\D+/g, '');//$('#'+page_id).closest('div').siblings().find(':checkbox').val();
        var question_div_id = $('#' + page_id).parent().next().next().attr('id');
        // $('#' + page_id).parent().closest('div').siblings().closest('.questions').attr('id');
        console.log(question_div_id);
        var selectable_id = $('#' + question_div_id).children().attr('id');
        console.log(selectable_id);
        var counter = $('#' + selectable_id + ' .ui-state-default').length + 1;
        if ($('#' + question_div_id).hasClass('hide')) {
          $('#' + question_div_id).removeClass('hide');
        }
        makeSortable(selectable_id);
        $("ol.selectable > li").each(function() {
          if ($(this).hasClass('ui-selected') && $(this).hasClass('question-text')) {
            var question = $(this).text();
            var question_id = $(this).find(':checkbox').val();
            var questionExist = checkQuestionsAddInList(question_id, selectable_id);
            console.log(questionExist);
            if (questionExist == '') {
              $('#' + selectable_id).append('<li class="ui-state-default">' + counter + '&nbsp;&nbsp;&nbsp;&nbsp;' + question.trim() + '<i class="vnn vnn-menu-down del"></i>\
                <input type="hidden" name="questions['+Number(page_count)+'][]" value="' + question_id + '">\
                <input type="hidden" name="question_name['+Number(page_count)+'][]" value="' + question.trim() + '">\
                <i class="vnn vnn-del arrow-up-down remove" style="cursor: pointer;"></i></li>');
              counter++;
            } else {
              console.log(questionExist);
              $('#myModal .errorMessage').html("This question " + '"' + questionExist.trim() + '"' + " is already added.");
            }
          }
        });
  });

  $(document).on('keydown', '#exercise_name_id', function(e) {
      if (!$(this).text().trim()) {  
         if(e.keyCode === 8){
             return false;
          }
      }
  });
  $(document).on('keydown', '.page_title_class', function(e) {
      if (!$(this).text().trim()) {  
         if(e.keyCode === 8){
             return false;
          }
      }
  });

  $(document).on('keyup', '#exercise_name_id', function() {
    console.log($(this).text().trim());
    $('#exercise_hidden_id').val($(this).text().trim());
  });
  $(document).on('keyup', '.page_title_class', function() {
    var page_hidden_id = $(this).find('input[type=hidden]').attr('id');
    $('#'+page_hidden_id).val($(this).text().trim());
  });
  $(document).on('click', '.remove', function() {
    var selectable_id = $(this).closest('ul').attr('id');
    var question_div_id = $(this).closest('ul').parent().attr('id');
    $(this).closest('li').remove();
    if ($('#' + selectable_id + ' li').length == 0) {
      $('#' + question_div_id).addClass('hide');
    }
    resetQuestionSequence(selectable_id);
  });
  $(document).on('change', '#exercise_type_id', function() {
    var exercise_type = $("#exercise_type_id option:selected").text();
    if (exercise_type == 'Assessment') {
      $('#assessment_type_div_id').removeClass('hide');
    } else {
      $('#assessment_type_div_id').addClass('hide');
    }
  });
  $(document).on('change', 'input.exercise_type_checkbox', function() {
    if ($(this).closest('label').text() == 'Assessment' && this.checked == true) {
      // $('#assessment_type_div_id').removeClass('hide');
    } else {
      $('#assessment_type_div_id').addClass('hide');
    }
    $('input.exercise_type_checkbox').not(this).prop('checked', false);
  });

  function validateMandatoryFields() {
    $('#errorMessageDiv .errorMessage').html('');
    var valid = true;
    var exercise_header = $('#exercise_hidden_id').val();
    var question_list = $('#sortable li').length;
    $(".mandatory").each(function() {
      if ($(this).val().trim() === '' || $(this).val() === '0') {
        $(".mandatory").removeClass('fillFields');
        $(this).addClass('fillFields');
        $('#errorMessageDiv .errorMessage').html('Please fill all mandatory fields.');
        valid = false;
        return false;
      } else {
        $(".mandatory").removeClass('fillFields');
      }
    });
    if (exercise_header == '') {
      console.log("exercise header" + exercise_header);
      $('#errorMessageDiv .errorMessage').html('Please fill up excercise header.');
      setFocus('exercise_name_id');
      valid = false;
      return valid;
    }
    if(checkQuestionsAdded()){
      $('#errorMessageDiv .errorMessage').html('Please add atleast one question.');
     valid = false;
    }
    return valid;
  }
function checkQuestionsAdded(){
    var countQuestions = $('.questions').length;
    if(countQuestions == 0){
      return true;
    }else{
      return false;
    }
}

  $(document).on('click', '#add_page', function() {
    $('#errorMessageDiv .errorMessage').html('');
    var rowLength = $('.questions').length;
      console.log(rowLength);
      if(rowLength == 10){
    $('#errorMessageDiv .errorMessage').html('Cannot add more than 10 Pages.');
    return false;
      }
    appendAPage('parent_page_div', 'page_title_div_id', 'page_title_hidden_id', 'questions_div_id', 'sortable', rowLength);

  });

  function appendAPage(parent_div, page_title_id, page_hidden_id, question_div_id, questions_order_list_id, rowLength) {
    $('#' + parent_div).append('<div class="row"><div class="row page-css">\
      <div class="col-lg-11 page_title_class" id="' + page_title_id + rowLength + '" contenteditable="true"><h4 class="" align="left">Page Title</h4>\
      <input type="hidden" id="' + page_hidden_id + rowLength + '" name="page_title[]" value="">\
       <input type="hidden" class="exercise_bank_page" id="exercise_bank_page_hidden_id'+rowLength+'" name="exercise_bank_page_id[]" value="">\
       <input type="hidden" class="page_no" id="page_no_id0" name="page_no[]" value="' + (rowLength + 1) + '">\
      </div>\
      <div class="col-lg-1">\
      <i class="vnn vnn-edit edit-pencil vnn-white" onclick="return setFocus("' + page_title_id + rowLength + '");"></i>\
      <i class="vnn vnn-create i-font add-plus add_question vnn-white"></i>\
      <i class="vnn vnn-del remove_page remove-circle vnn-white"></i></div></div><br>\
      <div id="' + question_div_id + rowLength + '" class="row col-lg-12 questions hide" style="overflow:auto; height:220px;">\
      <ul id="' + questions_order_list_id + rowLength + '" class="sortable ui-sortable"></ul>\
      </div><br/></div>');
  }
  $(document).on('click', '.add_question', function() {
    $('#errorMessageDiv .errorMessage').html('');
    $('#current_page_id').val($(this).parent().siblings().closest('.page_title_class').attr('id'));
      $('#myModal').modal({
        show: 'true',
        backdrop: 'static',
        keyboard: false
      });
  });
  function deleteRecord(id,route){

}
  $(document).on('click','.remove_page',function(){
     $('#errorMessageDiv .errorMessage').html('');
    var page_count = $('.page_title_class').length;
    if(page_count == 1){
       $('#errorMessageDiv .errorMessage').html('Cannot remove all pages from an exercise.');
      return false;
    }
     var exercise_bank_page_id = $(this).parent().parent().find('.page_title_class').children().closest('.exercise_bank_page').val();
     console.log(exercise_bank_page_id);
     var removePage = $(this).closest('div').parent().parent();


    bootbox.confirm({
      message: "Do you want to delete this page?",
      buttons: {
          confirm: {
              label: 'Yes',
              className: 'btn-success'
          },
          cancel: {
              label: 'No',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
          if (result){
            deleteActualPage(exercise_bank_page_id);
             removePage.remove();
            reSequencePageNo();

          }

      }
  });
  });
  function reSequencePageNo(){
    var pageCount = 1;
    $('.page_title_class').each(function(){
      $(this).children().closest('.page_no').val(pageCount);
      pageCount++;
    });

  }
  function deleteActualPage(exercise_bank_page_id){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  if(exercise_bank_page_id){
  $.ajax({
    type: "delete",
    url: "/deletePage/"+exercise_bank_page_id,
    success: success,
  });
  }
  function success(data){

    }
  }
