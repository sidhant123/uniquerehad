<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\ProfitLossReportService;
use App\Services\SiteService;
use App\Models\Site;
use App\Services\ProjectService;



class ProfitLossReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function create() {

         $project = ProjectService::retrieveProject();
		return view('reports.profit_loss_report' ,compact('project'));
	}

	public function exportReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$Code = ProfitLossReportService::getName($request['project_id']);
		$queryBuilder = ProfitLossReportService::retrieveProfitLossReportQuery($request['project_id']);
		$queryPayment = ProfitLossReportService::retrievePaymentAmountQuery($request['project_id']);
		$queryReceipt = ProfitLossReportService::retrieveReceiptAmountQuery($request['project_id']);
		$DataExtraAmount = ProfitLossReportService::retrieveExtraAmountQuery($request['project_id']);
// dd($queryTDS);
	    self::downloadReportExcel($queryBuilder, $Code, $queryPayment, $queryReceipt, $DataExtraAmount);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadReportExcel($queryBuilder, $Code, $queryPayment, $queryReceipt,$DataExtraAmount ) {
		$fileName = $Code . '_Profit_&_Loss';
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
          $payment_data = $queryPayment->get();
          $received_data = $queryReceipt->get();
       
//dd($row_data);

        // creating rows  
        $allData = array();
        $paid = 0;
        $received = 0;
       // if(!empty($row_data)) { 
        foreach ($row_data as $row) {
        	$allData[] = array($row->expenses, $row->amount);
		}
        foreach ($payment_data as $payment) {
    		$allData[] = array('', '');
    		$allData[] = array($payment->col1, $payment->total_payment_amount);
    		$paid = $payment->total_payment_amount;
        } 
        foreach ($received_data as $received) {
    		$allData[] = array($received->col1, $received->total_received_amount);
    		$received = $received->total_received_amount;
        }
         
		$profit = strval(trim($received)) - strval(trim($paid));
        $allData[] = array('Profit', $profit);
    	$allData[] = array('', '');

        foreach ($DataExtraAmount as $extra) {

    		$allData[] = array($extra->col1, $extra->total_extra_amount);
        }

    		
    	//}

        $keys = array("Expences","Amount");
        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}

}
