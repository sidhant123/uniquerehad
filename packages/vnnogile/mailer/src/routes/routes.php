<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ["web", "auth", "CheckRole:Tenant Administrator"]], function () {
	Route::get('email/send','vnnogile\Mailer\Controllers\MailController@create')->name('email.send');
	Route::post('/sendMail', 'vnnogile\Mailer\Controllers\MailController@sendMail');
	Route::get('/getEmailData', 'vnnogile\Mailer\Controllers\EmailAddressController@getEmailData');
	Route::post('/uploadAttachments', 'vnnogile\Mailer\Controllers\MailController@uploadFile');
	Route::post('/removeAttachment', 'vnnogile\Mailer\Controllers\MailController@deleteFile');
});



// Route::group(['middleware' => 'CheckCompositeMiddleWare'], function() {
//   Route::resource('templates', 'vnnogile\Utilities\Utilities\TemplateController',['only' => ['create', 'index']]);
// });