<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use App\Models\ArtefactType;
use App\Models\Configuration;
use App\Models\Forum;
use App\Models\ForumThread;
use App\Models\Organization;
use App\Models\PeopleAttribute;
use App\Models\Person;
use App\Models\Phone;
use App\Models\Program;
use App\Models\ProgramAttribute;
use App\Models\ProgramCredential;
use App\Models\ProgramLtTopic;
use App\Models\ProgramUserBadge;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\TopicUser;
use App\Services\TenantService;
use App\User;
use Config;
use DB;
use Hashids\Hashids;
use Illuminate\Support\Facades\Log;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\ProgramReminders\Services\ProgramReminderService;

class ProgramService {

	// setting global values
	var $vc_artefact_type_id;
	var $assignment_artefact_type_id;
	var $moderator_ids;
	var $facilitator_ids;
	var $program_lt_topics;

	var $topic_to_update;
	private static $filePath = "program/";

	// protected $Person;
	// protected  static $person=Person::all();

	// public function __construct()
	// {
	//     Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	//     $person= Person::all();
	//     Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	// }

	public static function saveProgram($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($data['program_id']);
		if ($data['program_id'] != "tesseract") {
			$program_id = ProgramService::updateProgram($data);
			return $program_id;
		}

		$program_id = DB::transaction(function () use ($data) {
			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['tenantPartyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			// learning tree
			$learning_tree['details']['programTitle'] = $data['program_name'];
			$lt = json_encode($learning_tree);

			//in program table
			$program = new Program;

			$program->code = $data['program_code'];

			$program->name = $data['program_name'];
			$program->description = $data['program_description'];
			$program->start_date = date('Y-m-d', strtotime($data['program_from']));
			$program->end_date = date('Y-m-d', strtotime($data['program_to']));
			$program->version = "1";

			if (!empty($data['program_photo'])) {
				$program->photo = $data['program_photo'];
			}

			if (isset($data['program_self_enrollment'])) {
				$program->allow_self_enrol = "1";
			}
			// else{
			// $program->allow_self_enrol="0";
			// }

			$customer = Organization::where('id', $data['company_id'])->first();
			// Log::debug($customer);
			$program->customer_party_id = $customer['party_id'];

			// $program->badges_enabled= "1";
			$program->fastest_activity_completion_user_limit = $data['fastest_activity'];
			$program->participant_distribution_badges = $data['participant_badges'];
			$program->content_rating_badges = $data['content_per_badge'];

			$program->tenant_party_id = $data['tenantPartyId'];
			$program->created_by = $data['loggedInUserId'];
			$program->updated_by = $data['loggedInUserId'];

			$program->learning_tree = $lt;
			$program->save();
			$program_id = $program->id;

			// saving forum
			$forum = new Forum;
			$forum->program_id = $program_id;
			$forum->created_by = $data['loggedInUserId'];
			$forum->updated_by = $data['loggedInUserId'];
			$forum->save();
			$forum_id = $forum->id;

			// saving in forum threads
			$forum_thread = new ForumThread;
			$forum_thread->forum_id = $forum_id;
			$forum_thread->subject = 'This is default forum thread for program ' . $data['program_code'];
			$forum_thread->description = 'This is default forum thread for program ' . $data['program_code'];
			$forum_thread->created_by = $data['loggedInUserId'];
			$forum_thread->updated_by = $data['loggedInUserId'];
			$forum_thread->save();

			// private forum
			if (isset($data['private_forum_available'])) {
				//
				// if ($data['approving_person_role'] == 'custom') {
				// 	//
				// 	$approving_user_emailId = $data['approving_person'];
				// } else {
				// 	//
				// 	$approving_user_id = $data['approving_person'];

				// }

				$approving_user = $data['approving_person'];
				$approving_role = $data['approving_person_role'];

				$programCredentials = new ProgramCredential;
				$programCredentials->program_id = $program_id;
				$programCredentials->attr_name = 'forum_approving_person_role';
				$programCredentials->attr_value = $approving_role;
				$programCredentials->created_by = $data['loggedInUserId'];
				$programCredentials->updated_by = $data['loggedInUserId'];
				$programCredentials->save();

				$programCredentials = new ProgramCredential;
				$programCredentials->program_id = $program_id;
				$programCredentials->attr_name = 'forum_approving_person';
				$programCredentials->attr_value = $approving_user;
				$programCredentials->created_by = $data['loggedInUserId'];
				$programCredentials->updated_by = $data['loggedInUserId'];
				$programCredentials->save();
			}

			Log::debug($program_id);

			// ///in program badge table
			// // $program=self::getUserObject($value);
			// $program->user()->attach($role['id'], ['created_by' => $data['loggedInUserPartyId'], 'updated_by' => $data['loggedInUserPartyId']]);
			self::uploadProgramDocument($data, $program_id);

			// saving default topic reminder time
			self::setDefaultTopicReminderTime($data, $program_id);
			return $program_id;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $program_id;
	}

	private static function setDefaultTopicReminderTime($data, $program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		//

		$default_start_time = 6;
		$default_end_time = 6;

		$program_attr = new ProgramAttribute();

		$program_attr->program_id = $program_id;
		$program_attr->remind_before_topic_start_time = $default_start_time;
		$program_attr->remind_before_topic_end_time = $default_end_time;

		$program_attr->created_by = $data['loggedInUserId'];
		$program_attr->updated_by = $data['loggedInUserId'];

		// for status_color_allocation_method
		if (isset($data['status_color_allocation_method'])) {
			$program_attr->status_color_allocation_method = $data['status_color_allocation_method'];
		}

		$status = $program_attr->save();

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	private static function uploadProgramDocument($data, $program_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$data['masterDataInterface'] = $data['masterDataInterface'];
		//storage configurations
		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;
		$data['program_id'] = $program_id;
		$document_details = self::uploadDocument($data);
		Log::debug($document_details);
		$prog = Program::find($program_id);
		if (isset($data['program_photo'])) {
			$prog->photo = $document_details['originalFileNames']['program_photo'][0];
			$prog->photo_path = "/storage/" . self::$filePath . $program_id . "/" . $document_details['uploadedFileNames']['program_photo'][0];
		}
		if (isset($data['program_document'])) {
			$prog->document_name = $document_details['originalFileNames']['program_document'][0];
			$prog->document_path = "/storage/" . self::$filePath . $program_id . "/" . $document_details['uploadedFileNames']['program_document'][0];
		}
		$prog->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

	}

	private static function uploadDocument($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->uploadFileToPublicStorage($data['program_request'], self::$filePath . $data['program_id']);
	}
	private static function deleteProgramDocument($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->deleteFile(str_replace("storage", "public", $data['remove_file']));
	}

	public static function saveProgramDirector($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		//temp code

//Log::debug($_SERVER['SERVER_ADDR']);
		//Log::debug($_SERVER['SERVER_PORT']);

		$program_id = DB::transaction(function () use ($data) {

			$role = Role::where('name', 'Program Director')->first();

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			//get Learning tree from program
			$program = Program::where('id', $data['program_id'])->first();
			$learning_tree = $program['learning_tree'];
			$lt = json_decode($learning_tree, true);

			//assgning roles *in program role remaining

			if (isset($lt['programDirector'])) {
				if ($lt['programDirector'] == $data['director_id']) {
					//no action
				} else {

					// detach from program user
					$role_user = RoleUser::where(['user_id' => $lt['programDirector'], 'role_id' => $role['id']])->first();
					$program->role_user()->detach($role_user['id']);

					//attach role -- only if not present
					self::attachRole($data['director_id'], $program, $role['id'], $data['loggedInUserId']);

					$approvedDirectorForMail = [$data['director_id']];
					$unapprovedDirectorForMail = [$lt['programDirector']];

					// sending mail for new director
					self::sendMailToProgramUsers($approvedDirectorForMail, 'Program Director', $program, 'add');
					// sending mail for removed director
					self::sendMailToProgramUsers($unapprovedDirectorForMail, 'Program Director', $program, 'remove');

				}
			} else {

				//attach role -- only if not present
				self::attachRole($data['director_id'], $program, $role['id'], $data['loggedInUserId']);
			}

			// new logic
			$lt['programDirector'] = $data['director_id'];
			$lt['programDirectorProfile'] = $data['profile_summary'];

			// abhi-- message from
			if ($data['message_from'] != "") {
				$lt['messageFrom'] = $data['message_from'];
			}

			if (isset($data['video_id_checkbox'])) {
				$lt['programDirectorVideoName'] = $data['document_name'];
				$lt['programDirectorVideoId'] = $data['video_id_checkbox'];
				$lt['programDirectorVideoUrl'] = str_replace("public", "storage", $data['document_url']);

			}
			$lt['programDirectorVideoAvailableFrom'] = $data['director_from'];
			$lt['programDirectorVideoAvailableTo'] = $data['director_to'];

			$lt2 = json_encode($lt);
			$program_id = Program::where('id', $data['program_id'])->update(['learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId']]);

			Log::debug($program_id);
			// for publish btn
			$publish = self::getPublishCondition($lt);

			$returning_data['id'] = $program_id;
			$returning_data['publish'] = $publish;

			return $returning_data;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $program_id;
	}

	public static function saveProgramCoordinators($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$program_id = DB::transaction(function () use ($data) {

			$role = Role::where('name', 'Program Coordinator')->first();

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			//get Learning tree from program
			$program = Program::where('id', $data['program_id'])->first();
			$learning_tree = $program['learning_tree'];
			$lt = json_decode($learning_tree, true);

			//////assigning roles
			$users = array_values($data["users"]);

			if (isset($lt['programCoordinator'])) {

				// $users=
				$common = array();
				///fitering the same
				foreach ($lt['programCoordinator'] as $key => $value) {
					foreach ($users as $no => $id) {
						if ($value == $id) {
							$common[] = $value;
						}
					}
				}
				Log::debug($lt['programCoordinator']);
				Log::debug($users);
				Log::debug($common);

				//detaching
				$participantToBeDetached = array_diff($lt['programCoordinator'], $common);
				Log::debug($participantToBeDetached);

				foreach ($participantToBeDetached as $key => $value) {

					// detach from program user --not in role_user
					$role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role['id']])->first();
					$program->role_user()->detach($role_user['id']);

				}

				//attaching
				//eleminating common from users
				$participantToBeAttached = array_diff($users, $common);
				Log::debug($participantToBeAttached);

				foreach ($participantToBeAttached as $key => $value) {

					self::attachRole($value, $program, $role['id'], $data['loggedInUserId']);
				}

				// sending mail for added coordinators
				if (count($participantToBeAttached) > 0) {
					//
					self::sendMailToProgramUsers($participantToBeAttached, 'Program Coordinator', $program, 'add');
				}
				// sending mail for removed coordiantors
				if (count($participantToBeDetached) > 0) {
					//
					self::sendMailToProgramUsers($participantToBeDetached, 'Program Coordinator', $program, 'remove');
				}

			} else {
				//attach role
				foreach ($users as $key => $value) {

					self::attachRole($value, $program, $role['id'], $data['loggedInUserId']);
				}
			}

			//
			$lt['programCoordinator'] = $users;

			$lt2 = json_encode($lt);
			$program_id = Program::where('id', $data['program_id'])->update(['learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId']]);

			Log::debug($program_id);

			// for publish btn -- return data
			$publish = self::getPublishCondition($lt);

			$returning_data['id'] = $program_id;
			$returning_data['publish'] = $publish;

			return $returning_data;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $program_id;
	}

	public static function updateProgram($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$program_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['tenantPartyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			$data['masterDataInterface'] = $data['masterDataInterface'];
			//storage configurations
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;
			//first remove and upload if availble
			if (isset($data['program_photo'])) {
				if (isset($data['program_photo_path'])) {
					$data['remove_file'] = $data['program_photo_path'];
					self::deleteProgramDocument($data);
				}
			}
			if (isset($data['program_document'])) {
				if (isset($data['program_document_path'])) {
					$data['remove_file'] = $data['program_document_path'];
					self::deleteProgramDocument($data);
				}
			}

			// getting learning tree
			$program = Program::where('id', $data['program_id'])->first();
			Log::debug($program);
			$learning_tree = $program['learning_tree'];
			Log::debug($learning_tree);
			$lt = json_decode($learning_tree, true);
			Log::debug($lt);

			// update lt
			// unset($learning_tree['details']['programTitle']);
			$lt['details']['programTitle'] = $data['program_name'];
			Log::debug("//////");
			Log::debug($lt);
			$lt2 = json_encode($lt);

			//self enrolment
			if (isset($data['program_self_enrollment'])) {
				Program::where('id', $data['program_id'])->update(['allow_self_enrol' => '1']);
			}

			//customer
			$customer = Organization::where('id', $data['company_id'])->first();

			///updating program
			// $program_id = Program::where('id', $data['program_id'])->update(['code' => $data['program_code'], 'name' => $data['program_name'], 'description' => $data['program_description'], 'start_date' => date('Y-m-d', strtotime($data['program_from'])), 'end_date' => date('Y-m-d', strtotime($data['program_to'])), 'customer_party_id' => $customer['party_id'], 'learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId'], 'participant_distribution_badges' => $data['participant_badges'], 'content_rating_badges' => $data['content_per_badge'], 'fastest_activity_completion_user_limit' => $data['fastest_activity']]);

			$update_array = ['code' => $data['program_code'], 'name' => $data['program_name'], 'description' => $data['program_description'], 'start_date' => date('Y-m-d', strtotime($data['program_from'])), 'end_date' => date('Y-m-d', strtotime($data['program_to'])), 'customer_party_id' => $customer['party_id'], 'learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId'], 'participant_distribution_badges' => $data['participant_badges'], 'content_rating_badges' => $data['content_per_badge'], 'fastest_activity_completion_user_limit' => $data['fastest_activity']];

			if (isset($data['program_photo']) || isset($data['program_document'])) {
				self::uploadProgramDocument($data, $data['program_id']);
			}
			///updating program
			$program_id = Program::where('id', $data['program_id'])->update($update_array);

			// program attribute
			$programAttribute = ProgramAttribute::where('program_id', $data['program_id'])->first();
			if ($programAttribute && isset($data['status_color_allocation_method'])) {
				$programAttribute->status_color_allocation_method = $data['status_color_allocation_method'];
				$programAttribute->updated_by = $data['loggedInUserId'];
				$programAttribute->save();
			}

			// private forum
			$program_credentials = ProgramCredential::where(['program_id' => $data['program_id'], 'attr_name' => 'forum_approving_person']);

			if (isset($data['private_forum_available'])) {
				$approving_user = $data['approving_person'];
				$approving_role = $data['approving_person_role'];

				if ($program_credentials->first()) {
					// update
					$program_credentials = $program_credentials->first();
					if ($program_credentials['attr_value'] != $approving_user) {
						$program_credentials->attr_value = $approving_user;
						$program_credentials->updated_by = $data['loggedInUserId'];
						$program_credentials->save();
					}

					$program_credentials_role = ProgramCredential::where(['program_id' => $data['program_id'], 'attr_name' => 'forum_approving_person_role'])->first();
					if ($program_credentials_role['attr_value'] != $approving_role) {
						$program_credentials_role->attr_value = $approving_role;
						$program_credentials_role->updated_by = $data['loggedInUserId'];
						$program_credentials_role->save();
					}

				} else {
					// create

					$programCredentials = new ProgramCredential;
					$programCredentials->program_id = $data['program_id'];
					$programCredentials->attr_name = 'forum_approving_person_role';
					$programCredentials->attr_value = $approving_role;
					$programCredentials->created_by = $data['loggedInUserId'];
					$programCredentials->updated_by = $data['loggedInUserId'];
					$programCredentials->save();

					$programCredentials = new ProgramCredential;
					$programCredentials->program_id = $data['program_id'];
					$programCredentials->attr_name = 'forum_approving_person';
					$programCredentials->attr_value = $approving_user;
					$programCredentials->created_by = $data['loggedInUserId'];
					$programCredentials->updated_by = $data['loggedInUserId'];
					$programCredentials->save();
				}

			} else {
				if ($program_credentials->first()) {
					//delete record
					$program_credentials->forceDelete();
					ProgramCredential::where(['program_id' => $data['program_id'], 'attr_name' => 'forum_approving_person_role'])->forceDelete();
				}
			}
			// private forum

			return $data['program_id'];
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		Log::debug($program_id);

		return $program_id;
	}

	// public static function getPersonPartyId($id){
	//     $people= Person::all();
	//     return $people;
	// }

	public static function getUserObject($user_id) {
		$user = User::where('id', $user_id)->first();
		return $user;
	}

	public static function importParticipantsFromProgram($program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($program_id . "hiiiiiiii");
		$program = Program::where('id', $program_id)->first();
		$people = Person::where('customer_party_id', $program['customer_party_id'])->get();
		$lt = json_decode($program['learning_tree'], true);

		$participants = array();
		if (isset($lt['participants'])) {
			//
			$users = User::all();
			$participants = $users->whereIn('id', $lt['participants'])->where('is_active', 1);
		}

		Log::debug(count($participants));
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}
	public static function importParticipantsWithPhoneFromProgram($program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($program_id . "hiiiiiiii");
		$program = Program::where('id', $program_id)->first();
		$people = Person::where('customer_party_id', $program['customer_party_id'])->get();
		$lt = json_decode($program['learning_tree'], true);

		$participants = array();
		if (isset($lt['participants'])) {
			//
			// $users = User::all();
			// $participants = $users->whereIn('id', $lt['participants']);
			$participants = DB::table('users')->select('users.id', 'users.first_name', 'users.last_name', 'phones.phone_number')->where('is_active', 1)
				->leftJoin('phones', function ($join) {
					$join->on('users.party_id', '=', 'phones.party_id');
					// $join->on('phones.phone_type_id', '=', 1);
				})->whereIn('users.id', $lt['participants'])->where('phones.phone_type_id', 1)
				->get();
		}

		Log::debug(count($participants));
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}
	public static function getProgram($id) {
		return Program::find($id);
	}

	public static function getProgramByCustomers($customer_party_id) {
		return Program::where('customer_party_id', $customer_party_id)->get();
	}

/////////////////////////////////////////////// Approve Participants  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public static function approveParticipants($data) {

		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$return_data = DB::transaction(function () use ($data) {

			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}
			// $data['loggedInUserId'] = '3';

			$role = Role::where('name', 'Participant')->first();
			$program = Program::where('id', $data['program_id'])->first();

			// finding lt
			$lt = json_decode($program['learning_tree'], true);
			if (!isset($lt['approvedParticipants'])) {
				//
				$lt['approvedParticipants'] = [];
			}

			if (isset($lt['participants'])) {

				$commonP = array_intersect($data['added_participants'], $lt['participants']);
				$participantToBeDetached = array_diff($lt['participants'], $commonP);
				$participantToBeAttached = array_diff($data['added_participants'], $commonP);

				$AttachApprove = array_intersect($data['approved_participants'], $participantToBeAttached);
				$ApproveC = array_intersect($data['approved_participants'], $commonP);
				$Approve = array_diff($ApproveC, $lt['approvedParticipants']);
				$Attach = array_diff($participantToBeAttached, $AttachApprove);

				$NewApproveForEmail = array_merge($Approve, $AttachApprove);

				//detaching
				foreach ($participantToBeDetached as $key => $value) {

					// dont detach from role user
					$program->user_badges()->detach($value);
				}

				// attach and approve
				foreach ($AttachApprove as $key => $value) {
					//
					self::attachApproveParticaipants($value, $program, $role['id'], $data['loggedInUserId']);
					//
					self::attachRole($value, $program, $role['id'], $data['loggedInUserId']);
				}
				foreach ($Attach as $key => $value) {
					//
					self::attachParticaipants($value, $program, $role['id'], $data['loggedInUserId']);
				}
				foreach ($Approve as $key => $value) {
					// only approve
					$status = ProgramUserBadge::where(['program_id' => $data['program_id'], 'user_id' => $value])->update(['approved' => 1, 'updated_by' => $data['loggedInUserId']]);
					//
					self::attachRole($value, $program, $role['id'], $data['loggedInUserId']);
				}

				// updating lt
				$lt['participants'] = $data['added_participants'];

				if ($lt['approvedParticipants']) {
					//
					$OldApprove = $lt['approvedParticipants'];
					$apprv_participants = array_merge($lt['approvedParticipants'], $data['approved_participants']);
					$updated_apr_parti = array_intersect($lt['participants'], $apprv_participants);
					$lt['approvedParticipants'] = $updated_apr_parti;

					$NewUnapproveForEmail = array_diff($OldApprove, $lt['approvedParticipants']);
					// detaching role from program user
					foreach ($NewUnapproveForEmail as $key => $value) {
						//
						self::detachRoleFromProgramUser($value, $role['id'], $program);
					}

				} else {
					$lt['approvedParticipants'] = $data['approved_participants'];
				}

			} else {
				// when no previous parti
				$aa = $data['approved_participants'];
				$ad = array_diff($data['added_participants'], $data['approved_participants']);

				foreach ($ad as $key => $value) {
					//
					self::attachParticaipants($value, $program, $role['id'], $data['loggedInUserId']);
				}

				foreach ($aa as $key => $value) {
					//
					self::attachApproveParticaipants($value, $program, $role['id'], $data['loggedInUserId']);
					self::attachRole($value, $program, $role['id'], $data['loggedInUserId']);
				}

				// update lt
				$lt['participants'] = $data['added_participants'];
				$lt['approvedParticipants'] = $data['approved_participants'];
			}

			//final update
			$lt2 = json_encode($lt);
			$program_id = Program::where('id', $data['program_id'])->update(['learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId']]);

			// email for approve and unapprove
			if (isset($NewApproveForEmail)) {
				//
				if (count($NewApproveForEmail) > 0) {
					//
					self::sendMailToProgramUsers($NewApproveForEmail, 'Participant', $program, 'add');
				}
			}
			if (isset($NewUnapproveForEmail)) {
				//
				if (count($NewUnapproveForEmail) > 0) {
					//
					self::sendMailToProgramUsers($NewUnapproveForEmail, 'Participant', $program, 'remove');
				}
			}

			// creating return data
			// $users = self::getUsersByCustomer($program['customer_party_id']);
			$person = Person::where('customer_party_id', $program['customer_party_id'])->get();
			$users = self::getOrganizationUsers($program['customer_party_id']);

			$program_users = $users->whereIn('id', $lt['participants']);
			$return_data['program_users'] = self::getUserWithEmployeeId($program_users, $person);

			$program_non_users = $users->whereNotIn('id', $lt['participants']);
			$return_data['program_non_users'] = self::getUserWithEmployeeId($program_non_users, $person);

			$return_data['program_appr_users'] = $lt['approvedParticipants'];

			// for publish
			$publish = self::getPublishCondition($lt);

			// for unapprove
			if ($lt['approvedParticipants']) {
				$return_data['unapprove'] = 'yes';
			} else {
				$return_data['unapprove'] = 'no';
			}

			$return_data['publish'] = $publish;

			return $return_data;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $return_data;
	}

	public static function detachRoleFromProgramUser($value, $role_id, $program) {
		//
		$role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();

		$program->role_user()->detach($role_user['id']);
	}

	public static function getPublishCondition($lt) {
		//
		$publish = 'no';
		if (isset($lt['approvedParticipants']) && isset($lt['programDirector']) && isset($lt['programCoordinator']) && isset($lt['learning_tree'])) {
			if ($lt['approvedParticipants'] && $lt['programDirector'] && $lt['programCoordinator'] && $lt['learning_tree']) {
				//
				$publish = 'yes';
			}
		}
		return $publish;
	}

///////////////////////////////////////////// Add participants \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public static function addParticipants($data) {

		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$return_data = DB::transaction(function () use ($data) {

			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}
			// $data['loggedInUserId'] = '3';

			$role = Role::where('name', 'Participant')->first();
			$program = Program::where('id', $data['program_id'])->first();
			$added_participants = $data['participants'];

			// Log::debug($added_participants);
			// exit();
			// finding lt
			$lt = json_decode($program['learning_tree'], true);
			if (!isset($lt['approvedParticipants'])) {
				//
				$lt['approvedParticipants'] = [];
			}

			if (isset($lt['participants'])) {

				$commonP = array_intersect($added_participants, $lt['participants']);
				$participantToBeDetached = array_diff($lt['participants'], $commonP);
				$participantToBeAttached = array_diff($added_participants, $commonP);

				//detaching
				foreach ($participantToBeDetached as $key => $value) {

					// dont detach from role user
					$program->user_badges()->detach($value);
				}

				//attaching
				foreach ($participantToBeAttached as $key => $value) {
					//
					self::attachParticaipants($value, $program, $role['id'], $data['loggedInUserId']);
				}

				// update lt
				$lt['participants'] = $added_participants;
				// for unapprove
				if ($lt['approvedParticipants']) {
					//
					$OldApprove = $lt['approvedParticipants'];
					$lt['approvedParticipants'] = array_intersect($lt['participants'], $lt['approvedParticipants']);
					$NewUnapproveForEmail = array_diff($OldApprove, $lt['approvedParticipants']);

					// detaching role from program user
					foreach ($NewUnapproveForEmail as $key => $value) {
						//
						self::detachRoleFromProgramUser($value, $role['id'], $program);
					}
				}
			} else {
				foreach ($added_participants as $key => $value) {
					//for roles
					self::attachParticaipants($value, $program, $role['id'], $data['loggedInUserId']);
				}
				// update lt
				$lt['participants'] = $added_participants;
			}

			//finally updating
			$lt2 = json_encode($lt);
			$program_id = Program::where('id', $data['program_id'])->update(['learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId']]);

			// email for unapprove
			if (isset($NewUnapproveForEmail)) {
				//
				if (count($NewUnapproveForEmail) > 0) {
					//
					self::sendMailToProgramUsers($NewUnapproveForEmail, 'Participant', $program, 'remove');
				}
			}

			$person = Person::where('customer_party_id', $program['customer_party_id'])->get();
			$users = self::getOrganizationUsers($program['customer_party_id']);

			$program_users = $users->whereIn('id', $lt['participants']);
			$return_data['program_users'] = self::getUserWithEmployeeId($program_users, $person);
			$program_non_users = $users->whereNotIn('id', $lt['participants']);
			$return_data['program_non_users'] = self::getUserWithEmployeeId($program_non_users, $person);
			$return_data['program_appr_users'] = $lt['approvedParticipants'];

			// for publish
			$publish = self::getPublishCondition($lt);

			// for unapprove
			if ($lt['approvedParticipants']) {
				$return_data['unapprove'] = 'yes';
			} else {
				$return_data['unapprove'] = 'no';
			}

			$return_data['publish'] = $publish;

			return $return_data;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $return_data;
	}

//////////////////////////////////////////// Unapprove participants \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public static function unapproveParticipants($data) {

		//it may be unapprove and save
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$return_data = DB::transaction(function () use ($data) {

			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}
			// $data['loggedInUserId'] = '3';

			$role = Role::where('name', 'Participant')->first();
			$program = Program::where('id', $data['program_id'])->first();
			$unapproved_participants = $data['participants'];

			// finding lt
			$lt = json_decode($program['learning_tree'], true);

			if ($unapproved_participants) {

				$real_unapproved = [];
				foreach ($unapproved_participants as $key => $value) {
					//
					if (in_array($value, $lt['approvedParticipants'])) {
						//
						array_push($real_unapproved, $value);
						$status = ProgramUserBadge::where(['program_id' => $data['program_id'], 'user_id' => $value])->update(['approved' => 0, 'updated_by' => $data['loggedInUserId']]);
						//
						self::detachRoleFromProgramUser($value, $role['id'], $program);
					}
				}
				//
				$lt['approvedParticipants'] = array_diff($lt['approvedParticipants'], $real_unapproved);
				$NewUnapproveForEmail = $real_unapproved;
			}

			//
			$lt2 = json_encode($lt);
			$program_id = Program::where('id', $data['program_id'])->update(['learning_tree' => $lt2, 'updated_by' => $data['loggedInUserId']]);

			// mail for unapprove
			if (isset($NewUnapproveForEmail)) {
				//
				if (count($NewUnapproveForEmail) > 0) {
					//
					self::sendMailToProgramUsers($NewUnapproveForEmail, 'Participant', $program, 'remove');
				}
			}

			$person = Person::where('customer_party_id', $program['customer_party_id'])->get();
			$users = self::getOrganizationUsers($program['customer_party_id']);

			$program_users = $users->whereIn('id', $lt['participants']);
			$return_data['program_users'] = self::getUserWithEmployeeId($program_users, $person);
			$program_non_users = $users->whereNotIn('id', $lt['participants']);
			$return_data['program_non_users'] = self::getUserWithEmployeeId($program_non_users, $person);
			$return_data['program_appr_users'] = $lt['approvedParticipants'];

			// for publish
			$publish = self::getPublishCondition($lt);

			// for unapprove
			if ($lt['approvedParticipants']) {
				$return_data['unapprove'] = 'yes';
			} else {
				$return_data['unapprove'] = 'no';
			}

			$return_data['publish'] = $publish;

			return $return_data;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $return_data;
	}

/////////////////// Sending Mail To Individuals \\\\\\\\\\\\\\\\\\\\\\\\
	public static function sendMailToProgramUsers($users, $role, $program, $flag) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		//
		try {
			Log::debug($users);
			Log::debug($flag);
			Log::debug($role);

			if ($program['published'] == 1) {

				$org = Organization::where('party_id', $program['customer_party_id'])->first();
				$customer_org_code = $org['organization_code'];
				$program_code = $program['code'];

				foreach ($users as $key => $id) {
					//
					$data[$role][$id] = self::getData($id, $program['name'], $customer_org_code, $org['id'], $role, $program_code);
				}

				// raise Event
				if ($flag == 'add') {
					//
					event(new CommEvent("ProgramPublishEvent", $data, 'CGX', \Auth::user()->id));
				}

			}
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

//////////////////////////////////// Program Publish \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\=
	public static function publishProgram($data) {

		//it may be unapprove and save
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$return_data = DB::transaction(function () use ($data) {

			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			$program = Program::where('id', $data['program_id'])->first();

			$lt = json_decode($program['learning_tree'], true);

			// create json with learning tree
			$json_status = self::createJson($program, $lt, $data['loggedInUserId']);
			Log::debug($json_status);

			if ($json_status == false) {
				//
				$return_data['publish_status'] = 'no';
				$return_data['topic_name'] = $GLOBALS['topic_to_update'];
				return $return_data;
			}

			$return_data['publish_status'] = 'yes';

			//self::raiseProgramPublishEventForVcSelect($program);
			// publishing program
			if ($program['published'] == 0) {
				//
				$status_id = Program::where('id', $data['program_id'])->update(['published' => 1, 'updated_by' => $data['loggedInUserId']]);

				self::raiseProgramPublishEvent($program['id']);
				self::raiseProgramPublishEventForVcSelect($program);

			} else {
				// dont send mail
			}

			$status = ProgramReminderService::saveTopicRemindConfigOnPublish($data['loggedInUserId'], $data['program_id']);

			return $return_data;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $return_data;
	}

	public static function raiseProgramPublishEventForVcSelect($program) {

		$org = Organization::where('party_id', $program['customer_party_id'])->first();
		$customer_org_code = $org['organization_code'];
		$program_code = $program['code'];

		//$host = $_SERVER['REMOTE_ADDR'];
		$config = Configuration::where('config_key', 'assessment_url')->first();

		$topic_user = TopicUser::where('program_id', $program['id'])->get();
		foreach ($topic_user as $key => $value) {
			//
			$topic = ProgramLtTopic::where('id', $value['topic_id'])->first();
			$data['Participant'][$value['id']] = self::getDataForVcSelectMail($value['user_id'], $program['name'], $customer_org_code, 'Participant', $topic, $config['config_value'], $program_code);

		}

		if (isset($data)) {
			Log::debug($data);
			event(new CommEvent("ProgramPublishChooseVcEvent", $data, 'CGX', \Auth::user()->id));
		}

	}

	public static function getDataForVcSelectMail($id, $program_name, $customer_org_code, $role_name, $topic, $url, $program_code) {
		//
		$user_object = self::getUserObject($id);

		$hashids = new Hashids(Config::get('app.assessment'), 10);
		$url = $url . "/chooseVcByEmail/" . $hashids->encode($topic['id']) . "/" . $hashids->encode($user_object['id']);

		// finding phone number
		$phone = Phone::where('party_id', $user_object['party_id'])->first();

		// creating data
		$data['template_data'] = ['FIRST_NAME' => $user_object['first_name'],
			'LAST_NAME' => $user_object['last_name'],
			'ROLE_NAME' => $role_name,
			'PROGRAM_NAME' => $program_name,
			'TOPIC_NAME' => $topic['name'],
			'START_DATE' => $topic['available_from'],
			'END_DATE' => $topic['available_to'],
			'EMAIL_LINK' => $url,
		];

		$data['program_code'] = $program_code;
		$data['email_id'] = $user_object['email'];
		$data['phone_no'] = $phone['phone_number'];
		$data['customer_organization_code'] = $customer_org_code;
		$data['party_id'] = $user_object['party_id'];

		return $data;
	}

///////////////////////// Communication Module  \\\\\\\\\\\\\\\\\\\\\\\
	public static function raiseProgramPublishEvent($program_id) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		// finding role
		// director
		// coordinator
		// participant
		// program manager
		// company director
		// moderator
		// fascilitator

		$program = Program::where('id', $program_id)->first();
		$program_code = $program['code'];
		$lt = json_decode($program['learning_tree'], true);

		// finding common data
		$org = Organization::where('party_id', $program['customer_party_id'])->first();
		$customer_org_code = $org['organization_code'];

		// program directors
		$program_director_id = $lt['programDirector'];
		$data['Program Director'][$program_director_id] = self::getData($program_director_id, $program['name'], $customer_org_code, $org['id'], 'Program Director', $program_code);

		// program participants
		$participant_ids = $lt['approvedParticipants'];
		foreach ($participant_ids as $key => $id) {
			//
			$data['Participant'][$id] = self::getData($id, $program['name'], $customer_org_code, $org['id'], 'Participant', $program_code);
		}

		// program coordinators
		$coordinator_ids = $lt['programCoordinator'];
		foreach ($coordinator_ids as $key => $id) {
			//
			$data['Program Coordinator'][$id] = self::getData($id, $program['name'], $customer_org_code, $org['id'], 'Program Coordinator', $program_code);
		}

		// program moderators
		$moderator_ids = $lt['moderators'];
		foreach ($moderator_ids as $key => $id) {
			//
			$data['Moderator'][$id] = self::getData($id, $program['name'], $customer_org_code, $org['id'], 'Moderator', $program_code);
		}

		// program facilitator
		$facilitator_ids = $lt['facilitators'];
		foreach ($facilitator_ids as $key => $id) {
			//
			$data['Facilitator'][$id] = self::getData($id, $program['name'], $customer_org_code, $org['id'], 'Facilitator', $program_code);
		}

		// program manager
		$role = Role::all();
		$pm_role = $role->where('name', 'Program Manager')->first();
		$pmId = self::getPersonByRole($program['customer_party_id'], $pm_role['id']);
		$data['Program Manager'][$pmId] = self::getData($pmId, $program['name'], $customer_org_code, $org['id'], 'Program Manager', $program_code);

		// company coordinator
		$cc_role = $role->where('name', 'Company Coordinator')->first();
		$ccId = self::getPersonByRole($program['customer_party_id'], $cc_role['id']);
		$data['Company Coordinator'][$ccId] = self::getData($ccId, $program['name'], $customer_org_code, $org['id'], 'Company Coordinator', $program_code);

		Log::debug($data);

		event(new CommEvent("ProgramPublishEvent", $data, 'CGX', \Auth::user()->id));

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function getData($id, $program_name, $customer_org_code, $org_id, $role_name, $program_code) {
		//
		$user_object = self::getUserObject($id);
		$people = Person::where('party_id', $user_object['party_id'])->first();

		// finding phone number
		$phone = Phone::where('party_id', $user_object['party_id'])->first();

		// abhi - 26/03/19

		$mobNo = $phone['phone_number'];
		$peopleAttribute = PeopleAttribute::where(['people_id' => $people['id'], 'attr_name' => 'dob'])->first();
		$dob = $peopleAttribute['attr_value'];
		$pass = UserService::generatePassword($mobNo, $dob, $org_id, 0);
		// Log::error('$pass');
		// Log::error($pass);

		// creating data
		$data['template_data'] = ['FIRST_NAME' => $user_object['first_name'],
			'ROLE_NAME' => $role_name,
			'PROGRAM_NAME' => $program_name,
			'USER_ID' => $user_object['userid'],
			'PASSWORD' => $pass,
		];

		$data['program_code'] = $program_code;
		$data['email_id'] = $user_object['email'];
		$data['phone_no'] = $phone['phone_number'];
		$data['customer_organization_code'] = $customer_org_code;
		$data['party_id'] = $user_object['party_id'];

		return $data;
	}

	public static function getPersonByRole($party_id, $role_id) {
		// echo $party_id;
		// exit();
		$ret = Role::with(['users.party.person' => function ($query) use ($party_id) {$query->where(['customer_party_id' => $party_id]);}])->where('id', '=', $role_id)->get();
		$persons = array();
		foreach ($ret as $key => $value) {
			foreach ($value['users'] as $k => $v) {
				if (!empty($v->party->person)) {
					$persons = $v->party->user;
				}
			}
		}
		return $persons['id'];
	}

	public static function createJson($program, $lt, $loggedInUserId) {
		// echo $party_id;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		// finding artefact type id for vc
		$artefact = ArtefactType::all();
		$vc_artefact = $artefact->where('name', 'Virtual Classroom')->first();
		$assignment_artefact = $artefact->where('name', 'Assignment')->first();

		$GLOBALS['vc_artefact_type_id'] = $vc_artefact['id'];
		$GLOBALS['assignment_artefact_type_id'] = $assignment_artefact['id'];
		$GLOBALS['moderator_ids'] = [];
		$GLOBALS['facilitator_ids'] = [];

		$top_level_topics = ProgramLtTopic::where(['container_program_id' => $program['id'], 'parent_topic_id' => null])->orderBy('sequence', 'asc')->get();

		$topics = [];
		$false_flag = true;
		foreach ($top_level_topics as $key => $topic_info) {
			//
			$info_array = self::fillTopicData($topic_info);
			if ($info_array == false) {
				$false_flag = false;
				return false;
			}
			//
			$child_json = self::createChildJson($topic_info['id']);
			if (gettype($child_json) == 'boolean') {
				$false_flag = false;
				return false;
			} elseif ($child_json != null) {
				//
				$info_array['topics'] = $child_json;
			}
			array_push($topics, $info_array);
		}
		if ($false_flag == false) {
			return false;
		}

		// finding role
		// facilitator
		// $facilitator_idqs = ProgramLtTopic::where('container_program_id', $program['id'])->get()->pluck('facilitator_id')->toArray();

		// //
		// // $facilitator_idss = array_values(array_unique($facilitator_ids));
		// $facilitator_idss = array_unique($facilitator_ids);
		// $null_index = array_search(null, $facilitator_idss);
		// unset($facilitator_idss[$null_index]);

		$temp_array['programCoordinator'] = $lt['programCoordinator'];
		$temp_array['moderators'] = array_values(array_unique($GLOBALS['moderator_ids']));
		$temp_array['participants'] = $lt['participants'];
		$temp_array['approvedParticipants'] = $lt['approvedParticipants'];
		$temp_array['facilitators'] = array_values(array_unique($GLOBALS['facilitator_ids']));
		$temp_array['learning_tree'] = $lt['learning_tree'];
		$temp_array['details']['programTitle'] = $lt['details']['programTitle'];
		$temp_array['details']['topics'] = $topics;

		unset($lt['details']);
		unset($lt['programCoordinator']);
		unset($lt['participants']);
		unset($lt['approvedParticipants']);
		unset($lt['learning_tree']);

		$learning_tree = array_merge($lt, $temp_array);

		Log::debug($learning_tree);
		$ltt = json_encode($learning_tree);
		Log::debug($ltt);

		// exit();

		$status_id = Program::where('id', $program['id'])->update(['learning_tree' => $ltt, 'updated_by' => $loggedInUserId]);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		return $status_id;

	}

	public static function createChildJson($parent_topic_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$program_lt_topics = ProgramLtTopic::where('parent_topic_id', $parent_topic_id)->orderBy('sequence', 'asc')->get();
		if (count($program_lt_topics)) {
			$topics = [];

			$false_flag = true;
			foreach ($program_lt_topics as $key => $topic_info) {
				//
				$info_array = self::fillTopicData($topic_info);
				if ($info_array == false) {
					$false_flag = false;
					return false;
				}
				//
				$child_json = self::createChildJson($topic_info['id']);
				Log::debug($child_json);
				Log::debug('aaaaaaaaaaa');
				if (gettype($child_json) == 'boolean') {
					$false_flag = false;
					return false;
				} elseif ($child_json != null) {
					//
					$info_array['topics'] = $child_json;
				}

				array_push($topics, $info_array);
			}
			if ($false_flag == false) {
				//
				return false;
			}
			$return_data = $topics;
		} else {
			$return_data = null;
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $return_data;
	}

	public static function fillTopicData($topic_info) {

		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$topic['topic_id'] = $topic_info['id'];
		$topic['name'] = $topic_info['name'];
		$topic['description'] = $topic_info['description'];
		$topic['duration'] = $topic_info['expected_duration'];
		$topic['topics_to_complete'] = $topic_info['topics_to_complete'];

// dkuncommentbegins
		// dk_uncomment_begins
		// checking for date
		//if (!$topic_info['available_from']) {
		//	$GLOBALS['topic_to_update'] = $topic_info['name'];
		//	return false;
		//}

// dkuncommentends
		// dk_uncomment_ends

		$topic['available_from'] = $topic_info['available_from'];
		$topic['available_to'] = $topic_info['available_to'];
		$topic['sequence'] = $topic_info['sequence'];

		$topic['artefact'] = $topic_info['artefact_type_id'];

		// crating artefact
		$topic['notify_before'] = $topic_info['alert_before_nodes'];

		// if ($topic_info['artefact_type_id'] == $GLOBALS['vc_artefact_type_id']) {
		if ($topic_info['details'] != null) {
			//
			$details = json_decode($topic_info['details'], true);

			if ($details['artefact_type_id'] == $GLOBALS['vc_artefact_type_id']) {
				//
				// if ($details['moderators']) {
				// 	foreach ($details['moderators'] as $key => $value) {
				// 		array_push($GLOBALS['moderator_ids'], $value);
				// 	}
				// } else {
				// 	$GLOBALS['topic_to_update'] = $topic_info['name'];
				// 	return false;
				// }

				// // facilitator_ids
				// if ($details['facilitators']) {
				// 	foreach ($details['facilitators'] as $key => $value) {
				// 		array_push($GLOBALS['facilitator_ids'], $value);
				// 	}
				// } else {
				// 	$GLOBALS['topic_to_update'] = $topic_info['name'];
				// 	return false;
				// }

				$arte['artefact_id'] = $details['artefact_type_id'];
				// $arte['artefact_title'] = ;
				// $arte['artefact_url'] = ;
				$arte['entity_name'] = $details['entity_type'];

				// setting facilitator and modrator
				if (isset($topic['moderators'])) {
					//
					$topic['moderators'] = $details['moderators'];
					$topic['facilitators'] = $details['facilitators'];
				} else {
					//	$GLOBALS['topic_to_update'] = $topic_info['name'];
					//	return false;
					$topic['moderators'] = [];
					$topic['facilitators'] = [];
				}

			} elseif ($details['artefact_type_id'] == $GLOBALS['assignment_artefact_type_id']) {
				//
				$arte['artefact_id'] = $details['artefact_type_id'];
				$arte['files'] = $details['files'];
				$arte['entity_name'] = $details['entity_type'];

			} else {
				$arte['artefact_id'] = $details['artefact_type_id'];

				if (isset($details['entity_name'])) {
					//
					$arte['artefact_title'] = $details['entity_name'];
				} else {
					$GLOBALS['topic_to_update'] = $topic_info['name'];
					return false;
				}
				if (isset($details['entity_url'])) {
					$arte['artefact_url'] = str_replace("public", "storage", $details['entity_url']);
				} else {
					$GLOBALS['topic_to_update'] = $topic_info['name'];
					return false;
				}

				$arte['entity_name'] = $details['entity_type'];
				// $arte['artefact_type']=$details[''];
			}
			$topic['artefact'] = $arte;

		} else {
			$topic['artefact'] = [];
		}

		return $topic;
	}
//////////////////////////////////// End Publish  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//////////////////////////////////// Role Functions  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public static function attachParticaipants($value, $program, $role_id, $loggedInUserId) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
		$i = 0;
		if (!$role_user) {
			//
			$i = 1;
			$user = self::getUserObject($value);
			$user->roles()->attach($role_id, ['created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);
		}

		//attaching user badge
		$program->user_badges()->attach($value, ['max_distribution_badges' => $program['participant_distribution_badges'], 'current_distribution_badges' => $program['participant_distribution_badges'], 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function attachApproveParticaipants($value, $program, $role_id, $loggedInUserId) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
		$i = 0;
		if (!$role_user) {
			//
			$i = 1;
			$user = self::getUserObject($value);
			Log::debug($user);
			$user->roles()->attach($role_id, ['created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);
		}

		//attaching user badge
		$program->user_badges()->attach($value, ['max_distribution_badges' => $program['participant_distribution_badges'], 'current_distribution_badges' => $program['participant_distribution_badges'], 'approved' => 1, 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function attachRole($value, $program, $role_id, $loggedInUserId) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$new_role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();

		if (!$new_role_user) {
			//
			$user = self::getUserObject($value);
			Log::debug($user);
			$user->roles()->attach($role_id, ['created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);

			$new_role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
		}

		$program->role_user()->attach($new_role_user['id'], ['user_id' => $value, 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	}

	public static function getOrganizationUsers($company_party_id) {
		//
		$company = Organization::where('party_id', $company_party_id)->first();
		$org_person = Person::where('customer_party_id', $company_party_id)->get();

		$org_users_party_id = $org_person->pluck('party_id');

		$users = User::all();
		$org_user = $users->whereIn('party_id', $org_users_party_id);

		return $org_user;
	}

	// 	public static function attachRole($value, $program, $role_id, $loggedInUserId) {
	// 	//
	// 	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

	// 	$role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
	// 	$i = 0;
	// 	if (!$role_user) {
	// 		//
	// 		$i = 1;
	// 		$user = self::getUserObject($value);
	// 		Log::debug($user);
	// 		$user->roles()->attach($role_id, ['created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);

	// 		$new_role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
	// 	}

	// 	// attach role in program user
	// 	if ($i == 1) {
	// 		//
	// 		$program->role_user()->attach($new_role_user['id'], ['user_id' => $value, 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);
	// 	} else {
	// 		//
	// 		$program_role_user = ProgramUser::where(['program_id' => $program['id'], 'role_user_id' => $role_user['id']])->first();

	// 		if ($program_role_user) {
	// 			//
	// 			$program->role_user()->attach($role_user['id'], ['user_id' => $value, 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);
	// 		}
	// 	}

	// 	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	// }

	// public static function getUsersByCustomer($customer_party_id) {
	// 	//
	// 	$person = Person::where('customer_party_id', $customer_party_id)->get();

	// 	$users = User::all();

	// 	$company_user = [];
	// 	foreach ($person as $key => $person_array) {
	// 		//
	// 		$user = $users->where('party_id', $person_array['party_id'])->first();
	// 		$user['employee_id'] = $person_array['employee_id'];
	// 		array_push($company_user, $user);
	// 	}

	// 	return $company_user;
	// }

	public static function getUserWithEmployeeId($users, $person) {
		//

		$company_user = [];
		foreach ($users as $key => $users_array) {
			//
			$person_array = $person->where('party_id', $users_array['party_id'])->first();

			$users_array['employee_id'] = $person_array['employee_id'];
			array_push($company_user, $users_array);
		}

		return $company_user;
	}
}
