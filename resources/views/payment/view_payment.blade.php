@extends('payment.payment')
@section('title','View payment')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $row_data->id)


@section('title','Edit Payment')
@section('name',$row_data->name)
@section('description',$row_data->description)
@section('address',$row_data->address)

@section('project_id',$row_data->project_id)
@section('expense_date',$row_data->expense_date)
@section('expense_type_id',$row_data->expense_type_id)
@section('organization_id',$row_data->organization_id)
@section('service_name',$row_data->service_name)
@section('bill_no',$row_data->bill_no)
@section('item_desc',$row_data->item_desc)
@section('unit',$row_data->unit)
@section('qty',$row_data->qty)
@section('rate',$row_data->rate)
@section('amount',$row_data->amount)
@section('gst_amount',$row_data->gst_amount)
@section('base_amount',$row_data->base_amount)
@section('amount_paid',$row_data->amount_paid)

@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('.form-control').attr('readonly',true);



});
</script>

@endpush
