<?php

namespace vnnogile\Client\Controllers;

use Illuminate\Http\Request;

//Custom Imports
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Controllers\BaseController;

class CacheController extends BaseController
{

    public function __construct(){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public function flushCacheView(){
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try{
            $records = parent::$masterDataInterface->all('EntityConfiguration');
            $url = route("flush.cache");
        }catch(Exception $e){
            Log::error("Exception : ");
            Log::error($e);
            $records = array();
        }
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    	return view('cache::flush', compact('records', 'url'));
    }

    public function flushCache(Request $request){
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try{
            parent::$masterDataInterface->flushCache($request->cache_tag_name);
        }catch(\Exception $e){
            Log::error("Exception : ");
            Log::error($e);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return False;
        }
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    	return True;
    }    
}
