<?php

namespace vnnogile\CompositeViews\Controllers;
use App\Http\Controllers\Controller;
use App\Services\CompositionMasterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CompositeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$compositionViewData = CompositionMasterService::getCompositionMasterDataFromRoute($request['route_name']);
		// session()->put('route_name', $route_name);
		foreach ($compositionViewData as $key => $controllerName) {
			$controller = app()->make($controllerName->controller_name);
			Log::debug("**********$controllerName************");
			Log::debug($controllerName->controller_name);
			Log::debug("**********isTaggable************");
			Log::debug($request['isTaggable']);
			// print_r($controller->create(true));
			if ($request['isTaggable']) {
				$page = $request['operation'];
				$params = array();
				if ($page == 'create') {
					array_push($params, $request, true);
				} else if ($page == 'edit') {
					array_push($params, $request, $request['id'], true);
				} else if ($page == 'show') {
					array_push($params, $request, $request['id'], true);
				} else {
					array_push($params, $request, true);
				}
				$compositeViewData[] = call_user_func_array(array($controller, $page), $params);
			} else {
				$compositeViewData[] = $controller->index($request, True);
			}

		}
		// Log::debug($compositeViewData[0]);
		// dump($compositeViewData);
		// exit();
		// exit();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return view('vnnogile::composite_view', compact('compositeViewData'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
