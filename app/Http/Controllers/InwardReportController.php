<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\InwardReportService;
use App\Services\ProjectService;
use App\Services\ExpenseTypeService;


class InwardReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function createInwardReport() {

        $project = ProjectService::retrieveProject();

		return view('reports.inward_report' ,compact('project','expense_type'));
	}


public function exportInwardReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$start_date = $request['start_date'];
		$end_date = $request['end_date'];
		$report_type = $request['report_type'];
		$projectCode = InwardReportService::getProjectName($request['project_id']);

		if ($report_type=='Inward') {
$queryBuilder = InwardReportService::retrieveInwardReportQuery($request['project_id'],$start_date,$end_date);			} 
		elseif ($report_type=='Transfer') {
$queryBuilder = InwardReportService::retrieveTransferReportQuery($request['project_id'],$start_date,$end_date);			}
		else {
$queryBuilder = InwardReportService::retrieveConsumptionReportQuery($request['project_id'],$start_date,$end_date);			}
		
	    self::downloadInwardReportExcel($queryBuilder,$projectCode,$start_date,$end_date,$report_type);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadInwardReportExcel($queryBuilder,$projectCode,$start_date,$end_date,$report_type ) {
		$fileName = $report_type.'_Report__From_'.$start_date.'_To_'.$end_date;
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
 
 // dd($row_data);
      $keys = array( "Project Name", "Date", "Material Name", "Quantity", "Price" );
        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array(
        		 $row->project_name, $row->d_date, $row->material_name, $row->quantity, $row->price);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}


}
