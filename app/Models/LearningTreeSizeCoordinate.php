<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Apr 2018 18:24:37 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class LearningTreeSizeCoordinate
 * 
 * @property int $id
 * @property int $node_count
 * @property int $left_circle_x
 * @property int $left_circle_y
 * @property int $pencil_x
 * @property int $pencil_y
 * @property int $icon_circle_x_offset
 * @property int $icon_circle_y_offset
 * @property int $offset_y
 * @property int $right_circle_x
 * @property int $right_circle_y
 * @property int $circle_radius
 * @property int $circle_stroke_width
 * @property int $pencil_width
 * @property int $pencil_height
 * @property int $left_text_x
 * @property int $left_text_y
 * @property int $right_text_x
 * @property int $right_text_y
 * @property int $icon_image_width
 * @property int $icon_image_height
 * @property int $zoom_controls_offset
 * @property int $program_screen_offset
 * @property int $program_title_height
 * @property int $program_title_offset
 * @property int $node_font_size
 * @property string $node_font_family
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class LearningTreeSizeCoordinate extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'node_count' => 'int',
		'left_circle_x' => 'int',
		'left_circle_y' => 'int',
		'pencil_x' => 'int',
		'pencil_y' => 'int',
		'icon_circle_x_offset' => 'int',
		'icon_circle_y_offset' => 'int',
		'offset_y' => 'int',
		'right_circle_x' => 'int',
		'right_circle_y' => 'int',
		'circle_radius' => 'int',
		'circle_stroke_width' => 'int',
		'pencil_width' => 'int',
		'pencil_height' => 'int',
		'left_text_x' => 'int',
		'left_text_y' => 'int',
		'right_text_x' => 'int',
		'right_text_y' => 'int',
		'icon_image_width' => 'int',
		'icon_image_height' => 'int',
		'zoom_controls_size' => 'int',
		'zoom_controls_offset' => 'int',
		'program_screen_offset' => 'int',
		'program_title_height' => 'int',
		'program_title_offset' => 'int',
		'node_font_size' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'node_count',
		'left_circle_x',
		'left_circle_y',
		'pencil_x',
		'pencil_y',
		'icon_circle_x_offset',
		'icon_circle_y_offset',
		'offset_y',
		'right_circle_x',
		'right_circle_y',
		'circle_radius',
		'circle_stroke_width',
		'pencil_width',
		'pencil_height',
		'left_text_x',
		'left_text_y',
		'right_text_x',
		'right_text_y',
		'icon_image_width',
		'icon_image_height',
		'zoom_controls_size',
		'zoom_controls_offset',
		'program_screen_offset',
		'program_title_height',
		'program_title_offset',
		'node_font_size',
		'node_font_family',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}
}
