@extends('company.company')
@section('title','View Company')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $orgData->id)


@section('title','View Company')
@section('organization_name',$orgData->name)
@section('organization_party_id',$orgData->party_id)
@section('old_org_logo',$orgData->logo)
@section('old_org_logo_name',$orgData->original_logo)
@section('tag_line',$orgData->tag_line)


@section('organization_url',$orgData->website_url)
@section('organization_code',$orgData->organization_code)

@section('organization_address1',$orgData->orgAddress->address1)
@section('organization_address2',$orgData->orgAddress->address2)
@section('organization_address3',$orgData->orgAddress->address3)
@section('organization_country',$orgData->orgAddress->country_id)
@section('organization_state',$orgData->orgAddress->state_id)
@section('organization_city',$orgData->orgAddress->city_id)
@section('organization_pincode',$orgData->orgAddress->pincode_id)
@section('email',$orgData->email)

@section('organization_contact',trim($orgData['contact']))



@push('scripts')
<style>

</style>

<script>

$(document).ready(function(){
$('#save_link').hide();
  $("input").attr("readonly", true);
  $('.form-control').attr('readonly',true);
// abhi 26/03/19
// comm_option
@if ($orgData['comm_option'] == 'email') {
     $("[name=comm_option]").val(["email"]);
}
@elseif ($orgData['comm_option'] == 'sms') {
     $("[name=comm_option]").val(["sms"]);
}
@elseif ($orgData['comm_option'] == 'both') {
     $("[name=comm_option]").val(["both"]);
}
@endif
   $("#file1").removeClass("mandatory");
   $('#file2').removeClass("mandatory");
   $('#file3').removeClass("mandatory");
   $('#logo_div').removeClass('required');
  


});
</script>

@endpush

