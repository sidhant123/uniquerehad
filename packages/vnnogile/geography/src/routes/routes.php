<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::post('/fileUpload','vnnogile\Dms\Controllers\TestController@uploadDoc');

Route::group(['middleware' => ["web", "auth", "CheckRole:Tenant Administrator"]], function () {

	Route::resource('states','vnnogile\Geography\Controllers\StateController');
	Route::resource('cities','vnnogile\Geography\Controllers\CityController');
	Route::resource('pincodes','vnnogile\Geography\Controllers\PincodeController');
});


