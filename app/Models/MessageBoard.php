<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:43:03 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;


//Custom Imports
use App\Models\BaseModel;

/**
 * Class MessageBoard
 * 
 * @property int $id
 * @property string $message_subject
 * @property string $message_body
 * @property string $message_for
 * @property int $tenant_party_id
 * @property string $customer_party_ids
 * @property string $program_ids
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property int $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class MessageBoard extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'tenant_party_id' => 'int',
		'is_active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];

	protected $fillable = [
		'message_subject',
		'message_body',
		'message_for',
		'tenant_party_id',
		'customer_party_ids',
		'program_ids',
		'start_date',
		'end_date',
		'is_active',
		'created_by',
		'updated_by',
		'deleted_by'
	];
	public function getStartDateAttribute() {
    return date('d-m-Y', strtotime($this->attributes['start_date']));
  }
  	public function getEndDateAttribute() {
    return date('d-m-Y', strtotime($this->attributes['end_date']));
  }
}
