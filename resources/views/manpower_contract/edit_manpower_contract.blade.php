@extends('manpower_contract.manpower_contract')
@section('editID', '/'.$row_data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('edit_btn','hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Manpower Contract')

@section('organization_id',$row_data->organization_id)
@section('project_id',$row_data->project_id)
@section('site_id',$row_data->site_id)
@section('service_name',$row_data->service_name)
@section('rate',$row_data->rate)


@push('scripts')
<script>
$(document).ready(function(){
@if ($row_data['status'] == 'Active') {
     $("[name=status]").val(["Active"]);
}
@else{
     $("[name=status]").val(["Inactive"]);
}
@endif
})
</script>
@endpush