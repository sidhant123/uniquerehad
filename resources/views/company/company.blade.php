@extends('layouts.base')
@section('content')

@include('errors.error_validation')
 <form action="/companies{{null}}@yield('editID')" style="border-style:dotted; border-width: 1px;" method="post" onsubmit="return validateMandatoryFieldsForCompany()" enctype="multipart/form-data">
  {{ csrf_field() }}

  @section('editMethod')
  @show
  @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/companies'])
  <div class="row">
      <div class="col-lg-11"><h3 class="headings">@yield('title')</h3></div>
    </div>

    <div class="row">
      <div class="col-lg-1"><h3></h3></div>
      @isset($orgData->logo)
      <div class="col-lg-11 @yield('logo')" align="right"><img src="/storage{{str_replace('public/','',$orgData->logo)}}" alt="Logo not found" width="100" height="100"  style="border:groove 1px;"></div>
      @endisset
    </div>
    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="org_name">{{ __('labels.company.organization_name') }}</label>
           @if(!empty(old('organization_name')))
            <input type="text" class="form-control mandatory" style="text-transform: capitalize;" id="org_name" name="organization_name" value="{{ old('organization_name') }}" @yield('organization_name_d')>
      @else
      <input type="text" class="form-control mandatory" style="text-transform: capitalize;" id="org_name" name="organization_name" value="@yield('organization_name')" @yield('organization_name_d')>
      @endif
           <input type="hidden" name="organization_party_id" value="@yield('organization_party_id')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="country_id"> {{ __('labels.company.organization_country') }}</label>
           <select id="country_id" class="form-control mandatory" name="organization_country" value="@yield('organization_country')"></select>
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="org_addr1">{{ __('labels.company.organization_address1') }}</label>
           @if(!empty(old('organization_address1')))
           <input type="text" id="org_addr1" maxlength="100" class="form-control mandatory" name="organization_address1" value="{{ old('organization_address1') }}">
           @else
           <input type="text" id="org_addr1" maxlength="100" class="form-control mandatory" name="organization_address1" value="@yield('organization_address1')">
           @endif
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="state_id">{{ __('labels.company.organization_state') }}</label>
           <select id="state_id" class="form-control mandatory" name="organization_state" value="@yield('organization_state')"></select>
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="org_addr2">{{ __('labels.company.organization_address2') }}</label>
           @if(!empty(old('organization_address2')))
           <input type="text" id="org_addr2" maxlength="100" class="form-control mandatory" name="organization_address2" value="{{ old('organization_address2') }}">
           @else
           <input type="text" id="org_addr2" maxlength="100" class="form-control mandatory" name="organization_address2" value="@yield('organization_address2')">
           @endif
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="city_id">{{ __('labels.company.organization_city') }}</label>
           <select id="city_id" class="form-control mandatory" name="organization_city" value="@yield('organization_city')"></select>
           <!-- <input type="text" class="form-control" name="organization_city" value="@yield('organization_city')" > -->
         </div>
     </div>
  </div>

   <div class="row">
     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_addr3">{{ __('labels.company.organization_address3') }}</label>
          @if(!empty(old('organization_address3')))
          <input type="text" id="org_addr3" maxlength="100" class="form-control mandatory" name="organization_address3" value="{{ old('organization_address3') }}">
          @else
          <input type="text" id="org_addr3" maxlength="100" class="form-control mandatory" name="organization_address3" value="@yield('organization_address3')">
          @endif
        </div>
    </div>

     <div class="col-lg-6">
      <div class="form-group required">
          <label for="pincode_id">{{ __('labels.company.organization_pincode') }}</label>
          <select id="pincode_id" class="form-control mandatory" name="organization_pincode" value="@yield('organization_pincode')"></select>
          <!--  <input type="text" class="form-control" name="organization_pincode" value="@yield('organization_pincode')"> -->
        </div>
    </div>
 </div>

   <div class="row">
     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_contact">{{ __('labels.company.organization_contact') }}</label>
          @if(!empty(old('organization_contact')))
          <input type="number" minlength="10" maxlength="10" class="form-control mandatory" onKeyPress="if(this.value.length>9) return false;" id="org_contact" name="organization_contact" value="{{ old('organization_contact') }}" pattern="[1-9]{1}[0-9]{9}">
          @else
          <input type="number" class="form-control mandatory" onKeyPress="if(this.value.length>9) return false;" pattern="[1-9]{1}[0-9]{9}" id="org_contact" name="organization_contact" value="@yield('organization_contact')">
          @endif
        </div>
    </div>

     <div class="col-lg-6">
      <div class="form-group">
          <label for="org_url">{{ __('labels.company.organization_url') }}</label>
          @if(!empty(old('organization_url')))
          <input type="text" class="form-control" id="org_url" name="organization_url" value="{{ old('organization_url') }}">
          @else
          <input type="text" class="form-control" id="org_url" name="organization_url" value="@yield('organization_url')">
          @endif
        </div>
    </div>
 </div>

  <!-- abhi 26/03/19 -->
 <div class="row">
     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_code">{{ __('labels.company.organization_code') }}</label>
          @if(!empty(old('organization_code')))
          <input type="text" style="text-transform: uppercase;"  class="form-control mandatory" id="org_code" name="organization_code" value="{{ old('organization_code') }}">
          @else
          <input type="text" style="text-transform: uppercase;"  class="form-control mandatory" id="org_code" name="organization_code" value="@yield('organization_code')">
          @endif
        </div>
    </div>

     <div class="col-lg-6">
      <div class="form-group required">
          <label for="email">{{ __('labels.company.email') }}</label>
          @if(!empty(old('email')))
          <input type="email" class="form-control mandatory" id="email_id" name="email" value="{{ old('email') }}">
          @else
          <input type="email" class="form-control mandatory" id="email_id" name="email" value="@yield('email')">
          @endif
        </div>
    </div>
   
 </div>

 <div class="row">
    <div class="col-lg-6 " >
      <div class="form-group" id="logo_div">
          <label for="file1">{{ __('labels.company.organization_logo') }}</label>
          <input type="file" class="form-control" id="file1" accept=".png,.jpeg,.jpg" onchange="return fileValidation(this.id);" name="organization_logo" value="@yield('organization_logo')" >
        </div>
    </div>
     <div class="col-lg-6">
      <div class="form-group">
          <label for="pass_logic">Password Logic</label>
          <select id="pass_logic" class="form-control" name="pass_logic" value="@yield('pass_logic')"></select>
        </div>
    </div>
   
 </div>

  <!-- abhi -->
 {{-- <div class="row">
     <div class="col-lg-12">
      <div class="form-group required" >
          <label for="tag_line">Tag Line</label>
          @if(!empty(old('tag_line')))
          <textarea class="form-control mandatory" id="tag_line" name="tag_line">{{ old('tag_line') }}</textarea>
          @else
          <textarea class="form-control mandatory" id="tag_line" name="tag_line">@yield('tag_line')</textarea>
          @endif
        </div>
    </div>
 </div> --}}


</div>

<p></p>
<div class="row col-lg-12" align="center">
  <button type="submit" id="save_btn" class="btn btn-primary hide" >{{ __('labels.company.submit') }}</button>
</div>
</form>
@endsection

@section('css')
<style>
  .disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}

</style>
@endsection

@push('scripts')
<link href="/vendor/vnnogile/css/summernote.css" rel="stylesheet">
<script src="/vendor/vnnogile/js/summernote.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
     $(".mandatory").removeClass('fillFields');

   $('#pm_user_first_name_id').select2({
    tags: true
  });
   $('#cc_first_name_id').select2({
    tags: true
  });
    $('#country_id,#state_id,#city_id,#pincode_id').select2({});
$('#country_id').on('select2:select', function (e) {
     var data = e.params.data;
     if(data.id > 0){
            getDataThroughAxios('/statesForFilter/'+data.id,'state_id');
        }
});
$('#state_id').on('select2:select', function (e) {
     var data = e.params.data;
     if(data.id > 0){
            getDataThroughAxios('/citiesForFilter/'+data.id,'city_id');
        }
});
$('#city_id').on('select2:select', function (e) {
     var data = e.params.data;
     if(data.id > 0){
            getDataThroughAxios('/pincodesForFilter/'+data.id,'pincode_id');
        }
});

// abhi
 $('#tag_line').summernote({

      toolbar: [
       ['font', ['bold', 'underline', 'clear']],
       ['fontsize', ['fontsize']],
       ['fontname', ['fontname']],
       ['color', ['color']],

       // ['para', ['ul', 'ol', 'paragraph', 'height', 'style']],
       // ['table', ['table']],
      // ['insert', ['link', 'picture'
        //, 'video'
        // ]],
        // ['view', ['fullscreen', 'codeview', 'help']]
        ],
 });
// front end validation of tag_line

 });
  $(document).on('change','#pm_user_first_name_id',function(){
    var user_id = $(this).val();
    if(user_id > 0){
      $.getJSON('/getUserDataByID/'+ user_id, new Date(), populateUserDataForPM).error(errorResponse);
    }else{
      clearFieldsForPM();
    }
  });
  $(document).on('change','#cc_first_name_id',function(){
    var user_id = $(this).val();
    if(user_id > 0){
      $.getJSON('/getUserDataByID/'+ user_id, new Date(), populateUserDataForCC).error(errorResponse);
    }else{
      clearFieldsForCC();
    }
  });
  $('#pm_user_first_name_id').empty();
  $('#cc_first_name_id').empty();
  $('#pm_user_first_name_id').append('<option value="0">--select--</option>');
  $('#cc_first_name_id').append('<option value="0">--select--</option>');

  @if(isset($people))
  @foreach($people as $user)
  @if(isset($pmData))
  @if($user->party_id === $pmData->party_id)
  $('#pm_user_first_name_id').append('<option value="{{$user->id}}" selected>{{$user->first_name}}</option>');
  @else
  $('#pm_user_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @else
  $('#pm_user_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @if(isset($ccData))
  @if($user->party_id === $ccData->party_id)
  $('#cc_first_name_id').append('<option value="{{$user->id}}" selected>{{$user->first_name}}</option>');
  @else
  $('#cc_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @else
  $('#cc_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @endforeach
  @endif
  function errorResponse(error){
    console.log(error);
  }


  @foreach($geoData as $key => $data)
  $('#{{$key}}').append('<option value="0" selected>---select---</option>');
  @foreach($geoData[$key] as $value)
  @if(isset($orgData['orgAddress'][$key]))
  @if($orgData['orgAddress'][$key] == $value['id'])
  $('#{{$key}}').append('<option value="{{$value["id"]}}" selected>{{$value["name"]}}</option>');
  @else
  $('#{{$key}}').append('<option value="{{$value["id"]}}" >{{$value["name"]}}</option>');
  @endif
  @else
  $('#{{$key}}').append('<option value="{{$value["id"]}}" >{{$value["name"]}}</option>');
  @endif
  @endforeach

  @endforeach

 function validateMandatoryFieldsForCompany(){

  var valid = true;
  valid=validateMandatoryFields();
 
  console.log("valida --- " + valid);
 

  var country_id = $('#country_id').val();
    // alert(country_id);
  if(country_id == 0) {
document.getElementById("select2-country_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-country_id-container").style.border = "0px solid red";

}

    var state_id = $('#state_id').val();
    // alert(state_id);
  if(state_id == 0) {
document.getElementById("select2-state_id-container").style.border = "1px solid red";
}else {
  document.getElementById("select2-state_id-container").style.border = "0px solid red";

}

    var city_id = $('#city_id').val();
        // alert(city_id);
  if(city_id == 0) {
document.getElementById("select2-city_id-container").style.border = "1px solid red";
}else {
  document.getElementById("select2-city_id-container").style.border = "0px solid red";

}

    var pincode_id = $('#pincode_id').val();
            // alert(pincode_id);
  if(pincode_id == 0) {
document.getElementById("select2-pincode_id-container").style.border = "1px solid red";
}else {
  document.getElementById("select2-pincode_id-container").style.border = "0px solid red";

}
if(valid == false){
return false;
}


  if(valid){
     var org_code = $('#org_code').val();
  // alert(org_code);
  var isnum = /^\d+$/.test(org_code);
  if(isnum) {
 alert('Company code can not be number');
 return false;
  }else {
    valid = true;
  }


  var phone = $('#org_contact').val();
  var contactRes = validatecontact(phone);
         if(!contactRes){
      return false;
     }
 
  return valid;
    }
    return valid;
}

function scrollToElement(selector){
  console.log('in scrollToElement')
  var scrollPos =  $(selector).offset().top;
  $(  'html, body').animate({scrollTop:(Number(scrollPos) - 72)}, 'slow');
}

// abhi - 26/03/19
$('#pass_logic').empty();

@if(isset($orgData['password_logics']))
  @foreach($orgData['password_logics'] as $pass)

  @if(isset($orgData['password_logic_id']))
    @if($pass['id'] == $orgData['password_logic_id'])
    $('#pass_logic').append('<option value="{{$pass['id']}}" selected>{{$pass['password_logic']}}</option>');
    @else
    $('#pass_logic').append('<option value="{{$pass['id']}}">{{$pass['password_logic']}}</option>');
    @endif
  @else
    @if($pass['id'] == 1)
    $('#pass_logic').append('<option value="{{$pass['id']}}" selected>{{$pass['password_logic']}}</option>');
    @else
    $('#pass_logic').append('<option value="{{$pass['id']}}">{{$pass['password_logic']}}</option>');
    @endif
  @endif

  @endforeach
@endif

function isNumeric(event,dob) { // Numeric only

    // var dob =$(obj).val().trim();
    var dob =dob.trim();
    if (dob.length == 2 || dob.length == 5) {
      if (event.which != 47) {
          return false;
      }
      else{
        return true;
      }
    }else if (dob.length == 10) {
        return false;
    }

    var k;
    document.all ? k = event.keycode : k = event.which;
    return((k > 47 && k < 58) || k === 0 || k === 8 || k === 118 || k === 86 || k === 17);
}

document.querySelector("#org_name").addEventListener("keypress", function (evt) {
    if (evt.which > 32 && evt.which < 65)
    {
        evt.preventDefault();
    }
});


document.querySelector("#org_contact").addEventListener("keypress", function (evt) {
    if (evt.which > 47 && evt.which < 58)
    {
      return true;
    } else {
        evt.preventDefault();
    }
});

function validatecontact(phone) {


  if(phone.length > 9 && phone.length < 11)
   {
    return true;
    }
  else {

  alert("Please enter a valid contact number");
  return false;
    }
  }


</script>
@endpush

