<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 19 Mar 2019 16:24:30 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganizationAttribute
 *
 * @property int $id
 * @property int $organization_id
 * @property string $attr_name
 * @property string $attr_value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @property \App\Models\Organization $organization
 *
 * @package App\Models
 */
class OrganizationAttribute extends Eloquent {
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'organization_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
	];

	protected $fillable = [
		'organization_id',
		'attr_name',
		'attr_value',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function organization() {
		return $this->belongsTo(\App\Models\Organization::class);
	}
}
