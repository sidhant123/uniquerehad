<?php

namespace vnnogile\Mailer\Utility;

//Custom Imports Below
use vnnogile\Mailer\Jobs\EmailJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * This is a a common utility method for email
 *
 * All file upload controllers can use this trait to 
 * upload files to the server
 */
class EmailUtility
{
    /**
     * Send email with or without attachments
     * @param Array $emailData
     * @return Array $status
     */
    public static function sendEmail($emailData, $masterDataInterface, $utilsInterface){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $queueName=$utilsInterface->getConfigValue($masterDataInterface, "email_queue");;
        if (isset($queueName)){
            EmailJob::dispatch($emailData)->onQueue($queueName);    
        }else{
            EmailJob::dispatch($emailData)->onQueue('emails');    
        }
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return array('success' => True);
    }

    public static function sendCommunicationEmail($role_data){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        foreach ($role_data as $id => $value) {

            $emailData['template_name']=$value['template'];
            $emailData['data']=$value['template_data'];
            $emailData['subject']=$value['template_subject'];
            $emailData['to']=$value['email_id'];

            // setting cc email_id
            if(isset($value['template_cc'])){
                $emailData['cc']=$value['template_cc'];
            }
            if(isset($value['attachments'])){
                $emailData['attachments']=$value['attachments'];
            }

            //trail data
            $emailData['trail_data']['login_id']=$value['login_id'];
            if (array_key_exists('program_code', $value)){
                $emailData['trail_data']['program_code']=$value['program_code'];    
            }
            if (array_key_exists('customer_organization_code', $value)){
                $emailData['trail_data']['customer_organization_code']=$value['customer_organization_code'];
            }
            if (array_key_exists('job_id', $value)){
                $emailData['trail_data']['job_id']=$value['job_id'];
            }
            if (array_key_exists('party_id', $value)){
                $emailData['trail_data']['party_id']=$value['party_id'];
            }

            $emailData['trail_data']['communication_event_id']=$value['communication_event_id'];
            $emailData['trail_data']['tenant_code']=$value['tenant_code'];

            EmailJob::dispatch($emailData)->onQueue('emails');  
            Log::debug("email sent in job for".$id);  
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return "success";

    }   
}
