<?php

namespace vnnogile\Client\Subscribers;

//Custom Imports Below
use App\User;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Log;

use vnnogile\client\interfaces\MasterDataInterface;
use vnnogile\Client\Db\MasterDataDbService;

class UserEventSubscriber 
{
    protected $masterDataInterface;

    public function __construct(MasterDataInterface $masterDataInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
    }
    /**
        * Handle user login events.
        */
    public function onUserLogin($event) {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");        
        $container = new Container();
        $container->singleton('vnnogile\Client\Models\UserPrincipal');
        $userPrincipal = $container->make('vnnogile\Client\Models\UserPrincipal');
        $userPrincipal->setUserId($event->user->id) ;

        $userPrincipal->setUserPartyId($event->user->party_id) ;
        $userPrincipal->setFirstName($event->user->first_name) ;
        $userPrincipal->setLastName($event->user->last_name) ;
        $userPrincipal->setEmail($event->user->email) ;
        
        //To get relations we need the model instance
        $user  = User::find($event->user->id);

        if ($user->party != null){
            $contacts = $user->party->contacts;
            $phones = array();
            Log::debug('Count ' . count($contacts));

            foreach ($contacts as $contact) {
                array_push($phones, $contact['phone_number']);
            }
            $userPrincipal->setPhones($phones) ;            

            $tenantOrg = $user->party->person->tenant_party->organization;
            // dd($tenantOrg);
            $userPrincipal->setTenantPartyId($tenantOrg->party_id) ;
            $userPrincipal->setTenantCode($tenantOrg->organization_code) ;
            $userPrincipal->setTenantName($tenantOrg->name) ;

            if (is_null($event->user->party->person->customer_party)){
                Log::debug("No customer associated with authenticated user");
            }else{
                $customerOrg =  $event->user->party->person->customer_party->organization;
                $userPrincipal->setCustomerPartyId($customerOrg->party_id) ;
                $userPrincipal->setCustomerCode($customerOrg->organization_code) ;
                $userPrincipal->setCustomerName($customerOrg->name) ;            
            }
        }


        $roles = $event->user->roles;
        $userRoles = array();
        if ($roles != null){
            foreach ($roles as $role) {
                $userRoles[$role->id] = $role->name; 
            }
        }
        $userPrincipal->setRoles($userRoles);

        session(['userPrincipal' => $userPrincipal]);

        Self::setUpOtherMasters();
        
        Log::debug("User Login occurred");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    private function setUpOtherMasters(){

        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $masterDataInterface = $this->masterDataInterface;
        $communicationChannels = $masterDataInterface->list('CommunicationChannel');
        foreach ($communicationChannels as $communicationChannel) {
            $communicationChannelsMap[$communicationChannel->id] = $communicationChannel->name;
        }
        
        $roles = $masterDataInterface->list('Role');
        foreach ($roles as $role) {
            $rolesMap[$role->id] = $role->name;
        }
        
        $partyTypes = $masterDataInterface->list('PartyType');
        foreach ($partyTypes as $partyType) {
            $partyTypesMap[$partyType->id] = $partyType->name;
        }

        $phoneTypes = $masterDataInterface->list('PhoneType');
        foreach ($phoneTypes as $phoneType) {
            $phoneTypesMap[$phoneType->id] = $phoneType->name;
        }        

        $configurations = $masterDataInterface->all('Configuration');
        foreach ($configurations as $configuration) {
            $configurationsMap[$configuration->configKey] = $configuration->configValue;
        }

        //session(['communicationChannels' => $communicationChannelsMap]);
        //session(['roles' => $rolesMap]);
        //session(['partyTypes' => $partyTypesMap]);
        //session(['phoneTypes' => $phoneTypesMap]);
        //session(['configurations' => $configurationsMap]);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
    * Handle user logout events.
    */
    public function onUserLogout($event) {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");        

        if (session()->has('userPrincipal')) {
            $userPrincipal = session('userPrincipal');
            
            $userPrincipal->setUserId(0) ;
            $userPrincipal->setUserPartyId(0) ;
            $userPrincipal->setFirstName(null) ;
            $userPrincipal->setLastName(null) ;
            $userPrincipal->setEmail(null) ;
            
            $userPrincipal->setPhones(null) ;
            $userPrincipal->setRoles(null) ;
            
            $userPrincipal->setTenantPartyId(0) ;
            $userPrincipal->setTenantCode(null) ;
            $userPrincipal->setTenantName(null) ;

            $userPrincipal->setCustomerPartyId(0) ;
            $userPrincipal->setCustomerCode(null) ;
            $userPrincipal->setCustomerName(null) ;
            session()->forget('userPrincipal');
            session()->forget('communicationChannels');
            session()->forget('roles');
            session()->forget('partyTypes');
            session()->forget('phoneTypes');
            session()->forget('configurations');
        }
        Log::debug("User Logout occurred");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");        
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'vnnogile\Client\Subscribers\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'vnnogile\Client\Subscribers\UserEventSubscriber@onUserLogout'
        );
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");        
    }
}
