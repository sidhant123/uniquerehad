@extends('vnnogile::state_base')
@section('title', 'Edit State')
@section('editMethod')
{{method_field('PUT')}}
@endsection
@section('editID', '/'.$data->id)
@section('state_name', $data->name)
@section('is_mandatory', 'mandatory')
@section('is_update', 'Y')
