@extends('expense_type.expense_type')
@section('title','View Expense Type')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $row_data->id)


@section('name',$row_data->name)
@section('description',$row_data->description)


@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('.form-control').attr('readonly',true);
});

 @if($row_data['track_payment']=='Yes'){
     $('#track_payment').attr('checked',true);
  }
 @endif

</script>

@endpush
