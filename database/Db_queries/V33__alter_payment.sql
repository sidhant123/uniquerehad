

  ALTER TABLE payments ALTER COLUMN amount_received TYPE NUMERIC(15,2);
  ALTER TABLE payments ALTER COLUMN invoice_amount TYPE NUMERIC(15,2);
  ALTER TABLE payments ALTER COLUMN total_amount TYPE NUMERIC(15,2);
  ALTER TABLE payments ALTER COLUMN gst_amount TYPE NUMERIC(10,2);


    ALTER TABLE expenses ALTER COLUMN amount TYPE NUMERIC(15,2);
    ALTER TABLE expenses ALTER COLUMN amount_paid TYPE NUMERIC(15,2);
	
	-- 13 june bug fixing
	
    ALTER TABLE expenses ALTER COLUMN rate TYPE NUMERIC(10,2);

