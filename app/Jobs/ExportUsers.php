<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Custom Imports
use DB;
use Excel;
use Illuminate\Support\Facades\Log;

class ExportUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $tenantPartyId;
    public $tries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tenantPartyId)
    {
        $this->tenantPartyId = $tenantPartyId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info(__CLASS__ . "::" . __METHOD__ . " started");
        Excel::create('Users', function($excel)  {

            $excel->sheet('Users', function($sheet) use($excel){
                  // Set the title
                  $excel->setTitle('Cgx Application - Users Dump')
                        ->setCreator('Cgx')
                        ->setCompany('Cognegix Digital Learning')
                        ->setDescription('Users Dump from CGX Application');

                  $documentCount = DB::table('documents')->count();
                  //Form the query
                  $query = 'select a.first_name, a.last_name, a.email, a.personal_email, c.name, c.organization_code, b.userid, a.gender, a.employee_id, a.department, a.designation, a.linkedin_profile, a.facebook_id, a.twitter_handle, a.google_profile
from people a, users b, organizations c
where a.party_id = b.party_id 
and a.customer_party_id = c.party_id
and a.tenant_party_id = ' . $this->tenantPartyId . 
' order by c.name';
                  $query = '(' . $query . ') somealias';

                  //Form the header and print the header
                  $keys = array("Organization", "Organization Code", "First Name", "Last Name", "Email", "Personal Email", "User Id", "Gender", "Employee Id" ,"Department", "designation", "LinkedIn Profile", "Facebook Id", "Twitter Handle", "Google Profile");
                  $sheet->appendRow($keys);
                  $sheet->setAutoFilter();
                  $sheet->row(1, function($row) {
                      // call cell manipulation methods
                      $row->setBackground('#000000');
                      $row->setFontFamily('Trebuchet MS');
                      $row->setFontColor('#ffffff');
                  });
                  
                  // Fire query and the sheet manipulation
                  DB::table(DB::raw($query))->orderBy('name')->chunk(50, function($rows) use($documentCount, $sheet){
                      foreach ($rows as $row) {
                          $rowArray = array($row->name, $row->organization_code, $row->first_name, $row->last_name, $row->email, $row->personal_email, $row->userid, $row->gender, $row->employee_id, $row->department, $row->designation, $row->linkedin_profile, $row->facebook_id, $row->twitter_handle, $row->google_profile);
                          $sheet->appendRow($rowArray);                                
                      }
              }); 
            });

        })->store('xlsx');
        Log::info(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
