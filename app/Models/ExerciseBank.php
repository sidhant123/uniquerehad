<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 15:08:16 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
/**
 * Class ExerciseBank
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $exercise_types_id
 * @property array $questions
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\ExerciseType $exercise_type
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class ExerciseBank extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'exercise_types_id' => 'int',
		// 'questions' => 'json',
		'tenant_party_id' => 'int',
		'version' => 'int',
		'usage_count' => 'int',
		'customer_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'exercise_types_id',
		'questions_per_page',
		'participant_instructions',
		'rater_instructions',
		'version',
		'usage_count',
		'questions',
		'tenant_party_id',
		'customer_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function exercise_type()
	{
		return $this->belongsTo(\App\Models\ExerciseType::class, 'exercise_types_id');
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'customer_party_id');
	}
	public function exercise_bank_pages(){
		return $this->hasMany(\App\Models\ExerciseBankPage::class, 'exercise_banks_id');
	}
}
