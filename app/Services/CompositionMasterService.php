<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use App\Models\CompositionMaster;
use Illuminate\Support\Facades\Log;
/**
 * Class CompositionMaster
 * 
 */
class CompositionMasterService
{
	public static function getCompositionMasterDataFromRoute($routeName){

		
		return CompositionMaster::select('route_name','controller_name','sequence')->where('route_name',$routeName)->orderBy('sequence', 'asc')->get();
		 	}
}
