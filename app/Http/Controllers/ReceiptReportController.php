<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\ReceiptReportService;
use App\Services\ProjectService;
use App\Services\ExpenseTypeService;


class ReceiptReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function createReceiptReport() {

        $project = ProjectService::retrieveProject();

		return view('reports.receipt_report' ,compact('project','expense_type'));
	}


public function exportReceiptReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$start_date = $request['start_date'];
		$end_date = $request['end_date'];
		$projectCode = ReceiptReportService::getProjectName($request['project_id']);
		$queryBuilder = ReceiptReportService::retrieveReceiptReportDetailQuery($request['project_id'],$start_date,$end_date);
	    self::downloadDetailReportExcel($queryBuilder,$projectCode,$start_date,$end_date);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadDetailReportExcel($queryBuilder,$projectCode,$start_date,$end_date ) {
		$fileName = $projectCode . '_Receipt_Report_From_'.$start_date.'_To_'.$end_date;
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
// dd($row_data);

      $keys = array( "Project Name", "Payment No", "Boq No", "R A Bill No", "Invoice Date", 
      "Base Amount", "GST" , "Total Invoice Amount" , 'Retension', 'Mobilization'
      );
        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array(
        		 $row->project_name, $row->payment_no, $row->boq_no, $row->invoice_no, $row->invoice_date, 
        		 $row->base_amount, $row->gst_amount, $row->invoice_amount, $row->retension, $row->mobilization);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}


}
