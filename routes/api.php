<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('user/authenticate', 'API\UserController@login');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('/demoFunction/', 'API\UserController@loginFo');


Route::get('/generateEncodedPassword', 'API\UserController@generateEncodedPassword');


//Dpr modules route start here


// to get maste boq data
Route::get('/dailyprogressreport/boq/{name}', 'API\DailyProgressReportController@getBoqHeaderList');

//to get boq items data
Route::get('/dailyprogressreport/boq_items/{boq_id}', 'API\DailyProgressReportController@getBoqDetailsList');