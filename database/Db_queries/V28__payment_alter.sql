

  alter table payments alter  payment_ref_no drop not null;
  alter table payments alter amount_received drop not null;
  alter table payments alter total_amount drop not null;


  delete from validations where entity_name = 'Payment';


INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Payment', 'Both', 'project_id', 'required', 1, now(), now(), 1, 1),
     ('Payment', 'Update', 'boq_no', 'required', 1, now(), now(), 1, 1),
     ('Payment', 'Create', 'boq_no', 'required', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'invoice_no', 'required|numeric', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'invoice_date', 'required|date', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'gst_amount', 'required|max:10|regex:/^\d+(\.\d{1,2})?$/', 1, now(), now(), 1, 1),
     ('Payment', 'Both', 'invoice_amount', 'required|max:10|regex:/^\d+(\.\d{1,2})?$/', 1, now(), now(), 1, 1);

delete from list_view_masters where  entity_name = 'Payment';
update menus set display_name = 'Update Payment' where display_name='New Payment';



  alter table payments add column tds_deduction numeric(10,2);
  alter table payments add column  other_deduction numeric(10,2);
  alter table payments add column  remark varchar ;


  INSERT INTO public.list_view_masters(
             function_name, entity_name,  
             query_columns, 
            heading_columns,
             filter_columns, 
             display_css_widths, 
             relations, replace_columns,
              created_at, updated_at, created_by, updated_by,
              filter_class_names,
              entity_namespace)
    VALUES ('List Payments', 'Payment',
            'payment_no,project_name,boq_no,invoice_no,invoice_date,invoice_amount,amount_received,id', 
            'Payment No.,Project,BOQ No.,Invoice No.,Invoice Date,Invoice Amount,Received Amount,Action',
             '', 
             'col-xs-1,col-xs-2,col-xs-2,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1', 
             '', '', now(),now(),1,1, 
             'App\Utilities\PaymentFilterUtility', 
             'App\Models\');





