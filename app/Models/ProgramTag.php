<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Feb 2018 18:20:16 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class ProgramTag
 * 
 * @property int $id
 * @property int $entity_id
 * @property array $tags
 * @property string $tags_virtual
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ProgramTag extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'entity_id' => 'int',
		//'tags' => 'json',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'entity_id',
		'tags',
		'tags_virtual',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
