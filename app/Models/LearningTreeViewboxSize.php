<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Apr 2018 10:36:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class LearningTreeViewboxSize
 * 
 * @property int $id
 * @property string $size
 * @property int $node_count
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class LearningTreeViewboxSize extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'node_count' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'size',
		'node_count',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}
}
