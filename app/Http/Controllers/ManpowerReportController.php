<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\ManpowerReportService;
use App\Services\ProjectService;
use App\Services\OrganizationService;
use App\Services\ExpenseTypeService;


class ManpowerReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function createManpowerReport() {

        $project = ProjectService::retrieveProject();
        $orgs = OrganizationService::retrieveOrganization();

		return view('reports.manpower_report' ,compact('project', 'orgs'));
	}


public function exportManpowerReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$project_id = $request['project_id'];
		$org_id = $request['org_id'];
    	$start_date = $request['start_date'];
		$end_date = $request['end_date'];

		$projectCode = 'All';
		$orgCode = 'All';
		if($org_id>0){
		$orgCode = ManpowerReportService::getOrgName($request['org_id']);
		}
		if($project_id>0){
		$projectCode = ManpowerReportService::getProjectName($request['project_id']);
		}
		$queryBuilder = ManpowerReportService::retrieveManpowerReportDetailQuery($project_id,$org_id,$start_date,$end_date);
	    self::downloadManpowerReportExcel($queryBuilder,$projectCode,$orgCode,$start_date,$end_date);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadManpowerReportExcel($queryBuilder,$projectCode,$orgCode,$start_date,$end_date) {
		$fileName = 'Manpower_Report__'.$orgCode.'_'.$projectCode.'_From_'.$start_date.'_To_'.$end_date;
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
         $keys = array( "Contractor Name", "Project Name", "No of Workers", "Date", 'Geo Location');
        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array(
        		 $row->org_name, $row->project_name, $row->no_of_workers, $row->d_date, $row->geolocation);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}


}
