<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Country
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 * @property \Illuminate\Database\Eloquent\Collection $cities
 * @property \Illuminate\Database\Eloquent\Collection $districts
 * @property \Illuminate\Database\Eloquent\Collection $pincodes
 * @property \Illuminate\Database\Eloquent\Collection $states
 *
 * @package App\Models
 */
class Country extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function addresses()
	{
		return $this->hasMany(\App\Models\Address::class);
	}

	public function cities()
	{
		return $this->hasMany(\App\Models\City::class);
	}

	public function districts()
	{
		return $this->hasMany(\App\Models\District::class);
	}

	public function pincodes()
	{
		return $this->hasMany(\App\Models\Pincode::class, 'countries_id');
	}

	public function states()
	{
		return $this->hasMany(\App\Models\State::class);
	}
}
