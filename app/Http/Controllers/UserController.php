<?php

namespace App\Http\Controllers;

//Custom Imports
use App\Models\Organization;
use App\Models\OrganizationAttribute;
use App\Models\PasswordLogic;
use App\Models\PeopleAttribute;
use App\Models\Person;
use App\Models\Role;
use App\Models\RoleUser;
use App\Services\OrganizationService;
use App\Services\UserService;
use App\Services\ValidationRulesService;
use App\Services\ProjectService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use DB;
use Session;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

class UserController extends BaseController {

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		$this->middleware('CheckCompositeMiddleWare', ['except' => ['update', 'create', 'show']]);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function index(Request $request, $flag = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(\Auth::user()->hasAccess(['Users']));
		$this->authorize('List Users', 'List Users');
		//parent::populateOrClearSearch($request);
		//self::filterParameters($request);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		//return PaginateController::paginate('Person','users', parent::$utilsInterface, parent::$masterDataInterface);
		return PaginateController::paginate('Person', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
	}

	private function filterParameters($request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		if ($request->path() == session('path')) {
			$customerPartyId = -1;
			$roleId = -1;

			if ($request->has('customer_party_id')) {
				$customerPartyId = $request['customer_party_id'];
			}

			if ($request->has('role_id')) {
				$roleId = $request['role_id'];
			}

			if ($customerPartyId == 0) {
				if ($request->session()->has('customer_party_id')) {
					$request->session()->forget('customer_party_id');
				}
			}

			if ($customerPartyId > 0) {
				session(['customer_party_id' => $customerPartyId]);
			}

			if ($roleId == 0) {
				if ($request->session()->has('role_id')) {
					$request->session()->forget('role_id');
				}
			}

			if ($roleId > 0) {
				session(['role_id' => $roleId]);
			}

			if ($request['page'] >= 1) {
				if ($request->session()->has('customer_party_id')) {
					$request['customer_party_id'] = session('customer_party_id');
				}
				if ($request->session()->has('role_id')) {
					$request['role_id'] = session('role_id');
				}
			}

		} else {
			session(['path' => $request->path()]);
			if ($request->session()->has('customer_party_id')) {
				$request->session()->forget('customer_party_id');
			}

			if ($request->session()->has('role_id')) {
				$request->session()->forget('role_id');
			}

		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function create() {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$this->authorize('Create Users', 'Create Users');
		$organization = OrganizationService::retrieveOrganization();
		//$role = UserService::retrieveRoles();
		//$role = UserService::retrieveNonAdminRoles();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return view('user.create_user', compact('organization','role'));
	}

	public function store(Request $userRequest) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$this->authorize('Create Users', 'Create Users');
		try {
		 $userRequest['firstName'] = ucwords(strtolower(trim($userRequest['firstName'])));
		 $userRequest['lastName'] = ucwords(strtolower(trim($userRequest['lastName'])));

			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('User');
			if ($userRequest->ajax()) {
				$userRequest->validate($rules);
			} else {
				$validator = Validator::make($userRequest->all(), $rules);
				if ($validator->fails()) {
					return redirect('users/create')
						->withErrors($validator)
						->withInput();
				}
			}
			// Get some basic data ahead  of time and set it in data object
			$data = $userRequest->all();
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();

			//Then call saveUser - which will do the needful
			if (sizeof($userRequest->allFiles()) > 0) {
				//userRequest needed only to upload the file otherwise not needed
				$data['userRequest'] = $userRequest;
				UserService::saveUser($data, true);
			} else {
				UserService::saveUser($data, false);
			}

			Session::flash('status', __('labels.flash_messages.store'));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return redirect()->action('UserController@index');

		} catch (Exception $e) {
			Log::error('Exception while storing user :: ', $e->getMessage());
		}

	}

	public function show($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$this->authorize('Show Users', 'Show Users');
		try {

			$data = Person::find($id);
			$user = User::where('party_id', $data['party_id'])->first();
			$data['userid'] = $user['userid'];

			if ($data['customer_party_id']) {
				$org = Organization::where('party_id', $data['customer_party_id'])->first();
			} else {
				$org = Organization::where('party_id', $data['tenant_party_id'])->first();
			}

			$org_id = $org['id'];
			$organization = OrganizationService::retrieveOrganization();
			
// role code addded by dhruvi
			$user_role_obj = RoleUser::where('user_id', $user->id)->first();

			//dd($user_role_obj);
			$sel_role_id = $user_role_obj['role_id'];

			$role = UserService::retrieveRoles();

// ends			

			// $role = Role::where('user', 'Facilitator')->first();
			// $role_user = RoleUser::where(['user_id' => $user->id, 'role_id' => $role['id']])->first();
			// if ($role_user) {
			// 	$facilitator = 1;
			// } else {
				$facilitator = 0;
			// }

			$peopleAttribute = PeopleAttribute::where(['people_id' => $id, 'attr_name' => 'dob'])->first();
			$data['dob'] = $peopleAttribute['attr_value'];

			$contacts = $data->party->contacts;
			Log::error($contacts);


			// Log::error($data);

			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return view('user.view_user', compact('data', 'organization', 'org_id', 'facilitator','sel_role_id','role', 'contacts'));

		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

	}

	public function showUserImport() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$this->authorize('Create Users', 'Create Users');
		$organization = OrganizationService::retrieveOrganization();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return view('user.import_user', compact('organization'));
	}

	public function edit($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$this->authorize('Edit Users', 'Edit Users');
			$data = Person::find($id);
			$user = User::where('party_id', $data['party_id'])->first();
			$data['userid'] = $user['userid'];
			Log::debug("***********USER*****************8");
			Log::debug($data['userid']);

			if ($data['customer_party_id']) {
				$org = Organization::where('party_id', $data['customer_party_id'])->first();
			} else {
				$org = Organization::where('party_id', $data['tenant_party_id'])->first();
			}

			$org_id = $org['id'];
			$organization = OrganizationService::retrieveOrganization();
// role code addded by dhruvi
			$user_role_obj = RoleUser::where('user_id', $user->id)->first();

			//dd($user_role_obj);
			$sel_role_id = $user_role_obj['role_id'];


			$role = UserService::retrieveRoles();

// ends	
			//checking for facilitator
			// $role = Role::where('name', 'Facilitator')->first();
			// $role_user = RoleUser::where(['user_id' => $user->id, 'role_id' => $role['id']])->first();
			// if ($role_user) {
			// 	$facilitator = 1;
			// } else {
				$facilitator = 0;
			// }
			$contact = $data->party->contacts;

			Log::error($data);

// dd($contact[0]['phone_number']);
			$peopleAttribute = PeopleAttribute::where(['people_id' => $id, 'attr_name' => 'dob'])->first();
			$data['dob'] = $peopleAttribute['attr_value'];
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return view('user.edit_user', compact('data', 'organization', 'org_id', 'facilitator','sel_role_id','role', 'contact'));

		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

	}

	public function update(Request $userRequest, $id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$this->authorize('Edit Users', 'Edit Users');

			 $userRequest['firstName'] = ucwords(strtolower(trim($userRequest['firstName'])));
     		 $userRequest['lastName'] = ucwords(strtolower(trim($userRequest['lastName'])));


     		 $data = Person::find($id);
     		 $contact = $data->party->contacts;
			 $contact_id = $contact[0]['id'];

			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('User', '$user', $id);

			
			if ($userRequest->ajax()) {
				$userRequest->validate($rules);
			} else {
				$validator = Validator::make($userRequest->all(), $rules);
				if ($validator->fails()) {
					return redirect('users/' . $id . '/edit')
						->withErrors($validator)
						->withInput();
				}
			}

			Log::debug(__LINE__ . "::" . __CLASS__ . "::" . __METHOD__ . $validator->fails());
			//Get some basic data ahead  of time and set it in data object
			$data = $userRequest->all();
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['id'] = $id;
			if (sizeof($userRequest->allFiles()) > 0) {
				//userRequest needed only to upload the file otherwise not needed
				$data['userRequest'] = $userRequest;
				$id = UserService::updateUser($data, true);
			} else {
				$id = UserService::updateUser($data, false);
			}

			Session::flash('status', __('labels.flash_messages.update'));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return redirect()->action('UserController@index');
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}
	}

	public function destroy($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {


			$user_id = auth()->user()->id;
			Log::error('see below for user id');
			Log::error($user_id);

			$role_user_data = RoleUser::where('user_id', $user_id)->first();
			$project_list = ProjectService::getProjectNamesForUser($id);

			Log::error($role_user_data);
			$this->authorize('Delete Users', 'Delete Users');
			if ($role_user_data['role_id'] != 1) {

				Log::error('user is not tenant admin');

				Session::flash('status', __('labels.users.unauthorised_user_message'));
			} 

			elseif(strlen($project_list)>=1){
				Session::flash('status', __('You can not delete this user as assigned in project(s) : '.$project_list));
			}
			else {

			$deleted = UserService::deleteUser($id);
			if ($deleted) {
				Session::flash('status', __('labels.flash_messages.delete'));
			} else {
				Session::flash('status', __('labels.users.delete_user_message'));
				}
			}

			$path_array = explode('/', action('UserController@index'));
			$route = array_pop($path_array);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return "deleted successfully" . "/" . $route;
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}
	}

	public function retrieveUser($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return UserService::getUserDetailsById($id, parent::$masterDataInterface);
	}

	public function verifyImportData(Request $request, MessageBag $messageBag) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$keys = array();
			$this->authorize('Create Users', 'Create Users');
			$data = $request->all();
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['request'] = $request;
			$check = UserService::verifyImportData($data, $messageBag);

			if ($check != 'SUCCESS') {
				if ($check == 'MODEL_NOT_FOUND') {
					Session::flash('status', __('labels.flash_messages.invalid_customer_code'));
					return redirect()->action('UserController@index');
				}
				foreach ($check as $key => $value) {
					foreach ($value as $key1 => $value1) {
						array_push($keys, $key1);
					}
					break;
				}
				//Parameters $fileName, $title, $sheetName, $description, $keys, $values){
				$timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
				parent::downloadSingleSheetExcelReport("Users" . $timeNow, "Invalid Records", "Invalid Records", "Invalid Records", $keys, $check);
				//Session::flash('status', __('labels.flash_messages.import_error'));
				Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
				//return redirect()->action('UserController@index');
			}
			Session::flash('status', __('labels.flash_messages.store'));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return redirect()->action('UserController@index');
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function userImport(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$this->authorize('Create Users', 'Create Users');
			$data = $request->all();
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['request'] = $request;
			UserService::importUsers($data);
			Session::flash('status', __('labels.flash_messages.store'));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return redirect()->action('UserController@index');
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}
	public static function getUsersByCompany($customer_party_id) {
		return UserService::getUsersByCompany($customer_party_id);
	}
	public function retrieveOrganization() {
		return OrganizationService::retrieveOrganizationForFilter();
	}
	public function retrieveRoles() {
		return UserService::retrieveRoles();
	}

	public function UserStatus() {
		//
		$organization = OrganizationService::retrieveOrganization();
		return view('user.user_status', compact('organization'));
	}

	public function getUsersForCompany($company_id, $action) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$company = Organization::where('id', $company_id)->first();
		$company_person = $company->person;
		$users = User::all();

		$company_user = [];
		foreach ($company_person as $key => $person_array) {
			//
			if ($action == 'activate') {
				$user = $users->where('party_id', $person_array['party_id'])->where('is_active', 0)->first();
			} elseif ($action == 'deactivate') {
				$user = $users->where('party_id', $person_array['party_id'])->where('is_active', 1)->first();
			}
			if ($user) {
				//
				$user['employee_id'] = $person_array['employee_id'];
				array_push($company_user, $user);
			}
		}

		return $company_user;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function activateUsers(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$data['users'] = json_decode($request['data'], true);
		$data['action'] = $request['action'];
		Log::debug($data);

		$data['authenticatedUser'] = \Auth::user();

		$participants = UserService::activateUsers($data);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}

	// abhi 26/03/19
	public static function getOrgPassLogic($id) {
		//

		$passLogic = self::checkOrgPasswordLogic($id);

		// Log::error($id);
		// Log::error($orgAttribute['attr_value']);
		// Log::error($passLogic['password_logic']);

		$response['dob_mandatory'] = false;
		if ($passLogic['password_logic'] == 'Date Of Birth') {
			$response['dob_mandatory'] = true;
		}

		return $response;
	}

	public static function checkOrgPasswordLogic($id) {

		$orgAttribute = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'password_logic'])->first();
		$passLogic = PasswordLogic::where('id', $orgAttribute['attr_value'])->first();
		return $passLogic;
	}

	public static function getResetPassword() {
		$organization = OrganizationService::retrieveOrganization();
		return view('user.user_reset_password', compact('organization'));
	}

	public static function resetUserPassword($userId) {
		//
		$user = User::where('id', $userId)->first();
		$person = Person::where('party_id', $user['party_id'])->first();

		//
		$peopleAttribute = PeopleAttribute::where(['people_id' => $person['id'], 'attr_name' => 'dob'])->first();
		$dob = $peopleAttribute['attr_value'];

		$organization = Organization::where('party_id', $person['customer_party_id'])->first();
		$orgId = $organization['id'];

		$phone = $user->party->contacts;
		if (isset($phone[0])) {
			$mobNo = $phone[0]['phone_number'];
		} else {
			$mobNo = '1111111';
		}

		//
		$pass = UserService::generatePassword($mobNo, $dob, $orgId, 0);

		$user->password = bcrypt($pass);
		$status = $user->save();

		$statusMsg = 'failed';
		if ($status) {
			$statusMsg = 'success';
		}

		return $statusMsg;
	}

// CREATED BY DHRUVI FOR GETTING  ROLE  COMPANY WISE

 public function getCompanyRole($org_id) {
    if($org_id==1){
    $roles = DB::table('roles')->select('id','name')
                 ->whereIn('id', [3,2])
				->get();
    }else {
    $roles = DB::table('roles')->select('id','name')
                 ->whereIn('id', [4])
				->get(); 	
    }
		return $roles;
    }
}
