<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 08 Jun 2018 11:54:20 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ForumThread
 * 
 * @property int $id
 * @property int $forum_id
 * @property string $subject
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ForumThread extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'forum_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'id',
		'forum_id',
		'subject',
		'description',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
