<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;


use App\Models\ExpenseType;
use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class ExpenseTypeService {
	use ValidationTrait;

	public static function retrieveExpenseTypes() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = ExpenseType::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

public static function saveExpenseTypeData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data) {
			$userId = $data['authenticatedUser']->id;

			$obj = new ExpenseType;
			$obj->name = $data['name'];
			$obj->description = $data['description'];
			if(isset($data['track_payment'])){

			$obj->track_payment = 'Yes';
			}
    		$obj->created_by = $userId;
			$obj->updated_by = $userId;

			$obj->save();
			$expense_type_id = $obj->id;
			return $expense_type_id;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	
public static function updateData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
					//$userId = $data['authenticatedUser']->id;
			if(isset($data['track_payment'])){
				$track_payment = 'Yes';
 				}	else {
				$track_payment = 'No'; 
				}
						
			ExpenseType::where('id', $data['id'])->update([
				'name' => $data['name'],
				'description' => $data['description'],
				'track_payment' => $track_payment,
				'updated_by' => $data['authenticatedUser']->id
			]);

			return $data['id'];
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
public static function deleteExpenseType($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$expense_type = ExpenseType::where('id', $id);
			//$user = self::validateActiveExpenseType($expense_type);

			//if ($user) {
				$userId = auth()->user()->id;

				$expense_type = ExpenseType::where('id', $id);
				$expense_type->update(['deleted_by' => $userId]);
				$check = $expense_type->delete();
			//} else {
			//	$check = false;
			//}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	private static function validateActiveExpenseType($expense_type) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$expense_type = $expense_type->first();
		Log::debug("********* VALIDATE ACTIVE expense_type 1".$expense_type);
		Log::debug("********* VALIDATE ACTIVE expense_type 2".$expense_type->id);


		$userObject = Project::where('expense_type_id', $expense_type->id);
		Log::debug("********* VALIDATE ACTIVE expense_type 3".$userObject);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		if ($userObject->first()->expense_type_id == $expense_type->id) {
			return false;
		} else {
			return $userObject;
		}
	}

}
