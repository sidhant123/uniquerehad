<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route::get('/', function () {
//  return view('welcome');
//});

Route::get('/svg', 'LearningTreeController@generateSvg');
Route::get('/dispatch', 'TestController@dispatchJob');
Auth::routes();
Route::get('/userLogIn', function () {
	return view('user.user_login');
});

Auth::routes();
// Route::get('/login', function () {
// 	return view('user.user_login');
// });
Auth::routes();
Route::get('/home', function () {
	return redirect('companies/')->withErrors(true)->withInput();
});
Route::group(['middleware' => ["auth", "CheckRole:Tenant Administrator,Admin"]], function () {

	//Learning Tree Creation and renderingg
	Route::get('/leafNodeDetails', 'TestController@getLeafNodeDetails');
	//Learning Tree Creation and renderingg

	//Resource routes
	Route::resource('/companies', 'OrganizationController');
	Route::resource('/users', 'UserController');
	// for user status
	Route::get('/userStatus', 'UserController@UserStatus');
	Route::get('/usersForCompany/{company_id}/{action}', 'UserController@getUsersForCompany');
	Route::post('/activateUsers', 'UserController@activateUsers');

	Route::get('/', 'OrganizationController@index');
	//Non-resource routes
	Route::get('/test', 'TestController@test');
	Route::get('/test2', '.0@createData');
	Route::get('/events', 'TestController@commTest');
	Route::get('/json', 'TestController@jsonTest');
	Route::get('/user/{id}', 'UserController@retrieveUser');
	Route::post('/userImport', 'UserController@userImport');
	Route::get('/test1', 'UserController@calledFunc')->name('reforward');

	//abhi - 26/03/19
	Route::get('/getOrgPassLogic/{id}', 'UserController@getOrgPassLogic');

	//Route::get('/home', 'HomeController@index')->name('home');

	Route::get('getUserDataByID/{id}', 'OrganizationController@getUserDataByID');
	Route::get('/addressReport', 'TestController@getListViewByQuery')->middleware('CheckCompositeMiddleWare')->name('addressReport');

	//for filter in pincodes
	Route::get('/countriesForFilter', 'AddressController@countries')->name('countriesForFilter');
	Route::get('/statesForFilter/{country_id}', 'AddressController@states')->name('statesForFilter');
	Route::get('/citiesForFilter/{state_id}', 'AddressController@cities')->name('citiesForFilter');
	Route::get('/pincodesForFilter/{city_id}', 'AddressController@pincodes')->name('pincodesForFilter');

	// Route::get('/program','ProgramController@ShowProgram');
	Route::post('/createProgram', 'ProgramController@ProgramCreation');
	Route::post('/programDirector', 'ProgramController@ProgramDirectorCreation');
	Route::post('/programCoordinators', 'ProgramController@ProgramCoordinatorsCreation');

	Route::get('/retrieveOrganization', 'UserController@retrieveOrganization')->name("retrieveOrganization");
	Route::get('/usersForProgram/{company_id}', 'ProgramController@users');
	Route::get('/getUsersByCompany/{customer_party_id}', 'UserController@getUsersByCompany');
	Route::resource('/programs', 'ProgramController');

	Route::get('/programDirectorVideo/{tag}', 'ProgramController@getVideoDocument');
	Route::get('/importParticipants/{program_id}', 'ProgramController@importParticipantsFromProgram');
	//Route::get('/programsForCompany/{company_id}', 'ProgramController@getProgramsForCompany');
	Route::get('/importParticipantsWithPhone/{program_id}', 'ProgramController@importParticipantsWithPhoneFromProgram');
	//new program routes
	Route::post('/approveParticipants', 'ProgramController@approveParticipants');
	Route::post('/addParticipants', 'ProgramController@addParticipants');
	Route::post('/unapproveParticipants', 'ProgramController@unapproveParticipants');
	Route::get('/publishProgram/{program_id}', 'ProgramController@publishProgram');

	//new for learning tree in program
	Route::get('/saveLTForProgram/{lt_id}/{p_id}', 'ProgramLearningTreeController@createLearningTreeForProgram');
	Route::post('/saveProgramTopic', 'ProgramLearningTreeController@saveProgramTopic');
	Route::get('/deleteProgramTopic/{id}/{cLT}/{tab}', 'ProgramLearningTreeController@deleteProgramTopic');
	Route::get('/getProgramTopicForEdit/{id}', 'ProgramLearningTreeController@getProgramTopicForEdit');
	Route::get('/getProgramLtParentStructure/{cLT}/{id}', 'ProgramLearningTreeController@getParentStructure');
	Route::get('/getParentLtUpdateStructure/{cLT}', 'ProgramLearningTreeController@getUpdateStructure');
	Route::get('/getSvgForProgram/{cLT}/{id}/{name}', 'ProgramLearningTreeController@getSvgForProgram');
	Route::post('/saveProgramAsLt', 'ProgramLearningTreeController@saveProgramAsLt');

	//Resource routes
	Route::resource('/companies', 'OrganizationController');
	Route::resource('/users', 'UserController');
	Route::get('/retrieveRoles', 'UserController@retrieveRoles')->name('retrieveRoles');
	Route::get('/', 'OrganizationController@index');
	//Non-resource routes
	Route::get('/test', 'TestController@test');
	Route::get('/test2', 'Test2Controller@createData');
	Route::get('/events', 'TestController@commTest');
	Route::get('/json', 'TestController@jsonTest');
	Route::get('/user/{id}', 'UserController@retrieveUser');
	Route::post('/userImport', 'UserController@userImport');
	Route::post('/userImportDataVerification', 'UserController@verifyImportData');
	Route::get('/test1', 'UserController@calledFunc')->name('reforward');

	//Route::get('/home', 'HomeController@index')->name('home');

	Route::get('getUserDataByID/{id}', 'OrganizationController@getUserDataByID');
	Route::get('/addressReport', 'TestController@getListViewByQuery')->middleware('CheckCompositeMiddleWare')->name('addressReport');

	//for filter in pincodes
	Route::get('/countriesForFilter', 'AddressController@countries')->name('countriesForFilter');
	Route::get('/statesForFilter/{country_id}', 'AddressController@states')->name('statesForFilter');
	Route::get('/citiesForFilter/{state_id}', 'AddressController@cities')->name('citiesForFilter');
	Route::get('/pincodesForFilter/{city_id}', 'AddressController@pincodes')->name('pincodesForFilter');

	// Route::get('/program','ProgramController@ShowProgram');
	Route::post('/createProgram', 'ProgramController@ProgramCreation');
	Route::post('/programDirector', 'ProgramController@ProgramDirectorCreation');
	Route::post('/programCoordinators', 'ProgramController@ProgramCoordinatorsCreation');

	Route::get('/retrieveOrganization', 'UserController@retrieveOrganization')->name("retrieveOrganization");
	Route::get('/usersForProgram/{company_id}', 'ProgramController@users');
	Route::get('/getUsersByCompany/{customer_party_id}', 'UserController@getUsersByCompany');
	Route::resource('/programs', 'ProgramController');

	Route::get('/programDirectorVideo/{tag}', 'ProgramController@getVideoDocument');
	Route::get('/importParticipants/{program_id}', 'ProgramController@importParticipantsFromProgram');
	Route::get('/programsForCompany/{company_id}', 'ProgramController@getProgramsForCompany');

	Route::resource('/learningTrees', 'LearningTreeController');

	// Route::post('/headerLearningTree', 'LearningTreeController@createLearningTree');

	Route::get('/programsByCustomer/{customer_party_id}', 'ProgramController@getProgramByCustomers');

	// learning tree routes
	Route::post('/headerLearningTree', 'LearningTreeController@createLearningTree');
	Route::get('/topicDocument/{tag}/{artefact_type_id}', 'LearningTreeController@getTopicDocument');
	Route::post('/saveTopic', 'LearningTreeController@saveTopic');
	// new routes
	Route::get('/getParentStructure/{cLT}/{id}', 'LearningTreeController@getParentStructure');
	Route::get('/getUpdateStructure/{cLT}', 'LearningTreeController@getUpdateStructure');
	Route::get('/deleteTopic/{id}/{cLT}/{tab}', 'LearningTreeController@deleteTopic');
	Route::get('/getTopicForEdit/{id}', 'LearningTreeController@getTopicForEdit');
	Route::get('/getSvg/{cLT}/{id}/{name}', 'LearningTreeController@getSvg');
	// learning tree routes ends

	Route::get('/testExcel', 'TestController@testExcel');

	Route::get('/eagerLoading', 'TestController@eagerLoading');

	Route::get('/vcParticipantAssign/{topic_id}/{parent_topic_id}', 'ProgramLearningTreeController@vcParticipantAssign');
	Route::post('/vcParticipants', 'ProgramLearningTreeController@saveVcParticipants');

	Route::get('/resetPassword', 'UserController@getResetPassword');
	Route::get('/resetUserPassword/{userId}', 'UserController@resetUserPassword');

	//// Private Forum
	Route::get('/getRolesList', 'ProgramController@getRolesList');
	Route::get('/getProgramRolePerson/{company_id}/{role_id}', 'ProgramController@getProgramRolePerson');

});
Route::get('/testPDF', 'TestController@testPDF');
Route::get('/testLearningTree/{learning_tree_id}', 'TestController@testLearningTree');

Route::get('logout', 'Auth\LoginController@logout');
Route::get('forgotPassword', function () {
	return view('user.user_forget_password');
});

// Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// ========Master Program Routes  registered by Dipak Rathod begins========
Route::resource('/masterProgram', 'MasterProgramController');
Route::resource('/learningTheme', 'LearningThemeController');
Route::get('/getMasterProgramName/{customer_party_id}', 'LearningThemeController@getMasterProgramName');
Route::get('/getLearningThemeName/{master_program_id}', 'ProgramController@getLearningThemeName');
Route::get('/getCompanyUsers/{company_id}', 'MasterProgramController@getCompanyUsers');
Route::post('storeMasterProgram', 'ProgramController@storeMasterProgram');
Route::post('storeLearningTheme', 'ProgramController@storeLearningTheme');
// ========Master Program Routes  registered by Dipak Rathod ends========


	Route::get('/readUserNeverLogged/{data}', 'UserController@readUexportReceiptserNeverLogged');




	Route::get('/generateEncodedPassword', 'demoController@generateEncodedPassword');

//--------- uniquerehad changes
Route::resource('/projects', 'ProjectController');
Route::resource('/sites', 'SiteController');
Route::get('/projects/{project_id}/exportBoq', 'ProjectController@exportBoq');
Route::delete('/projects/deleteBoq/{boq_id}', 'ProjectController@deleteBoq');
Route::resource('/materials', 'MaterialController');
Route::post('/materialImportDataVerification', 'MaterialController@verifyImporMaterialData');

Route::get('/getCompanyUser/{customer_party_id}', 'ProjectController@getClientUsers');
Route::resource('/receipts', 'ReceiptController');
Route::get('/receipts/{project_id}/exportReceipt', 'ReceiptController@exportReceipt');
Route::post('/receiptImport', 'ReceiptController@importReceipt');
Route::resource('/expenseTypes', 'ExpenseTypeController');
Route::resource('/payments', 'PaymentController');
Route::get('/getCompanyProject/{customer_party_id}', 'ProjectController@getCompanyProjects');
Route::resource('/manpowerContracts', 'ManpowerContractController');
Route::get('/getBoqList/{customer_party_id}', 'ReceiptController@retriveBoqList');
Route::get('/getSiteLocation/{site_id}', 'ProjectController@retriveSiteLocation');
Route::delete('/projects/changeStatusToStarted/{boq_id}', 'ProjectController@changeStatusToStarted');
Route::delete('/projects/changeStatusToCompleted/{boq_id}', 'ProjectController@changeStatusToCompleted');

Route::get('/retrieveProjects', 'ProjectController@retrieveProjectForFilter')->name("retrieveProjects");
Route::get('/retrieveClient', 'ProjectController@retrieveClient')->name("retrieveClient");
Route::get('/getCompanyRole/{customer_party_id}', 'UserController@getCompanyRole');
Route::get('/getServiceList/{customer_party_id}', 'ManpowerContractController@retriveServiceList');



	Route::post('stockReport', 'StockReportController@exportData');
	Route::get('stockReport', 'StockReportController@create');

	//Route::post('boqReport', 'BoqReportController@exportData');
	Route::get('boqReport', 'BoqReportController@create');
	Route::get('get_daily_usage_count', 'DumpReportController@getDailyUsageCount');


	Route::post('boq_detail_report', 'BoqReportController@exportDetailReport');
	Route::post('boq_summary_report', 'BoqReportController@exportSummaryReport');

	Route::post('attendanceReport', 'AttandanceReportController@attandanceReport');
	Route::get('attendanceReport', 'AttandanceReportController@create');
	Route::post('attendanceDetailReport', 'AttandanceReportController@exportAttendanceDetailReport');

	Route::post('profitLossReport', 'ProfitLossReportController@exportReport');
	Route::get('profitLossReport', 'ProfitLossReportController@create');

	Route::post('paymentReport', 'PaymentReportController@exportPaymentReport');
	Route::get('paymentReport', 'PaymentReportController@createPaymentReport');

	Route::post('receiptReport', 'ReceiptReportController@exportReceiptReport');
	Route::get('receiptReport', 'ReceiptReportController@createReceiptReport');

	Route::post('manpowerReport', 'ManpowerReportController@exportManpowerReport');
	Route::get('manpowerReport', 'ManpowerReportController@createManpowerReport');

	Route::post('inwardReport', 'InwardReportController@exportInwardReport');
	Route::get('inwardReport', 'InwardReportController@createInwardReport');

	