/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $(document).ready(function() {

  $.getJSON('/getEmailData', new Date(), autocompleteForEmail).error(errorResponse);
  $('.multiple_emails').select2({
   tags: true,
   minimumInputLength: 3,
   tokenSeparators: [',', ' ']
 });
  $('#templates_id').select2({
 });
  $('#summernote').summernote({

    toolbar: [
    ['style', ['style']],
    ['font', ['bold', 'underline', 'clear']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture'
        //, 'video'
        ]],
        ['view', ['fullscreen', 'codeview', 'help']]
        ],
      });


//  $.ajaxSetup({
//   headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//   }
// });   
});
$(document).on('change','#templates_id',function(){
  var data = $('#templates_id').select2('data');
  console.log(data[0].id);
  if(data[0].id){
      getDataThroughAxios('/getTemplate/'+ data[0].id);

    } 
});

$(document).on('change','#customer_id',function(){
var id = $(this).val();
if(id> 0){
  populateProgramByCustomer(id);
}
});
function populateProgramByCustomer(customer_party_id){
$('#program_id').empty();
$('#program_id').append('<option value="0">---Select---</option>');
 axios.get(url)
  .then(function (response) {
    console.log(response);
     $.each(response['data'], function (key, value) {
      $('#program_id').append('<option value='+value.id+'>'+value.code+' --- '+value.name+'</option>');
     }
  })
  .catch(function (error) {
    console.log("error"+error);
  });

}
 var postForm = function() {
   var content = $('.note-editable').html();
 }

 $(document).on('click','.note-btn',function(){
   console.log("clear modal-backdrop");
   $(".modal-backdrop").remove();
  });
 
 $(document).on('click','#clearBtn', function(){
  clearForm();
})
 $(document).on('click','#saveBtn',function(){
  var html_data = $('#summernote').summernote('code');
  var img_src = getImgSrc();
  var img_name = getImgName();
  var objData = {};
  objData['html_data'] = html_data;       
  objData['img_src'] = img_src;
  objData['img_name'] = img_name;
  console.log(objData);
  console.log(objData);
  var stringifyData = JSON.stringify(objData);           
  var formData = $('#form').serialize();
  var data = "data="+encodeURIComponent(stringifyData) + '&' +formData
  console.log(data);
  // console.log(validateMandatoryFields());
  //if(validateMandatoryFields() == true){
    // saveHtmlText(data);  
  if(validatePlaceHoldersFilledOut()){
    saveHtmlText(data);
  }

});
 function getImgName(){
  var img_name = getAttributeValue('data-filename');
  return img_name;
}
function getImgSrc(){
 var img_src = getAttributeValue('src');
 return img_src;
}
function getAttributeValue(attr_name){
  var img_attr = document.getElementsByTagName('img');
  var attr_array = [];
  var count = img_attr.length;
  if(count > 0){
    for(var i = 0;i<count;i++){
      attr_array.push(img_attr[i].getAttribute(attr_name));
    }
  }
  return attr_array;
} 
function saveHtmlText(data){
  $.ajax({
    type: "post",
    url: '/sendMail',
    data: data,
    success: success,
        //dataType: false,
      });
}
function success(data){

}
function autocompleteForEmail(data){
 $("#email_to_id").empty();
 $("#email_cc_id").empty();
 $("#email_bcc_id").empty();
 $.each(data, function (key, value) {
  $("#email_to_id").append("<option value='" + value.email + "'>" + value.email + "</option>");
  $("#email_cc_id").append("<option value='" + value.email + "'>" + value.email + "</option>");
  $("#email_bcc_id").append("<option value='" + value.email + "'>" + value.email + "</option>");
});   
}
function errorResponse(){

}
function validateMandatoryFields(){
  var valid = true;
  var to_email = $('#email_to_id').val();
  var summernote = $('.note-editable').val();
      if (to_email == null || to_email === '')
        {
          $('#email_to_id').addClass('fillFields');
          valid = false;
          return valid;
        }
      if(summernote === '' || summernote == null)
        {
        $('.note-editable').addClass('fillFields');
        }
return valid;
}
function getDataThroughAxios(url){
  axios.get(url)
  .then(function (response) {
    console.log(response);
    getTemplateData(response['data']);
  })
  .catch(function (error) {
    console.log("error"+error);
  });
}
function getTemplateData(template){
  $('#subject').val(template.subject);
  $('#summernote').summernote('code', template.template);
}
function clearForm(){
  $('.note-editable').html('');
  $('#email_to_id').val(null);
  $('#email_cc_id').val(null);
  $('#subject').val('');
}
function validatePlaceHoldersFilledOut(){
  var flag = true;
  var subject = $('#subject').val();
  var html_data = $('#summernote').summernote('code');
  var subjectplaceholders = placeholderValidator(subject);
  var summernoteplaceholders = placeholderValidator(html_data);
  var placeholder_array,msg;
  if(Object.keys(subjectplaceholders).length > 0){
       placeholder_array = placeHolders(subjectplaceholders);
       msg = placeholder_array.join();
    $('.errorMessage').html('Please filled out '+msg+' placeholders.');
    flag = false;
  }else{
    $('.errorMessage').html('');
    if(Object.keys(summernoteplaceholders).length > 0){
      placeholder_array = placeHolders(summernoteplaceholders);
       msg = placeholder_array.join();
    $('.errorMessage').html('Please filled out '+msg+' placeholders.');
    flag = false;
  }else{
    $('.errorMessage').html('');
  }
  }
  return flag;
}
function placeholderValidator(text){
    var found = [],    
    rxp = /{([^}]+)}/g,
    curMatch;
while( curMatch = rxp.exec( text ) ) {
    found.push( curMatch[1] );
}
  return  found;
}
function placeHolders(placeHolders){
  var placeholders = [];
  for(var ph in placeHolders){
    placeholders.push('{'+ placeHolders[ph] +'}');   
  }
  return placeholders;
}