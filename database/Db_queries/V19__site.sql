
INSERT INTO public.menus(
     id, display_name, name, sequence,  auth, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (83, 'Sites', 'Sites', 30,  1, 'glyphicon glyphicon-tent', now(), now(),  1, 1);


    INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES  
    ( 'List', 'List Sites', 31, 83,  1, now(), now(),  1, 1), 
    (  'New Site', 'Create Sites', 32, 83,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020

INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (25,'Sites', now(), now(), 1, 1),
     (26, 'List Sites', now(), now(), 1, 1),
     (27, 'Create Sites', now(), now(), 1, 1),
     (28, 'Show Sites', now(), now(), 1, 1),
     (29, 'Delete Sites', now(), now(), 1, 1),
     (30, 'Edit Sites', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (25, 1, 25, now(), now(), 1,1),
     (26, 1, 26, now(), now(), 1,1),
     (27, 1, 27, now(), now(), 1,1),
     (28, 1, 28, now(), now(), 1,1),
     (29, 1, 29, now(), now(), 1,1),
     (30, 1, 30, now(), now(), 1,1);


INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('Site', 'Update', 'name', 'required|max:255|unique:sites,name,$site,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('Site', 'Create', 'name', 'required|max:255|unique:sites,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1);
  

ALTER SEQUENCE sites_id_seq RESTART WITH 10;

INSERT INTO public.list_view_masters(
             function_name, entity_name,  query_columns, 
            heading_columns,
             filter_columns, display_css_widths, relations, replace_columns,
              created_at, updated_at, 
             created_by, updated_by ,entity_namespace)
    VALUES ('List Sites', 'Site',  
        'id,name,address,id', 
         'Id,Name,address,Action',
             '', 'col-xs-1,col-xs-2,col-xs-4,col-xs-2', 
             '', '',
             now(),now(),1,1, 'App\Models\');

