update filter_masters set function_name = 'Company' where id  =1;

INSERT INTO public.filter_masters(
             created_at, updated_at,  created_by, updated_by, 
             function_name, route_name, "row", sequence, control_type, 
            control_id, control_text,  control_name, data_url, 
            load_event, action, label_css, field_css, tenant_party_id)
    VALUES (now(), now(), 1, 1,
            'Project', 'expenses.index', 1, 1, 'select', 
            'project_id', '---select---', 'project_id', 'retrieveProjects', 
            0,0, 'col-xs-1', 'col-xs-2', 0);

update list_view_masters set filter_columns = 'project_id' where entity_name='Expense';


INSERT INTO public.composition_masters(
             route_name, controller_name, view_name, sequence, 
            created_at, updated_at, created_by, updated_by,  tenant_party_id)
    VALUES ('expenses.index', 'App\Http\Controllers\ExpenseController', '', 2, 
        now(), now(), 1, 1, 0);
INSERT INTO public.composition_masters(
             route_name, controller_name, view_name, sequence, 
            created_at, updated_at, created_by, updated_by,  tenant_party_id)
    VALUES ('expenses.index', 'vnnogile\FilterViews\Controllers\FilterController', '', 1, 
        now(), now(), 1, 1, 0);

