@extends('layouts.base')
@section('content')

<div id="main_form">

 @include('errors.error_validation')

 <form action="/companies/@yield('editID')" method="post" onsubmit="return validateMandatoryFields()" enctype="multipart/form-data">
  {{ csrf_field() }}

  @section('editMethod')
  @show

 <h3 class="headings">@yield('title')</h3>
  
  <h3>{{ __('labels.company.company') }}</h3>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="org_name">{{ __('labels.company.organization_name') }}</label>
           <input type="text" class="form-control mandatory" id="org_name" name="organization_name" value="@yield('organization_name')" @yeild('organization_name_d')>
           <input type="hidden" name="organization_party_id" value="@yield('organization_party_id')">
           <input type="hidden" name="old_org_logo" value="@yield('old_org_logo')">
           <input type="hidden" name="old_org_logo_name" value="@yield('old_org_logo_name')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="country_id"> {{ __('labels.company.organization_country') }}</label>
           <select id="country_id" class="form-control mandatory" name="organization_country" value="@yield('organization_country')"></select>
           <!-- <input type="text" class="form-control" name="organization_country" value="@yield('organization_country')"> -->
         </div>
     </div>
  </div>  

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="org_addr1">{{ __('labels.company.organization_address1') }}</label>
           <input type="text" id="org_addr1" class="form-control mandatory" name="organization_address1" value="@yield('organization_address1')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="state_id">{{ __('labels.company.organization_state') }}</label>
           <select id="state_id" class="form-control mandatory" name="organization_state" value="@yield('organization_state')"></select>
           <!-- <input type="text" class="form-control" name="organization_state" value="@yield('organization_state')"> -->
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="org_addr2">{{ __('labels.company.organization_address2') }}</label>
           <input type="text" id="org_addr2" class="form-control mandatory" name="organization_address2" value="@yield('organization_address2')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="city_id">{{ __('labels.company.organization_city') }}</label>
           <select id="city_id" class="form-control mandatory" name="organization_city" value="@yield('organization_city')"></select>
           <!-- <input type="text" class="form-control" name="organization_city" value="@yield('organization_city')" > -->
         </div>
     </div>
  </div>

   <div class="row">
     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_addr3">{{ __('labels.company.organization_address3') }}</label>
          <input type="text" id="org_addr3" class="form-control mandatory" name="organization_address3" value="@yield('organization_address3')">
        </div>
    </div>

     <div class="col-lg-6">
      <div class="form-group required">
          <label for="pincode_id">{{ __('labels.company.organization_pincode') }}</label>
          <select id="pincode_id" class="form-control mandatory" name="organization_pincode" value="@yield('organization_pincode')"></select>
          <!--  <input type="text" class="form-control" name="organization_pincode" value="@yield('organization_pincode')"> -->
        </div>
    </div>
 </div>

   <div class="row">
     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_contact">{{ __('labels.company.organization_contact') }}</label>
          <input type="text" class="form-control" id="org_contact" name="organization_contact" value="@yield('organization_contact')">
        </div>
    </div>

     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_url">{{ __('labels.company.organization_url') }}</label>
          <input type="text" class="form-control mandatory" id="org_url" name="organization_url" value="@yield('organization_url')">
        </div>
    </div>
 </div>

   <div class="row">
     <div class="col-lg-6">
      <div class="form-group required">
          <label for="org_code">{{ __('labels.company.organization_code') }}</label>
          <input type="text" class="form-control mandatory" id="org_code" name="organization_code" value="@yield('organization_code')">
        </div>
    </div>

     <div class="col-lg-6">
      <div class="form-group required">
          <label for="file1">{{ __('labels.company.organization_logo') }}</label>
          <input type="file" class="form-control mandatory" id="file1" onchange="return fileValidation(this.id);" name="organization_logo" value="@yield('organization_logo')" >
        </div>
    </div>
 </div>


<!--     for project manager           --> 

<h3>{{ __('labels.company.program_manager') }}</h3>

<div class="row" id="pmCheck">
  <div class="col-lg-12">
    <label><input class="form-check" type="checkbox" id="pmDetach" name="pmDetach" value="pmDetach"> Detach User</label>
  </div>
</div>

<div id="pmSelector">

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_user_first_name_id">{{ __('labels.company.pm_firstName') }}</label>
           <select  class="form-control mandatory" id="pm_user_first_name_id" name="pm_firstName"></select>
              <input type="hidden" name="old_pm_employee_id" value="@yield('old_pm_employee_id')">
              <input type="hidden" name="old_pm_party_id" value="@yield('old_pm_party_id')">
              <input type="hidden" name="old_pm_photo" value="@yield('old_pm_photo')">
              <input type="hidden" name="old_pm_photo_name" value="@yield('old_pm_photo_name')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_last_name_id">{{ __('labels.company.pm_lastName') }}</label>
           <input type="text" class="form-control mandatory" id="pm_last_name_id" name="pm_lastName" value="@yield('pm_lastName')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_employee_id">{{ __('labels.company.pm_employeeId') }}</label>
           <input type="text" class="form-control" id="pm_employee_id" name="pm_employeeId" value="@yield('pm_employeeId')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_department_id">{{ __('labels.company.pm_department') }}</label>
           <input type="text" class="form-control" id="pm_department_id" name="pm_department" value="@yield('pm_department')" >
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_designation_id">{{ __('labels.company.pm_designation') }}</label>
           <input type="text" class="form-control" id="pm_designation_id" name="pm_designation" value="@yield('pm_designation')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_email_id">{{ __('labels.company.pm_email') }}</label>
           <input type="text" class="form-control mandatory" id="pm_email_id" name="pm_email" value="@yield('pm_email')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_primary_contact_id">{{ __('labels.company.pm_primaryContact') }}</label>
           <input type="text" class="form-control" id="pm_primary_contact_id" name="pm_primaryContact" value="@yield('pm_primaryContact')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_secondary_contact_id">{{ __('labels.company.pm_secondaryContact') }}</label>
           <input type="text" class="form-control" id="pm_secondary_contact_id" name="pm_secondaryContact" value="@yield('pm_secondaryContact')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_linkedin_profile_id">{{ __('labels.company.pm_linkedinProfile') }}</label>
           <input type="text" class="form-control" id="pm_linkedin_profile_id" name="pm_linkedinProfile" value="@yield('pm_linkedinProfile')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_fb_profile_id">{{ __('labels.company.pm_fbProfile') }}</label>
           <input type="text" class="form-control" id="pm_fb_profile_id" name="pm_fbProfile" value="@yield('pm_fbProfile')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="pm_twitter_handle_id">{{ __('labels.company.pm_twitterHandle') }}</label>
           <input type="text" class="form-control" id="pm_twitter_handle_id" name="pm_twitterHandle" value="@yield('pm_twitterHandle')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="file2">{{ __('labels.company.pm_photo') }}</label>
           <input type="file" class="form-control mandatory" id="file2" onchange="return fileValidation(this.id);" name="pm_photo" value="@yield('pm_photo')">
         </div>
     </div>
  </div>

                
</div>   



<!--   for coordinator-->

<h3>{{ __('labels.company.company_coordinator') }}</h3>

<div class="row" id="checkUser">
  <div class="col-lg-12">
    <label><input class="form-check" type="checkbox" id="sameUser" name="sameUser" value="sameUser" >  Same as Program Manager</label>
  </div>
</div>

<div class="row" id="ccCheck">
  <div class="col-lg-12">
    <label><input class="form-check" type="checkbox" id="ccDetach" name="ccDetach" value="ccDetach"> Detach User</label>    
  </div>
</div>


<div id="ccSelector">

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_first_name_id">{{ __('labels.company.cc_firstName') }}</label>
           <select class="form-control mandatory img" id="cc_first_name_id" name="cc_firstName"></select>
               <input type="hidden" name="old_cc_employee_id" value="@yield('old_cc_employee_id')">
               <input type="hidden" name="old_cc_party_id" value="@yield('old_cc_party_id')">
               <input type="hidden" name="old_cc_photo" value="@yield('old_cc_photo')">
               <input type="hidden" name="old_cc_photo_name" value="@yield('old_cc_photo_name')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_last_name_id">{{ __('labels.company.cc_lastName') }}</label>
           <input type="text" class="form-control mandatory" id="cc_last_name_id" name="cc_lastName" value="@yield('cc_lastName')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_employee_id">{{ __('labels.company.cc_employeeId') }}</label>
           <input type="text" class="form-control" id="cc_employee_id" name="cc_employeeId" value="@yield('cc_employeeId')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_department_id">{{ __('labels.company.cc_department') }}</label>
           <input type="text" class="form-control" id="cc_department_id" name="cc_department" value="@yield('cc_department')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_designation_id">{{ __('labels.company.cc_designation') }}</label>
           <input type="text" class="form-control" id="cc_designation_id" name="cc_designation" value="@yield('cc_designation')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_email_id">{{ __('labels.company.cc_email') }}</label>
           <input type="text" class="form-control mandatory" id="cc_email_id" name="cc_email" value="@yield('cc_email')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_primary_contact_id">{{ __('labels.company.cc_primaryContact') }}</label>
           <input type="text" class="form-control" id="cc_primary_contact_id" name="cc_primaryContact" value="@yield('cc_primaryContact')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_secondary_contact_id">{{ __('labels.company.cc_secondaryContact') }}</label>
           <input type="text" class="form-control" id="cc_secondary_contact_id" name="cc_secondaryContact" value="@yield('cc_secondaryContact')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_linkedin_profile_id">{{ __('labels.company.cc_linkedinProfile') }}</label>
           <input type="text" class="form-control" id="cc_linkedin_profile_id" name="cc_linkedinProfile" value="@yield('cc_linkedinProfile')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_fb_profile_id">{{ __('labels.company.cc_fbProfile') }}</label>
           <input type="text" class="form-control" id="cc_fb_profile_id" name="cc_fbProfile" value="@yield('cc_fbProfile')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="cc_twitter_handle_id">{{ __('labels.company.cc_twitterHandle') }}</label>
           <input type="text" class="form-control" id="cc_twitter_handle_id" name="cc_twitterHandle" value="@yield('cc_twitterHandle')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="file3">{{ __('labels.company.cc_photo') }}</label>
           <input type="file" class="form-control " id="file3" name="cc_photo" onchange="return fileValidation(this.id);" value="@yield('cc_photo')">
         </div>
     </div>
  </div>


</div>                 

<p></p>
<div class="row col-lg-12" align="center">           
  <button type="submit" class="btn btn-primary" >{{ __('labels.company.submit') }}</button>    
</div>
</form>    
</div>
@endsection


@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
   $('#pm_user_first_name_id').select2({
    tags: true
  });
   $('#cc_first_name_id').select2({
    tags: true
  });

 });
  $(document).on('change','#pm_user_first_name_id',function(){
    var user_id = $(this).val();
    if(user_id > 0){
      $.getJSON('/getUserDataByID/'+ user_id, new Date(), populateUserDataForPM).error(errorResponse);
    }else{
      clearFieldsForPM();
    }
  });
  $(document).on('change','#cc_first_name_id',function(){
    var user_id = $(this).val();
    if(user_id > 0){
      $.getJSON('/getUserDataByID/'+ user_id, new Date(), populateUserDataForCC).error(errorResponse);
    }else{
      clearFieldsForCC();
    }
  });
  $('#pm_user_first_name_id').empty();
  $('#cc_first_name_id').empty();
  $('#pm_user_first_name_id').append('<option value="0">--select--</option>');
  $('#cc_first_name_id').append('<option value="0">--select--</option>');

  @if(isset($people))
  @foreach($people as $user)
  @if(isset($pmData))
  @if($user->party_id === $pmData->party_id)
  $('#pm_user_first_name_id').append('<option value="{{$user->id}}" selected>{{$user->first_name}}</option>');
  @else
  $('#pm_user_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @else
  $('#pm_user_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @if(isset($ccData))
  @if($user->party_id === $ccData->party_id)
  $('#cc_first_name_id').append('<option value="{{$user->id}}" selected>{{$user->first_name}}</option>');
  @else
  $('#cc_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @else
  $('#cc_first_name_id').append('<option value="{{$user->id}}">{{$user->first_name}}</option>');
  @endif
  @endforeach
  @endif

  function populateUserDataForPM(data){
    if(Object.keys(data).length > 0){
      $('#pm_last_name_id').val(data.last_name);    
      $('#pm_employee_id').val(data.employee_id);    
      $('#pm_department_id').val(data.department);    
      $('#pm_designation_id').val(data.designation);    
      $('#pm_email_id').val(data.email);    
      $('#pm_primary_contact_id').val(data.primary_contact);    
      $('#pm_secondary_contact_id').val(data.secondary_contact);    
      $('#pm_linkedin_profile_id').val(data.linkedin_profile);    
      $('#pm_fb_profile_id').val(data.facebook_id);    
      $('#pm_twitter_handle_id').val(data.twitter_handle);

    }
  }
  function populateUserDataForCC(data){
    if(Object.keys(data).length > 0){
      $('#cc_last_name_id').val(data.last_name);    
      $('#cc_employee_id').val(data.employee_id);    
      $('#cc_department_id').val(data.department);    
      $('#cc_designation_id').val(data.designation);    
      $('#cc_email_id').val(data.email);    
      $('#cc_primary_contact_id').val(data.primary_contact);    
      $('#cc_secondary_contact_id').val(data.secondary_contact);    
      $('#cc_linkedin_profile_id').val(data.linkedin_profile);    
      $('#cc_fb_profile_id').val(data.facebook_id);    
      $('#cc_twitter_handle_id').val(data.twitter_handle);    
    }
  }
  
  function clearFieldsForPM(){
    $('#pm_last_name_id').val('');    
    $('#pm_employee_id').val('');    
    $('#pm_department_id').val('');    
    $('#pm_designation_id').val('');    
    $('#pm_email_id').val('');    
    $('#pm_primary_contact_id').val('');    
    $('#pm_secondary_contact_id').val('');    
    $('#pm_linkedin_profile_id').val('');    
    $('#pm_fb_profile_id').val('');    
    $('#pm_twitter_handle_id').val('');    
  }
  function clearFieldsForCC(){
    $('#cc_last_name_id').val('');    
    $('#cc_employee_id').val('');    
    $('#cc_department_id').val('');    
    $('#cc_designation_id').val('');    
    $('#cc_email_id').val('');    
    $('#cc_primary_contact_id').val('');    
    $('#cc_secondary_contact_id').val('');    
    $('#cc_linkedin_profile_id').val('');    
    $('#cc_fb_profile_id').val('');    
    $('#cc_twitter_handle_id').val(''); 
  }
  function errorResponse(error){
    console.log(error);
  }


  @foreach($geoData as $key => $data)
  $('#{{$key}}').append('<option value="0" selected>---select---</option>');
  @foreach($geoData[$key] as $value)
  @if(isset($orgData['orgAddress'][$key]))
  @if($orgData['orgAddress'][$key] == $value['id'])
  $('#{{$key}}').append('<option value="{{$value["id"]}}" selected>{{$value["name"]}}</option>');
  @else
  $('#{{$key}}').append('<option value="{{$value["id"]}}" >{{$value["name"]}}</option>');
  @endif
  @else
  $('#{{$key}}').append('<option value="{{$value["id"]}}" >{{$value["name"]}}</option>');
  @endif
  @endforeach

  @endforeach

</script>
@endpush