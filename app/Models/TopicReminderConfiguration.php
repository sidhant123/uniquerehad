<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 30 Jul 2018 16:55:29 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TopicReminderConfiguration
 * 
 * @property int $id
 * @property int $topic_id
 * @property \Carbon\Carbon $before_start_remind_date
 * @property \Carbon\Carbon $before_end remind_date
 * @property int $tenant_party_id
 * @property int $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class TopicReminderConfiguration extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'topic_id' => 'int',
		'tenant_party_id' => 'int',
		'is_active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'before_start_remind_date',
		'before_end remind_date'
	];

	protected $fillable = [
		'topic_id',
		'before_start_remind_date',
		'before_end remind_date',
		'tenant_party_id',
		'is_active',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
