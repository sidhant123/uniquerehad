
/////program next button
$(document).on('click','#program_next_btn',function(){
  if(document.getElementById('program_hidden_program_id').value=="tesseract"){

    //alert('First save the Program');
    bootbox.alert('First save the Program');
  }
  else{
   $(".div").hide();
   $("#director").show();
   $('#nav a').css('font-weight','normal');
   $('#li2').css('font-weight','bold');
 }

});


/////program save button
$(document).on('click','#program_save_btn',function(){

  // var formData = $('#program_form').serializeArray();
  var formData =  new FormData(document.getElementById("program_form"));
  var url='/createProgram';
  if(validateMandatoryFieldsForProgram('program_form')== true){
    var check=sendingRequestThroughAxios(url,formData);
  }

  console.log('In Program nextBtn function');
});



 //director nxt button
 $(document).on('click','#director_next_btn',function(){
  
 ///validation for video document
  
  var formData = $('#director_form').serializeArray();

  if(document.getElementById('program_hidden_program_id').value!="tesseract"){
   var programId=document.getElementById('program_hidden_program_id').value;
   formData.push({name: 'program_id', value: programId});
   var url='/programDirector';

   if(validateMandatoryFieldsForProgram('director_form')== true){
    var check=sendingRequestThroughAxios(url,formData);
  }
}
else{
 //alert("first save the program");
 bootbox.alert("first save the program");
}
console.log('In Director nextBtn function');
}); 


 //coordinator nxtBtn
 $(document).on('click','#coordinator_next_btn',function(){

  var formData = $('#coordinator_form_id').serializeArray();
  console.log(formData);

  if(document.getElementById('program_hidden_program_id').value!="tesseract"){
   var programId=document.getElementById('program_hidden_program_id').value;
   formData.push({name: 'program_id', value: programId});
   var url='/programCoordinators';

   if(document.getElementById('selected_coordinators_table_body_id').innerHTML){
    var check=sendingRequestThroughAxios(url,formData);
  }
  else{
    //alert('please select the coordinators');
    bootbox.alert('please select the coordinators');
  }
}
else{
 //alert("first save the program");
 bootbox.alert("first save the program");
}
console.log('In Coordinator nextBtn function');
}); 


// learning tree next button 
$(document).on('click','#lt_next_btn',function(){
//
  $(".div").hide();
  $("#participant").show();
  $('#nav a').css('font-weight','normal');
  $('#li5').css('font-weight','bold');
});  



 function validateMandatoryFieldsForProgram(id){
 
   var valid = true;
   $("#"+id+" "+".mandatory").each(function () {
     if ($(this).val().trim() === '' || $(this).val() === '0')
     {
      $(".mandatory").removeClass('fillFields');
      $(this).addClass('fillFields');
      valid = false;
      return valid;
    }
  });
   
   if (valid == false) {
    return valid;
   }
   // validation for
   if (id == 'director_form') {
     var a=0;
     if($('.video_path_checkbox').val()){
        console.log('hi');
        $('.video_path_checkbox').each(function(index, el) {
            console.log('pii');
            if ($(this).is(":checked")) {
                a=1;
                console.log('hii');
            }
        });
        // a=1;
     }
     if (a ==1) {
       valid= true;
     }
     else{
     valid=false;
     $('#video_input_id').addClass('fillFields');
     }
   }
   return valid;
 }

 function validateMandatoryParticipants(){
  
  if(document.getElementById('selected_participants_table_body_id').innerHTML){
     return true;
  }
  else{
    //alert('please select the participant');
    bootbox.alert('please select the participant');
    return false;
  }
 }


function errorResponse(errorResponse){
  console.log(errorResponse);
}


/////// for coordnator add and delete
$(document).on('click','#coordinators_add_btn_id',function(){

  $('#potential_coordinators_table_body_id tr').each(function(){


   if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){

     var id=$(this).find('td:eq(0)').find(':checkbox').val();
     var emp_id=$(this).find('td:eq(1)').text();
     var name=$(this).find('td:eq(2)').text();
                      
     $('#selected_coordinators_table_body_id').append("<tr><td width=10%><input class=selected_coordinators type=checkbox value="+id+"><input type=hidden name="+name+" value="+id+"></td><td width=25%>"+emp_id+"</td><td width=65%>"+name+"</td></tr>");
     $(this).remove();
           
         }
       });
});


////////////////delete button
$(document).on('click','#coordinators_delete_btn_id',function(){

  $('#selected_coordinators_table_body_id tr').each(function(){


   if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){

     var id=$(this).find('td:eq(0)').find(':checkbox').val();
     var emp_id=$(this).find('td:eq(1)').text();
     var name=$(this).find('td:eq(2)').text();
              
     $('#potential_coordinators_table_body_id').append("<tr><td width='10%'><input class=potential_coordinators type='checkbox' value="+id+"></td><td width='25%'>"+emp_id+"</td><td width='65%'>"+name+"</td></tr>");
     $(this).remove();
         
         }
       });

});

/////////// participant add and delete

$(document).on('click','#participants_add_btn_id',function(){

// checking for coordinator login
  // if (hidingApproveBtn()) {
  //     //
  //     return false;
  // }


  $('#potential_participants_table_body_id tr').each(function(){


   if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){

     var id=$(this).find('td:eq(0)').find(':checkbox').val();
     var apprv=$(this).find('td:eq(0)').find(':checkbox').attr('name');
     var emp_id=$(this).find('td:eq(1)').text();
     var name=$(this).find('td:eq(2)').text();
     var userid=$(this).find('td:eq(0)').find(':hidden').val();

     // $('#selected_participants_table_body_id').append("<tr><td width=10%><input type=checkbox name="+id+" value=''><input type=hidden name="+id+"a value="+id+"></td><td width=25%>"+emp_id+"</td><td width=65%>"+name+"</td></tr>");
     $('#selected_participants_table_body_id').append("<tr><td width=10%><input class=selected_participant type=checkbox name="+id+" value=''> <input type=hidden value="+userid+"> </td><td width=25%>"+emp_id+"</td><td width=50%>"+name+"</td><td width=15%>"+apprv+"</td></tr>");
     $(this).remove();
           
         }
       });
});


////////////////delete button
$(document).on('click','#participants_delete_btn_id',function(){

  // check for coordinator login
  // if (hidingApproveBtn()) {
  //     //
  //     return false;
  // }

  $('#selected_participants_table_body_id tr').each(function(){


   if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){

     var id=$(this).find('td:eq(0)').find(':checkbox').attr('name');
     var emp_id=$(this).find('td:eq(1)').text();
     var name=$(this).find('td:eq(2)').text();
     var check_name=$(this).find('td:eq(3)').text();
     var userid=$(this).find('td:eq(0)').find(':hidden').val();
 
     $('#potential_participants_table_body_id').append("<tr><td width='10%'><input class=potential_participant type='checkbox' name="+check_name+" value="+id+"> <input type=hidden value="+userid+"> </td><td width='25%'>"+emp_id+"</td><td width='65%'>"+name+"</td></tr>");
     $(this).remove();
  
     }
  });

});

///////////////      \\\ logic

// function hidingApproveBtn(){
//      //
//      if ($('#participant_approve_btn').is(":visible") || $('#participant_unapprove_btn').is(":visible")) {
//      var r=confirm('If add participant operation is performed, participant approve or unapprove operation will not be accessed');
//      if (r == true) {
//         //
//         $('#participant_approve_btn').hide();
//         $('#participant_unapprove_btn').hide();
//         return false;
//      } else {
//      //
//        return true;
//      }
//      }
// }



//axios to send msg
function sendingRequestThroughAxios(url,data){

  console.log("in sendingThroughAxios");

  axios({
    method: 'post',
    url: url,
    data: data,

  }).then(function(response) {

    var data=response['data'];
    
    ///after program save
    if (url == '/createProgram') {
      console.log('after program save success');
      console.log(data);
      //alert('program saved successufully');
      bootbox.alert('program saved successufully');
      if(document.getElementById('program_hidden_program_id').value=="tesseract"){
        console.log("keirn");
       //setting hidden in program div first time
       document.getElementById('program_hidden_program_id').value=data;
       }
    
        var company=$('#company_name_id').select2('data');
        if(document.getElementById('updated_company_name_id').value!=company[0].id){ 
        getDataOfUsersOnProgramSave('/usersForProgram/'+company[0].id,'director_name_id','potential_coordinators_table_body_id','potential_participants_table_body_id');
        }
        document.getElementById('updated_company_name_id').value=company[0].id;
        
        ///append for import Participants from program
        $program_name=$('#program_name_id').val();
        getProgramDataForCompany('/programsForCompany/'+company[0].id,'import_program_id',$program_name);

       ////enabling all the list buttons
       $("#li2").removeClass("pointerEventNone");
       $("#li3").removeClass("pointerEventNone");
       $("#li4").removeClass("pointerEventNone");
       $("#li5").removeClass("pointerEventNone");

       ///////////////////***** setting for learning tree  *****\\\\\\\\\\\\\\\\
       $('#hidden_learning_tree_id_id').val(data);
       $('#hidden_learning_tree_name_id').val($('#program_name_id').val());

       //// setting for program director dates
       $('#director_from_id').val($('#program_from_id').val());
       $('#director_to_id').val($('#program_to_id').val());

       ////////ends\\\\\\\\\
    }
    
    //// after director save
    else if(url=='/programDirector'){
     console.log(data);
     $(".div").hide();
     $("#coordinator").show();
     $('#nav a').css('font-weight','normal');
     $('#li3').css('font-weight','bold');

     // checking for publish btn
     if (data['publish'] == 'yes') {
         $('#publish_save_btn').show();
     }
     else if (data['publish'] == 'no'){
         $('#publish_save_btn').hide();
     }
   }
   
   //// after coordinator save
   else if (url=='/programCoordinators') {
     console.log(data);
     $(".div").hide();
     $("#learning_tree").show();
     $('#nav a').css('font-weight','normal');
     $('#li4').css('font-weight','bold');

     //checking for publish btn
     if (data['publish'] == 'yes') {
         $('#publish_save_btn').show();
     }
     else if (data['publish'] == 'no'){
         $('#publish_save_btn').hide();
     }
     
   }  
   //// after participants save
   else if (url=='/programParticipants') {
     console.log(data);
   }


 }).catch(function (error) {
  console.log(error.response.data.errors);

  var error_msg='';
  $.each(error.response.data.errors,function(key,value){
         
         console.log(value[0]);
         error_msg=error_msg+'<li>'+value['0']+'</li>';
  })

  var error_msges='<ul>'+error_msg+'</ul>';
  showSuccessMessage(error_msges);
  console.log(error_msges);

});

}

function showSuccessMessage(message){
  console.log(message);
  $('#errorMessage').html('');
  $('#errorMessage').removeClass('hide');
  $('#errorMessage').html(message);
  var scrollPos =  $(".alert").offset().top;
    $(window).scrollTop(scrollPos);

  //
  setTimeout(function(){ 
    $('#errorMessage').addClass('hide');
  }, 8000);  

}

/////// Datepicker logic

$(document).on('change', '#program_from_id', function () {
  var start_date = $(this).val();
  console.log(start_date);
  var array_start_date = start_date.split('-');
  var corrected_start_date = new Date(array_start_date[2], array_start_date[1], array_start_date[0]);
  var end_date_id = $("#program_to_id").val();
  var array_end_date_id = end_date_id.split('-');
  var corrected_end_date_id = new Date(array_end_date_id[2], array_end_date_id[1], array_end_date_id[0]);
  console.log(start_date);
  $('#program_to_id').datepicker('destroy');
  $("#program_to_id").datepicker({
    dateFormat: "dd-mm-yy",
    minDate: start_date,
  });
  if (corrected_start_date > corrected_end_date_id) {
    $("#program_to_id").val('');
  }
});

$(document).on('change', '#director_from_id', function () {
  var start_date = $(this).val();
  console.log(start_date);
  var array_start_date = start_date.split('-');
  var corrected_start_date = new Date(array_start_date[2], array_start_date[1], array_start_date[0]);
  var end_date_id = $("#director_to_id").val();
  var array_end_date_id = end_date_id.split('-');
  var corrected_end_date_id = new Date(array_end_date_id[2], array_end_date_id[1], array_end_date_id[0]);
  console.log(start_date);
  $('#director_to_id').datepicker('destroy');
  $("#director_to_id").datepicker({
    dateFormat: "dd-mm-yy",
    minDate: start_date,
  });
  if (corrected_start_date > corrected_end_date_id) {
    $("#director_to_id").val('');
  }
});


////for welcome video

  
  $(document).on('click','#video_button_id',function(){
    //validation
      
    var tag=document.getElementById('video_input_id').value;
    console.log(tag);
    if(tag){
    getDocumentsForWeocomeVideo('/programDirectorVideo/'+tag);
    }

});

   var documents=new Object();

function getDocumentsForWeocomeVideo(url){
    axios.get(url)
        .then(function (response) {

        console.log(response);
        console.log('after printing response getDataThroughAxiosForVideo');

        if(Object.keys(response).length > 0){

           $("#video_path_option_id").empty();
           $.each(response['data'], function (key, value) {       
           console.log(value.document_path);
           $("#video_path_option_id").append("<tr><td width=10% ><input type=checkbox class='video_path_checkbox' name='video_id_checkbox' value="+value.id+"></td><td width=90%>"+value.name+"</td></tr>")
           
           documents[value.id]=[value.name,value.document_path];
           });
        }
    })
        .catch(function (error) {
        console.log("error"+error);
   });
  }


   $(document).on('change','input.video_path_checkbox',function(){
        // if(this.checked == true){
        var id=$(this).val();
        // $('#hidden_document_name_id').val=documents[id][0];
        document.getElementById('hidden_document_name_id').value=documents[id][0];
        console.log(documents[id][0]);
        document.getElementById('hidden_document_url_id').value=documents[id][1];
        // $('#hidden_document_url_id').val=documents[id][1];  
         // }   
         $('input.video_path_checkbox').not(this).prop('checked', false); 
      });


  /////import from program
  $(document).on('click','#import_participants_button',function(){
      
     var program_name=$('#import_program_id').select2('data');
     console.log(program_name[0].id);
     if(program_name[0].id==0 || program_name[0].id=== undefined  ){
      $('#import_program_id').addClass('fillFields');
      return false;
     }

     var url="/importParticipants/"+program_name[0].id;

     axios.get(url)
        .then(function (response) {

        if(Object.keys(response).length > 0){

            console.log(response['data']);
            
            $.each(response['data'], function (key, value) {
                    
                    var i=0; 
                    $('#potential_participants_table_body_id tr').each(function(){
                      console.log($(this).find('td:eq(0)').find(':checkbox').val());
                       if($(this).find('td:eq(0)').find(':checkbox').val()==value['id']){
                           i=1; 
                          // adding 
                           var id=$(this).find('td:eq(0)').find(':checkbox').val();
                           var apprv=$(this).find('td:eq(0)').find(':checkbox').attr('name');
                           var emp_id=$(this).find('td:eq(1)').text();
                           var name=$(this).find('td:eq(2)').text();
                           var userid=$(this).find('td:eq(0)').find(':hidden').val();

                            $('#selected_participants_table_body_id').append("<tr><td width=10%><input class=selected_participant type=checkbox name="+id+" value=''> <input type=hidden value="+userid+"> </td><td width=25%>"+emp_id+"</td><td width=50%>"+name+"</td><td width=15%>"+apprv+"</td></tr>");
                            $(this).remove();
                          // removing

                           return false;
                        }

                    });
                    
                    // console.log(i);
                    // if(i==0){
                    // //appending 
                    // // $('#selected_participants_table_body_id').append("<tr><td width='10%'><input type='checkbox' value="+value['id']+"><input type=hidden name="+value['first_name']+" value="+value['id']+"></td><td width='25%'>"+value['employee_id']+"</td><td width='65%'>"+value['first_name']+" "+value['last_name']+"</td></tr>");
                    // $('#selected_participants_table_body_id').append("<tr><td width='10%'><input class=selected_participant type='checkbox' name="+value['id']+" value=''><input type=hidden name="+value['id']+"a value="+value['id']+"></td><td width='25%'>"+value['employee_id']+"</td><td width='50%'>"+value['first_name']+" "+value['last_name']+"</td><td width=15%>No</td></tr>");
                 
                    // //removing 
                    // $('#potential_participants_table_body_id tr').each(function(){
                    //     console.log($(this).find('td:eq(0)').find(':checkbox').attr('value'));
                    //     if($(this).find('td:eq(0)').find(':checkbox').attr('value')==value['id']){
                    //        $(this).remove(); 
                    //        return false;
                    //     }
                    // });
                    // }
                    // else{
                    // i==0;
                    // }

            });
        
                }
    })
        .catch(function (error) {
        console.log("error"+error);
   });

});


function getProgramDataForCompany(url,import_participant_id,program_name){
    axios.get(url)
        .then(function (response) {

         resetSelect(import_participant_id);
         console.log(response);
         if(Object.keys(response).length > 0){

           $.each(response['data'], function (key, value) { 


            if(program_name!=value.name){
             $('#'+import_participant_id).append("<option value="+value.id+">"+value.name+"</option>");
            }
           });
        }
    })
        .catch(function (error) {
        console.log("error"+error);
   });
  }

  function resetSelect(control_id){
    $('#'+control_id).empty();
    $("#"+control_id).append("<option value='0'>----Select----</option>");
  }


  //////////////////////// select all  logic  \\\\\\\\\\\\\\\\\\\\\\\\\\

  $(document).on('change','#select_all_potential_participants',function(){
  if(this.checked == true){
    $('.potential_participant').each(function(index, el) {
      $(this).prop('checked', true);
    });
  }else{
   $('.potential_participant').each(function(index, el) {
    $(this).prop('checked', false);
  });

 }
});

  $(document).on('change','#select_all_selected_participants',function(){
  if(this.checked == true){
    $('.selected_participant').each(function(index, el) {
      $(this).prop('checked', true);
    });
  }else{
   $('.selected_participant').each(function(index, el) {
    $(this).prop('checked', false);
  });

 }
});

  $(document).on('change','#select_all_potential_coordinators',function(){
  if(this.checked == true){
    $('.potential_coordinators').each(function(index, el) {
      $(this).prop('checked', true);
    });
  }else{
   $('.potential_coordinators').each(function(index, el) {
    $(this).prop('checked', false);
  });

 }
});

  $(document).on('change','#select_all_selected_coordinators',function(){
  if(this.checked == true){
    $('.selected_coordinators').each(function(index, el) {
      $(this).prop('checked', true);
    });
  }else{
   $('.selected_coordinators').each(function(index, el) {
    $(this).prop('checked', false);
  });

 }
});

///////////////////////  approve and unapprove checkbox validations      \\\\\\\\\\\\\\\\\\\\\\\\\\\  

$(document).on('click','#participant_approve_btn',function(e){
      
      e.preventDefault();
      // validation 
      if (!validateParticipantCheck()) {
          //alert('Please select the participants for approving');
          bootbox.alert('Please select the participants for approving');
          return false;
      }   
      // which are checked validations

      approveParticipant();
})

$(document).on('click','#participant_unapprove_btn',function(e){
      
      e.preventDefault();
      // validation 
      if (!validateParticipantCheck()) {
          //alert('Please select the participants for unapproving');
          bootbox.alert('Please select the participants for unapproving');
          return false;
      } 
      unapproveParticipant();
})

$(document).on('click','#participant_save_btn',function(e){
      
      e.preventDefault();
      // validation 
      if (!validateMandatoryParticipants()) {
          return false;
      } 
      addParticipant(); 
})

$(document).on('click','#publish_save_btn',function(e){
      
      e.preventDefault();
      // validation 
      // if (!validateMandatoryParticipants()) {
      //     return false;
      // } 
      publishProgram(); 
})

function validateParticipantCheck() {
     //
     var a=0;
     $('.selected_participant').each(function(index, el) {
            console.log('hii');
            if ($(this).is(":checked")) {
                a=1;
                console.log('hii');
            }
     }); 
     if (a == 1) {
         valid= true;
     }
     else{
         valid=false;
     }
     return valid
}


function approveParticipant(){

  //getting data
  var added_participants=[];
  var approve_participants=[];
  
  $('#selected_participants_table_body_id tr').each(function(){

     var ids=$(this).find('td:eq(0)').find(':checkbox').attr('name');
     added_participants.push(ids);

     if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){
       
       var id=$(this).find('td:eq(0)').find(':checkbox').attr('name');
       approve_participants.push(id);
     }
  });
  
  console.log(added_participants);
  console.log(approve_participants);
  
  var data={};
  data['appr']=approve_participants;
  data['added']=added_participants;
  
  var programId=document.getElementById('program_hidden_program_id').value;

  var e="data="+JSON.stringify(data)+"&program_id="+programId;
  // dat="a="+approve_participants+ "& b="+added_participants;

  var url="/approveParticipants";
  send(url,e);
}

function addParticipant(){
  //
    //getting data
  var added_participants=[];
  
  $('#selected_participants_table_body_id tr').each(function(){

     var ids=$(this).find('td:eq(0)').find(':checkbox').attr('name');
     added_participants.push(ids);
  });
  console.log(added_participants);
  
  var programId=document.getElementById('program_hidden_program_id').value;

  var e="data="+JSON.stringify(added_participants)+"&program_id="+programId;
  var url="/addParticipants";
  send(url,e);
}


function unapproveParticipant(){
  //
  
  var checked_participants = $('.selected_participant:checkbox:checked').map(function() {
    return $(this).attr('name');
  }).get();

  console.log(checked_participants);
  
  var programId=document.getElementById('program_hidden_program_id').value;

  var e="data="+JSON.stringify(checked_participants)+"&program_id="+programId;
  var url="/unapproveParticipants";
  send(url,e);
}

function publishProgram(){
  // publishing programs

    $(".loader").fadeIn();

    var programId=document.getElementById('program_hidden_program_id').value;
    var url="/publishProgram/"+programId;

    axios.get(url)
        .then(function (response) {
        
        var server_data=response['data'];
        console.log(response['data']);
        // redirect and show symbol
        $(".loader").fadeOut('slow');

        if (server_data['publish_status']=='no') {
           //
           //alert('Please Update Topics Named '+server_data['topic_name']);
           bootbox.alert('Please Update Topics Named '+server_data['topic_name']);
        }
        else{
           //alert('program published successufully');
           bootbox.alert('program published successufully');
        }
        
    })
        .catch(function (error) {
        console.log("error"+error);
   });
}



function send(url,data){

  //
    $(".loader").fadeIn();
    axios({
        method: 'post',
        url: url,
        data: data,

       }).then(function(response) {

        var server_data=response['data'];
        console.log(server_data);

        if (url =='/approveParticipants') {
          appendParticipants(server_data,'a');
          
          // for unapprove
          if (server_data['unapprove'] == 'yes') {
             $('#participant_unapprove_btn').show();
          }
          else if (server_data['unapprove'] == 'no'){
             $('#participant_unapprove_btn').hide();
          }

          // checking for publish btn
          if (server_data['publish'] == 'yes') {
             $('#publish_save_btn').show();
          }
          else if (server_data['publish'] == 'no'){
             $('#publish_save_btn').hide();
          }
        }
        else if (url ='/unapproveParticipants') {
          appendParticipants(server_data,'unapprove');
          
          // for unapprove
          if (server_data['unapprove'] == 'yes') {
             $('#participant_unapprove_btn').show();
          }
          else if (server_data['unapprove'] == 'no'){
             $('#participant_unapprove_btn').hide();
          }

          // checking for publish btn
          if (server_data['publish'] == 'yes') {
             $('#publish_save_btn').show();
          }
          else if (server_data['publish'] == 'no'){
             $('#publish_save_btn').hide();
          }
        }
        else if (url ='/addParticipants') {
          appendParticipants(server_data,'a');

          // for unapprove
          if (server_data['unapprove'] == 'yes') {
             $('#participant_unapprove_btn').show();
          }
          else if (server_data['unapprove'] == 'no'){
             $('#participant_unapprove_btn').hide();
          }

          // checking for publish btn
          if (server_data['publish'] == 'yes') {
             $('#publish_save_btn').show();
          }
          else if (server_data['publish'] == 'no'){
             $('#publish_save_btn').hide();
          } 
       }
        
        
        $(".loader").fadeOut('slow');

      }).catch(function (error) {
      console.log("error"+error);
     });
}

function appendParticipants(server_data,flag){
  //
  $('#selected_participants_table_body_id tr').remove();
  $.each(server_data['program_users'], function (key, value) {
     //
     var apprv='No';
     $.each(server_data['program_appr_users'],function(key2,value2){
       if (value['id']==value2) {
           //
           apprv='Yes';
       }
     }) 
     if (!value['employee_id']) {
          value['employee_id']='';
     }

     //appending 
     $('#selected_participants_table_body_id').append("<tr><td width=10%><input type=checkbox class=selected_participant name="+value['id']+" value=''> <input type=hidden  value="+value['userid']+"> </td><td width=25%>"+value['employee_id']+"</td><td width=50%>"+value['first_name']+" "+value['last_name']+"</td><td width=15%>"+apprv+"</td></tr>");
  });
  
  // if (flag == 'unapprove') {
  $('#potential_participants_table_body_id tr').remove();
  $.each(server_data['program_non_users'], function (key, value) {
     // 
     //appending 
     if (!value['employee_id']) {
          value['employee_id']='';
     }

     $('#potential_participants_table_body_id').append("<tr><td width='10%'><input type='checkbox' class=potential_participant name=No value="+value['id']+"> <input type=hidden  value="+value['userid']+"> </td><td width='25%'>"+value['employee_id']+"</td><td width='65%'>"+value['first_name']+" "+value['last_name']+"</td></tr>");
  });
  // } 
}

//****************************** serch participants ************************************\\\
$(document).on('click','#sel_search_btn',function(){
  //
  var userid=$('#sel_userId_input').val();
  userid =userid.trim();
  console.log(userid);
  var element=$("input[value='"+userid+"']");

  if (element.val()) {
      var elementLocation=element.parent().find('input:checkbox').position().top;
      console.log(elementLocation);
      $('#sel_table_wrapper').scrollTop($('#sel_table_wrapper').scrollTop() + elementLocation - 80);
      element.parent().find('input:checkbox').attr('checked',true);
  }else{
      bootbox.alert('Please provide proper userid');
  }
});

$(document).on('click','#pot_search_btn',function(){
  //
  var userid=$('#pot_userId_input').val();
  userid =userid.trim();
  console.log(userid);
  var element=$("input[value='"+userid+"']");

  if (element.val()) {
      var elementLocation=element.parent().find('input:checkbox').position().top;
      console.log(elementLocation);
      $('#pot_table_wrapper').scrollTop($('#pot_table_wrapper').scrollTop() + elementLocation -80);
      element.parent().find('input:checkbox').attr('checked',true);
  }else{
      bootbox.alert('Please provide proper userid');
  }
})