<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/compositeView','vnnogile\CompositeViews\Controllers\CompositeController@index')->name('compositeView');
// Route::get('/compositeView/{route_name}','vnnogile\CompositeViews\Controllers\CompositeController@index')->name('compositeView');
