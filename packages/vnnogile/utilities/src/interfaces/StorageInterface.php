<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\Utilities\Interfaces;

//Custom Imports
use Illuminate\Http\Request;

/**
 * Service StorageInterface
 * 
 */
interface StorageInterface
{
	public function createFile($folder, $file, $contents);
	public function moveFile($fileName, $sourceFolder, $destinationFolder);
	public function deleteFile($file);
	public function deleteDirectory($directory);
	public function uploadFileToPublicStorage(Request $request, $folder);
	public function uploadFileToPrivateStorage(Request $request, $folder);
}
