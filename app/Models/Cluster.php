<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Sofa\Eloquence\Eloquence;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class Cluster
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $customer_party_id
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $competencies
 *
 * @package App\Models
 */
class Cluster extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['name'];
	protected $casts = [
		'customer_party_id' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'customer_party_id',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'customer_party_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function competencies()
	{
		return $this->hasMany(\App\Models\Competency::class);
	}
}
