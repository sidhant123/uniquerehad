/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 var fileList = [];
 var participants = [];
 $(document).ready(function() {
  // $.getJSON('/getEmailData', new Date(), autocompleteForEmail).error(errorResponse);
  $('.multiple_emails').select2({
   tags: true,
   minimumInputLength: 3,
   tokenSeparators: [',', ' ', 'U+0009']
 });
  $('#templates_id').select2({
  });
  $('#summernote').summernote({

    toolbar: [
    ['style', ['style']],
    ['font', ['bold', 'underline', 'clear']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    // ['insert', ['link', 'picture'
    //     //, 'video'
    //     ]],
        ['view', ['fullscreen', 'codeview', 'help']]
        ],
      });


//  $.ajaxSetup({
//   headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//   }
// });
$("#drop_zone").click(function(){
        $(this).next().trigger('click');
    });
});
 $(document).on('change','#templates_id',function(){
  var data = $('#templates_id').select2('data');
  console.log(data[0].id);
  if(data[0].id){
    getDataThroughAxios('/getTemplate/'+ data[0].id);

  }
});
 var postForm = function() {
   var content = $('.note-editable').html();
 }

 $(document).on('click','.note-btn',function(){
   console.log("clear modal-backdrop");
   $(".modal-backdrop").remove();
 });

 $(document).on('click','#clearBtn', function(){
  clearForm();
});
 $(document).on('change','#customer_id',function(){
  var id = $(this).val();
  if(id> 0){
    populateProgramByCustomer(id);
  }
});
 $(document).on('change','#program_id',function(){
  var id = $(this).val();
  if(id> 0){
    populateParicipantsFromProgram(id);
  }
});
 function populateProgramByCustomer(customer_party_id){
  $('#program_id').empty();
  $('#program_id').append('<option value="0">---Select---</option>');
  axios.get(/programsByCustomer/+customer_party_id)
  .then(function (response) {
    console.log(response);
    $.each(response['data'], function (key, value) {
      $('#program_id').append('<option value='+value.id+'>'+value.code+' --- '+value.name+'</option>');
    });
  })
  .catch(function (error) {
    console.log("error"+error);
  });

}
function populateParicipantsFromProgram(program_id){
  $('#program_participant_table_body_id').empty();
  axios.get(/importParticipantsWithPhone/+program_id)
  .then(function (response) {
    console.log(response);
    $.each(response['data'], function (key, value) {
      $('#program_participant_table_body_id').append("<tr><td width='10%'>\
        <input type='checkbox' class='participant_emails' value="+value.phone_number+" name='participant_emails[]'><input type='hidden' value="+value.id+"></td>\
        <td width='90%'>"+value.first_name+" "+value.last_name+"</td></tr>");
    });
  })
  .catch(function (error) {
    console.log("error"+error);
  });

}
$(document).on('change','#select_all_partcipants',function(){
  if(this.checked == true){
    $('.participant_emails').each(function(index, el) {
      $(this).prop('checked', true);
      setParticipantsEmails();
    });
  }else{
   $('.participant_emails').each(function(index, el) {
    $(this).prop('checked', false);
    setParticipantsEmails();
  });

 }
});
$(document).on('change','.participant_emails',function(){
 setParticipantsEmails();
});
$(document).on('click','#saveBtn',function(){
  var html_data = $('#summernote').summernote('code');
  var img_src = getImgSrc();
  var img_name = getImgName();
    console.log(img_name);
  var objData = {};
  objData['html_data'] = html_data;
  objData['img_src'] = img_src;
  objData['img_name'] = img_name;
  var stringifyData = JSON.stringify(objData);
  var formData = new FormData($('#form')[0]);
  formData.append('data', encodeURIComponent(stringifyData));
  formData.append('attachments', fileList);
  formData.append('participants', participants);
  console.log(formData);
  if(true){
    saveHtmlText(formData);
  }

});
//if uploaded by choosing file from browse button
$(document).on('change','#file',function(){
  var fileCount, file = '';
  fileCount = document.getElementById('file').files.length;
  if(fileCount < 5){
      var extensions = ["pdf","xls","docx","xlsx"];
    for (var i = 0; i < fileCount; i++) {
      console.log("FILE OBJECT");
            file = document.getElementById('file').files[i];
            var filename = file.name;
            var filesize = file.size;
            var extenstion = filename.split('.').pop();
            if(extensions.indexOf(extenstion) !== -1 && filesize <= 2000000){
               uploadDocument(file,filename);
             }else{
              bootbox.alert('Only '+extensions.join()+' files with size less than 2 MB  are allowed');
    }
             }
  }else{
    bootbox.alert("You can only upload a maximum of 5 files!");
  }
});


function dropHandler(ev) {
  console.log('File(s) dropped');

  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();

  // if (ev.dataTransfer.items) {
  //   // Use DataTransferItemList interface to access the file(s)

  //   for (var i = 0; i < ev.dataTransfer.items.length; i++) {
  //     // If dropped items aren't files, reject them
  //     if (ev.dataTransfer.items[i].kind === 'file') {
  //       var file = ev.dataTransfer.items[i].getAsFile();
  //       console.log(file);
  //       console.log('... file[' + i + '].name = ' + file.name);

  //       uploadDocument(file,file.name);
  //     }
  //   }
  // } else {
    // Use DataTransfer interface to access the file(s)
    if(ev.dataTransfer.files.length < 5){
      var extensions = ["pdf","xls","docx","xlsx"];
    for (var i = 0; i < ev.dataTransfer.files.length; i++) {
      console.log("FILE OBJECT");
      console.log(ev.dataTransfer.files[i]);
      console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
            var filename = ev.dataTransfer.files[i].name;
            var filesize = ev.dataTransfer.files[i].size;
            var extenstion = filename.split('.').pop();
            if(extensions.indexOf(extenstion) !== -1 && filesize <= 2000000){
               uploadDocument(ev.dataTransfer.files[i],filename);
             }else{
              bootbox.alert('Only '+extensions.join()+' files with size less than 2 MB  are allowed');
    }
             }
  }else{
    bootbox.alert("You can only upload a maximum of 5 files!");
  }
  // }

  // Pass event to removeDragData for cleanup
  removeDragData(ev)
}
function dragOverHandler(ev) {
  console.log('File(s) in drop zone');

  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();
}
function removeDragData(ev) {
  console.log('Removing drag data')

  if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to remove the drag data
    ev.dataTransfer.items.clear();
  } else {
    // Use DataTransfer interface to remove the drag data
    ev.dataTransfer.clearData();
  }
}
function addFileInFileDiv(fileName){
  var fileCount = $('.file_name').length;
  console.log(fileCount);
  console.log(fileName);
  $('.file_list').append('<div><div class="col-xs-9 file_name">'+fileName+'<input type="hidden" value='+fileCount+'></div>\
    <div class="col-xs-3"><div id='+fileName.replace('.','_').replace(/ /g,'')+'_div'+fileCount+'><div class="progress"><div id='+fileName.replace('.','_').replace(/ /g,'')+' class="progress-bar progress-bar-success progress-bar-striped active myprogress" role="progressbar" style="width:0%">0%</div></div></div></div>');
  // $('.file_list').append('<div><div class="col-xs-10 file_name">'+fileName+'<input type="hidden" value='+fileCount+'></div>\
  //   <div class="col-xs-2"><i class="vnn vnn-del remove_file" style="cursor: pointer;"></i></div></div>');
  console.log(fileList);

}
$(document).on('click','.remove_file',function(){
 var file_index = $(this).parent().parent().closest('div').siblings().find('input[type=hidden]').val();
 console.log(fileList[file_index]);
 removeFile(fileList[file_index],file_index,$(this).closest('div').parent().parent());

});
function removeFile(fileName,fileIndex,fileDivObject){
 axios.post('/removeAttachment', {
  fileName: fileName
}).then(function (response) {
  console.log(response);
  fileList.splice(fileIndex,1);
  fileDivObject.remove();
  resetFileSequence();
}).catch(function (error) {
  console.log(error);
});
}
function resetFileSequence(){
  var counter = 0;
  $('.remove_file').each(function(index, el) {
   $(this).parent().parent().closest('div').siblings().find('input[type=hidden]').val(counter);
   counter++;
  });
}
function setParticipantsEmails(){
  var selectParticipantIDs = $("#program_participant_table input:checkbox:checked.participant_emails").map(function(){
    return $(this).val();
  }).get();
  $("#email_to_id").empty();
  console.log(selectParticipantIDs);
  $.each(selectParticipantIDs, function (key, value) {
    $("#email_to_id").append('<option value="'+value+'">'+value+'</option>').val('id').trigger('change');
  });
  $("#email_to_id").val(selectParticipantIDs).select2();

  //get all participants
  participants = $("#program_participant_table input:checkbox:checked.participant_emails").next().map(function(){
    return $(this).val();
  }).get();


}
function getImgName(){
  var img_name = getAttributeValue('data-filename');
  return img_name;
}
function getImgSrc(){
 var img_src = getAttributeValue('src');
 return img_src;
}
function getAttributeValue(attr_name){
  var img_attr = document.getElementsByTagName('img');
  var attr_array = [];
  var count = img_attr.length;
  if(count > 0){
    for(var i = 0;i<count;i++){
      attr_array.push(img_attr[i].getAttribute(attr_name));
    }
  }
  return attr_array;
}
function saveHtmlText(data){
  $('.errorMessage').html("");
  $('.successMessage').html("");
  // $(".loader").fadeIn();
  axios({
    method: 'post',
    url: '/sendSMS',
    data: data,
     success: success,
  });
  // $.ajax({
  //   type: "post",
  //   enctype: 'multipart/form-data',
  //   url: '/sendMail',
  //   data: data,
  //   success: success,
  //   error: error,
  //       //dataType: false,
  //     });
}
function uploadDocument(fileData,fileName) {
  var myform = document.getElementById("form");
  var fd = new FormData(myform);
  var fileCount = $('.file_name').length;
  fd.append('file', fileData);
  addFileInFileDiv(fileName);
  $('#addProgress').append('')
  console.log(fd);
  $.ajax({
    url: '/uploadAttachments',
    data: fd,
    processData: false,
    contentType: false,
    type: 'POST',
    // this part is progress bar
    xhr: function() {
      var xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener("progress", function(evt) {
        if (evt.lengthComputable) {
          $('.progress').show();
          var percentComplete = evt.loaded / evt.total;
          console.log("load percentage"+percentComplete);
          percentComplete = parseInt(percentComplete * 100);
          $('#'+fileName.replace('.','_').replace(/ /g,'')).text(percentComplete + '%');
          $('#'+fileName.replace('.','_').replace(/ /g,'')).css('width', percentComplete + '%');
        }
      }, false);
      return xhr;
    },
    success: function(data) {
      console.log(data['original_name']);
      console.log(data['uploaded_file_path']);
      if (data) {
        fileList.push(data['uploaded_file_path'] + data['original_name']);
        $('#'+fileName.replace('.','_').replace(/ /g,'')+'_div'+ fileCount).html('<i class="vnn vnn-del remove_file" style="cursor: pointer;"></i>');
        console.log('#'+fileName.replace('.','_').replace(/ /g,'')+'_div'+ fileCount);
        // $('.progress').hide();
       // addFileInFileDiv(data['original_name'],data['uploaded_name']);
     }

   }
 });
}
function success(data){
  console.log(data);
  $('.successMessage').html("Mail successfully queued for sending an email.");
  $('.loader').fadeOut('slow');
  clearForm();
}
function error(data){
  $('.errorMessage').html("Some error occurred! Please try again...");
  $('.loader').fadeOut('slow');
}
function autocompleteForEmail(data){
 $("#email_to_id").empty();
 $("#email_cc_id").empty();
 $("#email_bcc_id").empty();
 $.each(data, function (key, value) {
  $("#email_to_id").append("<option value='" + value.email + "'>" + value.email + "</option>");
  $("#email_cc_id").append("<option value='" + value.email + "'>" + value.email + "</option>");
  $("#email_bcc_id").append("<option value='" + value.email + "'>" + value.email + "</option>");
});
}
function errorResponse(){

}
function validateMandatoryFields(){
  var valid = true;
  var to_email = $('#email_to_id').val();
  var summernote = $('.note-editable').val();
  if (to_email == null || to_email === '')
  {
    $('#email_to_id').addClass('fillFields');
    valid = false;
    return valid;
  }
  if(summernote === '' || summernote == null)
  {
    $('.note-editable').addClass('fillFields');
  }
  return valid;
}
function getDataThroughAxios(url){
  axios.get(url)
  .then(function (response) {
    console.log(response);
    getTemplateData(response['data']);
  })
  .catch(function (error) {
    console.log("error"+error);
  });
}
function getTemplateData(template){
  $('#subject').val(template.subject);
  $('#summernote').summernote('code', template.template);
}
function clearForm(){
  $('.note-editable').html('');
  $("#email_to_id").select2("val", " ");
  $('#email_cc_id').select2("val", " ");
  $('#email_bcc_id').select2("val", " ");
  $('#subject').val('');
}
function validatePlaceHoldersFilledOut(){
  var flag = true;
  var subject = $('#subject').val();
  var html_data = $('#summernote').summernote('code');
  var subjectplaceholders = placeholderValidator(subject);
  var summernoteplaceholders = placeholderValidator(html_data);
  var placeholder_array,msg;
  if(Object.keys(subjectplaceholders).length > 0){
   placeholder_array = placeHolders(subjectplaceholders);
   msg = placeholder_array.join();
   $('.errorMessage').html('Please filled out '+msg+' placeholders.');
   flag = false;
 }else{
  $('.errorMessage').html('');
  if(Object.keys(summernoteplaceholders).length > 0){
    placeholder_array = placeHolders(summernoteplaceholders);
    msg = placeholder_array.join();
    $('.errorMessage').html('Please filled out '+msg+' placeholders.');
    flag = false;
  }else{
    $('.errorMessage').html('');
  }
}
return flag;
}
function placeholderValidator(text){
  var found = [],
  rxp = /{([^}]+)}/g,
  curMatch;
  while( curMatch = rxp.exec( text ) ) {
    found.push( curMatch[1] );
  }
  return  found;
}
function placeHolders(placeHolders){
  var placeholders = [];
  for(var ph in placeHolders){
    placeholders.push('{'+ placeHolders[ph] +'}');
  }
  return placeholders;
}
function resetInputField(controlId,text){
  $('#'+controlId).append('<option value="0">'+text+'</option>');
}