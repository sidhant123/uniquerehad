<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class City
 * 
 * @property int $id
 * @property string $name
 * @property int $district_id
 * @property int $state_id
 * @property int $country_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Country $country
 * @property \App\Models\District $district
 * @property \App\Models\State $state
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 * @property \Illuminate\Database\Eloquent\Collection $pincodes
 *
 * @package App\Models
 */
class City extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'district_id' => 'int',
		'state_id' => 'int',
		'country_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'district_id',
		'state_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];


	public function district()
	{
		return $this->belongsTo(\App\Models\District::class);
	}

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class);
	}

	public function pincodes()
	{
		return $this->hasMany(\App\Models\Pincode::class);
	}
}
