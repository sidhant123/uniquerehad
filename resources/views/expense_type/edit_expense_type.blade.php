@extends('expense_type.expense_type')
@section('editID', '/'.$row_data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('edit_btn','hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Expense Type')
@section('name',$row_data->name)
@section('description',$row_data->description)
@section('address',$row_data->track_payment)


@push('scripts')

<script>

 @if($row_data['track_payment']=='Yes'){
     $('#track_payment').attr('checked',true);
  }
 @endif


</script>

@endpush