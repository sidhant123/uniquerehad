<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//Custom Imports
use Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/companies';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
    Custom added Function 
    for after Logout redirect to desired page 
    */
    public function logout(Request $request) {
      Auth::logout();
      return redirect('/userLogIn');
    }



    public function login(Request $request)
    {
        $field = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'userid';
        $request->merge([$field => $request->email]);

        if (auth()->attempt($request->only($field, 'password')))
        {
            return redirect('/companies');
        }

        return redirect('userLogIn')->withErrors([
            'error' => 'Invalid Username or Password.',
        ])->withInput();
        }
    }
