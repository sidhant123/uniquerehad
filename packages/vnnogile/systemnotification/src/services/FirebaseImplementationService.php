<?php

namespace vnnogile\SystemNotification\Services;

    //Custom Imports Below
use App\Models\Configuration;
use Illuminate\Support\Facades\Log;
use vnnogile\Utilities\Services\ConstantsUtility;

    /**
     * This is a a common utility method for email
     *
     * All file upload controllers can use this trait to 
     * upload files to the server
     */
    class FirebaseImplementationService
    {
        /**
         * Send email with or without attachments
         * @param Array $smsData
         * @param Array $masterDataInterface
         * @param Array $utilsInterface
         * @return Array $status
         */
        public static function sendNotification($subject,$message,$deviceToken){
            Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
            //Get the appropriate SMS implementation class
            $firebaseConfigurations = self::retrieveFirebaseConfiguration();
            $firebaseServerKey='';
            $firebaseAPI='';
            foreach($firebaseConfigurations as $firebaseConfiguration){
                switch ($firebaseConfiguration->config_key) {
                    case "firebase_api":
                    $firebaseAPI=$firebaseConfiguration->config_value;
                    break;
                    case "firebase_server_key":
                    $firebaseServerKey=$firebaseConfiguration->config_value;
                    break;
                    default:
                    break;
                }
            }

            // dd($deviceToken);
            //Firebase configuration data
            try{
                
                $msg = array
                        (
                        'body'  => $message,
                        'title' => $subject,
                
                        );

                $fields = array
                        (
                        'to'        => $deviceToken,
                        'notification'  => $msg
                        );
    
    
            $headers = array
            (
                'Authorization: key=' . $firebaseServerKey,
                'Content-Type: application/json'
            );
                $returnValue =  self::invokeFirebaseApi($firebaseAPI,$headers,$fields);
                // dd($returnValue);
            }catch (Exception $e) {
                Log::error('Exception while sending notification :: ', $e->getMessage());
            }        

            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $returnValue;
        }

        private static function invokeFirebaseApi($url,$header,$field){
            Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
            $output = array();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($field));
            $result = curl_exec($ch);
            
            if ($result === FALSE) {
                $output['status'] = false;
                $output['message'] = 'FCM Send Error: ' . curl_error($ch);
                
            }else{
                $output['status'] = true;
                $output['message'] = json_decode($result);
            }
            curl_close($ch);
            Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
            return $output;
        }

        public static function retrieveFirebaseConfiguration(){
            Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
            $configurations = null;
            try{
                $configurations = Configuration::where('config_key', 'like', 'firebase_%')->get();
            }catch (\Exception $exception) {
                Log::error(__LINE__ . 'Exception occurred ' . $exception->getMessage());
            } catch (\Throwable $exception) {
                Log::error(__LINE__ . 'Throwable exception occurred ' . $exception->getMessage());
            }
            
            Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
            return $configurations;
        }


    }
