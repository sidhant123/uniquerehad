INSERT INTO public.menus(
     id, display_name, name, sequence, parent_menu_id, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
  
    (330,'Attendance', 'List Attendance Report', 155, 327,  '', now(), now(),  1, 1);

INSERT INTO public.permissions(
          id,  name, created_at, updated_at,  created_by, updated_by)
    VALUES 
   
     (64,'List Attendance Report', now(), now(), 1, 1);

INSERT INTO public.permission_role(
             role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     ( 1, 64, now(), now(), 1,1),
     ( 2, 64, now(), now(), 1,1);

