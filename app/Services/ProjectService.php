<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\ImportJob;

use App\Models\Project;
use App\Models\Boq;
use App\Models\ProjectUser;
use App\Models\Person;
use App\Models\Organization;
use App\Events\BoqUploadEvent;
use App\Services\DocumentService;

/**
 * Class AddressMasterService
 *
 */
class ProjectService {
	use ValidationTrait;

	public static function getUsers() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return Project::select('id', 'first_name', 'uh_service_id')->get();
	}


	public static function saveData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data) {
			
			$userId = $data['authenticatedUser']->id;
			$obj_project = new Project;
			$obj_project->organization_id = $data['customer_id'];

			$obj_project->name = $data['name'];
			$obj_project->description = $data['description'];
			$obj_project->start_date = $data['start_date'];
			$obj_project->end_date = $data['end_date'];
			$obj_project->site_id = $data['site_name'];
			$obj_project->site_location = $data['site_location'];
			$obj_project->site_engineer_user_id = $data['site_eng_user_id'];
			$obj_project->client_user_id = $data['client_role_id'];
			$obj_project->delay_hr = $data['delay_hr'];
			$obj_project->created_by = $userId;
			$obj_project->updated_by = $userId;
			$obj_project->save();
			$project_id = $obj_project->id;

//---------------------------------------------------------------------
		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
			$data['configurations'] = $configurationsMap;
		if(isset($data['tender_doc'])) {
			self::saveTendorDoc($data,$project_id);	
		}
		if(isset($data['schedule_pdf'])) {
			self::saveSchedulePdf($data,$project_id);	
		}

			//---- saving site engg
		// $site_engg_array = $data['site_eng_user_id'];
		// 	foreach ($site_engg_array as $key => $value) {
		// 		$pusers = new ProjectUser;

		// 		$pusers->project_id = $project_id;
		// 		$pusers->user_id = $value;
		// 		$pusers->role_name = "Site Engineer";
		// 		$pusers->updated_by = $userId;
		// 		$pusers->created_by = $userId;
		// 		$pusers->save();
		// 	}
//----------------------------------------------------------------------------
			return $project_id;
		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}

public static function updateData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
					//$userId = $data['authenticatedUser']->id;
			Project::where('id', $data['id'])->update([
				'organization_id' => $data['customer_id'],
				'name' =>$data['name'],
				'description' => $data['description'],
				'start_date' => $data['start_date'],
				'end_date' => $data['end_date'],
				'site_id' => $data['site_name'],
				'site_location' => $data['site_location'],
				'site_engineer_user_id' => $data['site_eng_user_id'],
				'client_user_id' => $data['client_role_id'],
				'delay_hr' => $data['delay_hr'],
				'updated_by' => $data['authenticatedUser']->id
			]);
		$project_id	=  $data['id'] ;
			//---------------------------------------------------------------------
		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
			$data['configurations'] = $configurationsMap;
		if(isset($data['tender_doc'])) {
			self::saveTendorDoc($data,$project_id);	
		}
		if(isset($data['schedule_pdf'])) {
			self::saveSchedulePdf($data,$project_id);	
		}

			$update_user = self::updateSiteEngg($data['id'], 'Site Engineer');
			return $data['id'];
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}

	public static function deleteProject($id) {
		
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
				$userId = auth()->user()->id;

				$project = Project::where('id', $id);
				$project->update(['deleted_by' => $userId]);
				$check = $project
				->delete();

				$boqs = Boq::where('project_id', $id)->get();

					foreach ($boqs as $key => $value) {
					  $updateBOQ = Boq::where('id', $value['id']);
					  $updateBOQ->update(['deleted_by' => $userId]);  
					  $check1 = $value->delete();
					}
			
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;

	}
	//-------------------- import code

	public static function verifyBoqImportData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
			$project_id = $data['inserted_project_id'];
			$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;

			// $data['tempPath'] = "temp/projects/" . $project_id . "/boq/boq_imports/";
			$data['tempPath'] = "temp/projects/" . $project_id . "/boq/boq_imports/";
			$data['finalPath'] = "public/projects/" . $project_id . "/boq/boq_imports/";

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$class = $data['configurations']['storage_implementation_class'];
			$storageUtility = new $class;
			$updateInfo = $storageUtility->uploadFileToPrivateStorage($data['request'], $data['tempPath']);

			$uploadedFile = $updateInfo['uploadedFileNames']['imported_boq_file'][0];
			$uploadedFile_ogrName = $updateInfo['originalFileNames']['imported_boq_file'][0];
			$fileName = $uploadedFile;
			$folder = storage_path() . "/app/" . $data['tempPath'];

			if (isset($folder)) {
				Log::debug("**************FOLDER*************");
				Log::debug($folder);

				try {
					//carry out verification
					//carry out verification
					$excel = Excel::load($folder . '/' . $fileName);	
					$excelData = $excel->all()->toArray();	
					$sheetNames = $excel->getSheetNames();	
				   if(count($sheetNames)>1){
				   	$importData = $excelData[0];
				   }else {
				   		$importData = $excelData;
				   }
				 $headerRow = array_keys($importData[0]);

				//	dd($compare_result);
// dd($data['route_name']);
				if($data['route_name']=='store'){ 

					$title = array(  0 => "srno", 1 => "description",  2 => "unit",  3 => "qty",  4 => "rate",  5 => "amount",  6 => "service" );
					$compare_result=array_diff($headerRow,$title);
					// dd($compare_result);
             		if(!empty($compare_result))	{
             			$diff_cols = implode(',',$compare_result);
             			if($diff_cols=='0'){
             				$value["errors"] = "Please check excel format column title is not defined or check blank columns.";
             			}else {

             			$value["errors"] = "Please check excel format title column name(s) mismatched : ".$diff_cols ;
             			}
						$invalidBoqs[] = $value;
						return $invalidBoqs;
             		}
               	}
					if($data['route_name']=='update'){
						$title = array(0 =>"boq_no", 1 => "srno",  2 => "description",  3 => "unit",  4 => "qty",  5 => "rate",  6 => "amount",  7 => "service" );
					$compare_result=array_diff($title,$headerRow);
					//dd($compare_result);
             		if(!empty($compare_result))	{
             			$diff_cols = implode(',',$compare_result);
             			if($diff_cols=='0'){
             				$value["errors"] = "Please check excel format column title is not defined or check blank columns.";
             			}else {

             			$value["errors"] = "Please check excel format. Title column name mismatched. :".$diff_cols ;
             			}
						$invalidBoqs[] = $value;
						return $invalidBoqs;
             		}
				}

					// $COLS_TITLE_MISMATCH = true;
					
					// if (in_array("boq_no", $headerRow)) 
					// 	  { $COLS_TITLE_MISMATCH = false; } 
					// 	else
					// 	  { $COLS_TITLE_MISMATCH = true; } 
					// }
					// if($data['route_name']=='update'){	
					//       if (in_array("boq_no", $headerRow)) 
					// 	  { $COLS_TITLE_MISMATCH = true; } 
					// 	else
					// 	  { $COLS_TITLE_MISMATCH = false; } 
					// }
				
					// if($COLS_TITLE_MISMATCH == false){
					// 	return 'COLS_TITLE_MISMATCH';
					// }

					$keys = array();
					$validBoqs = array();
					$invalidBoqs = array();
					$manualErrors = array();
					$possibleDuplicateUserId = false;
					$possibleDuplicateEmailId = false;
					$possibleDuplicatePhotoPath = false;
					$isInvalid = false;
					$startAlphabet = 65;
					$alphabetBucket = [];
					//The following two are used to check if there are duplicate valies in the same file
					//Unique checks work only against records already present in the database
					//Hence we check for uniqueness in the file manually
					$userIdArray = array();
					$emailIdArray = array();
					$photoPathArray = array();
					$isChar=false;
					
					if (!empty($importData)) {
						// abhi - 26/03/19
						//$orgAttribute = OrganizationAttribute::where(['organization_id' => $org['id'], 'attr_name' => 'password_logic'])->first();
						//$passLogic = PasswordLogic::where('id', $orgAttribute['attr_value'])->first();
						foreach ($importData as $key => $value) {

							$isInvalid = false;
							$messageBag = new MessageBag();
							$newBoq = $value;
							$rule = [];
		 

		    				if(ctype_alpha($value['srno'])){
							    $rule = [
									 'srno' => 'required',
									 'description' => 'required|max:3000',
									 'unit' => 'nullable|max:30',
									 'qty' => 'nullable|numeric|max:999999',
									 'rate' => 'nullable|numeric|max:999999.00',
									 'amount' => 'nullable|numeric|max:999999999.00',
									 'service' => 'nullable|max:100'
								];
							} else {
								$rule = [
									 'srno' => 'required',
									 'description' => 'required|max:3000',
									 'unit' => 'required|max:30',
									 'qty' => 'required|numeric|max:999999', 
									 'rate' => 'required|numeric|max:999999.00', 
									 'amount' => 'required|numeric|max:999999999.00', 
									 'service' => 'nullable|max:100'
								];
							}
							$rules = $rule;			
							$validationResult = self::validateInputData($newBoq, $rules);
							//check for duplicate alphabet
							//dd(ctype_alpha($value['srno']));
							if(ctype_alpha($value['srno'])){
								$isChar=true;
							if(!in_array($value['srno'],$alphabetBucket)){
								array_push($alphabetBucket,$value['srno']);
							} else {
								$messageBag->add('srno','This Alphabet already added.');
								$validationResult['success'] == false;
								$isInvalid = true;
							}
						
							}

							if($isChar ==false) {
								$messageBag->add('srno','There should atleast one character to define header row.');
								$validationResult['success'] == false;
								$isInvalid = true;
							}
							/* check alphabet  order to decide header entries
							check for right sequence of alphabet	
							*/
							/*if(isset($value['srno'])){
							if(ctype_alpha($value['srno'])){
						
									if(chr($startAlphabet) != $value['srno']){	
										$messageBag->add('srno','Alphabet order mismatch.');
										$validationResult['success'] == false;
										$isInvalid = true;
									}
									$startAlphabet++;
							}
							} */

						if ($validationResult['success'] == false) {
							if ($isInvalid == true) {
								foreach ($messageBag->getMessages() as $msgKey => $msgValue) {
									$validationResult['errors']->add($msgKey, $msgValue[0]);
								}
							}
							$value["errors"] = $validationResult['errors'];
							$invalidBoqs[] = $value;
							foreach ($value as $key => $val) {
								if (array_key_exists($key, $keys)) {
								} else {
									array_push($keys, $key);
								}
							}
							continue;
						}

						//Finally records did not have errors with validations but internally the file had problems
						if($messageBag->isNotEmpty()){
						$value["errors"] = $messageBag;
						$invalidBoqs[] = $value;
						foreach ($value as $key => $val) {
							if (array_key_exists($key, $keys)) {
							} else {
								array_push($keys, $key);
							}
						}
					}
						} // foreach ends
					} 

					if (count($invalidBoqs) > 0) {

						//deletee already uploaded files
						$storageUtility->deleteFile($folder . $fileName);
						$storageUtility->deleteFile($folder . $uploadedFile);
						$storageUtility->deleteDirectory($folder . 'photos');
						return $invalidBoqs;
					}

				Log::debug("****IMPORT_JOB FOR USERIMPORT**************");
					$importJob = new ImportJob;
					$importJob->entity_name = 'Project';
					$importJob->original_input_file = $data['tempPath'] . $uploadedFile_ogrName;
					$importJob->uploaded_input_file = $data['tempPath'] . $uploadedFile;
					$importJob->status = 'IN_PROGRESS';
					$importJob->created_by = $data['authenticatedUser']->id;
					$importJob->updated_by = $data['authenticatedUser']->id;
					$importJob->save();
					$data['fileName'] = $fileName;
					$data['jobId'] = $importJob->id;
					//Self::sendEmail('UserImportStartEvent', $data);
					Log::debug("****BOQ UploadEvent Before Call**************");
					//event(new UserUploadEvent($folder, $fileName, $data['authenticatedUser']->id, $importJob->id, $data['tempPath'], $data['finalPath'], $uploadedFile));
					event(new BoqUploadEvent($folder, $fileName, $data['authenticatedUser']->id, $importJob->id, $data['tempPath'], $data['finalPath'], $uploadedFile, $data['inserted_project_id'], $data['route_name']));
					Log::debug("BoqUploadEvent Called");
					return 'SUCCESS';
				} catch (ModelNotFoundException $exception) {
					Log::error('Exception occurred in MODEL_NOT_FOUND ' . $exception->getMessage());
					return "MODEL_NOT_FOUND";
				} catch (Exception $exception) {
					Log::error('Exception occurred in ERROR_IN_IMPORT 1 => ' . $exception->getMessage());			
					return "ERROR_IN_IMPORT";

				} catch (\Throwable $exception) {
					Log::error('Throwable exception occurred in ERROR_IN_IMPORT 2 => ' . $exception->getMessage());
					return "ERROR_IN_IMPORT";
				}

			}

		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;

	}

	public static function uploadFile($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->uploadFileToPrivateStorage($data['request'], $data['tempPath']);
	}

	public static function moveFile($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->moveFile($data['uploadedFileName'], $data['tempPath'], $data['finalPath']);
	}

	public static function deleteFile($data, $file) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->deleteFile($file);
	}



	public static function saveTendorDoc($data,$project_id)
	{
		$data['tempPath'] = "/temp/project/" . $project_id . "/tender_doc/";
		$uploadInfo = Self::uploadFile($data);
		
				Log::debug("***********Final File Name**************");
				Log::debug($uploadInfo['uploadedFileNames']['tender_doc'][0]);
				Log::debug($uploadInfo['originalFileNames']['tender_doc'][0]);
				$data['finalPath'] = "/public/project/" . $project_id . "/tender_doc/";
				$data['uploadedFileName'] = $uploadInfo['uploadedFileNames']['tender_doc'][0];
				$data['doc_table_path'] = str_replace("/public", "", $data['finalPath']) . $uploadInfo['uploadedFileNames']['tender_doc'][0];
				$data['entity_id'] = $project_id;
				$data['entity_name'] = 'Project';
				$data['document_type'] = 'tender_doc';
				$data['doc_name'] = $uploadInfo['uploadedFileNames']['tender_doc'][0];
				$success = Self::moveFile($data);
				if ($success == true) {
			$check = DB::transaction(function () use ($data, $project_id) {

		// document table save
				$doc_id = DocumentService::saveDocument($data);
		//--------
			Project::where('id', $project_id)->update([
				'tender_doc' => $doc_id,
				'updated_by' => $data['authenticatedUser']->id
			]);

		});

		}	
				
		}

	public static function saveSchedulePdf($data,$project_id)
	{
		$data['tempPath'] = "/temp/project/" . $project_id . "/schedule_pdf/";
		$uploadInfo = Self::uploadFile($data);
		
				Log::debug("***********Final File Name**************");
				Log::debug($uploadInfo['uploadedFileNames']['schedule_pdf'][0]);
				Log::debug($uploadInfo['originalFileNames']['schedule_pdf'][0]);
				$data['finalPath'] = "/public/project/" . $project_id . "/schedule_pdf/";
				$data['uploadedFileName'] = $uploadInfo['uploadedFileNames']['schedule_pdf'][0];
				$data['doc_table_path'] = str_replace("/public", "", $data['finalPath']) . $uploadInfo['uploadedFileNames']['schedule_pdf'][0];
				$data['entity_id'] = $project_id;
				$data['entity_name'] = 'Project';
				$data['document_type'] = 'project_doc';
				$data['doc_name'] = $uploadInfo['uploadedFileNames']['schedule_pdf'][0];
				$success = Self::moveFile($data);
				if ($success == true) {
			$check = DB::transaction(function () use ($data, $project_id) {

		// document table save
				$doc_id = DocumentService::saveDocument($data);
		//--------
			Project::where('id', $project_id)->update([
				'schedule_pdf' => $doc_id,
				'updated_by' => $data['authenticatedUser']->id
			]);

		});

		}	
				
		}
		
	public static function retrieveProject() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Project::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}
	public static function getProjectUsers($id,$role_name) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = ProjectUser::select('user_id')->where
		([
		    ['role_name', '=', $role_name],
		    ['project_id', '=', $id],
		])
		->get();
		$newdata = array();

		foreach ($data as $key => $value) {
			$newdata[] = $value['user_id'];
		}
		$newdata = implode(',',$newdata);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
 		return $newdata;
	}

	public static function updateSiteEngg($id,$role_name) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = ProjectUser::select('user_id')->where
		([
		    ['role_name', '=', $role_name],
		    ['project_id', '=', $id],
		])
		->get();
		$newdata = array();

		foreach ($data as $key => $value) {
			$newdata[] = $value['user_id'];
		}
		$newdata = implode(',',$newdata);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
 		return $newdata;
	}

	public static function getProjectNamesForUser($person_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		        $people = Person::select('id','party_id')->where('id', $person_id)->first();  
		        $user = User::select('id')->where('party_id', $people['party_id'])->first();  
		        $user_id = $user['id'];

		$data = Project::select('name')->where('client_user_id',$user_id)
		->orWhere('site_engineer_user_id',$user_id)
		->get();
		$project_list="";
		$newdata= array();

		foreach ($data as $key => $value) {
			$newdata[] = $value['name'];
		}
		$project_list = implode(',',$newdata);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
 		return $project_list;
	}
	

 public static function getSiteEnggUsers() {
// get only site engineer user from tenant company
  $tenant_id = 1;
    $org = Organization::find($tenant_id);

        $org_party_id = $org['party_id'];   
        $people = Person::select('id','party_id')->where('customer_party_id', $org_party_id)->get();  
       $cuser = array();
       $count=0;
        foreach ($people as $key => $value) {
           $user_party_id = $value['party_id'];
            $users= User::select('id','first_name', 'email')->whereHas(
                'roles', function($q){
                    $q->where('name', 'Site Engineer');
                }
            )->where('party_id', $user_party_id)->get();
            if(count($users) >0){

            $cuser[$count]['id'] = $users[0]['id'];
            $cuser[$count]['first_name'] = $users[0]['first_name'];
            $cuser[$count]['email'] = $users[0]['email'];
            $count++;
          }
             
        }
        return $cuser;
    }
	}
