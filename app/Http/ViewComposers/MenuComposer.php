<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Http\ViewComposers;

//Custom imports
use App\Services\MenuService;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
/**
 * Class MenuComposer
 * 
 */
class MenuComposer
{

	public function compose(View $view)
	{
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$data['authenticatedUser'] = auth()->user();
		$menuDataItems = MenuService::buildMenu($data);
		$potentialParentMenuItems = array();
		$childrenMenu = array();
		foreach ($menuDataItems as $menuDataItem) {
			if (is_null($menuDataItem->parent_menu_id)){
				$potentialParentMenuItems[$menuDataItem->id] =$menuDataItem;
			}else{
				Log::debug("Parent Menu Id");

				if (array_key_exists($menuDataItem->parent_menu_id, $childrenMenu)){
					Log::debug('IN IF');
					Log::debug($menuDataItem->parent_menu_id);
					Log::debug($menuDataItem->name);
					array_push($childrenMenu[$menuDataItem->parent_menu_id], $menuDataItem);
				}else{
					Log::debug('IN ELSE');
					Log::debug($menuDataItem->parent_menu_id);
					Log::debug($menuDataItem->name);
					$childrenMenu[$menuDataItem->parent_menu_id] = array();
					array_push($childrenMenu[$menuDataItem->parent_menu_id], $menuDataItem);
				}
			}


		}
/*		dump($potentialParentMenuItems);
		dump($childrenMenu);
		exit();*/

		//$view->with('menuItems', MenuService::buildMenu($data));
		$view->with('authenticatedUser', auth()->user());
		$view->with('potentialParentMenuItems', $potentialParentMenuItems);
		$view->with('childrenMenu', $childrenMenu);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}
}
