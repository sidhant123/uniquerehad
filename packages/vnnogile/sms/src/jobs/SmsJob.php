<?php

namespace vnnogile\Sms\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Custom Import Below
use App\Models\CommunicationTrail;
use vnnogile\Sms\Services\NetCoreSmsImplementationService;
use Illuminate\Support\Facades\Log;

class SmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $smsData;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($smsData)
    {
        $this->smsData = $smsData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Extract the template
        //And replace all placeholders
        //With the actual data
        $template = $this->smsData['template_name'];
        $data = $this->smsData['data'];
        
        
        Log::debug("data in SmsJob Handle");
        Log::debug($data);
        foreach($data as $key => $value){
            $template = str_replace('{'.strtoupper($key).'}', $value, $template);
        }
        Log::debug("After replacing the parameters");
        Log::debug($template);
        $template = str_replace('<p>', '', $template);
        $template = str_replace('</p>', '', $template);

        if(isset($this->smsData['trail_data'])){
            $this->smsData['trail_data']['content'] = $template;
        }

        //NetCoreSmsImplementationService::sendSms($this->smsData['to'], $template);
        $transactionId='1';
        $transactionId = NetCoreSmsImplementationService::sendSms($template, $this->smsData['to']);

        if(isset($this->smsData['trail_data'])){
	  $this->smsData['trail_data']['transaction_id'] = $transactionId;
          $trail_data = $this->smsData['trail_data'];  
          SmsSuccessJob::dispatch($trail_data);
        }
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }



    public function failed($exception){
           Log::debug(__CLASS__ . "::" . __METHOD__ . " started---");

           if(isset($this->smsData['trail_data'])){
             $trail_data=$this->smsData['trail_data'];

             $communicationTrail=new CommunicationTrail();
             $communicationTrail->error_code=$exception->getCode();
             $communicationTrail->status="failed";
             $communicationTrail->exception=$exception->getMessage();

             if (array_key_exists('communication_event_id', $trail_data)){
                $communicationTrail->communication_event_id=$trail_data['communication_event_id'];
             }
             
             if (array_key_exists('login_id', $trail_data)){
                $communicationTrail->created_by=$trail_data['login_id'];
                $communicationTrail->updated_by=$trail_data['login_id'];
             }else{
                $communicationTrail->created_by=1;
                $communicationTrail->updated_by=1;
             }

             
             if (array_key_exists('program_code', $trail_data)){
                 $communicationTrail->program_code=$trail_data['program_code'];
             }

             if (array_key_exists('customer_organization_code', $trail_data)){
                 $communicationTrail->customer_organization_code=$trail_data['customer_organization_code'];
             }
            
             if (array_key_exists('tenant_code', $trail_data)){
                $communicationTrail->tenant_code=$trail_data['tenant_code'];
             }

              if (array_key_exists('content', $trail_data)){
                $communicationTrail->content=$trail_data['content'];
             }
           
             if (array_key_exists('job_id', $trail_data)){
                 $communicationTrail->job_id=$trail_data['job_id'];
             }

             if (array_key_exists('transaction_id', $trail_data)){
                 $communicationTrail->transaction_id=$trail_data['transaction_id'];
             }

             if (array_key_exists('party_id', $trail_data)){
                 $communicationTrail->party_id=$trail_data['party_id'];
             }
             $communicationTrail->save();
             $ct_id=$communicationTrail->id;
           }
           Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
     }   
}
