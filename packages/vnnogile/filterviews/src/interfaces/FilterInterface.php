<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\FilterViews\Interfaces;

/**
 * Service ServiceInterface
 *
 */
interface FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array());
}
