INSERT INTO public.menus(
     id, display_name, name, sequence, parent_menu_id, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (335,'Material In Out', 'List Inward Report', 180, 327,  '', now(), now(),  1, 1);

INSERT INTO public.permissions(
          id,  name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (69,'List Inward Report', now(), now(), 1, 1);
    

INSERT INTO public.permission_role(
             role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     ( 1, 69, now(), now(), 1,1),
     ( 2, 69, now(), now(), 1,1);


