<?php

namespace vnnogile\Cleanups\Commands;

use Illuminate\Console\Command;

//Custom Imports
use Illuminate\Support\Facades\Log;
use vnnogile\Cleanups\Services\CleanupService;

class CleanupCommunicationTrailsCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:communication_trails  {tenantCode}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup communication trail records';

    /**
     * The assessment service.
     *
     * @var CleanupService $cleanupService
     */
    protected $cleanupService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CleanupService $cleanupService)
    {
        parent::__construct();
        $this->cleanupService = $cleanupService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        //Retrieve the parameters of the command
        $tenantCode = $this->argument('tenantCode');
        //Invoke the cleanupCommunicationTrails from the CleanupService
        $this->cleanupService->cleanupCommunicationTrails($tenantCode);

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
