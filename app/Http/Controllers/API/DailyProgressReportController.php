<?php

namespace App\Http\Controllers\API;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAuthenticate;
use App\Repositories\Badge\BadgeRepositoryInterface;
use App\Repositories\Common\CommonRepositoryInterface;
use App\Repositories\HelpDesk\HelpDeskRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\Program\ProgramRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Boq;
use App\Models\BoqItem;
use App\Services\API\DailyProgressReportService;



class DailyProgressReportController extends Controller {
	

	public function getBoqHeaderList($name){

		Log::error(strlen($name));
		$len = strlen($name);
		if ($len >=5) {
			$res = DailyProgressReportService::getBoqHeaderList($name);
				Log::error($res);
				return $res;
		} else {
			Log::error('input is too short');
			return false;
		}

		return $res;
	}

	public function getBoqDetailsList($id){

		Log::error($id);
		if ($id > 0) {
		$res = DailyProgressReportService::getBoqDetailsList($id);
			return $res;
		} else {
			return "Please select an valid boq item";
		}



	}
}
