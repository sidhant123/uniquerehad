<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Tag
 * 
 * @property int $id
 * @property string $entity_name
 * @property int $entity_id
 * @property array $tags
 * @property string $tags_virtual
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Tag extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'entity_id' => 'int',
		'tags' => 'json',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'entity_name',
		'entity_id',
		'tags',
		'tags_virtual',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}
}
