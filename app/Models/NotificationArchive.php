<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 08 Aug 2018 01:42:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificationArchive
 * 
 * @property int $id
 * @property string $notification
 * @property int $is_read
 * @property int $tenant_party_id
 * @property int $party_id
 * @property string $video_call_url
 * @property int $is_video_call
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class NotificationArchive extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'is_read' => 'int',
		'tenant_party_id' => 'int',
		'party_id' => 'int',
		'is_video_call' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'notification',
		'is_read',
		'tenant_party_id',
		'party_id',
		'video_call_url',
		'is_video_call',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class);
	}
}
