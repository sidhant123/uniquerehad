<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use Illuminate\Support\Facades\Log;

/**
 * Class TenantService
 * 
 */
class AxiosService
{

     public static function convertAxiosData($data){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $actualData=array();
        foreach ($data  as $key => $value) { 
            $actualData[$value['name']] = $value['value'];
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $actualData;
    }
}
