@extends('layouts.base')
@section('content')

@include('errors.error_validation')
  <form style="border-style:dotted; border-width: 1px;" action="/manpowerContracts{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFieldsForManpowerContract()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/manpowerContracts'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Manpower Contract

      </h3></div>
    </div>

    <div class="row">
      <div class="col-lg-6">
          <div class="form-group required">
              <label for="organization_id@yield('org_name_d')">{{ __('labels.mn_contract.org_name') }}</label>
                <select  class="form-control mandatory" name="organization_id"
               id="organization_id"@yield('org_name_d') required>
                  @if(isset($organization))
                  <option value="0">--select--</option>'
                  @foreach($organization as $org_val)
                  @if(isset($sel_organization_id))
                  @if($org_val->id === $sel_organization_id)
                  <option value="{{$org_val->id}}" selected>{{$org_val->name}}</option>
                  @else
                  <option value="{{$org_val->id}}">{{$org_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$org_val->id}}">{{$org_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>     

       </div>
    <div class="col-lg-6">
    <div class="form-group required">
              <label for="project_id@yield('project_name_d')">{{ __('labels.mn_contract.project_name') }}</label>
              <select  class="form-control mandatory" name="project_id"
               id="project_id" @yield('project_name_d')>
                  @if(isset($project))
                  <option value="0">--select--</option>'
                  @foreach($project as $project_val)
                  @if(isset($sel_project_id))
                  @if($project_val->id === $sel_project_id)
                  <option value="{{$project_val->id}}" selected>{{$project_val->name}}</option>
                  @else
                  <option value="{{$project_val->id}}">{{$project_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$project_val->id}}">{{$project_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
       </div>
  </div>
    
  </div>


   <div class="row">
     <div class="col-lg-6">
       <div class="form-group required"> 
           <label for="service_name">{{__('labels.mn_contract.service_name')}}</label>
             <select  class="form-control mandatory" name="service_name" id="service_name" 
               @yield('service_name_d')>
                  <option value="0">--select--</option>'
                  @if(isset($service))
                  @foreach($service as $service_val)
                  @if(isset($sel_service))
                  @if($service_val->activity_name == $sel_service)
                  <option value="{{$service_val->activity_name}}" selected>{{$service_val->activity_name}}</option>
                  @else
                  <option value="{{$service_val->activity_name}}">{{$service_val->activity_name}}</option>
                  @endif
                  @else
                  <option value="{{$service_val->activity_name}}">{{$service_val->activity_name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
      </div>  
     </div>
   <div class="col-lg-6">
        <div class="form-group required">
           <label for="rate">{{__('labels.mn_contract.rate')}}</label>
            @if(!empty(old('rate')))
            <input type="number" id="rate" step=".01" min='1' class="form-control mandatory" name="rate" value="{{ old('rate') }}"   @yield('rate_d')>
            @else
            <input type="number" id="rate" step="0.01" min="1" class="form-control mandatory" name="rate" value="@yield('rate')" @yield('rate_d') >
            @endif

         </div>
   </div>
</div>

 <div class="row">
   <div class="col-lg-6 form-group required">
        <label for="compnay_id">Status</label>
        <div class="row" style="font-weight: bold;">
            <div class="col-lg-3">
             <input type="radio" name="status" id="active" value="Active" checked="checked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active
            </div>
            <div class="col-lg-6">
             <input type="radio" name="status" id="inactive" value="Inactive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inactive
            </div>
        </div>
     </div>
</div>
<p></p>

<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.mn_contract.submit')}}</button>
</div>
</div>
<p></p>

<!-- for import user -->
@section('importBoq')
@show
@endsection

@push('scripts')

<p></p>

<p></p>

</div>
</form> 

<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}

/*select:not(:placeholder-shown) {
  border-color: hsl(0, 76%, 50%);;
}
*/

</style>

<script>
$(document).ready(function(){

$('#organization_id').select2({
  }); 
$('#project_id').select2({
  });
$('#service_name').select2({
  });

});



$("#project_id").on("change", function() {
      // alert('ok');
      project_id = $("#project_id").val();
      console.log(project_id)
      if(project_id !=0) {
      // get_service_url = APP.'getCompanyName/'+formData;
        get_service_url= "{{url('/')}}"+"/getServiceList/"+project_id
        console.log(get_service_url)
        $.ajax({
          url: get_service_url ,
          method: 'get',
          type: 'JSON',
          success: function(response) {
            console.log('This is response')
            console.log(response)
            $('#service_name option').remove();
            $('#service_name').append("<option value=''>--Select--</option>");
            console.log($.type(response))
            for (var i = 0; i < response.length; i++) {
              console.log("activity_name LIST READING = "+response[i]['activity_name']);
              $('#service_name').append("<option id='service_nameOptions+"+response[i]['activity_name']+"' value="+response[i]['activity_name']+">"
                +response[i]['activity_name']+"</option>");
            }
          },
          error: function() {
            alert('Error : There no Service');
          }
        })
      }else {
          $('#service_name option').remove();

      }
    })


// var rate = $('#rate').val();

// alert(rate);
// if (parseFloat(rate) == 0.0) {

//   rate = 1;

// }


function validateMandatoryFieldsForManpowerContract(){
  var valid = true;
  valid=validateMandatoryFields();
        var organization_id = $('#organization_id').val();
        var project_id = $('#project_id').val();
        var service_name = $('#service_name').val();
  if(organization_id == 0) {
document.getElementById("select2-organization_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-organization_id-container").style.border = "0px solid red";
}
  if(project_id == 0) {
document.getElementById("select2-project_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-project_id-container").style.border = "0px solid red";
}

 if(service_name == 0) {
document.getElementById("select2-service_name-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-service_name-container").style.border = "0px solid red";
}

  var rate = $('#rate').val();
  console.log(rate);
  if(valid){
  var res = validaterate(rate);
  }
     if(!res){
      return false;
     } else {
      // alert('in else');
      return true;
     }
  //    else {

  //   var phone = $('#primaryContact').val();
  //   var contactRes = validatecontact(phone);
  //        if(!contactRes){
  //     return false;
  //    } else {

     return valid;
  //    }
  // }
}

function validaterate(rate){
  // alert(rate)
   if (rate == 0) {
     alert('The rate is less than zero, please enter a valid rate');
   } else {
    // alert('rate is proper');
    return true;
   }
 }



</script>
@endpush


