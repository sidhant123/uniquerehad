
update list_view_masters 
set query_columns='project_name,company_name,expense_type,expense_date,service_name,amount,amount_paid,id',
heading_columns = 'Project,Expense to Company,Expense Type,Expense Date,Service,Total Amount,Paid Amount,Action',
display_css_widths = 'col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-1'
where id = 13