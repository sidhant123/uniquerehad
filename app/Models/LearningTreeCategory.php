<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class LearningTreeCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $learning_trees
 *
 * @package App\Models
 */
class LearningTreeCategory extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function learning_trees()
	{
		return $this->belongsToMany(\App\Models\LearningTree::class, 'learning_tree_category_learning_tree', 'learning_tree_categories_id', 'learning_trees_id')
					->withPivot('id', 'deleted_at', 'created_by', 'updated_by', 'deleted_by')
					->withTimestamps();
	}
}
