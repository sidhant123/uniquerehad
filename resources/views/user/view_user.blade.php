@extends('user.user')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $data->id)

@section('organization_name', $org_id)

@section('firstName', $data->first_name)
@section('lastName',$data->last_name)
@section('employeeId',$data->employee_id)
@section('department',$data->department)

@section('designation',$data->designation)
@section('email',$data->email)
@section('linkedinProfile',$data->linkedin_profile)
@section('fbProfile',$data->facebook_id)
@section('twitterHandle',$data->twitter_handle)


@section('primaryContact',trim($contacts[0]['phone_number']))

@if(isset($contacts[1]['phone_number']))  
  @section('secondaryContact',trim($contacts[1]['phone_number']))
@endif

@section('personalEmail',$data->personal_email)
@section('userid',$data->userid)
@section('old_userId',$data->userid)
@section('googlePlus',$data->google_profile)

<!-- abhi 26/03/19 -->
@section('dob',$data->dob)

@section('edit_btn_link','/users/'.$data->id.'/edit')


@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('.form-control').attr('readonly',true);
});


@if ($data['gender'] == 'M') {
     $("[name=gender]").val(["M"]);
}
@else{
     $("[name=gender]").val(["F"]);
}
@endif

//abhi 26/03/19
@if ($data['is_private'] == 1) {
     $("[name=profile]").val(["private"]);
}
@else{
     $("[name=profile]").val(["public"]);
}
@endif

$('#photo_div_id').removeClass('required');

///// For facilitator
 @if($facilitator==1){
     $('#facilitator_check_id').attr('checked',true);
  }
 @endif
</script>
</script>

@endpush


