<?php

namespace App\Services;

use App\Models\LearningTheme;
use Illuminate\Support\Facades\Log;
use DB;
use Auth;
use Session;

class LearningThemeService {

	public static function saveLearningTheme($data) {
		// dd($data);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {		
			// dd($data['customer_party_id']);
	        Session::flash('status', __('labels.flash_messages.store'));
	        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	        $LearningTheme = new LearningTheme();
	        $LearningTheme->customer_party_id = $data['customer_party_id'];
	        $LearningTheme->name = $data['learningThemeName'];
	        $LearningTheme->master_program_id = $data['masterProgramName'];
	        $LearningTheme->description = $data['master_program_description'];
	        $LearningTheme->start_date = date('Y-m-d', strtotime($data['startDate']));
	        $LearningTheme->end_date = date('Y-m-d', strtotime($data['endDate']));
	        $LearningTheme->created_by = Auth::user()->id;
	        $LearningTheme->updated_by = Auth::user()->id;
	        // $MasterProgram->deleted_by = Auth::user()->id;
	        $LearningTheme->deleted_at = null;
	        $LearningTheme->save();
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return True;
		}
		catch(Exception $e) {
			return "Some error occured on server side!";
		}
	}

	public static function updateLearningTheme($data, $id) {
		// dd($data);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {		
			// dd($data);
	        Session::flash('status', __('labels.flash_messages.store'));
	        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	        $MasterProgram = DB::table('learning_themes')->where('id', $id);
	        // dd($data['customer_party_id']);
	        // $MasterProgram = new MasterProgram();

	        DB::table('learning_themes')->where('id', $id)->update(
	        	['customer_party_id'=> $data['customer_party_id'],
	        	 'name'=>$data['learningThemeName'],
	        	 'master_program_id'=>$data['masterProgramName'],
	        	 'description'=>$data['master_program_description'],
	        	 'start_date' => date('Y-m-d', strtotime($data['startDate'])),
	        	 'end_date' => date('Y-m-d', strtotime($data['endDate'])),
	        	 'created_by'=> Auth::user()->id,
	        	 'updated_by'=> Auth::user()->id,
	        	 // 'deleted_by'=> Auth::user()->id,
	        ]);
	        // dd('J');
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			// dd('j');
			return True;
		}
		catch(Exception $e) {
			return "Some error occured on server side!";
		}
	}

}
