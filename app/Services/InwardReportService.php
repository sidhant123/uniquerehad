<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Project;
use App\Models\ExpenseType;

/**
 * Class AddressMasterService
 *
 */
class InwardReportService {
	
	  public static function retrieveInwardReportQuery($project_id,$start_date,$end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';
        $queryBuilder = DB::table('material_inwards_headers')
            ->select(
                'projects.name as project_name' , 
                 DB::raw("material_inwards_details.created_date::Date as d_date"),   
                 'materials.name as material_name',
                // 'material_inwards_details.status' ,  
                'material_inwards_details.quantity' ,  
                'material_inwards_details.price'  
        )
            ->leftJoin('material_inwards_details', 'material_inwards_headers.id', '=', 'material_inwards_details.material_inwards_header_id')
            ->leftJoin('projects', 'projects.id', '=', 'material_inwards_headers.project_id')
            ->leftJoin('materials', 'materials.id', '=', 'material_inwards_details.material_id')
             ->where('material_inwards_headers.is_active', 1)
             ->where('material_inwards_details.is_active', 1)
            ->where('material_inwards_headers.project_id','=', $project_id)
            ->whereBetween('material_inwards_details.created_date', [$start_date, $end_date])
            ->orderBy('d_date','asc')
            ->orderBy('material_inwards_details.material_id', 'asc');

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    } 


     public static function retrieveTransferReportQuery($project_id,$start_date,$end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';
        $queryBuilder = DB::table('material_transfer_headers')
            ->select(
                'projects.name as project_name' , 
                 DB::raw("material_transfer_details.created_date::Date as d_date"),   
                 'materials.name as material_name',
                // 'material_transfer_details.status' ,  
                'material_transfer_details.quantity' ,  
                'material_transfer_details.price' 
                // 'material_transfer_details.gst_amount'   
        )
            ->leftJoin('material_transfer_details', 'material_transfer_headers.id', '=', 'material_transfer_details.material_transfer_header_id')
            ->leftJoin('projects', 'projects.id', '=', 'material_transfer_headers.project_id')
            ->leftJoin('materials', 'materials.id', '=', 'material_transfer_details.material_id')
             ->where('material_transfer_headers.is_active', 1)
             ->where('material_transfer_details.is_active', 1)
            ->where('material_transfer_headers.project_id','=', $project_id)
            ->whereBetween('material_transfer_details.created_date', [$start_date, $end_date])
            ->orderBy('d_date','asc')
            ->orderBy('material_transfer_details.material_id', 'asc');

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

     public static function retrieveConsumptionReportQuery($project_id,$start_date,$end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';
        $queryBuilder = DB::table('material_consumption_headers')
            ->select(
                'projects.name as project_name' , 
                 DB::raw("material_consumption_details.created_date::Date as d_date"),   
                 'materials.name as material_name',
                // 'material_consumption_details.status' ,  
                'material_consumption_details.quantity' ,  
                'material_consumption_details.price' 
        )
            ->leftJoin('material_consumption_details', 'material_consumption_headers.id', '=', 'material_consumption_details.material_consumption_header_id')
            ->leftJoin('projects', 'projects.id', '=', 'material_consumption_headers.project_id')
            ->leftJoin('materials', 'materials.id', '=', 'material_consumption_details.material_id')
             ->where('material_consumption_headers.is_active', 1)
             ->where('material_consumption_details.is_active', 1)
            ->where('material_consumption_headers.project_id','=', $project_id)
            ->whereBetween('material_consumption_details.created_date', [$start_date, $end_date])
            ->orderBy('d_date','asc')
            ->orderBy('material_consumption_details.material_id', 'asc');

        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

    public static function getProjectName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $project = Project::select('name')->where('id', '=', $Id)->first();
        return trim($project->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
     
}
