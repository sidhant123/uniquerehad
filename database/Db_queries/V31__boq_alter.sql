
-- V31__boq_alter.sql


INSERT INTO public.validations(
             entity_name, operation, field_name, validation_rule, active, created_at, updated_at, created_by, updated_by)
    VALUES 
     ('ExpenseType', 'Update', 'name', 'required|max:255|unique:expense_types,name,$name,id,deleted_at,NULL', 1, now(), now(), 1, 1),
     ('ExpenseType', 'Create', 'name', 'required|max:255|unique:expense_types,name,NULL,id,deleted_at,NULL', 1, now(), now(), 1, 1);


