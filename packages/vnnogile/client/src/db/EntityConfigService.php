<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\Client\Db;

//Custom imports
use Cache;
use DB;
use App\Models\EntityConfiguration;
use Illuminate\Support\Facades\Log;
/**
 * Class ConfigurationService
 * 
 */
class EntityConfigService
{

	public static function loadEntityConfiguration()
	{
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$cacheKey = config('data.config_cache_key');
		$duration = config('data.config_cache_duration');
		$records = Cache::rememberForever($cacheKey, function () {
		    	return EntityConfiguration::select('id', 'entity_name as entityName', 'entity_namespace as entityNamespace', 'cache_name as cacheName', 'cache_duration as duration', 'cache_tag_name as cacheTagName')->get();
		});	
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		Log::debug('records');
		Log::debug($records);
		return $records;
	}

	public static function retrieveEntityConfiguration($entity)
	{
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
 		$records =  self::loadEntityConfiguration();
		$data = array();
		foreach ($records as $record) {
			if ($record->entityName == $entity){
				$data['entityName'] = $record->entityName;
				$data['entityNamespace'] = $record->entityNamespace;
				$data['cacheKey'] = $record->cacheName;
				$data['cacheDuration'] = $record->duration;
				$data['cacheTagName'] = $record->cacheTagName;
				break;
			}
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $data;
	}
}
