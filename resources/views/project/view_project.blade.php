@extends('project.project')
@section('title','View Project')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $row_data->id)

@section('name',$row_data->name)
@section('description',$row_data->description)
@section('start_date',$row_data->start_date)
@section('end_date',$row_data->end_date)
@section('site_name',$row_data->site_id)
@section('delay_hr',$row_data->delay_hr)
@section('tender_doc_id',$row_data->tender_doc_path)
@section('schedule_pdf_id',$row_data->schedule_pdf_path)
@section('importBoq')
      @if(count($boq_list)>1)

 
{{-- <div style="border-style:dotted; border-width: 1px;">
    {{ csrf_field() }}
  <h3 class="headings">{{__('labels.projects.edit_import_boq')}}</h3>
  <div class="row">
     <div class="col-xs-12 req">
      <div class="form-group required">
          <label for="boqFile">{{__('labels.projects.edit_file')}}</label>
          <input type="file" class="form-control import"  id="boqFile"  name="imported_boq_file" accept=".xlsx" >
        </div>
   
    <div class="form-group">
      
    <button class="btn btn-primary">
    <a href="{{ url('/projects/' . $row_data->id . '/exportBoq') }}" >{{__('labels.projects.export_file')}}</a></button>

  </div>
</div>
</div>
</div>
 --}}
<p></p> 




  <h4 class="headings">{{__('labels.projects.import_boq_list')}} &emsp; 
  </h4>
<div id="boq_list">
<table id="boq_table" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>BOQ No.</th>
                <th>SrNo.</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Activity</th> 
       
            </tr>
        </thead>
        <tbody>
        @foreach($boq_list as $boq_val)
        <tr>
         <td>{{$boq_val->name}}</td>
         <td>{{$boq_val->srno}}</td>
          <td>{{$boq_val->description}}</td>
          <td>{{$boq_val->unit_rate}}</td>
          <td>{{$boq_val->quantity}}</td>
          <td>{{$boq_val->rate}}</td>
          <td>{{$boq_val->amount}}</td>
          <td>{{$boq_val->activity_name}}</td>
         
        <tr>
     @endforeach
        </tbody>
      </table>
</div>

         @endif
@endsection


@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('#customer_id').prop('disabled', true);
  $('#site_id').prop('disabled', true);
  $('#client_role_id').prop('disabled', true);
  $('#site_eng_user_id').prop('disabled', true);
  $('.form-control').attr('readonly',true);
  $("#site_eng_user_id").val([{{$site_engineer_user_id}}]).trigger("change");

});
</script>

@endpush
