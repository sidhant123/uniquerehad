<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Site;

/**
 * Class AddressMasterService
 *
 */
class AttandanceReportService {
	
	  public static function retrieveAttandanceReportQuery($start_date, $end_date){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

    $start_date = $start_date. ' 00:00:01';
    $end_date = $end_date . ' 23:59:59';
        $query = "select    temp.first_name, temp.last_name, count(first_name) as no_of_days from 
  (select users.first_name, users.last_name, case when  count(attendance_documents.created_by) >=1 
  then 1 else 0 end as no_of_days
  from attendance_documents 
  left join users on users.id = attendance_documents.created_by 
  where attendance_documents.is_active = 1 and attendance_documents.created_at between '".$start_date."' and '".$end_date."'
  group by users.first_name, users.last_name, attendance_documents.created_at::Date
  order by  users.first_name asc) as temp group by temp.first_name,  temp.last_name
";
      // dd($query);  
        $query = '(' . $query . ') somealias';
        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $query;
    }



  public static function retrieveAttandanceDetailReportQuery($start_date, $end_date,$user_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    $start_date = $start_date. ' 00:00:01';
    $end_date = $end_date . ' 23:59:59';
          
            $queryBuilder = DB::table('attendance_documents')
            ->select(
                 DB::raw("CONCAT(users.first_name,' ',users.last_name) AS name"),
                 DB::raw("attendance_documents.created_at::Date as d_date"),
                 DB::raw("attendance_documents.created_at::Time as t_time"),
                 'projects.name as project_name',
                 'attendance_documents.location'

            )
            ->leftJoin('users', 'users.id', '=', 'attendance_documents.created_by')
            ->leftJoin('projects', 'projects.id', '=', 'attendance_documents.project_id')
             ->where('attendance_documents.is_active', 1)
            ->whereBetween('attendance_documents.created_at', [$start_date, $end_date])
            ->orderBy('d_date','asc')
            ->orderBy('users.first_name', 'asc');
            if($user_id>0){
               $queryBuilder->where('attendance_documents.created_by', $user_id);
            }
        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }



    public static function getSiteName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $site = Site::select('name')->where('id', '=', $Id)->first();
        return trim($site->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
