<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class RoleUser
 * 
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Role $role
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class RoleUser extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'role_user';

	protected $casts = [
		'user_id' => 'int',
		'role_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'user_id',
		'role_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}
}
