@extends('layouts.logInBase')
@section('content')
    <section id="login">
    <div class="container">
        <h2>Back Office Login</h2>
        <div class="box">
             <div style="margin-bottom: 250px; margin-left: 5px;">
               <a class="navbar-brand" href="/userLogIn"><img src="/img/company_logo.png" alt="logo" width="290" height="250" style="padding: 0px;">
                </a>
            </div>

            <form method="post" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" id="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    <span>User Id / Email</span>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>




                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required>
                    <span>Password</span>
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>
        <div class="form-group">
                    @if ($errors->has('error'))
                                    <p style="color:red;">
                                        <strong>{{ $errors->first('error') }}</strong>
                                    </p>
                                @endif
                </div>



                <div class="form-group checkbox">
                     <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    <!-- <input type="checkbox">Remember Me -->
                </div>


               <!-- <a href="/forgotPassword" class="pull-right">Forgot Password?</a> -->
                <div class="clearfix"></div>
                <button type="submit" class="share-btn">Login</button>
                <div class="clearfix"></div>
            </form>
            <p>By login in, you agree to the <a href="#">Terms & Conditions</a> Unique Rehab Pvt Ltd.</p>
        </div>
    </div>
</section>
@endsection


