<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;


use App\Models\Material;
use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class MaterialService {
	use ValidationTrait;

	public static function retrieveMaterials() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Material::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

public static function saveMaterial($data) {

	// Log::error($data);
	// exit();

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data) {
		
			$userId = $data['authenticatedUser']->id;

			$obj = new Material;
			$obj->name = $data['name'];
			$obj->code = $data['code'];
			$obj->unit = $data['unit'];
			$obj->supplier_id = $data['supplier_id'];
    		$obj->created_by = $userId;
			$obj->updated_by = $userId;

			$obj->save();
			$material_id = $obj->id;
			return $material_id;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	
public static function updateMaterial($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
					//$userId = $data['authenticatedUser']->id;
			Material::where('id', $data['id'])->update([
				'name' => $data['name'],
				'code' => $data['code'],
				'unit' => $data['unit'],
				'supplier_id' => $data['supplier_id'],
				'updated_by' => $data['authenticatedUser']->id
			]);

			return $data['id'];
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
public static function deleteMaterial($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$material = Material::where('id', $id);
			//$user = self::validateActiveMaterial($material);

			//if ($user) {
				$userId = auth()->user()->id;

				$material = Material::where('id', $id);
				$material->update(['deleted_by' => $userId]);
				$check = $material->delete();
			//} else {
			//	$check = false;
			//}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	private static function validateActiveMaterial($material) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$material = $material->first();
		Log::debug("********* VALIDATE ACTIVE Material 1".$material);
		Log::debug("********* VALIDATE ACTIVE Material 2".$material->id);


		$userObject = Project::where('material_id', $material->id);
		Log::debug("********* VALIDATE ACTIVE Material 3".$userObject);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		if ($userObject->first()->material_id == $material->id) {
			return false;
		} else {
			return $userObject;
		}
	}

	//-------------------- import code

	public static function verifyMaterialImportData($data,$sup_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data,$sup_id) {
			$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;

			// $data['tempPath'] = "temp/projects/" . $project_id . "/material/material_imports/";
			$data['tempPath'] = "temp/material/material_imports/";
			$data['finalPath'] = "public/material/material_imports/";

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$class = $data['configurations']['storage_implementation_class'];
			$storageUtility = new $class;
//dd($data);
			$updateInfo = $storageUtility->uploadFileToPrivateStorage($data['request'], $data['tempPath']);

			//dd($updateInfo);
			$uploadedFile = $updateInfo['uploadedFileNames']['imported_material_file'][0];
			$uploadedFile_ogrName = $updateInfo['originalFileNames']['imported_material_file'][0];
			$fileName = $uploadedFile;
			$folder = storage_path() . "/app/" . $data['tempPath'];
			
			if (isset($folder)) {
				Log::debug("**************FOLDER*************");
				Log::debug($folder);

				try {
					//carry out verification
				//carry out verification
					$excel = Excel::load($folder . '/' . $fileName);	
					$excelData = $excel->all()->toArray();	
					$sheetNames = $excel->getSheetNames();	
				   if(count($sheetNames)>1){
				   	$importData = $excelData[0];
				   }else {
				   		$importData = $excelData;
				   }				   
				
					// $validationRuleService = new ValidationRulesService($data['masterDataInterface'], $data['utilsInterface']);
					$keys = array();
					$validUsers = array();
					$invalidUsers = array();
					$manualErrors = array();
					$possibleDuplicateUserId = false;
					$possibleDuplicateEmailId = false;
					$possibleDuplicatePhotoPath = false;
					$isInvalid = false;
					//The following two are used to check if there are duplicate valies in the same file
					//Unique checks work only against records already present in the database
					//Hence we check for uniqueness in the file manually
					$userIdArray = array();
					$emailIdArray = array();
					$photoPathArray = array();
					if (!empty($importData)) {
						
						foreach ($importData as $key => $value) {
							
		           $value['item'] = ucwords(strtolower(trim($value['item'])));
		           $value['code'] = strtoupper(trim($value['code']));
       
							
							$isInvalid = false;
							$messageBag = new MessageBag();
							$newUser = $value;
							$rules = [
								'srno' => 'numeric',
			                    'item' => 'required|min:3|max:100|unique:materials,name,NULL,id,deleted_at,NULL,code,'.$value['code'],
								'code' => 'required|min:2|max:4',
             				     'unit' => 'required|min:2|max:25,',
							];
							  $custmessage = [
				               'unique' => 'This material name and code has already been taken.',
				            ];
							$validationResult = self::validateInputData($newUser, $rules,$custmessage);
						
							$tmpArray['result'] = $validationResult['success'];
							// check alphabet  order to decide header enries
							if ($validationResult['success'] == false) {
								if ($isInvalid == true) {
									foreach ($messageBag->getMessages() as $msgKey => $msgValue) {
										$validationResult['errors']->add($msgKey, $msgValue[0]);
									}
								}
								$value["errors"] = $validationResult['errors'];
								$invalidUsers[] = $value;
								foreach ($value as $key => $val) {
									if (array_key_exists($key, $keys)) {
									} else {
								continue;
										array_push($keys, $key);
									}
								}
							}
							//Finally records did not have errors with validations but internally the file had problems
							// $value["errors"] = $messageBag;
							// $invalidUsers[] = $value;
							// foreach ($value as $key => $val) {
							// 	if (array_key_exists($key, $keys)) {
							// 	} else {
							// 		array_push($keys, $key);
							// 	}
							// }

						}
					}

					if (count($invalidUsers) > 0) {
						//delete already uploaded files

					//	$storageUtility->deleteFile($folder . $fileName);
					//	$storageUtility->deleteFile($folder . $uploadedFile);
						return $invalidUsers;
					}
					self::saveImportData($importData,$sup_id);
					Log::debug("---DATA SAVED----");
					return 'SUCCESS';

				} catch (ModelNotFoundException $exception) {
					Log::error('Exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
					return "MODEL_NOT_FOUND";
				} catch (Exception $exception) {
					Log::error('Exception 1 occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
						return "Error_occured";
				} catch (\Throwable $exception) {
					Log::error('Throwable exception 2 occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
					return "Error_occured";
				}

			}

		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;

	}

	public static function saveImportData($data,$sup_id)
	{
		//dd("save function");
		
			foreach ($data as $key => $value) {
			$data['authenticatedUser'] = \Auth::user();
			$userId = $data['authenticatedUser']->id;
 					$value['item'] = ucwords(strtolower(trim($value['item'])));
		           $value['code'] = strtoupper(trim($value['code']));
       
			$check = DB::transaction(function () use (&$value,$sup_id,$userId) {
				$obj = new Material;
		Log::debug("########## ITEM code NAME ### " . $value['item']);
				$obj->name = $value['item'];
				$obj->code = $value['code'];
				$obj->unit = $value['unit'];
				$obj->supplier_id = $sup_id;
	    		$obj->created_by = $userId;
				$obj->updated_by = $userId;
				$obj->save();				
		});
	}
		return $check;



	}


}
