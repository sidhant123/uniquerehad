Create sequence expense_types_seq; 

-- Table: public.expense_types

-- DROP TABLE public.expense_types;

CREATE TABLE public.expense_types
(
  id bigint NOT NULL DEFAULT nextval('expense_types_seq'::regclass),
  name varchar NOT NULL,
  description varchar,
  track_payment varchar,
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT expense_types_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.expense_types
  OWNER TO postgres;

-- ---------------------------------------------------------------------------------------------


INSERT INTO public.menus(
     id, display_name, name, sequence,  auth, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (94, 'Expense Types', 'Expense Types', 105,  1, '', now(), now(),  1, 1);


    INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES  
    ( 'List', 'List Expense Types', 110, 94,  1, now(), now(),  1, 1), 
    (  'New Expense Types', 'Create Expense Types', 115, 94,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020


INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (43,'Expense Types', now(), now(), 1, 1),
     (44, 'List Expense Types', now(), now(), 1, 1),
     (45, 'Create Expense Types', now(), now(), 1, 1),
     (46, 'Show Expense Types', now(), now(), 1, 1),
     (47, 'Delete Expense Types', now(), now(), 1, 1),
     (48, 'Edit Expense Types', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (43, 1, 43, now(), now(), 1,1),
     (44, 1, 44, now(), now(), 1,1),
     (45, 1, 45, now(), now(), 1,1),
     (46, 1, 46, now(), now(), 1,1),
     (47, 1, 47, now(), now(), 1,1),
     (48, 1, 48, now(), now(), 1,1);

alter table expense_types alter column track_payment set DEFAULT 'No';

INSERT INTO public.list_view_masters(
             function_name, entity_name,  
             query_columns, 
            heading_columns,
             filter_columns, 
             display_css_widths, 
             relations, replace_columns,
              created_at, updated_at, created_by, updated_by,
              filter_class_names,
              entity_namespace)
    VALUES ('List Expense Types', 'ExpenseType',
            'name,track_payment,id',
            'Name,Track Expense,Action', 
             '', 
             'col-xs-1,col-xs-2,col-xs-2,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1,col-xs-1', 
             '', '', now(),now(),1,1, 
             '', 
             'App\Models\');






