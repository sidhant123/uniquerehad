<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Log;
use vnnogile\Communication\Events\CommEvent;
use App\Models\CommunicationEvent;

class Test2Controller extends Controller
{
	   public function createData(){
	   	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	   	 $authenticatedUser=\Auth::user();
	   	 $tenant_code='CGX';
	   	 $login_id='1';
	   	 $attachment_path="storage/app/CommunicationMailAttachments/";
	   	 // data
	   	 $data['Participant'][31]=['template_data'=>['PARTICIPANT.NAME'=>'Abhishek Mishra','ORG.NAME'=>'TCS'],'email_id'=>'dhananjay@vnnogile.com','phone_no'=>'9930','program_code'=>'21','customer_organization_code'=>'TCS','attachments'=>[$attachment_path."butterfly.jpg",$attachment_path."sunset.jpg"]];
	   	 $data['Participant'][32]=['template_data'=>['PARTICIPANT.NAME'=>'Raj Malhotra','ORG.NAME'=>'TCS'],'email_id'=>'dhananjay.katre@gmail.com','phone_no'=>'9930','program_code'=>'21','customer_organization_code'=>'TCS'];
	   	 // $data['Participant'][33]=['template_data'=>['PARTICIPANT.NAME'=>'Rahul Desai','ORG.NAME'=>'TCS'],'email_id'=>'chandrabhan.pal@vnnogile.in','phone_no'=>'9930','program_code'=>'21','customer_organization_code'=>'TCS'];

	   	 
	   	 //other roles
	   	 $data['Company Coordinator'][3]=['email_id'=>'katre.ashwini@gmail.com','phone_no'=>'9930'];
	   	 //$data['Program Manager'][4]=['template_data'=>['PARTICIPANT.NAME'=>'Abhishek Manager','ORG.NAME'=>'TCS'],'email_id'=>'abhi.india.us.japan@gmail.com','phone_no'=>'9930','program_code'=>'21','customer_organization_code'=>'TCS'];
	   	 $data['Tenant Administrator'][1]=['email_id'=>'anupamakatre@gmail.com','phone_no'=>'9930'];

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	   	event(new CommEvent('UserEnrolledEvent',$data,$tenant_code,$login_id));

	   }
}
