<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 March 2018 13:31:34 +0530.
 */

namespace vnnogile\Geography\services;

//Custom imports
use App\Models\Contory;
use App\Models\State;
use App\Models\City;
use App\Models\Pincode;
use DB;
use Illuminate\Support\Facades\Log;

/*use vnnogile\Utilities\Services\FileUploadUtility;
use vnnogile\Utilities\Services\StorageUtility;*/
/**
 * Class CompositionDataService
 *
 */
class PincodeService
{
public static function savePincode($data)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        // First we will move file to temporary folder
        // Save the document information into the database
        // move the file from temporary file path to its respective location
        // finally update the file path of the document
        try {
            $check = DB::transaction(function () use ($data) {

                // print_r($data['authenticatedUser']->id);
                // exit();

                    $pincode_name = $data['pincode_name'];
                    $city_id = $data['city_id'];
                    // $state_id = $data['state_id'];
                    // $country_id = $data['country_id'];

                $pincode = new Pincode;
                $pincode->name = $pincode_name;
                $pincode->city_id = $city_id;
                // $pincode->state_id = $state_id;
               // $pincode->country_id = $country_id;
                $pincode->created_by = $data['authenticatedUser']->id;
                $pincode->updated_by = $data['authenticatedUser']->id;
                $pincode->save();

                Log::debug("***********AFTER SAVEING CITY**************");
                $pincode_id = $pincode->id;
                return $pincode_id;
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            });
            return $check;
	    } catch (Exception $e) {
	            Log::debug("PincodeService::saveRecord ended" . print_r($e, true));
	            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	            return $e;
        }
    }


    public static function updatePincode($data, $id)
    {
        try {
            $check = DB::transaction(function () use ($data, $id) {
                $pincode_id = $id;
                $pincode = self::getPincodeData($pincode_id);
                        $pincode->name = $data['pincode_name'];
                        $pincode->city_id =$data['city_id'];
                        // $pincode->state_id =$data['state_id'];
                      //  $pincode->country_id =$data['country_id'];
                        $pincode->updated_by = 1; //$data['authenticatedUser']->id;
                        $pincode->save();
                $pincode_id = $pincode->id;
                return $pincode_id;
            });
            return $check;
        } catch (Exception $e) {
            Log::debug("PincodeService::updateRecord ended" . print_r($e, true));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $e;
        }
    }

        public static function getPincodeData($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        $pincode = Pincode::find($id);
        return $pincode;
    }

        public static function deletePincode($id){
        Pincode::where('id', $id)->update(['deleted_by' =>auth()->user()->id]);
        Pincode::where('id', $id)->delete();
    }

    	 public static function getPincodeList(){
  		return Pincode::all();
  	}


}