<?php

namespace App\Services;


//Custom Imports Below
use App\Models\Validation;

use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

class ValidationRulesService
{

    protected $masterDataInterface;
    protected $utilsInterface;

    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
    }   

    
    public function validationRulesWithTenant($entityName, $routeParameterName = null,$id = NULL)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $userPrincipal = session('userPrincipal');
        $tenantPartyId = $userPrincipal->getTenantPartyId();   
        $routeParameter='';
        $validationRule= array();

        if (null != $routeParameterName){
            //if condition means update
             // $user = $this->route($routeParameterName);
             $validations = $this->utilsInterface->getValidationRulesForEntity($entityName, 'Update');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $extractedFromDb = str_replace('$tenantPartyId', $tenantPartyId, $extractedFromDb);
                 $extractedFromDb = str_replace("$routeParameterName", $id, $extractedFromDb);
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }else{
            //else condition means create
             $validations = $this->utilsInterface->getValidationRulesForEntity($entityName, 'Create');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $extractedFromDb = str_replace('$tenantPartyId', $tenantPartyId, $extractedFromDb);
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }
         Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
         return $validationRule;
    }

    public function validationRules($entityName, $routeParameterName = null,$id = NULL)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $userPrincipal = session('userPrincipal');
        $tenantPartyId = $userPrincipal->getTenantPartyId();   
        $routeParameter='';
        $validationRule= array();

        if (null != $routeParameterName){
            //if condition means update
             // $user = $this->route($routeParameterName);
             $validations = $this->utilsInterface->getValidationRulesForEntity($entityName, 'Update');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $extractedFromDb = str_replace("$routeParameterName", $id, $extractedFromDb);
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }else{
            //else condition means create
             $validations = $this->utilsInterface->getValidationRulesForEntity($entityName, 'Create');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }
         Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
         return $validationRule;
    }
public function validationRulesForEvent($entityName, $routeParameterName = null,$id = NULL,$tenantPartyId)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        // $userPrincipal = session('userPrincipal');
        // $tenantPartyId = $userPrincipal->getTenantPartyId();   
        $routeParameter='';
        $validationRule= array();

        if (null != $routeParameterName){
            //if condition means update
             // $user = $this->route($routeParameterName);
             $validations = $this->utilsInterface->getValidationRulesForEntity($entityName, 'Update');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $extractedFromDb = str_replace("$routeParameterName", $id, $extractedFromDb);
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }else{
            //else condition means create
             $validations = $this->utilsInterface->getValidationRulesForEntity($entityName, 'Create');
             foreach ($validations as $validation) {
                 $extractedFromDb = $validation->validation_rule;
                 $validationRule[$validation->field_name] = $extractedFromDb;
             }
         }
         Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
         return $validationRule;
    }
}
