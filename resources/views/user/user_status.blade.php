@extends('layouts.base')
@section('content')

<div id="main_div">
  <div><h3 class="headings">Assign User Status</h3></div>

  <div class="alert alert-success hide" align="center">
     <strong>Success!</strong> <span class="successMessage"></span>
  </div>

  <div class="row">
     <div class="col-lg-12 form-group required">
        <label for="compnay_id">Select Action</label>
        <div class="row" style="font-weight: bold;">
            <div class="col-lg-2">
             <input type="radio" name="action" value="activate">   Activate
            </div>
            <div class="col-lg-6">
             <input type="radio" name="action" value="deactivate">  Deactivate
            </div>
        </div>
     </div>
  </div>

  <div><p></p></div>

  <div class="row">
     <div class="col-lg-12">
       <div class="form-group required">
           <label for="compnay_id">Select Company</label>
           <select  class="form-control mandatory" id="compnay_id" name="company"></select>
         </div>
     </div>
  </div>

<div class="row">
     <div class="col-lg-5">

       <div class="row" style="text-align: center; padding: 1%; font-style: bold; font-size: 20px;">User List</div>
       <div class="row" style="overflow:auto; height:50%;">

        <table class="table table-bordered" id="list_user_table_id">
            <thead>
            <tr>
              <th width="10%"><input type="checkbox" id="select_all_list_user"></th>
              <th width="25%">Employee Id</th>
              <th width="65%">Name</th>
            </tr>
            </thead>
            <tbody id="list_user_table_body_id"></tbody>
          </table>
       </div>

     </div>

     <div class="col-lg-2">
       <div class="row" style="height: 25%" ></div>
       <div class="row">
         <div class="col-lg-3">
         </div>
         <div class="col-lg-6">

             <button id="user_add_btn_id" class="btn btn-primary"><i class="vnn vnn-mv-right" style="font-size:25px; width: 50px"></i></button>
             <p></p>
             <button id="user_delete_btn_id" class="btn btn-primary"><i class="vnn vnn-mv-left" style="font-size:25px; width: 50px"></i></button>
         </div>
         <div class="col-lg-3"></div>
       </div>
     </div>

      <div class="col-lg-5" >
        <div class="row" style="text-align: center; padding: 1%; font-style: bold; font-size: 20px;">Selected Users</div>
        <div class="row" style="overflow:auto; height: 50%; background-color: whitesmoke">
        <!-- <form id="coordinator_form_id"> -->
          <table class="table table-bordered" id="select_user_table_id">
            <thead>
            <tr>
              <th width="10%"><input type="checkbox" id="select_all_select_user"></th>
              <th width="25%">Employee Id</th>
              <th width="65%">Name</th>
            </tr>
            </thead>
            <tbody id="select_user_table_body_id"></tbody>
          </table>
        <!-- </form> -->
        </div>
     </div>
  </div>

  <!-- button -->
 <div class="row col-lg-12" align="center">
  <button class="btn btn-primary" type="button" id="submit_button_id">Activate</button>
 </div>

</div>
<!-- end of main div -->

@endsection


@push('scripts')

<style>


.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script src="/js/user.js"></script>
<script>

  //
  $(document).ready(function() {

  $("[name=action]").val(["activate"]);


  $('#compnay_id').select2({
  });
  $('#compnay_id').empty();
  $('#compnay_id').append('<option value="0">--select--</option>');

  @foreach($organization as $org)
  $('#compnay_id').append('<option value="{{$org->id}}">{{$org->name}}</option>');
  @endforeach
  });

  // reseting on activate or deactivate changes
  $('input[type=radio][name=action]').change(function() {
       //
       $("#compnay_id").select2("val","0");
       $('#list_user_table_body_id tr').remove();
       $('#select_user_table_body_id tr').remove();

       if ($('input[name=action]:checked').val() == 'deactivate') {
           //
           $('#submit_button_id').html('Deactivate');
       }else if ($('input[name=action]:checked').val() == 'activate') {
           //
           $('#submit_button_id').html('Activate');
       }

       // for aler message
       $('.successMessage').html('');
       $('.alert-success').addClass('hide');
  });

  // for user of company
$('#compnay_id').change(function(){

   //
   var compnay_id=$('#compnay_id').val();
   if (compnay_id ==0) {
       return false;
   }
   // var action=$('action').val();
   var action=$('input[name=action]:checked').val();
   console.log(compnay_id);
   console.log(action);

   if (!action) {
        alert('please select action');
        return false;
   }

   // for msg
   $('.successMessage').html('');
   $('.alert-success').addClass('hide');

   url='/usersForCompany/'+compnay_id+"/"+action;

    axios.get(url)
        .then(function (response) {

        var server_data=response['data'];
        console.log(server_data);

        // unsetting
        $('#list_user_table_body_id tr').remove();
        $('#select_user_table_body_id tr').remove();

        $.each(server_data,function(key,value){

              if (!value.employee_id) {
                   value.employee_id='';
              }

              $("#list_user_table_body_id").append("<tr><td width='10%'><input class=list_user type='checkbox' value="+value.id+"></td><td width='25%'>"+value.employee_id+"</td><td width='65%'>"+value.first_name+" "+value.last_name+"</td></tr>");
         })
    })
        .catch(function (error) {
        console.log("error"+error);
   });
});

///////////////////////////////////////// Logic  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///////  add and delete
$(document).on('click','#user_add_btn_id',function(){

  $('#list_user_table_body_id tr').each(function(){


   if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){

     var id=$(this).find('td:eq(0)').find(':checkbox').val();
     var emp_id=$(this).find('td:eq(1)').text();
     var name=$(this).find('td:eq(2)').text();

     $('#select_user_table_body_id').append("<tr><td width=10%><input class=select_user type=checkbox value="+id+"></td><td width=25%>"+emp_id+"</td><td width=65%>"+name+"</td></tr>");
     $(this).remove();

         }
       });
});


////////////////delete button
$(document).on('click','#user_delete_btn_id',function(){

  $('#select_user_table_body_id tr').each(function(){


   if($(this).find('td:eq(0)').find(':checkbox').prop('checked')){

     var id=$(this).find('td:eq(0)').find(':checkbox').val();
     var emp_id=$(this).find('td:eq(1)').text();
     var name=$(this).find('td:eq(2)').text();

     $('#list_user_table_body_id').append("<tr><td width='10%'><input class=list_user type='checkbox' value="+id+"></td><td width='25%'>"+emp_id+"</td><td width='65%'>"+name+"</td></tr>");
     $(this).remove();

         }
       });
});

/////////////////////////////////////// select all logic \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  $(document).on('change','#select_all_list_user',function(){
  if(this.checked == true){
    $('.list_user').each(function(index, el) {
      $(this).prop('checked', true);
    });
  }else{
   $('.list_user').each(function(index, el) {
    $(this).prop('checked', false);
  });

 }
});

  $(document).on('change','#select_all_select_user',function(){
  if(this.checked == true){
    $('.select_user').each(function(index, el) {
      $(this).prop('checked', true);
    });
  }else{
   $('.select_user').each(function(index, el) {
    $(this).prop('checked', false);
  });

 }
});

// submit button
$(document).on('click','#submit_button_id', function(){
     //
      if (!validateUsers()) {
          return false;
      }
      submitUsers();
})

function validateUsers(){

  if(document.getElementById('select_user_table_body_id').innerHTML){
     return true;
  }
  else{
    alert('please select the users');
    return false;
  }
 }

 function submitUsers(){

  var selected_users=[];
  var action=$('input[name=action]:checked').val();

  $('#select_user_table_body_id tr').each(function(){

     var ids=$(this).find('td:eq(0)').find(':checkbox').val();
     selected_users.push(ids);
  });
  console.log(selected_users);

  var data="data="+JSON.stringify(selected_users)+"&action="+action;
  var url="/activateUsers";

   axios({
        method: 'post',
        url: url,
        data: data,

       }).then(function(response) {

        var server_data=response['data'];
        console.log(server_data);

        // showing msg
        showSuccessMessage('Selected Users Successfully '+action+'d');

        // removing
        $('#select_user_table_body_id tr').remove();

      }).catch(function (error) {
      console.log("error"+error);
     });
 }

 function showSuccessMessage(message){
  $('.successMessage').html('');
  $('.alert-success').removeClass('hide');
  $('.successMessage').html(message);
  var scrollPos =  $(".alert").offset().top;
    $(window).scrollTop(scrollPos);
  }


</script>
@endpush


