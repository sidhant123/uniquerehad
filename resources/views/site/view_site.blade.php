@extends('site.site')
@section('title','View Site')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $row_data->id)


@section('name',$row_data->name)
@section('code',$row_data->code)
@section('location',$row_data->location)
@section('address',$row_data->address)


@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('.form-control').attr('readonly',true);
});
</script>

@endpush
