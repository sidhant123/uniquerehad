<?php

namespace vnnogile\Sms\Providers;

use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
public function boot()
    {
        //
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->publishes([
        __DIR__.'/../ui' => public_path('vendor/vnnogile'),
    ], 'public');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
        $this->app->make('vnnogile\sms\controllers\SMSController');
        $this->app->make('vnnogile\sms\controllers\EmailAddressController');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../jobs/SmsJob.php';
        include __DIR__.'/../jobs/SmsSuccessJob.php';        
        include __DIR__.'/../utility/SmsUtility.php';
        include __DIR__.'/../controllers/SMSController.php';
        include __DIR__.'/../controllers/EmailAddressController.php';
	include __DIR__.'/../services/NetCoreSmsImplementationService.php';
        
    }
}
