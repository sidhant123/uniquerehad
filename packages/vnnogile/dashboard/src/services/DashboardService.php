<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace vnnogile\Dashboard\Services;

//Custom imports
use App\User;
use App\Models\Program;
use App\Models\UserExerciseFinalResult;
use Illuminate\Support\Facades\Log;
/**
 * Class CompositionMaster
 * 
 */
class DashboardService
{
    public static function getQuizPerformanceForUserInProgram($programId, $userId){

        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try{

            try{
                $program = Program::findOrFail($programId);
            }catch(ModelNotFoundException $exception){
                Log::error("Unable to find program with id $programId");
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
                $response = array();
                $response['status'] = false;
                $response['message'] = __('custom.invalid_program');
                return $response;
            }
            try{
                $user = User::findOrFail($userId);
            }catch(ModelNotFoundException $exception){
                Log::error("Unable to find program with id $userId");
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
                $response = array();
                $response['status'] = false;
                $response['message'] = __('custom.invalid_user');
                return $response;
            }


            $totalCorrectAnswers =  $program->user_quiz()->where('user_exercise_final_results.user_id', $userId)->sum('user_exercise_final_results.total_correct_answers');

            $totalIncorrectAnswers =  $program->user_quiz()->where('user_exercise_final_results.user_id', $userId)->sum('user_exercise_final_results.total_incorrect_answers');

            $totalCorrectAnswersPercentage = ($totalCorrectAnswers / ($totalCorrectAnswers + $totalIncorrectAnswers)) * 100;
            $totalCorrectAnswersPercentage = number_format((float)$totalCorrectAnswersPercentage, 2, '.', '');

            $totalIncorrectAnswersPercentage = ($totalIncorrectAnswers / ($totalCorrectAnswers + $totalIncorrectAnswers)) * 100;
            $totalIncorrectAnswersPercentage = number_format((float)$totalIncorrectAnswersPercentage, 2, '.', '');

            $response = array();
            $response['status'] = true;
            $response['programId'] = $program->id;
            $response['programCode'] = $program->code;
            $response['programName'] = $program->name;
            $response['totalCorrectAnswers'] = $totalCorrectAnswers;
            $response['totalIncorrectAnswers'] = $totalIncorrectAnswers;
            $response['totalCorrectAnswersPercentage'] =  $totalCorrectAnswersPercentage;
            $response['totalIncorrectAnswersPercentage'] =  $totalIncorrectAnswersPercentage;

            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $response;

        }catch(ModelNotFoundException $exception){
            Log::error('Exception:: ' . $exception->getMessage());
            $response = array();
            $response['status'] = false;
            $response['message'] = __('custom.something_wrong');
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $response;
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

    }

    public static function getQuizPerformanceForUser($userId){

        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        try{

            try{
                $user = User::findOrFail($userId);
            }catch(ModelNotFoundException $exception){
                Log::error("Unable to find program with id $userId");
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
                $response = array();
                $response['status'] = false;
                $response['message'] = __('custom.invalid_user');
                return $response;
            }


            $totalCorrectAnswers = $user->quiz_results()->sum('user_exercise_final_results.total_correct_answers');

            $totalIncorrectAnswers =  $user->quiz_results()->sum('user_exercise_final_results.total_incorrect_answers');


            $totalCorrectAnswersPercentage = ($totalCorrectAnswers / ($totalCorrectAnswers + $totalIncorrectAnswers)) * 100;
            $totalCorrectAnswersPercentage = number_format((float)$totalCorrectAnswersPercentage, 2, '.', '');

            $totalIncorrectAnswersPercentage = ($totalIncorrectAnswers / ($totalCorrectAnswers + $totalIncorrectAnswers)) * 100;
            $totalIncorrectAnswersPercentage = number_format((float)$totalIncorrectAnswersPercentage, 2, '.', '');

            $response = array();
            $response['status'] = true;
            $response['name'] = $user->first_name . ' '. $user->last_name;
            $response['userid'] = $user->userid;
            $response['email'] = $user->email;
            $response['totalCorrectAnswers'] = $totalCorrectAnswers;
            $response['totalIncorrectAnswers'] = $totalIncorrectAnswers;
            $response['totalCorrectAnswersPercentage'] =  $totalCorrectAnswersPercentage;
            $response['totalIncorrectAnswersPercentage'] =  $totalIncorrectAnswersPercentage;

            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $response;

        }catch(ModelNotFoundException $exception){
            Log::error('Exception:: ' . $exception->getMessage());
            $response = array();
            $response['status'] = false;
            $response['message'] = __('custom.something_wrong');
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $response;
        }


        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

    }
}
