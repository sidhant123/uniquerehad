<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
                Passport::routes();
        $this->registerPermissionPolicies();

    }

    public function registerPermissionPolicies()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $permissions = Permission::all();
        foreach ($permissions as $permission) {
            Log::debug('***********Permission Name*******************');
            Log::debug($permission->name);
            $permissionName = $permission->name;
            Gate::define($permission->name, function ($user, $permissionName) {
                return $user->hasAccess(array($permissionName));
            });
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

}
