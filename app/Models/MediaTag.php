<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jan 2018 14:15:33 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class MediaTag
 * 
 * @property int $id
 * @property int $media_id
 * @property int $media_type
 * @property array $tags
 * @property string $tags_virtual
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class MediaTag extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'media_id' => 'int',
		'media_type' => 'int',
		//'tags' => 'json',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'media_id',
		'media_type',
		'tags',
		'tags_virtual',
		'created_by',
		'updated_by',
		'deleted_by'
	];


}
