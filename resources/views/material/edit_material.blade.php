@extends('material.material')
@section('editID', '/'.$row_data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('edit_btn','hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Material')
@section('name',$row_data->name)
@section('code',$row_data->code)
@section('unit',$row_data->unit)
