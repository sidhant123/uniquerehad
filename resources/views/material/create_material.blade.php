@extends('material.material')
@section('title','Create Material')
@section('editMethod')
{{method_field('post')}}
@endsection
@section('view-nav-bar', 'hide')
@section('update-nav-bar', 'hide')
@section('edit_btn','hide')

@section('importMaterials')

<div class="row col-xs-12">           
 <p></p> 
</div>
<div class="row col-xs-12">           
 <p></p> 
</div>

<div class="row col-xs-12" align="center" >           
  OR   
</div>
<p></p>

<form action="/materialImportDataVerification" method="post"  enctype="multipart/form-data" style="border-style:dotted; border-width: 1px;" onsubmit="return validateMandatoryFieldsForImport()">
    {{ csrf_field() }}
<div>

  <h3 class="headings">{{__('labels.materials.import_material')}}</h3>

  <div class="row">
  <div class="col-xs-6 ">
     <div class="form-group required">
              <label for="supplier_id@yield('supplier_name_d')">{{ __('labels.materials.supplier_name') }}</label>
              <select  class="form-control importMandatory" name="import_supplier_id"
               id="import_supplier_id"@yield('supplier_name_d')>
                  @if(isset($supplier))
                  <option value="0">--select--</option>'
                  @foreach($supplier as $sup_val)
                  @if(isset($sel_supplier_id))
                  @if($sup_val->id === $sel_supplier_id)
                  <option value="{{$sup_val->id}}" selected>{{$sup_val->name}}</option>
                  @else
                  <option value="{{$sup_val->id}}">{{$sup_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$sup_val->id}}">{{$sup_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>    
    </div>
     <div class="col-xs-6 required">
      <div class="form-group">
          <label for="materialFile">{{__('labels.materials.import_material')}}</label>
          <input type="file" class="form-control fillFields importMandatory"  id="materialFile"  name="imported_material_file" accept=".xlsx" >
        </div>
    </div>
  </div> 

   
<p></p>
<div class="row">
<div class="col-xs-12" align="center">
  <button class="btn btn-primary">
    <a href="\storage\ExcelFormat\Material_Import_Format.xlsx" target="_blank">Download Excel Format</a>
</button>
    <button type="submit" class="btn btn-small btn-primary">{{__('labels.users.submit')}}</button>
   
</div>
</div>
<p></p>


<p></p>
</form>
@endsection
