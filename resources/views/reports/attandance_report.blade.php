@extends('layouts.base')
@section('content')
<form  id="attendanceReportForm" method="post" onsubmit="return validateMandatoryFields();">
  {{ csrf_field() }}
  <input type="hidden" name="report_type" id="report_type">
  <input type="hidden" name="entity_type" id="entity_type">
  @section('editMethod')
  @show
  <h3 align="center">Export Attendance Report</h3>

    <strong>NOTE**</strong><p></p>
<textarea class="form-control" style="width:100%;" rows="4" cols="30" readonly="readonly">
 To download Summary report it's mandatory to select from and to date.
 To download Detail report it's mandatory to select from and to date. And site engineer name is non madatory.

</textarea>
    <p></p>

<!--   <div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <div class="radio-inline">
          <label><input type="radio" name="optassessmentorprogram" checked value="Assessment">Assessment</label>
        </div>
        <div class="radio-inline">
          <label><input type="radio" name="optassessmentorprogram" value="Program">Program</label>
        </div>    
      </div>
    </div>
  </div> -->
<div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <label for="project_id">Start Date</label>
        <input type="date" class="form-control mandatory" id="start_date" name="start_date" onchange="return validateDate()">
      </div>
</div>
<div class="col-lg-3">
      <div class="form-group">
        <label for="project_id">End Date</label>
        <input type="date" class="form-control mandatory" id="end_date" name="end_date" onchange="return validateDate()">
      </div>
</div>
<div class="col-lg-3">
      <div class="form-group">
        <label for="user_id">Site Engineer</label>
          <select type="text" id="user_id" name="user_id" class="form-control">
          <option value="0">---All---</option>
          @foreach($site_users as $val)
            <option value="{{$val['id']}}"> {{$val['first_name'].'-'.$val['email']}}</option>
          @endforeach
        </select>
      </div>
</div>
</div>

  <div class="row">
    
    <div class="col-lg-3">
      <div class="form-group">
        <label for=""></label>
        <br>
        <button id="btnSummary" class="btn btn-primary" type="button" style="height: 34px;" >
          Download Summary Report &nbsp;&nbsp;<i class="vnn vnn-download"></i>
        </button>
      </div>
    </div>   
    <div class="col-lg-3">
      <div class="form-group">
        <label for=""></label>
        <br>
        <button id="btnDetail" class="btn btn-primary" type="button" style="height: 34px;" >
          Download Detail Report &nbsp;&nbsp;<i class="vnn vnn-download"></i>
        </button>
      </div>
    </div>  

</div>      
</form>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
     $('#company_id,#assessment_id,#program_id,#node_id').select2({});
     $('#programSelect').hide();
     $('#nodeSelect').hide();



$(document).on('click', '#btnSummary', function() {
 
   var valid = true;
   valid = validateMandatoryFields();
   if(valid) {
     $('#attendanceReportForm').attr('action', "/attendanceReport").submit();
   }
});

$(document).on('click', '#btnDetail', function() {
 
   var valid = true;
   valid = validateMandatoryFields();
   if(valid) {
     $('#attendanceReportForm').attr('action', "/attendanceDetailReport").submit();
   }
});

});

  function validateDate(){

var valid = true;
var fromdate = Date.parse($("#start_date").val());
var todate = Date.parse($("#end_date").val());
//alert(fromdate>todate);
if(fromdate>todate){
  $("#end_date").val("");
     var selector='input[name=end_date]';
     scrollToElement(selector)
     valid=false;
}
  return valid;
}
  function populateData(data, control_id) {
    $("#" + control_id).empty();
   if (Object.keys(data).length > 0) {
        $("#" + control_id).append('<option value="0">--Select--</option>');
     $.each(data, function(key, value) {
         $("#" + control_id).append("<option value='" + value.id + "'>" + value.name + "</option>");
     });
   }
 }
</script>
@endpush
