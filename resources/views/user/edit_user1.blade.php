@extends('user.user')

@section('editID', $data->id)

@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('organization_name', $org_id)

@section('firstName', $data->first_name)
@section('lastName',$data->last_name)
@section('employeeId',$data->employee_id)
@section('department',$data->department)
@section('designation',$data->designation)

@foreach($data->party->contacts as $contact)
		@if ($loop->index == 0)
			@section('primaryContact',$contact->phone_number)
		@endif
		
		@if ($loop->index == 1)
			@section('secondaryContact',$contact->phone_number)
		@endif
		
@endforeach

@section('email',$data->email)
@section('linkedinProfile',$data->linkedin_profile)
@section('fbProfile',$data->facebook_id)
@section('twitterHandle',$data->twitter_handle)

@section('old_photo',$data->photo)
@section('old_photo_name',$data->original_photo)


@push('scripts')

<script>
$(document).ready(function() {
  
  $('#file').removeClass("mandatory");
  $('#photo_div_id').removeClass('req');   

});
</script>

@endpush



