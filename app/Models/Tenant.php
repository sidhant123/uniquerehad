<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:01 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Tenant
 * 
 * @property int $id
 * @property int $party_id
 * @property string $tenant_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $master_tenant
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Tenant extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
		'master_tenant' => 'int'
	];

	protected $fillable = [
		'party_id',
		'tenant_code',
		'created_by',
		'updated_by',
		'deleted_by',
		'master_tenant'
	];



}
