<?php

// cssir

namespace App\Services;

//Custom imports
use App\Models\ArtefactType;
use App\Models\Configuration;
use App\Models\Document;
use App\Models\LearningTreeColor;
use App\Models\Organization;
use App\Models\Person;
use App\Models\Program;
use App\Models\ProgramLtTopic;
use App\Models\ProgramUser;
use App\Models\ProgramVirtualRoom;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Topic;
use App\Models\TopicReminderConfiguration;
use App\Models\TopicUser;
use App\Services\ProgramService;
use App\User;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use vnnogile\DMS\controllers\DocumentController;
use vnnogile\DMS\services\DmsService;
use vnnogile\ProgramReminders\Services\ProgramReminderService;

class ProgramLearningTreeService {
	//Gloabal variable

	// public $related_child_topics = array();

	// for adding learning tree as node
	var $old_lt_id;
	var $new_lt_id;
	var $parent_topic_color;

	var $loggedInUserId;
	var $storage_utility_class_name;
	private static $filePath = "program/";
	private static $vc_delete_array = [];
	private static $delete_topic_array = [];

	public static function createLearningTreeForProgram($data) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$program_lt = DB::transaction(function () use ($data) {

			// deleting program virtual room  if topic is vc
			$vc_artefact = ArtefactType::where('name', 'Virtual Classroom')->first();
			$vc_topics = ProgramLtTopic::where(['container_program_id' => $data['program_id'], 'artefact_type_id' => $vc_artefact['id']])->get();
			if (count($vc_topics) > 0) {
				self::deleteProgramVirtualRooms($vc_topics);
			}

			// deleting old topics if exist
			self::deleteOldTopicsForProgram($data['program_id']);

			//replicating learning tree as program learning tree
			self::replicateTopicsForProgram($data);

			// saving selected lt in program json
			$program = Program::where('id', $data['program_id'])->first();
			$lt = json_decode($program['learning_tree'], true);
			$lt['learning_tree'] = $data['lt_id'];

			$dd = Program::where('id', $data['program_id'])->update(['learning_tree' => json_encode($lt)]);

			// generating object
			$object = self::gettingTabObject($data['program_id'], 1);

			// getting facilitator
			$facilitator = self::getFacilitators($program['customer_party_id']);

			// getting company users for moderator
			$returned_array['moderator'] = self::getUsers($program['customer_party_id']);

			$returned_array['object'] = $object;
			$returned_array['facilitator'] = $facilitator;
			$returned_array['publish'] = ProgramService::getPublishCondition($lt);

			return $returned_array;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $program_lt;
	}

	public static function getFacilitators($customer_party_id) {
		//
		$company = Organization::where('name', 'Facilitator Org')->first();
		$facilitator_person = Person::where('customer_party_id', $company['party_id'])->get();
		$party_id = $facilitator_person->pluck('party_id');

		$users = User::all();
		$facilitator_users = $users->whereIn('party_id', $party_id)->toArray();

		// finding facilitators from  customer organizations
		// $customer_party_id = 35;
		$role = Role::where('name', 'Facilitator')->first();
		$ret = Role::with(['users.party.person' => function ($query) use ($customer_party_id) {$query->where(['customer_party_id' => $customer_party_id]);}])->where('id', '=', $role['id'])->get();
		$persons = array();

		foreach ($ret as $key => $value) {
			foreach ($value['users'] as $k => $v) {
				if (!empty($v->party->person)) {
					// $persons = $v->party->person;
					array_push($persons, $v->party->user);
				}
			}
		}
		// Log::debug($persons);
		// print_r($facilitator_users);
		// print_r(count($facilitator_users));
		// exit();
		// return $persons;
		$final_facilitators = array_merge($facilitator_users, $persons);

		return $final_facilitators;
	}

	public static function getUsers($company_party_id) {
		//
		$company = Organization::where('party_id', $company_party_id)->first();

		$tenant_person = Person::where(['customer_party_id' => null, 'tenant_party_id' => $company['tenant_party_id']])->get();

		$tenant_party_id = $tenant_person->pluck('party_id');

		$users = User::all();
		$tenant_user = $users->whereIn('party_id', $tenant_party_id);

		return $tenant_user;
	}

	public static function deleteOldTopicsForProgram($program_id) {

		//
		$u = ProgramLtTopic::where('container_program_id', $program_id)->update(['deleted_by' => auth()->user()->id]);
		$d = ProgramLtTopic::where('container_program_id', $program_id)->delete();
		// delete from behind
	}

	public static function replicateTopicsForProgram($data) {
		//
		//getting data
		if (session()->has('userPrincipal')) {
			$userPrincipal = session('userPrincipal');
			$data['loggedInUserId'] = $userPrincipal->getUserId();
		} else {
			$data['loggedInUserId'] = $data['authenticatedUser']->id;
		}

		$lt_id = $data['lt_id'];

		// $topics = Topic::where('container_learning_tree_id', $lt_id)->get();
		$GLOBALS['new_lt_id'] = $data['program_id'];
		$GLOBALS['loggedInUserId'] = $data['loggedInUserId'];
		$GLOBALS['old_lt_id'] = $data['lt_id'];

		// for copying photo_path and photo
		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;
		$class = $data['configurations']['storage_implementation_class'];
		$GLOBALS['storage_utility_class_name'] = $class;

		//saving in topic table
		self::saveChildTopicInDatabase(null, null);

	}

	public static function saveTopic($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		// checking if save or update
		if ($data['hidden_flag'] != "") {
			$topic_id = self::updateTopic($data);
			return $topic_id;
		}

		$topic_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			//
			// $data['program'] = Program::where('id', $data['containerLearningTree'])->first();
			$program = Program::where('id', $data['containerLearningTree'])->first();

			/// saving topic

			$topic = new ProgramLtTopic;

			$topic->name = $data['topic_name'];
			$topic->description = $data['topic_description'];
			$topic->expected_duration = $data['expected_duration'];

			// saving durations
			//$topic->available_from = date('Y-m-d', strtotime($data['program_lt_from']));
			//$topic->available_to = date('Y-m-d', strtotime($data['program_lt_to']));

			$topic->available_from = date('Y-m-d H:i:s', strtotime($data['program_lt_from']));
			$topic->available_to = date('Y-m-d H:i:s', strtotime($data['program_lt_to']));

			// for parent topic
			if ($data['parent_topic_id'] != 0) {
				// finding id of parent node
				$topic->parent_topic_id = $data['parent_topic_id'];

				$parent_topic = ProgramLtTopic::where('id', $data['parent_topic_id'])->first();
				$topic->color = $parent_topic['color'];
			} else {
				// finding sequence for tab
				$last_tab_seq = ProgramLtTopic::where(['container_program_id' => $data['containerLearningTree'], 'parent_topic_id' => null])->max('sequence');
				if (!$last_tab_seq) {
					//
					$sequence = 600;
				} else {
					//
					$i = (int) $last_tab_seq / 600;
					if ((int) $last_tab_seq % 600 == 0) {
						$sequence = 600 * ($i + 1);
					} else {
						$sequence = 600 * ($i + 2);
					}
				}
				$data['sequence'] = $sequence;

				/// adding color new way
				$lastTabTopic = ProgramLtTopic::where(['container_program_id' => $data['containerLearningTree'], 'parent_topic_id' => null])->orderBy('sequence', 'desc')->first();
				$total_color = LearningTreeColor::all();
				$lastTabColor = $total_color->where('color', $lastTabTopic['color'])->first();

				if ($lastTabColor['id'] == count($total_color)) {
					//
					$currentTabColorId = 1;
				} else {
					$currentTabColorId = $lastTabColor['id'] + 1;
				}

				$color = $total_color->where('id', $currentTabColorId)->first();
				$topic->color = $color['color'];

				/// adding color old
				// $total_nodes = ProgramLtTopic::where(['container_program_id' => $data['containerLearningTree'], 'parent_topic_id' => null])->get();

				// $total_color = LearningTreeColor::all();
				// $total_color_count = count($total_color) + 1;

				// $color_rem = (count($total_nodes) + 1) % $total_color_count;
				// $color_div = (count($total_nodes) + 1) / $total_color_count;
				// $color_id = $color_rem + (int) $color_div;

				// // if id greater than 13

				// // Log::debug($color_rem+.' '.+$color_div);

				// $color = LearningTreeColor::select('color')->where('id', $color_id)->first();
				// $color = $color->color;
				// $topic->color = $color;

				// setting for topics_to_complete
				$data['topics_to_complete'] = 0;
			}

			// adding node icon
			// if (!empty($data['node_icon'])) {
			// 	$topic->icon = $data['node_icon'];
			// }

			//
			$topic->sequence = $data['sequence'];

			// creating json for details
			// prevent for learning tree also
			$check_moderator = 0;
			if ($data['entity_type'] != 'MajorNode' && $data['entity_type'] != 'LearningTree') {

				//for virtual class room
				if ($data['entity_type'] == 'Virtual Classroom') {
					//
					$check_moderator = 1;
					Log::debug('in VirtualClassRoom section');
					$details_array['moderators'] = $data['moderator'];
					$details_array['facilitators'] = $data['facilitator'];

					$role = Role::all();
					$mod_role = $role->where('name', 'Moderator')->first();
					$fac_role = $role->where('name', 'Facilitator')->first();

					// assigning user role and program role for facilitator moderators
					foreach ($data['moderator'] as $key => $value) {
						//
						self::assigningUserRole($value, $program, $mod_role['id'], $data['loggedInUserId']);
					}
					foreach ($data['facilitator'] as $key => $value) {
						self::assigningUserRole($value, $program, $fac_role['id'], $data['loggedInUserId']);
					}

					// saving in  program_virtual_rooms
					// self::saveInProgramVirtualRooms($topic->id, $data);

				} elseif ($data['entity_type'] == 'Assignment') {

					//checking type of action
					$action = $data['activityAction'];
					$details_array['activityAction'] = $action;
					$file = [];
					$submitType = 'submit';
					if ($action == 'links' || $action == 'editDoc') {
						if (isset($data['assignment_file'])) {
							$assignment_artefact = ArtefactType::where('name', 'Assignment')->first();
							$paths = DocumentController::getArtefactTypePaths($assignment_artefact['id']);
							$Info = self::uploadFile($data, $paths);

							foreach ($Info['uploadedFileNames']['assignment_file'] as $key => $value) {
								//
								$name = $Info['originalFileNames']['assignment_file'][$key];
								// $file[] = ['name' => $name, 'path' => $paths['temp_path'] . $value];
								$file['name'] = $name;
								$file['path'] = $paths['temp_path'] . $value;
							}
						}
					} elseif ($action = 'file') {
						if ($data['submitType'] == 'submit') {
							$submitType = 'submit';
						} else {
							$submitType = 'save';
						}
					}
					$details_array['files'] = $file;
					$details_array['submitType'] = $submitType;
					// print_r($data['submitType']);

				} else {
					//
					$details_array['entity_name'] = $data['entity_name'];
					$details_array['entity_id'] = $data['entity_id'];
					$details_array['entity_url'] = $data['entity_url'];

					$details_array['entity_duration'] = $data['entity_duration'];
					$details_array['entity_size'] = $data['entity_size'];
					$details_array['entity_description'] = $data['entity_description'];

					$details_array['entity_pages'] = $data['entity_pages'];
					$details_array['entity_title'] = $data['entity_title'];
					$details_array['entity_summary'] = $data['entity_summary'];
				}

				$details_array['entity_type'] = $data['entity_type'];

				// getting type
				// check for document and learning tree -- no need for learning tree
				if ($data['entity_type'] == 'Pdf' || $data['entity_type'] == 'Ppt' || $data['entity_type'] == 'Word' || $data['entity_type'] == 'Exel' || $data['entity_type'] == 'Text' || $data['entity_type'] == 'Web') {
					$entity_type_name = 'Document';
				} else if ($data['entity_type'] == 'Youtube' || $data['entity_type'] == 'Vimeo') {
					$entity_type_name = 'Video';
				} else {
					$entity_type_name = $data['entity_type'];
				}

				$artefact_type = ArtefactType::where('name', $entity_type_name)->first();
				Log::debug($artefact_type['id']);
				if ($artefact_type['id'] != null) {
					$details_array['artefact_type_id'] = $artefact_type['id'];

				} else {
					$details_array['artefact_type_id'] = null;
				}

				$details_jason = json_encode($details_array);
				$topic->details = $details_jason;

				// saving icon for leaf node

			} elseif ($data['entity_type'] == 'MajorNode') {
				//
				if ($data['topics_to_complete']) {
					$topic->topics_to_complete = $data['topics_to_complete'];
				}}

			// $topic->artefact_type_id=$data['entity_type'];

			if (array_key_exists("critical_topic", $data)) {
				// if(isset($data['critical_topic'])){
				$topic->critical = '1';
				$topic->alert_before_nodes = $data['alert_before_nodes'];
			} else {
				$topic->critical = 0;
			}
			if (array_key_exists('is_optional', $data)) {
				//
				$topic->optional = 1;
			}

			// $topic->container_learning_tree_id=$data['hidden_learning_tree_id'];
			$topic->container_program_id = $data['containerLearningTree'];
			$topic->created_by = $data['loggedInUserId'];
			$topic->updated_by = $data['loggedInUserId'];

			if (!isset($data['node_icon'])) {
				if (isset($data['entity_id'])) {
					$doc = Document::find($data['entity_id']);
					if ($doc['document_photo']) {
						$topic->photo = $doc['document_photo'];
						$topic->photo_path = $doc['document_photo_path'];
					}
				}
			}

			// status_color
			if (isset($data['topic_status_color_allocation_method'])) {
				if ($data['topic_status_color_allocation_method'] != '') {
					$topic->status_color_allocation_method = $data['topic_status_color_allocation_method'];
				}
			}

			$topic->save();
			$id = $topic->id;
			if (isset($data['node_icon'])) {
				self::uploadProgramLTIcon($data, $data['containerLearningTree'], $id);
			}
			//// after adding lt as node
			if ($data['entity_type'] == 'LearningTree') {

				Log::debug($data['learning_tree'] . '>>>>>>>>>>>>>>>>>');

				$GLOBALS['new_lt_id'] = $data['containerLearningTree'];
				$GLOBALS['loggedInUserId'] = $data['loggedInUserId'];
				$GLOBALS['old_lt_id'] = $data['learning_tree'];
				$GLOBALS['parent_topic_color'] = $topic->color;

				// for copying photo_path and photo
				$configurations = $data['masterDataInterface']->all('Configuration');
				foreach ($configurations as $configuration) {
					$configurationsMap[$configuration->configKey] = $configuration->configValue;
				}
				$data['configurations'] = $configurationsMap;
				$class = $data['configurations']['storage_implementation_class'];
				$GLOBALS['storage_utility_class_name'] = $class;

				//saving in topic table
				self::saveChildTopicInDatabase(null, $topic->id);
			}

			/// creating object to return
			if ($data['parent_topic_id'] != 0) {
				//

				$returned_array['topic_id'] = $id;
				$returned_array['topic_name'] = $data['topic_name'];
				// $returned_array['sequence']=$data['sequence'];
				// $returned_array['entity_type']=$data['entity_type'];

				$topic_array = ProgramLtTopic::where(['container_program_id' => $data['containerLearningTree'], 'parent_topic_id' => $data['parent_topic_id']])->orderBy('sequence', 'asc')->get();

				$object = self::gettingTopicObject($topic_array);

				$returned_array['object'] = $object;

			} else {
				//
				if ($data['entity_type'] == 'LearningTree') {
					//
					$topic_array = ProgramLtTopic::where(['container_program_id' => $data['containerLearningTree'], 'parent_topic_id' => $id])->orderBy('sequence', 'asc')->get();
					$object = self::gettingTopicObject($topic_array);
				} else {
					//
					$object = self::creatingObjectForNewTab();
				}

				$returned_array['object'] = $object;
				// $returned_array['facilitator_id'] = $data['facilitator'];
				$returned_array['topic_id'] = $id;
				$returned_array['topic_name'] = $data['topic_name'];
			}

			// saving in  program_virtual_rooms
			if ($check_moderator == 1) {
				self::saveInProgramVirtualRooms($topic->id, $data);
			}

			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished" . $topic->id);

			return $returned_array;
		});

		return $topic_id;

	}
	private static function uploadProgramLTIcon($data, $program_id, $topic_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$data['masterDataInterface'] = $data['masterDataInterface'];
		//storage configurations
		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;
		$data['topic_id'] = $topic_id;
		$data['program_id'] = $program_id;
		$document_details = self::uploadProgramPhoto($data);
		Log::debug($document_details);
		$program_lt_topic = ProgramLtTopic::find($topic_id);
		if (isset($data['node_icon'])) {
			$program_lt_topic->photo = $document_details['originalFileNames']['node_icon'][0];
			$program_lt_topic->photo_path = "/storage/" . self::$filePath . $program_id . "/programlttopic/" . $topic_id . "/" . $document_details['uploadedFileNames']['node_icon'][0];
		}
		$program_lt_topic->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

	}

	private static function uploadProgramPhoto($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->uploadFileToPublicStorage($data['request'], self::$filePath . $data['program_id'] . "/programlttopic/" . $data['topic_id']);
	}
	private static function deleteProgramPhoto($data) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		return $storageUtility->deleteFile(str_replace("storage", "public", $data['remove_file']));
	}
	private static function copyProgramLTPhoto($fileName, $sourceFolder, $id, $container_learning_tree_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//
		$source_array = explode('/', $sourceFolder);

		$corrected_source_folder1 = str_replace("storage", "public", $sourceFolder);
		$corrected_source_folder = str_replace(end($source_array), '', $corrected_source_folder1);
		$destination_folder = '/public/program/' . $container_learning_tree_id . '/programlttopic/' . $id . '/';

		$class = $GLOBALS['storage_utility_class_name'];
		$storageUtility = new $class;
		$copy_status = $storageUtility->copyFile(end($source_array), $corrected_source_folder, $destination_folder);

		$corrected_destination_folder = str_replace("public", "storage", $destination_folder) . '' . end($source_array);

		$topic = ProgramLtTopic::find($id);
		$topic->photo = $fileName;
		$topic->photo_path = $corrected_destination_folder;
		$topic->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function deleteFiles($file) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($file);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return Storage::delete($file);
	}

	public static function getUserObject($user_id) {
		$user = User::where('id', $user_id)->first();
		Log::debug($user_id);
		return $user;
	}

	public static function saveInProgramVirtualRooms($topic_id, $data) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		//
		$pvr = new ProgramVirtualRoom;

		$pvr->program_id = $data['containerLearningTree'];
		$pvr->meeting_id = $topic_id;
		$pvr->meeting_subject = $data['topic_name'];
		$pvr->start_datetime = date('Y-m-d H:i:s', strtotime($data['program_lt_from']));
		$pvr->meeting_duration = $data['expected_duration'];
		$pvr->moderator_password = 'm@123';
		$pvr->participant_password = 'p@123';

		$pvr->moderator_default_message = '';
		$pvr->welcome_message = '';

		$pvr->is_recording = 0;
		$pvr->default_layout = 'ab';
		$pvr->logout_url = 'ab';

		if ($data['moderator_msg']) {
			//
			$pvr->moderator_default_message = $data['moderator_msg'];
		} else {
			$pvr->moderator_default_message = 'default msg';
		}
		if ($data['welcome_msg']) {
			//
			$pvr->welcome_message = $data['welcome_msg'];
		} else {
			$pvr->welcome_message = 'default msg';
		}

		$pvr->created_by = $data['loggedInUserId'];
		$pvr->updated_by = $data['loggedInUserId'];

		$pvr->save();

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function uploadFile($data, $paths) {
		//

		$upload_data['request'] = $data['request'];
		$upload_data['temp_path'] = $paths['temp_path'];
		$upload_data['storage_path'] = $paths['storage_path'];
		$upload_data['authenticatedUser'] = $data['authenticatedUser'];
		$upload_data['utilsInterface'] = $data['utilsInterface'];
		$upload_data['masterDataInterface'] = $data['masterDataInterface'];

		$configurations = $data['masterDataInterface']->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$upload_data['configurations'] = $configurationsMap;

		$uploadInfo = DmsService::uploadDocument($upload_data);

		return $uploadInfo;
	}

	public static function creatingObjectForNewTab() {
		$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
		$max_cell_no = $config['config_value'];

		$object = [];
		for ($i = 1; $i <= $max_cell_no; $i++) {
			$object[$i] = '';
		}

		return $object;
	}

////// adding learning tree as topic functions starts \\\\\

	public static function saveChildTopicInDatabase($old_parent_id, $new_parent_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		if ($new_parent_id == null) {
			$flag = 'import';
		} else {
			$flag = 'insert';
		}
		// getting first level topics
		$topics = self::gettingTopicsByLevel($old_parent_id);
		Log::debug($topics);

		if (count($topics)) {

			$i = 1;
			foreach ($topics as $key => $topic_array) {

				//saving 1st topic
				$topic_id = self::saveTopicOfInsertedLtInDataBase($topic_array, $new_parent_id, $flag);

				$tt = self::saveChildUnderTab($topic_array['id'], $topic_id, $flag);

				$i++;
			}
			// svg only for last sequence
			// tab svg

		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function saveChildUnderTab($old_parent_id, $new_parent_id, $flag) {

		// getting first level onwards topics
		$topics = self::gettingTopicsByLevel($old_parent_id);
		Log::debug($topics);

		if (count($topics)) {

			$i = 1;
			foreach ($topics as $key => $topic_array) {

				//saving 1st topic
				$topic_id = self::saveTopicOfInsertedLtInDataBase($topic_array, $new_parent_id, $flag);

				$last_cell = self::saveChildUnderTab($topic_array['id'], $topic_id, $flag);
				$i++;
			}
			//non-leaf svg

		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function saveTopicOfInsertedLtInDataBase($topic_array, $new_parent_id, $flag) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		Log::debug($topic_array);
		$topic = new ProgramLtTopic;

		$topic->name = $topic_array['name'];
		$topic->description = $topic_array['description'];
		$topic->sequence = $topic_array['sequence'];
		//$topic->icon = $topic_array['icon'];
		$topic->optional = $topic_array['optional'];

		if ($flag == 'import') {
			$topic->color = $topic_array['color'];
		} elseif ($flag == 'insert') {
			$topic->color = $GLOBALS['parent_topic_color'];
		}
		// $topic->color = $topic_array['color'];

		$topic->expected_duration = $topic_array['expected_duration'];
		$topic->topics_to_complete = $topic_array['topics_to_complete'];
		$topic->parent_topic_id = $new_parent_id;

		if ($topic_array['details'] != null) {
			//
			$topic->details = $topic_array['details'];
		}

		if ($topic_array['critical'] != null) {
			$topic->critical = $topic_array['critical'];
			$topic->alert_before_nodes = $topic_array['alert_before_nodes'];
		}

		$topic->container_program_id = $GLOBALS['new_lt_id'];
		$topic->created_by = $GLOBALS['loggedInUserId'];
		$topic->updated_by = $GLOBALS['loggedInUserId'];

		$topic->save();
		$topic_id = $topic->id;
		//setting icon
		if ($topic_array['photo'] != null) {
			self::copyProgramLTPhoto($topic_array['photo'], $topic_array['photo_path'], $topic_id, $GLOBALS['new_lt_id']);
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		return $topic->id;
	}

	public static function gettingTopicsByLevel($parent_topic) {
		//
		$topics = Topic::where(['container_learning_tree_id' => $GLOBALS['old_lt_id'], 'parent_topic_id' => $parent_topic])->orderBy('sequence', 'asc')->get();

		return $topics;
	}

/////////////////////////////////////////////////
	public static function updateTopic($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//getting data
		$topic_id = DB::transaction(function () use ($data) {

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->party_id;
			}
			$data['masterDataInterface'] = $data['masterDataInterface'];
			//storage configurations
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;
			if (isset($data['node_icon'])) {
				if (isset($data['photo_path'])) {
					$data['remove_file'] = $data['photo_path'];
					self::deleteProgramPhoto($data);
				}
			}
			if (isset($data['node_icon'])) {
				self::uploadProgramLTIcon($data, $data['containerLearningTree'], $data['id']);
			} else {
				//also check if uploaded photo_path does'nt belong to 'topic/' -- not

				if (isset($data['entity_id'])) {
					$top = ProgramLtTopic::find($data['id']);
					Log::debug('checking');
					if (!$top['photo']) {
						Log::debug('checking1');
						$doc = Document::find($data['entity_id']);
						if ($doc['document_photo']) {
							Log::debug('checking2');
							$top->photo = $doc['document_photo'];
							$top->photo_path = $doc['document_photo_path'];
							$top->save();
						}
					}
				}
			}

			///updating topic
			$program = Program::where('id', $data['containerLearningTree'])->first();
			$data['program'] = $program;

			$containerLearningTree = $data['containerLearningTree'];

			if ($data['entity_type'] != 'MajorNode') {

				//for virtual class room
				if ($data['entity_type'] == 'Virtual Classroom') {
					//
					Log::debug('in VirtualClassRoom section');
					$details['moderators'] = $data['moderator'];
					$details['facilitators'] = $data['facilitator'];

					$role = Role::all();
					$mod_role = $role->where('name', 'Moderator')->first();
					$fac_role = $role->where('name', 'Facilitator')->first();

					// assigning roles and detaching roles
					self::roleAssigningForModeratorOnUpdate($data, $mod_role['id'], 'moderators');
					self::roleAssigningForModeratorOnUpdate($data, $fac_role['id'], 'facilitators');

					// program virtual table
					$p_v_r = ProgramVirtualRoom::where('meeting_id', $data['id'])->first();
					if ($p_v_r) {
						$p_v_r->update(['meeting_subject' => $data['topic_name'], 'meeting_duration' => $data['expected_duration'], 'start_datetime' => date('Y-m-d H:i:s', strtotime($data['program_lt_from'])), 'moderator_default_message' => $data['moderator_msg'], 'welcome_message' => $data['welcome_msg'], 'updated_by' => $data['loggedInUserId']]);
					} else {
						self::saveInProgramVirtualRooms($data['id'], $data);
					}

					// $tt = ProgramVirtualRoom::where('meeting_id', $data['id'])->update(['meeting_subject' => $data['topic_name'], 'meeting_duration' => $data['expected_duration'], 'start_datetime' => date('Y-m-d H:i:s', strtotime($data['program_lt_from'])), 'moderator_default_message' => $data['moderator_msg'], 'welcome_message' => $data['welcome_msg'], 'updated_by' => $data['loggedInUserId']]);

					//
				} elseif ($data['entity_type'] == 'Assignment') {

					// checking action
					$action = $data['activityAction'];
					$details['activityAction'] = $action;
					$file = [];
					$submitType = 'submit';

					$topic = ProgramLtTopic::where('id', $data['id'])->first();
					$old_details_array = json_decode($topic['details'], true);

					if ($action == 'links' || $action == 'editDoc') {
						if (isset($data['assignment_file'])) {
							$assignment_artefact = ArtefactType::where('name', 'Assignment')->first();
							$paths = DocumentController::getArtefactTypePaths($assignment_artefact['id']);
							$Info = self::uploadFile($data, $paths);

							foreach ($Info['uploadedFileNames']['assignment_file'] as $key => $value) {
								$name = $Info['originalFileNames']['assignment_file'][$key];
								$file['name'] = $name;
								$file['path'] = $paths['temp_path'] . $value;
							}
							//delete older file if exist
						} //new changes for activity
						else {
							$file = $old_details_array['files'];
						}
					} elseif ($action = 'file') {
						//
						if ($data['submitType'] == 'submit') {
							$submitType = 'submit';
						} else {
							$submitType = 'save';
						}

					} else {
						// check if previous action was link .. then delete
						if ($old_details_array['activityAction'] = 'links') {
							// delete old file
						}
					}

					// checking earliear once
					// $topic = ProgramLtTopic::where('id', $data['id'])->first();
					// $details_array = json_decode($topic['details'], true);

					// Log::debug($details_array['files']);
					// foreach ($details_array['files'] as $key => $value) {
					// 	//
					// 	Log::debug($value);
					// 	$check = implode('', explode('.', $value['name']));
					// 	if (isset($data[$check])) {
					// 		Log::debug($value);

					// 		$file[] = ['name' => $value['name'], 'path' => $value['path']];
					// 	} else {
					// 		// delete files
					// 		self::deleteFiles($value['path']);
					// 	}
					// }
					Log::debug($file);
					$details['submitType'] = $submitType;
					$details['files'] = $file;

				} else {

					$details['entity_name'] = $data['entity_name'];
					$details['entity_id'] = $data['entity_id'];
					$details['entity_url'] = $data['entity_url'];

					$details['entity_duration'] = $data['entity_duration'];
					$details['entity_size'] = $data['entity_size'];
					$details['entity_description'] = $data['entity_description'];

					$details['entity_pages'] = $data['entity_pages'];
					$details['entity_title'] = $data['entity_title'];
					$details['entity_summary'] = $data['entity_summary'];
				}

				$details['entity_type'] = $data['entity_type'];

				// getting type
				if ($data['entity_type'] == 'Pdf' || $data['entity_type'] == 'Ppt' || $data['entity_type'] == 'Word' || $data['entity_type'] == 'Exel' || $data['entity_type'] == 'Text' || $data['entity_type'] == 'Web') {
					$entity_type_name = 'Document';
				} else if ($data['entity_type'] == 'Youtube' || $data['entity_type'] == 'Vimeo') {
					$entity_type_name = 'Video';
				} else {
					$entity_type_name = $data['entity_type'];
				}
				$artefact_type = ArtefactType::where('name', $entity_type_name)->first();
				Log::debug($artefact_type['id']);
				if ($artefact_type['id'] != null) {
					$details['artefact_type_id'] = $artefact_type['id'];

				} else {
					$details['artefact_type_id'] = null;
				}

				$details_jason = json_encode($details);
			} else {
				$details_jason = NULL;
				if (!$data['topics_to_complete']) {
					$data['topics_to_complete'] = 0;
				}
			}

			Log::debug($details_jason . 'rrrrrr');

			//for critical nodes
			if (array_key_exists("critical_topic", $data)) {
				$critical = '1';
				$nodes_before = $data['alert_before_nodes'];
			} else {
				$critical = '0';
				// $nodes_before=$data['alert_before_nodes'];
				$nodes_before = 0;
			}
			if (array_key_exists('is_optional', $data)) {
				//
				$optional = '1';
			} else {
				$optional = '0';
			}
			Log::debug($critical);

			// updating icon
			// 'icon' => $data['node_icon']

			// status_color
			$topic_status_color_allocation_method = null;
			if (isset($data['topic_status_color_allocation_method'])) {
				if ($data['topic_status_color_allocation_method'] != '') {
					$topic_status_color_allocation_method = $data['topic_status_color_allocation_method'];
				}
			}

			// durations
			$available_from_update = date('Y-m-d H:i:s', strtotime($data['program_lt_from']));
			$available_to_update = date('Y-m-d H:i:s', strtotime($data['program_lt_to']));

			$topic_id = ProgramLtTopic::where('id', $data['id'])->update(['name' => $data['topic_name'], 'description' => $data['topic_description'], 'expected_duration' => $data['expected_duration'], 'details' => $details_jason, 'critical' => $critical, 'optional' => $optional, 'alert_before_nodes' => $nodes_before, 'topics_to_complete' => $data['topics_to_complete'], 'available_from' => $available_from_update, 'available_to' => $available_to_update, 'status_color_allocation_method' => $topic_status_color_allocation_method, 'updated_by' => $data['loggedInUserId']]);

			// apply date to child nodes
			$childDateUpdateStatus = self::applyDatesToChildTopics($data['id'], $available_from_update, $available_to_update);

			// updating topic_reminder_config table for date change.or do it for publish json change
			$topicRC = TopicReminderConfiguration::where('topic_id', $data['id']);
			$topicRC_object = $topicRC->first();
			if ($topicRC_object) {
				//
				$time_before_start = $topicRC_object['before_start_remind_duration'];
				$time_before_end = $topicRC_object['before_end_remind_duration'];

				Log::debug('topicremindupdate');
				$date_time_before_start = ProgramReminderService::calculateRemindDate($time_before_start, $available_from_update);
				$date_time_before_end = ProgramReminderService::calculateRemindDate($time_before_end, $available_to_update);

				$topicRC->update(['before_start_remind_date' => $date_time_before_start, 'before_end_remind_date' => $date_time_before_end, 'updated_by' => $data['loggedInUserId']]);
			}

			// creating returned data -- check for both tab and child  child remianing
			$returned_array['topic_id'] = $data['id'];
			$returned_array['topic_name'] = $data['topic_name'];
			if (isset($data['sequence'])) {
				$returned_array['sequence'] = $data['sequence'];
			}
			// $returned_array['facilitator_id'] = $data['facilitator'];
			$returned_array['entity_type'] = $data['entity_type'];

			// for svg
			return $returned_array;
		});

		return $topic_id;
	}

////////  delete topic under configuration starts \\\\\\\\\\\\\\\\\\\\
	public static function deleteTopic($id, $containerLearningTree, $tab, $masterDataInterface) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($id . ' ' . $containerLearningTree);

		$object_details = DB::transaction(function () use ($id, $containerLearningTree, $tab, $masterDataInterface) {

			// deleting from database
			self::deletChildUnderTab($id);
			array_push(self::$delete_topic_array, $id);

			// deleting program virtual room  if topic is vc
			$vc_artefact = ArtefactType::where('name', 'Virtual Classroom')->first();
			Log::debug(self::$vc_delete_array);
			Log::debug(self::$delete_topic_array);

			$vc_topics = ProgramLtTopic::whereIn('id', self::$vc_delete_array)->where('artefact_type_id', $vc_artefact['id'])->get();

			if (count($vc_topics) > 0) {
				self::deleteProgramVirtualRooms($vc_topics);
			}

			self::deleteTopicsFromDatabase();

			if ($tab == 0) {
				// its node
				// container learning tree is parent topic
				$topic_array = ProgramLtTopic::where('parent_topic_id', $containerLearningTree)->orderBy('sequence', 'asc')->get();

				$returned_array = self::gettingTopicObject($topic_array);

			} else {
				// its tab
				$returned_array = self::gettingTabObject($containerLearningTree, $tab);
			}

			return $returned_array;
		});
		return $object_details;
	}

	// public static function deleteFromDatabase($id) {
	// 	//
	// 	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

	// 	$u = ProgramLtTopic::where('id', $id)->update(['deleted_by' => auth()->user()->id]);
	// 	$d = ProgramLtTopic::where('id', $id)->delete();

	// 	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished" . $d);
	// }

	public static function deleteTopicsFromDatabase() {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$u = ProgramLtTopic::whereIn('id', self::$delete_topic_array)->update(['deleted_by' => auth()->user()->id]);

		$d = ProgramLtTopic::whereIn('id', self::$delete_topic_array)->delete();

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished" . $d);
	}

	public static function deletChildUnderTab($parent_id) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$child_topics = ProgramLtTopic::where('parent_topic_id', $parent_id)->get();
		Log::debug($child_topics);

		if (count($child_topics) > 0) {

			foreach ($child_topics as $key => $topic_array) {
				//
				self::deletChildUnderTab($topic_array['id']);
				array_push(self::$delete_topic_array, $topic_array['id']);
			}
			// deleting all child topics
			//
			// $u = ProgramLtTopic::where('parent_topic_id', $parent_id)->update(['deleted_by' => auth()->user()->id]);
			// $d = ProgramLtTopic::where('parent_topic_id', $parent_id)->delete();

		} else {
			// getting ids for moderator check and photo check
			Log::debug('else checking ' . $parent_id);
			array_push(self::$vc_delete_array, $parent_id);
		}
	}

	public static function deleteProgramVirtualRooms($vc_topics) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(count($vc_topics));
		Log::debug($vc_topics);

		foreach ($vc_topics as $key => $value) {
			//
			programVirtualRoom::where('meeting_id', $value['id'])->update(['deleted_by' => auth()->user()->id]);
			programVirtualRoom::where('meeting_id', $value['id'])->delete();
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// exit();
	}

	/////

	public static function getTopicForEdit($id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//
		$topic = ProgramLtTopic::where('id', $id)->first();
		Log::debug($topic);

		$artefact = ArtefactType::where('name', 'Virtual Classroom')->first();

		if ($topic['artefact_type_id'] == $artefact['id']) {
			//
			$programVirtualRoom = ProgramVirtualRoom::where('meeting_id', $id)->first();
			$topic['moderator_default_message'] = $programVirtualRoom['moderator_default_message'];
			$topic['welcome_message'] = $programVirtualRoom['welcome_message'];
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		return $topic;
	}

	public static function getParentStructure($containerLearningTree, $id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$check = DB::transaction(function () use ($containerLearningTree, $id) {

			//
			$topic_array = ProgramLtTopic::where(['container_program_id' => $containerLearningTree, 'parent_topic_id' => $id])->orderBy('sequence', 'asc')->get();

			$object = self::gettingTopicObject($topic_array);

			Log::debug($object);
			//
			return $object;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $check;
	}

	public static function gettingTopicObject($topic_array) {

		//
		$object = [];
		$i = 0;
		foreach ($topic_array as $key => $topic_data) {
			//
			$i++;

			$object[$i]['topic_id'] = $topic_data['id'];
			$object[$i]['topic_name'] = $topic_data['name'];
			$object[$i]['sequence'] = $topic_data['sequence'];

			// $object[$i]['entity_type']=$topic_data['artefact_type_id'];
			if ($topic_data['details'] != null) {
				//
				$details = json_decode($topic_data['details'], true);
				// print_r($details);
				$object[$i]['entity_type'] = $details['entity_type'];
			} else {
				$object[$i]['entity_type'] = 'MajorNode';
			}
		}

		$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
		$max_cell_no = $config['config_value'];

		if ($i < $max_cell_no) {
			//
			for ($j = $i + 1; $j <= $max_cell_no; $j++) {
				$object[$j] = '';
			}
		}

		// // completing row by 5 div
		// if ($end_cell % 5 != 0 && $end_cell > 11) {
		//     //
		//     $remainder= $end_cell % 5;

		//     for($i=1; $i <= (5-$remainder); $i++){
		//         $GLOBALS['cellObject'][$end_cell + $i]=[];
		//      }
		// }
		return $object;
	}

	public static function getUpdateStructure($containerLearningTree) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$check = DB::transaction(function () use ($containerLearningTree) {

			$object = self::gettingTabObject($containerLearningTree, 1);
			Log::debug($object);
			//
			$program = Program::where('id', $containerLearningTree)->first();

			$facilitator = self::getFacilitators($program['customer_party_id']);

			// getting company users for moderator
			$returned_array['moderator'] = self::getUsers($program['customer_party_id']);

			$returned_array['object'] = $object;
			$returned_array['facilitator'] = $facilitator;

			return $returned_array;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $check;
	}

	public static function gettingTabObject($containerLearningTree, $tab) {
		//
		$topic_tab_array = ProgramLtTopic::where(['container_program_id' => $containerLearningTree, 'parent_topic_id' => null])->orderBy('sequence', 'asc')->get();

		$i = 0;
		$tab_object = [];
		foreach ($topic_tab_array as $key => $topic_data) {
			//
			$i++;
			//
			$tab_object[$i]['topic_id'] = $topic_data['id'];
			$tab_object[$i]['topic_name'] = $topic_data['name'];
			// $tab_object[$i]['facilitator_id'] = $topic_data['facilitator_id'];
		}

		Log::debug($topic_tab_array);
		$object = [];

		if (count($topic_tab_array)) {
			$first_tab_id = $tab_object[$tab]['topic_id'];
			// print_r($topic_tab_array);
			// exit();

			$topic_array = ProgramLtTopic::where('parent_topic_id', $first_tab_id)->orderBy('sequence', 'asc')->get();
			$object = self::gettingTopicObject($topic_array);
		}

		$returned_array['tabs_object'] = $tab_object;
		$returned_array['first_tab_object'] = $object;

		return $returned_array;
	}

	public static function publishLearningTree($program_id) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		Log::debug($program_id);

		$program_lt = DB::transaction(function () use ($program_id) {
			//

			return 'hi';

		});

		//
		return 'hi';
	}

/////////////////////////////////// Assigning Roles  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	public static function roleAssigningForModeratorOnUpdate($data, $role_id, $flag) {

		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$role = Role::where('name', 'Moderator')->first();
		$program_topic = ProgramLtTopic::where('id', $data['id'])->first();

		$details = json_decode($program_topic['details'], true);

		if ($flag == 'moderators') {
			//
			if (isset($details['moderators'])) {
				$old_users = $details['moderators'];
			} else {
				$old_users = [];
			}
			$new_users = $data['moderator'];
		} else if ($flag == 'facilitators') {
			//
			if (isset($details['facilitators'])) {
				$old_users = $details['facilitators'];
			} else {
				$old_users = [];
			}
			$new_users = $data['facilitator'];
		}

		if ($old_users) {

			$common = array_intersect($new_users, $old_users);

			$usersToBeAttached = array_diff($new_users, $common);
			// $usersToBeDetached=array_diff($old_users, $common);

			Log::debug($common);

			//detaching --later mull
			// don't detach
			// foreach ($usersToBeDetached as $key => $value) {
			// }

			//attaching
			foreach ($usersToBeAttached as $key => $value) {

				// attaching role program user
				self::assigningUserRole($value, $data['program'], $role_id, $data['loggedInUserId']);
				// assigningUserRole($value, $program, $role_id, $loggedInUserId)
			}

		} else {
			//attach role
			foreach ($new_users as $key => $value) {

				// attaching role program user
				self::assigningUserRole($value, $data['program'], $role_id, $data['loggedInUserId']);
			}
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public static function assigningUserRole($value, $program, $role_id, $loggedInUserId) {
		//
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
		$i = 0;
		if (!$role_user) {
			//
			$i = 1;
			$user = self::getUserObject($value);
			Log::debug($user);
			$user->roles()->attach($role_id, ['created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);

			$new_role_user = RoleUser::where(['user_id' => $value, 'role_id' => $role_id])->first();
		}

		// attach role in program user
		if ($i == 1) {
			//
			$program->role_user()->attach($new_role_user['id'], ['user_id' => $value, 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);
		} else {
			//
			$program_role_user = ProgramUser::where(['program_id' => $program['id'], 'role_user_id' => $role_user['id']])->first();

			if (!$program_role_user) {
				//
				$program->role_user()->attach($role_user['id'], ['user_id' => $value, 'created_by' => $loggedInUserId, 'updated_by' => $loggedInUserId]);
			}
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	}

	public static function saveVcParticipants($data) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// Log::debug($_SERVER['HTTP_HOST']);
		// exit();

		$check = DB::transaction(function () use ($data) {

			//
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			unset($data['authenticatedUser']);

			$vc1_id = $data['vc1Id'];
			$vc1_topic = ProgramLtTopic::where('id', $vc1_id)->first();
			$program = Program::where('id', $vc1_topic['container_program_id'])->first();

			foreach ($data as $key => $value) {

				Log::debug($key);
				if (strpos($key, '_') != false) {
					$key_array = explode('_', $key);
					self::saveParticipantsForVc($key_array[0], $key_array[1], $vc1_topic['parent_topic_id'], $program['id'], $data['loggedInUserId']);

				}
			}
			return 1;
		});

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $check;
	}

	public static function saveParticipantsForVc($vc_id, $participant_id, $parent_topic_id, $program_id, $loggedInUserId) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($participant_id . ' ' . $parent_topic_id);

		$topicUser = TopicUser::where(['user_id' => $participant_id, 'parent_topic_id' => $parent_topic_id])->first();
		Log::debug($topicUser);

		if ($topicUser) {
			Log::debug('old record');
			if ($topicUser['topic_id'] != $vc_id) {
				//
				Log::debug('old update record');
				$topicUser->topic_id = $vc_id;
				$topicUser->updated_by = $loggedInUserId;
				$topicUser->save();

			}
		} else {
			// save new record
			Log::debug('new record');

			$topicUser = new TopicUser();
			$topicUser->topic_id = $vc_id;
			$topicUser->program_id = $program_id;
			$topicUser->user_id = $participant_id;
			$topicUser->parent_topic_id = $parent_topic_id;
			$topicUser->created_by = $loggedInUserId;
			$topicUser->updated_by = $loggedInUserId;

			$topicUser->save();
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		// return $check;
	}

	public static function applyDatesToChildTopics($parentTopicId, $available_from, $available_to) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		$status = true;
		$child_topics = ProgramLtTopic::where('parent_topic_id', $parentTopicId)->get();
		// $child_topics = ProgramLtTopic::where(['parent_topic_id'=>$parentTopicId,''])->get();
		// Log::debug($child_topics);

		if (count($child_topics) > 0) {
			foreach ($child_topics as $key => $topic_array) {
				$status = self::updateTopicDates($topic_array['id'], $available_from, $available_to);
				$status = self::applyDatesToChildTopics($topic_array['id'], $available_from, $available_to);
			}
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $status;
	}

	public static function updateTopicDates($topicId, $available_from, $available_to) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		$status = true;
		$topic = ProgramLtTopic::where('id', $topicId)->first();
		if (!$topic['available_to']) {
			$status = $topic->update(['available_from' => $available_from, 'available_to' => $available_to]);
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $status;
	}

}
