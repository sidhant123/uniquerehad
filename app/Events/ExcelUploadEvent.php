<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExcelUploadEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $folder;
    public $fileName;
    public $userId;
    public $jobId;
    public $tempPath;
    public $finalPath;
    public $uploadedFile;
    public $entityName;
    public $validationRules;
    public $entityService;
    public $saveMethod;
    public $referenceID;
    public $referenceFieldName;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($folder, $fileName, $userId, $jobId, $tempPath, $finalPath, $uploadedFile, $entityName, $validationRules,$entityService,$saveMethod,$referenceID = 0,$referenceFieldName = '')
    {

        $this->folder = $folder;
        $this->fileName = $fileName;
        $this->uploadedFile = $uploadedFile;
        $this->userId = $userId;
        $this->jobId = $jobId;
        $this->tempPath = $tempPath;
        $this->finalPath = $finalPath;
        $this->entityName = $entityName;
        $this->validationRules = $validationRules;
        $this->entityService = $entityService;
        $this->saveMethod = $saveMethod;
        $this->referenceID = $referenceID;
        $this->referenceFieldName = $referenceFieldName;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
