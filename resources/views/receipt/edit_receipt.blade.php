@extends('receipt.receipt')
@section('editID', '/'.$row_data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('edit_btn','hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Payment')

@section('project_id',$row_data->project_id)
@section('boq_no',$row_data->boq_no)
@section('invoice_no',$row_data->invoice_no)
@section('invoice_date',$row_data->invoice_date)
@section('payment_method',$row_data->payment_method)
@section('payment_ref_no',$row_data->payment_ref_no)
@section('payment_receipt_date',$row_data->payment_receipt_date)
@section('amount_received',$row_data->amount_received)
@section('gst_amount',$row_data->gst_amount)
@section('base_amount',$row_data->base_amount)
@section('invoice_amount',$row_data->invoice_amount)
@section('tds_deduction',$row_data->tds_deduction)
@section('other_deduction',$row_data->other_deduction)
@section('retension',$row_data->retension)
@section('mobilization',$row_data->mobilization)
@section('remark',$row_data->remark)

  