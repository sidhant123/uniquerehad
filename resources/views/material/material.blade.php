@extends('layouts.base')
@section('content')

  @include('errors.error_validation')
  <form style="border-style:dotted; border-width: 1px;" action="/materials{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFieldsForMaterial()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/materials'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Material

      </h3></div>
    </div>

   <div class="row">
      <div class="col-lg-6">
      
  <div class="form-group required">
              <label for="supplier_id@yield('supplier_name_d')">{{ __('labels.materials.supplier_name') }}</label>
              <select  class="form-control mandatory" name="supplier_id"
               id="supplier_id"@yield('supplier_name_d')>
                  @if(isset($supplier))
                  <option value="0">--select--</option>'
                  @foreach($supplier as $sup_val)
                  @if(isset($sel_supplier_id))
                  @if($sup_val->id === $sel_supplier_id)
                  <option value="{{$sup_val->id}}" selected>{{$sup_val->name}}</option>
                  @else
                  <option value="{{$sup_val->id}}">{{$sup_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$sup_val->id}}">{{$sup_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>    
</div>
   

   <div class="col-lg-6">
   <div class="form-group required">
       <label for="name">{{__('labels.materials.name')}}</label>
        @if(!empty(old('name')))
          <input type="text" class="form-control mandatory" id="material_name_id"  name="name" value="{{ old('name') }}" @yield('name_d')>
        @else
        <input type="text" class="form-control mandatory" id="material_name_id"  name="name" value="@yield('name')" @yield('name_d')>
        @endif        
     </div>
 </div> 
</div>

    <div class="row">
  
     <div class="col-lg-6">
       <div class="form-group required">
           <label for="code">{{__('labels.materials.code')}}</label>
            @if(!empty(old('code')))
            <input type="text" style="text-transform: uppercase;" class="form-control mandatory" name="code" value="{{ old('code') }}"  @yield('code_d')>
            @else
            <input type="text" style="text-transform: uppercase;" class="form-control mandatory" name="code" value="@yield('code')"  @yield('code_d')>
            @endif

         </div>
     </div>

       <div class="col-lg-6">
    <div class="form-group required">
           <label for="unit">{{__('labels.materials.unit')}}</label>
            @if(!empty(old('unit')))
            <input type="text" class="form-control mandatory" id="unit_id" name="unit" value="{{ old('unit') }}" @yield('unit_d') />
            @else
            <input type="text" class="form-control mandatory" id="unit_id" name="unit" value="@yield('unit')"  @yield('unit_d') />
            @endif

         </div>  
   </div>
  </div>


<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.users.submit')}}</button>
</div>
</div>
<p></p>
</form>

<p></p>


<!-- for import user -->
@section('importMaterials')
@show
@endsection

@push('scripts')
<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script>
$(document).ready(function(){

$('#supplier_id').select2({
  }); 
$('#import_supplier_id').select2({
  });

     $(".importMandatory").removeClass('fillFields');

});
function validateMandatoryFieldsForImport(){
  var valid = true;
     $(".mandatory").removeClass('fillFields');
     $(".importMandatory").removeClass('fillFields');
      document.getElementById("select2-import_supplier_id-container").style.border = "0px solid red";
     document.getElementById("select2-supplier_id-container").style.border = "0px solid red";

  console.log("In validateMandatoryFields function.");
     $(".importMandatory").each(function () {
       if ($(this).val().trim() === '' || $(this).val() === '0')
        {
            $(this).addClass('fillFields');
            valid = false;
            return valid;
        }
     });
      var import_supplier_id = $('#import_supplier_id').val();
    // alert(import_supplier_id);
      if(import_supplier_id == 0) {
    document.getElementById("select2-import_supplier_id-container").style.border = "1px solid red";
    } else {
      document.getElementById("select2-import_supplier_id-container").style.border = "0px solid red";

    }

     return valid;
} 


function validateMandatoryFieldsForMaterial(){

  var valid = true;
     document.getElementById("select2-import_supplier_id-container").style.border = "0px solid red";
     document.getElementById("select2-supplier_id-container").style.border = "0px solid red";

  valid=validateMandatoryFields();

      var supplier_id = $('#supplier_id').val();
    // alert(supplier_id);
  if(supplier_id == 0) {
document.getElementById("select2-supplier_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-supplier_id-container").style.border = "0px solid red";
}

 

     return valid;
}


$('#material_name_id').keyup(function(){
    if($(this).val().length>0){
      var character = $(this).val().charAt(0);
      if(character!=character.toUpperCase()){
          $(this).val($(this).val().charAt(0).toUpperCase()+$(this).val().substr(1));
       }
     }
});
$('#unit_id').keyup(function(){
    if($(this).val().length>0){
      var character = $(this).val().charAt(0);
      if(character!=character.toUpperCase()){
          $(this).val($(this).val().charAt(0).toUpperCase()+$(this).val().substr(1));
       }
     }
});

</script>
@endpush


