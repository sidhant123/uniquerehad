ALTER table receipts ADD column retension  numeric(12,2);
ALTER table receipts ADD column mobilization  numeric(12,2);


alter table receipts alter column boq_no drop not null;


update list_view_masters set heading_columns = 
	'Payment No.,Project,BOQ No.,R A Bill No.,Invoice Date,Invoice Amount,Total Deduction Amount,Received Amount,Action'
	where entity_name = 'Receipt';