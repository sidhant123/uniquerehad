/* select * from parties -- id = 1,2 
select * from users order by party_id -- id = 1
select * from organizations order by party_id -- id = 1
select * from tenants order by party_id -- id =1

select * from role_user order by id
select * from organization_attributes order by organization_id 
select * from people_attributes 
select * from phones order by party_id 
select * from addresses order by party_id -- left 2 id = 142,144
select * from users order by party_id -- id=1
select * from people order by party_id -- id=1

select * from customers where id = 84
select * from user_attributes  */


-- ### bo tables ## ---
update customers set created_by = 1 , updated_by =1;
delete from user_attributes;
delete from people_attributes;
delete from organization_attributes;
delete from customers where id in (1,2,3,5);
delete from role_user where id not in (1);
delete from people where id not in (1);
delete from addresses where id not in (142,144);
delete from phones where id not in (255,254,162);

delete from users  where id not in (1);
delete from organizations where id not in (1,2);
delete from tenants where id not in (1);

--delete from parties where id not in (1,2);
delete from manpower_contracts;
delete from manpower_documents ;
delete from manpowers;
delete from projects;
delete from boqs;
delete from sites;
delete from materials;
delete from payments;
delete from expense_types;
delete from expenses;

delete from documents;

--- ### fo tables -- 

delete from material_request_details;
delete from material_inwards_details;
delete from material_transfer_details;
delete from dprs;
delete from complaint_documents;
delete from complaints;
delete from petty_cash;
delete from petty_cash_details;

delete from material_inwards_documents;
delete from material_inwards_details;
delete from material_inwards_headers; 
delete from material_consumption_documents;
delete from material_consumption_details;
delete from material_consumption_headers;
delete from material_transfer_documents;
delete from material_transfer_details;

delete from material_transfer_headers;
delete from material_audit_documents;
delete from material_audit_details;

delete from material_audit_headers;
delete from manpower_documents;
delete from attendance_documents;
delete from manpowers;
