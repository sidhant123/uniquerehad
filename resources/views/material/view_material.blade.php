@extends('material.material')
@section('title','View Project')
@section('view-nav-bar', 'hide')
@section('save-nav-bar', 'disabled')
@section('editID', $row_data->id)


@section('title','Edit Site')
@section('name',$row_data->name)
@section('code',$row_data->code)
@section('unit',$row_data->unit)


@push('scripts')

<script>
$(document).ready(function() {
  $('#save_link').hide();
  $("input").attr("readonly", true);
  $('#supplier_id').prop('disabled', true);
  $('.form-control').attr('readonly',true);
});
</script>

@endpush
