@extends('layouts.base')
@section('content')
<form action="/profitLossReport" id="ExportProfitLossReportForm" method="post" >
  {{ csrf_field() }}
  <input type="hidden" name="report_type" id="report_type">
  <input type="hidden" name="entity_type" id="entity_type">
  @section('editMethod')
  @show
  <h3 align="center">Export Profit and Loss Report</h3>

    <strong>NOTE**</strong><p></p>
<textarea class="form-control" style="width:100%;" rows="4" cols="30" readonly="readonly">
Please select project name for download profit and loss report.
</textarea>
    <p></p>

  <div class="row">
    <div class="col-lg-3">
   
 <div class="form-group">
              <label for="project_id"></label>
              <select  class="form-control mandatory" name="project_id" id="project_id")>
                  <option value="0">--Project--</option>'
                  @if(isset($project))
                  @foreach($project as $pro_val)
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                @endforeach
                  @endif
              </select>
       </div>
    </div>
    <div class="col-lg-3">
      <div class="form-group">
        <label for=""></label>
        <br>
        <button id="btnCompletion" class="btn btn-primary" type="button" style="height: 34px;" >
          Download &nbsp;&nbsp;<i class="vnn vnn-download"></i>
        </button>
      </div>
    </div>

  </div>

  <div class="row">

  </div>
      
</form>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
     $('#project_id').select2({});


$(document).on('click', '#btnCompletion', function() {

  var valid = true;
  valid = validateMandatoryFields();

  var project_id = $('#project_id').val();
  if(project_id == 0) {
  document.getElementById("select2-project_id-container").style.border = "1px solid red";
  } else {
    document.getElementById("select2-project_id-container").style.border = "0px solid red";
  }

   if(valid) {
     $('#ExportProfitLossReportForm').submit();
   }

 });

  function getDataThroughAxios(url, value, control_id) {
   console.log(value);
   axios.get(url + value)
     .then(function(response) {
       console.log(response);
       populateData(response['data'], control_id);
     })
     .catch(function(error) {
       console.log("error" + error);
     });
 }
});
  function populateData(data, control_id) {
    $("#" + control_id).empty();
   if (Object.keys(data).length > 0) {
        $("#" + control_id).append('<option value="0">--Select--</option>');
     $.each(data, function(key, value) {
         $("#" + control_id).append("<option value='" + value.id + "'>" + value.name + "</option>");
     });
   }
 }
</script>
@endpush
