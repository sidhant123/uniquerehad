<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;
use Sofa\Eloquence\Eloquence;
/**
 * Class Program
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property int $version
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property int $allow_self_enrol
 * @property int $customer_party_id
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property array $learning_tree
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $communication_trails
 * @property \Illuminate\Database\Eloquent\Collection $forums
 *
 * @package App\Models
 */
class Program extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['name','code'];
	protected $casts = [
		'version' => 'int',
		'allow_self_enrol' => 'int',
		'customer_party_id' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
		//'learning_tree' => 'json'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];

	protected $fillable = [
		'code',
		'name',
		'description',
		'version',
		'start_date',
		'end_date',
		'allow_self_enrol',
		'customer_party_id',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by',
		'learning_tree'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\User::class, 'deleted_by');
	}

	public function communication_trails()
	{
		return $this->hasMany(\App\Models\CommunicationTrail::class);
	}

	public function forums()
	{
		return $this->hasMany(\App\Models\Forum::class);
	}
	public function role_user() {
		return $this->belongsToMany(\App\Models\RoleUser::class, 'program_user')->withPivot('id', 'user_id', 'is_active');
	}

	public function user_badges()
	{
		return $this->belongsToMany(\App\User::class, 'program_user_badge')->withPivot('id', 'max_distribution_badges', 'current_distribution_badges', 'content_rated_count', 'quiz_completion_badge_count' ,'fastest_activity_badge_count', 'content_rating_badge_count', 'facilitator_badge_count', 'participant_badge_count');
		//return $this->hasMany('App\User', 'program_user_badge');
	}
        
	public function user_quiz()
	{
		return $this->belongsToMany(\App\User::class, 'user_exercise_final_results')->withPivot('id', 'program_id', 'user_id', 'exercise_bank_id', 'exercise_name', 'total_questions', 'total_correct_answers', 'total_incorrect_answers');
	}

public function topics() {
		return $this->hasMany(\App\Models\ProgramLtTopic::class, 'container_program_id');
	}

}
