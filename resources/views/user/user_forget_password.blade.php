@extends('layouts.loginBase')
@section('content')
 <section id="login">
    <div class="container">
        <h2>Admin Panel Forgot Password</h2>
        <div class="box">
            <form method="post" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" id="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    <span>Email</span>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>

                <br/>
                <div class="clearfix"></div>
                <button type="submit" class="share-btn">Send Password Reset Link</button>
                <div class="clearfix"></div>
            </form>
            <p>By login in, you agree to the <a href="#">Terms & Conditions</a> of Cognegix Digital Learning
                Redefined</p>
        </div>
    </div>
</section>
@endsection