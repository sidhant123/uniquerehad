<?php

namespace vnnogile\Mailer\Controllers;

use App\Models\CommunicationEvent;
use App\Models\Phone;
//custom
//use Illuminate\Http\Request;
use App\Models\Program;
use App\Services\CommunicationEventService;
use App\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Mailer\Utility\EmailUtility;
use vnnogile\Templates\services\TemplateUtility;
use App\Services\TemplateService;

class MailController extends BaseController
{
    private static $filePath = "/temp/email/";
    public function __construct()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public function create()
    {
        $data['masterDataInterface'] = parent::$masterDataInterface;
        $templates = TemplateService::templates();
        $customers = parent::$masterDataInterface->all('Customer');
        // $communicationEvents = CommunicationEventService::communicationEvents($data);
        return view('vnnogile::create_mail', compact('customers','templates'));
    }
    public function getTemplatesFromCommEvent($comm_event_id)
    {
        $commEvent = CommunicationEvent::find($comm_event_id);
        return $commEvent->templates()->get();
    }
    public function sendMail(Request $request)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
        $data = array('team_name' => 'Mumbai Indians');
        $attachments = array();
        $request_data = $request->all();
        $to = $request_data['to_emails'];
        if (isset($request_data['cc_emails'])) {
            $cc = $request_data['cc_emails'];
        }
        if (isset($request_data['bcc_emails'])) {
            $bcc = $request_data['bcc_emails'];
        }
        if (!empty($request_data['attachments'])) {
            $attachments = explode(',', $request_data['attachments']);
        }
        if(!empty($request_data['program_id'])){
        $participant_ids = explode(',', $request_data['participants']);
        Log::debug($participant_ids);
        foreach ($participant_ids as $key => $id) {
        $replaceData = $this->replacePlaceHolders($request_data['program_id'],$id,$request_data['summernote'],$request_data['subject']);
        $to = $replaceData['to'];
        $subject = $replaceData['subject'];
        //$template = TemplateUtility::convertBase64ToImgfileAndReplaceSrc($request_data['data']);
	$template = $request_data['html_data'];//
        $emailData = array('template_name' => $replaceData['template'],
            'data' => $data,
            'to' => $to,
            'attachments' => $attachments,
            'subject' => $subject);
        if (isset($cc)) {
            $emailData['cc'] = $cc;
        }
        if (isset($bcc)) {
            $emailData['bcc'] = $bcc;
        }
        Log::debug('Before sending emails');
        Log::debug($emailData);
        EmailUtility::sendEmail($emailData, parent::$masterDataInterface, parent::$utilsInterface);
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        \Session::flash('status', __('labels.flash_messages.store'));
            // return redirect()->action('\vnnogile\Mailer\Controllers\MailController@create');
        }else{
        $subject = $request_data['subject'];
        $template = TemplateUtility::convertBase64ToImgfileAndReplaceSrc($request_data['data']);
        $emailData = array('template_name' => $request_data['summernote'],
            'data' => $data,
            'to' => $to,
            'attachments' => $attachments,
            'subject' => $subject);
        if (isset($cc)) {
            $emailData['cc'] = $cc;
        }
        if (isset($bcc)) {
            $emailData['bcc'] = $bcc;
        }
        Log::debug('Before sending emails');
        Log::debug($emailData);
        EmailUtility::sendEmail($emailData, parent::$masterDataInterface, parent::$utilsInterface);
        Log::debug('MailController::sendMail finished');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        \Session::flash('status', __('labels.flash_messages.store'));
            // return redirect()->action('\vnnogile\Mailer\Controllers\MailController@create');
        }
        } catch (Exception $e) {
            Log::debug($e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $e;
        }
        
    }
    private function replacePlaceHolders($program_id,$user_id,$template,$subject){
        $template = $template;
        $subject = $subject;
        $program = Program::find($program_id);
        $user = User::where('id', $user_id)->first();
         $data['template_data'] = ['FIRST_NAME' => $user['first_name'],
            'LAST_NAME' => $user['last_name'],
            'USER_ID' => $user['userid'],
            'PASSWORD' => 'pass@123',
            'ROLE_NAME' => 'Participant',
            'PROGRAM_NAME' => $program['name'],
            'START_DATE' => $program['start_date'],
            'END_DATE' => $program['end_date']
        ];
        foreach ($data['template_data'] as $key => $value) {
            # code...
            $template = str_replace('{'.strtoupper($key).'}', $value, $template);
            $subject = str_replace('{'.strtoupper($key).'}', $value, $subject);
        }
        $data['template'] = $template;
        $data['subject'] = $subject;
        $data['to'] = $user['email'];
        return $data;
    }
    public function uploadFile(Request $request)
    {
        $request['masterDataInterface'] = parent::$masterDataInterface;
        //storage configurations
        $configurations = $request['masterDataInterface']->all('Configuration');
        foreach ($configurations as $configuration) {
            $configurationsMap[$configuration->configKey] = $configuration->configValue;
        }
        $request['configurations'] = $configurationsMap;
        //end checking configurations
        $file_path = $this->uploadAttachment($request);
        $renameFile = $this->renameUploadedFile($file_path);
        $file['original_name'] = $renameFile;
        $file['uploaded_file_path'] = storage_path() . "/app" . self::$filePath;
        return $file;
    }
    public function deleteFile(Request $request)
    {
        $request = $request->all();
        $request['masterDataInterface'] = parent::$masterDataInterface;
        //storage configurations
        $configurations = $request['masterDataInterface']->all('Configuration');
        foreach ($configurations as $configuration) {
            $configurationsMap[$configuration->configKey] = $configuration->configValue;
        }
        $request['configurations'] = $configurationsMap;
        $class = $request['configurations']['storage_implementation_class'];
        $storageUtility = new $class;
        Log::debug('Before deleting file');
        Log::debug(self::$filePath . $request['fileName']);
        if ($storageUtility->deleteFile(str_replace(storage_path() . "/app",'',$request['fileName']))) {
            return array("Deleted Successfully.");
        } else {
            return array("Some error occured.");
        }
    }
    private function uploadAttachment($data)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        $class = $data['configurations']['storage_implementation_class'];
        $storageUtility = new $class;
        return $storageUtility->uploadFileToPrivateStorage($data, self::$filePath);
    }
    private function renameUploadedFile($uploadedFile)
    {
        $countFile = self::countFile($uploadedFile['originalFileNames']['file'][0]);
        if ($countFile == 0) {
            rename(storage_path() . "/app" . self::$filePath . $uploadedFile['uploadedFileNames']['file'][0], storage_path() . "/app" . self::$filePath . $uploadedFile['originalFileNames']['file'][0]);
            return $uploadedFile['originalFileNames']['file'][0];
        } else {
            $tempFileName = explode('.', $uploadedFile['originalFileNames']['file'][0]);
            rename(storage_path() . "/app" . self::$filePath . $uploadedFile['uploadedFileNames']['file'][0], storage_path() . "/app" . self::$filePath . $tempFileName[0] . "(" . $countFile . ")." . $tempFileName[1]);
            return $tempFileName[0] . "(" . $countFile . ")." . $tempFileName[1];
        }

    }
    private function countFile($fileOriginalName)
    {
        $countFile = 0;
        $fileName = explode('.', $fileOriginalName);
        while (true) {
            if ($countFile == 0) {
                if (file_exists(storage_path() . "/app" . self::$filePath . $fileOriginalName)) {
                    Log::debug("FILECOUNT:" . storage_path() . "/app" . self::$filePath . $fileOriginalName);
                    $countFile++;
                } else {
                    break;
                }
            } else {
                if (file_exists(storage_path() . "/app" . self::$filePath . $fileName[0] . "(" . $countFile . ")." . $fileName[1])) {
                    Log::debug("FILECOUNT:" . storage_path() . "/app" . self::$filePath . $fileName[0] . "(" . $countFile . ")." . $fileName[1]);
                    $countFile++;
                } else {
                    break;
                }
            }
        }
        Log::debug("FILECOUNT:" . $countFile);
        return $countFile;
    }
    public function oldsendMail(Request $request)
    {
        // Log::debug($request->all());
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $request = $request->all();
        $commEvent = CommunicationEvent::find($request['communication_event_id']);
        $program = Program::find($request['program_id']);
        $data = $request;
        $data['authenticatedUser'] = 1; //\Auth::user();
        $data['program_name'] = $program->name;
        $data['start_date'] = $program->start_date;
        $data['end_date'] = $program->end_date;
        $this->sendEmailViaCommEvent($commEvent->name, $data);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

    }
    public function sendEmailViaCommEvent($eventName, $data)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $authenticatedUser = $data['authenticatedUser'];
        $login_id = $data['authenticatedUser'];
        $participant_ids = explode(',', $data['participants']);
        foreach ($participant_ids as $key => $id) {
            $data['Participant'][$id] = self::getData($id, $data, 'CGX', 'Participant');
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        Log::debug($data);
        event(new CommEvent($eventName, $data, 'CGX', \Auth::user()->id));
        // event(new CommEvent("", $data, $tenant_code, $login_id));
    }
    public static function getData($id, $entityData, $customer_org_code, $role_name)
    {
        //
        $attachments = array();
        if (!empty($entityData['attachments'])) {
            $attachments = explode(',', $entityData['attachments']);
        }
        $user_object = self::getUserObject($id);

        // finding phone number
        $phone = Phone::where('party_id', $user_object['party_id'])->first();

        // creating data
        $data['template_data'] = ['FIRST_NAME' => $user_object['first_name'],
            'LAST_NAME' => $user_object['last_name'],
            'ROLE_NAME' => $role_name,
            'PROGRAM_NAME' => $entityData['program_name'],
            'START_DATE' => $entityData['start_date'],
            'END_DATE' => $entityData['end_date'],
            'USER_ID' => $user_object['userid'],
            'PASSWORD' => 'pass@123',
        ];

        $data['email_id'] = $user_object['email'];
        $data['phone_no'] = $phone['phone_number'];
        $data['customer_organization_code'] = $customer_org_code;
        $data['party_id'] = $user_object['party_id'];
        $data['attachments'] = $attachments;

        return $data;
    }
    private static function getUserObject($user_id)
    {
        $user = User::where('id', $user_id)->first();
        return $user;
    }
}
