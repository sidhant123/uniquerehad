@extends('layouts.base')
@section('content')

  @include('errors.error_validation')
  <form style="border-style:dotted; border-width: 1px;" action="/expenseTypes{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFields()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/expenseTypes'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Expense Type

      </h3></div>
    </div>

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="name">{{__('labels.expense_types.name')}}</label>
            @if(!empty(old('name')))
              <input type="text" class="form-control mandatory"  name="name"  style="text-transform:capitalize;" value="{{ old('name') }}" @yield('name_d')>
            @else
            <input type="text" class="form-control mandatory"  name="name"  style="text-transform:capitalize;" value="@yield('name')" @yield('name_d')>
            @endif        
         </div>
     </div>
     <div class="col-lg-6">
       <div class="form-group ">
           <label for="description">{{__('labels.expense_types.description')}}</label>
            @if(!empty(old('description')))
            <input type="text" class="form-control" name="description" value="{{ old('description') }}"  @yield('description_d')>
            @else
            <input type="text" class="form-control" name="description" value="@yield('description')"  @yield('description_d')>
            @endif

         </div>
     </div>
  </div>


   <div class="row">
   <div class="col-lg-6">

    <div class="form-group ">
                <label><input type="checkbox" id="track_payment" name="track_payment">{{__('labels.expense_types.track_payment')}}</label>

         </div>  
   </div>
</div>
<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.expense_types.submit')}}</button>
</div>
</div>
<p></p>
<!-- for import user -->
@section('importBoq')
@show
@endsection



@push('scripts')

<p></p>

<p></p>

</div>
</form> 

<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script>


</script>
@endpush


