<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace vnnogile\Tags\services;

//Custom imports
use App\Models\EntityTag;
use App\Models\MediaTag;
use Illuminate\Support\Facades\Log;

/**
 *
 *
 */
class TagService {
	public static function saveTags($document_id, $artefact_type, $tag) {
		try {
			Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

			$tags = explode(',', $tag);
			$mediaTag = new MediaTag;
			$mediaTag->media_id = $document_id;
			$mediaTag->media_type = $artefact_type;
			$mediaTag->tags = json_encode($tags);
			$mediaTag->created_by = 1;
			$mediaTag->updated_by = 1;
			$mediaTag->save();
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		} catch (Exception $e) {
			Log::debug(print_r($e, true));
		}
	}

	public static function updateTags($tag_id, $tag) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$tagData = self::getTags($tag_id);
		$mediaTag = MediaTag::find($tagData->id);
		$tags = explode(',', $tag);
		$mediaTag->update(['tags' => json_encode($tags)]);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}
	public static function saveEntityTags($entity_id, $entity_name, $tag, $user_id = 1) {
		try {
			Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

			$tags = explode(',', $tag);
			$entityTag = new EntityTag;
			$entityTag->entity_name = $entity_name;
			$entityTag->entity_id = $entity_id;
			$entityTag->tags = json_encode($tags);
			$entityTag->created_by = $user_id;
			$entityTag->updated_by = $user_id;
			$entityTag->save();
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		} catch (Exception $e) {
			Log::debug(print_r($e, true));
		}
	}

	public static function updateEntityTags($entity_id, $entity_name, $tag, $user_id = 1) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$tagData = self::getEntityTags($entity_id, $entity_name);
		$entityTag = EntityTag::find($tagData->id);
		$tags = explode(',', $tag);
		$entityTag->update(['tags' => json_encode($tags), 'updated_by' => $user_id]);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}
	public static function getTags($id) {
		return MediaTag::select('id', 'tags')->where('media_id', $id)->first();
	}
	public static function getEntityTags($id, $entity_name) {
		return EntityTag::select('id', 'tags')->where([
			['entity_id', '=', $id],
			['entity_name', '=', $entity_name],
		])->first();
	}
	public static function makeModel($entityName, $flag = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		if ($flag) {
			$model = \App::make("App\Models\\" . $entityName);
		} else {
			$model = \App::make("App\Models\\" . $entityName . "Tag");
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $model;
	}
	public static function saveCommonEntityTag($entity_id, $entity_name, $tag, $user_id = 1) {
		try {
			Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

			$tags['tags'] = explode(',', $tag);
			$model = self::makeModel($entity_name);
			$entityTag = new $model;
			$entityTag->entity_id = $entity_id;
			$entityTag->tags = json_encode($tags);
			$entityTag->created_by = $user_id;
			$entityTag->updated_by = $user_id;
			$entityTag->save();
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		} catch (Exception $e) {
			Log::debug($e);
		}

	}
	public static function updateCommonEntityTag($entity_id, $entity_name, $tag, $user_id = 1) {
		try {
			Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
			$model = self::makeModel($entity_name);
			$tags['tags'] = explode(',', $tag);
			$model::where('entity_id', $entity_id)->update(['tags' => json_encode($tags), 'updated_by' => $user_id]);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		} catch (Exception $e) {
			Log::debug($e);
		}

	}
	public static function getCommonEntityTag($entity_id, $entity_name) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$model = self::makeModel($entity_name);
		$tagData = $model::select('id', 'tags')->where('entity_id', $entity_id)->first();
		if (count($tagData) > 0) {
			$tagData['tags'] = json_decode($tagData->tags, true);
		} else {
			$tagData['tags'] = array();
		}

		return $tagData;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}
	public static function searchTagsByEntity($tags, $entity_name, $entity_id = 0, $flag = false) {
		if (!$entity_id) {
			$model = self::makeModel($entity_name);
			$tags = str_replace(',', '|', $tags);
			$tagdata = $model::select('entity_id')->whereRaw('tags_virtual REGEXP "' . $tags . '"')->get();
			$EntityIdArray = array();
			foreach ($tagdata as $key => $value) {
				array_push($EntityIdArray, $value->entity_id);
			}
			$model = self::makeModel($entity_name, true);
			if ($flag) {
				return $model::whereIn('id', $EntityIdArray);
			} else {
				return $model::whereIn('id', $EntityIdArray)->get();
			}

			// select * from media_tags where tags_virtual REGEXP 'kris|rad';
		}
	}
	public static function searchTagsForDocuments($tags, $entity_name, $entity_column_id, $entity_types_id, $artifact_type) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$model = self::makeModel($entity_name);
		$tags = str_replace(',', '|', $tags);
		$tagdata = $model::select('entity_id')->whereRaw('tags_virtual REGEXP "' . $tags . '"')->get();
		Log::debug($tagdata);
		$EntityIdArray = array();
		foreach ($tagdata as $key => $value) {
			array_push($EntityIdArray, $value->entity_id);
		}
		$model = self::makeModel($entity_name, true);
		Log::debug($entity_types_id);
		if ($entity_types_id > 0) {
			$conditions = array($entity_column_id => $entity_types_id);
			if ($entity_name = 'Document') {
				// $conditions = array_merge($conditions, array('extension' => $artifact_type));
			}
			Log::debug($conditions);
			Log::debug($EntityIdArray);
			Log::debug(self::getDocExtension($artifact_type));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			if(self::getDocExtension($artifact_type)){
			return $model::whereIn('id', $EntityIdArray)->where($conditions)->whereIn('extension', self::getDocExtension($artifact_type))->get();
			}else{
			return $model::whereIn('id', $EntityIdArray)->where($conditions)->get();	
			}	
			

		} else {
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $model::whereIn('id', $EntityIdArray)->get();
		}
	}
	public static function getDocExtension($docType) {
		$extensions;
		switch ($docType) {
		case 'Ppt':
			# code...
			$extensions = ['pptx', 'ppt'];
			break;

		case 'Pdf':
			# code...
			$extensions = ['pdf'];
			break;

		case 'Word':
			# code...
			$extensions = ['doc', 'docx'];
			break;

		case 'Exel':
			# code...
			$extensions = ['xlsx', 'xls'];
			break;

		case 'Text':
			# code...
			$extensions = ['txt'];
			break;

		case 'Web':
			# code...
			$extensions = ['web'];
			break;

		default:
			# code...
			$extensions = [];
			break;
		}
		return $extensions;
	}
}
