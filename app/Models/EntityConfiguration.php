<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class EntityConfiguration
 * 
 * @property int $id
 * @property array $properties
 * @property string $entity_name
 * @property string $cache_name
 * @property string $cache_tag_name
 * @property int $cache_duration
 * @property int $taggable
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class EntityConfiguration extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'properties' => 'json',
		'cache_duration' => 'int',
		'taggable' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'properties',
		'entity_name',
		'cache_name',
		'cache_tag_name',
		'cache_duration',
		'taggable',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
