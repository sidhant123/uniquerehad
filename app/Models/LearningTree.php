<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Sofa\Eloquence\Eloquence;
//Custom Imports
use App\Models\BaseModel;

/**
 * Class LearningTree
 * 
 * @property int $id
 * @property string $learning_tree_code
 * @property string $name
 * @property string $description
 * @property int $version
 * @property int $is_active
 * @property int $enable_completion_tracking
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $learning_tree_category_learning_trees
 * @property \Illuminate\Database\Eloquent\Collection $learning_tree_programs
 * @property \Illuminate\Database\Eloquent\Collection $topics
 *
 * @package App\Models
 */
class LearningTree extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['name','learning_tree_code'];

	protected $casts = [
		'version' => 'int',
		'is_active' => 'int',
		'enable_completion_tracking' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'learning_tree_code',
		'name',
		'description',
		'version',
		'is_active',
		'enable_completion_tracking',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function learning_tree_category_learning_trees()
	{
		return $this->hasMany(\App\Models\LearningTreeCategoryLearningTree::class, 'learning_trees_id');
	}

	public function learning_tree_programs()
	{
		return $this->hasMany(\App\Models\LearningTreeProgram::class, 'learning_trees_id');
	}

	public function topics()
	{
		return $this->hasMany(\App\Models\Topic::class, 'container_learning_tree_id');
	}
}
