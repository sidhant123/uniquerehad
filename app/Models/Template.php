<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jan 2018 14:15:46 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
use Sofa\Eloquence\Eloquence;
/**
 * Class Template
 * 
 * @property int $id
 * @property string $template_code
 * @property string $template_name
 * @property string $subject
 * @property string $template
 * @property string $template_url
 * @property int $version
 * @property string $to
 * @property string $cc
 * @property int $communication_channel_id
 * @property int $tenant_party_id
 * @property int $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\CommunicationChannel $communication_channel
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $communication_events
 *
 * @package App\Models
 */
class Template extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['template_name'];
	protected $casts = [
		'version' => 'int',
		//'communication_channel_id' => 'int',
		'tenant_party_id' => 'int',
		'is_active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'template_code',
		'template_name',
		'subject',
		'template',
		'template_url',
		'version',
		'to',
		'cc',
		'communication_channel_id',
		'tenant_party_id',
		'is_active',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function communication_channel()
	{
		return $this->belongsTo(\App\Models\CommunicationChannel::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function communication_events()
	{
		return $this->belongsToMany(\App\Models\CommunicationEvent::class)
					->withPivot('id', 'created_by', 'updated_by', 'deleted_by', 'deleted_at')
					->withTimestamps();
	}
}
