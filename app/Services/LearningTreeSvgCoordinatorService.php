<?php

// cssir

namespace App\Services;

//Custom imports
use Illuminate\Support\Facades\Log;

class LearningTreeSvgCoordinatorService {

	public static function createLearningTreeSvg($type, $learningTreeid, $title, $nodeId, $masterDataInterface, $tenantPartyId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//$type - MLT means master learning tree and PLT means Program Learning Tree
		//$nodeId - 0 means we have to fetch the first level nodes of the learning tree

		//Step 1 - Fetch the data from the database
		$topics = LearningTreeSvgDbService::retrieveNodes($type, $learningTreeid, $nodeId);
		$colors = LearningTreeSvgDbService::retrieveColors($tenantPartyId);
		$nodeCount = sizeof($topics);

		Log::debug("*************type*************");
		Log::debug("*************$type*************");
		Log::debug("*************learningTreeid*************");
		Log::debug("*************$learningTreeid*************");
		Log::debug("*************$nodeCount*************");
		if ($nodeCount > 0) {

			if ($nodeId == 0) {
				//Step 2 - fetch the size, coordindates and the viewport size
				$sizingAndCoordinates = LearningTreeSvgDbService::retrieveViewBoxSizingAndCoordinates($nodeCount, $tenantPartyId, 0);
				Log::error("*****SIZING COORDINATES**************");
				Log::error($sizingAndCoordinates);

				//Now generate the SVG
				//return  LearningTreeSvgGeneratorService::createLearningTreeSvg($nodeId, $topics, $title, $sizingAndCoordinates);

				$learningTreeService = self::getLevel1SvgGeneratorClass($masterDataInterface);
				$data = $learningTreeService::createLearningTreeSvg($nodeId, $topics, $title, $sizingAndCoordinates, $learningTreeid, $type);
			} else {
				$nodeData = LearningTreeSvgDbService::retrieveNodeHeaderData($type, $learningTreeid, $nodeId);
				if ($nodeData == "NO_DETAILS") {
					return 'Not Configured';
				} else {
					//Step 2 - fetch the size, coordindates and the viewport size
					$sizingAndCoordinates = LearningTreeSvgDbService::retrieveViewBoxSizingAndCoordinates($nodeCount, $tenantPartyId, $nodeId);

					//For internal levels
					$learningTreeService = self::getNonLevel1SvgGeneratorClass($masterDataInterface);
					$data = $learningTreeService::createLearningTreeSvg($nodeId, $nodeData, $topics, $title, $sizingAndCoordinates, $learningTreeid, $type, $colors);
				}

			}
			//$data =  LearningTreeSvgGeneratorService::createLearningTreeSvg($nodeId, $topics, $title, $sizingAndCoordinates);

		} else {
			$nodeData = LearningTreeSvgDbService::retrieveNodeData($type, $learningTreeid, $nodeId);
			if ($nodeData == "NO_DETAILS") {
				return 'Not Configured';
			} else {
				Log::debug("*************GENERATING LEAF NODE SVG*********************");
				$sizingAndCoordinates = LearningTreeSvgDbService::retrieveViewBoxSizingAndCoordinates(1, $tenantPartyId, 100);
				$learningTreeService = self::getLeafNodeSvgGeneratorClass($masterDataInterface);
				$data = $learningTreeService::createLearningTreeSvg($nodeId, $nodeData, $title, $sizingAndCoordinates, $learningTreeid, $type, $colors);
			}

		}

		//This will be done at the end after the entire structure is created
		$storageUtility = self::getStorageImplementationClass($masterDataInterface);
		//$data='This is sample data';
		$storageUtility->createFile('svgs', $type . "_" . $learningTreeid . "_" . $nodeId . ".svg", $data);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $data;
	}

	private static function getStorageImplementationClass($masterDataInterface) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$configurations = $masterDataInterface->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;

		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $storageUtility;
	}

	private static function getLevel1SvgGeneratorClass($masterDataInterface) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$configurations = $masterDataInterface->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;

		$class = $data['configurations']['level_1_svg_generation_class'];
		$svgGeneratorService = new $class;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $svgGeneratorService;
	}

	private static function getNonLevel1SvgGeneratorClass($masterDataInterface) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$configurations = $masterDataInterface->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;

		$class = $data['configurations']['non_level_1_svg_generation_class'];
		$svgGeneratorService = new $class;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $svgGeneratorService;
	}

	private static function getLeafNodeSvgGeneratorClass($masterDataInterface) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$configurations = $masterDataInterface->all('Configuration');
		foreach ($configurations as $configuration) {
			$configurationsMap[$configuration->configKey] = $configuration->configValue;
		}
		$data['configurations'] = $configurationsMap;

		$class = $data['configurations']['leaf_node_svg_generation_class'];
		$svgGeneratorService = new $class;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $svgGeneratorService;
	}

	public static function createLearningTreeSvg1($type, $id, $parentSequenceId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		//$type is whether we want to create master learning tree of program learning tree
		//Firt get the node data
		$nodeData = self::retrieveNodes($type, $id, $parentSequenceId);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $nodeData;
	}

}
