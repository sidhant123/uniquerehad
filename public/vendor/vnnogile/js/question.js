 var counter = 1;
 $(document).ready(function() {
  var isTaggable = $("#isTaggable").val();
   $('#customer_id').select2({});
   var question_type = $('#question_type_id').val();
  
   if(question_type > 0){
     console.log(question_type);
    triggerQuestionType();
   }
   if(isTaggable){

      var tagFromHidden = $('#tag_name').val();
      var tags = '';
      console.log(tags);
      if(typeof tagFromHidden != 'undefined'){
        var tagsArray = JSON.parse(tagFromHidden);  
        tags = tagsArray.join();
      }  
  var tagHtml = '';//$('.multiple').html();
  $('.multiple').each(function(){
    tagHtml = tagHtml + $(this).html();
  });
  var html = tagHtml.replace('value=""', 'value="'+tags+'"'); 
  $('#compositeDiv').append(html);
}
 });
 $(document).on('change', '#question_type_id',triggerQuestionType);

 function triggerQuestionType() {
    console.log("IN TRIGGER FUNCTION");
   var question_type =  $("#question_type_id option:selected").text();
   console.log(question_type);
   console.log('#'+ question_type.replace(/ +/g, "_").toLowerCase());
   $("#question_type_id > option").each(function() {
      $('#'+ this.text.replace(/ +/g, "_").toLowerCase()).addClass('hide');
    });
   $('#'+ question_type.replace(/ +/g, "_").toLowerCase()).removeClass('hide');
   console.log('#'+ question_type.replace(/ +/g, "_").toLowerCase());
   clearAppendedQuestionType();
 }
 $(document).on('change','#customer_id',function(){
    var id = $(this).val();
    if(id > 0){
      getDataThroughAxios('/getClustersFromCustomer/'+id,'cluster_id');
    }else{
      getDataThroughAxios('/getClusters','cluster_id');
    }
  });
$(document).on('change','#customer_id_import',function(){
    var id = $(this).val();
    if(id > 0){
      getDataThroughAxios('/getClustersFromCustomer/'+id,'cluster_import');
    }else{
      getDataThroughAxios('/getClusters','cluster_id_import');
    }
  });
 $(document).on('change','#cluster_id',function(){
    var id = $(this).val();
    if(id > 0){
      getDataThroughAxios('/getCompetencyfromCluster/'+id,'competency_name_id');
    }
  });
 $(document).on('change','#cluster_import',function(){
    var id = $(this).val();
    if(id > 0){
      getDataThroughAxios('/getCompetencyfromCluster/'+id,'competency_name_id_import');
    }
  });
  function getDataThroughAxios(url,control_id){
   axios.get(url)
  .then(function (response) {
    console.log(response);
    $("#"+control_id).empty();
    populateData(response['data'],control_id);
  })
  .catch(function (error) {
    console.log("error"+error);
  });
 }
  function populateData(data,control_id){
  if(Object.keys(data).length > 0){
  $('#'+control_id).append("<option value='0'>---Select---</option>");
   $.each(data, function (key, value) {
  $("#"+control_id).append("<option value='" + value.id + "'>"+ value.name + "</option>");
});
 }
}
 function clearAppendedQuestionType(){
  $('.appended_input').remove();
 }
