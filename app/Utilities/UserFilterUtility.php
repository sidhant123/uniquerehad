<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class UserFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('people')
                ->leftJoin('organizations', 'people.customer_party_id', '=', 'organizations.party_id')
                ->leftJoin('users', 'users.party_id', '=', 'people.party_id')
                ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('people.id as id', 'people.first_name as first_name', 'people.last_name as last_name', 'roles.name as role_name', 'people.email as email', 'organizations.id as organization_id', 'organizations.organization_code as org_code')->where('people.deleted_at', null);
            if ($request->has('role_id')) {
                if ($request['role_id'] > 0) {
                    $data->leftJoin('role_user', 'role_user.user_id', '=', 'users.id');
                }
            }
            if ($request->has('search')) {
                // $data->where('people.first_name', 'like', '%' . $request['search'] . '%');
                // $data->orWhere('people.last_name', 'like', '%' . $request['search'] . '%');
                // $data->orWhere('people.email', 'like', '%' . $request['search'] . '%');
                // $data->orWhere('roles.name', 'like', '%' . $request['search'] . '%');

                // $data->orWhere('organizations.organization_code', 'like', '%' . $request['search'] . '%');

            $data->whereRaw("
                    (lower(people.first_name) like '%" . strtolower($request['search']) . "%' OR
                    lower(people.last_name) like '%" . strtolower($request['search']) . "%' OR
                    lower(people.email) like '%" . strtolower($request['search']) . "%' OR
                    lower(roles.name) like '%" . strtolower($request['search']) . "%'

                )");
            }


            //new code starts here



            //new code ends here
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'id':
                        $data->orderBy('people.id', $request['sort']);
                        break;
                    case 'org_code':
                        $data->orderBy('organizations.organization_code', $request['sort']);
                        break;
                    case 'first_name':
                        $data->orderBy('people.first_name', $request['sort']);
                        break;
                    case 'last_name':
                        $data->orderBy('people.last_name', $request['sort']);
                        break;
                    case 'email':
                        $data->orderBy('people.email', $request['sort']);
                        break;
                        case 'role_name':
                        $data->orderBy('roles.name', $request['sort']);
                        break;
                }
            } else {
                $data->orderBy('people.id');
            }

            // dd($req);
            foreach ($filterColumns as $key => $columnName) {

                $columnName = str_replace('people.', '', $columnName);
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug(print_r($e, true));
        }
    }
}
