<?php

namespace vnnogile\SystemNotification\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Custom Import Below
use App\Models\CommunicationTrail;
use Illuminate\Support\Facades\Log;
use vnnogile\SystemNotification\Services\FirebaseImplementationService;
class SystemNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $notificationData;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notificationData)
    {
        $this->notificationData = $notificationData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Extract the template
        //And replace all placeholders
        //With the actual data
        $template = $this->notificationData['template_name'];
        $subject = $this->notificationData['subject'];
        $device_token = $this->notificationData['mobile_token'];
        $data = $this->notificationData['data'];
         Log::debug("data in SystemNotificationJob Handle");
        Log::debug($data);
        foreach($data as $key => $value){
            $template = str_replace('{'.strtoupper($key).'}', $value, $template);
            $subject = str_replace('{'.strtoupper($key).'}', $value, $subject);
        }
        

        
        if(isset($this->notificationData['trail_data'])){
            //Since we want
            $this->notificationData['trail_data']['content'] = $template;
        }
        Log::debug("After replacing the parameters");
        Log::debug($template);
        Log::debug($subject);

        //Then  send the actual email.
        // dd($this->notificationData);
        $response = FirebaseImplementationService::sendNotification($subject,$template,$device_token);
        // dd($response);
         // dd($this->notificationData['trail_data']);   
        if(isset($this->notificationData['trail_data'])){
          $trail_data = $this->notificationData['trail_data'];
          $trail_data['response'] = $response;
          SystemNotificationSuccessJob::dispatch($trail_data);
      }

      Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
  }


  public function failed($exception){
     Log::debug(__CLASS__ . "::" . __METHOD__ . " started---");

     if(isset($this->notificationData['trail_data'])){
       $trail_data=$this->notificationData['trail_data'];

       $communicationTrail=new CommunicationTrail();
       $communicationTrail->error_code=$exception->getCode();
       $communicationTrail->status="failed";
       $communicationTrail->exception=$exception->getMessage();

       if (array_key_exists('communication_event_id', $trail_data)){
        $communicationTrail->communication_event_id=$trail_data['communication_event_id'];
    }

    if (array_key_exists('login_id', $trail_data)){
        $communicationTrail->created_by=$trail_data['login_id'];
        $communicationTrail->updated_by=$trail_data['login_id'];
    }else{
        $communicationTrail->created_by=1;
        $communicationTrail->updated_by=1;
    }

    if (array_key_exists('program_code', $trail_data)){
       $communicationTrail->program_code=$trail_data['program_code'];
   }

   if (array_key_exists('customer_organization_code', $trail_data)){
       $communicationTrail->customer_organization_code=$trail_data['customer_organization_code'];
   }

   if (array_key_exists('tenant_code', $trail_data)){
    $communicationTrail->tenant_code=$trail_data['tenant_code'];
}

if (array_key_exists('content', $trail_data)){
   $communicationTrail->content=$trail_data['content'];
}

if (array_key_exists('job_id', $trail_data)){
   $communicationTrail->job_id=$trail_data['job_id'];
}

if (array_key_exists('party_id', $trail_data)){
    $communicationTrail->party_id=$trail_data['party_id'];
}


$communicationTrail->save();
$ct_id=$communicationTrail->id;
}
Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
}

}
