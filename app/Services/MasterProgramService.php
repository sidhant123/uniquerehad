<?php

namespace App\Services;

use App\Models\MasterProgram;
use Illuminate\Support\Facades\Log;
use DB;
use Auth;
use Session;
use App\Models\ProgramUserRole;

class MasterProgramService {

	public static function saveMasterProgram($data) {
		// dd($data);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {	
			// dd($data);
			// dd($data['customer_party_id']);
	        Session::flash('status', __('labels.flash_messages.store'));
	        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	        $MasterProgram = new MasterProgram();
	        $MasterProgram->customer_party_id = $data['customer_party_id'];	        
	        $MasterProgram->name = $data['master_program_name'];
	        $MasterProgram->description = $data['master_program_description'];
	        $MasterProgram->start_date = date('Y-m-d', strtotime($data['startDate']));
	        $MasterProgram->end_date = date('Y-m-d', strtotime($data['endDate']));
	        $MasterProgram->created_by = Auth::user()->id;
	        $MasterProgram->updated_by = Auth::user()->id;
	        // $MasterProgram->deleted_by = Auth::user()->id;
	        $MasterProgram->deleted_at = null;
	        $MasterProgram->save();


	        $ProgramUserRole = new ProgramUserRole();

	        $role_id =  DB::table('roles')->select('id')->where('name', 'Program Manager');

	        if($data['company_co_ordinator']!=NULL) {
		        $ProgramUserRole->master_program_id = $MasterProgram->id;
		        $ProgramUserRole->learning_theme_id = NULL;
		        $ProgramUserRole->user_id = $data['company_co_ordinator'];
		        $role_id =  DB::table('roles')->select('id')->where('name', 'Company Coordinator')->first();
		        $ProgramUserRole->role_id = $role_id;
		        $ProgramUserRole->save();
	        }
	        if($data['program_manager']!=NULL) {
		        $ProgramUserRole->master_program_id = $MasterProgram->id;
		        $ProgramUserRole->learning_theme_id = NULL;
		        $ProgramUserRole->user_id = $data['company_co_ordinator'];
		        $role_id =  DB::table('roles')->select('id')->where('name', 'Program Manager')->first();
		        $ProgramUserRole->role_id = $role_id;
		        $ProgramUserRole->save();
	        }

			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return True;
		}
		catch(Exception $e) {
			return "Some error occured on server side!";
		}
	}

	public static function updateMasterProgram($data, $id) {
		// dd($data);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {		
			// dd($data);
	        Session::flash('status', __('labels.flash_messages.store'));
	        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	        $MasterProgram = DB::table('master_programs')->where('id', $id);
	        // dd($data['customer_party_id']);
	        // $MasterProgram = new MasterProgram();
	        DB::table('master_programs')->where('id', $id)->update(
	        	['customer_party_id'=> $data['customer_party_id'],
	        	 'name'=>$data['master_program_name'],
	        	 'description'=>$data['master_program_description'],
	        	 'start_date' => date('Y-m-d', strtotime($data['startDate'])),
	        	 'end_date' => date('Y-m-d', strtotime($data['endDate'])),
	        	 'created_by'=> Auth::user()->id,
	        	 'updated_by'=> Auth::user()->id,
	        	 // 'deleted_by'=> Auth::user()->id,
        	]);
	        // dd('J');
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			// dd('j');
			return True;
		}
		catch(Exception $e) {
			return "Some error occured on server side!";
		}
	}

}
