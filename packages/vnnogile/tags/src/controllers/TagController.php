<?php

namespace vnnogile\Tags\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use vnnogile\Tags\services\TagService;
class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $tagData['vnnogile::tags'] = array(); 
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $tagData;
        //return view('vnnogile::tags');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $tagData['vnnogile::tags'] = array(); 
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $tagData;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $tagData['vnnogile::tags'] = array(); 
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $tagData;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
