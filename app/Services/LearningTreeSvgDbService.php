<?php

// cssir

namespace App\Services;

//Custom imports
use App\Models\ArtefactType;
use App\Models\LearningTree;
use App\Models\LearningTreeColor;
use App\Models\LearningTreeLeafSizeCoordinate;
use App\Models\LearningTreeSizeCoordinate;
use App\Models\LearningTreeViewboxSize;
use App\Models\Program;
use App\Models\ProgramLtTopic;
use App\Models\Topic;
use Illuminate\Support\Facades\Log;

class LearningTreeSvgDbService {

	public static function retrieveNodes($type, $learningTreeid, $nodeId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		if ($type == 'MLT') {
			//$type - MLT means master learning tree and PLT means Program Learning Tree
			//$nodeId - 0 means we have to fetch the first level nodes of the learning tree

			//Determine if it is a parent node or child node.
			if ($nodeId != 0) {
				$topic = Topic::select('details')->where(
					[
						['container_learning_tree_id', '=', $learningTreeid],
						['id', '=', $nodeId],
					])->first();

				Log::debug("***********topic details***********$topic***********");
				if (!empty($topic->details) or !$topic->details == null) {
					$topics = [];
					return $topics;
				}
			}

			//Fetch the topics data from the database for the specified nodeId
			if ($nodeId == 0) {
				$topics = Topic::select('id', 'name', 'description', 'color', 'sequence', 'details', 'available_from', 'expected_duration', 'icon', 'container_learning_tree_id')->where(
					[
						['container_learning_tree_id', '=', $learningTreeid],
					])->whereNull('parent_topic_id')->orderBy('sequence', 'desc')->get();
			} else {
				$topics = Topic::select('id', 'name', 'description', 'color', 'sequence', 'details', 'available_from', 'expected_duration', 'icon', 'container_learning_tree_id')->where(
					[
						['container_learning_tree_id', '=', $learningTreeid],
						['parent_topic_id', '=', $nodeId],
					])->orderBy('sequence', 'desc')->get();
			}
			return $topics;
		} else if ($type == "PLT") {
			//$type - MLT means master learning tree and PLT means Program Learning Tree
			//$nodeId - 0 means we have to fetch the first level nodes of the learning tree

			//Determine if it is a parent node or child node.
			if ($nodeId != 0) {
				$topic = ProgramLtTopic::select('details')->where(
					[
						['container_program_id', '=', $learningTreeid],
						['id', '=', $nodeId],
					])->first();

				Log::debug("***********topic details***********$topic***********");
				if (!empty($topic->details) or !$topic->details == null) {
					$topics = [];
					return $topics;
				}
			}

			//Fetch the topics data from the database for the specified nodeId
			if ($nodeId == 0) {
				$topics = ProgramLtTopic::select('id', 'name', 'description', 'color', 'sequence', 'details', 'available_from', 'expected_duration', 'icon', 'container_program_id')->where(
					[
						['container_program_id', '=', $learningTreeid],
					])->whereNull('parent_topic_id')->orderBy('sequence', 'desc')->get();
			} else {
				$topics = ProgramLtTopic::select('id', 'name', 'description', 'color', 'sequence', 'details', 'available_from', 'expected_duration', 'icon', 'container_program_id')->where(
					[
						['container_program_id', '=', $learningTreeid],
						['parent_topic_id', '=', $nodeId],
					])->orderBy('sequence', 'desc')->get();
			}
			return $topics;
		}

	}

	public static function retrieveNodeData($type, $learningTreeid, $nodeId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		if ($nodeId != 0) {
			if ($type == "MLT") {
				$topic = Topic::where(
					[
						['container_learning_tree_id', '=', $learningTreeid],
						['id', '=', $nodeId],
					])->first();
			} else {
				$topic = ProgramLtTopic::where(
					[
						['container_program_id', '=', $learningTreeid],
						['id', '=', $nodeId],
					])->first();
			}

			Log::debug("***********topic details***********$topic***********");
			if (!empty($topic->details) or !$topic->details == null) {
				//Since topic details are not empty - this really means that the node is a leaf node
				//If details are empty, it means that the node is a non-leaf node.
				Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
				return $topic;
			} else {
				return "NO_DETAILS";
			}
		}

	}

	public static function retrieveNodeHeaderData($type, $learningTreeid, $nodeId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		if ($nodeId != 0) {
			if ($type == 'MLT') {
				$topic = Topic::where(
					[
						['container_learning_tree_id', '=', $learningTreeid],
						['id', '=', $nodeId],
					])->first();
			}if ($type == 'PLT') {
				$topic = ProgramLtTopic::where(
					[
						['container_program_id', '=', $learningTreeid],
						['id', '=', $nodeId],
					])->first();
			}
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $topic;
		}

	}

	public static function retrieveLearningTreeInfo($type, $learningTreeid) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		if ($type == 'MLT') {
			$learningTree = LearningTree::where(
				[
					['id', '=', $learningTreeid],
				])->first();

		} else if ($type == 'PLT') {
			$learningTree = Program::where(
				[
					['id', '=', $learningTreeid],
				])->first();

		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $learningTree;

	}

	public static function retrieveViewBoxSizingAndCoordinates($nodeCount, $tenantPartyId, $level) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$viewBoxSize = LearningTreeViewboxSize::select('size')->where(
			[
				['node_count', '=', $nodeCount],
				['tenant_party_id', '=', $tenantPartyId],
			])->first();

		if ($level == 0) {
			$sizingAndCoordinates = LearningTreeSizeCoordinate::where(
				[
					['node_count', '=', $nodeCount],
					['tenant_party_id', '=', $tenantPartyId],
					['level', '=', 0],
				])->first();
		} elseif ($level == 100) {
			$sizingAndCoordinates = LearningTreeLeafSizeCoordinate::where(
				[
					['node_count', '=', $nodeCount],
					['tenant_party_id', '=', $tenantPartyId],
					['level', '=', 100],
				])->first();

		} else {
			$sizingAndCoordinates = LearningTreeSizeCoordinate::where(
				[
					['node_count', '=', $nodeCount],
					['tenant_party_id', '=', $tenantPartyId],
					['level', '!=', 0],
				])->first();

		}

		$sizingAndCoordinates['viewBoxSize'] = $viewBoxSize->size;
		return $sizingAndCoordinates;
	}

	public static function retrieveArtefactType($id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$artefactType = ArtefactType::select('name')->findOrFail($id);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $artefactType;
	}

	public static function retrieveColors($tenantPartyId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$colors = LearningTreeColor::where([
			['tenant_party_id', '=', $tenantPartyId],
		])->get();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $colors;
	}
}
