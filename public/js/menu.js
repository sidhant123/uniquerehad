function check(){
  console.log("oh! caught the event!");
  var navbarState = $('#menuNavBar').attr('class');
  if (navbarState === 'navbar-primary'){
  	//closeNav();
  	document.getElementById("menuId").style.width = "60px";
  }else if (navbarState === 'navbar-primary collapsed'){
  	openNav();
  }
  return true;
} 

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
	console.log('openNav called');
/*    document.getElementById("menuId").style.width = "15%";
    document.getElementById("main").style.marginLeft = "17%";
    document.getElementById("main").style.width = "79%";
    document.getElementById("navbar-header1").style.marginLeft = "17%";
    document.getElementById("navbar-header1").style.width = "79%";
    $('#openbars').css({display: "none"});*/
    $('#menuId').toggle();
    $('#openbars').toggle();
    $('#main_layout_content').removeClass('col-xs-12').addClass('col-xs-10');
    $('#navbar-div').removeClass('col-xs-11').addClass('col-xs-12');
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
 /*   document.getElementById("menuId").style.width = "0";
	document.getElementById("main").style.marginLeft = "4%";
	document.getElementById("main").style.width = "94%";
	document.getElementById("navbar-header1").style.marginLeft = "4%";
	document.getElementById("navbar-header1").style.width = "94%";
    $('.fa.fa-bars').css({display: "block"});*/
    $('#menuId').toggle();
    $('#openbars').toggle();
    $('#main_layout_content').removeClass('col-xs-10').addClass('col-xs-12');
    $('#navbar-div').removeClass('col-xs-12').addClass('col-xs-11');
}

function expandNode($this){
	console.log($this.attr("id"));
	$(".expanded-menu").each(function () {
			if ($(this).hasClass($this.attr("id"))){
				$(this).toggle();	
			}else{
				$(this).hide();	
			}
			
	});
}