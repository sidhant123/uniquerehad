<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        view()->composer("layouts.base", 'App\Http\ViewComposers\MenuComposer');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once __DIR__ . '/../Helpers/helpers.php';
    }
}
