<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use App\Models\FilterMaster;
use Illuminate\Support\Facades\Log;
/**
 * Class CompositionMaster
 * 
 */
class FilterMasterService
{
	public static function getFilterDataFromRouteName($routeName){
		return FilterMaster::select('function_name','route_name','row','sequence','control_type','control_id','control_text','control_data','control_name','data_url','load_event','action','field_css','label_css')->where('route_name',$routeName)->orderBy('sequence', 'asc')->get();
		 	}
}
