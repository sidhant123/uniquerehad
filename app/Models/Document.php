<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 19 Jan 2018 19:48:23 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Document
 * 
 * @property int $id
 * @property string $name
 * @property int $version
 * @property array $attributes
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $expiry_date
 * @property int $active
 * @property int $usage_count
 * @property string $external_url
 * @property int $size
 * @property int $duration
 * @property int $pages
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class Document extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		// 'version' => 'int',
		// //'attributes' => 'json',
		// 'active' => 'int',
		// 'usage_count' => 'int',
		// 'size' => 'int',
		// 'duration' => 'int',
		// 'pages' => 'int',
		// 'created_by' => 'int',
		// 'updated_by' => 'int',
		// 'deleted_by' => 'int'
	];

	// protected $dates = [
	// 	'start_date',
	// 	'expiry_date'
	// ];

	protected $fillable = [
		// 'name',
		// 'document_path',
		// 'version',
		// 'attributes',
		// 'start_date',
		// 'expiry_date',
		// 'active',
		// 'usage_count',
		// 'external_url',
		// 'size',
		// 'duration',
		// 'pages',
		// 'created_by',
		// 'updated_by',
		// 'deleted_by',
		// 'dms_channel_partner_id',
		// 'document_photo',
		// 'document_photo_path',
	];

	public function getStartDateAttribute() {
    // return date('d-m-Y', strtotime($this->attributes['start_date']));
    return \Carbon\Carbon::parse($this->attributes['start_date'])->format('d-m-Y');
  }
  	public function getExpiryDateAttribute() {
    // return date('d-m-Y', strtotime($this->attributes['expiry_date']));
    return \Carbon\Carbon::parse($this->attributes['expiry_date'])->format('d-m-Y');
  }
  public function media_tags()
    {
        return $this->hasOne('App\Models\MediaTag', 'media_id');
    }
}
