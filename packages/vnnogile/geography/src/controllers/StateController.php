<?php

namespace vnnogile\Geography\Controllers;

use Illuminate\Http\Request;

//Custom
use Config;
use vnnogile\Client\Controllers\BaseController;
use Illuminate\Support\Facades\Log;
use vnnogile\Geography\services\StateService;
use vnnogile\Pagination\Utilities\PaginateController;


class StateController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 public function __construct()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public function index(Request $request)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

        return PaginateController::paginate('State', $request->path(), parent::$utilsInterface, parent::$masterDataInterface);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {     
    	$country_list = StateService::getCountryList();
        return view('vnnogile::create_state', compact('country_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_obj = $request->all();
        // print_r($request_obj);
        // exit();
        try {
            $request_obj['authenticatedUser'] = \Auth::user();
            StateService::saveState($request_obj);
            return redirect()->action('\vnnogile\Geography\Controllers\StateController@index');
        } catch (Exception $e) {
            Log::debug(print_r($e, true));
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = StateService::getStateData($id);
        $country_list = StateService::getCountryList();
            return view('vnnogile::view_state', compact('data','country_list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $data = StateService::getStateData($id);
        $country_list = StateService::getCountryList();
        // print_r($data);
        // exit();
            return view('vnnogile::edit_state', compact('data','country_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_obj = $request->all();
        try {
        $request_obj['authenticatedUser'] = \Auth::user();

        StateService::updateState($request_obj, $id);
        return redirect()->action('\vnnogile\Geography\Controllers\StateController@index');

     	} catch (Exception $e) {
            Log::debug(print_r($e, true));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $getCurrentRoute = str_replace(Config::get('app.url') . ':8000', '', url()->current());
            StateService::deleteState($id);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . $getCurrentRoute;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug(print_r($e, true));
        }
    }


}
