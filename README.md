# Laravel

Base framework for starting any new project

Step1) Import Database => database/laravelDB_final.backup
Step2) Add .env (Database Configurations)
Step3) Open command prompt => Enter composer install
Step4) Start Laravel Development server => php artisan serve
Step5) URL - localhost:8000/login
Step6) Username - sidhant.mohapatra@vnnogile.in
       Password - pass@123
       
Start working on the base framework.       