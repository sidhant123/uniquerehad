
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
	<hr>
	<table cellpadding="3" id="" align="center"  width="100%">
	<tr>
        <td colspan="12" align="left"><img alt="Brand" src="{{ $company_logo }}" width="135" height="57" style="padding: 0px;"></td>
        {{-- <td colspan="6" align="center"><b>CGX</b></td> --}}
    </tr>
</table>
	<hr>
<table cellpadding="3" id="" align="center"  width="100%">
    <tr>
        <td colspan="3" align="left">Learning Tree :</td>
        <td colspan="3" align="left">{{ $lt->name }}</td>
        <td colspan="3" align="left">Learning Tree Code :</td>
        <td colspan="3" align="left">{{ $lt->learning_tree_code }}</td>
    </tr>
    <br>
    <tr>
    	<td colspan="12" align="left">Topics</td>
    </tr>
	@foreach($topics as $key => $sequence)
	<tr>
		<td colspan="12">{{ spaces(substr_count($sequence->child_nodes,'.')) }} {{ $sequence->child_nodes }} {{ $sequence->name }}<br>{{ spaces(strlen($sequence->child_nodes)) }}&nbsp;&nbsp;&nbsp;{{ $sequence->descp }}</td>
	</tr>
	{{-- <tr><td colspan="12" align="center">Page <span class="pagenum"></span></td></tr> --}}
	@endforeach
</table>
<div class="footer">
    Page <span class="pagenum"></span>
</div>
</body>
<style type="text/css">
.footer {
    width: 100%;
    text-align: center;
    position: fixed;
}
.footer {
    bottom: 0px;
}
.pagenum:before {
    content: counter(page);
}
         p { page-break-after: always; }
    p:last-child { page-break-after: never; }
</style>
</html>
