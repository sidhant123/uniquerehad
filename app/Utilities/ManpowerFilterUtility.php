<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class ManpowerFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('manpower_contracts')
                ->leftJoin('organizations as org', 'org.id', '=', 'manpower_contracts.organization_id')
                ->leftJoin('projects as project', 'project.id', '=', 'manpower_contracts.project_id')
                ->select(
                    'manpower_contracts.status as status',
                    'manpower_contracts.service_name as service_name',
                    'manpower_contracts.rate as rate', 
                    'org.name as company_name', 
                    'project.name as project_name', 
                    'manpower_contracts.status as active', 
                    'manpower_contracts.id as id')->where('manpower_contracts.deleted_at', null);
            if ($request->has('search')) {
                $data->whereRaw("( lower(manpower_contracts.service_name) like '%" . strtolower($request['search']) . "%' OR
                    lower(org.name) like '%" . strtolower($request['search']) . "%' OR 
                    lower(project.name) like '%" . strtolower($request['search']) . "%' OR
                    lower(manpower_contracts.status) like '%" . strtolower($request['search']) . "%'


            )");
            }
            if ($request->has('sort')) {
              /*  switch ($request['sort_column_name']) {
                    case 'name':
                        $data->orderBy('manpower_contracts.name', $request['sort']);
                        break;
                    case 'code':
                        $data->orderBy('manpower_contracts.code', $request['sort']);
                        break;
                    case 'unit':
                        $data->orderBy('manpower_contracts.unit', $request['sort']);
                        break;
                    case 'supplier_name':
                        $data->orderBy('org.name', $request['sort']);
                        break;
              
                   
                }*/
            } else {
                $data->orderBy('manpower_contracts.id');
            }
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            // dd($data);
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}
