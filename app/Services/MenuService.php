<?php

namespace App\Services;

//Custom imports

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

use App\Models\Menu;
use App\Models\Permission;
/**
 * Class AddressMasterService
 * 
 */
class MenuService{

	public static function buildMenu($data){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$menuItems = array();
		$menuItemsToBePassed = array();
		if (!isset($data['authenticatedUser'])){
			return $menuItemsToBePassed;
		}
		$user = $data['authenticatedUser'];

		if (session()->has('menu')){

		}else{
			$menuItems = Menu::orderBy('id','ASC')->orderBy('parent_menu_id','ASC')->orderBy('sequence','ASC')->get();
			foreach ($menuItems as $menuItem) {
				if ($user->hasAccess([$menuItem->name])){
					$menuItemArray = explode(" ", $menuItem->name, 2);
					$val = end($menuItemArray);
					$val = str_replace(' ', '', $val);
					$entity = $val;
					$action = reset($menuItemArray);
					Log::debug("*******buildMenu************");
					Log::debug($menuItemArray);
					Log::debug($entity);
					Log::debug($action);
					if ($action == 'List'){
						//$menuItem->url = '/' . strtolower($entity);
						$menuItem->url = '/' . lcfirst($entity);
					}else{
						//$menuItem->url = '/' . strtolower($entity) . '/' . strtolower($action);
						$menuItem->url = '/' . lcfirst($entity) . '/' . strtolower($action);
					}
					array_push($menuItemsToBePassed, $menuItem);
				}
			}
		}
		return $menuItemsToBePassed;
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

}
