<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Utilities\Services\ConstantsUtility;

class AddressController  extends BaseController
{

  public function __construct(){
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
    parent::__construct();
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
  }

  public function countries(Request $request){
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
    return Country::select('id','name')->get();
  }

  public function states($country_id){
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
    return Country::where('id', $country_id)->orderBy('name', 'asc')->first()->states;
  }

  public function cities($state_id){
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
    return State::where('id', $state_id)->orderBy('name', 'asc')->first()->cities;
  }

  public function pincodes($city_id){
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
    Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
    return City::where('id',$city_id)->orderBy('name', 'asc')->first()->pincodes;
  }

}
