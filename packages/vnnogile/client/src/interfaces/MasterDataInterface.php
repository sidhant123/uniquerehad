<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\Client\Interfaces;

//Custom Imports
use Illuminate\Container\Container as App;
/**
 * Service ServiceInterface
 * 
 */
interface MasterDataInterface
{
	public function all($model, $columns = array('*'));
	public function find($model, $id, $columns = array('*'));
	public function findBy($model, $whereConditions, $columns = array('*'));
	public function flushCache($cacheTag);
	public function list($model, $columns = array('*'));
	public function filteredList($model, $whereConditions, $id, $columns = array('*'));
}
