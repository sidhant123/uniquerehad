<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class MaterialFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('materials')
                ->leftJoin('organizations as org', 'org.id', '=', 'materials.supplier_id')
                ->select(
                    'materials.name as name',
                    'materials.code as code',
                    'materials.unit as unit', 
                    'org.name as supplier_name', 
                    'materials.id as id')->where('materials.deleted_at', null);
            // if ($request->has('search')) {
            //     $data->whereRaw("(lower(materials.name) like '%" . strtolower($request['search']). "%')");
            // }

                        if ($request->has('search')) {
                $data->whereRaw("( lower(materials.name) like '%" . strtolower($request['search']) . "%' OR
                    lower(materials.code) like '%" . strtolower($request['search']) . "%' OR 
                    lower(materials.unit) like '%" . strtolower($request['search']) . "%' OR
                    lower(org.name) like '%" . strtolower($request['search']) . "%'


            )");
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'name':
                        $data->orderBy('materials.name', $request['sort']);
                        break;
                    case 'code':
                        $data->orderBy('materials.code', $request['sort']);
                        break;
                    case 'unit':
                        $data->orderBy('materials.unit', $request['sort']);
                        break;
                    case 'supplier_name':
                        $data->orderBy('org.name', $request['sort']);
                        break;
                }
            } else {
                $data->orderBy('materials.id');
            }
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            // dd($data);
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}
