<?php

namespace vnnogile\Cleanups\Providers;

use Illuminate\Support\ServiceProvider;

class CleanupServiceProvider extends ServiceProvider
{

    protected $commands = [
        'vnnogile\Cleanups\Commands\CleanupNotificationsCommand',
        'vnnogile\Cleanups\Commands\CleanupCommunicationTrailsCommand',
        'vnnogile\Cleanups\Commands\CleanupImportJobsCommand',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../services/CleanupService.php';
        $this->commands($this->commands);
    }
}
