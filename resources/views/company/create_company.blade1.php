@extends('company.company')
@section('title','Create Company')
@section('editMethod')
{{method_field('post')}}
@endsection


@push('scripts')
<style>
#pmCheck{
	display: none;
}	
#ccCheck{
	display: none;
}
</style>

<script>
$('#sameUser').change(function() {

    // this will contain a reference to the checkbox   
    if (this.checked) {
     setUserdataOnChange();
     
    } 
    
});
function setUserdataOnChange(){
   var r=confirm("Program manager will be Company Coordinator");
        if (r == true) {



           $("#ccSelector :input").attr("readonly", true);

           // $('#ccDetach').attr("checked",false);
           // $('#ccDetach').attr("readonly",true);
           // $('#sameUser').attr("disabled",true);

           var z = $('#pm_user_first_name_id').select2('data');
        
           if ($('#cc_first_name_id').find("option[value='" + z[0].id + "']").length) {
               $('#cc_first_name_id').val(z[0].id).trigger('change');
               } else { 
               // Create a DOM Option and pre-select by default
               var newOption = new Option(z[0].text, z[0].id, true, true);
               $('#cc_first_name_id').append(newOption).trigger('change');
             }

           
           

           //changing mandatory
           $('#file3').removeClass("mandatory");
           //setting  value           
           setValue('cc_lastName','pm_lastName');
           setValue('cc_employeeId','pm_employeeId');
           setValue('cc_department','pm_department');
           setValue('cc_email','pm_email');
           setValue('cc_designation','pm_designation');
           setValue('cc_primaryContact','pm_primaryContact');
           setValue('cc_secondaryContact','pm_secondaryContact');
           setValue('cc_linkedinProfile','pm_linkedinProfile');
           setValue('cc_fbProfile','pm_fbProfile');
           setValue('cc_twitterHandle','pm_twitterHandle');
           
           console.log($('#pm_last_name_id').val());


         } 
      else {
           $('#sameUser').attr("checked",false);
      }
}

function setValue(a,b){
              // var c=$("#"+b).val();
              var c=$("input[name="+b+"]").val();
              // $("#"+a).val(c);

              $("input[name="+a+"]").val(c);
              }
</script>

@endpush
