<?php

namespace vnnogile\Cleanups\Commands;

use Illuminate\Console\Command;

//Custom Imports
use Illuminate\Support\Facades\Log;
use vnnogile\Cleanups\Services\CleanupService;

class CleanupNotificationsCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:notifications  {tenantPartyId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup notification records';

    /**
     * The assessment service.
     *
     * @var CleanupService $cleanupService
     */
    protected $cleanupService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CleanupService $cleanupService)
    {
        parent::__construct();
        $this->cleanupService = $cleanupService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        //Retrieve the parameters of the command
        $tenantPartyId = $this->argument('tenantPartyId');
        //Invoke the cleanupNotifications from the CleanupService
        $this->cleanupService->cleanupNotifications($tenantPartyId);

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
