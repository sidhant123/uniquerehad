INSERT INTO public.menus(
     id, display_name, name, sequence, parent_menu_id, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
  
    (334,'Manpower', 'List Manpower Report', 175, 327,  '', now(), now(),  1, 1);

INSERT INTO public.permissions(
          id,  name, created_at, updated_at,  created_by, updated_by)
    VALUES 
   
     (68,'List Manpower Report', now(), now(), 1, 1);

INSERT INTO public.permission_role(
             role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     ( 1, 68, now(), now(), 1,1),
     ( 2, 68, now(), now(), 1,1);


