<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;
use App\Models\Payment;
use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class PaymentService {
	use ValidationTrait;

	public static function retrievePayments() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Payment::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

public static function savePaymentData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		try {
			$userId = $data['authenticatedUser']->id;
			$obj = new Payment;
			$obj->project_id = $data['project_id'];
			$obj->organization_id = $data['organization_id'];
			$obj->expense_date = $data['expense_date'];
			$obj->expense_type_id = $data['expense_type_id'];
			$obj->service_name = $data['service_name'];
			$obj->bill_no = $data['bill_no'];
			$obj->item_desc = $data['item_desc'];
			$obj->unit = $data['unit'];
			$obj->rate = $data['rate'];
			$obj->qty = $data['qty'];
			$obj->amount = $data['amount'];
			$obj->gst_amount = $data['gst_amount'];
			$obj->base_amount = $data['base_amount'];
			$obj->amount_paid = $data['amount_paid'];
    		$obj->created_by = $userId;
			$obj->updated_by = $userId;

			$dd = $obj->save();

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			
			$res = true;
		} catch (Exception $e) {
		  Log::error('Exception while storing payment :: ', $e->getMessage());
		   $res = false;
		}
		return $res;
				

	}
	
public static function updatePaymentData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

			try {
				Payment::where('id', $data['id'])->update([
				'project_id' => $data['project_id'],
				'expense_date' => $data['expense_date'],
				'expense_type_id' => $data['expense_type_id'],
				'organization_id' => $data['organization_id'],
				'service_name' => $data['service_name'],
				'bill_no' => $data['bill_no'],
				'item_desc' => $data['item_desc'],
				'unit' => $data['unit'],
				'rate' => $data['rate'],
				'qty' => $data['qty'],
				'amount' => $data['amount'],
				'gst_amount' => $data['gst_amount'],
				'base_amount' => $data['base_amount'],
				'amount_paid' => $data['amount_paid'],
			
				'updated_by' => $data['authenticatedUser']->id
			]);
				Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

				$res = true;
		} catch (Exception $e) {
		  Log::error('Exception while updating payment :: ', $e->getMessage());
		   $res = false;
		}
		return $res;

	}
public static function deletePayment($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$payment = Payment::where('id', $id);
			//$user = self::validateActivePayment($payment);

			//if ($user) {
				$userId = auth()->user()->id;

				$payment = Payment::where('id', $id);
				$payment->update(['deleted_by' => $userId]);
				$check = $payment->delete();
			//} else {
			//	$check = false;
			//}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	

}
