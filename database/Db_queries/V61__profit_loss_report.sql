INSERT INTO public.menus(
     id, display_name, name, sequence, parent_menu_id, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
  
    (331,'Profit & Loss', 'List Profit Loss Report', 160, 327,  '', now(), now(),  1, 1);

INSERT INTO public.permissions(
          id,  name, created_at, updated_at,  created_by, updated_by)
    VALUES 
   
     (65,'List Profit Loss Report', now(), now(), 1, 1);

INSERT INTO public.permission_role(
             role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     ( 1, 65, now(), now(), 1,1),
     ( 2, 65, now(), now(), 1,1);

