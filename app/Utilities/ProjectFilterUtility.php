<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class ProjectFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        //id,company_name,name,site_location,s_first_name,c_first_name,start_date,end_date,id

        try {
            $data = DB::table('projects')
                ->leftJoin('organizations as org', 'org.id', '=', 'projects.organization_id')
                ->leftJoin('sites as site', 'site.id', '=', 'projects.site_id')
                ->leftJoin('users as s_user', 's_user.id', '=', 'projects.site_engineer_user_id')
                ->leftJoin('users as c_user', 'c_user.id', '=', 'projects.client_user_id')
                ->select('projects.id as id',
                    'projects.name as name',
                    'projects.start_date as start_date', 
                    'projects.end_date as end_date', 
                    's_user.first_name as s_first_name', 
                    'c_user.first_name as c_first_name', 
                    'org.name as company_name', 
                    'site.location as site_location', 
                    'projects.id as id')->where('projects.deleted_at', null);
                                
            if ($request->has('search')) {
                $data->whereRaw("
                    (lower(projects.name) like '%" . strtolower($request['search']) . "%' OR
                    lower(s_user.first_name) like '%" . strtolower($request['search']) . "%' OR
                    lower(c_user.first_name) like '%" . strtolower($request['search']) . "%'

            )");
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'company_name':
                        $data->orderBy('org.name', $request['sort']);
                        break;
                    case 'name':
                        $data->orderBy('projects.name', $request['sort']);
                        break;
                    case 'start_date':
                        $data->orderBy('projects.start_date', $request['sort']);
                        break;
                    case 'end_date':
                        $data->orderBy('projects.projects.end_date', $request['sort']);
                        break;
                    case 'c_first_name':
                        $data->orderBy('c_user.first_name', $request['sort']);
                        break;
                    case 's_first_name':
                        $data->orderBy('s_user.first_name', $request['sort']);
                        break;
                   
                }
            } else {
                $data->orderBy('projects.id');
            }
            //dd($filterColumns);
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
           // dd($data->get());
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}
