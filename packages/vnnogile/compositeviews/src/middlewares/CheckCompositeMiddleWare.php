<?php

namespace vnnogile\CompositeViews\Middlwares;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Custom Imports
use App\Services\CompositionMasterService;
use App\Services\CompositionDataService;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;


class CheckCompositeMiddleWare
{
    private $masterDataInterface;
    private $utilsInterface;

    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
          try {
        $routeName = Route::currentRouteName();
        $parameter = array_values($request->route()->parameters());
        $compositeViewData = $this->checkForCompositeView($routeName);
        $isTaggable = $this->utilsInterface->isTaggable($this->masterDataInterface,'Document');
        if(count($compositeViewData) > 0) {
            if($isTaggable){
            $routeType = explode('.', $routeName);
            $operation = array_pop($routeType);    
            $request['isTaggable'] = $isTaggable;        
            $request['operation'] = $operation;
            if(!empty($parameter)){
            $request['id'] = $parameter[0];    
            }            
            }
        $request['route_name'] = $routeName;
        $request = \Request::create(route("compositeView"), 'GET', $request->all());
        $user = \Auth::user();
        Log::debug('*******Authenticated User in CheckCompositeMiddleWare************');
        Log::debug($user);
        $request->merge(['user' => $user ]);
        //add this
        $request->setUserResolver(function () use ($user) {
            return $user;
        });
        Log::debug("***************IN composition********************");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return \Route::dispatch($request);
        }
        Log::debug("***************NO composition********************");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $next($request);
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e);
        }
    
    }
    private function checkForCompositeView($routeName){
       return CompositionMasterService::getCompositionMasterDataFromRoute($routeName);
    }
}
