<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 05 Apr 2019 20:43:32 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LearningTheme
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property int $customer_party_id
 * @property int $master_program_id
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\MasterProgram $master_program
 *
 * @package App\Models
 */
class LearningTheme extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'customer_party_id' => 'int',
		'master_program_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'start_date',
		'end_date',
		'customer_party_id',
		'master_program_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function master_program()
	{
		return $this->belongsTo(\App\Models\MasterProgram::class, 'id');
	}
}
