<?php
namespace vnnogile\Geography\services;
/**
 * Created by Reliese Model.
 * Date: Sun, 28 March 2018 13:31:34 +0530.
 */



//Custom imports
use App\Models\Country;
use App\Models\State;
use DB;
use Illuminate\Support\Facades\Log;

/*use vnnogile\Utilities\Services\FileUploadUtility;
use vnnogile\Utilities\Services\StorageUtility;*/
/**
 * Class CompositionDataService
 *
 */
class StateService
{
	public static function saveState($data)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        // First we will move file to temporary folder
        // Save the document information into the database
        // move the file from temporary file path to its respective location
        // finally update the file path of the document
        try {
            $check = DB::transaction(function () use ($data) {

                // print_r($data['authenticatedUser']->id);
                // exit();

                    $state_name = $data['state_name'];
                    $country_id = $data['country_id'];

                $state = new State;
                $state->name = $state_name;
                $state->country_id = $country_id;
                $state->created_by = $data['authenticatedUser']->id;
                $state->updated_by = $data['authenticatedUser']->id;
                $state->save();

                Log::debug("***********AFTER SAVEING STATE**************");
                $state_id = $state->id;
                return $state_id;
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            });
            return $check;
	    } catch (Exception $e) {
	            Log::debug("StateService::saveRecord ended" . print_r($e, true));
	            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	            return $e;
        }
    }


    public static function updateState($data, $id)
    {
        try {
            $check = DB::transaction(function () use ($data, $id) {
                $state_id = $id;
                $state = self::getStateData($state_id);
                        $state->name = $data['state_name'];
                        $state->country_id =$data['country_id'];
                        $state->updated_by = 1; //$data['authenticatedUser']->id;
                        $state->save();
                $state_id = $state->id;
                return $state_id;
            });
            return $check;
        } catch (Exception $e) {
            Log::debug("StateService::updateRecord ended" . print_r($e, true));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return $e;
        }
    }

        public static function getStateData($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        $state = State::find($id);
        return $state;
    }

        public static function deleteState($id){
        State::where('id', $id)->update(['deleted_by' =>auth()->user()->id]);
        State::where('id', $id)->delete();
    }


      public static function getCountryList(){
  		return Country::all();
  	}

	  public static function getStateList(){
  		return State::all();
  	}


 }