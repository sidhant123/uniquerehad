 var programs = [];
 var selectAll = {
    id: 'all',
    text: 'All'
};
 $(document).ready(function() {
   $('#customer_id,#program_id,#from_id').select2({});
//    $("#checkbox").click(function(){
//     if($("#checkbox").is(':checked') ){
//         selectAll(true,'');
//     }else{
//         selectAll($flag,selector);
//      }
// });

   $('#summernote').summernote({
     toolbar: [
       ['style', ['style']],
       ['font', ['bold', 'underline', 'clear']],
       ['fontname', ['fontname']],
       ['color', ['color']],
       ['para', ['ul', 'ol', 'paragraph']],
       ['table', ['table']],
       ['insert', ['link', 'picture'
         , 'video'
       ]],
       ['view', ['fullscreen', 'codeview', 'help']]
     ],
   });
   $("#start_date_id").datepicker({
     dateFormat: "dd-mm-yy",
     minDate: new Date(),
   });
   $("#end_date_id").datepicker({
     dateFormat: "dd-mm-yy",
     minDate: new Date(),
   });
 });
 $(document).on('change', '#customer_id', function() {
  if($("#customer_checkbox").is(':checked')){
    var customer_ids = $('#customer_hidden_id').val();//$(this).val();
    if (customer_ids) {
     getDataThroughAxios('/getProgramByCustomers/', JSON.parse(customer_ids).toString(), 'program_id')
   } else {
     $('#program_id').empty();
     $('#program_id').select2({});
   }
  }else{
    var customer_ids = $(this).val();
    if (customer_ids) {
     getDataThroughAxios('/getProgramByCustomers/', customer_ids.toString(), 'program_id')
   } else {
     $('#program_id').empty();
     $('#program_id').select2({});
   }
  }
   console.log(customer_ids);
   
 });
 function selectAllCustomer(flag,selector){
    var $S1 = $("select[id="+selector+"]");
    
  if(flag){
        var all_customers = [];
        $("#"+selector+" option").each(function()
{
        all_customers.push($(this).val());

});
         var newOption = new Option(selectAll.text, selectAll.id, false, false);
          $('#'+selector).append(newOption);
          $("#"+selector).val('all').select2();
        $('#customer_hidden_id').val(JSON.stringify(all_customers));
         $S1.attr("readonly", "readonly");
    }else{
        $S1.removeAttr('readonly');
        $('#customer_hidden_id').val('');
        $('#'+selector).find("option[value='all']").remove();
        $('#'+selector).val(null).trigger('change');
     }
      // $("#"+selector+"> option").trigger("change");
     $("#"+selector).trigger("change");
 }
 function selectAllProgram(flag,selector){
    var $S1 = $("select[id="+selector+"]");
  if(flag){
     $("#"+selector).val('all').select2();
     $S1.attr("readonly", "readonly");
     var newOption = new Option(selectAll.text, selectAll.id, false, false);
          $('#'+selector).append(newOption);
          $("#"+selector).val('all').select2();
  }else{
    $S1.removeAttr('readonly');
     $('#'+selector).find("option[value='all']").remove();
        $('#'+selector).val(null).trigger('change');
  }
 }
 //$('#customer_id').val(null).trigger('change');

 function populateData(data, control_id) {
   if (Object.keys(data).length > 0) {
    $('#program_id').empty();
     $('#program_id').select2({});
     console.log(programs);
     $.each(data, function(key, value) {
       // console.log(programs.indexOf(value.id));
       // if (programs.indexOf(value.id) == -1) {
         $("#" + control_id).append("<option value='" + value.id + "'>" + value.code + " " + value.name + "</option>");
         // programs.push(value.id);
       // }
     });
     if($('#program_checkbox:checkbox:checked').length > 0){
      $('#program_checkbox').trigger('change');
     }
   }
 }
 function getDataThroughAxios(url, value, control_id) {
   console.log(value);
   axios.get(url + value)
     .then(function(response) {
       console.log(response);
       populateData(response['data'], control_id);
     })
     .catch(function(error) {
       console.log("error" + error);
     });
 }
 $(document).on('click', '#saveBtn', function() {
   var html_data = $('#summernote').summernote('code');
   var message_board_id = $('#message_board_id').val();
   var img_src = getImgSrc();
   var img_name = getImgName();
   var objData = {};
   objData['html_data'] = html_data;
   objData['img_src'] = img_src;
   objData['img_name'] = img_name;
   var stringifyData = JSON.stringify(objData);
   var formData = $('#messageBoardForm').serialize();
   var data = "data=" + encodeURIComponent(stringifyData) + '&' + formData
   console.log(data);
   if (validateMandatoryFields() == true) {
     saveMessageBoard(data, message_board_id);
   }

 });

 function getImgName() {
   var img_name = getAttributeValue('data-filename');
   return img_name;
 }

 function getImgSrc() {
   var img_src = getAttributeValue('src');
   return img_src;
 }

 function getAttributeValue(attr_name) {
   var img_attr = document.getElementsByTagName('img');
   var attr_array = [];
   var count = img_attr.length;
   if (count > 0) {
     for (var i = 0; i < count; i++) {
       attr_array.push(img_attr[i].getAttribute(attr_name));
     }
   }
   return attr_array;
 }

 function saveMessageBoard(data, message_board_id) {
  $('.alert-danger').addClass('hide');
  $('.alert-danger').children().find('li').remove();
   var url = '/messageBoards';
   var type = 'post';
   console.log(message_board_id);
   if (message_board_id > 0) {
     url = '/messageBoards/' + message_board_id;
     type = 'put';
   }
   $.ajax({
     type: type,
     url: url,
     data: data,
     success: success,
     error: errorStatus,
   });
 }

 function success(data) {
   if (data == 'save successfully') {
     window.location = '/messageBoards';
   }

 }
function errorStatus(data){
  console.log(data);
      $('.alert-danger').removeClass('hide');
  var scrollPos =  $(".alert").offset().top;
  console.log(data.responseJSON.errors);
  $.each(data.responseJSON.errors, function(index, val) {
     /* iterate through array or object */
     $('.alert-danger').children().append('<li>'+val+'</li>');

  });
    $(window).scrollTop(scrollPos);
}
 function validateMandatoryFields() {
   var valid = true;
   var summernote = $('.note-editable').val();
   $(".mandatory").each(function() {
       if ($(this).val() === '' || $(this).val() === '0') {
         $(".mandatory").removeClass('fillFields');
         $(this).addClass('fillFields');
         valid = false;
         return valid;
       }
   });
   return valid;
 }
 $('#from_id').on('select2:select', function (e) {
  $('.errorMessage').html("");
     var data = e.params.data;
      var program = $("#program_id").select2("val");
      console.log();
     if(data.id > 0){
            if(data.text == 'Program Director'){
              if(typeof program == null){
              $("#from_id").select2("val","0");
              $('.errorMessage').html("Please select a program.");
              return false;
              }
            }
            if(data.text == 'Program Coordinator'){
              if(typeof program == null){
              $("#from_id").select2("val","0");
              $('.errorMessage').html("Please select a program.");
              return false;
            }
            }
            if(data.text == 'Program Manager'){
              if(typeof program == null){
              $("#from_id").select2("val","0");
              $('.errorMessage').html("Please select a program.");
              return false;
            }
            }
        }
});