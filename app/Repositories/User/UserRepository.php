<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 1/29/2018
 * Time: 9:54 AM
 */

namespace App\Repositories\User;

use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserRepository implements UserRepositoryInterface {

	protected $userProfileCompletionFields = array(
		'first_name',
		'last_name',
		'email',
		'personal_email',
		'gender',
		'designation',
		'linkedin_profile',
		'facebook_id',
		'twitter_handle',
		'google_profile',
		'photo',
		'work_phone_number',
		'home_phone_number',
	);

	/**
	 * @param $username
	 * @param $password
	 * @param bool $isRemember
	 * @return bool
	 *
	 * Authenticate User
	 */


	/**
	 * @param $model
	 * @param array $userArray
	 * @param int $userPartyId
	 * @return mixed
	 *
	 * Update User Profile and Personal Detail
	 *  tables : people, user
	 */
	public function updateUserDetail($model, $userArray = array(), $userPartyId) {
		return $model::where('party_id', $userPartyId)
			->update($userArray);
	}
	// store user device token
	public function storeUserDeviceToken($model, $userId, $token = '') {
		return $model::where('id', $userId)
			->update(array('notification_token' => $token));
	}

	/**
	 * @param int $userId
	 * @return mixed
	 *
	 * Get User Profile Detail
	 */
	public function getUserProfileDetail($userId) {
		try {
			$userProfile = App\User::select('people.id', 'people.first_name', 'people.last_name', 'people.email', 'people.personal_email',
				'people.gender', 'people.employee_id', 'people.department', 'people.designation', 'people.linkedin_profile', 'people.facebook_id',
				'people.twitter_handle', 'people.google_profile', 'people.party_id', 'people.tenant_party_id', 'people.customer_party_id',
				DB::raw('CONCAT("' . asset('/') . '", "storage", people.photo) AS photo'),
				'work_phones.phone_number as work_phone_number',
				'home_phones.phone_number as home_phone_number',
				'is_private', 'users.id as user_id'
			)
				->join('parties', function ($join) {
					$join->on('users.party_id', '=', 'parties.id');
				})
				->join('people', function ($join) {
					$join->on('people.party_id', '=', 'parties.id');
				})
				->leftjoin('phones as work_phones', function ($join) {
					$join->on('users.party_id', '=', 'work_phones.party_id');
					$join->where('work_phones.phone_type_id', '=', 1);
				})
				->leftjoin('phones as home_phones', function ($join) {
					$join->on('users.party_id', '=', 'home_phones.party_id');
					$join->where('home_phones.phone_type_id', '=', 2);
				})
				->where('users.id', $userId)
				->first();

			if ($userProfile) {
				$userProfile = $userProfile->toArray();
				//Added by vnnogile - to not show the phone number in case the profile is private
				if ($userProfile['is_private'] == 1) {
					$userProfile['work_phone_number'] = '**********';
				}
				//Added by vnnogile - to not show the phone number in case the profile is private
				//Calculating profile completion percentage
				$completedFields = 0;
				foreach ($userProfile as $key => $item) {
					if (in_array($key, $this->userProfileCompletionFields)) {
						if (!is_null($item) && trim($item) != '') {
							$completedFields++;
						}
					}
				}
				$userProfile['profile_completion_percentage'] = round(($completedFields / count($this->userProfileCompletionFields)) * 100, 2);
				return $userProfile;
			} else {
				return null;
			}

		} catch (\Exception $exception) {
			Log::error(__CLASS__ . "::" . __METHOD__ . ' ' . $exception->getMessage());
			return null;
		}
	}

	/**
	 * @param int $userId
	 * @return mixed
	 *
	 * Get User Tenant Detail
	 */
	public function getUserTenantDetail($userId) {
		return App\User::where('id', $userId)->first()->party->person->tenant_party;
	}

	/**
	 * @param int $userId
	 * @return mixed
	 *
	 * Get User Tenant Organnization
	 */
	public function getUserTenantOrg($userId) {
		return App\User::where('id', $userId)->first()->party->person->tenant_organization;
	}
	/**
	 * @param int $userId
	 * @param $customerPartyId
	 * @return mixed
	 *
	 * Get User Organization Detail
	 */
	public function getUserOrganizationDetail($userId, $customerPartyId) {
		if ($customerPartyId) {
			return App\User::where('id', $userId)->first()->party->person->customer_party->organization;
		} else {
			return App\User::where('id', $userId)->first()->party->person->tenant_party->organization;
		}
	}

	/**
	 * @param $userId
	 * @param string $roleName
	 * @param int $roleUserId
	 * @return mixed
	 *
	 * Get list of roles that user belongs to
	 */
	public function getUserRoles($userId, $roleName = '', $roleUserId = 0) {
		return App\User::find($userId)->roles()
		// ->when(strlen($roleName), function ($query) use ($roleName) {
		//     return $query->where('name', $roleName);
		// })
		// ->when($roleUserId, function ($query) use ($userId, $roleUserId) {
		//     return $query->where('role_user.id', $roleUserId);
		// })
			->get();
	}

	/**
	 * @param $userId
	 * @param $partyId
	 * @param $phoneNumber
	 * @param $phoneType
	 */
	public function updateOrCreateUserPhone($userId, $partyId, $phoneNumber, $phoneType) {
		$phoneDetail = App\Models\Phone::where('party_id', $partyId)->where('phone_type_id', $phoneType)->first();
		if ($phoneDetail) {
			if ($phoneNumber) {
				App\Models\Phone::where('party_id', $partyId)
					->where('phone_type_id', $phoneType)
					->update(['phone_number' => $phoneNumber, 'updated_by' => $userId]);
			} else {
				$phoneDetail->delete();
			}

		} else {
			if ($phoneNumber) {
				App\Models\Phone::create([
					'party_id' => $partyId,
					'phone_type_id' => $phoneType,
					'phone_number' => $phoneNumber,
					'created_by' => $userId,
					'updated_by' => $userId,
				]);
			}
		}
	}

	/**
	 * @param $programId
	 * @param $programUserId
	 * @return mixed
	 *
	 * Get User detail using program and program user combination table
	 */
	public function getUserDetailForProgram($programId, $programUserId) {
		return App\User::select(
			'people.id', 'people.first_name', 'people.last_name', 'people.email', 'people.personal_email',
			'people.gender', 'people.employee_id', 'people.department', 'people.designation', 'people.linkedin_profile',
			'people.facebook_id', 'people.twitter_handle', 'people.google_profile', 'people.party_id', 'people.tenant_party_id',
			'people.customer_party_id', DB::raw('CONCAT("' . asset('/') . '", "storage", people.photo) AS photo'),
			'work_phones.phone_number as work_phone_number', 'home_phones.phone_number as home_phone_number',
			'is_private', 'users.id as user_id')
			->join('people', function ($join) {
				$join->on('people.party_id', '=', 'users.party_id');
			})
			->leftjoin('phones as work_phones', function ($join) {
				$join->on('users.party_id', '=', 'work_phones.party_id');
				$join->where('work_phones.phone_type_id', '=', 1);
			})
			->leftjoin('phones as home_phones', function ($join) {
				$join->on('users.party_id', '=', 'home_phones.party_id');
				$join->where('home_phones.phone_type_id', '=', 2);
			})
			->join('program_user', function ($join) use ($programId, $programUserId) {
				$join->on('users.id', 'program_user.user_id');
				$join->where('program_user.id', $programUserId);
				$join->where('program_user.program_id', $programId);
			})
			->first();
	}

	public function getUserMainRole($userId) {
		$userRoles = $this->getUserRoles($userId);
		$userMainRole = '';
		if (count($userRoles)) {
			$roles = $userRoles->pluck('name')->toArray();
			$priorityRoles = config('constants.priority_roles');
			foreach ($priorityRoles as $key => $role) {
				if (in_array($role, $roles)) {
					$userMainRole = $role;
					break;
				}
			}
		}
		return $userMainRole;
	}

}