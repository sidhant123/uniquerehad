<?php

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

use Illuminate\Notifications\Notifiable;
use Sofa\Eloquence\Eloquence;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;



class User extends Authenticatable
{
        use HasApiTokens, Notifiable, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $searchableColumns = ['first_name','last_name', 'email','userid'];

    protected $fillable = [
        'first_name', 'last_name', 'email', 'personal_email', 'userid', 'password', 'remember_token', 'is_active', 'party_id', 'created_by', 'updated_by', 'deleted_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class)->withPivot('id');
    }

    /**
     * Checks if User has access to $permissions.
     */
    public function hasAccess(array $permissions) : bool
    {
        // check if the permission is available in any role
        foreach ($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }

    public function attributes()
    {
        return $this->hasMany(\App\Models\UserAttribute::class);
    }

    public function badges()
    {
        return $this->belongsToMany(\App\Models\Program::class, 'program_user_badge')->withPivot('id', 'user_id', 'max_distribution_badges', 'current_distribution_badges', 'content_rated_count', 'quiz_completion_badge_count' ,'fastest_activity_badge_count', 'content_rating_badge_count', 'facilitator_badge_count', 'participant_badge_count');
    }

    public function quiz_results()
    {
        return $this->belongsToMany(\App\Models\Program::class, 'user_exercise_final_results')->withPivot('id', 'program_id', 'user_id', 'exercise_bank_id', 'exercise_name', 'total_questions', 'total_correct_answers', 'total_incorrect_answers');
    }

    public function phones(){
        return $this->hasMany(\App\Models\Phone::class,'party_id', 'party_id');
    }

    public function customerExercises(){
        return $this->hasMany(\App\Models\CustomerExerciseParticipant::class,'party_id');
    }

    public function accessTokens()
    {
        return $this->hasMany(\App\Models\OauthAccessToken::class);
    }
}
