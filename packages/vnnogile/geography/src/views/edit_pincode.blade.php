@extends('vnnogile::pincode_base')
@section('title', 'Edit Pincode')
@section('editMethod')
{{method_field('PUT')}}
@endsection
@section('editID', '/'.$data->id)
@section('pincode_name', $data->name)
@section('is_mandatory', 'mandatory')
@section('is_update', 'Y')
