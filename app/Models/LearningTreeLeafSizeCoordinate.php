<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 06 May 2018 13:14:52 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
//Custom Imports
use App\Models\BaseModel;


/**
 * Class LearningTreeLeafSizeCoordinate
 * 
 * @property int $id
 * @property int $node_count
 * @property int $level
 * @property int $pencil_offset_y
 * @property int $pencil_circle_offset_x
 * @property int $pencil_circle_offset_y
 * @property int $text_offset_y
 * @property int $circle_radius
 * @property int $circle_stroke_width
 * @property int $pencil_width
 * @property int $pencil_height
 * @property int $zoom_controls_offset
 * @property int $program_screen_offset
 * @property int $title_font_size
 * @property string $title_font_family
 * @property int $main_font_size
 * @property string $main_font_family
 * @property int $launch_font_size
 * @property string $launch_font_family
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class LearningTreeLeafSizeCoordinate extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'node_count' => 'int',
		'level' => 'int',
		'pencil_offset_y' => 'int',
		'pencil_circle_offset_x' => 'int',
		'pencil_circle_offset_y' => 'int',
		'text_offset_y' => 'int',
		'circle_radius' => 'int',
		'circle_stroke_width' => 'int',
		'pencil_width' => 'int',
		'pencil_height' => 'int',
		'zoom_controls_offset' => 'int',
		'program_screen_offset' => 'int',
		'title_font_size' => 'int',
		'main_font_size' => 'int',
		'launch_font_size' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'node_count',
		'level',
		'pencil_offset_y',
		'pencil_circle_offset_x',
		'pencil_circle_offset_y',
		'text_offset_y',
		'circle_radius',
		'circle_stroke_width',
		'pencil_width',
		'pencil_height',
		'zoom_controls_offset',
		'program_screen_offset',
		'title_font_size',
		'title_font_family',
		'main_font_size',
		'main_font_family',
		'launch_font_size',
		'launch_font_family',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}
}
