<?php

namespace vnnogile\Geography\Controllers;

use Illuminate\Http\Request;

//Custom
use Config;
use vnnogile\Client\Controllers\BaseController;
use Illuminate\Support\Facades\Log;
use vnnogile\Geography\services\CityService;
use vnnogile\Pagination\Utilities\PaginateController;
use vnnogile\Geography\services\StateService;

class CityController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    public function index(Request $request)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

        return PaginateController::paginate('City', $request->path(), parent::$utilsInterface, parent::$masterDataInterface);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $state_list = StateService::getStateList();  
        $country_list = StateService::getCountryList();
        return view('vnnogile::create_city', compact('state_list','country_list'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_obj = $request->all();
        // print_r($request_obj);
        // exit();
        try {
            $request_obj['authenticatedUser'] = \Auth::user();
            CityService::saveCity($request_obj);
            return redirect()->action('\vnnogile\Geography\Controllers\CityController@index');
        } catch (Exception $e) {
            Log::debug(print_r($e, true));
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = CityService::getCityData($id);
        $state_list = StateService::getStateList(); 
        $country_list = StateService::getCountryList();
            return view('vnnogile::view_city', compact('state_list','data','country_list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $data = CityService::getCityData($id);
        $state_list = StateService::getStateList(); 
        $country_list = StateService::getCountryList();
        // print_r($data);
        // exit();
            return view('vnnogile::edit_city', compact('state_list','data','country_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_obj = $request->all();
        try {
        $request_obj['authenticatedUser'] = \Auth::user();

        CityService::updateCity($request_obj, $id);
        return redirect()->action('\vnnogile\Geography\Controllers\CityController@index');

        } catch (Exception $e) {
            Log::debug(print_r($e, true));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $getCurrentRoute = str_replace(Config::get('app.url') . ':8000', '', url()->current());
            CityService::deleteCity($id);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . $getCurrentRoute;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug(print_r($e, true));
        }
    }

}