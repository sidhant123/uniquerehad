<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Jan 2018 18:30:11 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

class ProjectUser extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
	
	];

	protected $fillable = [
		
	];
 	public function ProjectUser()
    {
        return $this->hasMany(\App\Models\ProjectUser::class);
    }
	
}
