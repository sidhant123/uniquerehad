<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\Client\Utils;

//Custom imports
use App\User;
use App\Models\Validation;

use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

use vnnogile\Client\Db\EntityConfigService;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

/**
 * Class UtilsService
 * 
 */
class UtilsService implements UtilsInterface
{

	public function getRowCountList(MasterDataInterface $masterDataInterface){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$configuration = $masterDataInterface->all('Configuration', ['config_key', 'config_value']);
		$cachedConfig = array();
		Log::debug('******* $configuration *******');
		Log::debug($configuration);
		foreach ($configuration as $config) {
 			$cachedConfig[$config->configKey] = $config->configValue;
		}		
		Log::debug('******* $configuration *******');
		$row_count_list = $cachedConfig['row_count_list'];
		$default_row_count = $cachedConfig['default_row_count'];

		$data = array();
		$data['row_count_list'] = explode(',', $row_count_list);
		$data['default_row_count'] = $default_row_count;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

	public function isTaggable(MasterDataInterface $masterDataInterface, $model){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$entityConfiguration = $masterDataInterface->all('EntityConfiguration');
		$taggable = False;
		foreach ($entityConfiguration as $entityConfig) {
 			if (($entityConfig->entityName == $model) && ($entityConfig->taggable == 1)){
				$taggable = True;
 			}
		}		
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $taggable;
	}

	public function getTenantForAuthUser($user){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		if ($user->party != null){
			return $user->party->person->tenant_party->organization;
		}
		
	}

	public function getCustomerForAuthUser($user){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

		if ($user->party != null){
			if (is_null($user->party->person->customer_party)){
				Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
				return json_encode(['message' => 'Does not belong to customer']);
			}else{
				Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
				return $user->party->person->customer_party->organization;
			}			
		}
	}

	public function getConfigValue(MasterDataInterface $masterDataInterface, $configKey){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$configurations = $masterDataInterface->all('Configuration');
		foreach ($configurations as $configuration) {
		    if ($configuration->configKey == $configKey){
		        $configValue = $configuration->configValue;
		        break;
		    }
		}		
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $configValue;
	}

	public function getUserPrincipal(){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		if (session()->has('userPrincipal')) {
		    $userPrincipal = session('userPrincipal');
		}else{
			$userPrincipal = null;
		}
				Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $userPrincipal;
	}


	public function getTenantForUser($userId){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$user = User::find($userId);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $user->party->person->tenant_party->organization;
	}

	public function getCustomerForUser($userId){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$user = User::firstOrFail($userId);
		if (is_null($user->party->person->customer_party)){
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return json_encode(['message' => 'Does not belong to customer']);
		}else{
			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $user->party->person->customer_party->organization;
		}
	}	

	public function getValidationRulesForEntity($entityName, $operation){
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

		$cacheConfig = EntityConfigService::retrieveEntityConfiguration('Validation');
		Log::debug("*****VALIDATION RULES CACHE FOR MODEL***********");
		Log::debug($entityName);
		Log::debug($cacheConfig);

		switch ($cacheConfig['cacheDuration']) {
			case 0:
				$validations = Cache::tags($cacheConfig['cacheTagName'] . '_' . $entityName)->rememberForever($cacheConfig['cacheKey']  . '_' . $entityName, function () use($entityName, $operation){
				    return Validation::select('field_name','validation_rule')
							->where(function($query) use($operation){
							    $query->whereIn('operation',['Both', $operation]);
							})->where(function($query) use($entityName) {
							    $query->where('active', 1)->where('entity_name', $entityName);
							})->get();
				});
				break;
			
			default:
				$validations = Cache::tags($cacheConfig['cacheTagName']  . '_' . $entityName)->remember($cacheConfig['cacheKey'] . '_' . $entityName, $cacheConfig['cacheDuration'], function () use($entityName, $operation) {
					    return Validation::select('field_name','validation_rule')
								->where(function($query) use($operation){
								    $query->whereIn('operation',['Both', $operation]);
								})->where(function($query) use($entityName) {
								    $query->where('active', 1)->where('entity_name', $entityName);
								})->get();
				});
				break;
		}

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $validations;
	}
}
