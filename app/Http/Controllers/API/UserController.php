<?php

namespace App\Http\Controllers\API;

use DB;
use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAuthenticate;
use App\Repositories\Badge\BadgeRepositoryInterface;
use App\Repositories\Common\CommonRepositoryInterface;
use App\Repositories\HelpDesk\HelpDeskRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\Program\ProgramRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Hashids\Hashids;


class UserController extends Controller {
	/**
	 * @var UserRepositoryInterface
	 */
	private $userRepository;
	private $commonRepository;
	private $programRepository;
	private $badgeRepository;
	private $notificationRepository;
	private $helpDeskRepository;

	/**
	 * UserController constructor.
	 * @param UserRepositoryInterface $userRepository
	 * @param CommonRepositoryInterface $commonRepository
	 * @param ProgramRepositoryInterface $programRepository
	 * @param BadgeRepositoryInterface $badgeRepository
	 * @param NotificationRepositoryInterface $notificationRepository
	 * @param HelpDeskRepositoryInterface $helpDeskRepository
	 */
	// function __construct(UserRepositoryInterface $userRepository, CommonRepositoryInterface $commonRepository,
	// 	ProgramRepositoryInterface $programRepository, BadgeRepositoryInterface $badgeRepository,
	// 	NotificationRepositoryInterface $notificationRepository, HelpDeskRepositoryInterface $helpDeskRepository) {
	// 	$this->userRepository = $userRepository;
	// 	$this->commonRepository = $commonRepository;
	// 	$this->programRepository = $programRepository;
	// 	$this->badgeRepository = $badgeRepository;
	// 	$this->notificationRepository = $notificationRepository;
	// 	$this->helpDeskRepository = $helpDeskRepository;
	// }

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	// public function login(Request $request) {
	// 	$phoneNumber = $this->helpDeskRepository->getHelpConfig('helpdesk_phone');
	// 	if (isset($request['redirectBack']) && isset($request['exercise'])) {
	// 		$redirectBack = $request['redirectBack'];
	// 		$exercise = $request['exercise'];
	// 		return view('front.login', compact('redirectBack', 'exercise', 'phoneNumber'));
	// 	} else {
	// 		return view('front.login', compact('phoneNumber'));
	// 	}
	// }

	/**
	 * @param UserAuthenticate $request
	 * @return $this
	 */
	// public function checkAuth(UserAuthenticate $request) {
	// 	try {
	// 		$username = trim($request->username);
	// 		$password = trim($request->password);
	// 		$isRemember = false;
	// 		if (isset($request->remember) && $request->remember == '1') {
	// 			$isRemember = true;
	// 		}
	// 		$isAuth = $this->userRepository->login($username, $password, $isRemember);

	// 		if (isset($request['redirectBack']) && isset($request['exercise'])) {
	// 			$exercise = $request['exercise'];
	// 			if ($isAuth) {
	// 				return redirect('user/exercise/' . $exercise);
	// 			} else {
	// 				return redirect('login?redirectBack=user_exercise&exercise=' . $exercise)->withInput()->withErrors(['username' => Lang::get('auth.failed')]);
	// 			}
	// 		} else {
	// 			if ($isAuth) {
	// 				return redirect('programs');
	// 			} else {
	// 				return redirect('login')->withInput()->withErrors(['username' => Lang::get('auth.failed')]);
	// 			}
	// 		}
	// 	} catch (\Exception $e) {
	// 		Log::error(__CLASS__ . "::" . __METHOD__ . ' ' . $e->getMessage());
	// 		if (isset($request['redirectBack']) && isset($request['exercise'])) {
	// 			$exercise = $request['exercise'];
	// 			return redirect('login?redirectBack=user_exercise&exercise=' . $exercise)->withInput()->withErrors(['error' => Lang::get('custom.something_wrong')]);
	// 		} else {
	// 			return redirect('login')->withInput()->withErrors(['error' => Lang::get('custom.something_wrong')]);
	// 		}
	// 	}
	// }



public function demoFunction() {


	Log::error('Hello demo function here');
	print_r("demo function");
	exit();
}



   public function generateEncodedPassword() {

        // $data = $request->all();
        // Log::error($data);

        $name = 'pass@123';

        print_r(bcrypt($name));
        exit();
    }




	public function loginFo(Request $request) {

		Auth::logout();
		$response = array();
		$validator = Validator::make($request->all(), [
			'username' => 'required',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			$response['status'] = false;
			$response['message'] = Lang::get('custom.invalid_input');
			$response['errors'] = $validator->messages();
			return Response::json($response);
		}

		try {
			$username = trim($request->username);
			$password = trim($request->password);

			Log::error($username);
			Log::error($password);

			// $username = 'mohit.landge@vnnogile.in';
			// $password = 'pass@123';
			// $deviceToken = trim($request->device_token);
			// $deviceType = trim($request->device_type);

			$isAuth = self::login($username, $password); 
			Log::error($isAuth);
			// exit();
			// $this->userRepository->login($username, $password);
			if ($isAuth) {
				Log::error("validation passed");
				$user = Auth::user();

				Log::error($user);

				if ($user->accessTokens->count() == 0) {
				// if (true) {
					//Delete old access tokens
					// $user->accessTokens()->delete();

					$token = $user->createToken('MyApp')->accessToken;

					$updateStatus = self::updateUserDetail(new App\User, array('api_token' => $token), $user['party_id']);
					if ($updateStatus) {
						$user['api_token'] = $token;
					}

				}

				// $token = $user->createToken('MyApp')->accessToken;

				// $updateStatus = $this->userRepository->updateUserDetail(new App\User, array('api_token' => $token, 'device_token' => $deviceToken, 'device_type' => $deviceType), $user['party_id']);
				// if ($updateStatus) {
				// 	$user['api_token'] = $token;
				// }

				$hash = new Hashids(config('app.hash_security_key'), 10);
				$userHash = $hash->encode($user['id']);

				$response['status'] = true;
				$response['message'] = Lang::get('custom.auth_success');
				$response['user_hash'] = $userHash;
				$response['user_detail'] = App\User::where('id', $user['id'])->select('id', 'first_name', 'last_name', 'email', 'personal_email', 'api_token', 'userid')->first();
				$response['user_profile_detail'] = self::getUserProfileDetail($user['id']);
				// $response['user_organization_detail'] = $this->userRepository->getUserOrganizationDetail($user['id'], $response['user_profile_detail']['customer_party_id']);

				/// ML
				// $appLanguages = $this->MultilingualRepository->getAppLanguages($user['id']);
				// $response['user_organization_detail']['app_languages'] = $appLanguages;
				// $userLanguageDetails = $this->MultilingualRepository->getUserLanguageDetails($user['id']);
				// $response['user_profile_detail']['preferred_language'] = $userLanguageDetails['preferred_language'];
				// $response['user_profile_detail']['selected_app_language'] = $userLanguageDetails['app_language'];
				// ML

			} else {
				$response['status'] = false;
				$response['message'] = Lang::get('auth.failed');
			}
		} catch (\Exception $e) {
			Log::error(__CLASS__ . "::" . __METHOD__ . ' ' . $e->getMessage());
			$response = array();
			$response['status'] = false;
			$response['message'] = Lang::get('custom.something_wrong');
		}
		return Response::json($response);
	}


public function login($username, $password, $isRemember = false) {

		if (Auth::attempt(['email' => $username, 'password' => $password, 'is_active' => 1], $isRemember)) {
			return true;
		} elseif (Auth::attempt(['personal_email' => $username, 'password' => $password, 'is_active' => 1], $isRemember)) {
			return true;
		} elseif (Auth::attempt(['userid' => $username, 'password' => $password, 'is_active' => 1], $isRemember)) {
			return true;
		} else {
			return false;
		}
	}


	public function updateUserDetail($model, $userArray = array(), $userPartyId) {
		return $model::where('party_id', $userPartyId)
			->update($userArray);
	}



		public function getUserProfileDetail($userId) {
		try {
			$userProfile = App\User::select('people.id', 'people.first_name', 'people.last_name', 'people.email', 'people.personal_email',
				'people.gender', 'people.employee_id', 'people.department', 'people.designation', 'people.linkedin_profile', 'people.facebook_id',
				'people.twitter_handle', 'people.google_profile', 'people.party_id', 'people.tenant_party_id', 'people.customer_party_id',
				DB::raw('CONCAT("' . asset('/') . '", "storage", people.photo) AS photo'),
				'work_phones.phone_number as work_phone_number',
				'home_phones.phone_number as home_phone_number',
				'is_private', 'users.id as user_id'
			)
				->join('parties', function ($join) {
					$join->on('users.party_id', '=', 'parties.id');
				})
				->join('people', function ($join) {
					$join->on('people.party_id', '=', 'parties.id');
				})
				->leftjoin('phones as work_phones', function ($join) {
					$join->on('users.party_id', '=', 'work_phones.party_id');
					$join->where('work_phones.phone_type_id', '=', 1);
				})
				->leftjoin('phones as home_phones', function ($join) {
					$join->on('users.party_id', '=', 'home_phones.party_id');
					$join->where('home_phones.phone_type_id', '=', 2);
				})
				->where('users.id', $userId)
				->first();

			if ($userProfile) {
				$userProfile = $userProfile->toArray();
				//Added by vnnogile - to not show the phone number in case the profile is private
				if ($userProfile['is_private'] == 1) {
					$userProfile['work_phone_number'] = '**********';
				}
				//Added by vnnogile - to not show the phone number in case the profile is private
				//Calculating profile completion percentage
				$completedFields = 0;
				foreach ($userProfile as $key => $item) {
					if (in_array($key, $this->userProfileCompletionFields)) {
						if (!is_null($item) && trim($item) != '') {
							$completedFields++;
						}
					}
				}
				$userProfile['profile_completion_percentage'] = round(($completedFields / count($this->userProfileCompletionFields)) * 100, 2);
				return $userProfile;
			} else {
				return null;
			}

		} catch (\Exception $exception) {
			Log::error(__CLASS__ . "::" . __METHOD__ . ' ' . $exception->getMessage());
			return null;
		}
	}

}

