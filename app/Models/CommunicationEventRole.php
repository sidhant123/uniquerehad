<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class CommunicationEventRole
 * 
 * @property int $id
 * @property int $communication_event_id
 * @property int $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \App\Models\CommunicationEvent $communication_event
 * @property \App\Models\Role $role
 * @property \Illuminate\Database\Eloquent\Collection $templates
 *
 * @package App\Models
 */
class CommunicationEventRole extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'communication_event_role';

	protected $casts = [
		'communication_event_id' => 'int',
		'role_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'communication_event_id',
		'role_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function communication_event()
	{
		return $this->belongsTo(\App\Models\CommunicationEvent::class);
	}

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class);
	}

	public function templates()
	{
		return $this->belongsToMany(\App\Models\Template::class, 'communication_event_role_templates', 'communication_event_role_id', 'templates_id')
					->withPivot('id', 'deleted_at', 'created_by', 'updated_by', 'deleted_by')
					->withTimestamps();
	}
}
