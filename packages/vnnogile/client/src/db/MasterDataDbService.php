<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace vnnogile\Client\Db;

//Custom imports

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\MasterDataInterface;

/**
 * Class ConfigurationService
 * 
 */
class MasterDataDbService implements MasterDataInterface
{

	public function all($model, $columns = array('*')){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        //$modelArray = explode("\\", $model);
        //$modelWithoutNamespace = end($modelArray);
        
        //First get the configuration information for this entity
        $cacheConfig = EntityConfigService::retrieveEntityConfiguration($model);
        Log::debug("*****FOR MODEL***********");
        Log::debug($model);
        Log::debug($cacheConfig);
		
		//now put the entityNamespace in front of it
		$model = $cacheConfig['entityNamespace'] . $model;
		$model = $this->makeModel($model);
		

		//If there is no caching invokved, then exit after firing query
		switch ($cacheConfig['cacheKey']) {
			case 'NA':
				Log::info("No caching for " . $model->getTable());
				Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");		
				return $model->select($this->getColumnNames($model, $columns))->get();
				break;
		}

		//If there is caching, then following logic is executed
		switch ($cacheConfig['cacheDuration']) {
			case 0:
				$value = Cache::tags($cacheConfig['cacheTagName'])->rememberForever($cacheConfig['cacheKey'], function () use($model, $columns) {
				    return $model->select($this->getColumnNames($model, $columns))->get();
				});
				break;
			
			default:
				$value = Cache::tags($cacheConfig['cacheTagName'])->remember($cacheConfig['cacheKey'], $cacheConfig['cacheDuration'], function () use($model, $columns){
				    return $model->select($this->getColumnNames($model, $columns))->get();
				});
				break;
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $value;
	}

	public function find($model, $id, $columns = array('*')){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //First get the configuration information for this entity
        $cacheConfig = EntityConfigService::retrieveEntityConfiguration($model);
		
		//now put the namespace in front of it
		$model = $cacheConfig['entityNamespace'] . $model;

		$model = $this->makeModel($model);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $model->select($this->getColumnNames($model, $columns))->find($id);
	}
	
	public function findBy($model, $whereConditions, $columns = array('*')){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //First get the configuration information for this entity
        $cacheConfig = EntityConfigService::retrieveEntityConfiguration($model);
		Log::debug('cacheConfig');
		Log::debug($cacheConfig);
		//now put the namespace in front of it
		$model = $cacheConfig['entityNamespace'] . $model;

		$model = $this->makeModel($model);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $model->select($this->getColumnNames($model, $columns))->where($whereConditions)->get();
	}

	public function flushCache($cacheTagName){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Cache::tags($cacheTagName)->flush();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}	

	public function filteredList($model, $whereCondition, $id, $columns = array('*')) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

/*        $modelArray = explode("\\", $model);
        $modelWithoutNamespace = end($modelArray);*/
        //First get the configuration information for this entity
        $cacheConfig = EntityConfigService::retrieveEntityConfiguration($model);
		
		//now put the namespace in front of it
		$model = $cacheConfig['entityNamespace'] . $model;
		$model = $this->makeModel($model);

		$cacheKey = $cacheConfig['cacheKey'] . '_' . $id;
		if ($cacheConfig['cacheDuration'] == 0){
			$value = Cache::tags($cacheConfig['cacheTagName'])->rememberForever($cacheKey, function () use($model, $whereCondition) {
			    return $model->select('id', 'name')->where($whereCondition)->get();
			});
		}else{
			$value = Cache::tags($cacheConfig['cacheTagName'])->remember($cacheKey, $cacheConfig['cacheDuration'], function () use($model, $whereCondition) {
			    return $model->select('id', 'name')->where($whereCondition)->get();
			});			
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $value;
	}

	public function list($model, $columns = array('*')) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		Log::error("MODEL : ". $model);

        //$modelArray = explode("\\", $model);
        //$modelWithoutNamespace = end($modelArray);
        //First get the configuration information for this entity
        $cacheConfig = EntityConfigService::retrieveEntityConfiguration($model);
		
		//now put the namespace in front of it
		$model = $cacheConfig['entityNamespace'] . $model;
		$model = $this->makeModel($model);

		if ($cacheConfig['cacheDuration'] == 0){
			$value = Cache::tags($cacheConfig['cacheTagName'])->rememberForever($cacheConfig['cacheKey'], function () use($model) {
			    return $model->select('id', 'name')->get();
			});
		}else{
			$value = Cache::tags($cacheConfig['cacheTagName'])->remember($cacheConfig['cacheKey'], $cacheConfig['cacheDuration'], function () use($model) {
			    return $model->select('id', 'name')->get();
			});			
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $value;
	}	

	//Utility Functions
	function underscoreToCamelCase($string, $capitalizeFirstCharacter = false) 
	{
	    $str = str_replace('_', '', ucwords($string, '_'));
	    if (!$capitalizeFirstCharacter) {
	        $str = lcfirst($str);
	    }
	    return $str;
	}

	//Model related functions
	// function setModel($model){
	// 	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	// 	$this->makeModel($app);
	// 	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	// }

    function makeModel($model) {
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$model = \App::make($model);
        if (!$model instanceof Model)
           throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $model;
    }

    function getColumnNames($model, $columns){
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    	if ($columns[0] == '*'){
    		//which means all columns	
    		$fillable = $model->getFillable();
    		$hidden = $model->getHidden();
    		$columns = array_diff($fillable, $hidden); 
    		array_unshift($columns, 'id');
    	}
    	foreach ($columns as $property) {
    		$changedProperty = $this->underscoreToCamelCase($property);
    		$columnNames[] =$property . ' as ' . $changedProperty;
    	}		 

    	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    	return $columnNames;
    }
}
