Create sequence expenses_seq; 

-- Table: public.expenses

-- DROP TABLE public.expenses;

CREATE TABLE public.expenses
(
  id bigint NOT NULL DEFAULT nextval('expenses_seq'::regclass),
  project_id integer NOT NULL,
  expense_date date NOT NULL,
  expense_type_id integer NOT NULL,
  organization_id integer NOT NULL,
  service_name varchar NOT NULL,
  bill_no integer,
  item_desc varchar NOT NULL, 
  unit varchar(30) NOT NULL,
  qty integer NOT NULL,
  rate numeric(8,2) NOT NULL,
  amount numeric(10,2) NOT NULL,
  gst_amount numeric(10,2) NOT NULL,
  amount_paid numeric(10,2),
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT expenses_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.expenses
  OWNER TO postgres;

-- ---------------------------------------------------------------------------------------------


INSERT INTO public.menus(
     id, display_name, name, sequence,  auth, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (200, 'Expenses', 'Expenses', 120,  1, '', now(), now(),  1, 1);


INSERT INTO public.menus(
      display_name, name, sequence, parent_menu_id,  auth, created_at, updated_at,  created_by, updated_by)
    VALUES  
    ( 'List', 'List Expenses', 125, 200,  1, now(), now(),  1, 1), 
    ( 'New Expenses', 'Create Expenses', 130, 200,  1, now(), now(),  1, 1);

-- Permission table insert query 
-- By Dhruvi 10-may-2020


INSERT INTO public.permissions(
            id, name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (49,'Expenses', now(), now(), 1, 1),
     (50, 'List Expenses', now(), now(), 1, 1),
     (51, 'Create Expenses', now(), now(), 1, 1),
     (52, 'Show Expenses', now(), now(), 1, 1),
     (53, 'Delete Expenses', now(), now(), 1, 1),
     (54, 'Edit Expenses', now(), now(), 1, 1);

INSERT INTO public.permission_role(
            id, role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     (49, 1, 49, now(), now(), 1,1),
     (50, 1, 50, now(), now(), 1,1),
     (51, 1, 51, now(), now(), 1,1),
     (52, 1, 52, now(), now(), 1,1),
     (53, 1, 53, now(), now(), 1,1),
     (54, 1, 54, now(), now(), 1,1);


INSERT INTO public.list_view_masters(
             function_name, entity_name,  
             query_columns, 
             heading_columns,
             filter_columns, 
             display_css_widths, 
             relations, replace_columns,
              created_at, updated_at, created_by, updated_by,
              filter_class_names,
              entity_namespace)
    VALUES ('List Expenses', 'Expense',
            'project_name,company_name,expense_type,expense_date,service_name,amount,id',
            'Project,Expense to Company,Expense Type,Expense Date,Service,Total Amount,Action', 
             '', 
             'col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-2,col-xs-1', 
             '', '', now(),now(),1,1, 
             'App\Utilities\ExpenseFilterUtility', 
             'App\Models\');

