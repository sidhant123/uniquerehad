@extends('receipt.receipt')
@section('title','Update Receipt')
@section('editMethod')
{{method_field('post')}}
@endsection
@section('view-nav-bar', 'hide')
@section('update-nav-bar', 'hide')
@section('edit_btn','hide')

@section('importReceipts')


<div class="row col-xs-12">           
 <p></p> 
</div>
<div class="row col-xs-12">           
 <p></p> 
</div>



<div class="row col-xs-12" align="center" style="color: black; ">           
  OR   
</div>
<p></p>

<form action="/receiptImport" method="post"  enctype="multipart/form-data"  onsubmit="return validateMandatoryFieldsForUserImport()" style="border-style:dotted; border-width: 1px;">
    {{ csrf_field() }}
  <h3 class="headings">{{__('labels.receipts.import_receipt')}}</h3>

<div>
  <div class="row">
  <div class="col-xs-6 req">
     <div class="form-group required">
              <label for="org_id@yield('org_name_d')">{{ __('labels.receipts.project_name') }}</label>
              <select  class="form-control importMandatory" name="project_id"
               id="project_id"@yield('org_name_d')>
                  @if(isset($project))
                  <option value="0">--select--</option>
                  @foreach($project as $pro_val)
                  @if(isset($sel_project_id))
                  @if($pro_val->id === $sel_project_id)
                  <option value="{{$pro_val->id}}" selected>{{$pro_val->name}}</option>
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>    
    </div>
     <div class="col-xs-6 required">
      <div class="form-group">
          <label for="receiptFile">{{__('labels.receipts.import_receipt')}}</label>
          <input type="file" class="form-control fillFields importMandatory"  id="receiptFile"  name="imported_receipt_file" accept=".xlsx" >
        </div>
    </div>
  </div> 

<p></p>
<div class="row">
<div class="col-xs-12" align="center">
    <button class="btn btn-primary">
    <a href="\storage\ExcelFormat\Receipt_Import_Format.xlsx" target="_blank">Download Excel Format</a></button>
    &emsp;
    <button type="submit" class="btn btn-small btn-primary">{{__('labels.users.submit')}}</button>

  <p></p>
</form>

@endsection
