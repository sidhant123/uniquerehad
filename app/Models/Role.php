<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;
use Illuminate\Support\Facades\Log;
/**
 * Class Role
 * 
 * @property int $id
 * @property string $name
 * @property int $tenant_party_id
 * @property int $customer_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $communication_events
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $program_roles
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Role extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'tenant_party_id' => 'int',
		'customer_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'tenant_party_id',
		'customer_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}

	public function communication_events()
	{
		return $this->belongsToMany(\App\Models\CommunicationEvent::class)
					->withPivot('id', 'deleted_at', 'created_by', 'updated_by', 'deleted_by')
					->withTimestamps();
	}

	public function permissions()
	{
		return $this->belongsToMany(\App\Models\Permission::class)
					->withPivot('id', 'deleted_at', 'created_by', 'updated_by', 'deleted_by')
					->withTimestamps();
	}

	public function program_roles()
	{
		return $this->hasMany(\App\Models\ProgramRole::class, 'app_role_id');
	}

	public function users()
	{
		return $this->belongsToMany(\App\User::class);
	}

	public function hasAccess(array $permissions) : bool {
		    Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        foreach ($permissions as $permission) {
            if ($this->hasPermission($permission)){
            	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
                return true;
            }
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return false;
	}

	private function hasPermission(string $permissionToBeChecked) : bool
	{
		    Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
			$permissions = $this->permissions;
			foreach ($permissions as $permission) {
				Log::debug("permission to be checked " . $permissionToBeChecked);
				Log::debug("master permission " . $permission->name);
				if ($permission->name == $permissionToBeChecked){
					Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
					return true;
				}
			}
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return false;
	}
}
