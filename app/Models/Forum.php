<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Forum
 * 
 * @property int $id
 * @property int $program_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Program $program
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $threads
 *
 * @package App\Models
 */
class Forum extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function program()
	{
		return $this->belongsTo(\App\Models\Program::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function threads()
	{
		return $this->hasMany(\App\Models\Thread::class, 'forums_id');
	}
}
