<?php

namespace vnnogile\Communication\Listeners;

use App\User;
use Mail;

use vnnogile\Communication\Events\CommEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

//Custom Imports Below
use App\Models\CommunicationChannel;
use App\Models\CommunicationEvent;
use App\Models\CommunicationEventRole;
use App\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;
use vnnogile\Communication\Utilities\InAppNotificationUtility;
use vnnogile\Mailer\Utility\EmailUtility;
use vnnogile\Sms\Utility\SmsUtility;
use vnnogile\SystemNotification\Utility\SystemNotificationUtility;

class CommEventListener
{

    // protected $masterDataInterface;
    // protected $utilsInterface;
    // public $tries = 1;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
    }


    /**
     * Handle the event.
     *
     * @param  UserUploadEvent  $event
     * @return void
     */
    public function handle(CommEvent $event)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug("****Event Name****** " . $event->eventName);
        $data=$event->data;
        $tenant_code=$event->tenant_code;
        $login_id=$event->login_id;

        $event_row= CommunicationEvent::where('name',$event->eventName)->first();
        $event_id= $event_row['id'];

    // getting template
        //$roleChannelTemplate structure
        // Array ( [Participant] => Array ( [EMAIL] => Array ( [template_sub] => [template] =>
        // Hello {PARTICIPANT.NAME}, you are added to the {ORG.NAME} training system.
       $template_idRoleChannelTemplate=self::extractRoleWiseTemplates($event_row,$data);
       //dump($template_idRoleChannelTemplate);
       //exit();
        Log::debug("******************TEMPLATE******************");
        Log::debug($template_idRoleChannelTemplate);
       foreach ($template_idRoleChannelTemplate as $template_id => $roleChannelTemplate) {
        foreach ($roleChannelTemplate as $role => $channelTemplate) {
            foreach ($channelTemplate as $channel => $templateData) {

                //dump($data[$role]);
                if(isset($data[$role])){
                  foreach ($data[$role] as $id => $id_value) {
                    $data[$role][$id]['template']=$templateData['template'];
                    $data[$role][$id]['template_subject']=$templateData['template_subject'];
                    $data[$role][$id]['template_cc']=$templateData['template_cc'];

                    $data[$role][$id]['tenant_code']=$tenant_code;
                    $data[$role][$id]['login_id']= $login_id;
                    $data[$role][$id]['communication_event_id']=$event_id;
                  }

                  if($channel=='EMAIL'){
                    Log::debug('-------APP Channel-----------');
                    $return=EmailUtility::sendCommunicationEmail($data[$role]);
                  }

                  if($channel=='APP'){
                    Log::debug('-------APP Channel-----------');
                    $return = InAppNotificationUtility::sendInAppNotification($data[$role]);
                  }

                  if($channel=='SMS'){
                    Log::debug('-------SMS Channel-----------');
                    $return = SmsUtility::sendCommunicationSms($data[$role], $this->masterDataInterface, $this->utilsInterface);
                  }

                  if($channel=='MOB NOTIFICATION'){
                    Log::debug('-------MOB NOTIFICATION Channel-----------');
                    $return = SystemNotificationUtility::sendCommunicationMobileNotification($data[$role], $this->masterDataInterface, $this->utilsInterface);
                  }

               }//closing of if(isset)
            }
        }
       }//closing of main foreach

       Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public static function extractRoleWiseTemplates($event_row,$data){
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $templates=array();
        $roles= Role::all();
        $channels= CommunicationChannel::all();

        // $event = CommunicationEvent::where('name',$event)->first();
        $temp = $event_row->templates()->get();
        Log::debug("******************TEMPLATE DATA******************");
         Log::debug($temp);
        foreach ($temp as $key => $template_row) {
          $channel=CommunicationChannel::find($template_row['communication_channel_id']);

          //getting cc EmailId;
          $cc_mails=null;
          $ccs=$template_row->cc;
          if(isset($ccs)){
          $ccs=explode(",", $ccs);
           foreach ($ccs as $int => $role_id) {

             $role=$roles->where('id',$role_id)->first();

             if(isset($data[$role->name])){
              foreach ($data[$role->name] as $id => $value) {
                  $cc_mails[]=$value['email_id'];
               }
             }
           }  //for foreach
          }  //for if
          else{
            $cc_mails=null;
          }

          //setting templates template_id and template_cc role wise
          $tos=$template_row->to;
          if(isset($tos)){
          $tos=explode(",", $tos);
          foreach ($tos as $int => $role_id) {
             $role=$roles->where('id',$role_id)->first();

             $templates[$template_row->id][$role->name][$channel->name]=['template'=>$template_row['template'],'template_subject'=>$template_row['subject'],'template_cc'=>$cc_mails];
          }
          }
        }
         Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $templates;
      }
}
