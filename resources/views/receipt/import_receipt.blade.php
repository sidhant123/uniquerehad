@extends('receipt.receipt')
@section('title','Create Payment')
@section('editMethod')
{{method_field('post')}}
@endsection
@section('view-nav-bar', 'hide')
@section('update-nav-bar', 'hide')
@section('edit_btn','hide')

@section('importReceipts')


<div class="row col-xs-12">           
 <p></p> 
</div>
<div class="row col-xs-12">           
 <p></p> 
</div>


<p></p>
<div>
  <div class="row">
  <div class="col-xs-6 req">
     <div class="form-group required">
              <label for="org_id@yield('org_name_d')">{{ __('labels.receipts.org_name') }}</label>
              <select  class="form-control mandatory" name="project_id"
               id="project_id"@yield('org_name_d')>
                  @if(isset($project))
                  <option value="0">--select--</option>'
                  @foreach($project as $pro_val)
                  @if(isset($sel_peoject_id))
                  @if($pro_val->id === $sel_peoject_id)
                  <option value="{{$pro_val->id}}" selected>{{$pro_val->name}}</option>
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>    
    </div>
     <div class="col-xs-6 required">
      <div class="form-group">
          <label for="paymentFile">{{__('labels.receipts.import_payment')}}</label>
          <input type="file" class="form-control fillFields mandatory"  id="paymentFile"  name="imported_payment_file" accept=".xlsx" >
        </div>
    </div>
  </div> 



<p></p>
@endsection
