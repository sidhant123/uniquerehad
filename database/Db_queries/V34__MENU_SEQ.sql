delete from menus;

INSERT INTO public.menus(
     id, display_name, name, sequence, parent_menu_id, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (300, 'Companies', 'Companies', 1,  null, 'menu-vnn vnn-home', now(), now(),  1, 1),
    (301,'List', 'List Companies', 5, 300,  '', now(), now(),  1, 1), 
    (302, 'New Company', 'Create Companies', 15, 300, '', now(), now(),  1, 1),

    (303, 'Users', 'Users', 20,  null, 'menu-vnn vnn-user', now(), now(),  1, 1),
    (304,'List', 'List Users', 25, 303,  '', now(), now(),  1, 1), 
    (305, 'New User', 'Create Users', 30, 303, '', now(), now(),  1, 1), 

    (306, 'Materials', 'Materials', 35,  null, '', now(), now(),  1, 1),
    (307,'List', 'List Materials', 40, 306,  '', now(), now(),  1, 1), 
    (308, 'New Material', 'Create Materials', 45, 306, '', now(), now(),  1, 1), 

    (309, 'Expense Types', 'Expense Types', 50,  null, '', now(), now(),  1, 1),
    (310,'List', 'List Expense Types', 55, 309,  '', now(), now(),  1, 1), 
    (311, 'New Expense Type', 'Create Expense Types', 60, 309, '', now(), now(),  1, 1), 

    (312, 'Sites', 'Sites', 65,  null, '', now(), now(),  1, 1),
    (313,'List', 'List Sites', 70, 312,  '', now(), now(),  1, 1), 
    (314, 'New Site', 'Create Sites', 75, 312, '', now(), now(),  1, 1), 

    (315, 'Manpower Contracts', 'Manpower Contracts', 80,  null, '', now(), now(),  1, 1),
    (316,'List', 'List Manpower Contracts', 85, 315,  '', now(), now(),  1, 1), 
    (317, 'New Manpower Contract', 'Create Manpower Contracts', 90, 315, '', now(), now(),  1, 1), 
 
    (318, 'Projects', 'Projects', 95,  null, '', now(), now(),  1, 1),
    (319,'List', 'List Projects', 100, 318,  '', now(), now(),  1, 1), 
    (320, 'New Project', 'Create Projects', 105, 318, '', now(), now(),  1, 1), 

    (321, 'Expenses', 'Expenses', 110,  null, '', now(), now(),  1, 1),
    (322,'List', 'List Expenses', 115, 321,  '', now(), now(),  1, 1), 
    (323, 'New Expense', 'Create Expenses', 120, 321, '', now(), now(),  1, 1), 

    (324, 'Payments', 'Payments', 125,  null, '', now(), now(),  1, 1),
    (325,'List', 'List Payments', 130, 324,  '', now(), now(),  1, 1), 
    (326, 'Update Payment', 'Create Payments', 135, 324, '', now(), now(),  1, 1);

   