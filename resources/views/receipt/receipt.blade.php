@extends('layouts.base')
@section('content')

  @include('errors.error_validation')


  <form style="border-style:dotted; border-width: 1px;" action="/receipts{{null}}@yield('editID')" method="post" onsubmit="return validateMandatoryFieldsForReceipt()" enctype="multipart/form-data" >
    {{ csrf_field() }}

    @section('editMethod')
    @show
    @include('navbar.nav_bar',['save_btn' => 'save_btn','url'=> '/receipts'])
    <div class="row">
      <div class="col-lg-11">
      <h3 class="headings">
      {{ucfirst(explode('.',Route::currentRouteName())[1])}} Receipt

      </h3></div>
    </div>

<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.users.submit')}}</button>
</div>
</div>
<p></p>

   <div class="row">
      <div class="col-lg-6">
      
  <div class="form-group required">
              <label for="project_id@yield('project_name_d')">{{ __('labels.receipts.project_name') }}

              </label>
              <select  class="form-control mandatory" name="project_id"
               id="project_id"@yield('project_name_d')>
                  @if(isset($project))
                  <option value="0">--select--</option>'
                  @foreach($project as $pro_val)
                  @if(isset($sel_project_id))
                  @if($pro_val->id === $sel_project_id)
                  <option value="{{$pro_val->id}}" selected>{{$pro_val->name}}</option>
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$pro_val->id}}">{{$pro_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>
    </div>    
</div>
   

   <div class="col-lg-6">
   <div class="form-group">
       <label for="name">{{__('labels.receipts.boq_no')}}</label>
       <select  class="form-control" name="boq_no"
               id="boq_no"@yield('org_name_d')>
                  @if(isset($boq))
                  <option value="">--select--</option>'
                  @foreach($boq as $boq_val)
                  @if(isset($sel_boq_no))
                  @if($boq_val->name === $sel_boq_no)
                  <option value="{{$boq_val->name}}" selected>{{$boq_val->name}}</option>
                  @else
                  <option value="{{$boq_val->name}}">{{$boq_val->name}}</option>
                  @endif
                  @else
                  <option value="{{$boq_val->name}}">{{$boq_val->name}}</option>
                  @endif
                  @endforeach
                  @endif
              </select>      
     </div>
 </div> 
</div>

    <div class="row">
  
     <div class="col-lg-6">
       <div class="form-group required">
           <label for="invoice_no">{{__('labels.receipts.invoice_no')}}</label>
            @if(!empty(old('invoice_no')))
            <input type="text"  class="form-control mandatory" name="invoice_no"  value="{{ old('invoice_no') }}"  style="text-transform:uppercase " @yield('invoice_no_d')>
            @else
            <input type="text" class="form-control mandatory" name="invoice_no" value="@yield('invoice_no')"  style="text-transform:uppercase " @yield('invoice_no_d')>
            @endif
         </div>
     </div>

       <div class="col-lg-6">
    <div class="form-group required">
           <label for="invoice_date">{{__('labels.receipts.invoice_date')}}</label>
            @if(!empty(old('invoice_date')))
            <input type="date" class="form-control mandatory datepicker" name="invoice_date" id="invoice_date" 
             value="{{ old('invoice_date') }}" @yield('invoice_date_d') onkeydown="return false" />
            @else
            <input type="date" class="form-control mandatory datepicker" name="invoice_date"  id="invoice_date"  value="@yield('invoice_date')"  @yield('invoice_date_d')  onkeydown="return false"/>
            @endif

         </div>  
   </div>
  </div>

   <div class="row">
      <div class="col-lg-4">
       <div class="form-group required">
           <label for="base_amount">{{__('labels.receipts.base_amount')}}</label>
            @if(!empty(old('base_amount')))
            <input type="number" step=".01" min="1"  class="form-control mandatory" id="base_amount" name="base_amount"  value="{{ old('base_amount') }}"  @yield('base_amount_d')>
            @else
            <input type="number" step=".01" min="1"  class="form-control mandatory" id="base_amount" name="base_amount"  value="@yield('base_amount')"  @yield('base_amount_d')>
            @endif

         </div>
     </div> 

      <div class="col-lg-4">
       <div class="form-group required">
           <label for="gst_amount">{{__('labels.receipts.gst_amount')}}</label>
            @if(!empty(old('gst_amount')))
            <input type="number" step=".01" min="1"  class="form-control mandatory" id="gst_amount" name="gst_amount"  value="{{ old('gst_amount') }}"  @yield('gst_amount_d')>
            @else
            <input type="number" step=".01" min="1"  class="form-control mandatory" id="gst_amount" name="gst_amount"  value="@yield('gst_amount')"  @yield('gst_amount_d')>
            @endif

         </div>
     </div> 
    <div class="col-lg-4">
    <div class="form-group required">
           <label for="invoice_amount">{{__('labels.receipts.invoice_amount')}}</label>
            @if(!empty(old('invoice_amount')))
            <input type="number" step=".01" min="0" class="form-control mandatory" id="invoice_amount" name="invoice_amount" readonly="readonly" value="{{ old('invoice_amount') }}" @yield('invoice_amount_d') />
            @else
            <input type="number" step=".01" min="0"  class="form-control mandatory" id="invoice_amount" name="invoice_amount" readonly="readonly" value="@yield('invoice_amount')"  @yield('invoice_amount_d') />
            @endif

         </div>  
   </div>
   </div>
 <div class="row">
    <div class="col-lg-6">
    <div class="form-group">
           <label for="payment_ref_no">{{__('labels.receipts.payment_ref_no')}}</label>
            @if(!empty(old('payment_ref_no')))
            <input type="number" min="0" class="form-control " name="payment_ref_no" value="{{ old('payment_ref_no') }}" @yield('payment_ref_no_d') />
            @else
            <input type="number" min="0" class="form-control " name="payment_ref_no" value="@yield('payment_ref_no')"  @yield('payment_ref_no_d') />
            @endif

         </div>  
   </div>
     <div class="col-lg-6">
       <div class="form-group">
           <label for="payment_method">{{__('labels.receipts.payment_method')}}</label>
          
            <select  class="form-control" name="payment_method"
               id="payment_method"@yield('payment_method_d')>
                  <option value="0">--select--</option>
                    @if(isset($row_data->payment_method))
                  <option value="Cash" {{$row_data->payment_method=='Cash'?'selected':''}} >Cash</option>
                  <option value="Cheque" {{$row_data->payment_method=='Cheque'?'selected':''}}>Cheque</option>
                  <option value="Online" {{$row_data->payment_method=='Online'?'selected':''}}>Online</option>
                 <option value="Other" {{$row_data->payment_method=='Other'?'selected':''}}>Other</option>
                  @else 
                  <option value="Cash" >Cash</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Online">Online</option>
                  <option value="Other">Other</option>
                  @endif
              </select>      

         </div>
     </div>

  </div>


   <div class="row">
  
     <div class="col-lg-6">
       <div class="form-group">
           <label for="payment_receipt_date">{{__('labels.receipts.payment_receipt_date')}}</label>
            @if(!empty(old('payment_receipt_date')))

           
            <input type="date" class="form-control" name="payment_receipt_date" id="payment_receipt_date" 
              value="{{ old('payment_receipt_date') }}"  @yield('payment_receipt_date_d')  onkeydown="return false"/>
            @else
            <input type="date" class="form-control " name="payment_receipt_date" id="payment_receipt_date" 
             value="@yield('payment_receipt_date')"  @yield('payment_receipt_date_d') onkeydown="return false"/>
            @endif

         </div>
     </div>

    <div class="col-lg-6">
    <div class="form-group ">
           <label for="amount_received">{{__('labels.receipts.amount_received')}}</label>
            @if(!empty(old('amount_received')))
            <input type="number"  step=".01" min="0"   
            class="form-control " name="amount_received" value="{{ old('amount_received') }}" @yield('amount_received_d') />
            @else
            <input type="number"  step=".01" min="0"  class="form-control " name="amount_received" value="@yield('amount_received')"  @yield('amount_received_d') />
            @endif

         </div>  
   </div>
  </div>

<div class="row">
   <div class="col-lg-6">
    <div class="form-group ">
           <label for="tds_deduction">{{__('labels.receipts.tds_deduction')}}</label>
            @if(!empty(old('tds_deduction')))
            <input type="number"  step=".01" min="0" 
            class="form-control " name="tds_deduction" value="{{ old('tds_deduction') }}" @yield('tds_deduction_d') />
            @else
            <input type="number"  step=".01" min="0" class="form-control " name="tds_deduction" value="@yield('tds_deduction')"  @yield('tds_deduction_d') />
            @endif

         </div>  
   </div> <div class="col-lg-6">
    <div class="form-group ">
           <label for="other_deduction">{{__('labels.receipts.other_deduction')}}</label>
            @if(!empty(old('other_deduction')))
            <input type="number"  step=".01" min="0" 
            class="form-control " name="other_deduction" value="{{ old('other_deduction') }}" @yield('other_deduction_d') />
            @else
            <input type="number"  step=".01" min="0"  class="form-control " name="other_deduction" value="@yield('other_deduction')"  @yield('other_deduction_d') />
            @endif

         </div>  
   </div>
</div>


<div class="row">
   <div class="col-lg-6">
    <div class="form-group ">
           <label for="retension">{{__('labels.receipts.retension')}}</label>
            @if(!empty(old('retension')))
            <input type="number"  step=".01" min="0" 
            class="form-control " name="retension" value="{{ old('retension') }}" @yield('retension_d') />
            @else
            <input type="number"  step=".01" min="0" class="form-control " name="retension" value="@yield('retension')"  @yield('retension_d') />
            @endif

         </div>  
   </div> <div class="col-lg-6">
    <div class="form-group ">
           <label for="mobilization">{{__('labels.receipts.mobilization')}}</label>
            @if(!empty(old('mobilization')))
            <input type="number"  step=".01" min="0" 
            class="form-control " name="mobilization" value="{{ old('mobilization') }}" @yield('mobilization_d') />
            @else
            <input type="number"  step=".01" min="0"  class="form-control " name="mobilization" value="@yield('mobilization')"  @yield('mobilization_d') />
            @endif

         </div>  
   </div>
</div>
<div class="row">
   <div class="col-lg-6">
    <div class="form-group ">
           <label for="remark">{{__('labels.receipts.remark')}}</label>
            @if(!empty(old('remark')))
            <input type="text" class="form-control " name="remark" max="250" value="{{ old('remark') }}" @yield('remark_d') />
            @else
            <input type="text" class="form-control " name="remark" max="250" value="@yield('remark')"  @yield('remark_d') />
            @endif

         </div>  
   </div>
</div>
<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" id="save_btn" class="btn btn-small btn-primary hide"  @yield('sub_btn_d') >{{__('labels.users.submit')}}</button>
</div>
</div>
<p></p>
</form>

<p></p>

<!-- for import user -->
@section('importReceipts')
@show
@endsection

</form>

<p></p>



@push('scripts')
<style>
.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script>

$(document).ready(function() {
$('#boq_no').select2({
  });

$('#project_id').select2({
  }); 

 $(".mandatory").removeClass('fillFields');
     $(".importMandatory").removeClass('fillFields');

var now = new Date(),
    // max date the user can choose, in this case now and in the future
minDate = now.toISOString().substring(0,10);

$('#invoice_date').prop('max', minDate);
$('#payment_receipt_date').prop('max', minDate);
});


 var kk=$("#gst_amount");
    kk.keyup(function(){
       var total=isNaN(parseFloat(kk.val()) + parseFloat($("#base_amount").val())) ? 0 :
                      (parseFloat(kk.val()) + parseFloat($("#base_amount").val()))
        $("#invoice_amount").val(Number(total).toFixed(2));
    }); 

 var jj=$("#base_amount");
    jj.keyup(function(){
       var total=isNaN(parseFloat(jj.val()) + parseFloat($("#gst_amount").val())) ? 0 :
                      (parseFloat(jj.val()) + parseFloat($("#gst_amount").val()))
        $("#invoice_amount").val(Number(total).toFixed(2));
    }); 



$("#project_id").on("change", function() {
      // alert('ok');
      project_id = $("#project_id").val();
      console.log(project_id)
      if(project_id !=0) {
      // get_project_url = APP.'getCompanyName/'+formData;
        get_project_url= "{{url('/')}}"+"/getBoqList/"+project_id
        console.log(get_project_url)
        $.ajax({
          url: get_project_url ,
          method: 'get',
          type: 'JSON',
          success: function(response) {
            console.log('This is response')
            console.log(response)
            $('#boq_no option').remove();
            $('#boq_no').append("<option value=''>--Select--</option>");
            console.log($.type(response))
            for (var i = 0; i < response.length; i++) {
              console.log("BOQ LIST READING = "+response[i]['name']);
              $('#boq_no').append("<option id='boq_noOptions+"+response[i]['id']+"' value="+response[i]['name']+">"
                +response[i]['name']+"</option>");
            }
          },
          error: function() {
            alert('Error : There no Boqs')
          }
        })
      }else {
          $('#boq_no option').remove();

      }
    })



function validateMandatoryFieldsForReceipt(){

 var valid = true;
 valid=validateMandatoryFields();

 var project_id = $('#project_id').val();
    // alert(project_id);
  if(project_id == 0) {
document.getElementById("select2-project_id-container").style.border = "1px solid red";
} else {
  document.getElementById("select2-project_id-container").style.border = "0px solid red";
}

//   var boq_no = $('#boq_no').val();
//     // alert(boq_no);
//   if(boq_no == 0) {
// document.getElementById("select2-boq_no-container").style.border = "1px solid red";
// } else {
//   document.getElementById("select2-boq_no-container").style.border = "0px solid red";
// }

    return valid;
}
</script>
@endpush


