<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\DumpReportService;




class DumpReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	public function getDailyUsageCount(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");

		$queryBuilder = "
(select 'Daily progress report ' as Module, now()::timestamp as Date, count(id) as count from dprs where is_active='Y')
union 
(select 'Daily progress photo ' as Module, now()::timestamp as Date, count(id) as count from daily_progress_report_documents where is_active='1')
union 
(select 'Manpower contract' as Module, now()::timestamp as Date, count(id) as count from manpowers where is_active='1')
union 
(select 'Material request' as Module, now()::timestamp as Date, count(id) as count from material_request_details where is_active='1')
union 
(select 'Material inward ' as Module, now()::timestamp as Date, count(id) as count from material_inwards_details where is_active='1')
union 
(select 'Material consumption ' as Module, now()::timestamp as Date, count(id) as count from material_consumption_details where is_active='1')
union 
(select 'Material transfer' as Module, now()::timestamp as Date, count(id) as count from material_transfer_details where is_active='1')
union 
(select 'Material audit' as Module, now()::timestamp as Date, count(id) as count from material_audit_details where is_active='1')
union 
(select 'Attendance record' as Module, now()::timestamp as Date, count(id) as count from attendance_documents where is_active='1'  and created_date::date = CURRENT_DATE)";

        $queryBuilder = '(' . $queryBuilder . ') somealias';

	    self::downloadReportExcel($queryBuilder );
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadReportExcel($queryBuilder ) {
		$fileName = 'Daily_usage_count_Report';
		  ob_end_clean();
          ob_start();
          $row_data = DB::table(DB::raw($queryBuilder))->get();
         // dd($row_data);

        $keys = array("Module Name","Date", "Count");

        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array($row->module, $row->date, $row->count);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}

}
