<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 04 Jan 2018 22:50:52 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class ListViewMaster
 * 
 * @property int $id
 * @property string $function_name
 * @property string $entity_name
 * @property string $entity_namespace
 * @property string $query_columns
 * @property string $heading_columns
 * @property string $filter_columns
 * @property string $display_css_widths
 * @property string $relations
 * @property string $replace_columns
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by

 * @property int $deleted_by
 *
 * @package App\Models
 */
class ListViewMaster extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'function_name',
		'entity_name',
		'entity_namespace',
		'query_columns',
		'heading_columns',
		'filter_columns',
		'filter_class_names',
		'display_css_widths',
		'relations',
		'replace_columns',
		'html_tag',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
