<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class PermissionRole
 * 
 * @property int $id
 * @property int $role_id
 * @property int $permission_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Permission $permission
 * @property \App\Models\Role $role
 *
 * @package App\Models
 */
class PermissionRole extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'permission_role';

	protected $casts = [
		'role_id' => 'int',
		'permission_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'role_id',
		'permission_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function permission()
	{
		return $this->belongsTo(\App\Models\Permission::class);
	}

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class);
	}
}
