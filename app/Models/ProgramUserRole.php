<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 15:42:31 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramUserRole
 * 
 * @property int $id
 * @property int $master_program_id
 * @property int $learning_theme_id
 * @property int $user_id
 * @property int $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\LearningTheme $learning_theme
 * @property \App\Models\MasterProgram $master_program
 * @property \App\Models\Role $role
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class ProgramUserRole extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'master_program_id' => 'int',
		'learning_theme_id' => 'int',
		'user_id' => 'int',
		'role_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'master_program_id',
		'learning_theme_id',
		'user_id',
		'role_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function learning_theme()
	{
		return $this->belongsTo(\App\Models\LearningTheme::class);
	}

	public function master_program()
	{
		return $this->belongsTo(\App\Models\MasterProgram::class);
	}

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
