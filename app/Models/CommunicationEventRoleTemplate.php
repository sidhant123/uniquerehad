<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class CommunicationEventRoleTemplate
 * 
 * @property int $id
 * @property int $communication_event_role_id
 * @property int $templates_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\CommunicationEventRole $communication_event_role
 * @property \App\Models\Template $template
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class CommunicationEventRoleTemplate extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'communication_event_role_id' => 'int',
		'templates_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'communication_event_role_id',
		'templates_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function communication_event_role()
	{
		return $this->belongsTo(\App\Models\CommunicationEventRole::class);
	}

	public function template()
	{
		return $this->belongsTo(\App\Models\Template::class, 'templates_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}
}
