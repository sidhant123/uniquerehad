<?php

namespace vnnogile\Utilities\Services;


/**
 * This is a a common utility method for URL handling
 *
 */
class ConstantsUtility
{

    public const STARTED = " started";
    public const FINISHED = " finished";
    public const SEPARATOR = "::";
    public const USERPRINCIPAL = "userPrincipal";
    public const TENANT_PARTYID = "tenant_partyId";
    public const COMPANY_NAME = 'Cognigix Digital Learning';
    public const COMPANY_SHORT_NAME = 'CGX';
    public const EXCEL_HEADING_BACKGROUND = '#000000';
    public const EXCEL_HEADING_FONT_FAMILY = 'Trebuchet MS';
    public const EXCEL_HEADING_FONT_COLOR = '#ffffff';
    public const EXCEL_FILE_EXTENSION = 'xlsx';
    public const SUCCESS = 'SUCCESS';
    public const IN_PROGRESS = 'IN_PROGRESS';
    public const FAILURE = 'FAILURE';
    public const HELPDESK_EMAIL = 'helpdesk_email';
    public const HELPDESK_SMS = 'helpdesk_sms';
    public const HELPDESK_PHONE = 'helpdesk_phone';
    public const COMM_EMAIL_ID = 'email_id';
    public const COMM_PHONE = 'phone_no';
    public const COMM_PROGRAM_CODE = 'program_code';
    public const COMM_PARTY_ID = 'party_id';
    public const COMM_ATTACHMENTS = 'attachments';
    public const FAILED_COMMUNICATION_EVENT_NAME = 'FailedCommunicationsEvent';
    public const CONTENT_URL_VALIDATION_EVENT_NAME = 'ContentUrlValidationEvent';
    public const HTTP = 'http';
    public const VALID = 'Valid';
    public const INVALID = 'Invalid';

}
