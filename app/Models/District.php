<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class District
 * 
 * @property int $id
 * @property string $name
 * @property int $state_id
 * @property int $country_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Country $country
 * @property \App\Models\State $state
 * @property \Illuminate\Database\Eloquent\Collection $cities
 * @property \Illuminate\Database\Eloquent\Collection $pincodes
 *
 * @package App\Models
 */
class District extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'state_id' => 'int',
		'country_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'state_id',
		'country_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function state()
	{
		return $this->belongsTo(\App\Models\State::class);
	}

	public function cities()
	{
		return $this->hasMany(\App\Models\City::class);
	}

	public function pincodes()
	{
		return $this->hasMany(\App\Models\Pincode::class, 'districts_id');
	}
}
