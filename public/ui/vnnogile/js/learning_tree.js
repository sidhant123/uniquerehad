
/////Learning Tree next button
$(document).on('click','#header_next_btn',function(){

  if(document.getElementById('learning_tree_hidden_id').value=="tesseract"){

    alert('First save the Program');
  }
  else{
   $(".div").hide();
   $("#learning_tree_configuration").show();
   populateLearningTreeConfiguration();
 }
});


/////Learning Tree save button
$(document).on('click','#header_save_btn',function(){

  var formData = $('#header_form').serializeArray();
  var url='/headerLearningTree';
  if(validateMandatoryFields('header_form')== true){
    var check=sendingRequestThroughAxios(url,formData);
  }

  console.log('In Learning Tree saveBtn function');
});



 function validateMandatoryFields(id){

   var valid = true;
   $("#"+id+" "+".mandatory").each(function () {
     if ($(this).val().trim() === '' || $(this).val() === '0')
     {
      $(".mandatory").removeClass('fillFields');
      $(this).addClass('fillFields');
      valid = false;
      return valid;
    }
  });
   return valid;
 }

function errorResponse(errorResponse){
  console.log(errorResponse);
}



//axios to send msg
function sendingRequestThroughAxios(url,data){

  console.log("in sendingThroughAxios");

  axios({
    method: 'post',
    url: url,
    data: data,

  }).then(function(response) {

   if(url == '/headerLearningTree') {
      var lt=response['data'];

      console.log('after learning_treee save success');
      console.log(lt);

      // checking new version creation
      if(lt['version']=='old'){

      alert('Learning Tree has been saved saved successfully');

      if(document.getElementById('learning_tree_hidden_id').value=="tesseract"){
        console.log("keirn");
        //setting hidden in learning_tree div first time
        document.getElementById('learning_tree_hidden_id').value=response['data']['id'];
        $('#hidden_learning_tree_id_id').val(response['data']['id']);
       }
      }
      else if(lt['version']=='new') {

        // window.location = '/'+data[1];
        alert('new version of Learning Tree '+lt['name']+' has been created to be edited');
        window.location = '/learningtrees/'+lt['id']+'/edit';
        document.getElementById('learning_tree_hidden_id').value=lt['id'];
      }
   }

   else if (url=='/learningtree') {
     console.log(data);
     $(".div").hide();
     $("#participant").show();
   }




 }).catch(function (error) {
  console.log("error"+error);
});
}
/////////////////////////////////  ****************  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///
///
///














///
///
///
////////////////////////////////// L.T. Configuration \\\\\\\\\\\\\\\\\\\\\\\\\

// single object
// cells_nodes_object
         var cells_demo_object=new Object();

         // cells_demo_object['1']=[];
         // cells_demo_object['2']=[];
         // cells_demo_object['3']=[];
         // cells_demo_object['4']=[];
         // cells_demo_object['5']=[];

         // cells_demo_object['6']=[];
         // cells_demo_object['7']=[];
         // cells_demo_object['8']=[];
         // cells_demo_object['9']=[];
         // cells_demo_object['10']=[];


         var cells_nodes_object=new Object();

         var nodeData=new Object();

         var nodeNoData=new Object();


 function populateLearningTreeConfiguration(){
   var containerLearningTree=$('#hidden_learning_tree_id_id').val();
    getStructureObjectsFromDataBase(containerLearningTree);
}


  $('#preview_btn').click(function(){

    // getting strucutre objects from database
    // var containerLearningTree=$('#hidden_learning_tree_id_id').val();
    // getStructureObjectsFromDataBase(containerLearningTree);
    populateLearningTreeConfiguration();
    // appendTabs();
    // tabEvent('tab1');
    // createTables(cells_nodes_object);
  });

  var right_clicked_td;
  var artefact_type_id;

  var entityNameObject=new Object();
  // entityNameObject['vnn-bookmark draggable']='MajorNode';
  entityNameObject['vnn-node']='MajorNode';

  entityNameObject['vnn-video']='Video';
  entityNameObject['vnn-pdf']='Pdf';
  entityNameObject['vnn-word']='Word';
   entityNameObject['vnn-ppt']='Ppt';
  entityNameObject['vnn-document']='Document';

  entityNameObject['vnn-question']='Quiz';
  entityNameObject['vnn-bell']='Assessment';

  entityNameObject['vnn-vimeo']='Vimeo';
  entityNameObject['vnn-youtube']='Youtube';

///////// tabs starts
function tabEvent(id){
   console.log(id);
   $('.tab_button').css('background-color','#f1f1f1;');
   $('#'+id).css('background-color','#ddd');

   //setting tab value
   var tab_no=id.split('b');
   document.getElementById('hidden_tab_value').value=tab_no[1];

   // table events
   $('.tab_divs').hide();
   $('#tab_'+tab_no[1]+"_div").show();

   ///parent node appending

}

function increaseButton(){

    var i=1;
    $(".tab_button").each(function () {
           i++;
     });
    console.log(i);

  // providing madal  to be filled
    resetModal();

    // no entity type
    $('#hidden_entity_type_id').val('MajorNode');

    // hiding document part
    $('#document_section').hide();
     $('#icon_div').show();
    $('#node_title').html('Node '+i);
    $('#hidden_node_no').val(i);
    // parent node
    $('#parent_node_div').hide();
    // $('#parent_node').empty();
    // $('#parent_node').append('<option value="0">--select--</option>');

    $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
    });

    // saving topic in database
  }


  function tabStructure(i){

    console.log(i);

    var tab_id="tab"+i;
    $('#plus').remove();
    $('.tab').append("<button class='tab_button' id="+tab_id+" onclick=tabEvent('"+tab_id+"')>"+i+"</button>");
    $('.tab').append("<button id='plus' style='width: 30px;' onclick='increaseButton()'>+</button>");

    // appending new tab div
    $('#prime').append("<div id='tab_"+i+"_div' class='tab_divs'><table class='table table-bordered' style='border-color: blue;'><tbody id='table_body'></tbody></table></div>");


         var cells_demo_object=new Object();

         cells_demo_object['1']=[];
         cells_demo_object['2']=[];
         cells_demo_object['3']=[];
         cells_demo_object['4']=[];
         cells_demo_object['5']=[];

         cells_demo_object['6']=[];
         cells_demo_object['7']=[];
         cells_demo_object['8']=[];
         cells_demo_object['9']=[];
         cells_demo_object['10']=[];

    cells_nodes_object[i]=cells_demo_object;


    createTabWiseStructure(i,cells_nodes_object[i]);
    tabEvent(tab_id);

    // storing structure in database
    storingObjectInDatabase(nodeData,nodeNoData,cells_nodes_object,'save');

    return('successful');
  }
////////tabs ends


/////////drag an drop starts
function allowDrop(ev) {
    ev.preventDefault();
}
function noAllowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    console.log(data);
    var cl=$('#'+data).attr('class');
    console.log(data+" "+cl);

    $('#'+data).parent().append("<i class='"+cl+"' id='"+data+"' draggable='true' ondragstart='drag(event)' ></i>");
    ev.target.appendChild(document.getElementById(data));
    ev.target.setAttribute('ondragover','noAllowDrop(event)')


    //// before presenting modal
    resetModal();

    ////// setting current list a id
    $('#'+ev.target.id).attr('id','current_list');
    console.log($('#'+ev.target.id).html());


    // for docment path -> entity name;
    var icon_class = $('#'+data).attr('class');
    console.log(icon_class);
    saveEntityName(icon_class);

    /// getting current tab
    var tab=$('#hidden_tab_value').val();
    onParentNodeZero(tab);

    ///creating data of node not to parent(of same cell)
    appendParentNode(ev,'abc');


    $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
      });

}
///////////// d & d ends



/////////// modal starts
 $('#closeBtn').click(function() {

    // tab logic
    if ($('#parent_node').val() == null) {
         //removing tab
     }

    if ($('#hidden_old_node_no').val() != ""){
        // removing list
        $('#current_list').parent().remove();
        // removing id
        $('#current_list').removeAttr('id');
        // resetModal();
    }
  });

 $('#modal_close_sign').click(function() {

    // tab logic
    if ($('#parent_node').val() == null) {
         //removing tab
     }

    else if($('#hidden_old_node_no').val() == ""){
        // removing list
        $('#current_list').parent().remove();
        // removing id
        $('#current_list').removeAttr('id');
        // resetModal();
    }

  });



 $('#saveBtn').click(function() {

       var tab=$('#hidden_tab_value').val();
       var node_for_edit=$('#hidden_old_node_no').val();
       var containerLearningTreeId=$('#hidden_learning_tree_id_id').val();
       var fd = new FormData(document.getElementById("modal_form_id"));
       fd.append('containerLearningTree', containerLearningTreeId);
          
//////// tab logic
     if ($('#parent_node').val() == null) {
      
         var i=$('#hidden_node_no').val();
         console.log(i);
         var formDataForTab = $('#modal_form_id').serializeArray();
         var node_icon = document.getElementById('node_icon').files[0];
         fd.append('parent_node', 0);
          
         if(validateMandatoryFields('modal_form_id')== true){
           // var node_info=saveTopicInObject(formDataForTab);
           console.log('verified');
         }
         else{
           alert('please fill all the mandatory fields');
            return false;
         }
         console.log(node_info);

      // save in database
        
        formDataForTab.push({name: 'containerLearningTree', value:containerLearningTreeId});
        formDataForTab.push({name: 'parent_node', value:0});
        var url='/saveTopic';
        var topic_id=saveTopicInDatabase(url,fd);

        $('#parent_node_div').show();

        var a='for edit';
        if(node_for_edit == ""){
        a=tabStructure(i);
        }

        console.log(a);
        console.log(cells_nodes_object);
        // return false;
     }
///////// end tab logic
  else{

    /// deleting old node data from nodeNoData and nodeData obj
     if(node_for_edit != ""){
       var node_index=nodeNoData[tab].indexOf(node_for_edit);
       console.log(node_index);
       nodeNoData[tab].splice(node_index,1);
       delete nodeData[tab][node_for_edit];
       // deleteNodeFromCellObject(child_node);
       deleteNodeFromcells_nodes_object(node_for_edit);

     }

     //form logic
      var formData = $('#modal_form_id').serializeArray();

      if(validateMandatoryFields('modal_form_id')== true){
        var node_info=saveTopicInObject(formData);
        //apply data-dismiss
      }
      else{
        alert('please fill all the mandatory fields');
        // remove data dismiss
         return false;
      }
      console.log(node_info);


      // save in database
      var containerLearningTreeId=$('#hidden_learning_tree_id_id').val()
      formData.push({name: 'containerLearningTree', value:containerLearningTreeId});
      var url='/saveTopic';
      var topic_id=saveTopicInDatabase(url,fd);


      $('#current_list').parent().find('#node_topic').html('&nbsp;&nbsp'+node_info['info']['topic_name']);
      $('#current_list').parent().attr('title',node_info['info']['topic_name']);
      // adding value in hidden field
      console.log(node_info['node']);
      // $('#current_list').find(':input[type="hidden"]').val(node_info['node']);
      $('#current_list').find('.list_hidden_node_value').val(node_info['node']);
      $('#current_list').find('.list_hidden_node_value').attr('id',node_info['node']);

      // removing current id
      $('#current_list').removeAttr('id');

    }

});


  function saveTopicInDatabase(url,data){

     console.log('in saveTopicInDatabase function');
     console.log(url+" "+data);

     axios({
        method: 'post',
        url: url,
        data: data,

       }).then(function(response) {

        var data=response['data'];
        console.log(data);


      }).catch(function (error) {
      console.log("error"+error);
     });
  }


  function resetModal() {

      $('#modal_form_id').find(':input').val("");
      $('#modal_form_id').find(':input[type="checkbox"]').attr('checked',false);

      $('#document_path_option_id').empty();
      //changing id off
      $('#current_list').removeAttr('id');
  }
 //////////// modal ends

   function saveTopicInObject(formData){

          var tab=$('#hidden_tab_value').val();
          var node=$('#hidden_node_no').val();

          var limited_info =Object();
          $.each(formData, function (i, field) {

              limited_info[field.name]=field.value;
              console.log(field.name+" "+field.value);

           });
          console.log(limited_info);

          var info=Object();

          info['topic_name']=limited_info['topic_name'];
          info['entity_type']=limited_info['entity_type'];
          info['node_no']=limited_info['node_no'];
          info['parent_node']=limited_info['parent_node'];

          // limited_info['old_node_no']=info['old_node_no'];

          // node_info=limited_info;


   /// putting in nodeData and nodeNoData objects
          var nodeAny = nodeNoData[tab];
          var nodeAny2 = nodeData[tab];

          var any = [];
          var any2 = Object();

          if(typeof(nodeAny) != "undefined" && nodeAny !== null){
            nodeAny.push(node);
            nodeAny2[node]=info;

            nodeNoData[tab]=nodeAny;
            nodeData[tab]=nodeAny2;

          }else{
          any.push(node);
          any2[node]=info;

          nodeNoData[tab]=any;
          nodeData[tab]=any2;
          }
          console.log(nodeData);

          var node_info=[];
          node_info['node']=node;
          node_info['info']=info;

    /// put in cells_nodes_object
          var cells_id=$('#current_list').parent().parent().parent().attr('id');
          var cells_id_array=cells_id.split('d');

          var nodeAnyy = cells_nodes_object[tab][cells_id_array[1]];
          var anyy=[];

          if(typeof(nodeAnyy) != "undefined" && nodeAnyy !== null){

            nodeAnyy.push(node_info['node']);
            cells_nodes_object[tab][cells_id_array[1]]=nodeAnyy;
          }else{

            anyy.push(node_info['node']);
            cells_nodes_object[tab][cells_id_array[1]]=anyy;
         }

         //storing in localStorage
          // localStorage.setItem("nodeData", JSON.stringify(nodeData));
          // localStorage.setItem("nodeNoData", JSON.stringify(nodeNoData));
          // localStorage.setItem("cells_nodes_object", JSON.stringify(cells_nodes_object));

         // storing in strucure in data base
           storingObjectInDatabase(nodeData,nodeNoData,cells_nodes_object,'update');

       /// returning to main function
          return node_info;
    }

   function validateMandatoryFields(id){

         var valid = true;
         $("#"+id+" "+".mandatory").each(function () {
          if ($(this).val().trim() === '' || $(this).val() === '0')
          {
              $(".mandatory").removeClass('fillFields');
              $(this).addClass('fillFields');
              valid = false;
              return valid;
            }
         });

         console.log('in validateMandatoryFields'+valid);
          return valid;
    }


  $(document).on('dblclick','.table-cell',function() {

         //checking previous list has
         console.log($("#prime").find(".td_li:last").attr('class'));

         var j=0;
         if (!$(this).parent().parent().find(".td_li:last").attr("class")) {
             j=1;
             $('ul', this).append("<li class='td_li'><span id='temp' ondrop='drop(event)' ondragover='allowDrop(event)' style='height: 15px; width: 13px; background-color: gainsboro; display: inline-block'><input type='hidden' id='node_no' class='list_hidden_node_value'></span><span id='node_topic'></span></li>");
          }

         var i=0;
         if(j == 0){
         $(this).parent().parent().find(".td_li").each(function () {

            if (!$(this).attr('title')) {
               i++;
               $(this).remove();
            }
         });

         if(i==0){
           $('ul', this).append("<li class='td_li'><span id='temp' ondrop='drop(event)' ondragover='allowDrop(event)' style='height: 15px; width: 13px; background-color: gainsboro; display: inline-block'><input type='hidden' id='node_no' class='list_hidden_node_value'></span><span id='node_topic'></span></li>");
         }
        }
  });


////////on change parent node select
$('#parent_node').change(function(){

      var parentNode=$('#parent_node').val();
      var parent_node_length=parentNode.length;

    if(parentNode!=0 && parentNode!=""){

      ///checking node to be display by applying loop
      var tab=$('#hidden_tab_value').val();
      var i=0;
       $.each(nodeNoData[tab], function (j, node) {
            var node_length=node.length;

            if(node_length > parent_node_length && node_length < parent_node_length+3){
               sub_node=node.substring(0,parent_node_length);

               if(sub_node==parentNode){
                  i++;
               }
            }
       });

      var nodePoint=i+1;

      $('#node_title').html('Node '+parentNode+'.'+nodePoint);
      $('#hidden_node_no').val(parentNode+'.'+nodePoint);
    }
 else{
    //// calling function if parent node is zero
       var tab=$('#hidden_tab_value').val();
       onParentNodeZero(tab);
  }

});
////////// end of onchang parent node select



//   $(document).on('click','.td_li',function(event){
//     console.log('hii');

//     ///set id temperorily
//     $('#selected_list').removeAttr('id');
//     $(this).attr('id','selected_list');

//     $('.td_li').css('background-color','skyblue');
//     $(this).css('background-color','gray');

// });


  ////on clicking elsewhere
  $('html').click(function (e) {
     console.log(e.target.className);
    if (e.target.className == 'td_li') {
        //do something
    $('#selected_list').removeAttr('id');
    $(e.target).attr('id','selected_list');
    $('.td_li').css('background-color','skyblue');
    $(e.target).css('background-color','gray');

    } else {

       // event.target.className
       $('#selected_list').css('background-color','skyblue');
       $('#selected_list').removeAttr('id');
       // $('.td_li').css('background-color','skyblue');
       console.log('outside the list');
    }
});



////// deleting nodes and its child nodes
  $('#delete_btn').click(function(){

    // delete record from nodeData obj
       var tab=$('#hidden_tab_value').val();
       var containerLearningTree=$('#hidden_learning_tree_id_id').val();

    if (!$('#selected_list').attr('id')) {

        console.log(tab);

        // deleting tab and all its child nodes from database
        // deleting tab from structure table
        deleteChildNodesFromDatabase(tab,containerLearningTree);

        //operation  on tab
        $('#tab'+tab).remove();

        //removing div from table structure
        $('#tab_'+tab+'_div').remove();

        // removing from objects whole tabs from objects
        
        delete nodeData[tab];
        delete nodeNoData[tab];
        delete cells_nodes_object[tab];

        // storing structure in database -- don't store

    }else{

      if(typeof(nodeNoData[tab]) != "undefined" && tab !== null){
       var node=$('#selected_list').find('.list_hidden_node_value').val();
       var node_index=nodeNoData[tab].indexOf(node);

       console.log(node_index);
       if(node != "" && node_index != '-1'){

              //deleting all child nodes
       deleteChildNodes(tab,node);

       // delete from database
       // var containerLearningTree=20;
       var containerLearningTree=$('#hidden_learning_tree_id_id').val();
       deleteNodesFromDataBase(node,containerLearningTree);

       // deleteNodeFromCellObject(node);
       deleteNodeFromcells_nodes_object(node);



       //deleting node from nodeData obj
       nodeNoData[tab].splice(node_index,1);
       delete nodeData[tab][node];

       //changing obj on local storage
       // localStorage.setItem("nodeData", JSON.stringify(nodeData));
       // localStorage.setItem("nodeNoData", JSON.stringify(nodeNoData));
       // localStorage.setItem("cells_nodes_object", JSON.stringify(cells_nodes_object));
        // reconfigureOnSave();
       // storing structure in databasae
       storingObjectInDatabase(nodeData,nodeNoData,cells_nodes_object,'update');
       console.log(node+'node object record deleted');

       }
     }

    // removing list(node) from table
       $('#selected_list').remove();
   }

    });
  function deleteChildNodesFromDatabase(tab,containerLearningTree){

         //
        console.log('in deleteChildNodesFromDatabase function');
        console.log(tab+" "+containerLearningTree);
        var url="/deleteChildTopic/"+tab+"/"+containerLearningTree;
        // getTopicDocumentPath('/topicDocument/'+tag+'/'+artefact_type_id);

    axios.get(url)
        .then(function (response) {

             var data=response['data'];
             console.log(data);

             }).catch(function (error) {
             console.log("error"+error);
          });
  }



  function deleteChildNodes(tab,node){

          console.log('in deleteChildNodes function');
          console.log(nodeData[tab]);

          // deleting all the child nodes from database at once
          // deleteChildNodesFromDatabase(tab,containerLearningTree);

          $.each(nodeData[tab], function (node_key, node_info) {

             var child_node=node_key;
             console.log(child_node);
             console.log(node_info['parent_node']);

          if(node_info['parent_node']==node){

            // call the function again
            deleteChildNodes(tab,child_node);

            var node_index=nodeNoData[tab].indexOf(child_node);
            nodeNoData[tab].splice(node_index,1);
            delete nodeData[tab][child_node];

            //removing from cell objects
            // deleteNodeFromCellObject(child_node);
            deleteNodeFromcells_nodes_object(child_node);

            // delete from database
            // var containerLearningTree=20;
            var containerLearningTree=$('#hidden_learning_tree_id_id').val();
            deleteNodesFromDataBase(child_node,containerLearningTree);


            //remove from screen
            $("input[id='"+child_node+"']").parent().parent().remove();

          }
        });
          $.each(nodeNoData[tab], function (node_key, node_info) {
              console.log(node_key);
          });
     }

   function deleteNodeFromcells_nodes_object(node){

            var tab=$('#hidden_tab_value').val();
            var cell_id=$("input[id='"+node+"']").parent().parent().parent().parent().attr('id');
            var cell_id_array=cell_id.split('d');

            var lists_nodes_array=cells_nodes_object[tab][cell_id_array[1]];
            var indexOfNode=lists_nodes_array.indexOf(node);
            console.log(lists_nodes_array);
            cells_nodes_object[tab][cell_id_array[1]].splice(indexOfNode,1);
            console.log(node+'cells_nodes_object record deleted for the node');
   }

   function deleteNodesFromDataBase(node,containerLearningTree){

            //
            console.log('in deleteNodesFromDataBase function');
            console.log(node+" "+containerLearningTree);
            var url="/deleteTopic/"+node+"/"+containerLearningTree;
            // getTopicDocumentPath('/topicDocument/'+tag+'/'+artefact_type_id);

    axios.get(url)
        .then(function (response) {

             var data=response['data'];
             console.log(data);

             }).catch(function (error) {
             console.log("error"+error);
          });
   }


///////// delete ends


  $('#edit_btn').click(function(){

          var tab=$('#hidden_tab_value').val();
          var containerLearningTree=$('#hidden_learning_tree_id_id').val();

        if (!$('#selected_list').attr('id')) {

          //operation  on tab

          getTopicDataForEdit(tab,containerLearningTree,'it_is_a_tab');

        }

        else{
          var node=$('#selected_list').find('.list_hidden_node_value').val();

          console.log(tab+' '+node);
      /// showing data for update
          resetModal();
          $('#modal_heading').html('Edit Node');
          console.log($('#selected_list').attr('id'));


       // filling data in modal to show

          var icon_class = $('#selected_list').find('i').attr('class');
          console.log(icon_class);
          saveEntityName(icon_class);


          var cellId=$('#selected_list').parent().parent().attr('id');
          // set current_list id
          $('#selected_list').find('span').first().attr('id','current_list');
          getTopicDataForEdit(node,containerLearningTree,cellId);

          // return false;
        }
    });


   function getTopicDataForEdit(node,containerLearningTree,cellId){

            //
            console.log('in getTopicDataForEdit function');
            console.log(node+" "+containerLearningTree);
            var url="/getTopicForEdit/"+node+"/"+containerLearningTree;
            // getTopicDocumentPath('/topicDocument/'+tag+'/'+artefact_type_id);

            var data;

    axios.get(url)
        .then(function (response) {

             data=response['data'];
             console.log(data);
             // return data;

             if (cellId=='it_is_a_tab') {
               editTopicForTab(data,node);
             }
             else{
              editTopic(data,cellId,node);
             }

             }).catch(function (error) {
             console.log("error"+error);
          });
   }


   function editTopic(data,cellId,node){

          var tab=$('#hidden_tab_value').val();
          // var node=$('#selected_list').find('.list_hidden_node_value').val();

          var topic_info_array=data;
          console.log(topic_info_array);
          console.log($('#selected_list').attr('id'));

          $('#node_title').html('Node '+node);
          $('#hidden_node_no').val(node);

          $('#topic_name').val(topic_info_array['name']);
          $('#topic_description').val(topic_info_array['description']);
          $('#expected_duration').val(topic_info_array['expected_duration']);

          // if(node_info['parent_node'] !="0" ){
             appendParentNode('abc',cellId,topic_info_array['parent_node']);
          // }

          if(topic_info_array['critical'] == 1){
             $('#critical_topic').prop('checked',true);
             $('#alert_before_nodes').val(topic_info_array['alert_before_nodes'])
          }

          // getting JSON
          var details=JSON.parse(topic_info_array['details']);
          console.log(details);

          // ///for document
          if(typeof(details['entity_url']) != "undefined" && details['entity_url'] !== null){
          // if(details['entity_url']!=""){
          $('#hidden_document_name_id').val(details['entity_name']);
          $('#hidden_document_url_id').val(details['entity_url']);

          console.log(details['entity_id']);
          $("#document_path_option_id").append("<tr><td width=10% ><input type=checkbox class='document_checkbox_class' name='entity_id' value="+details['entity_id']+" checked></td><td width=90%>"+details['entity_name']+"</td></tr>");

          }


       /// setting old node in hidden old node no
          $('#hidden_old_node_no').val(node);

          $("#myModal").modal({
          backdrop: 'static',
          keyboard: false
          });


      // // set current_list id
      //    $('#selected_list').find('span').first().attr('id','current_list');
   }

   function editTopicForTab(data,node){

          var topic_info_array=data;
          var tab=node;
          console.log(topic_info_array);

          resetModal();
          $('#modal_heading').html('Edit Node');
      
          // no entity type
          $('#hidden_entity_type_id').val('MajorNode');

          // hiding document part
          $('#document_section').hide();
          $('#icon_div').show();
          $('#node_title').html('Node '+tab);
          $('#hidden_node_no').val(tab);

          // parent node
          $('#parent_node_div').hide();

          $('#topic_name').val(topic_info_array['name']);
          $('#topic_description').val(topic_info_array['description']);
          $('#expected_duration').val(topic_info_array['expected_duration']);

          if(topic_info_array['critical'] == 1){
             $('#critical_topic').prop('checked',true);
             $('#alert_before_nodes').val(topic_info_array['alert_before_nodes'])
          }

       /// setting old node in hidden old node no
          $('#hidden_old_node_no').val(node);

          $("#myModal").modal({
          backdrop: 'static',
          keyboard: false
          });

   }




//////// for document path \\\\\\\\
  $(document).on('click','#document_button_id',function(){
    //validation

    console.log(artefact_type_id+'checkit');
    var tag=document.getElementById('document_input_id').value;
    console.log(tag);
    if(tag){
    getTopicDocumentPath('/topicDocument/'+tag+'/'+artefact_type_id);
    }

});

   var documents=new Object();

function getTopicDocumentPath(url){
    axios.get(url)
        .then(function (response) {

        console.log(response);
        console.log('after printing response getTopicDocumentPath');

        $("#document_path_option_id").empty();
        if(Object.keys(response).length > 0){


           $.each(response['data'], function (key, value) {
           console.log(value.document_path);
           $("#document_path_option_id").append("<tr><td width=10% ><input type=checkbox class='document_checkbox_class' name='entity_id' value="+value.id+"></td><td width=90%>"+value.name+"</td></tr>")

           documents[value.id]=[value.name,value.document_path];
           });
        }
    })
        .catch(function (error) {
        console.log("error"+error);
   });
  }


   $(document).on('change','input.document_checkbox_class',function(){
        // if(this.checked == true){
        var id=$(this).val();
        // $('#hidden_document_name_id').val=documents[id][0];
        document.getElementById('hidden_document_name_id').value=documents[id][0];
        console.log(documents[id][0]);
        document.getElementById('hidden_document_url_id').value=documents[id][1];


         $('input.document_checkbox_class').not(this).prop('checked', false);
      });

//////// document path ends




/////// appending parent node starts
function appendParentNode(ev,cellId,parent_node){

    var tab=$('#hidden_tab_value').val();
    var tab_div_id='tab_'+tab+'_div';
    ///creating data of node not to parent(of same cell)
    var temp_array=[];

    if(cellId =='abc'){

        var cell_id=$('#'+ev.target.id).parent().parent().parent().attr('id');
    }
    else{
        var cell_id=cellId;
        // var cell_id=$('#selected_list').parent().parent().attr('id');
        console.log(temp_array);
    }

    ///for forward cells
      // var row_id=$('#'+cell_id).parent().attr('id');
      var cell_id_array=cell_id.split('d');
      // var row_id_array=row_id.split('r');

      // var row_no=row_id_array[1];
      var current_cell_no=cell_id_array[1];
      console.log(current_cell_no);

     //// for cells_nodes_object
     $.each(cells_nodes_object[tab], function (cell_no, nodes) {

            console.log(cell_no+" "+current_cell_no);
            if (Number(cell_no) >= Number(current_cell_no)) {

                console.log(cell_no+" "+current_cell_no);
                console.log(cell_no >= current_cell_no)
                var current_cell_id='td'+cell_no;
                console.log(current_cell_id);
                $('#'+tab_div_id).find('#'+current_cell_id).find('.list_hidden_node_value').each(function () {
                   temp_array.push($(this).val());
                });
            }
     });


      console.log(temp_array);

    ///appending parent node
    $('#parent_node').empty();
    // $('#parent_node').append('<option value="0">--select--</option>');
    $('#parent_node').append('<option value='+tab+'>'+tab+'</option>');

    $.each(nodeNoData[tab], function (j, node) {

       var i=0;
       $.each(temp_array, function (k, nodeNo) {
         if (node==nodeNo) {
            i++;
          }
       });

       console.log(i);
       if(i==0){
         if (node==parent_node) {
            $('#parent_node').append('<option value='+node+' selected>'+node+'</option>');
         }
         else{
         $('#parent_node').append('<option value='+node+'>'+node+'</option>');
         }
       }
    });
  }
  //////// appending parent node ends \\\\\\\\



  function saveEntityName(icon_class){

       console.log(icon_class);
       $.each(entityNameObject, function (clas, artefact_type) {
            console.log(clas+" "+artefact_type);
             if(icon_class==clas){

                console.log(artefact_type);
                artefact_type_id=artefact_type;
                $('#hidden_entity_type_id').val(artefact_type);
             }
           });
       /// changing modal as per the artefact type
       if (artefact_type_id == 'MajorNode') {
           $('#document_section').hide();
            $('#icon_div').hide();
       }
       else{
           $('#document_section').show();
           $('#icon_div').show();
       }
  }

  function onParentNodeZero(tab){

    if(typeof(nodeNoData[tab]) != "undefined" && nodeNoData[tab] !== null){
      var i=1;
      $.each(nodeNoData[tab], function (j, node) {
             console.log(i);
             if(node.length < 4){
               i++;
             }
           });
      console.log(i);
      $('#node_title').html('Node '+tab+'.'+i);
      $('#hidden_node_no').val(tab+'.'+i);
    }

    else{
      console.log('before showing node value'+tab);
      $('#node_title').html('Node '+tab+'.1');
      $('#hidden_node_no').val(tab+'.1');
    }
  }


// edit page
// function getObjectsFromDataBase(){
//      var nodeData
//      var
//      storeOnLocalStorage()
//      createTable()
// }



////// for creating table structure
function createTables(cells_nodes_object){

         // creating whole structure
         // $('.tab_divs').empty();
         $.each(cells_nodes_object, function (tab, tab_table_structure) {

            createTabWiseStructure(tab,tab_table_structure);
         });

         $('td').attr('contextmenu','mymenu');
  }

  function createTabWiseStructure(tab,cells_nodes_object_tab){

         //
         var tab_div_id="tab_"+tab+"_div";
         console.log(tab_div_id);

         /// completing the half filled  row with the cells(td) or clipping extra row
         var remainder=Object.keys(cells_nodes_object_tab).length % 5;
         var dividend =Math.floor(Object.keys(cells_nodes_object_tab).length/5);
         console.log(remainder+" "+dividend);
         /// cmpleting row
         if (remainder !=0) {

             // clipping if extra
            var td_id=(dividend*5)+1;
            console.log(td_id);
            if(!$('#'+tab_div_id).find('#td'+td_id).find('li:first').attr('title')){
                //
                var i;
                for(i=(dividend*5)+1; i<= (dividend*5)+remainder; i++){
                    //deleting row's cells
                    delete cells_nodes_object_tab[i];
             }

            }
            else{
             var i;
             for(i=dividend*5+(remainder+1); i<= (dividend+1)*5; i++){
                 console.log(i);
                 cells_nodes_object_tab[i]=[];
             }
            }
         }
         console.log(cells_nodes_object_tab);

         /// start
         var i=0;
         var j=1;
         // append at first row
         var row_id = appendRow(j,tab_div_id);
         console.log(row_id);

       $.each(cells_nodes_object_tab, function (cell_no, nodes) {

          i++;
          if (cell_no%5 == 1 && cell_no > 5) {
           j++;
           // append at row level
           row_id = appendRow(j,tab_div_id);
           console.log(cell_no+" "+i+" "+j);
          }
          console.log(cell_no+" "+i+" "+j);

          // append at cell level
          console.log(row_id);
          var cell_id = appendCell(cell_no,row_id,tab_div_id);
          appendsLists(cell_no,tab,cells_nodes_object_tab,tab_div_id);
        });
  }


  function appendRow(row_value,tab_div_id){

       var id='tr'+row_value;
       $('#'+tab_div_id).find('#table_body').append("<tr height='80px' id="+id+"></tr>");
       // $('#table_body').append("abhi");
       console.log(id);
       return id;
  }

  function appendCell(cell_value,row_id,tab_div_id){

       var id='td'+cell_value;
       $('#'+tab_div_id).find('#'+row_id).append("<td style='width: 20%;' class='table-cell' id="+id+"><ul style='list-style-type: none;' ></ul></td>")
       console.log(id);
       return id;
  }


  function appendsLists(cell_no,tab,cell_object,tab_div_id){

      // appending UL
       var cell_id='td'+cell_no;

      $.each(cell_object[cell_no], function (cell_no_key, cell_node) {
        console.log(cell_node+' '+tab);

       var node_info=nodeData[tab][cell_node];
       console.log(node_info);

       // getting icon class
       var icon_class;
       $.each(entityNameObject, function (clas, artefact_type) {

             if(artefact_type==node_info['entity_type']){
                console.log(node_info['entity_type']);
                icon_class=clas;
             }
           });

       //showing on screen table
       console.log(cell_id+' '+node_info['topic_name']+' '+icon_class);
       $('#'+tab_div_id).find('#'+cell_id+' ul').append("<li class='td_li' title="+node_info['topic_name']+"><span style='height: 15px; width: 13px; background-color: gainsboro; display:inline-block;' ondragover='allowDrop(event)' ondrop='drop(event)'><input id="+node_info['node_no']+" class='list_hidden_node_value' type='hidden' value="+node_info['node_no']+"><i id='drag1' class="+icon_class+" ondragstart='drag(event)' draggable='true'></i></span><span id='node_topic'>  "+node_info['topic_name']+"</span></li>");

       });
       $(".loader").fadeOut('slow');
  }
/////////////// table structure ends \\\\\\\\\\\


/////////////// Append Cells Started \\\\\\\\\\\\
  $(document).on('contextmenu','td',function() {
       right_clicked_td=$(this).attr('id');
       console.log($(this).attr('id'));
  });

  $('#right').click(function(){

        var tab=$('#hidden_tab_value').val();
        var tab_div_id='tab_'+tab+'_div';

     /// validations
        right_clicked_td_array=right_clicked_td.split('d');
        var next_td_id='td'+(parseInt(right_clicked_td_array[1])+1);
        console.log(next_td_id);
        // check under that tab
        // $()
        if(!$('#'+tab_div_id).find('#'+next_td_id).find('li:first').attr('title')){
             alert('please select right cell');
             return false;
        }

        var no_of_cells = prompt("Enter the no of cells less than 5:", 1);
        console.log(no_of_cells);

        if (no_of_cells == null || no_of_cells == "") {
         alert('please provide valid input');
         return false;
        }
        else if(isNaN(no_of_cells)){
         alert('please provide valid input');
         return false;
        }
        else if(no_of_cells>4){
          alert('please enter no. of cells less than 5');
          return false;
        }

        var selected_cell_id_array=right_clicked_td.split('d');
        var idd=parseInt(selected_cell_id_array[1]);
        appendCells(idd,no_of_cells);
  });

    $('#left').click(function(){

        var tab=$('#hidden_tab_value').val();
        var tab_div_id='tab_'+tab+'_div';

     /// side validations
        right_clicked_td_array=right_clicked_td.split('d');
        var next_td_id='td'+(parseInt(right_clicked_td_array[1])-1);
        console.log(next_td_id);

        if(!$('#'+tab_div_id).find('#'+next_td_id).find('li:first').attr('title')){
             alert('please select right cell');
             return false;
        }


       // validations for starting nodes
       right_clicked_td_array=right_clicked_td.split('d');
       if (right_clicked_td_array[1]==1) {
             alert('please select right cell');
             return false;
       }


        var no_of_cells_left = prompt("Enter the no of cells less than 5:", 1);
        console.log(no_of_cells_left);

        if (no_of_cells_left == null || no_of_cells_left == "") {
         alert('please provide valid input');
         return false;
        }
        else if(isNaN(no_of_cells_left)){
         alert('please provide valid input');
         return false;
        }
        else if(no_of_cells_left>4){
          alert('please enter no. of cells less than 5');
          return false;
        }

        //extra cell validation
        if(no_of_cells_left >= right_clicked_td_array[1]){
             alert('please select right no of cells');
             return false;
        }

        var selected_cell_id_array=right_clicked_td.split('d');
        var idd=parseInt(selected_cell_id_array[1]);
        var iddd=idd-(parseInt(no_of_cells_left) +1);
        console.log(no_of_cells_left+" "+idd+" "+iddd);
        appendCells(iddd,no_of_cells_left);
  });

  function appendCells(idd, no_of_cells){

        var tab=$('#hidden_tab_value').val();
        var tab_div_id='tab_'+tab+'_div';

        // cells_nodes_object=JSON.parse(localStorage.getItem('cells_nodes_object'));
        // var new_object_full=JSON.parse(localStorage.getItem('cells_nodes_object'));
        // var new_object=new_object_full[tab]

        var new_object=cells_nodes_object[tab];

        /// deleting selcted cell's right cell
        var i;
        var k=no_of_cells;
        for (i = 1; i <= k; i++) {
             new_object[idd+i]=[];
        }

        console.log(new_object);
        console.log(cells_nodes_object[tab]);
        var cells_nodes_object_tab=cells_nodes_object[tab];

        $.each(cells_nodes_object_tab, function (cell_no, nodes) {

            console.log(cell_no);
            if (cell_no > idd) {

               console.log(cells_nodes_object_tab[cell_no]);
               if (cells_nodes_object_tab[cell_no] != '') {
               new_object[parseInt(cell_no)+parseInt(k)]=cells_nodes_object_tab[cell_no];
               }
               // no need later
               else if(cells_nodes_object_tab[parseInt(cell_no)+parseInt(k)] != ''){
               new_object[parseInt(cell_no)+parseInt(k)]=[];
               }
            }

        });

        //storing on local storage
        cells_nodes_object[tab]=new_object;
        console.log(new_object);
        // localStorage.setItem("cells_nodes_object", JSON.stringify(cells_nodes_object));


        // deleting table
         $('#'+tab_div_id).find('tr').remove();
         // createTables();
         createTabWiseStructure(tab,new_object);
  }
  ///////////// Append Cells Finished \\\\\\\\\\\\




  $('#append_row').click(function(){

         var tab=$('#hidden_tab_value').val();
         var tab_div_id='tab_'+tab+'_div';

         // validation
         var last_row_no=Math.floor(Object.keys(cells_nodes_object[tab]).length/5);
         var l=0;
         console.log(last_row_no);
         $('#'+tab_div_id).find("#tr"+last_row_no).find('.td_li').each(function () {
          console.log(l);
              if ($(this).attr('title')) {
                  l++;
              }
         });

         if (l==0) {
             alert('cannot append row unless last row is filled');
             return false;
         }



         /// completing row with the cells
         var remainder=Object.keys(cells_nodes_object[tab]).length % 5;
         var quotient =Math.floor(Object.keys(cells_nodes_object[tab]).length/5);
         console.log(remainder+" "+quotient);
         if (remainder ==0) {

             var i;
             for(i=(quotient*5)+1; i<= (quotient+1)*5; i++){
                 console.log(i);
                 cells_nodes_object[tab][i]=[];
             }
         }

        //storing on local storage
        console.log(cells_nodes_object);
        // localStorage.setItem("cells_nodes_object", JSON.stringify(cells_nodes_object));

         // deleting table
         $('#'+tab_div_id).find('tr').remove();
         // createTables();
         createTabWiseStructure(tab,cells_nodes_object[tab]);
  });


  /// on save reconfigure all the objects
  function reconfigureOnSave(){

           // reconfigure cell

           // reconfigure row

           // on adding

           // on deleting

           ////
        var tab=$('#hidden_tab_value').val();
        var tab_div_id='tab_'+tab+'_div';

        // cells_nodes_object=JSON.parse(localStorage.getItem('cells_nodes_object'));
        var new_object=new Object();

        var i=0;
        $.each(cells_nodes_object[tab], function (cell_no, nodes) {

            console.log(cell_no);
            if (nodes != "") {

               if (cell_no < 11) {
               new_object[cell_no]=[];
               }

               i++;
               new_object[i]=nodes;
               console.log(new_object);
            }
            else if(nodes == "" && cell_no < 11){
              new_object[cell_no]=cells_nodes_object[tab][cell_no];
            }
        });
        // managing rows



        cells_nodes_object[tab]=new_object;
        console.log(cells_nodes_object);
        // localStorage.setItem("cells_nodes_object", JSON.stringify(cells_nodes_object));

        // deleting table
        $('#'+tab_div_id).find('tr').remove();
        // createTables();
        createTabWiseStructure(tab,new_object);
  }


  function appendTabs(){

         //
         var tab_id;
         var i;
         $('#plus').remove();
         $('.tab').empty();
         $('#prime').empty();
         $.each(cells_nodes_object, function (tab, tab_table_structure) {

       console.log(tab);
       // if (tab != 1) {
       //appending tab
       i=tab;
       tab_id="tab"+tab;
       $('.tab').append("<button class='tab_button' id="+tab_id+" onclick=tabEvent('"+tab_id+"')>"+i+"</button>");


       // appending new tab div
       $('#prime').append("<div id='tab_"+i+"_div' class='tab_divs'><table class='table table-bordered' style='border-color: blue;'><tbody id='table_body'></tbody></table></div>");
       // }
         });

         $('.tab').append("<button id='plus' style='width: 30px;' onclick='increaseButton()'>+</button>");

  }

  function storingObjectInDatabase(nodeData,nodeNoData,cellObject,flag){

        console.log('in storingObjectInDatabase function');
        var tab=$('#hidden_tab_value').val();

        //make complete object
        var detail_object=new Object();
        detail_object['nodeData']=nodeData[tab];
        detail_object['nodeNoData']=nodeNoData[tab];
        detail_object['cellObject']=cellObject[tab];

        // making json
        var details_json= JSON.stringify(detail_object);

        var details=new Object();
        details['details']=details_json;

        details['tab']=tab;
        details['containerLearningTree']=$('#hidden_learning_tree_id_id').val();

        if(flag == 'update'){
           details['update_flag']='update';
        }
        else if(flag == 'save'){
           details['update_flag']='save';
        }

        console.log(detail_object);
        console.log(details);

        var url='/saveStructure';

      axios({
        method: 'post',
        url: url,
        data: details,

       }).then(function(response) {

        var data=response['data'];
        console.log(data);


      }).catch(function (error) {
      console.log("error"+error);
     });

  }


  function getStructureObjectsFromDataBase(containerLearningTree){
          $(".loader").fadeIn();
         var url="/getLtStructureObjects/"+containerLearningTree;
         //
         axios.get(url)
        .then(function (response) {

             data=response['data'];
             console.log(Object.keys(data).length);
             console.log(typeof(data));
             // return data;
             if(Object.keys(data).length>0){
              createStructureObjects(data);
            }else{
              $(".loader").fadeOut('slow');
            }
             

             }).catch(function (error) {
              $(".loader").fadeOut('slow');
             console.log("error"+error);
          });
  }

  function createStructureObjects(data){

         cells_nodes_object = {};
         nodeData = {};
         nodeNoData = {};
        $.each(data, function (cell_no, tab_object) {

            var details=JSON.parse(tab_object['details']);
            console.log(details);
            // creating objects
            cells_nodes_object[tab_object['tab']]=details['cellObject'];

            nodeData[tab_object['tab']]=details['nodeData'];

            nodeNoData[tab_object['tab']]=details['nodeNoData'];
        });

        appendTabs();
        tabEvent('tab1');

        createTables(cells_nodes_object);


  }
