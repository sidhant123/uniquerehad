<?php

namespace vnnogile\Utilities\Services;

//Custom Imports Below
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * This is a a common utility method for file upload
 *
 * All file upload controllers can use this trait to
 * upload files to the server
 */
class FileUploadUtility
{
    /**
     *
     * @param Request $request
     * @param string $folder
     *
     * @return array $uploadedFiles
     */
    public static function uploadFileToPublicStorage(Request $request, $folder){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $data=$request->all();
        Log::debug(print_r($data));

        $uploadedFiles = self::uploadFiles($request, $folder, 'public');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $uploadedFiles;
    }

    public static function uploadFileToPrivateStorage(Request $request, $folder){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Log::debug(');
        $uploadedFiles = self::uploadFiles($request, $folder, 'private');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $uploadedFiles;
    }

    private static function uploadFiles(Request $request, $folder, $mode){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	Log::error("*******IN FILE UPLOAD***********");	
        $allFiles  = $request->allFiles();
        $d=$request->all();
        $uploadedFiles = array();
        $original_file_names = array();

        foreach ($allFiles as $key => $value) {
	Log::debug("*******IN FILE UPLOAD***********");	
	Log::debug("FOLDER:".$folder);
            Log::debug(print_r($key, true));
            if (is_array($request->$key)){
                foreach ($request->$key as $file){
                    // finding temp name
                    $originalFileName = $file->getClientOriginalName();
                    $file_name_array = explode('.', $originalFileName);
                    $file_extension = array_pop($file_name_array);
                    $tempFileName = uniqid().".".$file_extension;

                    if($mode == 'private'){
	Log::debug("*******PRIVATE***********");
                        $uploadedFlag = $file->storeAs($folder, $tempFileName);
                    }else {
                        $uploadedFlag = $file->storeAs('public/' . $folder, $tempFileName);
                    }
                    Log::debug('Uploading ' . $originalFileName);
                    if ($uploadedFlag){
                        $uploadedFiles[$key][] = $tempFileName;
                        $original_file_names[$key][] = $originalFileName;
                        Log::debug('Uploaded ' . $originalFileName);
                    }
                }
            }else{
                $file = $request->$key;
                $originalFileName = $file->getClientOriginalName();
                $file_name_array = explode('.', $originalFileName);
                $file_extension = array_pop($file_name_array);
                $tempFileName = uniqid().".".$file_extension;
Log::debug("*******IN FILE UPLOAD2***********");	
                if($mode == 'private'){
                    $uploadedFlag = $file->storeAs($folder, $tempFileName);
                }else {
                    $uploadedFlag = $file->storeAs('public/' . $folder, $tempFileName);
                }
                Log::debug('Uploading ' . $originalFileName);
                if ($uploadedFlag){
                    $uploadedFiles[$key][] = $tempFileName;
                    $original_file_names[$key][] = $originalFileName;
                    Log::debug('Uploaded ' . $originalFileName);
                }
            }
        }
        //$uploadInfo=['first'=>$uploadedFiles,'sec'=>$original_file_names];
        $uploadInfo['uploadedFileNames'] = $uploadedFiles;
        $uploadInfo['originalFileNames'] = $original_file_names;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $uploadInfo;
    }

}
