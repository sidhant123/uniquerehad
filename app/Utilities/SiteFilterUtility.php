<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class SiteFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array())
    {
        // filter method for getting filtered data
        try {
            $data = DB::table('sites')
                ->select(
                    'sites.name as name',
                    'sites.address as address',
                    'sites.location as location',
                     'sites.id as id')->where('sites.deleted_at', null);
                                
            if ($request->has('search')) {
                $data->whereRaw("(lower(sites.name) like '%" . strtolower($request['search']) . "%')");
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'name':
                        $data->orderBy('sites.name', $request['sort']);
                        break;
                    case 'address':
                        $data->orderBy('sites.address', $request['sort']);
                        break;
                                     
                }
            } else {
                $data->orderBy('sites.id');
            }
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
             //dd($data->get());
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e->getMessage());
        }
    }
}
