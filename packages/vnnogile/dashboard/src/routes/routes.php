<?php


//Route::group(['middleware' => 'auth:api'], function() {
Route::group(['middleware' => ["web", "auth"]], function () {
	Route::get('/quizPercentByUser', 'vnnogile\Dashboard\Controllers\API\DashboardController@getOverallQuizPerformanceForUser');
	Route::get('/quizPercentByUserInProgram', 'vnnogile\Dashboard\Controllers\API\DashboardController@getQuizPerformanceForUserInProgram');
});
