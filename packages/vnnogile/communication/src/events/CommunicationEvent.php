<?php

namespace vnnogile\Communication\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CommunicationEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $eventName;
    public $data;
    public $authenticatedUserId;
    public $tenantPartyId;
    public $log;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($eventName, $data, $authenticatedUserId, $tenantPartyId, $log=True)
    {
        $this->eventName = $eventName;
        $this->data = $data;
        $this->authenticatedUserId = $authenticatedUserId;
        $this->tenantPartyId = $tenantPartyId;
        $this->log = $log;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
