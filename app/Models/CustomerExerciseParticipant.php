<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Mar 2018 17:04:44 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
/**
 * Class CustomerExerciseParticipant
 * 
 * @property int $id
 * @property int $participant_user_id
 * @property string $exercise_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class CustomerExerciseParticipant extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'participant_user_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'participant_user_id',
		'exercise_code',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function users(){
	    return $this->belongsTo(\App\User::class, 'participant_user_id');
	}

}
