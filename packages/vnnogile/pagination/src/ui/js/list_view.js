$(document).ready(function() {
	$.getJSON('/getRowCount', new Date(), populateRowCount).error(errorResponse);
});

function setRowCount(value,routeName){
	console.log(routeName);
	var url = '/'+routeName+'/?rowCount='+value;
	window.location = url;
	//localStorage.setItem('rowCount', value);
  sessionStorage.setItem('rowCount', value);
}


function populateRowCount(data){
	$("#row_count").empty();

	$.each(data['row_count_list'], function (key, value) {

		$("#row_count").append("<option value='" + value + "'>" + value+ "</option>");

	}); 
	// if(localStorage.getItem('rowCount')){
	// 	console.log("localStorage");
	// 	$("#row_count").val(localStorage.getItem('rowCount'));	
	// }else{
	// 	$("#row_count").val(data['default_row_count']);
	// }  
  if(sessionStorage.getItem('rowCount')){
   console.log("sessionStorage");
   $("#row_count").val(sessionStorage.getItem('rowCount'));  
  }else{
   $("#row_count").val(data['default_row_count']);
  }  
}

function deleteRecord(id,route){

  // bootbox.alert("do you want to delete",function(){ });
  var url = '/'+ route +'/'+ id;
  $.ajaxSetup({
  	headers: {
  		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  	}
  });	
  $.ajax({
  	type: "delete",
  	url: url,

  	success: success,
  });
  function success(data){
  	var data = data.split("/");
  	if(data[0] == 'deleted successfully'){
  		window.location = '/'+data[1];
  	}
  }    	
}

function errorResponse(errorResponse){
	console.log(errorResponse);
}