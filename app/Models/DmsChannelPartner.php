<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 02 Feb 2018 16:11:12 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;


//Custom Imports
use App\Models\BaseModel;

/**
 * Class DmsChannelPartner
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class DmsChannelPartner extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
