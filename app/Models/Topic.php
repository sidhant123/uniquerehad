<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:01 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Topic
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $available_from
 * @property \Carbon\Carbon $available_to
 * @property string $sequence
 * @property string $direction
 * @property float $expected_duration
 * @property int $parent_topic_id
 * @property int $container_learning_tree_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \App\Models\LearningTree $learning_tree
 * @property \App\Models\Topic $topic
 * @property \Illuminate\Database\Eloquent\Collection $topics
 *
 * @package App\Models
 */
class Topic extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'expected_duration' => 'float',
		'parent_topic_id' => 'int',
		'container_learning_tree_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'available_from',
		'available_to'
	];

	protected $fillable = [
		'name',
		'description',
		'available_from',
		'available_to',
		'sequence',
		'color',
		'expected_duration',
		'parent_topic_id',
		'details',
		'container_learning_tree_id',
		'created_by',
		'updated_by',
		'deleted_by',
		'icon'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function learning_tree()
	{
		return $this->belongsTo(\App\Models\LearningTree::class, 'container_learning_tree_id');
	}

	public function topic()
	{
		return $this->belongsTo(\App\Models\Topic::class, 'parent_topic_id');
	}

	public function topics()
	{
		return $this->hasMany(\App\Models\Topic::class, 'parent_topic_id');
	}
}
