<?php

namespace vnnogile\Geography\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class GeographyService
 * 
 */
class GeographyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'vnnogile');
        $this->publishes([
            __DIR__.'/../vendors' => public_path('vendors/vnnogile'),
        ], 'public');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
        $this->app->make('vnnogile\Geography\controllers\StateController');
        $this->app->make('vnnogile\Geography\controllers\CityController');
        $this->app->make('vnnogile\Geography\controllers\PincodeController');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../services/StateService.php';
        include __DIR__.'/../services/CityService.php';
        include __DIR__.'/../services/PincodeService.php';

    }
}
