/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var parentNode;
var range;
var selection;

$(document).on('change', '#place_holder_id', function() {
  var text = $('#place_holder_id :selected').text();
  addPlaceHolders(text);
});
$(document).on('change', '#communication_event_id', function() {
  var id = $(this).val();
  if (id > 0) {
    populateTemplatePlaceHolders('/placeholders', id);
  }
});
$(document).on('click', '#copy_template', function() {
  $('.errorMessage').html('');
  var data = $('#templates_id').select2('data');
  if (data[0].id == 0) {
    $('.errorMessage').html('Please select a template.');
    return false;
  }
  //window.confirm("Do you want to copy the template.");
  bootbox.confirm({
    message: "Do you want to copy the template?",
    buttons: {
      confirm: {
        label: 'Yes',
        className: 'btn-success'
      },
      cancel: {
        label: 'No',
        className: 'btn-danger'
      }
    },
    callback: function(result) {
      if (result) {

        $('#template_event_type_id').val("copyTemplate");
        if (data[0].id > 0) {
          getDataThroughAxios('/getTemplate/' + data[0].id);

        }

      }
      console.log("else" + result);
    }
  });

});
$(document).on('click', '#attach_template', function() {
  $('.errorMessage').html('');
  var comm_event = $('#communication_event_id').select2('data');
  if (comm_event[0].id == 0) {
    $('.errorMessage').html('Please select a Communication Event.');
    return false;
  }
  var data = $('#templates_id').select2('data');
  if (data[0].id == 0) {
    $('.errorMessage').html('Please select a template.');
    return false;
  }
  bootbox.confirm({
    message: "Do you want to attach this template.?",
    buttons: {
      confirm: {
        label: 'Yes',
        className: 'btn-success'
      },
      cancel: {
        label: 'No',
        className: 'btn-danger'
      }
    },
    callback: function(result) {
      if (result) {
        getAttachedCommEvents(data[0].id, data[0].text);
      }
      console.log("else" + result);
    }
  });
});
$(document).on('click', '#detach_template', function() {
  $('.errorMessage').html('');
  var comm_event = $('#communication_event_id').select2('data');
  if (comm_event[0].id == 0) {
    $('.errorMessage').html('Please select a Communication Event.');
    return false;
  }
  var confirm = window.confirm("Do you want to detach the template.");
  if (confirm == true) {
    $('#template_event_type_id').val("detachTemplate");
  }
});
$(document).on('keyup mouseup', '.note-editable', function(e) {
  // $('.note-editable').on('keyup mouseup',function (e){
  console.log('in note-editable');
  selection = window.getSelection();
  range = selection.getRangeAt(0);
  parentNode = range.commonAncestorContainer.parentNode;
});

$(document).on('keyup mouseup', '#template_subject_id', function(e) {
  // $('.note-editable').on('keyup mouseup',function (e){
  console.log('in subject');
  selection = window.getSelection();
  range = selection.getRangeAt(0);
  parentNode = range.commonAncestorContainer.parentNode;
});

var postForm = function() {
  var content = $('.note-editable').html();
}
$(document).on('click', '#clearBtn', function() {
  clearForm();
});
$(document).on('click', '#saveBtn', function() {
  var id = $('#summernote_id').val();
  var template_id = $('#template_id').val();
  var content = $('.note-editable').html();
  var img_src = getImgSrc();
  var img_name = getImgName();
  var objData = {};
  objData['html_data'] = content;
  objData['img_src'] = img_src;
  objData['img_name'] = img_name;
  var stringifyData = JSON.stringify(objData);
  var formData = $('#form').serialize();
  var data = "data=" + encodeURIComponent(stringifyData) + '&' + formData
  console.log(data);
  console.log(validateMandatoryFields());
  if (validateMandatoryFields() == true) {
    saveHtmlText(data, template_id);
  }

});
function showDesiredTextarea(event){
  var communication_channel = $("#"+$(event).attr('id')+" option:selected").text();
  console.log(communication_channel);
  if(communication_channel == 'SMS' || communication_channel == 'APP'){
    $('#summernote_div').css('display','none');
    //$('#summernote').removeAttr('name');
    $('#summernote').removeClass('mandatory');
    $('#textarea_div').css('display','block');
    $('#textarea').attr({
      name: 'summernote'
    });
    $('#textarea').addClass('mandatory');
  }else{
    $('#summernote_div').css('display','block');
    $('#summernote').addClass('mandatory');
    // $('#summernote').attr({
    //   name: 'summernote'
    // });
    $('#textarea_div').css('display','none');
    $('#textarea').removeAttr('name');
    $('#textarea').removeClass('mandatory');
  }
}
function getImgName() {
  var img_name = getAttributeValue('data-filename');
  return img_name;
}

function getImgSrc() {
  var img_src = getAttributeValue('src');
  return img_src;
}

function getAttributeValue(attr_name) {
  var img_attr = document.getElementsByTagName('img');
  var attr_array = [];
  var count = img_attr.length;
  if (count > 0) {
    for (var i = 0; i < count; i++) {
      attr_array.push(img_attr[i].getAttribute(attr_name));
    }
  }
  return attr_array;
}

function saveHtmlText(data, template_id) {
  $('.alert-danger').addClass('hide');
  $('.alert-danger').children().find('li').remove();
  var url = '/templates';
  var type = 'post';
  if (template_id) {
    url = '/templates/' + template_id;
    type = 'put';
  }
  $.ajax({
    type: type,
    url: url,
    data: data,
    success: success,
    error: errorStatus,
  });
}

function success(data) {
  if (data == 'save successfully') {
    window.location = '/templates';
  }

}

function errorStatus(data){
  $('.alert-danger').removeClass('hide');
  var scrollPos =  $(".alert").offset().top;
  console.log(data.responseJSON.errors);
  $.each(data.responseJSON.errors, function(index, val) {
     /* iterate through array or object */
     $('.alert-danger').children().append('<li>'+val+'</li>');
     
  });
    $(window).scrollTop(scrollPos);
}

function getDataThroughAxios(url) {
  axios.get(url)
    .then(function(response) {
      console.log(response);
      getTemplateData(response['data']);
    })
    .catch(function(error) {
      console.log("error" + error);
    });
}

function getAttachedCommEvents(template_id, template) {
  axios.get('/getAttachedCommEvents/' + template_id)
    .then(function(response) {
      console.log(response);
      var attachedCommunicationEvents = response['data'];
      if (Object.keys(response['data']).length > 0) {
        console.log(attachedCommunicationEvents);
        bootbox.confirm({
          message: template + " is already associated with the " + attachedCommunicationEvents + ".",
          buttons: {
            confirm: {
              label: 'Yes',
              className: 'btn-success'
            },
            cancel: {
              label: 'No',
              className: 'btn-danger'
            }
          },
          callback: function(result) {
            if (result) {
              $('#template_event_type_id').val("attachTemplate");
              if (template_id > 0) {
                getDataThroughAxios('/getTemplate/' + template_id);
              }
            }
            console.log("else" + result);
          }
        });
      } else {
        $('#template_event_type_id').val("attachTemplate");
        if (template_id) {
          getDataThroughAxios('/getTemplate/' + template_id);

        }
      }

    })
    .catch(function(error) {
      console.log("error" + error);
    });
}

function populateTemplatePlaceHolders(url, id) {
  axios.get(url + "/" + id)
    .then(function(response) {
      console.log(response);
      if (Object.keys(response['data'].length > 0)) {
        $("#place_holder_id").empty();
        $("#place_holder_id").append('<option value="0">--Select--</option>');
        $.each(response['data'], function(key, value) {
          $("#place_holder_id").append("<option value='" + value.id + "'>" + value.name + "</option>");
        });
      }
    })
    .catch(function(error) {
      console.log("error" + error);
    });
}

function getTemplateData(template) {
  $('#template_name_id').val(template.template_name);
  $('#template_subject_id').val(template.subject);
  $("#communication_channel_type_id").select2("val", '' + template.communication_channel_id + '');
  $('#template_code_id').val(template.template_code);
  $('#template_url_id').val(template.template_url);
  $('#tag_name_id').val(template.tags);
  var to = template.to;
  $("#template_to").val(to.split(',')).select2();
  if (template.cc != null) {
    console.log(template.cc);
    var cc = template.cc;
    $("#template_cc").val(cc.split(',')).select2();
  }
  $('#summernote').summernote('code', template.template);
}

function populateCommunicationChannels(data) {
  $("#communication_channel_type_id").empty();
  $("#communication_channel_type_id").append('<option value="0">--select--</option>');
  $.each(data, function(key, value) {
    $("#communication_channel_type_id").append("<option value='" + value.id + "'>" + value.name + "</option>");
  });
}

function errorResponse() {

}

function validateMandatoryFields() {
  var valid = true;
  var summernote = $('.note-editable').val();
  $(".mandatory").each(function() {
    if ($(this).val().trim() === '' || $(this).val() === '0') {
      $(".mandatory").removeClass('fillFields');
      $(this).addClass('fillFields');
      valid = false;
      return valid;
    }
  });
  // if(summernote === '' || summernote == null)
  // {
  //   $('.note-editable').addClass('fillFields');
  //     valid = false;
  // }
  return valid;
}
// adding placeholders from select
function addPlaceHolders(placeHolders) {
  console.log($(parentNode).parents());
  if ($(parentNode).parents().is('.note-editable') || $(parentNode).is('.note-editable')) {
    var span = document.createElement('span');
    // span.setAttribute('style','font-style:normal');
    span.innerHTML = placeHolders;
    range.deleteContents();
    range.insertNode(span);
    //cursor at the last with this
    range.collapse(false);
    selection.removeAllRanges();
    selection.addRange(range);
  } else {
    if($(parentNode).is('.subject')){
    var $txt = jQuery("#template_subject_id");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = placeHolders;
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
    return;
  }else{
    if($(parentNode).is('.textarea')){
    var $txt = jQuery("#textarea");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = placeHolders;
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
    return;
  }
  }
  }
}

function clearForm() {
  $('.note-editable').html('');
  $('#communication_channel_type_id').val(0);
  $('#template_name_id').val('');
  $('#template_url_id').val('');
}

function dialogMessage(message) {
  bootbox.confirm({
    message: message,
    buttons: {
      confirm: {
        label: 'Yes',
        className: 'btn-success'
      },
      cancel: {
        label: 'No',
        className: 'btn-danger'
      }
    },
    callback: function(result) {
      if (result) {
        console.log(result);
        return true;

      }
      console.log("else" + result);
    }
  });
}
