<?php

namespace vnnogile\Sms\Utility;

//Custom Imports Below
use vnnogile\Sms\Jobs\SmsJob;
use Illuminate\Support\Facades\Log;

/**
 * This is a a common utility method for email
 *
 * All file upload controllers can use this trait to 
 * upload files to the server
 */
class SmsUtility
{
    /**
     * Send email with or without attachments
     * @param Array $smsData
     * @param Array $masterDataInterface
     * @param Array $utilsInterface
     * @return Array $status
     */
    public static function sendSms($smsData, $masterDataInterface, $utilsInterface){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $queueName=$utilsInterface->getConfigValue($masterDataInterface, "sms_queue");
        if (isset($queueName)){
            SmsJob::dispatch($emailData)->onQueue($queueName);    
        }else{
            SmsJob::dispatch($emailData)->onQueue('sms');    
        }
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return array('success' => True);
    }

    /**
     * Send email with or without attachments
     * @param Array $smsData
     * @param Array $utilsInterface
     * @return Array $status
     */
    public static function sendCommunicationSms($role_data, $masterDataInterface, $utilsInterface){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $queueName=$utilsInterface->getConfigValue($masterDataInterface, "sms_queue");;
        if (!isset($queueName)){
            $queueName = "sms";
        }

        foreach ($role_data as $id => $value) {

            $smsData['template_name']=$value['template'];
            $smsData['data']=$value['template_data'];
            $smsData['to']=$value['phone_no'];

            //trail data
            $smsData['trail_data']['login_id']=$value['login_id'];
            if (array_key_exists('program_code', $value)){
                $smsData['trail_data']['program_code']=$value['program_code'];    
            }
            if (array_key_exists('customer_organization_code', $value)){
                $smsData['trail_data']['customer_organization_code']=$value['customer_organization_code'];
            }
            if (array_key_exists('job_id', $value)){
                $smsData['trail_data']['job_id']=$value['job_id'];
            }

            if (array_key_exists('party_id', $value)){
                $smsData['trail_data']['party_id']=$value['party_id'];
            }

            $smsData['trail_data']['communication_event_id']=$value['communication_event_id'];
            $smsData['trail_data']['tenant_code']=$value['tenant_code'];

            SmsJob::dispatch($smsData)->onQueue($queueName);  
            Log::debug("sms sent in job for".$id);  
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return "success";

    }   
}
