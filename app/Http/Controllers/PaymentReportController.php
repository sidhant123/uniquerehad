<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\PaymentReportService;
use App\Services\ProjectService;
use App\Services\ExpenseTypeService;


class PaymentReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function createPaymentReport() {

        $project = ProjectService::retrieveProject();
        $expense_type = ExpenseTypeService::retrieveExpenseTypes();

		return view('reports.payment_report' ,compact('project','expense_type'));
	}


public function exportPaymentReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$start_date = $request['start_date'];
		$end_date = $request['end_date'];
		$projectCode = PaymentReportService::getProjectName($request['project_id']);
		$expense_code = 'All';
		if($request['expense_type_id']>0){
		$expense_code = PaymentReportService::getExpenseTypeName($request['expense_type_id']);
		}
		$queryBuilder = PaymentReportService::retrievePaymentReportDetailQuery($request['project_id'],$start_date,$end_date,$request['expense_type_id']);
	    self::downloadDetailReportExcel($queryBuilder,$projectCode, $expense_code, $start_date,$end_date);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadDetailReportExcel($queryBuilder, $projectCode,$expense_code,$start_date,$end_date ) {
		$fileName = $projectCode . '_Payment_Report_'.$expense_code.'_From_'.$start_date.'_To_'.$end_date;

		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();

      $keys = array( "Project Name", "Service", "Expense to Company", "Expense Type", "Expense Date", 
      	"Bill No",  "Unit", "Qty", "Rate", 
      	"Base Amout", "GST", "Total Amount", "Amount Paid" );
        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array(
        		 $row->project_name, $row->service_name, $row->org_name, $row->expense_type_name, $row->expense_date, 
        		 $row->bill_no,  $row->unit, $row->qty, $row->rate, 
        		 $row->base_amount, $row->gst_amount, $row->amount, $row->amount_paid );
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}


}
