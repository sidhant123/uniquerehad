<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Apr 2018 19:13:38 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LearningTreeStructure
 * 
 * @property int $id
 * @property int $tab
 * @property int $container_learning_tree_id
 * @property array $details
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \App\Models\LearningTree $learning_tree
 *
 * @package App\Models
 */
class LearningTreeStructure extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'tab' => 'int',
		'container_learning_tree_id' => 'int',
		// 'details' => 'json',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'tab',
		'container_learning_tree_id',
		'details',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function learning_tree()
	{
		return $this->belongsTo(\App\Models\LearningTree::class, 'container_learning_tree_id');
	}
}
