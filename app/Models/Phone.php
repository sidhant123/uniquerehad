<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Jan 2018 18:30:11 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class Phone
 * 
 * @property int $id
 * @property int $party_id
 * @property int $phone_type_id
 * @property string $phone_number
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\PhoneType $phone_type
 * @property \App\Models\Party $party
 *
 * @package App\Models
 */
class Phone extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'party_id' => 'int',
		'phone_type_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'party_id',
		'phone_type_id',
		'phone_number',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function phone_type()
	{
		return $this->belongsTo(\App\Models\PhoneType::class);
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class);
	}
}
