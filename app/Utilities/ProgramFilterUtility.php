<?php

/**
 * Created by vnnogile Solutions.
 * Date: Sun, 24 Dec 2017.
 */

namespace App\Utilities;

use DB;
use Illuminate\Support\Facades\Log;
use vnnogile\FilterViews\Interfaces\FilterInterface;

/**
 * Service ServiceInterface
 *
 */
class ProgramFilterUtility implements FilterInterface
{
    public static function filter($request = array(), $filterColumns = array(), $records = array(), $isTaggable = false)
    {
        // filter method for getting filtered data
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            if (session()->has('userPrincipal')) {
                $userPrincipal = session('userPrincipal');
            }
            $program_user_exist = DB::table('program_user')
                ->where('user_id', '=', $userPrincipal->getUserId())->count();

            $data = DB::table('programs')
                ->select('programs.id as id', 'programs.name as name', 'programs.code as code', 'programs.id as id');
            if ($program_user_exist) {
                $data->leftJoin('program_user', 'program_user.program_id', '=', 'programs.id')
                    ->where('user_id', '=', $userPrincipal->getUserId());
            }
            if ($isTaggable) {
                if ($request->has('search')) {
                    $data->leftJoin('program_tags', 'program_tags.entity_id', '=', 'programs.id');
                    $data->whereRaw('program_tags.tags_virtual REGEXP "' . $request['search'] . '"');
                }
            } else {
                if ($request->has('search')) {
                    $data->whereRaw("(programs.name like '%" . $request['search'] . "%' or programs.code like '%" . $request['search'] . "%' )");
                }
            }
            if ($request->has('sort')) {
                switch ($request['sort_column_name']) {
                    case 'id':
                        $data->orderBy('programs.id', $request['sort']);
                        break;
                    case 'name':
                        $data->orderBy('programs.name', $request['sort']);
                        break;
                    case 'code':
                        $data->orderBy('programs.code', $request['sort']);
                        break;
                }
            } else {
                $data->orderBy('programs.id');
            }
            foreach ($filterColumns as $key => $columnName) {
                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $data->where($columnName, '=', $request[$columnName]);
                    }
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            Log::debug($e);
        }
    }
}
