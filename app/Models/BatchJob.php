<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 08 Aug 2018 16:43:42 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BatchJob
 * 
 * @property int $id
 * @property string $job_name
 * @property string $status
 * @property string $exception
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class BatchJob extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'job_name',
		'status',
		'exception',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
