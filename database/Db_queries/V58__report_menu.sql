INSERT INTO public.menus(
     id, display_name, name, sequence, parent_menu_id, icon, created_at, updated_at,  created_by, updated_by)
    VALUES 
    (327, 'Data Dumps', 'Data Dumps', 140,  null, 'menu-big-vnn vnn-messageboard', now(), now(),  1, 1),
    (328,'Stock', 'List Stock Report', 145, 327,  '', now(), now(),  1, 1), 
    (329, 'BOQ', 'List Boq Report', 150, 327, '', now(), now(),  1, 1);

INSERT INTO public.permissions(
          id,  name, created_at, updated_at,  created_by, updated_by)
    VALUES 
     (61,'Data Dumps', now(), now(), 1, 1),
     (62,'List Stock Report', now(), now(), 1, 1),
     (63,'List Boq Report', now(), now(), 1, 1);

INSERT INTO public.permission_role(
             role_id, permission_id, created_at, updated_at, created_by, updated_by)
    VALUES 
     ( 1, 61, now(), now(), 1,1),
     ( 1, 62, now(), now(), 1,1),
     ( 1, 63, now(), now(), 1,1),
     ( 2, 61, now(), now(), 1,1),
     ( 2, 62, now(), now(), 1,1),
     ( 2, 63, now(), now(), 1,1);

