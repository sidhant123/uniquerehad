@extends('vnnogile::state_base')
@section('title', 'View State')
@section('editMethod')

@endsection
@section('disabled', 'disabled')
@section('showStateName', 'readonly')
@section('editID', $data->id)
@section('state_name', $data->name)
@section('disable_submit', 'pointerEventNone')
