@section('filterContent')
<p></p>
<form id="filterForm">
	<div class="row" id="filter_section_div">
		@foreach ($filter_master_data as $data)
		<div class="{{$data->label_css}}"><label>{{ucfirst($data->function_name)}}: </label></div>
		<div class="{{$data->field_css}}">
			<select class="form-control" id='{{$data->control_id}}' name='{{$data->control_name}}'>
				<option value="0">{{$data->control_text}}</option>
				@isset($compositeViewDataURL[$data->function_name])
				@foreach ($compositeViewDataURL[$data->function_name] as $key => $url_data)
				@if($data->action == 0)
				@if(isset($request[$data->control_name]))
				@if($url_data["id"] == $request[$data->control_name])
				$("#{{$data->control_id}}").append("<option value='{{ $url_data["id"] }}' selected>{{ $url_data['name'] }}</option>");
				@else
				$("#{{$data->control_id}}").append("<option value='{{ $url_data["id"] }}'>{{ $url_data['name'] }}</option>");
				@endif
				@else
				$("#{{$data->control_id}}").append("<option value='{{ $url_data["id"] }}'>{{ $url_data['name'] }}</option>");
				@endif
				@endif
				@if($data->action == 1)
				@if(isset($request[$data->control_name]))
				@if($url_data["id"] == $request[$data->control_name])
				$("#{{$data->control_id}}").append("<option value='{{ $url_data["id"] }}' selected>{{ $url_data['name'] }}</option>");
				@else
				$("#{{$data->control_id}}").append("<option value='{{ $url_data["id"] }}'>{{ $url_data['name'] }}</option>");
				@endif
				@else
				$("#{{$data->control_id}}").append("<option value='{{ $url_data["id"] }}'>{{ $url_data['name'] }}</option>");
				@endif
				@endif
				@endforeach
				@endisset
			</select>
		</div>
		@endforeach
		<div class="col-xs-2"><button id="filterBtn" type="button" class="btn btn-primary">Filter</button></div>
	</div>
</form>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
	@foreach($filter_master_data as $data)
	$("#{{$data->control_id}}").select2({});
	@if($data->function_name == 'Customer')
	@endif
	@if($data->action == 1)
	$(document).on('change','#{{$data->load_event}}',function(){
		var id = $(this).val();
		if(id > 0){
		console.log(id);
		$("#{{$data->control_id}}").empty();
		axios.get('{{$data->data_url}}'+'/'+id)
  		.then(function (response) {
    	console.log(response);
    	if(Object.keys(response).length > 0){
					$.each(response['data'], function (key, value) {
						$("#{{$data->control_id}}").append("<option value="+value.id+">"+value.name+"</option>");
					});
				}
  	})
  		.catch(function (error) {
    	console.log("error"+error);
  });
  	}
	});
	@endif
	@endforeach
$(document).on('click','#filterBtn',function(){
var formData = $('#filterForm').serialize();
console.log(formData);
var route = '{{$route_name}}';
var route_name = route.split('.');
var url = '/'+route_name[0]+'?' + formData;
window.location = url;
});
});

</script>
@endpush
