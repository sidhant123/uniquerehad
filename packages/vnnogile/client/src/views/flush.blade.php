<form method="POST" action="{{ $url }} ">
<select name="cache_tag_name">
	<option value="0">-- Select Entity --</option>
	@foreach($records as $record)
		<option value="{{ $record->cacheTagName }}">{{ $record->entityName }}</option>
	@endforeach
</select>&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" value="Flush Entity Cache">
</form>