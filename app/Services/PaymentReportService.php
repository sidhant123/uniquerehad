<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Utilities\Services\ConstantsUtility;

use App\Models\Project;
use App\Models\ExpenseType;

/**
 * Class AddressMasterService
 *
 */
class PaymentReportService {
	
	  public static function retrievePaymentReportDetailQuery($project_id,$start_date,$end_date,$expense_type_id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $start_date = $start_date. ' 00:00:01';
        $end_date = $end_date.' 23:59:59';

        $queryBuilder = DB::table('payments')
            ->select(
                'projects.name as project_name' , 
                'expense_types.name as expense_type_name' , 
                'organizations.name as org_name' ,  
                'payments.expense_date' ,  
                'payments.service_name' ,  
                'payments.bill_no' ,  
                'payments.item_desc' ,  
                'payments.unit' ,  
                'payments.qty' ,  
                'payments.rate' ,  
                'payments.amount' ,  
                'payments.gst_amount' ,  
                'payments.amount_paid' ,  
                'payments.base_amount' 
        )
            ->leftJoin('projects', 'projects.id', '=', 'payments.project_id')
            ->leftJoin('expense_types', 'expense_types.id', '=', 'payments.expense_type_id')
            ->leftJoin('organizations', 'organizations.id', '=', 'payments.organization_id')
            ->where('payments.project_id','=', $project_id)
            ->where('payments.deleted_at', null)
            ->whereBetween('payments.expense_date', [$start_date, $end_date])
            ->orderBy('payments.expense_date','asc')
            ->orderBy('payments.expense_type_id', 'asc');

             if($expense_type_id>0)
             {
               $queryBuilder->where('payments.expense_type_id','=', $expense_type_id);
             }
        Log::debug('****QUERY*****');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $queryBuilder;
    }

    public static function getProjectName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $project = Project::select('name')->where('id', '=', $Id)->first();
        return trim($project->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
     public static function getExpenseTypeName($Id){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $exp = ExpenseType::select('name')->where('id', '=', $Id)->first();
        return trim($exp->name);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
