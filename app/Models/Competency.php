<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;
use Sofa\Eloquence\Eloquence;
/**
 * Class Competency
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $cluster_id
 * @property int $customer_party_id
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Cluster $cluster
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $questions
 *
 * @package App\Models
 */
class Competency extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['name'];
	protected $casts = [
		'cluster_id' => 'int',
		'customer_party_id' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'cluster_id',
		'customer_party_id',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function cluster()
	{
		return $this->belongsTo(\App\Models\Cluster::class);
	}

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'customer_party_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function questions()
	{
		return $this->hasMany(\App\Models\Question::class, 'competencies_id');
	}
}
