<?php

namespace App\Http\Requests;

use App\Models\Organization;
use App\Models\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

class OrganizationRequest extends FormRequest
{
     
    protected $masterDataInterface;
    protected $utilsInterface;

    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
    }

    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $userPrincipal = session('userPrincipal');
        $tenantPartyId = $userPrincipal->getTenantPartyId();   
        $company='';
        $validationRule= array();

        if (null != $this->route('company')){
            Log::debug("Route company is not null");
            $company = $this->route('company');
            $validations = $this->utilsInterface->getValidationRulesForEntity('Organization', 'Update');
            foreach ($validations as $validation) {
                $extractedFromDb = $validation->validation_rule;
                $extractedFromDb = str_replace('$tenantPartyId', $tenantPartyId, $extractedFromDb);
                $extractedFromDb = str_replace('$company', $company, $extractedFromDb);
                $validationRule[$validation->field_name] = $extractedFromDb;
            }
/*            return [
                'organization_name' => 'required|max:255|unique:organizations,name,' . $this->route('company') . ',id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId,
                'organization_url' => 'required',
                'organization_code' => 'required|max:20|unique:organizations,organization_code,' . $this->route('company') . ',id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId,

                'pm_firstName'=> 'required',
                'pm_lastName'=> 'required',
                'pm_email'=> 'required|max:1000',
                'cc_firstName'=> 'required',
                'cc_lastName'=> 'required',
                'cc_email'=> 'required|max:1000',
            ];*/

        }else{
            Log::debug("Route company is not sent");
            $validations = $this->utilsInterface->getValidationRulesForEntity('Organization', 'Create');
            foreach ($validations as $validation) {
                $extractedFromDb = $validation->validation_rule;
                $extractedFromDb = str_replace('$tenantPartyId', $tenantPartyId, $extractedFromDb);
                $validationRule[$validation->field_name] = $extractedFromDb;
            }
/*            return [
                'organization_name' => 'required|max:255|unique:organizations,name,NULL,id,deleted_at,NULL,tenant_party_id,' . $tenantPartyId,
                'organization_url' => 'required',
                'organization_code' => 'required|max:20|unique:organizations,organization_code,NULL,id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId,

                'pm_firstName'=> 'required|max:255',
                'pm_lastName'=> 'required|max:255',
                'pm_email'=> 'required|max:1000|unique:people,email,NULL,id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId,

                'cc_firstName'=> 'required|max:255',
                'cc_lastName'=> 'required|max:255',
                'cc_email'=> 'required|max:1000|unique:people,email,NULL,id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId,
            ];*/
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $validationRule;        
    }

    // public function rules()
    // {
    //     Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    //     $userPrincipal = session('userPrincipal');
    //     $tenantPartyId = $userPrincipal->getTenantPartyId();
    //     if (null != $this->route('company')){
    //         Log::debug("Route company is not null");
    //         $company = $this->route('company');
    //         $organization_name_rule = 'required|max:255|unique:organizations,name,' . $company . ',id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId;
    //         $organization_code_rule = 'required|max:255|unique:organizations,organization_code,' . $company . ',id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId;
    //     }else{
    //         Log::debug("Route company is not sent");
    //         $organization_name_rule = 'required|max:255|unique:organizations,name,NULL,id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId;
    //         $organization_code_rule = 'required|max:255|unique:organizations,organization_code,NULL,id,deleted_at,NULL,tenant_party_id,' .$tenantPartyId;
    //     }

    //     Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    //     return [
    //         'organization_name' => $organization_name_rule,
    //         'organization_url' => 'required',
    //         'organization_code' => $organization_code_rule,

    //         'pm_firstName'=> 'required',
    //         'pm_lastName'=> 'required',
    //         'pm_email'=> 'required',

    //         'cc_firstName'=> 'required',
    //         'cc_lastName'=> 'required',
    //         'cc_email'=> 'required',
    //     ];
    // }    
}
