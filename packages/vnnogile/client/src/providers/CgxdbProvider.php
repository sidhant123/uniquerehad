<?php

namespace vnnogile\Client\Providers;

use Illuminate\Support\ServiceProvider;


class CgxdbProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'cache');
        $this->app->make('vnnogile\Client\Controllers\BaseController');
        $this->app->make('vnnogile\Client\Controllers\CacheController');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
                __DIR__. '/../config/data.php', 'data'
        );
        include __DIR__. '/../routes/routes.php';

        $this->app->bind('vnnogile\Client\Interfaces\MasterDataInterface', function ($app) {
            return new \vnnogile\Client\Db\MasterDataDbService;
        });
        $this->app->bind('vnnogile\Client\Interfaces\UtilsInterface', function ($app) {
            return new \vnnogile\Client\Utils\UtilsService;
        });        

    }
}
