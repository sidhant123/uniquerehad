@extends('project.project')
@section('title','Create Project')
@section('editMethod')
{{method_field('post')}}
@endsection
@section('view-nav-bar', 'hide')
@section('update-nav-bar', 'hide')
@section('edit_btn','hide')

@section('importBoq')
<div   style="border-style:dotted; border-width: 1px;">
    {{ csrf_field() }}

  <h3 class="headings">{{__('labels.users.import_boq')}}</h3>

  <div class="row">

     <div class="col-xs-12 req">
      <div class="form-group required">
          <label for="boqFile">{{__('labels.users.import_boq')}}</label>
          <input type="file" class="form-control mandatory fillFields"  id="boqFile"  name="imported_boq_file" accept=".xlsx" >
        </div>
    </div>


  </div>   

 
<p></p>
<div class="row">
<div class="col-xs-12" align="center">
    <button class="btn btn-primary">
 <!-- <a href="\excel\user_import_with_photo.zip" target="_blank">Download Excel Format</a>-->
    <a href="\storage\ExcelFormat\BOQ_Import_Format.xlsx" target="_blank">Download Excel Format</a>

</button>

</div>
</div>
@endsection