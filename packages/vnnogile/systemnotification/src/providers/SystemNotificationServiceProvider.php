<?php

namespace vnnogile\SystemNotification\Providers;

use Illuminate\Support\ServiceProvider;

class SystemNotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
 
        $this->app->make('vnnogile\systemnotification\controllers\SystemNotificationController');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../jobs/SystemNotificationJob.php';        
        include __DIR__.'/../utility/SystemNotificationUtility.php';
        include __DIR__.'/../controllers/SystemNotificationController.php';

        
        include __DIR__.'/../jobs/SystemNotificationSuccessJob.php';        
        
        
        include __DIR__.'/../services/FirebaseImplementationService.php';
        
        
    }
}
