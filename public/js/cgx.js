
function validateMandatoryFields(){
	var valid = true;
	console.log("In validateMandatoryFields function.");
   $(".mandatory").removeClass('fillFields');

   var i=0;
     $(".mandatory").each(function () {
     	 if ($(this).val().trim() === '' || $(this).val() === '0')
        {
            // $(".mandatory").removeClass('fillFields');
            $(this).addClass('fillFields');
            valid = false;
            if (i==0) {
               //
               i=1;
               var scrollPos =  $(this).offset().top;
               console.log(scrollPos);
               $('html, body').animate({scrollTop:(Number(scrollPos) - 72)}, 'slow');
            }
            // return valid;
        }
     });
     return valid;
}

function validateMandatoryFieldsForUserImport(){
  var valid = true;
  console.log("In validateMandatoryFields function.");
     $(".importMandatory").each(function () {
       if ($(this).val().trim() === '' || $(this).val() === '0')
        {

            $(this).addClass('fillFields');
            valid = false;
            return valid;
        }
     });
     return valid;
}



function fileValidation(id){
  console.log(id);
    var imgValidate = validateImgSizeAndPixel(id);
    if(imgValidate){
      alert(imgValidate);
      console.log(imgValidate);
    }
}


function validateImgSizeAndPixel(img_id){
    var img = document.getElementById(img_id);
    if( img.files[0].size > 8388608){
      $('#'+img_id).val('');
      return "Image size should be less than 8mb.";
    }
    console.log(img.clientWidth);
    console.log(img.clientHeight);

    // if((img.clientWidth < 400 && img.clientHeight < 400) || (img.clientWidth > 2000 && img.clientHeight > 2000)){
    //  // $('#file').val('');
    //   return "Image pixel width and height should be in the range of 400x400px - 2000x2000px.";
    // }
  }

function zipValidation(){


      var file=document.getElementById('zip');
      var fileName=file.files[0].name;
      console.log(fileName);

      if(!fileName.endsWith("zip")){
        $('#zip').val('');
        window.alert("only zip files are allowed");
      }


}

// selectTo and autocomplete for country, state, city and pincode

var reset_input = 0;

function getDataThroughAxios(url,control_id){
    axios.get(url)
        .then(function (response) {
        console.log(response);
        if(Object.keys(response).length > 0){
                    resetSelect(control_id)
                    $.each(response['data'], function (key, value) {
                        $("#"+control_id).append("<option value="+value.id+">"+value.name+"</option>");
                    });
                }
    })
        .catch(function (error) {
        console.log("error"+error);
  });
}
function resetSelect(control_id){
    $('#'+control_id).empty();
    $("#"+control_id).append("<option value='0'>----Select----</option>");
}

$(window).load(function() {
    $(".loader").fadeOut("slow");
    //document.getElementById("main").style.marginLeft = "4%";
});



$('.btn-expand-collapse').click(function(e) {
        $('.navbar-primary').toggleClass('collapsed');
});
