


update menus set display_name='Receipts', name='Receipts' where name = 'Payments' ;
update menus set display_name='List', name='List Receipts' where name = 'List Payments' ;
update menus set display_name='Create Receipt', name='Create Receipts' where name = 'Create Payments' ;


update permissions set name = 'Receipts' where name = 'Payments';
update permissions set name = 'List Receipts' where name = 'List Payments';
update permissions set name = 'Create Receipts' where name = 'Create Payments';
update permissions set name = 'Show Receipts' where name = 'Show Payments';
update permissions set name = 'Delete Receipts' where name = 'Delete Payments';
update permissions set name = 'Edit Receipts' where name = 'Edit Payments';

  
drop table payments;
-- Table: public.receipts

-- DROP TABLE public.receipts;
create sequence receipts_seq ;
CREATE TABLE public.receipts
(
  id bigint NOT NULL DEFAULT nextval('receipts_seq'::regclass),
  payment_no character varying,
  project_id integer NOT NULL,
  boq_no character varying NOT NULL,
  invoice_no character varying(100) NOT NULL,
  invoice_date date NOT NULL,
  payment_method character varying(50),
  payment_ref_no character varying,
  payment_receipt_date date,
  amount_received numeric(15,2),
  gst_amount numeric(10,2) NOT NULL,
  invoice_amount numeric(15,2),
  total_amount numeric(15,2),
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  tds_deduction numeric(10,2),
  other_deduction numeric(10,2),
  remark character varying,
  base_amount numeric(15,2),
  total_deduction numeric(15,2),
  CONSTRAINT receipts_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.receipts
  OWNER TO postgres;





  update list_view_masters function_name = 'List Receipts', entity_name ='Receipt', 
    filter_class_name = 'App\Utilities\ReceiptFilterUtility'
    where entity_name = 'Payment';
