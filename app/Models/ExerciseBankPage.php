<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 15 Mar 2018 16:32:45 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
/**
 * Class ExerciseBankPage
 * 
 * @property int $id
 * @property int $exercise_banks_id
 * @property string $page_title
 * @property array $questions
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\ExerciseBank $exercise_bank
 *
 * @package App\Models
 */
class ExerciseBankPage extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'exercise_banks_id' => 'int',
		//'questions' => 'json',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'exercise_banks_id',
		'page_title',
		'questions',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function exercise_bank()
	{
		return $this->belongsTo(\App\Models\ExerciseBank::class, 'exercise_banks_id');
	}
}
