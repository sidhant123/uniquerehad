<nav id="sidebar">
<span id="sidebarCollapse" style="float: right;"><i class="vnn vnn-hamburger" style=" font-size:large;"></i></span>
<ul class="list-unstyled components">
 @foreach ($potentialParentMenuItems as $key => $value)
  @if(array_key_exists($key, $childrenMenu))
  <li>
      <a class="sidebar_menu" href="#{{$key}}" data-toggle="collapse" aria-expanded="false" >
          <i class="{{ $value->icon }}"></i>
          <span class="inner_text">{{ $value->name }}</span>
      </a>
      <ul class="collapse list-unstyled" id="{{ $key }}">
      @foreach ($childrenMenu[$key] as $menuItem)
      @if($menuItem->parent_menu_id == $key)
          <li><a href="{{ $menuItem->url }}"><span class="inner_text">{{$menuItem->display_name}}</span></a></li>
      @endif
      @endforeach
      </ul>
  </li>
  @else
  <li>
      <a href="#">
          <i class="glyphicon glyphicon-link"></i>
          Portfolio
      </a>
  </li>
  @endif
 @endforeach
</ul> 
</nav>
