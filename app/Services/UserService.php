<?php

namespace App\Services;

//Custom imports
use App\Events\UserUploadEvent;
use App\Models\ImportJob;
use App\Models\Organization;
use App\Models\OrganizationAttribute;
use App\Models\Party;
use App\Models\PasswordLogic;
use App\Models\PeopleAttribute;
use App\Models\Person;
use App\Models\Phone;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\UserAttribute;
use App\Services\PartyTypeService;
use App\Services\TenantService;
use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;
use ZipArchive;

/**
 * Class AddressMasterService
 *
 */
class UserService {
	use ValidationTrait;

	private static $PRIMARY_CONTACT = 'primaryContact';
	private static $SECONDARY_CONTACT = 'secondaryContact';

	public static function getUserEmailIDs() {
		return User::select('email')->get();
	}

	private static function endsWith($str, $sub) {
		return (substr($str, strlen($str) - strlen($sub)) === $sub);
	}

	public static function unzipFile($zipFile, $folder) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$zip = new ZipArchive;
		if ($zip->open($zipFile) == TRUE) {
			for ($i = 0; $i < $zip->numFiles; $i++) {
				$stat = $zip->statIndex($i);
				if (Self::endsWith($stat['name'], '.xlsx')) {
					$excelFileName = $stat['name'];
				}
			}
			$zip->extractTo($folder);
			$zip->close();
		} else {
			$excelFileName = 'failed';
		}

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $excelFileName;
	}

	public static function uploadFile($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->uploadFileToPrivateStorage($data['userRequest'], $data['tempPath']);
	}

	public static function moveFile($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->moveFile($data['uploadedFileName'], $data['tempPath'], $data['finalPath']);
	}

	public static function deleteFile($data, $file) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->deleteFile($file);
	}

	public static function deleteDirectory($data, $directory) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $storageUtility->deleteDirectory($directory);
	}

	public static function getPersonByRolePM($party_id, $role_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$ret = Role::with(['users.party.person' => function ($query) use ($party_id) {$query->where(['customer_party_id' => $party_id]);}])->where('id', '=', $role_id)->get();
		$persons = array();
		foreach ($ret as $key => $value) {
			foreach ($value['users'] as $k => $v) {
				if (!empty($v->party->person)) {
					$persons = $v->party->person;
				}
			}
		}
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $persons;
	}
	public static function getPersonByRoleCC($party_id, $role_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$ret = Role::with(['users.party.person' => function ($query) use ($party_id) {$query->where(['customer_party_id' => $party_id]);}])->where('id', '=', $role_id)->get();
		$persons = array();
		foreach ($ret as $key => $value) {
			foreach ($value['users'] as $k => $v) {
				if (!empty($v->party->person)) {
					$persons = $v->party->person;
				}
			}
		}
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $persons;
	}
	public static function getUsers() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return Person::select('id', 'first_name', 'party_id')->get();
	}

	public static function getUsersByCompany($party_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return Person::select('id', 'first_name', 'last_name', 'party_id')->where('customer_party_id', $party_id)->get();
	}

	public static function getUsersByPatyId($party_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);

		$data = DB::table('people')->where('people.customer_party_id', $party_id)
			->leftJoin('users', 'users.party_id', '=', 'people.party_id')
			->select('users.id as id', 'users.first_name as first_name', 'users.last_name as last_name')->get();

		return $data;

	}
	public static function getUserDetailsById($id, MasterDataInterface $masterDataInterface) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		// $return_data = $masterDataInterface->find("Person", $id, array('last_name', 'employee_id', 'department', 'designation', 'email', 'linkedin_profile', 'facebook_id', 'twitter_handle'));
		$people = Person::find($id);
		$user = User::where('party_id', $people['party_id'])->first();
		$people['user_id'] = $user['userid'];

		$contacts = $people->party->contacts;
		$people['primary_contact'] = $contacts[0]['phone_number'];
		if (isset($contacts[1])) {
			$people['secondary_contact'] = $contacts[1]['phone_number'];
		}

		// abhi - 26/03/19
		$dobAttr = PeopleAttribute::where(['people_id' => $people['id'], 'attr_name' => 'dob'])->first();
		$people['dob'] = $dobAttr['attr_value'];

		Log::debug($people);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $people;
	}
	public static function updateUser($data, $fileUpload = False) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		//dd($data);
		$check = DB::transaction(function () use ($data, $fileUpload) {
			$data['tenantPartyId'] = 1;//TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
			$data['userPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "User");
			$data['companyPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "Company");
			$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;
			$data['tempPath'] = "/temp/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . '/';

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$person = Person::find($data['id']);

			if ($fileUpload == True) {
				//Delete the earlier file from the folder structure
				Self::deleteFile($data, $person->photo);
				//then upload new file
				$uploadInfo = Self::uploadFile($data);
			}

			$org = Organization::where(
				[
					['id', '=', $data['organization_name']],
					['tenant_party_id', '=', $data['tenantPartyId']],
				])->first();

			$userId = $person->update(['first_name' => $data['firstName'],
				'last_name' => $data['lastName'],
				//'department' => $data['department'],
				//'employee_id' => $data['employeeId'],
				//'designation' => $data['designation'],
				'email' => $data['email'],
				//'personal_email' => $data['personalEmail'],
				// 'facebook_id' => $data['facebookId'],
				// 'linkedin_profile' => $data['linkedinProfile'],
				// 'twitter_handle' => $data['twitterHandle'],
				// 'google_profile' => $data['googlePlus'],
				'updated_by' => $data['authenticatedUser']->id]);

			Log::debug($data['gender']);
			if (isset($data['gender'])) {
				if ($data['gender'] == 'M') {
					$person->gender = 'M';
					if ($person->photo == '/tenants/1/general/user-female.png') {
						$person->photo = '/tenants/1/general/user-male.png';
						$person->original_photo = '/tenants/1/general/user-male.png';
					}
				} else {
					$person->gender = 'F';
					if ($person->photo == '/tenants/1/general/user-male.png') {
						$person->photo = '/tenants/1/general/user-female.png';
						$person->original_photo = '/tenants/1/general/user-female.png';
					}
				}
				// $person->save();
			}

			// abhi - 26/03/19
			if (isset($data['profile']) && $data['profile'] == 'private') {
				$person->is_private = 1;
			} else {
				$person->is_private = 0;
			}

			$person->save();

			$peopleAttribute = PeopleAttribute::where(['people_id' => $data['id'], 'attr_name' => 'dob'])->first();
			if ($peopleAttribute) {
				// if (isset($data['dob'])) {
				if (!isset($data['dob'])) {
					$data['dob'] = '';
				}
				$peopleAttribute->attr_value = $data['dob'];
				$peopleAttribute->updated_by = $data['authenticatedUser']->id;
				$peopleAttribute->save();
				// } else {
				// 	$peopleAttribute->update(['deleted_by' => $data['authenticatedUser']->id]);
				// 	$peopleAttribute->delete();
				// }

			} elseif (!$peopleAttribute && isset($data['dob'])) {
				//
				$peopleAttribute = new PeopleAttribute;
				$peopleAttribute->people_id = $data['id'];
				$peopleAttribute->attr_name = 'dob';

				$peopleAttribute->attr_value = $data['dob'];

				$peopleAttribute->created_by = $data['authenticatedUser']->id;
				$peopleAttribute->updated_by = $data['authenticatedUser']->id;
				$peopleAttribute->save();
			}

			if ($fileUpload == True) {
				Log::debug("***********Final File Name**************");
				Log::debug($uploadInfo['uploadedFileNames']['photo'][0]);
				Log::debug($uploadInfo['originalFileNames']['photo'][0]);
				$data['finalPath'] = "/public/tenants/" . $data['tenantPartyId'] . "/user/" . $person->party_id . '/profiles/';
				$data['uploadedFileName'] = $uploadInfo['uploadedFileNames']['photo'][0];
				$success = Self::moveFile($data);
				if ($success == true) {
					$person->photo = str_replace("/public", "", $data['finalPath']) . $uploadInfo['uploadedFileNames']['photo'][0];
					$person->original_photo = $uploadInfo['originalFileNames']['photo'][0];
					$person->save();
				}
			}

			$phoneTypes = $data['masterDataInterface']->list("PhoneType");
			$phoneTypeId = $phoneTypes[0]->id;
			$count = sizeof($person->party->contacts);
			Log::debug('************dddddd*********');
			Log::debug($data[Self::$PRIMARY_CONTACT]);
			Log::debug($data[Self::$SECONDARY_CONTACT]);
			Log::debug($count);
			if ($count == 0) {
				if (isset($data[Self::$PRIMARY_CONTACT])) {
					$phone = new Phone;
					$phone->phone_number = $data[Self::$PRIMARY_CONTACT];
					$phone->party_id = $person->party_id;
					$phone->phone_type_id = $phoneTypes[0]->id;
					$phone->created_by = $data['authenticatedUser']->id;
					$phone->updated_by = $data['authenticatedUser']->id;
					$phone->save();
				}
				if (isset($data[Self::$SECONDARY_CONTACT])) {
					$phone = new Phone;
					$phone->phone_number = $data[Self::$SECONDARY_CONTACT];
					$phone->party_id = $person->party_id;
					$phone->phone_type_id = $phoneTypes[1]->id;
					$phone->created_by = $data['authenticatedUser']->id;
					$phone->updated_by = $data['authenticatedUser']->id;
					$phone->save();
				}
			}

			if ($count == 1) {
				if (isset($data[Self::$PRIMARY_CONTACT])) {
					$contact = $person->party->contacts[0];
					if ($contact->phone_number != $data[Self::$PRIMARY_CONTACT]) {
						$contact->phone_number = $data[Self::$PRIMARY_CONTACT];
						$contact->party_id = $person->party_id;
						$contact->phone_type_id = $phoneTypes[0]->id;
						$contact->created_by = $data['authenticatedUser']->id;
						$contact->updated_by = $data['authenticatedUser']->id;
						$contact->save();
					}
				}

				if (isset($data[Self::$SECONDARY_CONTACT])) {
					$phone = new Phone;
					$phone->phone_number = $data[Self::$SECONDARY_CONTACT];
					$phone->party_id = $person->party_id;
					$phone->phone_type_id = $phoneTypes[1]->id;
					$phone->created_by = $data['authenticatedUser']->id;
					$phone->updated_by = $data['authenticatedUser']->id;
					$phone->save();
				}
			}

			if ($count == 2) {
				if (isset($data[Self::$PRIMARY_CONTACT])) {
					$contact = $person->party->contacts[0];
					if ($contact->phone_number != $data[Self::$PRIMARY_CONTACT]) {
						$contact->phone_number = $data[Self::$PRIMARY_CONTACT];
						$contact->party_id = $person->party_id;
						$contact->phone_type_id = $phoneTypes[0]->id;
						$contact->created_by = $data['authenticatedUser']->id;
						$contact->updated_by = $data['authenticatedUser']->id;
						$contact->save();
					}
				}

				$contact = $person->party->contacts[1];
				if (isset($data[Self::$SECONDARY_CONTACT])) {
					if ($contact->phone_number != $data[Self::$SECONDARY_CONTACT]) {
						$contact->phone_number = $data[Self::$SECONDARY_CONTACT];
						$contact->party_id = $person->party_id;
						$contact->phone_type_id = $phoneTypes[1]->id;
						$contact->created_by = $data['authenticatedUser']->id;
						$contact->updated_by = $data['authenticatedUser']->id;
						$contact->save();
					}
				} else {
					$contact->deleted_by = $userId;
					$contact->save();
					$contact->delete();
				}
			}
			$user = User::where('party_id', $person['party_id'])->first();
			$user->update(['first_name' => $data['firstName'],
				'last_name' => $data['lastName'],
				'email' => $data['email'],
				//'personal_email' => $data['personalEmail'],
				//'userid' => $data['userid'],
				'updated_by' => $data['authenticatedUser']->id]);

			//updating user_attribute table
			//updating user_attribute table
			$attrValue = self::getAttrValue($data, 'user');
			UserAttribute::where('user_id', $user->id)->update(['attr_value' => $attrValue, 'updated_by' => $data['authenticatedUser']->id]);

// Setting as a Facilitator
			//commented by dhruvi
			// $role = Role::where('name', 'Facilitator')->first();
			// if (isset($data['facilitator_check'])) {

			// 	$role_user = RoleUser::where(['user_id' => $user->id, 'role_id' => $role['id']])->first();
			// 	if (!$role_user) {
			// 		$user->roles()->attach($role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
			// 		log::debug("Role Attached");

			// 		// detaching as participants
			// 		$participant_role = Role::where('name', 'Participant')->first();
			// 		$user->roles()->detach($participant_role['id']);
			// 	}
			// } elseif (!isset($data['facilitator_check'])) {

			// 	$role_user = RoleUser::where(['user_id' => $user->id, 'role_id' => $role['id']])->first();
			// 	if ($role_user) {
			// 		$user->roles()->detach($role['id']);
			// 		log::debug("Role Detached");

			// 		// Saving as participant
			// 		$participant_role = Role::where('name', 'Participant')->first();
			// 		$user->roles()->attach($participant_role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
			// 	}
			// }

			return $userId;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}

	public static function saveUser($data, $fileUpload = False, $fileImport = False) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data, &$fileUpload, &$fileImport) {
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['tenantPartyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedInUserPartyId'] = $userPrincipal->getUserPartyId();
			} else {
				$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;
			}

			$data['tenantPartyId'] = 1 ;// hard core by dhruvi
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$data['userPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "User");
			$data['companyPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "Company");
			if ($fileUpload) {
				$data['tempPath'] = "/temp/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . '/';
				$uploadInfo = Self::uploadFile($data);
			}
			if ($fileImport) {
				$data['tempPath'] = "/temp/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . "/userimports/photos/";
			}
$data['tenantPartyId'] = 1 ;// hard code by dhruvi
			$org = Organization::where(
				[
					['id', '=', $data['organization_name']],
					['tenant_party_id', '=', $data['tenantPartyId']],
				])->first();
			//dd($org);

			$org_party_id = $org['party_id'];
			$userId = $data['authenticatedUser']->id;

			$party = new Party;
			$party->party_type_id = $data['userPartyTypeId'];
			$party->created_by = $userId;
			$party->updated_by = $userId;
			$party->save();
			$party_id = $party->id;

			$person = new Person;
			$person->first_name = $data['firstName'];
			$person->last_name = $data['lastName'];
			$person->email = $data['email'];
			//$person->personal_email = $data['personalEmail'];
			$person->party_id = $party_id;

			$person->tenant_party_id = $data['tenantPartyId'];
			//if ($org['party_id'] != $org['tenant_party_id']) {
				$person->customer_party_id = $org_party_id;
			//}

			// $person->department = $data['department'];
			// $person->designation = $data['designation'];
			// $person->linkedin_profile = $data['linkedinProfile'];
			// $person->facebook_id = $data['facebookId'];
			// $person->twitter_handle = $data['twitterHandle'];
			// $person->google_profile = $data['googlePlus'];

			$person->created_by = $userId;
			$person->updated_by = $userId;
			//$person->employee_id = $data['employeeId'];

			if (isset($data['gender'])) {
				if ($data['gender'] == 'M') {
					$person->gender = 'M';
					$person->photo = '/tenants/1/general/user-male.png';
					$person->original_photo = '/tenants/1/general/user-male.png';
				} else {
					$person->gender = 'F';
					$person->photo = '/tenants/1/general/user-female.png';
					$person->original_photo = '/tenants/1/general/user-female.png';
				}
			} else {
				$person->photo = '/tenants/1/general/user-male.png';
				$person->original_photo = '/tenants/1/general/user-male.png';
			}

			// abhi -- 26/03/19
			if (isset($data['profile']) && $data['profile'] == 'private') {
				$person->is_private = 1;
			} else {
				$person->is_private = 0;
			}

			$person->save();

			if (isset($data['dob'])) {
				//
				$peopleAttribute = new PeopleAttribute;
				$peopleAttribute->people_id = $person->id;
				$peopleAttribute->attr_name = 'dob';

				$peopleAttribute->attr_value = $data['dob'];

				$peopleAttribute->created_by = $userId;
				$peopleAttribute->updated_by = $userId;
				$peopleAttribute->save();
			}

			$phoneTypes = $data['masterDataInterface']->list("PhoneType");
			$phoneTypeId = $phoneTypes[0]->id;
			//Save the phone numbers also
			if (isset($data[Self::$PRIMARY_CONTACT])) {
				$phone = new Phone;
				$phone->phone_number = $data[Self::$PRIMARY_CONTACT];
				$phone->party_id = $person->party_id;
				$phone->phone_type_id = $phoneTypes[0]->id;
				$phone->created_by = $userId;
				$phone->updated_by = $userId;
				$phone->save();
			}
			if (isset($data[Self::$SECONDARY_CONTACT])) {
				$phone = new Phone;
				$phone->phone_number = $data[Self::$SECONDARY_CONTACT];
				$phone->party_id = $person->party_id;
				$phone->phone_type_id = $phoneTypes[1]->id;
				$phone->created_by = $userId;
				$phone->updated_by = $userId;
				$phone->save();
			}

			$user = new User;
			$user->first_name = $data['firstName'];
			$user->last_name = $data['lastName'];
			//$user->userid = $data['userid'];
			$user->userid = $data['email'];
			//$user->personal_email = $data['personalEmail'];
			$user->email = $data['email'];
			// $pass = substr((string) $data[Self::$PRIMARY_CONTACT], -4) . '@cgx';
			// $pass = self::generatePassword($data[Self::$PRIMARY_CONTACT]);

			// abhi - 26/03/19
			$mobNo = $data[Self::$PRIMARY_CONTACT];

			//dd($org);
			$pass = self::generatePassword($mobNo, $data['dob'], $org->id, 0);
			// Log::error($pass);
			// Log::error('$pass');

			$user->password = bcrypt($pass);
			$user->party_id = $party_id;
			$user->created_by = $userId;
			$user->updated_by = $userId;
			$user->save();

			$attrValue = self::getAttrValue($data, 'user');
			$user_attribute = new UserAttribute;
			$user_attribute->user_id = $user->id;
			$user_attribute->attr_value = $attrValue;
			$user_attribute->created_by = $userId;
			$user_attribute->updated_by = $userId;
			$user_attribute->attr_name = 'profile_badge_count';
			$user_attribute->save();

			if ($fileUpload == True) {
				Log::debug("***********Final File Name**************");
				Log::debug($uploadInfo['uploadedFileNames']['photo'][0]);
				Log::debug($uploadInfo['originalFileNames']['photo'][0]);
				$data['finalPath'] = "/public/tenants/" . $data['tenantPartyId'] . "/user/" . $party_id . '/profiles/';
				$data['uploadedFileName'] = $uploadInfo['uploadedFileNames']['photo'][0];
				$success = Self::moveFile($data);
				if ($success == true) {
					$person->photo = str_replace("/public", "", $data['finalPath']) . $uploadInfo['uploadedFileNames']['photo'][0];
					$person->original_photo = $uploadInfo['originalFileNames']['photo'][0];
					$person->save();
				}
			}

			if ($fileImport == True) {
				Log::debug("***********Final File Name**************");
				$data['finalPath'] = "/public/tenants/" . $data['tenantPartyId'] . "/user/" . $party_id . '/profiles/';
				$data['uploadedFileName'] = $data['photoPath'];
				$success = Self::moveFile($data);
				if ($success == true) {
					$person->photo = $data['finalPath'] . $data['photoPath'];
					$person->original_photo = $data['photoPath'];
					$person->save();
				}
			}
			// Setting as facilitator
			$role = Role::all();

				$assigned_role = $role->where('id', $data['assign_role_id'])->first();
				$user->roles()->attach($assigned_role['id'], ['created_by' => $userId, 'updated_by' => $userId]);


			// if (isset($data['facilitator_check'])) {
			// 	$facilitator_role = $role->where('name', 'Facilitator')->first();
			// 	$user->roles()->attach($facilitator_role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
			// 	log::debug("Role Attached");
			// } else {
			// 	// Saving as participant
			// 	$participant_role = $role->where('name', 'Participant')->first();
			// 	$user->roles()->attach($participant_role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
			// }

			return $party_id;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}

	public static function verifyImportData($data, MessageBag $messageBag) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data, $messageBag) {
			$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
			$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;
			$data['tempPath'] = "temp/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . "/userimports/";
			$data['finalPath'] = "public/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . "/userimports/";

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$class = $data['configurations']['storage_implementation_class'];
			$storageUtility = new $class;

			$updateInfo = $storageUtility->uploadFileToPrivateStorage($data['request'], $data['tempPath']);
			$uploadedFile = $updateInfo['uploadedFileNames']['uploaded_file'][0];
			$uploadedFile_ogrName = $updateInfo['originalFileNames']['uploaded_file'][0];

			$fileName = $uploadedFile;
			$folder = storage_path() . "/app/" . $data['tempPath'];
			//$folder = $data['tempPath'];
			//checkig zip file
			$tem = explode(".", $uploadedFile);
			$len = count($tem);
			$checkZip = $tem[$len - 1];
			if ($checkZip == 'zip') {
				$zipFile = $folder . "/" . $uploadedFile;
				$excelFileName = Self::unzipFile($zipFile, $folder);
				//$fileName=str_replace("zip","xlsx", $uploadedFile_ogrName);
				//Log::debug("********Original file name********$fileName***");
				//Log::debug($fileName);
				//$fileName = "UserUpload_v1.1.xlsx";
				$fileName = $excelFileName;
			}

			if (isset($folder)) {
				Log::debug("**************FOLDER*************");
				Log::debug($folder);

				try {

					//carry out verification
					$excel = Excel::load($folder . '/' . $fileName)->all()->toArray();
					$importData = $excel[2];
					$organization_code = $excel[1][0]['customer_code'];
					// dump($organization_code);
					// exit();
					$org = Organization::where(
						[
							['organization_code', '=', $organization_code],
							['tenant_party_id', '=', $data['tenantPartyId']],
						])->firstOrFail();

					$validationRuleService = new ValidationRulesService($data['masterDataInterface'], $data['utilsInterface']);
					$keys = array();
					$validUsers = array();
					$invalidUsers = array();
					$manualErrors = array();
					$possibleDuplicateUserId = false;
					$possibleDuplicateEmailId = false;
					$possibleDuplicatePhotoPath = false;
					$isInvalid = false;
					//The following two are used to check if there are duplicate valies in the same file
					//Unique checks work only against records already present in the database
					//Hence we check for uniqueness in the file manually
					$userIdArray = array();
					$emailIdArray = array();
					$photoPathArray = array();
					if (!empty($importData)) {

						// abhi - 26/03/19
						$orgAttribute = OrganizationAttribute::where(['organization_id' => $org['id'], 'attr_name' => 'password_logic'])->first();
						$passLogic = PasswordLogic::where('id', $orgAttribute['attr_value'])->first();

						foreach ($importData as $key => $value) {
							$isInvalid = false;
							$messageBag = new MessageBag();
							$newUser = $value;
							$rules = [
								'first_name' => 'required|max:255',
								'last_name' => 'required|max:255',
								'userid' => 'required|max:20|unique:users,userid,$user,id,deleted_at,NULL',
								'designation' => 'required|max:500',
								'email' => 'required|max:1000|unique:people,email,$user,id,deleted_at,NULL,tenant_party_id,' . $data['tenantPartyId'],
//			                    'photo_path' => 'required',
							];

							// abhi - 26/03/19
							if ($passLogic['password_logic'] == 'Date Of Birth') {
								$rules['dob'] = 'required';
							}

							$validationResult = self::validateInputData($newUser, $rules);

							if (in_array($value["userid"], $userIdArray)) {
								//This means a record is existing with same user id in the current file which will ptentially fail
								//when the users are imported in the sstem
								$possibleDuplicateUserId = true;
								$messageBag->add('userid', 'Duplicate values of userid found in the file');
							}
							array_push($userIdArray, $value["userid"]);

							if (in_array($value["email"], $emailIdArray)) {
								//This means a record is existing with same user id in the current file which will ptentially fail
								//when the users are imported in the sstem
								$possibleDuplicateEmailId = true;
								$messageBag->add('email', 'Duplicate values of email found in the file');
							}
							array_push($emailIdArray, $value["email"]);

							if (in_array($value["photo_path"], $photoPathArray)) {
								//This means a record is existing with same user id in the current file which will ptentially fail
								//when the users are imported in the sstem
								$possibleDuplicatePhotoPath = true;
								$messageBag->add('photo_path', 'Same photo file applied to different users');
							}
							array_push($photoPathArray, $value["photo_path"]);

							if ($possibleDuplicateUserId or $possibleDuplicateEmailId) {
								$isInvalid = true;
							} else if ($validationResult['success'] == true) {
								$validUsers[] = $value;
								continue;
							}

							if ($validationResult['success'] == false) {
								if ($isInvalid == true) {
									foreach ($messageBag->getMessages() as $msgKey => $msgValue) {
										$validationResult['errors']->add($msgKey, $msgValue[0]);
									}
								}
								$value["errors"] = $validationResult['errors'];
								$invalidUsers[] = $value;
								foreach ($value as $key => $val) {
									if (array_key_exists($key, $keys)) {
									} else {
										array_push($keys, $key);
									}
								}
								continue;
							}

							//Finally records did not have errors with validations but internally the file had problems
							$value["errors"] = $messageBag;
							$invalidUsers[] = $value;
							foreach ($value as $key => $val) {
								if (array_key_exists($key, $keys)) {
								} else {
									array_push($keys, $key);
								}
							}

						}
					}

					if (count($invalidUsers) > 0) {
/*			        	dump($invalidUsers);
exit();*/
						//delete already uploaded files

						$storageUtility->deleteFile($folder . $fileName);
						$storageUtility->deleteFile($folder . $uploadedFile);
						$storageUtility->deleteDirectory($folder . 'photos');
						return $invalidUsers;
					}

					Log::debug("****IMPORT_JOB FOR USERIMPORT**************");
					$importJob = new ImportJob;
					$importJob->entity_name = 'Users';
					$importJob->original_input_file = $data['tempPath'] . $uploadedFile_ogrName;
					$importJob->uploaded_input_file = $data['tempPath'] . $uploadedFile;
					$importJob->status = 'IN_PROGRESS';
					$importJob->created_by = $data['authenticatedUser']->id;
					$importJob->updated_by = $data['authenticatedUser']->id;
					$importJob->save();
					$data['fileName'] = $fileName;
					$data['jobId'] = $importJob->id;
					//Self::sendEmail('UserImportStartEvent', $data);
					Log::debug("****UserUploadEvent Before Call**************");
					event(new UserUploadEvent($folder, $fileName, $data['authenticatedUser']->id, $importJob->id, $data['tempPath'], $data['finalPath'], $uploadedFile));
					Log::debug("UserUploadEvent Called");
					return 'SUCCESS';
				} catch (ModelNotFoundException $exception) {
					Log::error('Exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
					return "MODEL_NOT_FOUND";
				} catch (Exception $exception) {
					Log::error('Exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());

				} catch (\Throwable $exception) {
					Log::error('Throwable exception occurred in __CLASS__  :: __METHOD__ => ' . $exception->getMessage());
				}

			}

		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;

	}

	public static function importUsers($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
			$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
			$data['loggedInUserPartyId'] = $data['authenticatedUser']->party_id;
			$data['tempPath'] = "/temp/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . "/userimports/";
			$data['finalPath'] = "/public/tenants/" . $data['tenantPartyId'] . "/user/" . $data['loggedInUserPartyId'] . "/userimports/";

			//$updateInfo=FileUploadUtility::uploadFileToPublicStorage($data['request'], $data['tempPath']);
			//$updateInfo=FileUploadUtility::uploadFileToPrivateStorage($data['request'], $data['tempPath']);
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$class = $data['configurations']['storage_implementation_class'];
			$storageUtility = new $class;

			$updateInfo = $storageUtility->uploadFileToPrivateStorage($data['request'], $data['tempPath']);
			$uploadedFile = $updateInfo['uploadedFileNames']['uploaded_file'][0];
			$uploadedFile_ogrName = $updateInfo['originalFileNames']['uploaded_file'][0];

			$fileName = $uploadedFile;
			$folder = storage_path() . "/app/" . $data['tempPath'];
			//$folder = $data['tempPath'];
			//checkig zip file
			$tem = explode(".", $uploadedFile);
			$len = count($tem);
			$checkZip = $tem[$len - 1];
			if ($checkZip == 'zip') {
				$zipFile = $folder . "/" . $uploadedFile;
				$excelFileName = Self::unzipFile($zipFile, $folder);
				//$fileName=str_replace("zip","xlsx", $uploadedFile_ogrName);
				//Log::debug("********Original file name********$fileName***");
				//Log::debug($fileName);
				//$fileName = "UserUpload_v1.1.xlsx";
				$fileName = $excelFileName;
			}

			if (isset($folder)) {
				$importJob = new ImportJob;
				$importJob->entity_name = 'Users';
				$importJob->original_input_file = $data['tempPath'] . $uploadedFile_ogrName;
				$importJob->uploaded_input_file = $data['tempPath'] . $uploadedFile;
				$importJob->status = 'IN_PROGRESS';
				$importJob->created_by = $data['authenticatedUser']->id;
				$importJob->updated_by = $data['authenticatedUser']->id;
				$importJob->save();

				$data['fileName'] = $fileName;
				$data['jobId'] = $importJob->id;
				Self::sendEmail('UserImportStartEvent', $data);
				event(new UserUploadEvent($folder, $fileName, $data['authenticatedUser']->id, $importJob->id, $data['tempPath'], $data['finalPath'], $uploadedFile));
			}

		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
	}

	public static function sendEmail($eventName, $data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$authenticatedUser = $data['authenticatedUser'];
		$authenticatedUserParty = Party::findOrFail($authenticatedUser->party_id);
		$contacts = $authenticatedUserParty->contacts;
		if (session()->has('userPrincipal')) {
			$userPrincipal = session('userPrincipal');
			$tenant_code = $userPrincipal->getTenantCode();
			$roles = $userPrincipal->getRoles();
			foreach ($roles as $role) {
				if ($role == "Tenant Administrator") {
					$loginUserRole = $role;
				}
			}
		} else {
			$tenant_code = 'CGX';
			$loginUserRole = 'Tenant Administrator';
		}

		$login_id = $data['authenticatedUser']->id;

		if ($eventName == 'UserImportStartEvent') {
			$data[$loginUserRole][31] =
				['template_data' =>
				['LOGINUSER.FIRSTNAME' => $data['authenticatedUser']->first_name,
					'LOGINUSER.LASTNAME' => $data['authenticatedUser']->last_name,
					'USERIMPORT.FILENAME' => $data['fileName'],
					'USERIMPORT.JOBID' => $data['jobId'],
				],
				'email_id' => $data['authenticatedUser']->email, 'phone_no' => $contacts[0]->phone_number, 'party_id' => $data['authenticatedUser']->party_id];
		}

		if ($eventName == 'UserImportFinishEvent') {
			if (isset($data['errorFileName'])) {
				$data[$loginUserRole][31] =
					['template_data' =>
					['LOGINUSER.FIRSTNAME' => $data['authenticatedUser']->first_name,
						'LOGINUSER.LASTNAME' => $data['authenticatedUser']->last_name,
						'USERIMPORT.FILENAME' => $data['fileName'],
						'USERIMPORT.JOBID' => $data['jobId'],
						'USERIMPORT.TOTAL_RECORDS' => $data['totalRecords'],
						'USERIMPORT.IMPORTED_RECORDS' => $data['importedRecords'],
						'USERIMPORT.ERROR_RECORDS' => $data['errorRecords'],
					],
					'email_id' => $data['authenticatedUser']->email, 'phone_no' => $contacts[0]->phone_number, 'program_code' => '0', 'party_id' => $data['authenticatedUser']->party_id, 'customer_organization_code' => $data['organizationCode'], 'attachments' => array(storage_path() . "/app/" . $data['errorFileName'])];
			} else {
				$data[$loginUserRole][31] =
					['template_data' =>
					['LOGINUSER.FIRSTNAME' => $data['authenticatedUser']->first_name,
						'LOGINUSER.LASTNAME' => $data['authenticatedUser']->last_name,
						'USERIMPORT.FILENAME' => $data['fileName'],
						'USERIMPORT.JOBID' => $data['jobId'],
						'USERIMPORT.TOTAL_RECORDS' => $data['totalRecords'],
						'USERIMPORT.IMPORTED_RECORDS' => $data['importedRecords'],
						'USERIMPORT.ERROR_RECORDS' => $data['errorRecords'],
					],
					'email_id' => $data['authenticatedUser']->email, 'phone_no' => $contacts[0]->phone_number, 'program_code' => '0', 'party_id' => $data['authenticatedUser']->party_id, 'customer_organization_code' => $data['organizationCode']];
			}

		}

		if ($eventName == 'UserImportFailEvent') {

			$data[$loginUserRole][31] =
				['template_data' =>
				['LOGINUSER.FIRSTNAME' => $data['authenticatedUser']->first_name,
					'LOGINUSER.LASTNAME' => $data['authenticatedUser']->last_name,
					'USERIMPORT.FILENAME' => $data['fileName'],
					'USERIMPORT.JOBID' => $data['jobId'],
					'USERIMPORT.ERROR' => $data['errorMessage'],
				],
				'email_id' => $data['authenticatedUser']->email, 'phone_no' => $contacts[0]->phone_number, 'customer_organization_code' => $data['organizationCode'], 'party_id' => $data['authenticatedUser']->party_id];
		}
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		event(new CommEvent($eventName, $data, $tenant_code, $login_id));
	}

	public static function getAttrValue($data, $hint) {
		$i = 0;

		if ($hint == 'user') {
			$nonMandatoryFields = ['secondaryContact'];
		} 
		
		foreach ($nonMandatoryFields as $key => $value) {
			if ($data[$value]) {
				$i++;
			}
		}
		return $i;
	}

	public static function activateUsers($data) {

		//it may be unapprove and save
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$return_data = DB::transaction(function () use ($data) {

			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$data['loggedInUserId'] = $userPrincipal->getUserId();
			} else {
				$data['loggedInUserId'] = $data['authenticatedUser']->id;
			}

			if ($data['action'] == 'activate') {
				//
				$org_user = User::whereIn('id', $data['users'])->update(['is_active' => 1, 'updated_by' => $data['loggedInUserId']]);

			} elseif ($data['action'] == 'deactivate') {
				//
				$org_user = User::whereIn('id', $data['users'])->update(['is_active' => 0, 'updated_by' => $data['loggedInUserId']]);
			}

			return 'ok';
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $return_data;
	}

	public static function retrieveRoles() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return Role::get();
	}	
	public static function retrieveNonAdminRoles() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		$roles = DB::table('roles')
                 ->whereIn('id', [4,3,2])
				->get();
		return $roles;
	}	


// abhi - 26/03/19
	public static function generatePasswordDefaultLogic($mobileNo) {
		//
		$mobNo = (string) $mobileNo;
		$mobNoArray = str_split($mobNo);

		$i = 1;
		$pass = '';
		foreach ($mobNoArray as $key => $value) {
			if ($i < 8 && $i % 2 != 0) {
				$pass = $pass . '' . $value;
			}
			$i++;
		}
		return $pass;
	}

	public static function generatePassword($mobNo, $dob, $orgId = 0, $orgPasswordLogic = 0) {
		//
		// Log::error($orgId . ' ' . $mobNo . ' ' . $dob);
		if ($orgId) {
			//
			$orgPass = OrganizationAttribute::where(['organization_id' => $orgId, 'attr_name' => 'password_logic'])->first();
			$orgPasswordLogicId = $orgPass['attr_value'];
		} elseif ($orgPasswordLogic) {
			$orgPasswordLogicId = $orgPasswordLogic;
		}

		$passLogicObj = PasswordLogic::where('id', $orgPasswordLogicId)->first();
		$passLogic = $passLogicObj['password_logic'];
		// Log::error($passLogic);

		switch ($passLogic) {
		case 'Default Logic':
			//
			$password = self::generatePasswordDefaultLogic($mobNo);
			break;

		case 'Date Of Birth':
			//
			// $password=self::generatePasswordDOB($mobNo);
			$password = $dob;
			break;

		case 'Default Password':
			//
			$password = 'pass@123';
			break;

		case 'Mobile No':
			//
			$password = $mobNo;
			break;

		default:
			//
			$password = self::generatePasswordDefaultLogic($mobNo);
		}

		return $password;
	}

// FUNCTION OVER WRITE BY MOHIT
	private static function validateUserActive($person) {

		Log::error('in the validate user active function of user service');
		Log::error($person);
		// Log::error($person->party_id);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		// $person = $person->first();
		$userObject = User::where('party_id', $person->party_id)->first();
				Log::error($userObject);
		// exit();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		if ($userObject->first()->is_active == 1) {
			Log::error('in the if is_active is 1');
				return $userObject;
			// return false;
		} else {
			Log::error('in the else is_active is 0');
			Log::error($userObject);
			return false;
		}
	}
	public static function deleteUser($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {

			Log::error('in the delete function');
			Log::error($id);
			$person = Person::where('id', $id)->first();
			Log::error($person);

		 if($id == 1){
		 	return false;
		 }
			$user = self::validateUserActive($person);

			Log::error($user);
			// exit();
			if ($user) {
				$userId = auth()->user()->id;

				$userAttribute = UserAttribute::where('user_id', $user->first()->id);
				$userAttribute->update(['deleted_by' => $userId]);
				$userAttribute->delete();

				$user->update(['deleted_by' => $userId]);
				$user->delete();

				$person = Person::where('id', $id);
				$person->update(['deleted_by' => $userId]);
				$check = $person->delete();
			} else {
				$check = false;
			}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
}
