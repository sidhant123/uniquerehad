<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use App\Models\Address;
use App\Models\Customer;
use App\Models\Organization;
use App\Models\OrganizationAttribute;
use App\Models\Party;
use App\Models\PeopleAttribute;
use App\Models\Person;
use App\Models\RoleUser;
use App\Models\Phone;
use App\Models\UserAttribute;
use App\Services\PartyTypeService;
use App\Services\TenantService;
use App\User;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use vnnogile\Utilities\Services\ConstantsUtility;

class OrganizationService {
// function saveOnlyCompany() created by dhruvi 
// to insert data into only organization	
	public static function saveOnlyCompany($data, $fileUpload = False) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data, $fileUpload) {

			if (session()->has(ConstantsUtility::USERPRINCIPAL)) {
				$userPrincipal = session(ConstantsUtility::USERPRINCIPAL);
				$data['tenant_partyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedin_UserPartyId'] = $userPrincipal->getUserPartyId();
				$userId = $userPrincipal->getUserId();
			} else {
				$data['tenant_partyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedin_UserPartyId'] = $data['authenticatedUser']->party_id;
				$userId = $data['authenticatedUser']->id;
			}
$data['tenant_partyId'] = 1 ; // HARD CODE 1 BY DHRUVI
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			Log::debug("User ID :: " . $userId);
			// Log::debug($phoneTypeMap);
			$data['userPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "User");
			$data['companyPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "Company");

			$data['temp_path'] = "/tenants/" . $data['tenant_partyId'] . "/user/" . $data['loggedin_UserPartyId'] . "/temp/";
			$data['storage_path'] = "/tenants/" . $data['tenant_partyId'];

			if ($fileUpload == True) {
				$uploadInfo = Self::fileUpload($data);
				Log::debug($uploadInfo);
			}

			$orgPartyTypeId = $data['companyPartyTypeId'];

			$orgParty = new Party;
			$orgParty->party_type_id = $data['companyPartyTypeId'];;
			$orgParty->created_by = $userId;
			$orgParty->updated_by = $userId;
			$orgParty->save();
			$orgParty_id = $orgParty->id;

			$organization = new Organization;
			$organization->name = $data['organization_name'];
			$organization->website_url = $data['organization_url'];
			$organization->email = $data['email'];
			$organization->party_id = $orgParty_id;
			$organization->tenant_party_id = $data['tenant_partyId'];
			$organization->created_by = $userId;
			$organization->updated_by = $userId;
			$organization->organization_code = $data['organization_code'];

			if (isset($uploadInfo['uploadedFileNames']['organization_logo'])) {
				$stored_path = Self::moveLogo($uploadInfo, $data, $orgParty_id);
				$organization->logo = $stored_path['org_logo'];
				$organization->original_logo = $stored_path['org_logo_name'];
			}

			// abhi
			// $organization->tag_line = $data['tag_line'];

			$organization->save();
			$organizationId = $organization->id;

			$customer = new Customer;
			$customer->party_id = $orgParty_id;
			$customer->customer_code = $data['organization_code'];
			$customer->tenant_party_id = $data['tenant_partyId'];
			$customer->created_by = $userId;
			$customer->updated_by = $userId;
			$customer->save();

			$address_type = $data['masterDataInterface']->filteredList('AddressType', ['name' => 'POSTAL_ADDRESS'], "POSTAL_ADDRESS")[0];
			//$address_type=AddressType::where('name', 'POSTAL_ADDRESS')->first();
			$address_typeId = $address_type['id'];

			if (isset($data['organization_address1'])) {
				$address = new Address;
				$address->address1 = $data['organization_address1'];
				$address->address2 = $data['organization_address2'];
				$address->address3 = $data['organization_address3'];
				$address->address_type_id = $address_typeId;
				$address->party_id = $orgParty_id;

				$address->country_id = $data['organization_country'];
				$address->state_id = $data['organization_state'];
				$address->city_id = $data['organization_city'];
				$address->pincode_id = $data['organization_pincode'];
				$address->created_by = $userId;
				$address->updated_by = $userId;

				$address->save();
				$addressId = $address->id;
			}
			Log::debug($addressId);
			// $orgAttribute = new OrganizationAttribute;
			// $orgAttribute->organization_id = $organizationId;
			// $orgAttribute->attr_name = 'comm_option';
			// $orgAttribute->attr_value = $data['comm_option'];
			// $orgAttribute->created_by = $userId;
			// $orgAttribute->updated_by = $userId;
			// $orgAttribute->save();

			$orgAttribute = new OrganizationAttribute;
			$orgAttribute->organization_id = $organizationId;
			$orgAttribute->attr_name = 'password_logic';
			$orgAttribute->attr_value = $data['pass_logic'];
			$orgAttribute->created_by = $userId;
			$orgAttribute->updated_by = $userId;
			$orgAttribute->save();

			$phoneTypes = $data['masterDataInterface']->list("PhoneType");
			if (isset($data['organization_contact'])) {
				$phone = new Phone;
				$phone->phone_number = $data['organization_contact'];
				$phone->party_id = $orgParty_id;
				$phone->phone_type_id = $phoneTypes[0]->id;
				$phone->party_id = $orgParty_id;
				$phone->created_by = $userId;
				$phone->updated_by = $userId;
				$phone->save();
			}


			return $organizationId;

		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	public static function saveOrganization($data, $fileUpload = False) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data, $fileUpload) {

			if (session()->has(ConstantsUtility::USERPRINCIPAL)) {
				$userPrincipal = session(ConstantsUtility::USERPRINCIPAL);
				$data['tenant_partyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedin_UserPartyId'] = $userPrincipal->getUserPartyId();
				$userId = $userPrincipal->getUserId();
			} else {
				$data['tenant_partyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedin_UserPartyId'] = $data['authenticatedUser']->party_id;
				$userId = $data['authenticatedUser']->id;
			}
$data['tenant_partyId'] = 1 ; // HARD CODE 1 BY DHRUVI
			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			Log::debug("User ID :: " . $userId);
			// Log::debug($phoneTypeMap);
			$data['userPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "User");
			$data['companyPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "Company");

			$data['temp_path'] = "/tenants/" . $data['tenant_partyId'] . "/user/" . $data['loggedin_UserPartyId'] . "/temp/";
			$data['storage_path'] = "/tenants/" . $data['tenant_partyId'];

			if ($fileUpload == True) {
				$uploadInfo = Self::fileUpload($data);
				Log::debug($uploadInfo);
			}

			$orgPartyTypeId = $data['companyPartyTypeId'];

			$orgParty = new Party;
			$orgParty->party_type_id = $data['companyPartyTypeId'];;
			$orgParty->created_by = $userId;
			$orgParty->updated_by = $userId;
			$orgParty->save();
			$orgParty_id = $orgParty->id;

			$organization = new Organization;
			$organization->name = $data['organization_name'];
			$organization->website_url = $data['organization_url'];
			$organization->email = $data['email'];
			$organization->party_id = $orgParty_id;
			$organization->tenant_party_id = $data['tenant_partyId'];
			$organization->created_by = $userId;
			$organization->updated_by = $userId;
			$organization->organization_code = $data['organization_code'];

			if (isset($uploadInfo['uploadedFileNames']['organization_logo'])) {
				$stored_path = Self::moveLogo($uploadInfo, $data, $orgParty_id);
				$organization->logo = $stored_path['org_logo'];
				$organization->original_logo = $stored_path['org_logo_name'];
			}

			// abhi
			//$organization->tag_line = $data['tag_line'];

			$organization->save();
			$organizationId = $organization->id;

			// abhi - 26/03/19
			// $orgAttribute = new OrganizationAttribute;
			// $orgAttribute->organization_id = $organizationId;
			// $orgAttribute->attr_name = 'comm_option';
			// $orgAttribute->attr_value = $data['comm_option'];
			// $orgAttribute->created_by = $userId;
			// $orgAttribute->updated_by = $userId;
			// $orgAttribute->save();

			$orgAttribute = new OrganizationAttribute;
			$orgAttribute->organization_id = $organizationId;
			$orgAttribute->attr_name = 'password_logic';
			$orgAttribute->attr_value = $data['pass_logic'];
			$orgAttribute->created_by = $userId;
			$orgAttribute->updated_by = $userId;
			$orgAttribute->save();

			$customer = new Customer;
			$customer->party_id = $orgParty_id;
			$customer->customer_code = $data['organization_code'];
			$customer->tenant_party_id = $data['tenant_partyId'];
			$customer->created_by = $userId;
			$customer->updated_by = $userId;
			$customer->save();

			$address_type = $data['masterDataInterface']->filteredList('AddressType', ['name' => 'POSTAL_ADDRESS'], "POSTAL_ADDRESS")[0];
			//$address_type=AddressType::where('name', 'POSTAL_ADDRESS')->first();
			$address_typeId = $address_type['id'];

			if (isset($data['organization_address1'])) {
				$address = new Address;
				$address->address1 = $data['organization_address1'];
				$address->address2 = $data['organization_address2'];
				$address->address3 = $data['organization_address3'];
				$address->address_type_id = $address_typeId;
				$address->party_id = $orgParty_id;

				$address->country_id = $data['organization_country'];
				$address->state_id = $data['organization_state'];
				$address->city_id = $data['organization_city'];
				$address->pincode_id = $data['organization_pincode'];
				$address->created_by = $userId;
				$address->updated_by = $userId;

				$address->save();
				$addressId = $address->id;
			}
			Log::debug($addressId);

			$phoneTypes = $data['masterDataInterface']->list("PhoneType");
			if (isset($data['organization_contact'])) {
				$phone = new Phone;
				$phone->phone_number = $data['organization_contact'];
				$phone->party_id = $orgParty_id;
				$phone->phone_type_id = $phoneTypes[0]->id;
				$phone->party_id = $orgParty_id;
				$phone->created_by = $userId;
				$phone->updated_by = $userId;
				$phone->save();
			}

			//pm
			$pmParty = new Party;
			$pmParty->party_type_id = $data['userPartyTypeId'];
			$pmParty->created_by = $userId;
			$pmParty->updated_by = $userId;
			$pmParty->save();
			$pmParty_id = $pmParty->id;

			$programManager = new Person;
			$programManager->first_name = $data['pm_firstName'];
			$programManager->last_name = $data['pm_lastName'];
			$programManager->email = $data['pm_email'];
			$programManager->party_id = $pmParty_id;
			$programManager->tenant_party_id = $data['tenant_partyId'];
			$programManager->customer_party_id = $orgParty_id;

			$programManager->department = $data['pm_department'];
			$programManager->designation = $data['pm_designation'];
			$programManager->linkedin_profile = $data['pm_linkedinProfile'];
			$programManager->facebook_id = $data['pm_fbProfile'];
			$programManager->twitter_handle = $data['pm_twitterHandle'];

			$programManager->created_by = $userId;
			$programManager->updated_by = $userId;
			$programManager->employee_id = $data['pm_employeeId'];

			if (isset($data['pm_gender'])) {
				if ($data['pm_gender'] == 'M') {
					$programManager->gender = 'M';
				} else {
					$programManager->gender = 'F';
				}
			}

			if (isset($uploadInfo['uploadedFileNames']['pm_photo'])) {
				$stored_path = Self::movePmPhoto($uploadInfo, $data, $pmParty_id);
				$programManager->photo = $stored_path['pm_photo'];
				$programManager->original_photo = $stored_path['pm_photo_name'];
			}
			$programManager->personal_email = $data['pm_personalEmail'];
			$programManager->google_profile = $data['pm_googleId'];

			// abhi -26/03/19
			if (isset($data['pm_profile']) && $data['pm_profile'] == 'private') {
				$programManager->is_private = 1;
			} else {
				$programManager->is_private = 0;
			}

			$programManager->save();

			if (isset($data['pm_dob'])) {
				//
				$peopleAttribute = new PeopleAttribute;
				$peopleAttribute->people_id = $programManager->id;
				$peopleAttribute->attr_name = 'dob';

				$peopleAttribute->attr_value = $data['pm_dob'];

				$peopleAttribute->created_by = $userId;
				$peopleAttribute->updated_by = $userId;
				$peopleAttribute->save();
			}

			if (isset($data['pm_primaryContact'])) {
				$phone = new Phone;
				$phone->phone_number = $data['pm_primaryContact'];
				$phone->party_id = $pmParty_id;
				$phone->phone_type_id = $phoneTypes[0]->id;
				$phone->created_by = $userId;
				$phone->updated_by = $userId;
				$phone->save();
			}

			if (isset($data['pm_secondaryContact'])) {
				$phone = new Phone;
				$phone->phone_number = $data['pm_secondaryContact'];
				$phone->party_id = $pmParty_id;
				$phone->phone_type_id = $phoneTypes[1]->id;
				$phone->created_by = $userId;
				$phone->updated_by = $userId;
				$phone->save();
			}

			$pmUser = new User;
			$pmUser->first_name = $data['pm_firstName'];
			$pmUser->last_name = $data['pm_lastName'];
			$pmUser->email = $data['pm_email'];
			// $pass = substr((string) $data['pm_primaryContact'], -4) . '@cgx';
			// $pass = UserService::generatePassword($data['pm_primaryContact']);

			// abhi - 26/03/19
			$mobNo = $data['pm_primaryContact'];
			$pass = UserService::generatePassword($mobNo, $data['pm_dob'], 0, $data['pass_logic']);
			Log::error($pass);

			$pmUser->password = bcrypt($pass);
			$pmUser->party_id = $pmParty_id;
			$pmUser->created_by = $userId;
			$pmUser->updated_by = $userId;
			$pmUser->userid = $data['pm_userId'];
			$pmUser->personal_email = $data['pm_personalEmail'];
			$pmUser->save();
			$pm_user_id = $pmUser->id;
			log::debug("PM User ID " . $pm_user_id);

			$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Program Manager'], "Program Manager")[0];
			//$role= Role::where('name', 'Program Manager')->first();
			log::debug("Role");
			log::debug($role);
			log::debug($userId);
			//$pmUser = User::find($pm_user_id);
			$pmUser->roles()->attach($role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
			log::debug("Role Attached");

			// user_attribute table
			$attrValue = UserService::getAttrValue($data, 'pmUser');
			$user_attribute = new UserAttribute;
			$user_attribute->user_id = $pmUser->id;
			$user_attribute->attr_value = $attrValue;
			$user_attribute->created_by = $userId;
			$user_attribute->updated_by = $userId;
			$user_attribute->attr_name = 'profile_badge_count';
			$user_attribute->save();

			$ccPartyTypeId = $data['userPartyTypeId'];
			if (!isset($data['sameUser'])) {
				$ccParty = new Party;
				$ccParty->party_type_id = $ccPartyTypeId;
				$ccParty->created_by = $userId;
				$ccParty->updated_by = $userId;
				$ccParty->save();
				$ccParty_id = $ccParty->id;
				log::debug("CCParty created");

				$companyCoordinator = new Person;
				$companyCoordinator->first_name = $data['cc_firstName'];
				$companyCoordinator->last_name = $data['cc_lastName'];
				$companyCoordinator->email = $data['cc_email'];
				$companyCoordinator->party_id = $ccParty_id;
				$companyCoordinator->tenant_party_id = $data['tenant_partyId'];
				$companyCoordinator->customer_party_id = $orgParty_id;

				$companyCoordinator->department = $data['cc_department'];
				$companyCoordinator->designation = $data['cc_designation'];
				$companyCoordinator->linkedin_profile = $data['cc_linkedinProfile'];
				$companyCoordinator->facebook_id = $data['cc_fbProfile'];
				$companyCoordinator->twitter_handle = $data['cc_twitterHandle'];

				$companyCoordinator->created_by = $userId;
				$companyCoordinator->updated_by = $userId;
				$companyCoordinator->employee_id = $data['cc_employeeId'];

				if (isset($data['cc_gender'])) {
					if ($data['cc_gender'] == 'M') {
						$companyCoordinator->gender = 'M';
					} else {
						$companyCoordinator->gender = 'F';
					}
				}

				if (isset($uploadInfo['uploadedFileNames']['cc_photo'])) {
					$stored_path = Self::moveCcPhoto($uploadInfo, $data, $ccParty_id);
					$companyCoordinator->photo = $stored_path['cc_photo'];
					$companyCoordinator->original_photo = $stored_path['cc_photo_name'];
				}
				$companyCoordinator->personal_email = $data['cc_personalEmail'];
				$companyCoordinator->google_profile = $data['cc_googleId'];

				// abhi -26/03/19
				if (isset($data['cc_profile']) && $data['cc_profile'] == 'private') {
					$companyCoordinator->is_private = 1;
				} else {
					$companyCoordinator->is_private = 0;
				}

				$companyCoordinator->save();
				log::debug("CC Person created");

				if (isset($data['cc_dob'])) {
					//
					$peopleAttribute = new PeopleAttribute;
					$peopleAttribute->people_id = $companyCoordinator->id;
					$peopleAttribute->attr_name = 'dob';

					$peopleAttribute->attr_value = $data['cc_dob'];

					$peopleAttribute->created_by = $userId;
					$peopleAttribute->updated_by = $userId;
					$peopleAttribute->save();
				}

				//TBD $phoneType =PhoneType::where('name', 'Mobile Phone (Work)')->first();
				//TBD $phoneTypeId = $phoneType['id'];

				if (isset($data['cc_primaryContact'])) {
					$phone = new Phone;
					$phone->phone_number = $data['cc_primaryContact'];
					$phone->party_id = $ccParty_id;
					$phone->phone_type_id = $phoneTypes[0]->id;
					$phone->created_by = $userId;
					$phone->updated_by = $userId;
					$phone->save();
					log::debug("CC Phone 1 created");
				}

				//TBD $phoneType =PhoneType::where('name', 'Mobile Phone (Home)')->first();
				//TBD $phoneTypeId = $phoneType['id'];
				if (isset($data['cc_secondaryContact'])) {
					$phone = new Phone;
					$phone->phone_number = $data['cc_secondaryContact'];
					$phone->party_id = $ccParty_id;
					$phone->phone_type_id = $phoneTypes[1]->id;
					$phone->created_by = $userId;
					$phone->updated_by = $userId;
					$phone->save();
					log::debug("CC Phone 2 created");
				}

				$ccUser = new User;
				$ccUser->first_name = $data['cc_firstName'];
				$ccUser->last_name = $data['cc_lastName'];
				$ccUser->email = $data['cc_email'];
				// $pass = substr((string) $data['cc_primaryContact'], -4) . '@cgx';
				// $pass = UserService::generatePassword($data['cc_primaryContact']);

				// abhi - password
				$mobNo = $data['cc_primaryContact'];
				$pass = UserService::generatePassword($mobNo, $data['cc_dob'], 0, $data['pass_logic']);
				Log::error($pass);

				$ccUser->password = bcrypt($pass);
				$ccUser->party_id = $ccParty_id;
				$ccUser->created_by = $userId;
				$ccUser->updated_by = $userId;
				$ccUser->userid = $data['cc_userId'];
				$ccUser->personal_email = $data['cc_personalEmail'];
				$ccUser->save();
				log::debug("CC User created");
				log::debug($ccUser->id);

				$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Company Coordinator'], "Company Coordinator")[0];
				$ccUser->roles()->attach($role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
				Log::debug("Role attached for CCUser in if condition");

				// user_attribute table
				$attrValue = UserService::getAttrValue($data, 'ccUser');
				$user_attribute = new UserAttribute;
				$user_attribute->user_id = $ccUser->id;
				$user_attribute->attr_value = $attrValue;
				$user_attribute->created_by = $userId;
				$user_attribute->updated_by = $userId;
				$user_attribute->attr_name = 'profile_badge_count';
				$user_attribute->save();

			} else {
				$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Company Coordinator'], "Company Coordinator")[0];
				//$role= Role::where('name', 'Company Coordinator')->first();
				$pmUser->roles()->attach($role['id'], ['created_by' => $userId, 'updated_by' => $userId]);
				Log::debug("Role attached for CCUser in else condition");
				//$pmUser->roles()->attach($role['id']);
			}

			return $organizationId;

		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}



	public static function retrieveOrganization() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Organization::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}
	public static function retrieveOrganizationForFilter() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Organization::select('party_id as id', 'name')->get();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

	public static function updateOrganization($id, $data, $fileUpload = False) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

		$check = DB::transaction(function () use ($data, $id, $fileUpload) {

			if (session()->has(ConstantsUtility::USERPRINCIPAL)) {
				$userPrincipal = session(ConstantsUtility::USERPRINCIPAL);
				$data['tenant_partyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedin_UserPartyId'] = $userPrincipal->getUserPartyId();
				$userId = $userPrincipal->getUserId();
			} else {
				$data['tenant_partyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedin_UserPartyId'] = $data['authenticatedUser']->party_id;
				$userId = $data['authenticatedUser']->id;
			}

			//Get Phone Types and Configuration Data
			// $phoneTypes = $data['masterDataInterface']->list("PhoneType");
			// foreach ($phoneTypes as $phoneType) {
			// 	//We need to look up id based on name
			// 	$phoneTypeMap[$phoneType->name] = $phoneType->id;
			// }

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$data['userPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "User");
			$data['companyPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "Company");

			$data['temp_path'] = "/tenants/" . $data['tenant_partyId'] . "/user/" . $data['loggedin_UserPartyId'] . "/temp/";
			$data['storage_path'] = "/tenants/" . $data['tenant_partyId'];

			$orgParty_id = $data['organization_party_id'];

			if ($fileUpload == True) {
				$uploadInfo = Self::fileUpload($data);
				Log::debug($uploadInfo);
			}

			if (isset($uploadInfo['uploadedFileNames']['organization_logo'])) {
				$stored_path = Self::moveLogo($uploadInfo, $data, $orgParty_id);
				Organization::where('id', $id)->update(['logo' => $stored_path['org_logo'], 'original_logo' => $stored_path['org_logo_name']]);
			}

			//abhi
			$organizationId = Organization::where('id', $id)->update(['name' => $data['organization_name'], 'website_url' => $data['organization_url'], 'email' => $data['email'], 'organization_code' => $data['organization_code'], 'updated_by' => $userId]);

			// abhi - 26/03/19
			// org attribute
			// $orgAttribute = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'comm_option'])->first();
			// if ($orgAttribute) {
			// 	$orgAttribute->attr_value = $data['comm_option'];
			// 	$orgAttribute->updated_by = $userId;
			// 	$orgAttribute->save();

			// 	// } elseif (!$orgAttribute) {
			// } else {
			// 	//
			// 	$orgAttribute = new OrganizationAttribute;
			// 	$orgAttribute->organization_id = $id;
			// 	$orgAttribute->attr_name = 'comm_option';
			// 	$orgAttribute->attr_value = $data['comm_option'];
			// 	$orgAttribute->created_by = $userId;
			// 	$orgAttribute->updated_by = $userId;
			// 	$orgAttribute->save();
			// }

			$orgAttribute1 = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'password_logic'])->first();
			if ($orgAttribute1) {

				$orgAttribute1->attr_value = $data['pass_logic'];
				$orgAttribute1->updated_by = $userId;
				$orgAttribute1->save();

				// } elseif (!$orgAttribute1) {
			} else {
				//
				$orgAttribute = new OrganizationAttribute;
				$orgAttribute->organization_id = $id;
				$orgAttribute->attr_name = 'password_logic';
				$orgAttribute->attr_value = $data['pass_logic'];
				$orgAttribute->created_by = $userId;
				$orgAttribute->updated_by = $userId;
				$orgAttribute->save();
			}

			$customerId = Customer::where('party_id', $data['organization_party_id'])->update(['customer_code' => $data['organization_code'], 'updated_by' => $userId]);
			$address_type = $data['masterDataInterface']->filteredList('AddressType', ['name' => 'POSTAL_ADDRESS'], "POSTAL_ADDRESS")[0];
			//$address_type=AddressType::where('name', 'POSTAL_ADDRESS')->first();
			$address_typeId = $address_type['id'];
			$address = Address::where('party_id', $data['organization_party_id'])->first();
			$phoneTypes = $data['masterDataInterface']->list("PhoneType");
			if (isset($data['organization_address1'])) {
				$address->address1 = $data['organization_address1'];
				$address->address2 = $data['organization_address2'];
				$address->address3 = $data['organization_address3'];
				$address->country_id = $data['organization_country'];
				$address->state_id = $data['organization_state'];
				$address->city_id = $data['organization_city'];
				$address->pincode_id = $data['organization_pincode'];
				$address->updated_by = $userId;
				$address->save();
				// update phone ->later
				$phone = Phone::where('party_id', $data['organization_party_id'])->first();
				if (empty($phone)) {
					if (isset($data['organization_contact'])) {
						$phone = new Phone;
						$phone->phone_number = $data['organization_contact'];
						$phone->party_id = $data['organization_party_id'];
						$phone->phone_type_id = $phoneTypes[0]->id;
						$phone->created_by = $userId;
						$phone->updated_by = $userId;
						$phone->save();
					}
				} else {
					$phone->phone_number = $data['organization_contact'];
					$phone->updated_by = $userId;
					$phone->save();
				}
			

			Log::debug("OrganizationService:: finished");
			return $organizationId;
}
		});

		Log::debug("OrganizationService::updateOrganization finished");
		return $check;
	}


	public static function updateOrganizationOLD($id, $data, $fileUpload = False) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

		$check = DB::transaction(function () use ($data, $id, $fileUpload) {

			if (session()->has(ConstantsUtility::USERPRINCIPAL)) {
				$userPrincipal = session(ConstantsUtility::USERPRINCIPAL);
				$data['tenant_partyId'] = $userPrincipal->getTenantPartyId();
				$data['loggedin_UserPartyId'] = $userPrincipal->getUserPartyId();
				$userId = $userPrincipal->getUserId();
			} else {
				$data['tenant_partyId'] = TenantService::getTenantPartyIdForAuthUser($data['utilsInterface'], $data['authenticatedUser']);
				$data['loggedin_UserPartyId'] = $data['authenticatedUser']->party_id;
				$userId = $data['authenticatedUser']->id;
			}

			//Get Phone Types and Configuration Data
			// $phoneTypes = $data['masterDataInterface']->list("PhoneType");
			// foreach ($phoneTypes as $phoneType) {
			// 	//We need to look up id based on name
			// 	$phoneTypeMap[$phoneType->name] = $phoneType->id;
			// }

			$configurations = $data['masterDataInterface']->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;

			$data['userPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "User");
			$data['companyPartyTypeId'] = PartyTypeService::getPartyTypeId($data['masterDataInterface'], "Company");

			$data['temp_path'] = "/tenants/" . $data['tenant_partyId'] . "/user/" . $data['loggedin_UserPartyId'] . "/temp/";
			$data['storage_path'] = "/tenants/" . $data['tenant_partyId'];

			$orgParty_id = $data['organization_party_id'];

			if ($fileUpload == True) {
				$uploadInfo = Self::fileUpload($data);
				Log::debug($uploadInfo);
			}

			if (isset($uploadInfo['uploadedFileNames']['organization_logo'])) {
				$stored_path = Self::moveLogo($uploadInfo, $data, $orgParty_id);
				Organization::where('id', $id)->update(['logo' => $stored_path['org_logo'], 'original_logo' => $stored_path['org_logo_name']]);
			}

			//abhi
			$organizationId = Organization::where('id', $id)->update(['name' => $data['organization_name'], 'website_url' => $data['organization_url'], 'email' => $data['email'], 'organization_code' => $data['organization_code'],  'updated_by' => $userId]);

			// abhi - 26/03/19
			// org attribute
			// $orgAttribute = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'comm_option'])->first();
			// if ($orgAttribute) {
			// 	$orgAttribute->attr_value = $data['comm_option'];
			// 	$orgAttribute->updated_by = $userId;
			// 	$orgAttribute->save();

			// 	// } elseif (!$orgAttribute) {
			// } else {
			// 	//
			// 	$orgAttribute = new OrganizationAttribute;
			// 	$orgAttribute->organization_id = $id;
			// 	$orgAttribute->attr_name = 'comm_option';
			// 	$orgAttribute->attr_value = $data['comm_option'];
			// 	$orgAttribute->created_by = $userId;
			// 	$orgAttribute->updated_by = $userId;
			// 	$orgAttribute->save();
			// }

			$orgAttribute1 = OrganizationAttribute::where(['organization_id' => $id, 'attr_name' => 'password_logic'])->first();
			if ($orgAttribute1) {

				$orgAttribute1->attr_value = $data['pass_logic'];
				$orgAttribute1->updated_by = $userId;
				$orgAttribute1->save();

				// } elseif (!$orgAttribute1) {
			} else {
				//
				$orgAttribute = new OrganizationAttribute;
				$orgAttribute->organization_id = $id;
				$orgAttribute->attr_name = 'password_logic';
				$orgAttribute->attr_value = $data['pass_logic'];
				$orgAttribute->created_by = $userId;
				$orgAttribute->updated_by = $userId;
				$orgAttribute->save();
			}

			$customerId = Customer::where('party_id', $data['organization_party_id'])->update(['customer_code' => $data['organization_code'], 'updated_by' => $userId]);
			$address_type = $data['masterDataInterface']->filteredList('AddressType', ['name' => 'POSTAL_ADDRESS'], "POSTAL_ADDRESS")[0];
			//$address_type=AddressType::where('name', 'POSTAL_ADDRESS')->first();
			$address_typeId = $address_type['id'];
			$address = Address::where('party_id', $data['organization_party_id'])->first();
			$phoneTypes = $data['masterDataInterface']->list("PhoneType");
			if (isset($data['organization_address1'])) {
				$address->address1 = $data['organization_address1'];
				$address->address2 = $data['organization_address2'];
				$address->address3 = $data['organization_address3'];
				$address->country_id = $data['organization_country'];
				$address->state_id = $data['organization_state'];
				$address->city_id = $data['organization_city'];
				$address->pincode_id = $data['organization_pincode'];
				$address->updated_by = $userId;
				$address->save();
				// update phone ->later
				$phone = Phone::where('party_id', $data['organization_party_id'])->first();
				if (empty($phone)) {
					if (isset($data['organization_contact'])) {
						$phone = new Phone;
						$phone->phone_number = $data['organization_contact'];
						$phone->party_id = $data['organization_party_id'];
						$phone->phone_type_id = $phoneTypes[0]->id;
						$phone->created_by = $userId;
						$phone->updated_by = $userId;
						$phone->save();
					}
				} else {
					$phone->phone_number = $data['organization_contact'];
					$phone->updated_by = $userId;
					$phone->save();
				}
			}

			//update user

			if (isset($data['pmDetach'])) {
				$pmPartyTypeId = $data['userPartyTypeId'];
				if (!is_numeric($data['pm_firstName'])) {
					$pmParty = new Party;
					$pmParty->party_type_id = $pmPartyTypeId;
					$pmParty->created_by = $userId;
					$pmParty->updated_by = $userId;
					$pmParty->save();
					$pmParty_id = $pmParty->id;

					$programManager = new Person;
					$programManager->first_name = $data['pm_firstName'];
					$programManager->last_name = $data['pm_lastName'];
					$programManager->email = $data['pm_email'];
					$programManager->party_id = $pmParty_id;
					$programManager->tenant_party_id = $data['tenant_partyId'];
					$programManager->customer_party_id = $data['organization_party_id'];

					$programManager->department = $data['pm_department'];
					$programManager->designation = $data['pm_designation'];
					$programManager->linkedin_profile = $data['pm_linkedinProfile'];
					$programManager->facebook_id = $data['pm_fbProfile'];
					$programManager->twitter_handle = $data['pm_twitterHandle'];

					$programManager->created_by = $userId;
					$programManager->updated_by = $userId;
					$programManager->employee_id = $data['pm_employeeId'];

					if (isset($data['pm_gender'])) {
						if ($data['pm_gender'] == 'M') {
							$programManager->gender = 'M';
						} else {
							$programManager->gender = 'F';
						}
					}

					if (isset($uploadInfo['uploadedFileNames']['pm_photo'])) {
						$stored_path = Self::movePmPhoto($uploadInfo, $data, $pmParty_id);
						$programManager->photo = $stored_path['pm_photo'];
						$programManager->original_photo = $stored_path['pm_photo_name'];
					}
					$programManager->personal_email = $data['pm_personalEmail'];
					$programManager->google_profile = $data['pm_googleId'];

					// abhi - 26/03/19
					// abhi
					if (isset($data['pm_profile']) && $data['pm_profile'] == 'private') {
						$programManager->is_private = 1;
					} else {
						$programManager->is_private = 0;
					}

					$programManager->save();

					if (isset($data['pm_dob'])) {
						//
						$peopleAttribute = new PeopleAttribute;
						$peopleAttribute->people_id = $programManager->id;
						$peopleAttribute->attr_name = 'dob';

						$peopleAttribute->attr_value = $data['pm_dob'];

						$peopleAttribute->created_by = $userId;
						$peopleAttribute->updated_by = $userId;
						$peopleAttribute->save();
					}

					//TBD $phoneType =PhoneType::where('name', 'Mobile Phone (Work)')->first();
					//TBD $phoneTypeId = $phoneType['id'];

					//Save the phone numbers also
					if (isset($data['pm_primaryContact'])) {
						$phone = new Phone;
						$phone->phone_number = $data['pm_primaryContact'];
						$phone->party_id = $pmParty_id;
						$phone->phone_type_id = $phoneTypes[0]->id;
						$phone->created_by = $userId;
						$phone->updated_by = $userId;
						$phone->save();
					}

					//TBD $phoneType =PhoneType::where('name', 'Mobile Phone (Home)')->first();
					//TBD $phoneTypeId = $phoneType['id'];

					if (isset($data['pm_secondaryContact'])) {
						$phone = new Phone;
						$phone->phone_number = $data['pm_secondaryContact'];
						$phone->party_id = $pmParty_id;
						$phone->phone_type_id = $phoneTypes[1]->id;
						$phone->created_by = $userId;
						$phone->updated_by = $userId;
						$phone->save();
					}

					$pmUser = new User;
					$pmUser->first_name = $data['pm_firstName'];
					$pmUser->last_name = $data['pm_lastName'];
					$pmUser->email = $data['pm_email'];
					// $pass = substr((string) $data['pm_primaryContact'], -4) . '@cgx';
					// $pass = UserService::generatePassword($data['pm_primaryContact']);

					// abhi - 26/03/19
					$mobNo = $data['pm_primaryContact'];
					$pass = UserService::generatePassword($mobNo, $data['pm_dob'], 0, $data['pass_logic']);
					Log::error($pass);

					$pmUser->password = bcrypt($pass);
					$pmUser->party_id = $pmParty_id;
					$pmUser->created_by = $userId;
					$pmUser->updated_by = $userId;
					$pmUser->userid = $data['pm_userId'];
					$pmUser->personal_email = $data['pm_personalEmail'];
					$pmUser->save();

					// user_attribute table
					$attrValue = UserService::getAttrValue($data, 'pmUser');
					$user_attribute = new UserAttribute;
					$user_attribute->user_id = $pmUser->id;
					$user_attribute->attr_value = $attrValue;
					$user_attribute->created_by = $userId;
					$user_attribute->updated_by = $userId;
					$user_attribute->attr_name = 'profile_badge_count';
					$user_attribute->save();

					// attaching new user
					$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Program Manager'], "Program Manager")[0];
					//$role= Role::where('name', 'Program Manager')->first();
					$pmUser->roles()->attach($role['id'], ['updated_by' => $userId, 'created_by' => $userId]);
					Log::debug("new user atteched ");

					// detaching old user role
					$user = User::where('party_id', $data['old_pm_party_id'])->first();
					$user->roles()->detach($role['id']);
					Log::debug("old user detached ");
				}

				//for existing new user
				else {

					//finding new user pary_id
					$users_party = Person::where('id', $data['pm_firstName'])->first();
					$users_party_id = $users_party['party_id'];

					//attaching new user
					$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Program Manager'], "Program Manager")[0];
					//$role= Role::where('name', 'Program Manager')->first();
					$pmUser = User::where('party_id', $users_party_id)->first();
					$pmUser->roles()->attach($role['id'], ['updated_by' => $userId, 'created_by' => $userId]);
					Log::debug("new user atteched ");

					//detaching old user

					$user = User::where('party_id', $data['old_pm_party_id'])->first();
					$user->roles()->detach($role['id']);
					Log::debug("old user deteched ");
				}

			} else { /*no user update*/}

			if (isset($data['ccDetach'])) {

				if (!isset($data['sameUser'])) {
					//normal proceedure

					if (!is_numeric($data['cc_firstName'])) {

						$ccPartyTypeId = $data['userPartyTypeId'];

						$ccParty = new Party;
						$ccParty->party_type_id = $ccPartyTypeId;
						$ccParty->created_by = $userId;
						$ccParty->updated_by = $userId;
						$ccParty->save();
						$ccParty_id = $ccParty->id;

						$companyCoordinator = new Person;
						$companyCoordinator->first_name = $data['cc_firstName'];
						$companyCoordinator->last_name = $data['cc_lastName'];
						$companyCoordinator->email = $data['cc_email'];
						$companyCoordinator->party_id = $ccParty_id;
						$companyCoordinator->tenant_party_id = $data['tenant_partyId'];
						$companyCoordinator->customer_party_id = $data['organization_party_id'];

						$companyCoordinator->department = $data['cc_department'];
						$companyCoordinator->designation = $data['cc_designation'];
						$companyCoordinator->linkedin_profile = $data['cc_linkedinProfile'];
						$companyCoordinator->facebook_id = $data['cc_fbProfile'];
						$companyCoordinator->twitter_handle = $data['cc_twitterHandle'];

						$companyCoordinator->created_by = $userId;
						$companyCoordinator->updated_by = $userId;
						$companyCoordinator->employee_id = $data['cc_employeeId'];

						if (isset($data['cc_gender'])) {
							if ($data['cc_gender'] == 'M') {
								$companyCoordinator->gender = 'M';
							} else {
								$companyCoordinator->gender = 'F';
							}
						}

						//  saving uploaded details
						//Now we are at the end -> so let us move the photo if uploaded to final destination folder

						if (isset($uploadInfo['uploadedFileNames']['cc_photo'])) {
							//calling moveCcPhoto function
							$stored_path = Self::moveCcPhoto($uploadInfo, $data, $ccParty_id);
							$companyCoordinator->photo = $stored_path['cc_photo'];
							$companyCoordinator->original_photo = $stored_path['cc_photo_name'];

						}
						$companyCoordinator->personal_email = $data['cc_personalEmail'];
						$companyCoordinator->google_profile = $data['cc_googleId'];

						// abhi - 26/03/19
						if (isset($data['cc_profile']) && $data['cc_profile'] == 'private') {
							$companyCoordinator->is_private = 1;
						} else {
							$companyCoordinator->is_private = 0;
						}

						$companyCoordinator->save();
						log::debug("CC Person created");

						if (isset($data['cc_dob'])) {
							//
							$peopleAttribute = new PeopleAttribute;
							$peopleAttribute->people_id = $companyCoordinator->id;
							$peopleAttribute->attr_name = 'dob';

							$peopleAttribute->attr_value = $data['cc_dob'];

							$peopleAttribute->created_by = $userId;
							$peopleAttribute->updated_by = $userId;
							$peopleAttribute->save();
						}

						//  for phone table

						//TBD $phoneType =PhoneType::where('name', 'Mobile Phone (Work)')->first();
						//TBD $phoneTypeId = $phoneType['id'];

						//Save the phone numbers also
						if (isset($data['cc_primaryContact'])) {
							$phone = new Phone;
							$phone->phone_number = $data['cc_primaryContact'];
							$phone->party_id = $ccParty_id;
							$phone->phone_type_id = $phoneTypes[0]->id;
							$phone->created_by = $userId;
							$phone->updated_by = $userId;
							$phone->save();
						}

						//TBD $phoneType =PhoneType::where('name', 'Mobile Phone (Home)')->first();
						//TBD $phoneTypeId = $phoneType['id'];
						if (isset($data['cc_secondaryContact'])) {
							$phone = new Phone;
							$phone->phone_number = $data['cc_secondaryContact'];
							$phone->party_id = $ccParty_id;
							$phone->phone_type_id = $phoneTypes[1]->id;
							$phone->created_by = $userId;
							$phone->updated_by = $userId;
							$phone->save();
						}

						// for user table
						$ccUser = new User;
						$ccUser->first_name = $data['cc_firstName'];
						$ccUser->last_name = $data['cc_lastName'];
						$ccUser->email = $data['cc_email'];
						// $pass = substr((string) $data['cc_primaryContact'], -4) . '@cgx';
						// $pass = UserService::generatePassword($data['cc_primaryContact']);

						// abhi -26/03/19
						$mobNo = $data['cc_primaryContact'];
						$pass = UserService::generatePassword($mobNo, $data['cc_dob'], 0, $data['pass_logic']);
						Log::error($pass);

						$ccUser->password = bcrypt($pass);
						$ccUser->party_id = $ccParty_id;
						$ccUser->created_by = $userId;
						$ccUser->updated_by = $userId;
						$ccUser->userid = $data['cc_userId'];
						$ccUser->personal_email = $data['cc_personalEmail'];
						$ccUser->save();
						// user_attribute table
						$attrValue = UserService::getAttrValue($data, 'ccUser');
						$user_attribute = new UserAttribute;
						$user_attribute->user_id = $ccUser->id;
						$user_attribute->attr_value = $attrValue;
						$user_attribute->created_by = $userId;
						$user_attribute->updated_by = $userId;
						$user_attribute->attr_name = 'profile_badge_count';
						$user_attribute->save();

						//attaching new user
						$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Company Coordinator'], "Company Coordinator")[0];
						//$role= Role::where('name', 'Company Coordinator')->first();
						$ccUser->roles()->attach($role['id'], ['updated_by' => $userId, 'created_by' => $userId]);
						Log::debug("new user attached");

						//detaching old users role

						$user = User::where('party_id', $data['old_cc_party_id'])->first();
						$user->roles()->detach($role['id']);
						Log::debug("old user detached");

					}

					//for existing new user
					else {

						//finding new user pary_id
						$users_party = Person::where('id', $data['cc_firstName'])->first();
						$users_party_id = $users_party['party_id'];

						//attaching new user

						$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Company Coordinator'], "Company Coordinator")[0];
						//$role= Role::where('name', 'Company Coordinator')->first();
						$ccUser = User::where('party_id', $users_party_id)->first();
						$ccUser->roles()->attach($role['id'], ['updated_by' => $userId, 'created_by' => $userId]);
						Log::debug("new user attached");

						//detaching old user

						$user = User::where('party_id', $data['old_cc_party_id'])->first();
						$user->roles()->detach($role['id']);
						Log::debug("old user detached");

					}
				}

				//else for checking same user
				else {
					//detaching old user

					$role = $data['masterDataInterface']->filteredList('Role', ['name' => 'Company Coordinator'], "Company Coordinator")[0];

					$user = User::where('party_id', $data['old_cc_party_id'])->first();
					$user->roles()->detach($role['id']);
					Log::debug("old user detached");

					//assigning role of pm

					if (isset($pmUser)) {
						Log::debug("old user detached1");
						$pmUser->roles()->attach($role['id'], ['updated_by' => $userId, 'created_by' => $userId]);
					} else {
						Log::debug("old user detached2");
						$pmUser = User::where('party_id', $data['old_pm_party_id'])->first();
						$pmUser->roles()->attach($role['id'], ['updated_by' => $userId, 'created_by' => $userId]);
					}
				}
			} else { /*no user update*/}

			Log::debug('' . $organizationId);
			Log::debug("OrganizationService:: finished");
			return $organizationId;

		});

		Log::debug("OrganizationService::updateOrganization finished");
		return $check;
	}

	public static function deleteOrganization($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {

			// $user_id = auth()->user()->id;
			// Log::error($user_id);

			// // $role_user_data = RoleUser::where('user_id', $user_id)->first();

			// Log::error($role_user_data);

			// Log::error($role_user_data['role_id']);
			// // exit();

			// $role_user_data['role_id'] = 3;

			// if ($role_user_data['role_id'] != 1) {

			// 	return false;
			// 	# code...
			// }
			// exit();
			// check constraint
			$organization = Organization::where('id', $id)->first();
			$people = Person::where('customer_party_id', $organization['party_id'])->first();

			if (!$people) {
				Organization::where('id', $id)->update(['deleted_by' => auth()->user()->id]);
				$check = Organization::where('id', $id)->delete();
			} else {
				$check = false;
			}

			return $check;
		});
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}

	public static function fileUpload($data) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$class = $data['configurations']['storage_implementation_class'];
		$storageUtility = new $class;
		$uploadInfo = $storageUtility->uploadFileToPublicStorage($data['organizationRequest'], $data['temp_path']);
		// $files=$uploadInfo['uploadedFileNames'];
		Log::debug('OrganizationService::uploadFile calling finished');
		Log::debug($uploadInfo);
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $uploadInfo;
	}

	//moving organization logo
	public static function moveLogo($uploadInfo, $data, $orgParty_id) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$files = $uploadInfo['uploadedFileNames'];

		foreach ($files['organization_logo'] as $file) {
			Storage::move("public/" . $data['temp_path'] . "/" . $file, "public/" . $data['storage_path'] . "/customer/" . $orgParty_id . "/logo/" . $file);
			$org_logo_path = "public/" . $data['storage_path'] . "/customer/" . $orgParty_id . "/logo/" . $file;
			$stored_path['org_logo'] = $org_logo_path;
			$stored_path['org_logo_name'] = $uploadInfo['originalFileNames']['organization_logo'][0];
		}

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $stored_path;
	}

	//moving  pm photo
	public static function movePmPhoto($uploadInfo, $data, $pmParty_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$files = $uploadInfo['uploadedFileNames'];

		foreach ($files['pm_photo'] as $file) {
			Storage::move("public/" . $data['temp_path'] . "/" . $file, "public/" . $data['storage_path'] . "/user/" . $pmParty_id . "/profile/" . $file);

			$pm_photo_path = "public/" . $data['storage_path'] . "/user/" . $pmParty_id . "/profile/" . $file;

			$stored_path['pm_photo'] = $pm_photo_path;
			$stored_path['pm_photo_name'] = $uploadInfo['originalFileNames']['pm_photo'][0];
		}

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $stored_path;

	}

	//moving  cc photo
	public static function moveCcPhoto($uploadInfo, $data, $ccParty_id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$files = $uploadInfo['uploadedFileNames'];

		foreach ($files['cc_photo'] as $file) {
			Storage::move("public/" . $data['temp_path'] . "/" . $file, "public/" . $data['storage_path'] . "/user/" . $ccParty_id . "/profile/" . $file);
			$cc_photo_path = "public/" . $data['storage_path'] . "/user/" . $ccParty_id . "/profile/" . $file;
			$stored_path['cc_photo'] = $cc_photo_path;
			$stored_path['cc_photo_name'] = $uploadInfo['originalFileNames']['cc_photo'][0];
		}

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $stored_path;
	}

	// created by dhruvi
	// for get company type organization only

	public static function retrieveClientCompany() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Organization::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}



}
