@extends('layouts.base')
@section('content')
<form id="pincodeForm" action="/pincodes{{null}}@yield('editID')" onsubmit="return validateMandatoryFields();" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}

	@section('editMethod')
	@show
	<div class="row">
    	<h3 class="headings" align="left">@yield('title')</h3>
    </div>
    <div class="row">
		<div class="col-lg-6 col-lg-offset-3" align="center">
			<span class="errorMessage">
			</span>
			<span class="successMessage">
			</span>
		</div>
	</div>
	<p></p>

<!-- 	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="form-group required">
				<label for="country_id">{{__('labels.geography.country_id')}}</label>

			<select id="country_id" name="country_id" class="form-control mandatory" @yield('disabled')>
				<option value="0">---select---</option>
				@foreach($country_list as $list)
				@if(isset($data))
				@if($list->id == $data->country_id)
				<option value="{{$list->id}}" selected>{{$list->name}}</option>
				@else
				<option value="{{$list->id}}">{{$list->name}}</option>	
				@endif
				@else
				<option value="{{$list->id}}">{{$list->name}}</option>
				@endif
				@endforeach
			</select>
			</div>
		</div>
	</div> 
	<p></p>

	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="form-group required">
				<label for="state_id">{{__('labels.geography.state_id')}}</label>

			<select id="state_id" name="state_id" class="form-control mandatory" @yield('disabled')>
				<option value="0">---select---</option>
				@foreach($state_list as $list)
				@if(isset($data))
				@if($list->id == $data->state_id)
				<option value="{{$list->id}}" selected>{{$list->name}}</option>
				@else
				<option value="{{$list->id}}">{{$list->name}}</option>	
				@endif
				@else
				<option value="{{$list->id}}">{{$list->name}}</option>
				@endif
				@endforeach
			</select>
			</div>
		</div>
	</div> 
	<p></p>
 -->
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="form-group required">
				<label for="city_id">{{__('labels.geography.city_id')}}</label>

			<select id="city_id" name="city_id" class="form-control mandatory" @yield('disabled')>
				<option value="0">---select---</option>
				@foreach($city_list as $list)
				@if(isset($data))
				@if($list->id == $data->city_id)
				<option value="{{$list->id}}" selected>{{$list->name}}</option>
				@else
				<option value="{{$list->id}}">{{$list->name}}</option>	
				@endif
				@else
				<option value="{{$list->id}}">{{$list->name}}</option>
				@endif
				@endforeach
			</select>
			</div>
		</div>
	</div> 
	<p></p>

	<div class="row"  >
		<div class="col-lg-6 col-lg-offset-3">
			<div class="form-group required">
				<label for="pincode_name">{{__('labels.geography.pincode_name')}}</label>
				<input type="text" class="form-control @yield('is_mandatory')" id="pincode_name"  value="@yield('pincode_name')" name="pincode_name" @yield('showPincodeName')>
			</div>
		</div>
	</div> 
	<p></p>


	<div class="row col-lg-6 col-lg-offset-3" align="center">
		<button type="submit" onclick="return validateMandatoryFields();" class="btn btn-primary" name="submit" @yield('disabled')>submit</button>
		<!-- <input type='button' id='_submit' value='Upload!'> -->
	</div>
</form>
@endsection	
