<?php

namespace App\Http\Controllers;

use App\Models\ArtefactType;
use App\Models\Program;
use App\Models\ProgramLtTopic;
use App\Models\TopicUser;
use App\Services\LearningTreeSvgCoordinatorService;
use App\Services\ProgramLearningTreeService;
use App\Services\ProgramLTToMasterLTService;
use App\Services\ValidationRulesService;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Controllers\BaseController;


class ProgramLearningTreeController extends BaseController {
	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		$this->middleware('CheckCompositeMiddleWare', ['except' => ['index', 'update']]);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function createLearningTreeForProgram($lt_id, $program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			Log::debug($lt_id . " " . $program_id);

			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['lt_id'] = $lt_id;
			$data['program_id'] = $program_id;

			$lt = ProgramLearningTreeService::createLearningTreeForProgram($data);

		} catch (Exception $e) {
			Log::error('Exception while creating learning tree for program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $lt;
	}

	public function saveProgramTopic(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {

			$data = $request->all();
			log::debug($data);
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['utilsInterface'] = parent::$utilsInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['request'] = $request;

			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('Topic');
			$request->validate($rules);


			//
			$topic_id = ProgramLearningTreeService::saveTopic($data);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $topic_id;
	}

	public function deleteProgramTopic($id, $containerLearningTree, $tab) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {

			$masterDataInterface = parent::$masterDataInterface;

			$topic_id = ProgramLearningTreeService::deleteTopic($id, $containerLearningTree, $tab, $masterDataInterface);
			return $topic_id;

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function getProgramTopicForEdit($id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$topic = ProgramLearningTreeService::getTopicForEdit($id);
			Log::debug($topic);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $topic;
	}

	public function getParentStructure($containerLearningTree, $id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$parent_structure = ProgramLearningTreeService::getParentStructure($containerLearningTree, $id);
			Log::debug($parent_structure);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $parent_structure;
	}

	public function getUpdateStructure($containerLearningTree) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$update_structure = ProgramLearningTreeService::getUpdateStructure($containerLearningTree);
			Log::debug($update_structure);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $update_structure;
	}

	public function getSvgForProgram($containerLearningTree, $parent_id, $parent_name) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug($parent_id);
		// var_dump($parent_id);
		// exit();

		try {
			//
			$svg = LearningTreeSvgCoordinatorService::createLearningTreeSvg('PLT', $containerLearningTree, $parent_name, $parent_id, parent::$masterDataInterface, 1);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $svg;
	}

	public function publishLearningTree($program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//
			$id = ProgramLearningTreeService::publishLearningTree($program_id);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $id;
	}

	public function saveProgramAsLt(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {

			$data = $request->all();
			log::debug($data);

			//getting data
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');
				$userId = $userPrincipal->getUserId();
			} else {
				$userId = \Auth::user()->id;
			}

			$programId = $data['prograId'];
			$learningTreeName = $data['exporting_lt_name'];
			$learningTreeCode = $data['exporting_lt_code'];
			$learningTreeDescription = $data['exporting_lt_description'];
			// if (isset($data['copy_content_flag'])) {
			if (array_key_exists("copy_content_flag", $data)) {
				$contentToBeCopied = 'Y';
			} else {
				$contentToBeCopied = 'N';
			}
			// $contentToBeCopied = 'Y';
			$masterDataInterface = parent::$masterDataInterface;

			// calling ProgramLTToMasterService
			$status = ProgramLTToMasterLTService::saveProgramLtAsMasterLt($programId, $contentToBeCopied, $userId, $learningTreeName, $learningTreeCode, $learningTreeDescription, $masterDataInterface);

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return 'succes';
	}

	public function vcParticipantAssign($topic_id, $parent_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		// getting participants
		$topic = ProgramLtTopic::where('id', $topic_id)->first();
		$program = Program::where('id', $topic['container_program_id'])->first();

		$lt = json_decode($program['learning_tree'], true);
		$approved_participants = $lt['approvedParticipants'];
		Log::debug($approved_participants);

		$vc_artefact = ArtefactType::where('name', 'Virtual Classroom')->first();
		$vc1 = ProgramLtTopic::where(['parent_topic_id' => $topic['parent_topic_id'], 'artefact_type_id' => $vc_artefact['id'], 'optional' => 1])->get();

		$vc_count = count($vc1);

		foreach ($vc1 as $key => $value) {
			$vc_user_ids = TopicUser::where('topic_id', $value['id'])->pluck('user_id')->toArray();
			// $vc_users = User::whereIn('id', $vc_user_ids)->get();
			$vc_users = DB::table('users')
				->leftJoin('people', 'users.party_id', '=', 'people.party_id')
				->select('users.id', 'users.first_name', 'users.last_name', 'people.employee_id')->whereIn('users.id', $vc_user_ids)->get();
			$value['users'] = $vc_users;
			$approved_participants = array_diff($approved_participants, $vc_user_ids);
		}
		$vc = $vc1;
		Log::debug($vc);
		Log::debug($approved_participants);

		$participants = DB::table('users')
			->leftJoin('people', 'users.party_id', '=', 'people.party_id')
			->select('users.id', 'users.first_name', 'users.last_name', 'people.employee_id')->whereIn('users.id', $approved_participants)->get();

		Log::debug($participants);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return view('program.vc_participant_assign', compact('participants', 'vc', 'vc_count'));
	}

	public function saveVcParticipants(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$data = $request->all();
		log::debug($data);
		$data['authenticatedUser'] = \Auth::user();

		$status = ProgramLearningTreeService::saveVcParticipants($data);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $status;
	}

}
