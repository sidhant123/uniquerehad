<?php

namespace vnnogile\SystemNotification\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//custom import
use App\Models\CommunicationTrail;
use Illuminate\Support\Facades\Log;

class SystemNotificationSuccessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $trail_data;
    public $tries = 5;
    
    public function __construct($trail_data)
    {
        $this->trail_data = $trail_data;
    }


    public function handle()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started---");

        $trail_data=$this->trail_data;
        $response = $trail_data['response'];
            
        $communicationTrail=new CommunicationTrail();
        $communicationTrail->error_code="no error";
        $communicationTrail->exception="no exception";
        if(isset($response['message']->results[0]->error)){
            $communicationTrail->error_code=$response['message']->results[0]->error;
            $communicationTrail->exception=$response['message']->results[0]->error;
        }
        
        
        $communicationTrail->status="failed";
        if(isset($response['message']->success)){
            if($response['message']->success){
               $communicationTrail->status="success"; 
            }
            
        }
        
        if (array_key_exists('customer_organization_code', $trail_data)){
            $communicationTrail->communication_event_id=$trail_data['communication_event_id'];
        }
        

        $communicationTrail->created_by=$trail_data['login_id'];
        $communicationTrail->updated_by=$trail_data['login_id'];
        if (array_key_exists('customer_organization_code', $trail_data)){
            $communicationTrail->customer_organization_code=$trail_data['customer_organization_code'];
        }            
        
        if (array_key_exists('tenant_code', $trail_data)){
            $communicationTrail->tenant_code=$trail_data['tenant_code'];
        }
        
        if (array_key_exists('program_code', $trail_data)){
            $communicationTrail->program_code=$trail_data['program_code'];
        }
        
        if (array_key_exists('content', $trail_data)){
            $communicationTrail->content=$trail_data['content'];
        }
        
        if (array_key_exists('job_id', $trail_data)){
            $communicationTrail->job_id=$trail_data['job_id'];
        }

        if(isset($response['message']->multicast_id)){
        if (array_key_exists('multicast_id', $response['message'])){
            $communicationTrail->transaction_id=$response['message']->multicast_id;
        }
        }
        if (array_key_exists('party_id', $trail_data)){
            $communicationTrail->party_id=$trail_data['party_id'];
        }
        dd($communicationTrail);
        $res = $communicationTrail->save();
        $ct_id=$communicationTrail->id;
            // dd($ct_id);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " saved:".$ct_id);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished******");
        
    }
}
