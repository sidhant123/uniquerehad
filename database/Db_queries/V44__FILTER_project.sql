

INSERT INTO public.filter_masters(
             created_at, updated_at,  created_by, updated_by, 
             function_name, route_name, "row", sequence, control_type, 
            control_id, control_text,  control_name, data_url, 
            load_event, action, label_css, field_css, tenant_party_id)
    VALUES (now(), now(), 1, 1,
            'Client', 'projects.index', 1, 1, 'select', 
            'organization_id', '---select---', 'organization_id', 'retrieveClient', 
            0,0, 'col-xs-1', 'col-xs-2', 0) ;

update list_view_masters set filter_columns = 'organization_id' where entity_name='Project';


INSERT INTO public.composition_masters(
             route_name, controller_name, view_name, sequence, 
            created_at, updated_at, created_by, updated_by,  tenant_party_id)
    VALUES ('projects.index', 'App\Http\Controllers\ProjectController', '', 2, 
        now(), now(), 1, 1, 0);
INSERT INTO public.composition_masters(
             route_name, controller_name, view_name, sequence, 
            created_at, updated_at, created_by, updated_by,  tenant_party_id)
    VALUES ('projects.index', 'vnnogile\FilterViews\Controllers\FilterController', '', 1, 
        now(), now(), 1, 1, 0);

