@extends('vnnogile::city_base')
@section('title', 'Edit City')
@section('editMethod')
{{method_field('PUT')}}
@endsection
@section('editID', '/'.$data->id)
@section('city_name', $data->name)
@section('is_mandatory', 'mandatory')
@section('is_update', 'Y')
