<?php

namespace App\Http\Middleware;

use Closure;
//Custom Imorts
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class CheckRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
/*        print_r($roles[0]);
print_r($roles[1]);
exit();*/
        $isInRole = false;
        $routeName = Route::currentRouteName();
        Log::debug("*****Routes*****");
        Log::debug("$routeName");
        // if ($routeName == 'countries'){
        // dump($request->user());
        // Log::debug("dumped");
        // exit();
        // }

        $systemRoles = $request->user()->roles;
        foreach ($roles as $role) {
            foreach ($systemRoles as $systemRole) {
                if ($systemRole->name == $role) {
                    $isInRole = true;
                    break 2;
                }
            }
        }
        Log::debug("*****Routes 1*****");
        if (!$isInRole) {
            Log::debug("*********NOT IN ROLE *************");
            //return redirect()->guest(route('login'));
            \Auth::logout();
            abort(403, 'You are not authorized to use the CGX Backoffice Application.');
        }
        Log::debug("*********IS IN ROLE *************");
        return $next($request);
    }
}
