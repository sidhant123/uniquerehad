<?php

namespace vnnogile\Client\Controllers;

use App\Http\Controllers\Controller;

//Custom Imports
use App\Models\Configuration;
use Excel;
use Illuminate\Support\Facades\Log;

class BaseController extends Controller
{
    protected static $masterDataInterface;
    protected static $utilsInterface;

    public function __construct()
    {

        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $masterDataConfigKey = 'masterDataImplClass';
        $utilsConfigKey = 'utilsImplClass';
        $masterDataInterfaceImplClass = "";
        $utilsInterfaceImplClass = "";
        $this->middleware('auth');

        if (session()->has('configurations')) {
            $masterDataInterfaceImplClass = session('configurations')[$masterDataConfigKey];
            $utilsInterfaceImplClass = session('configurations')[$utilsConfigKey];
        } else {
            $implClasses = Configuration::select('config_key', 'config_value')->whereIn('config_key', array('master_data_impl_class', 'utils_impl_class'))->get();

            foreach ($implClasses as $implClass) {
                if ($implClass->config_key == 'master_data_impl_class') {
                    //session([$masterDataConfigKey => $implClass->config_value]);
                    $masterDataInterfaceImplClass = new $implClass->config_value;
                } else if ($implClass->config_key == 'utils_impl_class') {
                    //session([$utilsConfigKey => $implClass->config_value]);
                    $utilsInterfaceImplClass = new $implClass->config_value;
                }
            }
        }

        self::$masterDataInterface = new $masterDataInterfaceImplClass;
        self::$utilsInterface = new $utilsInterfaceImplClass;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    public function downloadSingleSheetExcelReport($fileName, $title, $sheetName, $description, $keys, $values)
    {
        Excel::create($fileName, function ($excel) use ($sheetName, $title, $description, $values, $keys) {
            $excel->setTitle($title);
            $excel->setCreator('Cognegix')->setCompany('Cognegix');
            $excel->setDescription($description);
            $excel->sheet($sheetName, function ($sheet) use ($values, $keys) {
                $sheet->row(1, $keys);
                $counter = 2;
                foreach ($values as $value) {
                    Log::debug($value);
                    $sheet->row($counter, $value);
                    $counter++;
                }
            })->download('xlsx');
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        });
    }

    public function downloadMultiSheetExcelReport($fileName, $title, $description, $sheetsData)
    {
        Excel::create($fileName, function ($excel) use ($title, $description, $sheetsData) {
            $excel->setTitle($title);
            $excel->setCreator('Cognegix')->setCompany('Cognegix');
            $excel->setDescription($description);
            foreach ($sheetsData as $sheetData) {
                $excel->sheet($sheetData['name'], function ($sheet) use ($sheetData) {
                    $sheet->row(1, $sheetData['keys']);
                    $counter = 2;
                    foreach ($sheetData['values'] as $value) {
                        Log::debug($value);
                        $sheet->row($counter, $value);
                        $counter++;
                    }
                });
            }
            $excel->download('xlsx');
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        });
    }
    public function downloadMultipleSheetExcel($fileName, $title, $description, $invalidRecords, $keys)
    {
        Excel::create($fileName, function ($excel) use ($fileName, $title, $description, $invalidRecords, $keys) {
            Log::debug('*****INVALID RECORDS IN LOOP 2 ***');
            // Set the title
            $excel->setTitle($title);
            // Chain the setters
            $excel->setCreator('Cognegix')
                ->setCompany('Cognegix');
            // Call them separately
            $excel->setDescription($description);
            foreach ($invalidRecords as $key => $sheetData) {
                # code...
                $excel->sheet($key, function ($sheet) use ($sheetData, $keys, $key) {
                    Log::debug('*****INVALID RECORDS IN LOOP 3 ***');
                    if(isset($keys[$key])){
                    $sheet->row(1, $keys[$key]);
                    $counter = 2;
                    Log::debug('*****INVALID RECORDS IN LOOP 4 ***');
                    foreach ($sheetData as $invalidData) {
                        $sheet->row($counter, $invalidData);
                        $counter++;
                    }

                }
                });
            }
            $excel->download('xlsx');
        });
        
    }

    public function populateOrClearSearch($request){
        if ($request['search']){
            session(['search' => $request['search']]);
            session(['path' => $request->path()]);
        }else{
             if ($request['fromSearch']){
                 $request->session()->forget('search');
                 $request->session()->forget('path');
            }else if ($request->session()->has('search') && $request->session()->has('path')) {
                if (session('path') == $request->path()){
                    $request['search'] = session('search');
                }else{
                    $request->session()->forget('search');    
                    $request->session()->forget('path');    
                }
            }
        }

    }

}
