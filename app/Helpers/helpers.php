<?php
function spaces($count)
{

    $spaces = '';
    if ($count == 0) {
        return '&nbsp;';
    } else {
        for ($i = 0; $i < $count; $i++) {
            # code...
            $spaces = $spaces . '&nbsp;&nbsp;';
        }
        return $spaces;
    }

}
function imgToBase64($imgPath){
        $type = pathinfo($imgPath, PATHINFO_EXTENSION);
        $file = file_get_contents($imgPath);
        return 'data:image/' . $type . ';base64,' . base64_encode($file);
}
