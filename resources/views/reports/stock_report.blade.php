@extends('layouts.base')
@section('content')
<form action="/stockReport" id="ExportStockREportForm" method="post" >
  {{ csrf_field() }}
  <input type="hidden" name="report_type" id="report_type">
  <input type="hidden" name="entity_type" id="entity_type">
  @section('editMethod')
  @show
  <h3 align="center">Export Stock Report</h3>

    <strong>NOTE**</strong><p></p>
<textarea class="form-control" style="width:100%;" rows="4" cols="30" readonly="readonly">
You can select site for download site wise stock report.
</textarea>
    <p></p>

  <div class="row">
    <div class="col-lg-3">
      <div class="form-group">
        <label for="site_id"></label>
        <select type="text" id="site_id" name="site_id" class="form-control mandatory">
          <option value="0">---Site---</option>
          @foreach($site as $val)
            <option value="{{$val->id}}">{{$val->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="form-group">
        <label for=""></label>
        <br>
        <button id="btnCompletion" class="btn btn-primary" type="button" style="height: 34px;" >
          Download &nbsp;&nbsp;<i class="vnn vnn-download"></i>
        </button>
      </div>
    </div>

  </div>

  <div class="row">

  </div>
      
</form>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
     $('#site_id').select2({});


$(document).on('click', '#btnCompletion', function() {

  var valid = true;
  valid = validateMandatoryFields();

  var site_id = $('#site_id').val();
  if(site_id == 0) {
  document.getElementById("select2-site_id-container").style.border = "1px solid red";
  } else {
    document.getElementById("select2-site_id-container").style.border = "0px solid red";
  }

   if(valid) {
     $('#ExportStockREportForm').submit();
   }

 });

  function getDataThroughAxios(url, value, control_id) {
   console.log(value);
   axios.get(url + value)
     .then(function(response) {
       console.log(response);
       populateData(response['data'], control_id);
     })
     .catch(function(error) {
       console.log("error" + error);
     });
 }
});
  function populateData(data, control_id) {
    $("#" + control_id).empty();
   if (Object.keys(data).length > 0) {
        $("#" + control_id).append('<option value="0">--Select--</option>');
     $.each(data, function(key, value) {
         $("#" + control_id).append("<option value='" + value.id + "'>" + value.name + "</option>");
     });
   }
 }
</script>
@endpush
