<?php

namespace App\Http\Controllers;

// custom imports
// use App\Http\Controllers\ProgramController;
use App\Models\ArtefactType;
use App\Models\BadgeType;
use App\Models\Configuration;
use App\Models\LearningTree;
use App\Models\Organization;
use App\Models\Person;
use App\Models\Program;
use App\Models\ProgramAttribute;
use App\Models\ProgramCredential;
use App\Models\Role;
use App\Services\AxiosService;
use App\Services\MasterProgramService;
use App\Services\OrganizationService;
use App\Services\ProgramService;
use App\Services\ValidationRulesService;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;
use vnnogile\Tags\services\TagService;

class ProgramController extends BaseController {

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		$this->middleware('CheckCompositeMiddleWare', ['except' => ['update']]);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function index(Request $request, $flag = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// $request['customer_party_id']=45;
		//$this->authorize('List Companies');
		// $this->authorize('List Companies', 'List Companies');
		$this->authorize('List Programs', 'List Programs');
		//parent::populateOrClearSearch($request);

		// checking for company coordinator party id
		if (session()->has('userPrincipal')) {
			$userPrincipal = session('userPrincipal');
			//return $userPrincipal->getCustomerPartyId() . ' = ' . $userPrincipal->getCustomerName() ;

			if ($userPrincipal->getCustomerPartyId()) {
				$request['customer_party_id'] = $userPrincipal->getCustomerPartyId();
			}
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		//return PaginateController::paginate('Program', 'programs', parent::$utilsInterface, parent::$masterDataInterface);
		return PaginateController::paginate('Program', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
	}

	public function create(Request $request, $flag = false) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// $this->authorize('Create Programs', 'Create Programs');
		$organization = OrganizationService::retrieveOrganization();
		$badge = BadgeType::all();
		$fast = $badge->where('name', 'Fastest Activity Completion Badge')->first();
		$content = $badge->where('name', 'Content Rating Badge')->first();
		$participant = $badge->where('name', 'Participant Badge')->first();

		$badgeInfo['fast'] = $fast->default_config_value;
		$badgeInfo['content'] = $content->default_config_value;
		$badgeInfo['participant'] = $participant->default_config_value;

		$LT = LearningTree::all();

		$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
		$max_cell_no = $config['config_value'];
		Log::debug($_SERVER['HTTP_HOST']);
		$host = $_SERVER['HTTP_HOST'];

		if ($flag) {
			$programData['program.create_program'] = array('organization' => $organization, 'isTaggale' => $request['isTaggable'], 'badgeInfo' => $badgeInfo, 'LT' => $LT, 'max_cell_no' => $max_cell_no, 'host' => $host);
			return $programData;
		} else {
			return view('program.create_program', compact('organization', 'badgeInfo', 'LT', 'max_cell_no', 'host'));
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		// return view('program.create_program', compact('organization','badgeInfo'));
	}

	public function store(Request $request) {

	}

	private function checkAuthorization($id) {
		$isAuthorized = false;
		if (session()->has('userPrincipal')) {
			$userPrincipal = session('userPrincipal');

			if (empty($userPrincipal->getCustomerPartyId())) {
				return true;
			}

			$program = Program::find($id);
			if ($program['customer_party_id'] != $userPrincipal->getCustomerPartyId()) {
				$isAuthorized = false;
				Log::debug(__LINE__ . " NOT AUTHORIZED " . $userPrincipal->getCustomerPartyId() . " -- " . $program['customer_party_id']);
			} else {
				$isAuthorized = true;
				Log::debug(__LINE__ . " AUTHORIZED " . $userPrincipal->getCustomerPartyId() . " -- " . $program['customer_party_id']);
			}

		}
		return $isAuthorized;
	}

	public function show(Request $request, $id, $flag = false) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$this->authorize('Edit Programs', 'Edit Programs');

			// checking for valid user
			if (!$this->checkAuthorization($id)) {
				Session::flash('status', 'you are not authorized to access this program');
				return redirect()->action('ProgramController@index');
			}
			// getting role of logged in user
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');

				$role_array = $userPrincipal->getRoles();
			}

			$role = $role_array;
			$data = Program::find($id);
			$org = Organization::where('party_id', $data['customer_party_id'])->first();
			$org_id = $org['id'];
			$organization = OrganizationService::retrieveOrganization();
			$LT = LearningTree::all();
			// $lt_data=

			//program attribute
			$programAttributes = ProgramAttribute::where('program_id', $id)->first();
			$data['status_color_allocation_method'] = $programAttributes['status_color_allocation_method'];

			//decoding json
			$lt = json_decode($data['learning_tree'], true);
			Log::debug($lt);

			// getting peoples
			$org = Organization::where('party_id', $data['customer_party_id'])->first();
			$users_data = self::users($org['id']);
			$company_users = $users_data['company_users'];
			$tenant_users = $users_data['tenant_users'];

			$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
			$max_cell_no = $config['config_value'];
			Log::debug($_SERVER['HTTP_HOST']);
			$host = $_SERVER['HTTP_HOST'];

			if ($flag) {
				$tag = TagService::getCommonEntityTag($id, 'Program');
				if (count($tag['tags']) > 0) {
					$tag = $tag->tags;
				}
				$programData['program.view_program'] = array('data' => $data, 'isTaggale' => $request['isTaggable'], 'organization' => $organization, 'tag' => $tag, 'org_id' => $org_id, 'lt' => $lt, 'company_users' => $company_users, 'tenant_users' => $tenant_users, 'role' => $role, 'LT' => $LT, 'max_cell_no' => $max_cell_no, 'host' => $host);
				return $programData;
			} else {
				Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
				return view('program.view_program', compact('data', 'organization', 'org_id', 'lt', 'role', 'LT', 'tenant_users', 'company_users', 'max_cell_no', 'host'));
			}
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

	}

	public function edit(Request $request, $id, $flag = false) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// $this->authorize('Show Users', 'Show Users');
		try {
			$this->authorize('Edit Programs', 'Edit Programs');

			// checking for valid user
			if (!$this->checkAuthorization($id)) {
				Session::flash('status', 'you are not authorized to access this program');
				return redirect()->action('ProgramController@index');
			}
			// getting role of logged in user
			if (session()->has('userPrincipal')) {
				$userPrincipal = session('userPrincipal');

				$role_array = $userPrincipal->getRoles();
			}

			$role = $role_array;
			$data = Program::find($id);
			$org = Organization::where('party_id', $data['customer_party_id'])->first();
			$org_id = $org['id'];
			$organization = OrganizationService::retrieveOrganization();
			$LT = LearningTree::all();
			// $lt_data=

			//program attribute
			$programAttributes = ProgramAttribute::where('program_id', $id)->first();
			$data['status_color_allocation_method'] = $programAttributes['status_color_allocation_method'];

			//decoding json
			$lt = json_decode($data['learning_tree'], true);
			Log::debug($lt);
			// $artefactName=$lt['details']['topics']['artefact_name'];
			// $artefactUrl=$lt['details']['topics']['artefact_url'];
			// Log::debug('888888888'.$artefactName);

			//** creating data for learning tree table and lt selected
			// $object = ProgramLearningTreeService::gettingTabObject($id, 1);
			// Log::deb$objecutg();

			// getting peoples
			$org = Organization::where('party_id', $data['customer_party_id'])->first();
			$users_data = self::users($org['id']);
			$company_users = $users_data['company_users'];
			$tenant_users = $users_data['tenant_users'];

			$config = Configuration::where('config_key', 'learning_tree_max_cell_no')->first();
			$max_cell_no = $config['config_value'];
			Log::debug($_SERVER['HTTP_HOST']);
			$host = $_SERVER['HTTP_HOST'];

			// private forum
			$roles = [];
			$approvingPerson = '';
			$approvingPersonRole = ProgramCredential::where(['program_id' => $id, 'attr_name' => 'forum_approving_person_role'])->first()['attr_value'];
			$rolePerson = [];

			if ($approvingPersonRole) {
				//
				$roles = Role::select('id', 'name')->get();
				$approvingPerson = ProgramCredential::where(['program_id' => $id, 'attr_name' => 'forum_approving_person'])->first()['attr_value'];

				if (is_numeric($approvingPersonRole)) {
					//role persons
					$rolePerson = self::getRolePerson($org_id, $approvingPersonRole);
					$rolePerson = $rolePerson[0];
				} else {
					//role persong empty
					$rolePerson = ['id' => 'custom', 'first_name' => $approvingPerson, 'last_name' => ''];
				}
			}

			if ($flag) {
				$tag = TagService::getCommonEntityTag($id, 'Program');
				if (count($tag['tags']) > 0) {
					$tag = $tag->tags;
				}
				$programData['program.edit_program'] = array('data' => $data, 'isTaggale' => $request['isTaggable'], 'organization' => $organization, 'tag' => $tag, 'org_id' => $org_id, 'lt' => $lt, 'company_users' => $company_users, 'tenant_users' => $tenant_users, 'role' => $role, 'LT' => $LT, 'max_cell_no' => $max_cell_no, 'host' => $host, 'roles' => $roles, 'approvingPerson' => $approvingPerson, 'approvingPersonRole' => $approvingPersonRole, 'rolePerson' => $rolePerson);
				return $programData;
			} else {
				Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
				return view('program.edit_program', compact('data', 'organization', 'org_id', 'lt', 'role', 'LT', 'tenant_users', 'company_users', 'max_cell_no', 'host', 'roles', 'approvingPerson', 'approvingPersonRole', 'rolePerson'));
			}
		} catch (Exception $e) {
			Log::debug('error in store', $e->getMessage());
		}

	}

	public function update(Request $request, $id) {

	}

	public function destroy($id) {

		// checking for valid user
		if (!$this->checkAuthorization($id)) {
			Session::flash('status', 'you are not authorized to access this program');
			return redirect()->action('ProgramController@index');
		}
		print_r($id);
	}

	public function ProgramCreation(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//Get some basic data ahead  of time and set it in data object
			$data = $request->all();

			// validations part
			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			if ($data['program_id'] != "tesseract") {
				$rules = $valdationRulesService->validationRulesWithTenant('Program', '$program', $data['program_id']);
			} else {
				$rules = $valdationRulesService->validationRulesWithTenant('Program');
			}
			$validate_status = $request->validate($rules);

			$data['utilsInterface'] = parent::$utilsInterface;
			$data['masterDataInterface'] = parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();
			$data['program_request'] = $request;

			$program_id = ProgramService::saveProgram($data);
			if ($program_id) {
				if (isset($data['tag'])) {
					if ($data['program_id'] != "tesseract") {
						TagService::updateCommonEntityTag($program_id, 'Program', $data['tag'], $data['authenticatedUser']->id);
						// TagService::saveCommonEntityTag($program_id,'Program',$data['tag'],$data['authenticatedUser']->id);
					} else {
						TagService::saveCommonEntityTag($program_id, 'Program', $data['tag'], $data['authenticatedUser']->id);
					}

				}
			}
			Log::debug($program_id);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $program_id;

		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}
	}

	public function ProgramDirectorCreation(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
//Log::debug(request()->server('SERVER_ADDR'));
		//Log::debug(Request::server('SERVER_ADDR'));

		try {
			//Get some basic data ahead  of time and set it in data object
			$data1 = $request->all();
			//$data= self::convertAxtiosData($data1);
			$data = AxiosService::convertAxiosData($data1);

			$data['authenticatedUser'] = \Auth::user();
			// $director='abhi';
			$director = ProgramService::saveProgramDirector($data);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $director;

		} catch (Exception $e) {
			Log::error('Exception while saving program director :: ', $e->getMessage());
		}
	}

	public function ProgramCoordinatorsCreation(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try {
			//Get some basic data ahead  of time and set it in data object
			$data1 = $request->all();
			//$data= self::convertAxtiosData($data1);
			$data = AxiosService::convertAxiosData($data1);
			Log::debug($data);
			// exit();
			$users = $data;
			log::debug($data);
			unset($users['program_id']);
			$data['users'] = $users;
			Log::debug($data);
			Log::debug($users);
			// exit();
			// $data['utilsInterface']=parent::$utilsInterface;
			// $data['masterDataInterface']=parent::$masterDataInterface;
			$data['authenticatedUser'] = \Auth::user();

			$coordinator = ProgramService::saveProgramCoordinators($data);
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $coordinator;

		} catch (Exception $e) {
			Log::error('Exception while saving program director :: ', $e->getMessage());
		}

	}

	public function users($company_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$company = Organization::where('id', $company_id)->first();

		$company_person = $company->person;
		$tenant_person = Person::where(['customer_party_id' => null, 'tenant_party_id' => $company['tenant_party_id']])->get();
		// $tenant_person = Tenant::where('party_id', $company['tenant_party_id'])->first()->person;

		// $company_party_id = $company_person->pluck('party_id');
		// $tenant_party_id = $tenant_person->pluck('party_id');

		$users = User::all();

		$company_user = [];
		foreach ($company_person as $key => $person_array) {
			//
			// $user = $users->where('party_id', $person_array['party_id'])->first();
			$user = $users->where('party_id', $person_array['party_id'])->where('is_active', 1)->first();

			if ($user) {
				$user['employee_id'] = $person_array['employee_id'];
				array_push($company_user, $user);
			}
		}

		$tenant_user = [];
		foreach ($tenant_person as $key => $person_array) {
			//
			$user = $users->where('party_id', $person_array['party_id'])->first();
			$user['employee_id'] = $person_array['employee_id'];
			array_push($tenant_user, $user);
		}

		// $company_user = $users->whereIn('party_id', $company_party_id);
		// $tenant_user = $users->whereIn('party_id', $tenant_party_id);

		$return_array['company_users'] = $company_user;
		$return_array['tenant_users'] = $tenant_user;

		// print_r($tenant_person)

		return $return_array;

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function getVideoDocument($tag) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$video_artefact = ArtefactType::where('name', 'Video')->first();

		$documents = TagService::searchTagsForDocuments($tag, 'Document', 'artefact_type_id', $video_artefact['id'], 'Video');
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

		return $documents;
	}

	public static function importParticipantsFromProgram($program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$participants = ProgramService::importParticipantsFromProgram($program_id);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}
	public static function importParticipantsWithPhoneFromProgram($program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$participants = ProgramService::importParticipantsWithPhoneFromProgram($program_id);
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}
	public static function getProgramsForCompany($company_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$company = Organization::where('id', $company_id)->first();
		$programs = Program::where('customer_party_id', $company['party_id'])->get();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $programs;
	}
	public function getProgramByCustomers($customer_party_id) {
		return ProgramService::getProgramByCustomers($customer_party_id);
	}

	public static function approveParticipants(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$participants = json_decode($request['data'], true);

		$data['program_id'] = $request['program_id'];
		$data['added_participants'] = $participants['added'];
		$data['approved_participants'] = $participants['appr'];
		$data['authenticatedUser'] = \Auth::user();
		// Log::debug($data);

		$return_data = ProgramService::approveParticipants($data);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $return_data;
	}

	public static function addParticipants(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$data['participants'] = json_decode($request['data'], true);
		$data['program_id'] = $request['program_id'];

		Log::debug($data);
		$data['authenticatedUser'] = \Auth::user();

		$participants = ProgramService::addParticipants($data);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}

	public static function unapproveParticipants(Request $request) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$data['participants'] = json_decode($request['data'], true);
		$data['program_id'] = $request['program_id'];
		Log::debug($data);

		$data['authenticatedUser'] = \Auth::user();

		$participants = ProgramService::unapproveParticipants($data);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}

	public static function publishProgram($program_id) {

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		$data['program_id'] = $program_id;
		$data['authenticatedUser'] = \Auth::user();

		$participants = ProgramService::publishProgram($data);

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $participants;
	}

// ==============Get learning themes to populate on program module begins(Dipak Rathod)

	public function getLearningThemeName($master_program_id) {

		$learning_themes = DB::table('learning_themes')->select('id', 'name')->where('master_program_id', $master_program_id)->get();

		return $learning_themes;
	}
// ==============Get learning themes to populate on program module ends(Dipak Rathod)

// =====================Program for create master program ajax call begins (Dipak R)
	public function storeMasterProgram(Request $request) {
		// dd('$validator');
		// dd($request->all());
		$masterProgramExists = DB::table('master_programs')->where('name', $request->master_program_name)->first();
		// dd($masterProgramExists!=null);
		if ($masterProgramExists != null) {
			// dd('Already Esisst');
			return view('master_program.create')->with("warning", "Master Program with this name has already been taken.");
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('MasterProgram');
			$data = $request->all();
			// dd($rules);
			if ($request->ajax()) {
				$request->validate($rules);
			} else {
				$validator = Validator::make($data, $rules);
				if ($validator->fails()) {
					// dd($validator);
					return redirect('masterProgram/create')
						->withErrors($validator)
						->withInput();
				}
			}

			$masterProgramFlag = MasterProgramService::saveMasterProgram($data);

			if ($masterProgramFlag == True) {
				return redirect()->action('MasterProgramController@index');
			} else {
				Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
			}
		} catch (Exception $e) {
			Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
		}
	}
// =====================Program for create master program ajax call ends (Dipak R)

// =====================Program for create learning theme ajax call begins (Dipak R)
	public function storeLearningTheme(Request $request) {
		// dd('$validator');
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try {
			$valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
			$rules = $valdationRulesService->validationRulesWithTenant('LearningTheme');
			$data = $request->all();
			// dd($rules);
			if ($request->ajax()) {
				$request->validate($rules);
			} else {
				$validator = Validator::make($data, $rules);
				if ($validator->fails()) {
					// dd($validator);
					return redirect('learningTheme/create')
						->withErrors($validator)
						->withInput();
				}
			}
			// dd('H');
			$learning_theme_flag = LearningThemeService::saveLearningTheme($data);

			if ($learning_theme_flag == True) {
				return redirect()->action('LearningThemeController@index');
			} else {
				Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
			}
		} catch (Exception $e) {
			Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
		}
	}
// =====================Program for create learning theme ajax call ends (Dipak R)

	public function getRolesList() {
		try {
			$roles = Role::select('id', 'name')->get();
			return $roles;
		} catch (Exception $e) {
			Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
		}
	}

	public function getProgramRolePerson($company_id, $role_id) {
		try {
			//
			$return_data = self::getRolePerson($company_id, $role_id);
			return $return_data;

		} catch (Exception $e) {
			Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
		}
	}

	public function getRolePerson($company_id, $role_id) {
		//
		$company_party_id = Organization::where('id', $company_id)->first()->party_id;
		$roleName = Role::where('id', $role_id)->first()->name;

		$return_data = [];
		switch ($roleName) {
		case 'Program Manager':
			//
			$pm_role_id = Role::where('name', 'Program Manager')->first()->id;
			$pm = self::getUserDetailByRole($company_party_id, $pm_role_id);
			Log::error($pm->id);
			$return_data[0]['id'] = $pm->id;
			$return_data[0]['first_name'] = $pm['first_name'];
			$return_data[0]['last_name'] = $pm['last_name'];

			break;

		case 'Company Coordinator':
			//
			$cc_role_id = Role::where('name', 'Company Coordinator')->first()->id;
			$cc = self::getUserDetailByRole($company_party_id, $cc_role_id);
			$return_data[0]['id'] = $cc['id'];
			$return_data[0]['first_name'] = $cc['first_name'];
			$return_data[0]['last_name'] = $cc['last_name'];

			break;

		default:
			$return_data = [];
			break;
		}

		return $return_data;
	}

	public static function getUserDetailByRole($party_id, $role_id) {

		$ret = Role::with(['users.party.person' => function ($query) use ($party_id) {$query->where(['customer_party_id' => $party_id]);}])->where('id', '=', $role_id)->get();
		$persons = array();
		foreach ($ret as $key => $value) {
			foreach ($value['users'] as $k => $v) {
				if (!empty($v->party->person)) {
					$persons = $v->party->user;
				}
			}
		}
		return $persons;
	}

}
