<?php

namespace vnnogile\Utilities\Services;

//Custom Imports Below
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
    use vnnogile\Utilities\Interfaces\StorageInterface;

/**
 * This is a a common utility method for file upload
 *
 * All file upload controllers can use this trait to 
 * upload files to the server
 */
class LocalStorageUtility implements StorageInterface
{

    public function moveFile($fileName, $sourceFolder, $destinationFolder){
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return Storage::move($sourceFolder . $fileName, $destinationFolder . $fileName);
    }
	public function copyFile($fileName, $sourceFolder, $destinationFolder) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return Storage::copy($sourceFolder . $fileName, $destinationFolder . $fileName);
	}

    public function deleteFile($file){
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");    	
        return Storage::delete($file);
    }

    public function deleteDirectory($directory){
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
    	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");    	
        return Storage::deleteDirectory($directory);
    }
public function createFile($folder, $file, $contents){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        Storage::put($folder . '/' . $file, $contents);
    }

    /**
     *
     * @param Request $request
     * @param string $folder
     *
     * @return array $uploadedFiles
     */
    public function uploadFileToPublicStorage(Request $request, $folder){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $data=$request->all();
        $uploadedFiles = self::uploadFiles($request, $folder, 'public');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $uploadedFiles;
    }

    public function uploadFileToPrivateStorage(Request $request, $folder){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $uploadedFiles = self::uploadFiles($request, $folder, 'private');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $uploadedFiles;
    }

    private function uploadFiles(Request $request, $folder, $mode){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $allFiles  = $request->allFiles();
        $d=$request->all();
        $uploadedFiles = array();
        $original_file_names = array();
        Log::debug("*******IN FILE UPLOAD***********");	
	Log::debug("FOLDER:".$folder);
        foreach ($allFiles as $key => $value) {       
            Log::debug(print_r($key, true));
    	Log::debug(print_r($value, true));
            if (is_array($request->$key)){
                foreach ($request->$key as $file){
                    // finding temp name
                    $originalFileName = $file->getClientOriginalName();
                    $file_name_array = explode('.', $originalFileName);
                    $file_extension = array_pop($file_name_array);
                    $tempFileName = uniqid().".".$file_extension;

                    if($mode == 'private'){
                        $uploadedFlag = $file->storeAs($folder, $tempFileName);    
                    }else {
                        $uploadedFlag = $file->storeAs('public/' . $folder, $tempFileName);    
                    }
                    Log::debug('Uploading ' . $originalFileName);
                    if ($uploadedFlag){
                        $uploadedFiles[$key][] = $tempFileName;
                        $original_file_names[$key][] = $originalFileName;
                        Log::debug('Uploaded ' . $originalFileName);
                    }
                }
            }else{
                $file = $request->$key;
                $originalFileName = $file->getClientOriginalName();
                $file_name_array = explode('.', $originalFileName);
                $file_extension = array_pop($file_name_array);
                $tempFileName = uniqid().".".$file_extension;
                
                if($mode == 'private'){
                    $uploadedFlag = $file->storeAs($folder, $tempFileName);    
                }else {
                    $uploadedFlag = $file->storeAs('public/' . $folder, $tempFileName);    
                }
                Log::debug('Uploading ' . $originalFileName);
                if ($uploadedFlag){
                    $uploadedFiles[$key][] = $tempFileName;
                    $original_file_names[$key][] = $originalFileName;
                    Log::debug('Uploaded ' . $originalFileName);
                }
            }
        }               
        $uploadInfo['uploadedFileNames'] = $uploadedFiles;
        $uploadInfo['originalFileNames'] = $original_file_names;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $uploadInfo;
    }
}
