<?php

namespace vnnogile\Sms\Services;

//Custom Imports Below
use App\Models\Configuration;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Log;
use vnnogile\Utilities\Services\ConstantsUtility;

/**
 * This is a a common utility method for email
 *
 * All file upload controllers can use this trait to 
 * upload files to the server
 */
class NetCoreSmsImplementationService
{
    /**
     * Send email with or without attachments
     * @param Array $smsData
     * @param Array $masterDataInterface
     * @param Array $utilsInterface
     * @return Array $status
     */
    public static function sendSms($message, $to){
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);

        //Get the appropriate SMS implementation class
        $netCoreConfigurations = self::retrieveNetcoreConfiguration();
        $usr='';
        $pwd='';
        $sender='';
        $smsFeedId='';
        foreach($netCoreConfigurations as $netCoreConfiguration){
            switch ($netCoreConfiguration->config_key) {
                case "netcore_usr":
                    $usr=$netCoreConfiguration->config_value;
                    break;
                case "netcore_pwd":
                    $pwd=$netCoreConfiguration->config_value;
                    $crypt = new Encrypter(config("cool.stuff"));
                    $pwd = $crypt->decrypt(substr($pwd, 4));
                    break;
                case "netcore_sender":
                    $sender=$netCoreConfiguration->config_value;
                    break;
                case "netcore_feed":
                    $smsFeedId=$netCoreConfiguration->config_value;
                    break;
                default:
                    break;
            }
        }

        //Netcore configuration data
        $url = 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=<FEEDID>&username=<USERNAME>&password=<PASSWORD>&To=<TO>&Text=<TEXT>&time=&senderid=<SENDERID>';
        $url = str_replace('<FEEDID>', $smsFeedId, $url);
        $url = str_replace('<USERNAME>', $usr, $url);
        $url = str_replace('<PASSWORD>', $pwd, $url);
        $url = str_replace('<SENDERID>', $sender, $url);

        //Variable Data
        $url = str_replace('<TO>', $to, $url);
        $url = str_replace('<TEXT>', urlencode($message), $url);

        Log::debug('message');
        Log::debug($message);
        Log::debug($url);

        $returnValue =  self::invokeSmsApi($url);
        Log::debug($returnValue);

        $transactionId = 1;
        try{
            $xml=simplexml_load_string($returnValue) or die("Error: Cannot create object");
            $transactionIdObj = $xml->MID['TID'];
            $transactionId = $transactionIdObj['0'];
            print_r((String)$transactionId["0"]);
        }catch (Exception $e) {
            Log::error('Exception while parsing SMS transaction :: ', $e->getMessage());
        }        

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return ((string)$transactionId["0"]);
    }

    private static function invokeSmsApi($url){
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
        return $output;
    }

    public static function retrieveNetcoreConfiguration(){
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
        $configurations = null;
        try{
            $configurations = Configuration::where('config_key', 'like', 'netcore_%')->get();
        }catch (\Exception $exception) {
            Log::error(__LINE__ . 'Exception occurred ' . $exception->getMessage());
        } catch (\Throwable $exception) {
            Log::error(__LINE__ . 'Throwable exception occurred ' . $exception->getMessage());
        }
        
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
        return $configurations;
    }


}
