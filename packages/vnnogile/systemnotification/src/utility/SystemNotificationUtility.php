<?php

namespace vnnogile\SystemNotification\Utility;

//Custom Imports Below
use vnnogile\SystemNotification\Jobs\SystemNotificationJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\User;
/**
 * This is a a common utility method for email
 *
 * All file upload controllers can use this trait to 
 * upload files to the server
 */
class SystemNotificationUtility
{
    /**
     * Send email with or without attachments
     * @param Array $notificationData
     * @return Array $status
     */
    public static function sendCommunicationMobileNotification($role_data){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        foreach ($role_data as $id => $value) {

            $notificationData['template_name']=$value['template'];
            $notificationData['data']=$value['template_data'];
            $notificationData['subject']=$value['template_subject'];
            $notificationData['to']=$value['email_id'];
            $notificationData['mobile_token'] = '';
            //get user mobile token
            $mobile_token = User::select('notification_token')->where('userid',$notificationData['data']['USER_ID'])->first();
            if(!empty($mobile_token->notification_token)){
                $notificationData['mobile_token'] = $mobile_token->notification_token; 
            }
            
            // dd($notificationData);
            //trail data
            $notificationData['trail_data']['login_id']=$value['login_id'];
            if (array_key_exists('program_code', $value)){
                $notificationData['trail_data']['program_code']=$value['program_code'];    
            }
            if (array_key_exists('customer_organization_code', $value)){
                $notificationData['trail_data']['customer_organization_code']=$value['customer_organization_code'];
            }
            if (array_key_exists('job_id', $value)){
                $notificationData['trail_data']['job_id']=$value['job_id'];
            }
            if (array_key_exists('party_id', $value)){
                $notificationData['trail_data']['party_id']=$value['party_id'];
            }

            $notificationData['trail_data']['communication_event_id']=$value['communication_event_id'];
            $notificationData['trail_data']['tenant_code']=$value['tenant_code'];
            // dd($notificationData);
            SystemNotificationJob::dispatch($notificationData)->onQueue('systemnotifications');  
            // Log::debug("email sent in job for".$id);  
            // dd("FINISH");
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return "success";

    }   
}
