@extends('user.user')

@section('editID','/'.$data->id)
@section('update-nav-bar', 'hide')
@section('view-nav-bar', 'hide')
@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('organization_name', $org_id)
@section('assign_role_id', $sel_role_id)

@section('firstName', $data->first_name)
@section('lastName',$data->last_name)


@section('primaryContact',trim($contact[0]['phone_number']))

@if(isset($contact[1]['phone_number']))  
  @section('secondaryContact',trim($contact[1]['phone_number']))
@endif


@section('email',$data->email)
@section('old_userId',$data->userid)

<!-- abhi 26/03/19 -->
@section('dob',$data->dob)

@section('edit_btn','hide')


@push('scripts')

<script>
$(document).ready(function() {

  $('#file').removeClass("mandatory");
  $('#photo_div_id').removeClass('required');
  $("#company_id").attr("readonly", true);
  $("#assign_role_id").attr("readonly", true);

});

//
@if ($data['gender'] == 'M') {
     $("[name=gender]").val(["M"]);
}
@else{
     $("[name=gender]").val(["F"]);
}
@endif

//abhi 26/03/19
@if ($data['is_private'] == 1) {
     $("[name=profile]").val(["private"]);
}
@else{
     $("[name=profile]").val(["public"]);
}
@endif

//for making fields mandotory
@if($data['userid']){
       $("#userid").addClass("mandatory");
}
@endif
@if($data['department']){
       $("#department").addClass("mandatory");
}
@endif
@if($data['personal_email']){
       $("#personalEmail").addClass("mandatory");
}
@endif
@if($data['linkedin_profile']){
       $("#linkedinProfile").addClass("mandatory");
}
@endif
@if($data['facebook_id']){
       $("#facebookId").addClass("mandatory");
}
@endif
@if($data['google_profile']){
       $("#googlePlus").addClass("mandatory");
}
@endif
@if($data['twitter_handle']){
       $("#twitterHandle").addClass("mandatory");
}
@endif

///// For facilitator
console.log('facilitator');
console.log({{$facilitator}});
 @if($facilitator==1){
     $('#facilitator_check_id').attr('checked',true);
  }
 @endif

</script>

@endpush
