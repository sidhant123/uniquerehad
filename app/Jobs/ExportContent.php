<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Custom Imports
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use vnnogile\Utilities\Services\ConstantsUtility;

class ExportContent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $tenantPartyId;
    public $tries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tenantPartyId)
    {
        $this->tenantPartyId = $tenantPartyId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
        Excel::create('Content', function($excel)  {

            $excel->sheet('Content', function($sheet) use($excel){
                  // Set the title
                  $excel->setTitle('CGX Application - Content Dump')
                        ->setCreator(ConstantsUtility::COMPANY_SHORT_NAME)
                        ->setCompany(ConstantsUtility::COMPANY_NAME)
                        ->setDescription('Content and content ratings Dump');

                  $documentCount = DB::table('documents')->count();
                  //Form the query
                  $query = 'select  d.id, d.name, d.title, d.document_path, d.summary, d.duration, d.pages, d.extension, testavg from documents d
                      left join (select artefact_type_id,entity_id, avg(rating) testavg from content_ratings group by artefact_type_id,entity_id) cr
                on (cr.entity_id = d.id and cr.artefact_type_id = d.artefact_type_id)
                order by d.id asc';
                  $query = '(' . $query . ') somealias';

                  //Form the header and print the header
                  $keys = array("Id", "Name", "Title", "Path", "Summary", "Duration", "Pages", "Extension" ,"Avg Content Rating");
                  $sheet->appendRow($keys);
                  $sheet->setAutoFilter();
                  $sheet->row(1, function($row) {
                      // call cell manipulation methods
                      $row->setBackground('#000000');
                      $row->setFontFamily('Trebuchet MS');
                      $row->setFontColor('#ffffff');
                  });

                  // Fire query and the sheet manipulation
                  DB::table(DB::raw($query))->orderBy('id')->chunk(50, function($rows) use($documentCount, $sheet){
                      foreach ($rows as $row) {
                          $rowArray = array($row->id, $row->name, $row->title, $row->document_path, $row->summary, $row->duration, $row->pages, $row->extension, $row->testavg);
                          $sheet->appendRow($rowArray);
                      }
              });
            });

        })->store('xlsx');
        Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
    }
}
