<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 09 Jan 2018 12:42:51 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FilterMaster
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property string $function_name
 * @property string $route_name
 * @property int $row
 * @property int $sequence
 * @property string $control_type
 * @property string $control_id
 * @property string $control_text
 * @property array $control_data
 * @property string $control_name
 * @property string $data_url
 * @property string $load_event
 * @property int $action
 *
 * @package App\Models
 */
class FilterMaster extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
		'row' => 'int',
		'sequence' => 'int',
		'control_data' => 'json',
		'action' => 'int'
	];

	protected $fillable = [
		'created_by',
		'updated_by',
		'deleted_by',
		'function_name',
		'route_name',
		'row',
		'sequence',
		'control_type',
		'control_id',
		'control_text',
		'control_data',
		'control_name',
		'data_url',
		'load_event',
		'action',
		'label_css',
		'field_css'
	];
}
