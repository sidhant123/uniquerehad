<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 25 May 2018 19:23:58 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramLtTopic
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $available_from
 * @property \Carbon\Carbon $available_to
 * @property int $sequence
 * @property string $color
 * @property float $expected_duration
 * @property int $parent_topic_id
 * @property int $container_program_id
 * @property array $details
 * @property int $artefact_type_id
 * @property int $critical
 * @property string $icon
 * @property int $alert_before_nodes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @property \App\Models\Topic $topic
 *
 * @package App\Models
 */
class ProgramLtTopic extends Eloquent {
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'expected_duration' => 'float',
		'parent_topic_id' => 'int',
		'container_program_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
	];

	protected $dates = [
		'available_from',
		'available_to',
	];

	protected $fillable = [
		'name',
		'description',
		'available_from',
		'available_to',
		'sequence',
		'color',
		'expected_duration',
		'parent_topic_id',
		'container_program_id',
		'details',
		'artefact_type_id',
		'critical',
		'icon',
		'alert_before_nodes',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function subtopics()
	{
		return $this->hasMany(\App\Models\ProgramLtTopic::class, 'parent_topic_id');
	}
	public function getAvailableFromAttribute() {
		// return date('d-m-Y', strtotime($this->attributes['start_date']));
		if ($this->attributes['available_from'] != null) {
			return \Carbon\Carbon::parse($this->attributes['available_from'])->format('d-m-Y H:i');
		} else {
			return null;
		}
		// return \Carbon\Carbon::parse($this->attributes['available_from'])->format('d-m-Y');
	}

	public function getAvailableToAttribute() {
		// return date('d-m-Y', strtotime($this->attributes['expiry_date']));
		if ($this->attributes['available_to'] != null) {
			return \Carbon\Carbon::parse($this->attributes['available_to'])->format('d-m-Y H:i');
		} else {
			return null;
		}
		// return \Carbon\Carbon::parse($this->attributes['available_to'])->format('d-m-Y');
	}
}
