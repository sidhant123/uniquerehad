<?php
namespace vnnogile\Mailer\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
class EmailAddressController extends Controller
{
    function getEmailData(){
    	$emails = UserService::getUserEmailIDs();
    	return $emails;
    }
}
