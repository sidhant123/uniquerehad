<?php

namespace vnnogile\Pagination\Utilities;

//Custom Imports Below
use App;
use App\Services\ConfigurationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Cache;
use Config;

/**
 * This is a a common utility method for handling PDF files
 *
 * All controllers that want to generate PDF files from views
 * or specific HTML should use this class.
 *
 * PDFS can be downloaded or viewed inline within the browser
 * if the browser supports viewing of documents inline
 * 
 * PDF file can be saved locally and sent in email.
 */

class PaginationUtility
{
    /**
     *
     * @param Request $request
     * @param string $html
     *
     * @return PDF
     */
    public static function getRowCountAndRoutesData(Request $request,$route, $utilsInterface, $masterDataInterface){
        Log::debug('PaginationUtility::generatePaginationData started');
        // $request = request();
        $rowCountList = $utilsInterface->getRowCountList($masterDataInterface);
        if($request->has('rowCount')){
        $rowCount = $request['rowCount'];
        Cache::put('rowCount', $rowCount, 10);
        }
        else if(Cache::get('rowCount')){
        $rowCount = Cache::get('rowCount'); 
        }
        else{
        $rowCount = $rowCountList['default_row_count'];
        }
        $getCurrentRoute = str_replace(Config::get('app.url').':8000','', url()->current());
        Log::debug("************getCurrentRoute*************");
        Log::debug("$route");
        $dependentData['route'] = $route;
        $dependentData['current_route'] = $getCurrentRoute;
        $dependentData['rowCount'] = $rowCount;
        Log::debug('PaginationUtility::generatePaginationData ended');
        return $dependentData;
    }    
}
