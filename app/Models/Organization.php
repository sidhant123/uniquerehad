<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;
use Sofa\Eloquence\Eloquence;
/**
 * Class Organization
 * 
 * @property int $id
 * @property string $name
 * @property string $organization_code
 * @property string $website_url
 * @property string $logo
 * @property int $party_id
 * @property int $tenant_party_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Party $party
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Organization extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use Eloquence;
	protected $searchableColumns = ['name','organization_code'];
	protected $casts = [
		'party_id' => 'int',
		'tenant_party_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'organization_code',
		'website_url',
		'logo',
		'party_id',
		'tenant_party_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function party()
	{
		return $this->belongsTo(\App\Models\Party::class, 'tenant_party_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\User::class, 'deleted_by');
	}

	public function person()
	{
		return $this->hasMany(\App\Models\Person::class, 'customer_party_id','party_id');
	}

	public function customer_exercise_banks()
	{
	    return $this->hasMany(\App\Models\CustomerExerciseBank::class, 'customer_party_id', 'party_id');
	} 

}
