
<div id="navbar">
  <a id="save_link" @yield('save-nav-bar')><i class="vnn vnn-save active"></i></a>
  <a class="@yield('update-nav-bar')" href="{{ $url }}/@yield('editID')/edit"><i class="vnn vnn-edit"></i></a>
  <a  class="@yield('view-nav-bar')" href="{{ $url }}/@yield('editID')"><i class="vnn vnn-edit"></i></a>
  <a  href="{{ $url }}"><i class="vnn vnn-listview"></i></a>
</div>
 @push('scripts')
<style type="text/css">
#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}
#navbar a {
  float: right;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}
#navbar a:hover {
  background-color: #ddd;
  color: black;
}
#navbar a.active {
  background-color: #4CAF50;
  color: white;
}
.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 78.5%;
}

.sticky + .content {
  padding-top: 60px;
}
</style>


<script>
    var submit = document.getElementById('{{ $save_btn }}');
    console.log(submit)
    var nav_save_btn = document.getElementById("save_link");
        nav_save_btn.addEventListener("click", function () {
        submit.click();
        nav_save_btn.classList.add("disabled");
      setTimeout(function () {
        nav_save_btn.classList.remove("disabled");
      }, 2000);
});
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
 @endpush
