<?php

namespace App\Listeners;

use App\Events\ExcelUploadEvent;
use App\Models\ImportJob;
use App\Models\Organization;
use App\Services\TenantService;

//Custom Imports Below
use App\Traits\ValidationTrait;
use App\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

class ExcelUploadEventListener implements ShouldQueue
{

    use ValidationTrait;

    protected $masterDataInterface;
    protected $utilsInterface;
    protected $request;
    public $tries = 1;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  ExcelUploadEvent  $event
     * @return void
     */
    public function handle(ExcelUploadEvent $event)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug("****Event Name****** " . $event->entityService);
        Log::debug($event->entityService);
        Log::debug(print_r($event, true));
        $folder = $event->folder;
        $fileName = $event->fileName;
        $uploadedFile = $event->uploadedFile;
        $entityName = $event->entityName;
        $validationRules = $event->validationRules;
        $userId = $event->userId;
        $importedRecords = 0;
        $errorRecords = 0;
        $entityService = app()->make($event->entityService);
        $saveMethod = $event->saveMethod;
        $keys = array();
        $file = storage_path() . '/' . $folder . '/' . $uploadedFile;

        $excel = Excel::load($file)->all()->toArray();
        $data = $excel[1];
        Log::debug($data);
        $authenticatedUser = User::find($userId);
        $tenantPartyId = TenantService::getTenantPartyIdForAuthUser($this->utilsInterface, $authenticatedUser);
        Log::debug('*****BEFORE TENENTPARTY************************');
        try {

            // $organization_code = $excel[1][0]['customer_code'];
            // Log::debug($organization_code);
            // $org = Organization::where(
            //     [
            //         ['organization_code', '=', $organization_code],
            //         ['tenant_party_id', '=', $tenantPartyId],
            //     ])->first();
            // Log::debug($org);
            //validation of users
            Log::debug('*****AFTER TENENT PARTY************************');
            $event->organizationCode = '';//$organization_code;
            $event->organizationName = '';
            // if (!empty($org)) {
            //     $event->organizationName = $org->name;
            // }

            Log::debug('*****rule ************************ ');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $newRecord = $value;
                    $validationResult = $this->validate($newRecord, $validationRules);

                    if ($validationResult['success'] == true) {
                        Log::debug('*****validation success ************************ ');
                        $validRecords[] = $value;
                    } elseif ($validationResult['success'] == false) {
                        Log::debug('*****validation fail ************************ ');
                        $value["errors"] = $validationResult['errors'];
                        $invalidRecords[] = $value;
                        foreach ($value as $key => $val) {
                            if (array_key_exists($key, $keys)) {
                            } else {
                                array_push($keys, $key);
                            }
                        }
                    }
                }
            }

            //DKDKDK -- Call dynamic class
            if (isset($validRecords)) {
                Log::debug('*****VALID RECORDS***');
                foreach ($validRecords as $validRecord) {
                    $data = $validRecord;
                    foreach ($data as $key => $value) {
                        $data[self::underscoreToCamelCase($key)] = $value;
                    }
                    //the naming convention for organization_name is not OrganizationName
                    //since when user is created through screen, this is the way
                    // $data['customer_party_id'] = $org->party_id;
                    $data['utilsInterface'] = $this->utilsInterface;
                    $data['masterDataInterface'] = $this->masterDataInterface;
                    $data['authenticatedUser'] = $authenticatedUser;
                    if ($event->referenceID > 0) {
                        $data[$event->referenceFieldName] = $event->referenceID;
                    }
                    $entityService->$saveMethod($data);
                    $importedRecords = $importedRecords + 1;
                }
            }

            if (isset($invalidRecords)) {
                Log::debug('*****INVALID RECORDS***');
                Log::debug($invalidRecords);
            }
            $errorFileName = '1';
            if (isset($invalidRecords)) {
                if (!empty($invalidRecords)) {
                    Log::debug('*****INVALID RECORDS IN LOOP 1 ***');
                    $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
                    $errorFileName = trim($fileName, '.xlsx') . '_errors_' . $timeNow;
                    Excel::create($errorFileName, function ($excel) use ($invalidRecords, $keys, &$errorRecords, $entityName) {
                        Log::debug('*****INVALID RECORDS IN LOOP 2 ***');
                        // Set the title
                        $excel->setTitle('Errorneous Records');
                        // Chain the setters
                        $excel->setCreator('Cognegix')
                            ->setCompany('Cognegix');
                        // Call them separately
                        $excel->setDescription($entityName . ' Upload Error Records');
                        $excel->sheet('Invalid ' . $entityName, function ($sheet) use ($invalidRecords, $keys, &$errorRecords) {
                            Log::debug('*****INVALID RECORDS IN LOOP 3 ***');
                            $sheet->row(1, $keys);
                            $counter = 2;
                            Log::debug('*****INVALID RECORDS IN LOOP 4 ***');
                            foreach ($invalidRecords as $invalidData) {
                                $sheet->row($counter, $invalidData);
                                $counter++;
                                $errorRecords = $errorRecords + 1;
                            }
                        });
                    })->store('xlsx', storage_path($event->finalPath));
                    //in above code, we need to give '/app/' other wise file will get saved under different path directly below
                    //storage folder instead of 'storage/app'
                }
            }

            $moveFile['uploadedFileName'] = $event->uploadedFile;
            $moveFile['tempPath'] = $event->tempPath;
            $moveFile['finalPath'] = $event->finalPath;
            $configurations = $this->masterDataInterface->all('Configuration');
            foreach ($configurations as $configuration) {
                $configurationsMap[$configuration->configKey] = $configuration->configValue;
            }
            $moveFile['configurations'] = $configurationsMap;
            Log::debug('****moveFile****');
            $entityService->moveFile($moveFile);
            $entityService->deleteDirectory($moveFile, $event->tempPath);
            //Finally set the status
            $importJob = ImportJob::find($event->jobId);
            if ($errorFileName != 1) {
                $importJob->error_file = $event->finalPath . $errorFileName . '.xlsx';
            }

            $importJob->status = "COMPLETED";
            $importJob->save();
            $data['masterDataInterface'] = $this->masterDataInterface;
            $data['utilsInterface'] = $this->utilsInterface;
            $data['authenticatedUser'] = $authenticatedUser;
            $data['fileName'] = $event->fileName;
            if ($errorFileName != 1) {
                $data['errorFileName'] = $importJob->error_file;
            }
            $data['totalRecords'] = $importedRecords + $errorRecords;
            $data['importedRecords'] = $importedRecords;
            $data['errorRecords'] = $errorRecords;
            $data['jobId'] = $event->jobId;
            $data['organizationCode'] = $event->organizationCode;
            $data['organizationName'] = $event->organizationName;
            $entityService->sendEmail($entityName . 'ImportFinishEvent', $data);

        } catch (Exception $exception) {
            Log::error('Exception occurred ' . $exception->getMessage());
            $this->sendEmail($event, '', $entityService);
        } catch (\Throwable $exception) {
            Log::error('Throwable exception occurred ' . $exception->getMessage());
            $this->sendEmail($event, '', $entityService);
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    private static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
        return $str;
    }
    private function sendEmail($event, $errorMessage = 'Unknown Error', $entityService)
    {
        $data['errorMessage'] = $errorMessage;
        $data['masterDataInterface'] = $this->masterDataInterface;
        $data['utilsInterface'] = $this->utilsInterface;
        $data['authenticatedUser'] = $authenticatedUser = User::find($event->userId);
        $data['fileName'] = $event->fileName;
        $data['totalRecords'] = 0;
        $data['importedRecords'] = 0;
        $data['errorRecords'] = 0;
        $data['jobId'] = $event->jobId;
        $data['organizationCode'] = $event->organizationCode;
        if (isset($event->organizationName)) {
            $data['organizationName'] = $event->organizationName;
        } else {
            $data['errorMessage'] = "Customer code specified in the Excel file is invalid";
        }
        $importJob = ImportJob::find($event->jobId);
        $importJob->status = "FAILED";
        $importJob->save();
        $entityService->sendEmail($event->entityName . 'ImportFailEvent', $data);
    }
}
