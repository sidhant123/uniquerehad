<?php

namespace vnnogile\Pagination\Utilities;

//Custom Imports Below
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Tags\services\TagService;

/**
 * This is a a common utility method for handling PDF files
 *
 * All controllers that want to generate PDF files from views
 * or specific HTML should use this class.
 *
 * PDFS can be downloaded or viewed inline within the browser
 * if the browser supports viewing of documents inline
 *
 * PDF file can be saved locally and sent in email.
 */

class ListingUtility
{
    /**
     *
     * @param Request $request
     * @param string $html
     *
     * @return PDF
     */
    public static function generateQueryForListView(Request $request, $records, $displayColumnNames, $filterColumnNames, $masterDataInterface, $utilsInterface, $entityName, $filterClass)
    {
        Log::debug('ListingUtility::generateQueryForListView started');
        // dd($filterClass);
        if (empty($filterClass)) {
            if ($request->has('search')) {
                $isTaggable = $utilsInterface->isTaggable($masterDataInterface, ucfirst($entityName));
                Log::debug($isTaggable);
                if ($isTaggable) {
                    Log::debug("**************IS TAGGABLE*************");
                    $records = TagService::searchTagsByEntity($request['search'], ucfirst($entityName), 0, true);
                } else {
                    Log::debug("**************NOT TAGGABLE*************");
                    Log::debug("**************IN SEARCHABLE*************");
                    $records = $records::search($request['search']);
                }

                // Log::debug($records->get());
            } else {
                Log::debug("**************NOT TAGGABLE*************");
                Log::debug("**************NOT SEARCHABLE*************");
                $records = $records->select($displayColumnNames);
            }
        }
        $queries = [];

        if (empty($filterClass)) {
            foreach ($filterColumnNames as $columnName) {

                if ($request->has($columnName)) {
                    if ($request[$columnName] > 0) {
                        $records = $records->where($columnName, $request[$columnName]);
                        $queries[$columnName] = $request->$columnName;
                    }
                }
            }
        } else {
            // if ($request->has('search')) {
            $filterClass = new $filterClass;
            $isTaggable = $utilsInterface->isTaggable($masterDataInterface, ucfirst($entityName));
            $records = $filterClass->filter($request, $filterColumnNames, $records, $isTaggable);
            // }
        }
        if (empty($filterClass)) {
            if ($request->has('sort')) {
                $records = $records->orderBy($request['sort_column_name'], $request['sort']);
                $queries['sort'] = $request['sort'];
            }
        }
        Log::debug('ListingUtility::generateQueryForListView finished');
        $data['records'] = $records;
        $data['queries'] = $queries;
        //$data['with'] = 'communication_channel';
        return $data;
    }
}
