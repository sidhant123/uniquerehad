<?php

// cssir

namespace App\Services;

//Custom imports
use App\Models\ArtefactType;
use App\Models\LearningTree;
use App\Models\Program;
use App\Models\Topic;
use DB;
use Illuminate\Support\Facades\Log;

class ProgramLTToMasterLTService {

	var $storage_utility_class_name;

	public static function saveProgramLtAsMasterLt($programId, $contentToBeCopied, $userId, $learningTreeName, $learningTreeCode, $learningTreeDescription, $masterDataInterface) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		// Log::debug($contentToBeCopied);
		// exit();
		DB::beginTransaction();
		try {

			// for copying photo_path and photo
			$configurations = $masterDataInterface->all('Configuration');
			foreach ($configurations as $configuration) {
				$configurationsMap[$configuration->configKey] = $configuration->configValue;
			}
			$data['configurations'] = $configurationsMap;
			$class = $data['configurations']['storage_implementation_class'];
			$GLOBALS['storage_utility_class_name'] = $class;

			$program = self::getProgramName($programId);
			$allProgramHierarchy = array();
			$allProgramHierarchy = self::getTopics($program, $allProgramHierarchy);
			// dump($allProgramHierarchy);
			// exit();

			//Now we got the program information - so let us now go to insert the learning tree
			$learningTree = self::createLearningTreeHeader($program, $learningTreeName, $learningTreeCode, $learningTreeDescription, $userId);

			$virtual_artefact = ArtefactType::where('name', 'Virtual Classroom')->first();
			self::createLearningTreeTopics($learningTree, $contentToBeCopied, $allProgramHierarchy, $userId, $virtual_artefact['id']);

			DB::commit();
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return "success";
		} catch (\Exception $exception) {
			Log::error("Exception while saving Program Learning Tree into masters " . $exception->getMessage());
			DB::rollback();
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return "success";
	}

	private static function createLearningTreeHeader($program, $learningTreeName, $learningTreeCode, $learningTreeDescription, $userId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$learningTree = new LearningTree();
		$learningTree->name = $learningTreeName;
		$learningTree->learning_tree_code = $learningTreeCode;
		$learningTree->description = $learningTreeDescription;
		$learningTree->created_by = $userId;
		$learningTree->updated_by = $userId;
		$learningTree->tenant_party_id = $program->tenant_party_id;
		$learningTree->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $learningTree;
	}

	private static function createLearningTreeTopics($learningTree, $contentToBeCopied, $topics, $userId, $virtual_artefact_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$idMap = array();

		try {
			foreach ($topics as $topic) {
				Log::debug($topic);
				$modifiedTopic = new Topic();
				$modifiedTopic->name = $topic->name;
				$modifiedTopic->description = $topic->description;
				$modifiedTopic->sequence = $topic->sequence;
				$modifiedTopic->color = $topic->color;
				$modifiedTopic->expected_duration = $topic->expected_duration;

				// Log::debug($topic->parent_topic_id);
				// if ($topic->details == '') {
				// 	Log::debug('chekc');
				// }
				// exit();

				if ($topic->details == null) {
					$modifiedTopic->details = $topic->details;
				} else {
					if ($contentToBeCopied != 'N') {
						if ($topic->artefact_type_id == $virtual_artefact_id) {
							//
							$details_decode = json_decode($topic->details, true);
							$details_to_copy['entity_type'] = $details_decode['entity_type'];
							$details_to_copy['artefact_type_id'] = $details_decode['artefact_type_id'];
							$modifiedTopic->details = json_encode($details_to_copy);
						} else {
							$modifiedTopic->details = $topic->details;
						}
					} else {
						//
						$details_decode = json_decode($topic->details, true);
						$details_to_copy['entity_type'] = $details_decode['entity_type'];
						$details_to_copy['artefact_type_id'] = $details_decode['artefact_type_id'];
						$modifiedTopic->details = json_encode($details_to_copy);
					}
				}

				//This will be the new parent ID of the topic
				if (array_key_exists($topic->parent_topic_id, $idMap)) {
					$modifiedTopic->parent_topic_id = $idMap[$topic->parent_topic_id];
				}

				$modifiedTopic->container_learning_tree_id = $learningTree->id;

				$modifiedTopic->critical = $topic->critical;
				$modifiedTopic->alert_before_nodes = $topic->alert_before_nodes;
				// $modifiedTopic->icon = $topic->icon;
				$modifiedTopic->optional = $topic->optional;
				$modifiedTopic->topics_to_complete = $topic->topics_to_complete;
				$modifiedTopic->created_by = $userId;
				$modifiedTopic->updated_by = $userId;
				$modifiedTopic->save();
				$idMap[$topic->id] = $modifiedTopic->id;

				$topic_id = $modifiedTopic->id;
				//setting icon
				if ($topic['photo'] != null) {
					self::copyLTPhoto($topic['photo'], $topic['photo_path'], $topic_id, $learningTree['id']);
				}
			}
		} catch (Exception $e) {
			Log::error('Exception while creating program :: ', $e->getMessage());
		}

		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return "success";
	}

	private static function getTopics($program, $alltopics, $parentTopicId = "NOTSET") {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("**parentTopicId*** " . $parentTopicId);
		/*if ($parentTopicId == 110){
			      dump($alltopics);
			      exit();
		*/
		//Log::debug($alltopics);
		if ($parentTopicId == "NOTSET") {
			$topics = $program->topics()->whereNull('parent_topic_id')->get();
		} else {
			$topics = $program->topics()->where('parent_topic_id', $parentTopicId)->get();
		}

		foreach ($topics as $topic) {
			$alltopics[$topic->id] = $topic;
			$alltopics = self::getTopics($program, $alltopics, $topic->id);
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $alltopics;
	}

	private static function getProgramName($programId) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return Program::findOrFail($programId);

	}

	private static function copyLTPhoto($fileName, $sourceFolder, $id, $container_learning_tree_id) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		//
		$source_array = explode('/', $sourceFolder);

		$corrected_source_folder1 = str_replace("storage", "public", $sourceFolder);
		$corrected_source_folder = str_replace(end($source_array), '', $corrected_source_folder1);
		$destination_folder = '/public/lt/' . $container_learning_tree_id . '/topic/' . $id . '/';

		$class = $GLOBALS['storage_utility_class_name'];
		$storageUtility = new $class;
		$copy_status = $storageUtility->copyFile(end($source_array), $corrected_source_folder, $destination_folder);

		$corrected_destination_folder = str_replace("public", "storage", $destination_folder) . '' . end($source_array);

		$topic = Topic::find($id);
		$topic->photo = $fileName;
		$topic->photo_path = $corrected_destination_folder;
		$topic->save();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

}

