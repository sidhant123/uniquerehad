<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;


use App\Models\Site;
use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class SiteService {
	use ValidationTrait;

	public static function retrieveSites() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Site::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

public static function saveData($data) {

	Log::error($data);
	// exit();

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data) {
			$userId = $data['authenticatedUser']->id;
			$obj = new Site;
			$obj->name = $data['name'];
			$obj->code = strtoupper(trim($data['code']));
			$obj->location = ucfirst(trim($data['location']));
			$obj->address = ucfirst(trim($data['address']));
    		$obj->created_by = $userId;
			$obj->updated_by = $userId;

			$obj->save();
			$site_id = $obj->id;
			return $site_id;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	
public static function updateData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
					//$userId = $data['authenticatedUser']->id;
			Site::where('id', $data['id'])->update([
				'name' => ucwords(trim($data['name'])),
				'code' => strtoupper(trim($data['code'])),
				'location' => $data['location'],
				'address' => ucfirst(trim($data['location'])),
				'updated_by' => $data['authenticatedUser']->id
			]);

			return $data['id'];
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
public static function deleteSite($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$site = Site::where('id', $id);
			//$user = self::validateActiveSite($site);

			//if ($user) {
				$userId = auth()->user()->id;

				$site = Site::where('id', $id);
				$site->update(['deleted_by' => $userId]);
				$check = $site->delete();
			//} else {
			//	$check = false;
			//}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	private static function validateActiveSite($site) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$site = $site->first();
		Log::debug("********* VALIDATE ACTIVE SITE 1".$site);
		Log::debug("********* VALIDATE ACTIVE SITE 2".$site->id);


		$userObject = Project::where('site_id', $site->id);
		Log::debug("********* VALIDATE ACTIVE SITE 3".$userObject);

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		if ($userObject->first()->site_id == $site->id) {
			return false;
		} else {
			return $userObject;
		}
	}
	public static function retriveSiteLocation($site_id) {
		 $data = Site::select('location')->where('id', $site_id)->get();  

        return $data[0];
	}

}
