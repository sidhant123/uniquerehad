@extends('layouts.base')
@section('content')

<div id="main_div">
  <div><h3 class="headings">Reset User Password</h3></div>

  <div><p></p></div>

  <div class="row">
     <div class="col-lg-12">
       <div class="form-group required">
           <label for="compnay_id">Select Company</label>
           <select  class="form-control mandatory" id="compnay_id" name="company"></select>
         </div>
     </div>
  </div>

  <div class="row">
     <div class="col-lg-12">
       <div class="form-group required">
           <label for="user_id">Select User</label>
           <select  class="form-control mandatory" id="user_id" name="user"></select>
         </div>
     </div>
  </div>

  <div><p></p></div>


  <!-- button -->
 <div class="row col-lg-12" align="center">
  <button class="btn btn-primary" type="button" id="submit_button_id">Reset Password</button>
 </div>

</div>
<!-- end of main div -->

@endsection


@push('scripts')

<style>


.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script src="/js/user.js"></script>

<script>

  //
  $(document).ready(function() {

  $("[name=action]").val(["activate"]);


  $('#compnay_id').select2({
  });
  $('#compnay_id').empty();
  $('#compnay_id').append('<option value="0">--select--</option>');

  $('#user_id').select2({
  });
  $('#user_id').empty();
  $('#user_id').append('<option value="0">--select--</option>');

  @foreach($organization as $org)
  $('#compnay_id').append('<option value="{{$org->id}}">{{$org->name}}</option>');
  @endforeach
  });


  // for user of company
$(document).on('change','#compnay_id', function(){
   //
   var compnay_id=$('#compnay_id').val();
   if (compnay_id ==0) {
       return false;
   }

   url='/usersForCompany/'+compnay_id+"/deactivate";

    $(".loader").fadeIn();

    axios.get(url)
        .then(function (response) {

        var server_data=response['data'];
        console.log(server_data);

        reset('user_id');

        $.each(server_data,function(key,value){
          //
          $('#user_id').append('<option value='+value['id']+'>'+value['first_name']+' '+value['last_name']+'</option>');
         })
        $(".loader").fadeOut('slow');
    })
        .catch(function (error) {
        console.log("error"+error);
        $(".loader").fadeOut('slow');
        bootbox.alert('Something went wrong');

   });
});

$(document).on('click','#submit_button_id', function(){
   //
   var user_id=$('#user_id').val();
   if (user_id ==0) {
       bootbox.alert('Please select the mandatory fields');
       return false;
   }

   url='/resetUserPassword/'+user_id;

    $(".loader").fadeIn();

    axios.get(url)
        .then(function (response) {

        var server_data=response['data'];
        console.log(server_data);

        $(".loader").fadeOut('slow');
        // alert
        if (server_data=='success') {
          bootbox.alert('Password has been reset for the user');
        }else{
          bootbox.alert('Something went wrong');
        }
    })
        .catch(function (error) {
        console.log("error"+error);

        $(".loader").fadeOut('slow');

        var errorMsg='Something went wrong';
        bootbox.alert(errorMsg);
   });
});


function reset(id){
      $('#'+id).empty();
      $('#'+id).append('<option value="0">--select--</option>');
}


</script>
@endpush


