<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 21 Feb 2018 23:24:04 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class FastCompletion
 * 
 * @property int $id
 * @property int $activity_completion_max_count
 * @property int $count
 * @property int $done
 * @property int $program_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Program $program
 *
 * @package App\Models
 */
class FastCompletion extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'activity_completion_max_count' => 'int',
		'count' => 'int',
		'done' => 'int',
		'program_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'activity_completion_max_count',
		'count',
		'done',
		'program_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function program()
	{
		return $this->belongsTo(\App\Models\Program::class);
	}
}
