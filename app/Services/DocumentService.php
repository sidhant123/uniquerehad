<?php

namespace App\Services;

//Custom imports

use App\Traits\ValidationTrait;
use App\User;
use DB;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Communication\Events\CommEvent;
use vnnogile\Utilities\Services\ConstantsUtility;


use App\Models\Document;
use App\Models\Project;

/**
 * Class AddressMasterService
 *
 */
class DocumentService {
	use ValidationTrait;

	public static function retrieveDocuments() {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$data = Document::all();
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $data;
	}

public static function saveDocument($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use (&$data) {
		
			$userId = $data['authenticatedUser']->id;
			$obj = new Document;
			$obj->name = $data['doc_name'];
			$obj->path = $data['doc_table_path'];
			$obj->document_type = $data['document_type'];
			$obj->entity_id = $data['entity_id'];
			$obj->entity_name = $data['entity_name'];
			$obj->created_date = now();
    		$obj->created_by = $userId;
			$obj->updated_by = $userId;

			$obj->save();
			$Document_id = $obj->id;
			return $Document_id;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	
public static function updateData($data) {

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($data) {
					//$userId = $data['authenticatedUser']->id;
			Document::where('id', $data['id'])->update([
				'name' => $data['name'],
				'description' => $data['description'],
				'address' => $data['address'],
				'updated_by' => $data['authenticatedUser']->id
			]);

			return $data['id'];
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
public static function deleteDocument($id) {
		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::STARTED);
		$check = DB::transaction(function () use ($id) {
			$Document = Document::where('id', $id);
			//$user = self::validateActiveDocument($Document);

			//if ($user) {
				$userId = auth()->user()->id;

				$Document = Document::where('id', $id);
				$Document->update(['deleted_by' => $userId]);
				$check = $Document->delete();
			//} else {
			//	$check = false;
			//}

			Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
			return $check;
		});

		Log::debug(__CLASS__ . ConstantsUtility::SEPARATOR . __METHOD__ . ConstantsUtility::FINISHED);
		return $check;
	}
	
	public static function getDocumentDetails($doc_id,$type)
	{
			$Document = Document::where('id', $doc_id)->where('document_type',$type)->get();
			if(count($Document)>0){
					$Document = $Document[0];
			}
			return $Document;

	}

}
