<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

//Custome imports
use Illuminate\Encryption\Encrypter;
use Symfony\Component\Console\Input\InputArgument;

class EncryptEnvCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'encrypt-env {value*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encrypt environment variable value so it can be safely stored';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crypt = new Encrypter(config("cool.stuff"));

        foreach ($this->argument('value') as $value)
        {
            $this->output->writeln("ENC:" . $crypt->encrypt($value));
        }
    }

    protected function getArguments()
    {
        return [
            ['value', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Value of the environment variable'],
        ];
    }
}

