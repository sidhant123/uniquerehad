@extends('layouts.base')
@section('content')

<div id="main_form">

  @include('errors.error_validation')

  <form style="border-style:dotted; border-width: 1px;" action="/users/@yield('editID')" method="post" onsubmit="return validateMandatoryFields()" enctype="multipart/form-data">
    {{ csrf_field() }}

    @section('editMethod')
    @show
    <h3 class="headings">{{ucfirst(substr(Route::currentRouteName(),6))}} User</h3>

    <div class="row">
      <div class="col-lg-12">
        <div class="form-group required">
            <label for="company_id@yield('organization_name_d')">{{ __('labels.users.company_name') }}</label>
            <select  class="form-control mandatory" name="organization_name" id="company_id"@yield('organization_name_d')></select>
        </div>
      </div>
    </div>  

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group required">
           <label for="firstName">{{__('labels.users.first_name')}}</label>
           <input type="text" class="form-control mandatory"  name="firstName" value="@yield('firstName')" @yield('firstName_d')>
           <input type="hidden" name="old_photo" value="@yield('old_photo')">
           <input type="hidden" name="old_photo_name" value="@yield('old_photo_name')">
         </div>
     </div>

      <div class="col-lg-6">
       <div class="form-group required">
           <label for="lastName">{{__('labels.users.last_name')}}</label>
           <input type="text" class="form-control mandatory" name="lastName" value="@yield('lastName')"  @yield('lastName_d')>
         </div>
     </div>
  </div>  

    <div class="row">
      <div class="col-lg-6">
       <div class="form-group">
           <label for="employeeId">{{__('labels.users.employee_id')}}</label>
           <input type="text" class="form-control" name="employeeId" id="employeeId" value="@yield('employeeId')"  @yield('employeeId_d')>
         </div>
     </div>

      <div class="col-lg-6 req">
       <div class="form-group">
           <label for="department">{{__('labels.users.department')}}</label>
           <input type="text" class="form-control" id="department" name="department" value="@yield('department')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6 req">
       <div class="form-group">
           <label for="designation">{{__('labels.users.designation')}}</label>
           <input type="text" class="form-control" id="designation" name="designation" value="@yield('designation')">
         </div>
     </div>

      <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="email">{{__('labels.users.email')}}</label>
           <input type="text" class="form-control mandatory" id="email" name="email" value="@yield('email')">
         </div>
     </div>
  </div>


    <div class="row">
      <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="primaryContact">{{__('labels.users.primary_contact_no')}}</label>
           <input type="text" class="form-control mandatory" id="primaryContact" name="primaryContact" value="@yield('primaryContact')">
         </div>
     </div>

      <div class="col-lg-6 req">
       <div class="form-group">
           <label for="secondaryContact">{{__('labels.users.secondary_contact_no')}}</label>
           <input type="text" class="form-control" id="secondaryContact" name="secondaryContact" value="@yield('secondaryContact')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6 req">
       <div class="form-group">
           <label for="linkedinProfile">{{__('labels.users.linkedin_profile')}}</label>
           <input type="text" class="form-control" id="linkedinProfile" name="linkedinProfile" value="@yield('linkedinProfile')" @yield('linkedinProfile_d')>
         </div>
     </div>

      <div class="col-lg-6 req">
       <div class="form-group">
           <label for="facebookId">{{__('labels.users.fb_profile')}}</label>
           <input type="text" class="form-control" id="facebookId" name="facebookId" value="@yield('fbProfile')">
         </div>
     </div>
  </div>

    <div class="row">
      <div class="col-lg-6 req">
       <div class="form-group" >
           <label for="twitterHandle">{{__('labels.users.twitter_handle')}}</label>
           <input type="text" class="form-control" id="twitterHandle" name="twitterHandle" value="@yield('twitterHandle')">
         </div>
     </div>

      <div class="col-lg-6 req">
       <div class="form-group required">
           <label for="file">{{__('labels.users.photo')}}</label>
           <input  type="file" class="form-control mandatory" id="file" onchange="return fileValidation(this.id);" name="photo" accept=".png,.jpeg,.jpg">
         </div>
     </div>
  </div>


<p></p>
<div class="row">
  <div class="offset-lg-5" align="center">
    <button type="submit" class="btn btn-small btn-primary" @yield('sub_btn_d') >{{__('labels.users.submit')}}</button>    
</div>
</div>
<p></p>
</form>

<p></p>

<!-- for import user -->
@section('importUser')
@show


</div>


@endsection


@push('scripts')

<style>


.col-sm-3.req:after{
 content:"*";
}

::-webkit-input-placeholder {
 text-align: center;
}


</style>

<script src="/js/user.js"></script>
<script>


  @if(isset($organization))
  $('#company_id').empty();
  $('#company_id').append('<option value="0">--select--</option>');
  @foreach($organization as $company)
  @isset($org_id)
  @if($company->id === $org_id)
  $('#company_id').append('<option value="{{$company->id}}" selected>{{$company->name}}</option>');
  @else
  $('#company_id').append('<option value="{{$company->id}}">{{$company->name}}</option>');
  @endif
  @endisset
  $('#company_id').append('<option value="{{$company->id}}">{{$company->name}}</option>');
  @endforeach
  @endif
</script>
@endpush


