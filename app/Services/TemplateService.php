<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use App\Models\Template;
use App\Models\CommunicationEvent;
use App\Models\TemplateLog;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use vnnogile\Templates\services\TemplateUtility;
/**
 * Class AddressMasterService
 *
 */
class TemplateService
{

	public static function templates()
	{
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return Template::all();
	}

	public static function getTemplateById($id)
	{
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		$template = Template::find($id);
		$communication_event = $template->communication_events()->first();
		if(!empty($communication_event)){
			$template['communication_event_id'] = $communication_event->id;
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $template;
	}
	public static function saveRecord($request){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

		try{
			if($request['template_event_type'] == 'copyTemplate'){
				$template = self::saveTemplate($request);
				if($request['communication_event_id'] > 0){
					$template->communication_events()->attach($request['communication_event_id'], ['created_by' => 1, 'updated_by' => 1]);
				}
				return $template->id;
			}else if($request['template_event_type'] == 'attachTemplate'){
				$template = self::getTemplateById($request['templates']);
				if(!self::checkAttachCommEvent($request['templates'],$request['communication_event_id'])){
				$template->communication_events()->attach($request['communication_event_id'], ['created_by' => 1, 'updated_by' => 1]);
				}
				return $template->id;
			}else{
				$template = self::saveTemplate($request);
				if(isset($request['communication_event_id'])){
					$template->communication_events()->attach($request['communication_event_id'], ['created_by' => 1, 'updated_by' => 1]);
				}
				return $template->id;
			}

		}catch(Exception $e){
			Log::debug("TemplateService::saveRecord ended".print_r($e,true));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $e;
		}
	}
	public static function updateTemplate($data,$id){
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		try{
			if($data['template_event_type'] == 'detachTemplate'){
				$template = self::getTemplateById($id);
				$template->communication_events()->detach($data['communication_event_id']);
				$template_id = self::editTemplate($data,$id);
				return $template_id;
			}else{
				$template_id = self::editTemplate($data,$id);
				return $template_id;
			}
		}catch(Exception $e){
			Log::debug("TemplateService::saveRecord ended".print_r($e,true));
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $e;
		}
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return true;
	}
	public static function saveTemplate($request){
		$note_data = json_decode($request['data'], true);
		$data['tenantPartyId'] = TenantService::getTenantPartyIdForAuthUser($request['utilsInterface'], $request['authenticatedUser']);
		//if(empty($note_data['img_src'])){
			$template_body = $note_data['html_data'];
		//	}else{
		//	$template_body = TemplateUtility::convertBase64ToImgfileAndReplaceSrc($request['data']);
		//	}
			if (isset($request['summernote'])) {
            $template_body = $request['summernote'];
        }
			$template = new Template;
			$template->template_code = $request['template_code'];
			$template->template_name = $request['template_name'];
			$template->template = $template_body;
			$template->template_url = $request['template_url'];
			$template->subject = $request['subject'];
			if(isset($request['to'])){
			$template->to = implode(',', $request['to']);
			}
			if(isset($request['cc'])){
			$template->cc = implode(',', $request['cc']);
			}
			$template->version = 1;
			$template->tenant_party_id = $data['tenantPartyId'];
			$template->is_active = 1;
			$template->created_by = $request['authenticatedUser']->id;
			$template->updated_by = $request['authenticatedUser']->id;
			$template->communication_channel_id = $request['communication_channel_type'];
			$template->save();
			Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
			return $template;
	}
	public static function editTemplate($data,$id){
		$note_data = json_decode($data['data'], true);
		//	if(empty($note_data['img_src'])){
			$template = $note_data['html_data'];
		//	}else{
		//	$template = TemplateUtility::convertBase64ToImgfileAndReplaceSrc($data['data']);
		//	}
		if (isset($data['summernote'])) {
            $template = $data['summernote'];
        }
		$version = $data['version'];
		self::saveTemplateLog($id,$note_data['html_data'],$data['authenticatedUser']->id);
		$update_array = ['template_name' =>$data['template_name'] ,'subject'=>$data['subject'],'template_code'=>$data['template_code'],'template'=> $template,'template_url'=> $data['template_url'], 'version'=>++$version,'updated_by'=>$data['authenticatedUser']->id,'communication_channel_id'=> $data['communication_channel_type']];
		if(isset($data['to'])){
			$to = ['to'=>implode(',', $data['to'])];
			$update_array = array_merge($update_array,$to);
		}
		if(isset($data['cc'])){
			$cc = ['cc'=>implode(',', $data['cc'])];
			$update_array = array_merge($update_array,$cc);
		}
		Template::where('id', $id)->update($update_array);
		return $id;
	}
	public static function getAttachedCommunicationEvents($template_id){
				$template = self::getTemplateById($template_id);
				$comm = $template->communication_events()->get();
				$events = array();
				foreach ($comm as $key => $value) {
					// print_r($value->communication_event_id);
					array_push($events, $value->name);
				}
			return implode(',',$events);
	}
	public static function checkAttachCommEvent($template_id,$communication_event_id){
		$template = self::getTemplateById($template_id);
				$comm = $template->communication_events()->where('communication_event_id',$communication_event_id)->count();
				if($comm > 0 ){
					return true;
				}else{
					return false;
				}
	}
	public static function getTemplateCommEvents($template_id){
		$template = self::getTemplateById($template_id);
				$comm = $template->communication_events()->get();
				return $comm;
	}
	public static function matchTemplateWithCurrentTemplate($template_id,$template){
		$template = self::getTemplateById($template_id);
		return  strcmp($template->template,$template);
	}
	public static function saveTemplateLog($id,$template,$user_id){

		$templateLog = new TemplateLog;
		$templateLog->template_id = $id;
		$templateLog->log = $template;
		$templateLog->created_by = $user_id;
		$templateLog->updated_by = $user_id;
		$templateLog->save();
	}
	public static function getTemplatePlaceHoldersByCommEvent($comm_event_id){
		$communication_event = CommunicationEvent::find($comm_event_id);
		$placeholders = $communication_event->templates_place_holders;
		return $placeholders;
	}
}
