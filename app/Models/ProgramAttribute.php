<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 07 Aug 2018 12:17:57 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramAttribute
 * 
 * @property int $id
 * @property int $program_id
 * @property string $remind_before_topic_start_time
 * @property string $remind_before_topic_end_time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ProgramAttribute extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'remind_before_topic_start_time',
		'remind_before_topic_end_time',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
