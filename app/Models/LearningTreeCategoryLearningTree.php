<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:35:59 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class LearningTreeCategoryLearningTree
 * 
 * @property int $id
 * @property int $learning_tree_categories_id
 * @property int $learning_trees_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\LearningTreeCategory $learning_tree_category
 * @property \App\Models\LearningTree $learning_tree
 *
 * @package App\Models
 */
class LearningTreeCategoryLearningTree extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'learning_tree_category_learning_tree';

	protected $casts = [
		'learning_tree_categories_id' => 'int',
		'learning_trees_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'learning_tree_categories_id',
		'learning_trees_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function learning_tree_category()
	{
		return $this->belongsTo(\App\Models\LearningTreeCategory::class, 'learning_tree_categories_id');
	}

	public function learning_tree()
	{
		return $this->belongsTo(\App\Models\LearningTree::class, 'learning_trees_id');
	}
}
