<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\masterDataInterface;

/**
 * Class PartyTypeService
 * 
 */
class PartyTypeService
{
    private static $entityName = "PartyType";

    public static function getPartyTypeId(masterDataInterface $masterDataInterface, $partyTypeName)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");        
        $partyTypeId=0;

        //We can directly query -- however party type records are already cached
        //There are not many records -- hence we can loop through records in  te cache
        $partyTypes = $masterDataInterface->all(self::$entityName);
        foreach ($partyTypes as $partyType) {
            if ($partyType->name == $partyTypeName){
                $partyTypeId = $partyType->id;
            }
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return $partyTypeId;
    }


}
