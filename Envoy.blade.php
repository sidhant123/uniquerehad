@setup
    $environment = isset($env) ? $env : 'test';
    $basepath;
    $ssh_server;

    switch ($environment) {
        case 'test':
            $ssh_server = 'cgxstaging';
            $basepath = '/var/www/html';
	    $projectbasepath = '/var/www/html/cgxbo';
	    $supervisorDestination = '/etc/supervisor/conf.d';
            break;
        case 'live':
            $ssh_server = 'cgx';
            $basepath = '/var/www/html';
	    $projectbasepath = '/var/www/html/cgxbo';
	    $supervisorDestination = '/etc/supervisor/conf.d';
            break;
        default:
            $ssh_server = 'cgx';
            $basepath = '/var/www/html';
	    $projectbasepath = '/var/www/html/cgxbo';
	    $supervisorDestination = '/etc/supervisor/conf.d';
    }
@endsetup

@servers(['cgx_staging' => $ssh_server])

@task('stage:init', ['on' => ['cgx_staging']])
    cd {{ $basepath }}
    sudo git clone https://vnnodeveloper@bitbucket.org/vnnodeveloper/cgxbo.git
    sudo chown -R cognegix cgxbo
    cd $HOME
    cp .env {{ $projectbasepath }}
    cd {{ $projectbasepath }}
    composer install
    composer dump-autoload -o
@endtask

@task('stage:clean', ['on' => 'cgx_staging'])
    cd {{ $projectbasepath }}
    sudo mv vendor $HOME
@endtask

@task('stage:deploy', ['on' => 'cgx_staging'])
    cd {{ $projectbasepath }}
    git pull origin master
    composer install
    composer dump-autoload -o
@endtask

@task('stage:up', ['on' => ['cgx_staging']])
    cd {{ $projectbasepath }}
    php artisan up
@endtask

@task('stage:down', ['on' => ['cgx_staging']])
    cd {{ $projectbasepath }}
    php artisan down
@endtask

@task('stage:supervisor:init', ['on' => ['cgx_staging']])
    cd $HOME
    git clone https://vnnodeveloper@bitbucket.org/vnnodeveloper/cgxbo_supervisor.git
    cd cgxbo_supervisor

    sudo cp *.conf {{ $supervisorDestination }}
    sudo supervisorctl reread

    sudo supervisorctl update
    sudo supervisorctl start laravel-emails:*
    sudo supervisorctl start laravel-notifications:*
    sudo supervisorctl start laravel-sms:*
    sudo supervisorctl start laravel-content:*
    sudo supervisorctl start laravel-worker:*
@endtask

@task('stage:supervisor:deploy', ['on' => ['cgx_staging']])
    cd $HOME
    cd cgxbo_supervisor
    git pull origin master

    sudo cp *.conf {{ $supervisorDestination }}

    sudo supervisorctl stop laravel-emails:*
    sudo supervisorctl stop laravel-notifications:*
    sudo supervisorctl stop laravel-sms:*
    sudo supervisorctl stop laravel-content:*
    sudo supervisorctl stop laravel-worker:*

    sudo supervisorctl reread
    sudo supervisorctl update

    sudo supervisorctl start laravel-emails:*
    sudo supervisorctl start laravel-notifications:*
    sudo supervisorctl start laravel-sms:*
    sudo supervisorctl start laravel-content:*
    sudo supervisorctl start laravel-worker:*
@endtask

@macro('staging:deploy', ['on' => 'cgx_staging'])
    stage:down
    stage:deploy
    stage:up
@endmacro
