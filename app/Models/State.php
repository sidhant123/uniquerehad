<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 08:36:00 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class State
 * 
 * @property int $id
 * @property string $name
 * @property string $state_or_union_terrirtory
 * @property int $tin_number
 * @property string $abbreviation
 * @property int $country_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Country $country
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 * @property \Illuminate\Database\Eloquent\Collection $cities
 * @property \Illuminate\Database\Eloquent\Collection $districts
 * @property \Illuminate\Database\Eloquent\Collection $pincodes
 *
 * @package App\Models
 */
class State extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'tin_number' => 'int',
		'country_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'state_or_union_terrirtory',
		'tin_number',
		'abbreviation',
		'country_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'deleted_by');
	}

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function addresses()
	{
		return $this->hasMany(\App\Models\Address::class);
	}

	public function cities()
	{
		return $this->hasMany(\App\Models\City::class);
	}

	public function districts()
	{
		return $this->hasMany(\App\Models\District::class);
	}

	public function pincodes()
	{
		return $this->hasManyThrough(\App\Models\Pincode::class, \App\Models\city::class);
	}
}
