<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;
use App\Services\ValidationRulesService;

use App\Models\Material;
use App\Services\MaterialService;
use App\Services\OrganizationService;

class MaterialController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(\Auth::user()->hasAccess(['Material']));
        $this->authorize('List Materials', 'List Materials');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return PaginateController::paginate('Material', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
     //     $this->authorize('Create materials', 'Create materials');
         $supplier = OrganizationService::retrieveOrganization();

                         
         return view('material.create_material', compact('supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Create materials', 'Create materials');
        try {

           $request['name'] = ucwords(strtolower(trim($request['name'])));
           $request['code'] = strtoupper(trim($request['code']));

             $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            //$rules = $valdationRulesService->validationRulesWithTenant('Material');
            $rules = [
                  'name' => 'required|min:3|max:100|unique:materials,name,NULL,id,deleted_at,NULL,code,'.$request['code'],
                  'code' => 'required|min:2|max:4',
                  'unit' => 'required|min:2|max:25',
            ];
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
               // dd($validator);
                if ($validator->fails()) {
                    return redirect('materials/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }         

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['Material'] = $request;
            $data['request'] = $request;
           MaterialService::saveMaterial($data);
           

            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('MaterialController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = Material::where('id', $id)->first();
         $supplier = OrganizationService::retrieveOrganization();
                     $sel_supplier_id = $row_data['supplier_id'];


            return view('material.view_material', compact('row_data', 'supplier', 'sel_supplier_id'));


        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Edit Services', 'Edit Services');
          
            $row_data = Material::where('id', $id)->first();
            $supplier = OrganizationService::retrieveOrganization();
            $sel_supplier_id = $row_data['supplier_id'];

            return view('material.edit_material', compact('row_data', 'supplier', 'sel_supplier_id'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
       // $this->authorize('Edit materials', 'Edit materials');
        try {
            
           $request['name'] = ucwords(strtolower(trim($request['name'])));
           $request['code'] = strtoupper(trim($request['code']));

        
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
           // $rules = $valdationRulesService->validationRulesWithTenant('Material','$name', $id);
             $rules = [
                  'name' => 'required|min:4|max:100|unique:materials,name,'. $id.',id,deleted_at,NULL,code,'.$request['code'],
                  'code' => 'required|min:2|max:4',
                  'unit' => 'required|min:2|max:25,',
            ];
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                   return redirect('materials/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
           // dd($rules);
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
            $data['request'] = $request;

            Log::error($data);

               $res = MaterialService::updateMaterial($data);

            Log::error($res);
          
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('MaterialController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
           // $this->authorize('Delete Materials', 'Delete Materials');

            $deleted = MaterialService::deleteMaterial($id);
            if ($deleted) {
                Session::flash('status', 'Material deleted successfully');
            } else {
                Session::flash('status', 'Material cant be deleted');
            }
            // $path_array = explode('/', action('MaterialController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'materials';

        } catch (Exception $e) {
            Log::error('Exception while deleting Material ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }

      public function verifyImporMaterialData(Request $request, MessageBag $MessageBag) {

            $msg = null;

            $import_supplier_id = $request['import_supplier_id'];
            $imported_material_file = $request['imported_material_file'];
            if( $import_supplier_id == "" or $import_supplier_id == "0"){
                //   Session::flash('status', __('labels.flash_messages.material_import_supplier_manadatory'));
               // return redirect()->action('MaterialController@create');
                $validator = "Please select supplier to import";
                  return redirect('materials/create')
                        ->withErrors($validator)
                        ->withInput();
            }
            if($imported_material_file == null){
                  $validator = "Please upload excel file to import";
                  return redirect('materials/create')
                        ->withErrors($validator)
                        ->withInput();
            }
            $data = $request->all();
              $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            //$rules = $valdationRulesService->validationRulesWithTenant('Material');
            $rules = [
                  'imported_material_file' => 'required|file|mimes:xlsx|max:2000',
      
            ];
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
               // dd($validator);
                if ($validator->fails()) {
                    return redirect('materials/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }    

            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['project'] = $request;
            $data['request'] = $request;
            $check = MaterialService::verifyMaterialImportData($data,$import_supplier_id);
            if ($check != 'SUCCESS') {
                if ($check == 'MODEL_NOT_FOUND') {
                    Session::flash('status', __('labels.flash_messages.invalid_customer_code'));
                    return redirect()->action('MaterialController@index');
                } elseif($check == 'Error_occured') {
                 Session::flash('status', __('Error in file format or import process.'));
                return redirect()->action('MaterialController@index');    
                }
                foreach ($check as $key => $value) {

                 //foreach ($value as $key1 => $value1) {
                     $keys =  array_keys($value);  
                      //  array_push($keys, $key1);
                 //   }
                    break;
                }

              
                //Parameters $fileName, $title, $sheetName, $description, $keys, $values){
                $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
               parent::downloadSingleSheetExcelReport("Material_Errors_" . $timeNow, "Invalid Records", "Invalid Records", "Invalid Records", $keys, $check);
                Session::flash('status', __('labels.flash_messages.import_error'));
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
                return redirect()->action('MaterialController@index');
            }else {
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('MaterialController@index');             }
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }


}
