<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;
use vnnogile\Pagination\Utilities\PaginateController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Session;
use App\User;
use App\Services\ValidationRulesService;

use App\Models\Receipt;
use App\Models\Project;
use App\Models\Boq;
use App\Services\ReceiptService;
use App\Services\ProjectService;

class ReceiptController extends BaseController
{
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct() {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        parent::__construct();
        $this->middleware('CheckCompositeMiddleWare', ['except' => ['update', 'create', 'show']]);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
    
    public function index(Request $request, $flag = false)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //Log::debug(\Auth::user()->hasAccess(['Receipt']));
        $this->authorize('List Receipts', 'List Receipts');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return PaginateController::paginate('Receipt', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
         $project = ProjectService::retrieveProject();
         $boq = self::retriveBoqList();
         return view('receipt.create_receipt', compact('project','boq'));
    }
 public function store(Request $request)
    {
           log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        //$this->authorize('Create receipts', 'Create receipts');
        try {
           
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            //$rules = $valdationRulesService->validationRulesWithTenant('Receipt');
            $rules = [
               // 'boq_no'=>'unique:receipts,boq_no,NULL,id,deleted_at,NULL,invoice_no,'.$request['invoice_no'],
  'invoice_no' => 'required|max:100|unique:receipts,invoice_no,NULL,id,deleted_at,NULL,project_id,'.$request['project_id'],
                'payment_method' => 'max:50',
                'gst_amount' => 'required|max:10',
                'invoice_amount' => 'required|max:10',
                'payment_ref_no' => 'max:10',
                'amount_received' => 'max:10',
                'tds_deduction' => 'max:10',
                'other_deduction' => 'max:10',
                'remark' => 'max:250',
            ];
             $custmessage = [
                       'unique' => 'The R A Bill No. has already been taken.'
                    ];
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules,$custmessage);
                if ($validator->fails()) {
                    return redirect('receipts/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }  

            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['Receipt'] = $request;
            $data['request'] = $request;
            $saveResult = ReceiptService::saveReceipt($data);
        
         if($saveResult){

            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ReceiptController@index');
          }else{
             Session::flash('status', __('labels.flash_messages.store_error'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ReceiptController@index');
          }
           

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
      *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Show Receipts', 'Show Receipts');
          
            $row_data = Receipt::where('id', $id)->first();
           //dd($row_data);
            $project = ProjectService::retrieveProject();
            $sel_project_id = $row_data['project_id'];
            $sel_boq_no = $row_data['boq_no'];

            $boq = self::retriveBoqList($sel_project_id);


            return view('receipt.view_receipt', compact('row_data', 'project', 'sel_project_id','sel_boq_no','boq'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Edit Receipts', 'Edit Receipts');
          
            $row_data = Receipt::where('id', $id)->first();
           //dd($row_data);
            $project = ProjectService::retrieveProject();
            $sel_project_id = $row_data['project_id'];
            $sel_boq_no = $row_data['boq_no'];
            $boq = self::retriveBoqList($sel_project_id);

            return view('receipt.edit_receipt', compact('row_data', 'project', 'sel_project_id','sel_boq_no','boq'));

        } catch (Exception $e) {
            Log::debug('error in store', $e->getMessage());
        }

    }


  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
            //$this->authorize('Edit Receipts', 'Edit Receipts');
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            //$rules = $valdationRulesService->validationRulesWithTenant('Receipt', '$receipt', $id);
              $rules = [
                // 'boq_no'=>'unique:receipts,boq_no,'.$id.',id,deleted_at,NULL,invoice_no,'.$request['invoice_no'],
                'invoice_no' => 'unique:receipts,invoice_no,'.$id.',id,deleted_at,NULL,project_id,'.$request['project_id'],
                'payment_method' => 'max:50',
                'gst_amount' => 'required|max:10',
                'invoice_amount' => 'required|max:10',
                'payment_ref_no' => 'max:10',
                'amount_received' => 'max:10',
                'tds_deduction' => 'max:10',
                'other_deduction' => 'max:10',
                'remark' => 'max:250',
            ];
              $custmessage = [
                       'unique' => 'The R A Bill No. has already been taken.'
                    ];
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules,$custmessage);
                if ($validator->fails()) {
                   return redirect('receipts/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            //check date
            //Get some basic data ahead  of time and set it in data object
            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
            $data['id'] = $id;
            $data['request'] = $request;

               ReceiptService::updateReceipt($data);
          
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ReceiptController@index');

        } catch (Exception $e) {
            Log::error('Exception while storing user :: ', $e->getMessage());
        }
    }

  




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $this->authorize('Delete Receipts', 'Delete Receipts');

            $deleted = ReceiptService::deleteReceipt($id);
            if ($deleted) {
                Session::flash('status', 'Receipt deleted successfully');
            } else {
                Session::flash('status', 'Receipt cant be deleted');
            }
            // $path_array = explode('/', action('ReceiptController@index'));
            // $route = array_pop($path_array);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return "deleted successfully" . "/" . 'receipts';

        } catch (Exception $e) {
            Log::error('Exception while deleting Receipt ', $e->getMessage());
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        }
    }
//-------------------- import code

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function importReceipt(Request $request)
    {
            $msg = null;
            $project_id = $request['project_id'];
            $imported_receipt_file = $request['imported_receipt_file'];
            if( $project_id == "" or $project_id == "0"){
                //   Session::flash('status', __('labels.flash_messages.receipt_import_project_manadatory'));
               // return redirect()->action('ReceiptController@create');
                $validator = "Please select project to import";
                  return redirect('receipts/create')
                        ->withErrors($validator)
                        ->withInput();
            }
            if($imported_receipt_file == null){
                  $validator = "Please upload excel file to import";
                  return redirect('receipts/create')
                        ->withErrors($validator)
                        ->withInput();
            }


             $data = $request->all();
              $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);

            $rules = [
                  'imported_receipt_file' => 'required|file|mimes:xlsx|max:2000',
      
            ];

             if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($request->all(), $rules);
               // dd($validator);
                if ($validator->fails()) {
                    return redirect('receipts/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }    

            $data = $request->all();
            $data['utilsInterface'] = parent::$utilsInterface;
            $data['masterDataInterface'] = parent::$masterDataInterface;
            $data['authenticatedUser'] = \Auth::user();
                //$data['project'] = $request;
            $data['request'] = $request;
            $check = ReceiptService::verifyReceiptImportData($data,$project_id);
          //  dd($check);
            if ($check != 'SUCCESS') {
                if ($check == 'MODEL_NOT_FOUND') {
                    Session::flash('status', __('labels.flash_messages.invalid_customer_code'));
                    return redirect()->action('ReceiptController@index');
                }elseif($check=='some_error'){
                Session::flash('status', __('Error in file format or import process.'));
                return redirect()->action('ReceiptController@index');    
                } 
                foreach ($check as $key => $value) {

                 //foreach ($value as $key1 => $value1) {
                     $keys =  array_keys($value);  
                      //  array_push($keys, $key1);
                 //   }
                    break;
                }

              
                //Parameters $fileName, $title, $sheetName, $description, $keys, $values){
                $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
               parent::downloadSingleSheetExcelReport("Receipt_Errors_file_" . $timeNow, "Invalid Records", "Invalid Records", "Invalid Records", $keys, $check);
                Session::flash('status', __('labels.flash_messages.import_error'));
                Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
                return redirect()->action('ReceiptController@index');
            }else {
            Session::flash('status', __('labels.flash_messages.store'));
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return redirect()->action('ReceiptController@index');            
         }
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

//------------------- export code
      public function exportReceipt($project_id, Request $request)
    {

         Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
         $receipt_data = ReceiptService::getReceiptDataForExport($project_id);
         if(count($receipt_data)>1) { 

            $project_data = Project::where('id', $project_id)->first();
            $project_name = $project_data['name'];

         $result = self::WriteDataDownloadExcel($receipt_data,$project_name);
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");

    }


public static function WriteDataDownloadExcel($data,$project_name)
{
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug('*************** WRITE DATA IN EXCEL ****************');
            $keys = array();

        // extract key

            $row_data=$data->toArray();

           foreach ($row_data as $key => $value) {
            foreach ($value as $key1 => $value1) {
           
                array_push($keys, $key1);
            }
            break;
        }

        $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
        $filename = $project_name. "-Receipt Data-" . $timeNow;
        $basec = new BaseController();
       
                 $basec->downloadSingleSheetExcelReport($filename, "Records 1", "Records2", "Records3", $keys, $row_data); 

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
}

public static function retriveBoqList($id=null) {
    
        if($id==null){
            $data = Boq::select('id','name')->get();  
        }else {
            $data = Boq::select('id','name')->where('project_id', $id)->get();  
        }

        return $data;
    }

     

}
