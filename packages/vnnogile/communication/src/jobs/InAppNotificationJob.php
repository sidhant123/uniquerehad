<?php

namespace vnnogile\Communication\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Custom Imports Below
use App\Models\Notification;
use App\Models\Organization;
use Illuminate\Support\Facades\Log;

class InAppNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $inAppNotificationData;
    public $tries = 1;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inAppNotificationData)
    {
        $this->inAppNotificationData = $inAppNotificationData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        
        $template = $this->inAppNotificationData['template_name'];
        $loginId = $this->inAppNotificationData['login_id'];
        $data = $this->inAppNotificationData['data'];

        $org=Organization::where('organization_code',$this->inAppNotificationData['tenant_code'])->first();
        $party_id = $this->inAppNotificationData['party_id'];
        foreach($data as $key => $value){
            $template = str_replace('{'.strtoupper($key).'}', $value, $template);
        }

        $notification = new Notification();
        $notification->party_id = $party_id;    
        $notification->tenant_party_id = $org->party_id;
        $notification->notification = $template;
        $notification->is_read = 0;
        $notification->created_by = $loginId;
        $notification->updated_by = $loginId;
        $notification->save();
        
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
