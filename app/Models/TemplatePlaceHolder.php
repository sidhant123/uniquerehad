<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 27 Jan 2018 12:08:35 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;

/**
 * Class TemplatePlaceHolder
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class TemplatePlaceHolder extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function communication_events()
	{
		return $this->belongsToMany(\App\Models\CommunicationEvent::class)
					->withPivot('id');
		// return $this->belongsToMany(\App\Models\CommunicationEvent::class)
		// 			->withPivot('id', 'created_by', 'updated_by', 'deleted_by', 'deleted_at')
		// 			->withTimestamps();			
					
	}
}
