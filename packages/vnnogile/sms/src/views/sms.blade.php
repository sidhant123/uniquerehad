@extends('layouts.base')
@section('content')

    <div class="alert alert-success hide" align="center">
  <strong>Success!</strong> <span class="successMessage"></span>
</div>
<div class="alert alert-danger hide">
  <strong>Error!</strong> <span class="errorMessage"></span>
</div>
<p></p>
<form id="form" style="border-style:dotted; border-width: 1px;" enctype="application/x-www-form-urlencoded">
  {{ csrf_field() }}
  <!-- <div class="col-xs-12" style="border: 1px solid blue;"> -->

    {{-- <div class="row">
      <div class="col-lg-6 col-lg-offset-3" align="center">



      </div>
    </div> --}}
     @include('navbar.nav_bar',['save_btn' => 'saveBtn','url'=> '/sendSMS'])
     <p></p>
     <div class="row">
      <div class="col-lg-11">
        <h3 class="headings">Send SMS</h3></div>
    </div>
    <p></p>
    {{-- <input type="hidden" id="participants_id" name="participants"> --}}
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3">
       <div class="form-group">
         <label for="customer_id">{{ __('labels.cluster.customer') }}</label>
         <select type="text" id="customer_id" class="form-control" name="customer_party_id" @yield('customer_name_readonly')>
           <option value="0">---Select---</option>
           @foreach($customers as $customer)
           @if(isset($cluster))
           @if($cluster['customer_party_id'] == $customer->partyId)
           <option value="{{$customer->partyId}}" selected>{{$customer->customerCode}}</option>
           @else
           <option value="{{$customer->partyId}}">{{$customer->customerCode}}</option>
           @endif
           @else
           <option value="{{$customer->partyId}}">{{$customer->customerCode}}</option>
           @endif
           @endforeach
         </select>
       </div>
     </div>
   </div>
   <p></p>
   <div class="row">
    <div class="col-lg-6 col-lg-offset-3">
      <div class="form-group">
        <label>Program</label>
        <select class="form-control mandatory" name="program_id" id="program_id">
          <option value="0">---Select---</option>
        </select>
      </div>
    </div>
  </div>
  <p></p>
  <div class="row">
    <div class="col-lg-6 col-lg-offset-3">
      <div class="form-group">
        <label>Participants</label>
       <div class="" style="overflow:auto; height:40%; background-color: whitesmoke"">

        <table class="table table-bordered" id="program_participant_table">
            <thead>
            <tr>
              <th width="10%"><input type="checkbox" class="participants" id="select_all_partcipants"></th>
              <th width="90%">Participants</th>
              <!-- <th width="65%">Name</th> -->
            </tr>
            </thead>
            <tbody id="program_participant_table_body_id">
            </tbody>
          </table>
     </div>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-lg-6 col-lg-offset-3">
      <div class="form-group Required">
        <label>{{__('labels.email.to')}}</label>
        <select class="multiple_emails form-control mandatory" name="to_emails[]" multiple="multiple" id="email_to_id">
        </select>
        <input type="hidden" id="email_to_id_hidden">
      </div>
    </div>
  </div>
  <p></p>
  {{-- <div class="row">
    <div class="col-xs-6 col-lg-offset-3">
      <div class="form-group">
        <label>{{__('labels.email.cc')}} :</label>
        <select class="multiple_emails form-control" id="email_cc_id" name="cc_emails[]" multiple="multiple"></select>
      </div>
    </div>
  </div>
  <p></p> --}}
  {{-- <div class="row">
    <div class="col-xs-6 col-lg-offset-3">
      <div class="form-group">
        <label>{{__('labels.email.bcc')}} :</label>
        <select class="multiple_emails form-control" id="email_bcc_id" name="bcc_emails[]" multiple="multiple"></select>
      </div>
    </div>
  </div> --}}
  <p></p>
  <div class="row">
   <div class="col-lg-6 col-lg-offset-3">
    <div class="form-group">
      <label>{{__('labels.email.template')}} :</label>
      <select  class="form-control" name="templates" id="templates_id" @yield('disabled_templates')>
        <option value="0">---Select---</option>
        @foreach($templates as $templ)
        <option value="{{$templ->id}}">{{$templ->template_name}}</option>
        @endforeach
      </select>
    </div>

  </div>
</div>
<p></p>
{{-- <div class="row">
  <div class="col-xs-6 col-lg-offset-3">
    <div class="form-group">
      <label>{{__('labels.email.subject')}} :</label>
      <input  class="form-control input-sm" id="subject" name="subject">
    </div>
  </div>
</div>
<p></p> --}}
<div class="row">
  <div class="col-lg-6 col-lg-offset-3">
    {{-- <div id="summernote"></div> --}}
    <textarea class="form-control" id="summernote" name="summernote" ></textarea>
  </div>
</div>
{{-- <p></p>
<div class="row">
  <div class="col-lg-6 col-lg-offset-3">
    <div class="form-group">
      <label>Attach Files</label>
    <div id="drop_zone" ondrop="dropHandler(event);" ondragover="dragOverHandler(event);">
    <p>Drag one or more files to this Drop Zone ...</p>
    </div>
    <input type="file" class="hide" id="file" name="file[]" multiple>
    </div>
  </div>
</div>
<p></p>
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
      <div class="form-group">
        <div class="file_list">

        </div>
      </div>
    </div>
  </div>
  <p></p> --}}
  {{-- <div class="row"> --}}
    {{-- <div class="col-lg-6 col-lg-offset-3" id="addProgress"> --}}
      {{-- <div class="progress">
        <div class="progress-bar progress-bar-success progress-bar-striped active myprogress" role="progressbar" style="width:0%">0%</div>
      </div> --}}
    {{-- </div> --}}
  {{-- </div> --}}
<p></p>
<div class="row col-xs-6 col-lg-offset-3" align="center">
  <input  type="button" class="btn-sm  btn-primary hide" id="saveBtn" value="Send">
  {{-- <input type="button" class="btn-sm btn-danger" id="clearBtn" value="Clear"> --}}
  <p></p>
</div>

<!-- </div> -->
</form>
@endsection

@section('css')
<style type="text/css">
.note-editable{
  height: 200px;
  max-height: 200px;
}
.fillFields
{
  border-color: red;
}
</style>

@endsection
@push('scripts')
<style type="text/css">
  #drop_zone {
  border: 1px solid black;
  height: 100px;
}
#drop_zone p{
left: 20%;
position: absolute;
top: 40%;
}
</style>
<script src="{{ asset('/vendor/vnnogile/js/select2.min.js')}}"></script>


<!-- include summernote css/js-->
<link href="{{ asset('/vendor/vnnogile/css/summernote.css')}}" rel="stylesheet">
<!-- <link href="/css/jquery-ui.css" rel="stylesheet"> -->
<link href="{{ asset('/vendor/vnnogile/css/select2.min.css')}}" rel="stylesheet">
<script src="/vendor/vnnogile/js/summernote.js"></script>
<script src="/vendor/vnnogile/js/summer_events_sms.js"></script>
@endpush
