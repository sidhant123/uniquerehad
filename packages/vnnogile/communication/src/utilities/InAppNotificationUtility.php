<?php

namespace vnnogile\Communication\Utilities;

//Custom Imports Below
use vnnogile\Communication\Jobs\InAppNotificationJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * This is a a common utility method for sending In App Notifications
 *
 */
class InAppNotificationUtility
{

    /**
     * Send in App Notifications
     * @param Array $role_data
     * @return Array $status
     */
    public static function sendInAppNotification($role_data){
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        foreach ($role_data as $id => $value) {

            $inAppNotificationData['template_name']=$value['template'];
            $inAppNotificationData['data']=$value['template_data'];
            $inAppNotificationData['to']=$value['email_id'];
            $inAppNotificationData['party_id']=$value['party_id'];
            $inAppNotificationData['tenant_code']=$value['tenant_code'];
            $inAppNotificationData['login_id']=$value['login_id'];
            //$inAppNotificationData['tenant_party_id']=$value['tenant_party_id'];
            //$inAppNotificationData['notificationsion']=$value['notification'];
            if(isset($value['template_cc'])){
                //What does CC mean in App Notification?
                $inAppNotificationData['cc']=$value['template_cc'];
            }


            InAppNotificationJob::dispatch($inAppNotificationData)->onQueue('notifications');

            Log::debug("In App Notification Saved For " . $id);  
        }
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        return "success";

    }   
}
