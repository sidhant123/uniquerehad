<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Services\OrganizationService;
use Illuminate\Support\Facades\Log;
use vnnogile\Pagination\Utilities\PaginateController;
use vnnogile\Client\Controllers\BaseController;
use App\Models\LearningTheme;
use App\Services\ValidationRulesService;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Session;
use DB;
use App\Services\LearningThemeService;


class LearningThemeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getMasterProgramName($customer_party_id) {
        $masterPrograms = DB::table('master_programs')->select('id', 'name')->where('customer_party_id', $customer_party_id)->get();
        // dd($masterPrograms);
        return $masterPrograms;
    }

    public function index(Request $request, $flag=false)
    {
        // $now = new DateTime('Today');
        // dd($request->path());
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::debug(\Auth::user()->hasAccess(['Users']));
        // $this->authorize('List Users', 'List Users');
        //parent::populateOrClearSearch($request);
        //self::filterParameters($request);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        //return PaginateController::paginate('Person','users', parent::$utilsInterface, parent::$masterDataInterface);
        // dd($request->path());
        return PaginateController::paginate('LearningTheme', $request->path(), parent::$utilsInterface, parent::$masterDataInterface, $flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organization = OrganizationService::retrieveOrganizationForFilter();
        return view('learning_theme.create', compact('organization'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('$validator');
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('LearningTheme');
            $data = $request->all();
            // dd($rules);
            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                // dd($validator);
                    return redirect('learningTheme/create')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            // dd('H');
            $learning_theme_flag = LearningThemeService::saveLearningTheme($data);

            
            if($learning_theme_flag == True) {
                return redirect()->action('LearningThemeController@index');
            }
            else {
                Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
            }
        } catch (Exception $e) {
            Log::error('Sorry! Some Error Occured on Server. :: ', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            // $this->authorize('Edit Users', 'Edit Users');
            $data = LearningTheme::find($id);
            // dd($data);
            Log::debug("***********MASTER PROGRAM*****************");

            $organization_details = DB::table('organizations')->where('party_id', $data->customer_party_id)->first();
            $organization_name =  $organization_details->name;
            $organization_id =  $organization_details->id;

            // dd($data->customer_party_id);
            // dd($organization_name);
            $master_programs = self::getMasterProgramNameLearningThemeEdit($data->master_program_id);
            // dd($data->master_program_id);
            // dd($organization_details);
            $master_program_name = $master_programs->name;
            $master_program_id = $master_programs->id;


            $organization_name =  $organization_details->name;
            $organization = OrganizationService::retrieveOrganization();
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return view('learning_theme.view', compact('data', 'organization', 'organization_name','master_program_name','master_program_id','organization_id'));

        } catch (Exception $e) {
            Log::debug('Some error occured on server!', $e->getMessage());
        }    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            // $this->authorize('Edit Users', 'Edit Users');
            $data = LearningTheme::find($id);
            // dd($data);
            Log::debug("***********MASTER PROGRAM*****************");

            // dd($data->customer_party_id);
            $organization_details = DB::table('organizations')->where('party_id', $data->customer_party_id)->first();

            $organization_name =  $organization_details->name;
            $organization_id =  $organization_details->party_id;
            // dd($data->master_program_id);

            $master_programs = self::getMasterProgramNameLearningThemeEdit($data->master_program_id);
            // dd($data->master_program_id);
            // dd($master_programs);

            $master_program_name = $master_programs->name;
            $master_program_id = $master_programs->id;
            $organization = OrganizationService::retrieveOrganization();
            // dd($organization);
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
            return view('learning_theme.edit', compact('data', 'organization', 'organization_name', 'organization_id', 'master_program_name','master_program_id'));

        } catch (Exception $e) {
            Log::debug('Some error occured on server!', $e->getMessage());
        }    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        try {
            $valdationRulesService = new ValidationRulesService(parent::$masterDataInterface, parent::$utilsInterface);
            $rules = $valdationRulesService->validationRulesWithTenant('LearningTheme', '$user', $id);

            if ($request->ajax()) {
                $request->validate($rules);
            } else {
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return redirect('LearningTheme/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            // dd('He');
            Log::debug(__LINE__ . "::" . __CLASS__ . "::" . __METHOD__ . $validator->fails());
            // exit();
            //Get some basic data ahead  of time and set it in data object
            // dd($id);
            $learning_theme_update_flag = LearningThemeService::updateLearningTheme($data, $id);

            if($learning_theme_update_flag == 'True') {                
                Session::flash('status', __('labels.flash_messages.update'));
                return redirect()->action('LearningThemeController@index');
            }
            else {
                Log::debug('Some Error occured on server ', $e->getMessage());                
            }
            Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
        } catch (Exception $e) {
            Log::debug('Some Error occured on server ', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getMasterProgramNameLearningThemeEdit($id) {
        $masterPrograms = DB::table('master_programs')->select('id', 'name')->where('id', $id)->first();
        // dd($masterPrograms);
        return $masterPrograms;
    }

}
