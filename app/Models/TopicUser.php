<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 13 Aug 2018 15:24:27 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TopicUser
 * 
 * @property int $id
 * @property int $program_id
 * @property int $topic_id
 * @property int $parent_topic_id
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class TopicUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'topic_id' => 'int',
		'parent_topic_id' => 'int',
		'user_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'program_id',
		'topic_id',
		'parent_topic_id',
		'user_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
