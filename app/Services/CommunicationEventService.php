<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 Dec 2017 14:31:34 +0530.
 */

namespace App\Services;

//Custom imports
//use App\Models\CommunicationChannel;
use Illuminate\Support\Facades\Log;
/**
 * 
 * 
 */
class CommunicationEventService
{

	public static function communicationEvents($data)
	{
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
		return $data['masterDataInterface']->all('CommunicationEvent');

	}
}
