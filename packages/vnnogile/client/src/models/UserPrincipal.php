<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Jan 2018 18:30:04 +0530.
 */

namespace vnnogile\Client\Models;

/**
 * Class UserPrincipal
 * 
 * @package App\Models
 */
class UserPrincipal
{

	private $userId;
	private $userPartyId;
	private $firstName;
	private $lastName;
	private $email;
	private $phones;	
	private $tenantPartyId;
	private $tenantCode;
	private $tenantName;
	private $customerPartyId;
	private $customerCode;
	private $customerName;
	private $facebookId;
	private $twitterHandle;
	private $linkedinProfile;
	private $photo;
	private $roles;

	public function getUserId()
	{
		return $this->userId;
	}

	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	public function getUserPartyId()
	{
		return $this->userPartyId;
	}

	public function setUserPartyId($userPartyId)
	{
		$this->userPartyId = $userPartyId;
	}

	public function getFirstName()
	{
		return $this->firstName;
	}

	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	public function getLastName()
	{
		return $this->lastName;
	}

	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	public function getPhones()
	{
		return $this->phones;
	}

	public function setPhones($phones)
	{
		$this->phones = $phones;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}	

	public function getTenantPartyId()
	{
		return $this->tenantPartyId;
	}

	public function setTenantPartyId($tenantPartyId)
	{
		$this->tenantPartyId = $tenantPartyId;
	}

	public function getTenantCode()
	{
		return $this->tenantCode;
	}

	public function setTenantCode($tenantCode)
	{
		$this->tenantCode = $tenantCode;
	}

	public function getTenantName()
	{
		return $this->tenantName;
	}

	public function setTenantName($tenantName)
	{
		$this->tenantName = $tenantName;
	}

	public function getCustomerId()
	{
		return $this->customerId;
	}

	public function setCustomerPartyId($customerPartyId)
	{
		$this->customerPartyId = $customerPartyId;
	}

	public function getCustomerPartyId()
	{
 		return $this->customerPartyId;
	}

	public function getCustomerCode()
	{
		return $this->customerCode;
	}

	public function setCustomerCode($customerCode)
	{
		$this->customerCode = $customerCode;
	}

	public function getCustomerName()
	{
		return $this->customerName;
	}

	public function setCustomerName($customerName)
	{
		$this->customerName = $customerName;
	}

	public function getFacebookId()
	{
		return $this->facebook_id;
	}

	public function setFacebookId($facebookId)
	{
		$this->facebookId = $facebookId;
	}

	public function getTwitterHandle()
	{
		return $this->twitterHandle;
	}

	public function setTwitterHandle($twitterHandle)
	{
		$this->twitterHandle = $twitterHandle;
	}

	public function getLinkedinProfile()
	{
		return $this->linkedinProfile;
	}

	public function setLinkedinProfile($linkedinProfile)
	{
		$this->linkedinProfile = $linkedinProfile;
	}

	public function getPhoto()
	{
		return $this->photo;
	}

	public function setPhoto($photo)
	{
		$this->photo = $photo;
	}	

	public function getRoles()
	{
		return $this->roles;
	}

	public function setRoles($roles)
	{
		$this->roles = $roles;
	}
}
