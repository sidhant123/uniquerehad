@extends('layouts.base')
@section('content')		
<div class="row">
	<div class='col-lg-12'>
		{{-- <h3 class="headings">List of {{ucfirst($paginationData['route'])}}</h3> --}}
		<h3 class="headings">List of {{ucfirst($paginationData['finalRoute'])}}</h3>
	</div>
</div>
<p></p>
<div class="row">
	<div class="col-lg-4 col-sm-4">
			<span valign="middle">Records per page <select id="row_count" onchange="return setRowCount(this.value,'{{$paginationData['route']}}')"></select></span>
	</div>
	<div class="col-lg-4 col-sm-4">
			Showing&nbsp;<strong>{{$records->count()}}</strong>&nbsp;records out of&nbsp;<strong>{{$records->total()}}</strong>
	</div>
	<div class='col-lg-4 col-sm-4'>
		<a href="/{{$paginationData['route']}}/create"><button type="button" class="btn btn-small btn-primary pull-right">{{__('labels.generic.new')}}</button></a>
	</div>
</div>
<p></p>
 @if(Session::has('status'))
 <p id="flash_message_id" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('status') }}</p>
 @endif
<div class="row" id="table-div">
	<table  class="table table-sm table-striped table-bordered table-fixed">
		<thead class="thead-dark">
			@foreach ($entityColumns['headerColumnNames'] as $key => $header)

			@if($header === 'Action')
			<th class="unique_rehab_th_class {{$entityColumns['displayColumnCSS'][$key]}}" align="center">{{ $header }} </th>
			@else
			<th class=" unique_rehab_th_class {{$entityColumns['displayColumnCSS'][$key]}}">{{ $header }}
				<a href="/{{$paginationData['route']}}/?sort=desc&sort_column_name={{$entityColumns['queryColumnNames'][$key]}}&rowCount={{$paginationData['rowCount']}}">
					<i class="vnn vnn-menu-down pull-right" aria-hidden="true" style="font-size: 10px">
					</i>
				</a>
				<a href="/{{$paginationData['route']}}/?sort=asc&sort_column_name={{$entityColumns['queryColumnNames'][$key]}}&rowCount={{$paginationData['rowCount']}}">
					<i class="vnn vnn-menu-up pull-right" aria-hidden="true" style="font-size: 10px">&nbsp;&nbsp;</i>
				</a>
			</th>
			@endif
			@endforeach
		</thead>
		<tbody>
			@foreach ($records as $record)
			<tr>

				@foreach($entityColumns['queryColumnNames'] as $key => $columnName)
				@if ($entityColumns['headerColumnNames'][$key] === 'Action')
				<td align="center">
					<a href="/{{ $paginationData['route']}}/{{ $record->$columnName}}/edit" class="disabled1{{ Auth::user()->hasAccess(['Edit ' . ucfirst($paginationData['route'])]) }}">
						<i class="action-vnn vnn-edit" aria-hidden="true"></i>
					</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="#" onclick="return deleteRecord('{{$record->$columnName}}','{{$paginationData['route']}}')" class="disabled1{{ Auth::user()->hasAccess(['Delete ' . ucfirst($paginationData['route'])]) }}">
						<i class="action-big-vnn vnn-del" aria-hidden="true"></i>
					</a> 
				</td>
				@elseif ($key == 0)
				<td class="unique_rehab_td_class" style="padding-right: 8px">
					<a href="/{{ $paginationData['route'] }}/{{$record->id}}">{{ $record->$columnName }}
					</a>
				</td>
				@elseif(strpos($entityColumns['headerColumnNames'][$key], 'Amount'))
				<td align="right"> {{ $record->$columnName }}</td>

				@else
				@if(isset($entityColumns['htmlTag'][$key]))
				@if($entityColumns['htmlTag'][$key] == 'a')
				<td  class="unique_rehab_td_class">
				<a href="{{ $record->$columnName }}" target="_blank">{{ $record->$columnName }}
					</a>
					</td>
				{{-- <td> {{ $record->$columnName }}</td> --}}
				@else
				<td  class="unique_rehab_td_class"> {{ $record->$columnName }}</td>
				@endif
				@else
				<td  class="unique_rehab_td_class"> {{ $record->$columnName }}</td>
				@endif
				@endif
				@endforeach
			</tr>
			@endforeach 			
		</tbody>
	</table>
	{{-- {{ $records->links() }} --}}
	{{ $records->appends($_GET)->links() }}
</div>	
@endsection
@push('scripts')
    <script src="/ui/vnnogile/js/list_view.js"></script>
@endpush


@section('css')
<style>
	.unique_rehab_th_class{
	font-weight: bold;
    font-size: x-small;
    background-color: #d4d4d4;
	}
	/*.unique_rehab_td_class{
		padding-right: 8px !important;
	}*/
	.JColResizer > tbody > tr > td{
		padding-left: 8px !important;
	}
</style>
@endsection
