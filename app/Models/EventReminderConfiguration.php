<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 21 Jul 2018 07:17:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

//Custom Imports
use App\Models\BaseModel;
use Sofa\Eloquence\Eloquence;

/**
 * Class EventReminderConfiguration
 * 
 * @property int $id
 * @property string $event_name
 * @property string $entity_name
 * @property int $entity_id
 * @property string $event_date
 * @property int $duration_in_hours
 * @property int $tenant_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class EventReminderConfiguration extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'entity_id' => 'int',
		'duration_in_hours' => 'int',
		'tenant_party_id' => 'int',
		'is_active' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'event_name',
		'entity_name',
		'entity_id',
		'event_date',
		'duration_in_hours',
		'tenant_party_id',
		'is_active',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
