<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Custom imports
use Illuminate\Support\Facades\Log;

class ScheduledJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
	Log::debug('*******ScheduledJob*******');
	Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
