<?php

namespace App\Listeners;

use App\Events\UserUploadEvent;
use App\Models\ImportJob;

//Custom Imports Below
use App\Models\Organization;
use App\Services\TenantService;
use App\Services\UserService;
use App\Traits\ValidationTrait;
use App\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

class UserUploadEventListener implements ShouldQueue
{
    use ValidationTrait;

    protected $masterDataInterface;
    protected $utilsInterface;
    protected $request;
    public $tries = 1;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
        //$this->request = $request;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
     * Handle the event.
     *
     * @param  UserUploadEvent  $event
     * @return void
     */
    public function handle(UserUploadEvent $event)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $folder = $event->folder;
        $fileName = $event->fileName;
        $userId = $event->userId;
        $importedRecords = 0;
        $errorRecords = 0;

        $keys = array();
        $file = $folder . "/" . $fileName;

        $excel = Excel::load($file)->all()->toArray();
        $data = $excel[2];

        $authenticatedUser = User::find($userId);
        $tenantPartyId = TenantService::getTenantPartyIdForAuthUser($this->utilsInterface, $authenticatedUser);

        try {

            $organization_code = $excel[1][0]['customer_code'];
            $org = Organization::where(
                [
                    ['organization_code', '=', $organization_code],
                    ['tenant_party_id', '=', $tenantPartyId],
                ])->first();
            //validation of users
            $event->organizationCode = $organization_code;
            $event->organizationName = $org->name;

            $rule = [
                'first_name' => 'required|max:255',
                // 'last_name' => 'required|max:255',
                'designation' => 'required|max:500',
                'email' => 'required|unique:people,email,NULL,id,tenant_party_id,' . $tenantPartyId,
            ];

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $newUser = $value;
                    $validationResult = $this->validate($newUser, $rule);

                    if ($validationResult['success'] == true) {
                        $validUsers[] = $value;
                    } elseif ($validationResult['success'] == false) {
                        $value["errors"] = $validationResult['errors'];
                        $invalidUsers[] = $value;
                        foreach ($value as $key => $val) {
                            if (array_key_exists($key, $keys)) {
                            } else {
                                array_push($keys, $key);
                            }
                        }
                    }
                }
            }

            if (isset($validUsers)) {
                foreach ($validUsers as $validUser) {
                    $data = $validUser;
                    foreach ($data as $key => $value) {
                        $data[self::underscoreToCamelCase($key)] = $value;
                    }
                    //the naming convention for organization_name is not OrganizationName
                    //since when user is created through screen, this is the way
                    $data['organization_name'] = $org->id;

                    $data['utilsInterface'] = $this->utilsInterface;
                    $data['masterDataInterface'] = $this->masterDataInterface;
                    $data['authenticatedUser'] = $authenticatedUser;
                    //We set fileUpload parameter as False and fileImport parameter as True
                    Log::debug("data['photoPath']");
                    Log::debug($data['photoPath']);
                    if ($data['photoPath'] != null) {
                        UserService::saveUser($data, false, true);
                        $importedRecords = $importedRecords + 1;
                    } else {
                        UserService::saveUser($data, false, false);
                        $importedRecords = $importedRecords + 1;
                    }

                }
            }

            Log::debug('*****INVALID USERS***');
            if (isset($invalidUsers)) {
                Log::debug($invalidUsers);
            }
            $errorFileName = '1';
            if (isset($invalidUsers)) {
                if (!empty($invalidUsers)) {
                    Log::debug('*****INVALID USERS IN LOOP 1 ***');
                    $timeNow = Carbon::now('Asia/Kolkata')->subDay()->format('h_i_s');
                    $errorFileName = trim($fileName, '.xlsx') . '_errors_' . $timeNow;
                    Excel::create($errorFileName, function ($excel) use ($invalidUsers, $keys, &$errorRecords) {
                        Log::debug('*****INVALID USERS IN LOOP 2 ***');
                        // Set the title
                        $excel->setTitle('Erroneouse Records');
                        // Chain the setters
                        $excel->setCreator('Cognegix')
                            ->setCompany('Cognegix');
                        // Call them separately
                        $excel->setDescription('User Upload Error Records');
                        $excel->sheet('Invalid Users', function ($sheet) use ($invalidUsers, $keys, &$errorRecords) {
                            Log::debug('*****INVALID USERS IN LOOP 3 ***');
                            //$sheet->row(1, array('Sr No', 'First Name', 'Last Name', 'Email', 'Gender', 'Enabled', 'Employee Id', 'Department', 'Designation', 'Linkedin Profile', 'Facebook Id', 'Twitter Handle','Photo Path'));
                            $sheet->row(1, $keys);
                            $counter = 2;
                            Log::debug('*****INVALID USERS IN LOOP 4 ***');
                            foreach ($invalidUsers as $invalidData) {
                                $sheet->row($counter, $invalidData);
                                $counter++;
                                $errorRecords = $errorRecords + 1;
                            }
                        });
                    })->store('xlsx', storage_path('app/' . $event->finalPath));
                    //in above code, we need to give '/app/' other wise file will get saved under different path directly below
                    //storage folder instead of 'storage/app'
                }
            }
            $moveFile['uploadedFileName'] = $event->uploadedFile;
            $moveFile['tempPath'] = $event->tempPath;
            $moveFile['finalPath'] = $event->finalPath;
            $configurations = $this->masterDataInterface->all('Configuration');
            foreach ($configurations as $configuration) {
                $configurationsMap[$configuration->configKey] = $configuration->configValue;
            }
            $moveFile['configurations'] = $configurationsMap;
            Log::debug('****moveFile****');
            Log::debug($moveFile);
            UserService::moveFile($moveFile);
            UserService::deleteDirectory($moveFile, $event->tempPath);

            //Finally set the status
            $importJob = ImportJob::find($event->jobId);
            if ($errorFileName != 1) {
                $importJob->error_file = $event->finalPath . $errorFileName . '.xlsx';
            }

            $importJob->status = "COMPLETED";
            $importJob->save();

            $data['masterDataInterface'] = $this->masterDataInterface;
            $data['utilsInterface'] = $this->utilsInterface;
            $data['authenticatedUser'] = $authenticatedUser;
            $data['fileName'] = $event->fileName;
            if ($errorFileName != 1) {
                $data['errorFileName'] = $importJob->error_file;
            }
            $data['totalRecords'] = $importedRecords + $errorRecords;
            $data['importedRecords'] = $importedRecords;
            $data['errorRecords'] = $errorRecords;
            $data['jobId'] = $event->jobId;
            $data['organizationCode'] = $event->organizationCode;
            $data['organizationName'] = $event->organizationName;
            UserService::sendEmail('UserImportFinishEvent', $data);

        } catch (Exception $exception) {
            Log::error('Exception occurred ' . $exception->getMessage());
            $this->sendEmail($event);
        } catch (\Throwable $exception) {
            Log::error('Throwable exception occurred ' . $exception->getMessage());
            $this->sendEmail($event);
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    private static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
        return $str;
    }

    private function sendEmail($event, $errorMessage = 'Unknown Error')
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::error('***Exception occurred***');
        $data['errorMessage'] = $errorMessage;
        $data['masterDataInterface'] = $this->masterDataInterface;
        $data['utilsInterface'] = $this->utilsInterface;
        $data['authenticatedUser'] = $authenticatedUser = User::find($event->userId);
        $data['fileName'] = $event->fileName;
        $data['totalRecords'] = 0;
        $data['importedRecords'] = 0;
        $data['errorRecords'] = 0;
        $data['jobId'] = $event->jobId;
        $data['organizationCode'] = $event->organizationCode;
        if (isset($event->organizationName)) {
            $data['organizationName'] = $event->organizationName;
        } else {
            $data['errorMessage'] = "Customer code specified in the Excel file is invalid";
        }

        $importJob = ImportJob::find($event->jobId);
        $importJob->status = "FAILED";
        $importJob->save();
        UserService::sendEmail('UserImportFailEvent', $data);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
