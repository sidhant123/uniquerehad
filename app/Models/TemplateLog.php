<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Feb 2018 12:49:43 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\BaseModel;
/**
 * Class TemplateLog
 * 
 * @property int $id
 * @property int $template_id
 * @property string $log
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class TemplateLog extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'template_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'template_id',
		'log',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
