update validations set validation_rule='required|file|mimes:xlsx|max:2000' where field_name='imported_boq_file';


Create sequence project_role_users_seq; 

-- Table: public.project_users

-- DROP TABLE public.project_users;

CREATE TABLE public.project_users
(
  id bigint NOT NULL DEFAULT nextval('project_role_users_seq'::regclass),
  project_id integer NOT NULL,
  user_id integer NOT NULL,
  role_name varchar NOT NULL,
  created_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  deleted_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  created_by integer NOT NULL,
  updated_by integer NOT NULL,
  deleted_by integer,
  CONSTRAINT project_users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.project_users
  OWNER TO postgres;