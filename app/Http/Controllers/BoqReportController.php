<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use vnnogile\Client\Controllers\BaseController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;
use DB;
use Excel;

use App\Services\BoqReportService;
use App\Services\ProjectService;
use App\Models\Project;



class BoqReportController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		parent::__construct();
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}

	public function create() {

        $project = ProjectService::retrieveProject();
		return view('reports.boq_report' ,compact('project'));
	}

	public function exportData(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");

		$projectCode = BoqReportService::getProjectName($request['project_id']);
		$queryBuilder = BoqReportService::retrieveBoqReportQuery($request['project_id']);

	    self::downloadReportExcel($queryBuilder, $projectCode);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadReportExcel($queryBuilder, $projectCode) {
		$fileName = $projectCode . '_BOQ_Report';
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();

        // creating title header row
        // $keys = array();
        // foreach ($row_data as $key => $value) {
        // foreach ($value as $key1 => $value1) {
       
        //     array_push($keys, $key1);
        // }
        // break;
        // }
             
        $keys = array("Project Name","Boq No", "Description","Unit Rate", "Rate", "Quantity", "Amount", "Boq Status");

        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array($row->project_name, $row->boq_no, $row->description, $row->unit_rate, $row->rate, $row->quantity, $row->amount, $row->status, );
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}
////-------- changes on 10 august
public function exportDetailReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		$start_date = $request['start_date'];
		$end_date = $request['end_date'];
		$projectCode = BoqReportService::getProjectName($request['project_id']);
		$queryBuilder = BoqReportService::retrieveBoqReportDetailQuery($request['project_id'],$start_date,$end_date);
// dd($queryBuilder);
	    self::downloadDetailReportExcel($queryBuilder, $projectCode,$start_date,$end_date);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadDetailReportExcel($queryBuilder, $projectCode,$start_date,$end_date ) {
		$fileName = $projectCode . '_BOQ_Detail_Report_From_'.$start_date.'_To_'.$end_date;
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
  //  dd($row_data);
      $keys = array( "Created Date", "Boq No", "Description", "Length", "Width", "Depth", "Quantity", "Total Area of Work
");
        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array(
        		 $row->xcreated_date, $row->name, $row->description, $row->length, $row->width, $row->depth,$row->quantity,$row->total );
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}
	
public function exportSummaryReport(Request $request) {
		Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
		Log::debug("******Report Type and Entity Type******");
		
		$projectCode  = BoqReportService::getProjectName($request['project_id']);
		$queryBuilder = BoqReportService::retrieveBoqSummaryReportQuery($request['project_id']);

	    self::downloadSummaryReportExcel($queryBuilder, $projectCode);
			
		Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
	}


	private function downloadSummaryReportExcel($queryBuilder, $projectCode ) {
		$fileName = $projectCode . '_BOQ_Summarise_Report';
		  ob_end_clean();
          ob_start();
          $row_data = $queryBuilder->get();
         // dd($row_data);
        $keys = array("Boq No", 'Description', "Length", "Width",  "Depth", "Total Area of Work", "Total Quantity", "Actual Quantity" );
        // creating rows  
        $allData = array();
        foreach ($row_data as $row) {
        	$allData[] = array($row->name, $row->description, $row->length, $row->width,  $row->depth,  $row->total, $row->quantity, $row->actual_qty);
		}

        parent::downloadSingleSheetExcelReport($fileName, "Records", "Records", "Records", $keys, $allData);

       //  });
		
	}

}
