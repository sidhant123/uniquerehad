<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'config_cache_key' => 'config_key',
    'config_cache_duration' => 'config_duration',
    'config_cache_tag' => 'configs',

];
