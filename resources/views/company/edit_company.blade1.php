@extends('company.company')
@section('editID', $orgData->id)

@section('editMethod')
{{method_field('PUT')}}
@endsection

@section('title','Edit Company')
@section('organization_name',$orgData->name)
@section('organization_party_id',$orgData->party_id)
@section('old_org_logo',$orgData->logo)
@section('old_org_logo_name',$orgData->original_logo)


@section('organization_url',$orgData->website_url)
@section('organization_code',$orgData->organization_code)

@section('organization_address1',$orgData->orgAddress->address1)
@section('organization_address2',$orgData->orgAddress->address2)
@section('organization_address3',$orgData->orgAddress->address3)
@section('organization_country',$orgData->orgAddress->country_id)
@section('organization_state',$orgData->orgAddress->state_id)
@section('organization_city',$orgData->orgAddress->city_id)
@section('organization_pincode',$orgData->orgAddress->pincode_id)

@section('organization_contact',$orgData->contact)




<!-- for pm -->
@section('pm_employeeId',$pmData->employee_id)
@section('pm_email',$pmData->email)
@section('pm_lastName',$pmData->last_name)
@section('pm_employeeId',$pmData->employee_id)
@section('pm_department',$pmData->department)
@section('pm_designation',$pmData->designation)
@section('pm_linkedinProfile',$pmData->linkedin_profile)
@section('pm_fbProfile',$pmData->facebook_id)
@section('pm_twitterHandle',$pmData->twitter_handle)

@section('pm_twitterHandle',$pmData->twitter_handle)
@section('pm_primaryContact',$pmData->pm_primary_contact)
@section('pm_secondaryContact',$pmData->pm_secondary_contact)

@section('old_pm_employee_id',$pmData->employee_id)
@section('old_pm_party_id',$pmData->party_id)
@section('old_pm_photo',$pmData->photo)
@section('old_pm_photo_name',$pmData->original_photo)

<!-- for cc -->
@section('cc_employeeId',$ccData->employee_id)
@section('cc_email',$ccData->email)
@section('cc_lastName',$ccData->last_name)
@section('cc_employeeId',$ccData->employee_id)
@section('cc_department',$ccData->department)
@section('cc_designation',$ccData->designation)
@section('cc_linkedinProfile',$ccData->linkedin_profile)
@section('cc_fbProfile',$ccData->facebook_id)
@section('cc_twitterHandle',$ccData->twitter_handle)

@section('cc_primaryContact',$ccData->cc_primary_contact)
@section('cc_secondaryContact',$ccData->cc_secondary_contact)

@section('old_cc_employee_id',$ccData->employee_id)
@section('old_cc_party_id',$ccData->party_id)
@section('old_cc_photo',$ccData->photo)
@section('old_cc_photo_name',$ccData->original_photo)



@push('scripts')
<style>

</style>

<script>

$(document).ready(function(){

  // $("pmDetach").css('pointer-events','none');

   // $('#cc_first_name_id').select2().select2("readonly", true);
   // $("#cc_first_name_id").select2({disabled:readonly});
  // $("#sameUser").click(function() { return false; });
   
   $("#pmSelector :input").attr("readonly", true);
   $("#ccSelector :input").attr("readonly", true);

   $("#file1").removeClass("mandatory");
   $('#file2').removeClass("mandatory");
   $('#file3').removeClass("mandatory");
   $('#logo_div').removeClass('req');
   $('#pm_photo_div').removeClass('req');
   $('#cc_photo_div').removeClass('req');
  

$('#sameUser').change(function() {

    // this will contain a reference to the checkbox   
    if (this.checked) {
      console.log($('#ccDetach').prop('checked'));

      if ($('#ccDetach').prop('checked')==true){
     
        console.log('cc is checked');
        setUserdataOnChange();
      }
      else{

        alert("to change the Company Coordinator first detach it");
        // return false;
        $('#sameUser').attr("checked",false);
      }
          
    } 
    
});
function setUserdataOnChange(){
   var r=confirm("Program manager will be Company Coordinator");
        if (r == true) {
           // $("#ccSelector :input").attr("disabled", true);

           // $('#ccDetach').attr("checked",false);
           // $('#ccDetach').attr("disabled",true);

           // $("#ccSelector :input").prop("readonly", true);
           // $('#cc_first_name_id option:not(:selected)').prop('disabled', true);
           $("#ccDetach").prop("readonly", true);


           var z = $('#pm_user_first_name_id').select2('data');

           if ($('#cc_first_name_id').find("option[value='" + z[0].id + "']").length) {
               $('#cc_first_name_id').val(z[0].id).trigger('change');
               } else { 
               // Create a DOM Option and pre-select by default
               var newOption = new Option(z[0].text, z[0].id, true, true);
                  // Append it to the select
               $('#cc_first_name_id').append(newOption).trigger('change');
               } 

           //setting  value           
           setValue('cc_lastName','pm_lastName');
           setValue('cc_employeeId','pm_employeeId');
           setValue('cc_department','pm_department');
           setValue('cc_email','pm_email');
           setValue('cc_designation','pm_designation');
           setValue('cc_primaryContact','pm_primaryContact');
           setValue('cc_secondaryContact','pm_secondaryContact');
           setValue('cc_linkedinProfile','pm_linkedinProfile');
           setValue('cc_fbProfile','pm_fbProfile');
           setValue('cc_twitterHandle','pm_twitterHandle');
           
           console.log("ok");

           $('#file3').removeClass('mandatory');


           
         } 
      else {
           $('#sameUser').attr("checked",false);
      }
}
// $("#pmDetach").on("click", false);

$('#pmDetach').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {

      var id=$('#pm_user_first_name_id').val();	
      console.log(id);
      
      var r = confirm("Are you sure you want to detach this user and add a new user as a Program manager");
      if (r == true) {
              	
              // $('#pmDetach').attr("disabled",true);
              $("#pmSelector :input").attr("readonly", false);
              $('#pm_user_first_name_id option:not(:selected)').prop('disabled', false);

              $('#pmSelector').find('input:text').val('');
              $("#pm_user_first_name_id").select2("val","0");

              $("#pmDetach").prop("readonly", true);

         } 
      else {
           $('#pmDetach').attr("checked",false);

      }
    } else {}
    
});

$('#ccDetach').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {

      var id=$('#cc_first_name_id').val();	
     
      var r = confirm("Are you sure you want to detach this user and add a new user as a Company Coordinator");
      if (r == true) {
      
              $("#ccSelector :input").attr("readonly", false);
              $('#cc_first_name_id option:not(:selected)').prop('disabled', false);

              $('#ccSelector').find('input:text').val('');
              $("#cc_first_name_id").select2("val", "0");

              $("#ccDetach").prop("readonly", true);


         } 
      else {
           $('#ccDetach').attr("checked",false);
      }
    } 
    else {}
    
});

     function setValue(a,b){
              // var c=$("#"+b).val();
              var c=$("input[name="+b+"]").val();
              // $("#"+a).val(c);

              $("input[name="+a+"]").val(c);
              }


$(document).on('change','#pm_last_name_id',function(){
if(isNaN($('#pm_user_first_name_id').val())){

  $('#file2').addClass("mandatory");
  console.log("class is changed to mandatory");
  // alert("mandatory");
}
else{
  $('#file2').removeClass("mandatory");
  console.log("mandatory is removed");
  // alert("removed");
   $('#file2').removeClass('fillFields');
}
});


$(document).on('change','#cc_last_name_id',function(){
  if(isNaN($('#cc_first_name_id').val())){
   
  $('#file3').addClass("mandatory");
  console.log("class is changed");

}
else{
   $('#file3').removeClass("mandatory");
  console.log("mandatory is removed");
  $('#file3').removeClass('fillFields');
}

});
             

});


</script>

@endpush
