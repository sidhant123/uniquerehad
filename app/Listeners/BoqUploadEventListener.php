<?php

namespace App\Listeners;

use App\Events\BoqUploadEvent;
use App\Models\ImportJob;
use App\Models\Boq;

//Custom Imports Below
use App\Models\Organization;
use App\Services\TenantService;
use App\Services\UserService;
use App\Services\BoqService;
use App\Traits\ValidationTrait;
use App\User;
use Carbon\Carbon;
use Excel;
//use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use vnnogile\Client\Interfaces\MasterDataInterface;
use vnnogile\Client\Interfaces\UtilsInterface;

class BoqUploadEventListener 
{
    use ValidationTrait;

    protected $masterDataInterface;
    protected $utilsInterface;
    protected $request;
    public $tries = 1;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MasterDataInterface $masterDataInterface, UtilsInterface $utilsInterface)
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        $this->masterDataInterface = $masterDataInterface;
        $this->utilsInterface = $utilsInterface;
        //$this->request = $request;
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    /**
     * Handle the event.
     *
     * @param  BoqUploadEvent  $event
     * @return void
     */
    public function handle(BoqUploadEvent $event)
    {

        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");

        $folder = $event->folder;
        $fileName = $event->fileName;
        $userId = $event->userId;
        $importedRecords = 0;
        $errorRecords = 0;

        $keys = array();
        $file = $folder . "/" . $fileName;

       //carry out verification
                    $excel = Excel::load($folder . '/' . $fileName);    
                    $excelData = $excel->all()->toArray();  
                    $sheetNames = $excel->getSheetNames();  
                   if(count($sheetNames)>1){
                    $data = $excelData[0];
                   }else {
                        $data = $excelData;
                   }                   
        $authenticatedUser = User::find($userId);

        try {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                        $validRow[] = $value;
                }
            }

$parent_id=null;
$temp_boq_id=null;
            if (isset($validRow)) {
$counter =intval(1);
                foreach ($validRow as $validBoq) {
                    $data = $validBoq;
                    foreach ($data as $key => $value) {
                        $data[self::underscoreToCamelCase($key)] = $value;
                    }
                    //the naming convention for organization_name is not OrganizationName
                    //since when user is created through screen, this is the way

                    $data['utilsInterface'] = $this->utilsInterface;
                    $data['masterDataInterface'] = $this->masterDataInterface;
                    $data['authenticatedUser'] = $authenticatedUser;
                    //We set fileUpload parameter as False and fileImport parameter as True
                    if(ctype_alpha($data['srno'])){
                            $data['boq_type'] = "Header";
                            $data['boq_parent_id'] = null;
                        }else {
                            $data['boq_type'] = "Detail"; 
                            $data['boq_parent_id'] = $tmp_parent_id;
                        }
                    $data['counter'] = $counter;
                    if($event->route_name == 'store'){
                         $counter++;

                        $tmp_parent_id = BoqService::saveBoq($data, $event->project_id);
                    } 
                    else {
                    $boq_data = Boq::where('name', $data['boq_no'])->get();

                    if(count($boq_data)==0){
                        $counter++;

                        $tmp_parent_id = BoqService::saveBoq($data, $event->project_id);
                        Log::debug("*****add* EDIT BOQ ADD FUNCTION CALLED ******".$data['boq_no']);
                    }else {
                        $data['project_id'] = $event->project_id;
                        $tmp_parent_id = BoqService::editBoq($data); 
                    }

                    }

                }   // foreeach 2

               // exit();
                   
                        // $importedRecords = $importedRecords + 1;
                        //Log::debug('***** DD save boq *** BOQ ID ='.$temp_boq_id);
              
                } // end second
            // end issett 
            //Finally set the status
            $importJob = ImportJob::find($event->jobId);
            // if ($errorFileName != 1) {
            //     $importJob->error_file = $event->finalPath . $errorFileName . '.xlsx';
            // }

            $importJob->status = "COMPLETED";
            $importJob->save();

            $data['masterDataInterface'] = $this->masterDataInterface;
            $data['utilsInterface'] = $this->utilsInterface;
            $data['authenticatedUser'] = $authenticatedUser;
            $data['fileName'] = $event->fileName;
            // if ($errorFileName != 1) {
            //     $data['errorFileName'] = $importJob->error_file;
            // }
            $data['totalRecords'] = $importedRecords + $errorRecords;
            $data['importedRecords'] = $importedRecords;
            $data['errorRecords'] = $errorRecords;
            $data['jobId'] = $event->jobId;
           // $data['organizationCode'] = $event->organizationCode;
            //$data['organizationName'] = $event->organizationName;
            //UserService::sendEmail('UserImportFinishEvent', $data);

        } catch (Exception $exception) {
            Log::error('Exception occurred ' . $exception->getMessage());
            //$this->sendEmail($event);
        } catch (\Throwable $exception) {
            Log::error('Throwable exception occurred ' . $exception->getMessage());
            //$this->sendEmail($event);
        }

        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }

    private static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
        return $str;
    }

    private function sendEmail($event, $errorMessage = 'Unknown Error')
    {
        Log::debug(__CLASS__ . "::" . __METHOD__ . " started");
        Log::error('***Exception occurred***');
        $data['errorMessage'] = $errorMessage;
        $data['masterDataInterface'] = $this->masterDataInterface;
        $data['utilsInterface'] = $this->utilsInterface;
        $data['authenticatedUser'] = $authenticatedUser = User::find($event->userId);
        $data['fileName'] = $event->fileName;
        $data['totalRecords'] = 0;
        $data['importedRecords'] = 0;
        $data['errorRecords'] = 0;
        $data['jobId'] = $event->jobId;
        $data['organizationCode'] = $event->organizationCode;
        if (isset($event->organizationName)) {
            $data['organizationName'] = $event->organizationName;
        } else {
            $data['errorMessage'] = "Customer code specified in the Excel file is invalid";
        }

        $importJob = ImportJob::find($event->jobId);
        $importJob->status = "FAILED";
        $importJob->save();
        UserService::sendEmail('UserImportFailEvent', $data);
        Log::debug(__CLASS__ . "::" . __METHOD__ . " finished");
    }
}
