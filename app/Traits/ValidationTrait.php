<?php

namespace App\Traits;

//Custom Imports
use Validator;
use Illuminate\Support\Facades\Log;
trait ValidationTrait {
	public function validate($data, $rules)
    {
    	// make a new validator object
		$v = Validator::make($data, $rules);
		$validationResults[] = [];
		// check for failure
		Log::info('***** VALIDATOR MAKE OUTPUT START *****');
		Log::info($data);
		Log::info('***** VALIDATOR MAKE OUTPUT END *****');		
		if ($v->fails())
		{

		   // set errors and return false
			$validationResults['success']=false;
			$validationResults['errors']=$v->errors();
		   return $validationResults;
		}
		// validation pass
		$validationResults['success']=true;
		$validationResults['message']='Validation successful';
		return $validationResults;
    }


    public static function validateInputData($data, $rules, $custom_msg = array())
    {
        // make a new validator object
        $v = Validator::make($data, $rules,$custom_msg);
        $validationResults[] = [];
        // check for failure
        Log::info('***** VALIDATOR MAKE OUTPUT START *****');
        Log::info($data);
        Log::info('***** VALIDATOR MAKE OUTPUT END *****');     
        if ($v->fails())
        {

           // set errors and return false
            $validationResults['success']=false;
            $validationResults['errors']=$v->errors();
           return $validationResults;
        }
        // validation pass
        $validationResults['success']=true;
        $validationResults['message']='Validation successful';
        return $validationResults;
    }


    	public function validateData($data, $rules)
        {
        	// make a new validator object
    		$v = Validator::make($data, $rules);
    		$validationResults[] = [];
    		// check for failure
    		Log::info('***** VALIDATOR MAKE OUTPUT START *****');
    		Log::info($data);
    		Log::info('***** VALIDATOR MAKE OUTPUT END *****');		
    		if ($v->fails())
    		{

    		   // set errors and return false
    			$validationResults['success']=false;
    			$validationResults['errors']=$v->errors();
    		   return $validationResults;
    		}
    		// validation pass
    		$validationResults['success']=true;
    		$validationResults['message']='Validation successful';
    		return $validationResults;
        }
}