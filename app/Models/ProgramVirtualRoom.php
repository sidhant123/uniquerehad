<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 08 Jun 2018 19:11:44 +0530.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgramVirtualRoom
 * 
 * @property int $id
 * @property int $program_id
 * @property string $meeting_id
 * @property string $meeting_subject
 * @property \Carbon\Carbon $start_datetime
 * @property string $meeting_duration
 * @property string $moderator_password
 * @property string $participant_password
 * @property string $moderator_default_message
 * @property string $welcome_message
 * @property int $is_recording
 * @property string $default_layout
 * @property string $logout_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @package App\Models
 */
class ProgramVirtualRoom extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'program_id' => 'int',
		'is_recording' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'start_datetime'
	];

	protected $hidden = [
		'moderator_password',
		'participant_password'
	];

	protected $fillable = [
		'program_id',
		'meeting_id',
		'meeting_subject',
		'start_datetime',
		'meeting_duration',
		'moderator_password',
		'participant_password',
		'moderator_default_message',
		'welcome_message',
		'is_recording',
		'default_layout',
		'logout_url',
		'created_by',
		'updated_by',
		'deleted_by'
	];
}
